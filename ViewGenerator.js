var fs = require('fs');
var lib = require('./prod/bz.js')

try {
    let views = lib.exportViews();
    if (views) {
        fs.writeFile('./CB_resources/files/views.json', views, (err) => {
            if(err) {
                // return '';
                console.log('Views export incomplete.', err)
            }
            console.log("Views export done.");
        })
    } else {
        console.log('Views generate failure 1.');
    }
} catch (ex) {
    console.log('Views generate failure.', ex);
}
