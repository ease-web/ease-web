var http = require('http');
var fs = require('fs');
var _ = require('lodash');

const config = require('../conf/extConfig.json');

global.config = config;

const basicPlanList = ['SAV', 'IND', 'BAA', 'ASIM', 'FPX', 'FSX', 'TPX', 'TPPX', 'AWT', 'PUL', 'RHP', 'ESP', 'LMP', 'HIM', 'HER'];

const main = function () {
  const paras = process.argv;
  if (paras) {
    for (var i = 1; i < paras.length; i++) {
      if (paras[i] === '-path') {
        global.config.sg.path = paras[++i];
      } else if (paras[i] === '-port') {
        global.config.sg.port = paras[++i];
      } else if (paras[i] === '-name') {
        global.config.sg.name = paras[++i];
      } else if (paras[i] === 'basicPlan') {
        return exportProduct([paras[++i]], true, true, false);
      } else if (paras[i] === 'plan') {
        return exportProduct([paras[++i]], false, true, false);
      } else if (paras[i] === 'list') {
        return exportProduct(basicPlanList, true, false, true);
      } else if (paras[i] === 'all') {
        return exportProduct(basicPlanList, true, true, true);
      }
    }
  }
  console.log(
    'Usage: node product_exporter.js [OPTION] basicPlan COV_CODE\n' +
    '  or:  node product_exporter.js [OPTION] plan COV_CODE\n' +
    '  or:  node product_exporter.js [OPTION] list\n' +
    '  or:  node product_exporter.js [OPTION] all'
  );
};

// by binary
const getAttachmentByBinary = function(name, cb) {
  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + name,
    method: 'GET',
  };
  console.log('INFO: getAttachmentByBinary: Options: ',options.path);
  var req = http.request(options, (res) => {
    res.on('data', (chunk) => {
      cb(chunk, true);
    });
    res.on('end', () => {
      cb(false);
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getFromSG = function(method, name, cb) {
  if (name && typeof name !== 'string') {
    console.log("ERROR: invalid doc name:", name);
    cb(false);
    return;
  }
  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + encodeURIComponent(name),
    method: method
  };
  console.log('INFO: getFromSG Options: ',options.path);
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      console.log('INFO: getFromSG End:', (resp.length < 100 && resp) || resp.length);
      if (typeof cb === 'function') {
        if (resp) {
          resp = JSON.parse(resp);
          cb(resp);
        } else {
          console.log('Error: getFromSG non-string resp?', resp);
          cb(false);
        }
      }
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getDoc = function(docId, callback) {
  getFromSG('GET', docId, callback);
};

const queryFromSG = function(method, name, params, updateIndex, cb) {
  var paramStr = '';
  if (params) {
    for (var p in params) {
      if (params[p]) {
        paramStr += (paramStr ? "&" : "?") + p + '=' + params[p];
      }
    }
  }
  //set stale to false, The index is updated before the query is executed
  paramStr += (paramStr ? "&" : "?") + (updateIndex ? "stale=false" : "stale=ok");

  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + name + (paramStr ? paramStr : ''),
    method: method,
    params: {
      params
    },
    header: {
      'Content-type': 'application/json'
    }
  };
  console.log('INFO: queryFromSG: Options: ', options.path);

  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      console.log('INFO: queryFromSG End:', (resp.length < 100 && resp) || resp.length);
      if (cb && typeof cb === 'function') {
        if (resp) {
          resp = JSON.parse(resp);
          cb(resp);
        } else {
          console.log('ERROR: queryFromSG:', resp);
          cb(false);
        }
      }
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getViewRange = function(ddname, vname, start, end, params, callback) {
  var query = {
    startkey: start ? start : null,
    endkey: end ? end : null
  };

  if (params) {
    for (var p in params) {
      query[p] = params[p];
    }
  }

  // get session
  try {
    var updateIndex = false;
    queryFromSG('GET', '_design/' + ddname + '/_view/' + vname, query, updateIndex, function(res) {
        console.log('remoteCB + res: ', res);
        if (typeof callback === 'function') {
            callback(res);
        }
    });
  } catch (ex) {
      callback(false);
  }
};

const exportProduct = function (covCodes, isBasicPlan, writeJson, writeList) {
  let importFile = null;
  if (writeList) {
    importFile = fs.createWriteStream('products.txt');
  }
  getViewRange('main', 'products', '["01","0","0"]', '["01","Z","ZZZ"]', '', function(list) {
    if (list && list.total_rows > 0) {
      let promise = Promise.resolve();
      _.each(list.rows, (row) => {
        if (!covCodes.length || covCodes.indexOf(getCovCodeFromProductId(row.id)) > -1) {
          promise = promise.then(() => {
            return new Promise((resolve) => {
              getDoc(row.id, resolve);
            }).then((planDetail) => {
              return saveProduct(planDetail, writeJson, importFile).then(() => {
                if (!isBasicPlan) {
                  return;
                }
                return saveTemplates(planDetail, writeJson, importFile).then(() => {
                  let p = Promise.resolve();
                  _.each(planDetail.riderList, r => {
                    p = p.then(() => getRider(r.covCode).then((rider) => saveProduct(rider, writeJson, importFile)));
                  });
                  return p;
                });
              });
            });
          });
        }
      });
    }
  });
};

const getRider = function (covCode) {
  return new Promise((resolve) => {
    getViewRange('main', 'products', '["01","R","' + covCode + '"]', '["01","R","' + covCode + '"]', '', function (list) {
      if (list && list.rows) {
        getDoc(list.rows[0].id, resolve);
      } else {
        resolve(null);
      }
    });
  });
};

const saveProduct = function (planDetail, writeJson, importFile) {
  if (!planDetail) {
    return null;
  }
  if (writeJson) {
    const productJson = getCleanDoc(planDetail);
    fs.writeFileSync('./files/product/' + planDetail._id + '.json', JSON.stringify(productJson, null, 4));
  }
  if (importFile) {
    importFile.write('D\t' + planDetail._id + '\t' + planDetail._id + '.json\n');
  }
  return saveAttachments(planDetail, writeJson, importFile);
};

const saveTemplates = function (planDetail, writeJson, importFile) {
  let p = Promise.resolve();
  var pdfCodes = new Set();
  _.each(planDetail.reportTemplate, (reportTemplate) => {
    if (pdfCodes.has(reportTemplate.pdfCode)) {
      return;
    }
    pdfCodes.add(reportTemplate.pdfCode);
    p = p.then(() => {
      return new Promise((resolve) => {
        getViewRange('main', 'pdfTemplates',
          '["01","QUOT","' + reportTemplate.pdfCode + '"]',
          '["01","QUOT","' + reportTemplate.pdfCode + '"]',
          null,
          resolve
        );
      }).then((list) => {
        return Promise.all(_.map(list.rows, (row) => {
          return new Promise((resolve) => {
            getDoc(row.id, (template) => {
              if (writeJson) {
                const json = getCleanDoc(template);
                fs.writeFileSync('./files/product/' + template._id + '.json', JSON.stringify(json, null, 4));
              }
              if (importFile) {
                importFile.write('D\t' + template._id + '\t' + template._id + '.json\n');
              }
              resolve();
            });
          });
        }));
      });
    });
  });
  return p;
};

const saveAttachments = function (planDetail, writeJson, importFile) {
  let p = Promise.resolve();
  _.each(planDetail._attachments, (att, key) => {
    p = p.then(() => {
      return new Promise((resolve) => {
        const type = att.content_type;
        let extension;
        if (type.indexOf('pdf') > 0) {
          extension = '.pdf';
        } else if (type.indexOf('jpg') > 0 || type.indexOf('jpeg') > 0) {
          extension = '.jpg';
        } else {
          extension = '.png';
        }
        if (importFile) {
          importFile.write('A\t' + planDetail._id + '\t' + key + '\t' + planDetail.covCode + '_' + key + extension + '\n');
        }
        if (writeJson) {
          const ws = fs.createWriteStream('./files/product/' + planDetail.covCode + '_' + key + extension);
          getAttachmentByBinary(planDetail._id + '/' + key, (chunk, isData) => {
            if (isData) {
              ws.write(chunk);
            } else {
              ws.end();
              resolve();
            }
          });
        } else {
          resolve();
        }
      });
    });
  });
  return p;
};

const getCleanDoc = function (doc) {
  const json = {};
  _.each(doc, (val, key) => {
    if (key !== '_id' && key !== '_attachments' && key !== '_rev') {
      json[key] = val;
    }
  });
  return json;
};

const getCovCodeFromProductId = (productId) => {
  if (!productId) {
    return null;
  }
  let m = productId.match(/_product_(.*)_[0-9]+$/);
  if (m && m.length > 1) {
    return m[1];
  }
};

main();
