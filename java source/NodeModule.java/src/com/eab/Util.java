package com.eab;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.codec.binary.Hex;

public class Util {	
	public static int sum(int a, int b) {
		return a + b;
	}
	
	public static int subtract(int a, int b) {
		return a - b;
	}
	
	public static String GenerateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return Base64.getEncoder().encodeToString(bytes);
	}
	
	public static String GenerateRandomASCII(int length) {
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		// Random rng = new Random();
		java.security.SecureRandom rng = new java.security.SecureRandom();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}
	
	public static String GetCurrentDateTimeString() {
		return new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());
	}
	
	public static String ByteToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToByte(String s) {
		return new BigInteger(s, 16).toByteArray();
	}
	
	public static String ByteToStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes)
			sb.append((char) b);
		return sb.toString();
	}

	public static byte[] StrToByte(String text) {
		return text.getBytes();
	}
}
