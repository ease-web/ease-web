/* eslint no-console: 0 */
"use strict"

if (process.env.ENABLE_DYNATRACE) {
  console.log('[Dynatrace] >>> Process (' + process.env.DT_COLLECTOR_HOST + ' | ' + process.env.DT_AGENT_NAME + ')');
  try {
    require('/opt/app-root/src/dynatrace/dynatrace-oneagent-7.0/agent/bin/any/onenodeloader.js')({
      server: process.env.DT_COLLECTOR_HOST,
      agentName: process.env.DT_AGENT_NAME
    });
	console.log('[Dynatrace] >>> Dynatrace Agent is enabled.');
  } catch (err) {
    // logger.error('Error Initializing Dynatrace Agent');
	console.log('[Dynatrace] >>> Error Initializing Dynatrace Agent.');
	throw new Error(err);
  }
}

var domain = require('domain');
// create a top-level domain for the server
var serverDomain = domain.create();
const express = require('express');
const session = require('express-session');
const cookieParser = require( 'cookie-parser');
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http');
// const https = require('https');
const url = require('url');
const fs = require('fs');
const helmet = require('helmet');
// var cron = require('node-cron');

// var task = cron.schedule('*/1 * * * *', function() {
//   console.log('will execute every minute until stopped');
// });
// task.start();
const csurf = require('csurf');  // for csrf checking
const moment = require('moment');
var os = require("os");
var hostname = os.hostname();

const MODE_TYPE = {
  PROD: "PRO",
  SIT: "SIT",
  DEV: "DEV",
  LOCAL: "LOC"
}

let logger = global.logger = {
  info: console.info,
  log: console.log,
  debug: console.debug,
  warn: console.warn,
  error: console.error,
  trace: console.trace
}

global.NODE_ENV = process.env.NODE_ENV;
let mode = global.MODE = process.env.CONFIG;

logger.log('Node env:', process.env.NODE_ENV);

if (!global.rootPath) {
  global.rootPath = __dirname;
}

const config = require('./conf/extConfig.json');

if (!global.config) {
  global.config = config;
}

const versionFile = require('./version.json');
global.config.version = versionFile.version;
global.config.build = versionFile.build;

let ser = null;
if (global.MODE == MODE_TYPE.PROD || global.MODE == MODE_TYPE.SIT ) {
  ser = require('./prod/bz.js');
} else {
  ser = require('./bz');
}

ser.initServer(function(success) {
  global.logger.info = function(...arg) { let now = moment(); console.info(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | INFO ]", ...arg)}
  global.logger.log = function(...arg) { let now = moment(); console.log(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" |  LOG ] ", ...arg)}
  global.logger.debug = function(...arg) { if (global.config.debugMode) {let now = moment(); console.log(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | DEBUG]", ...arg)}}
  global.logger.warn = function(...arg) { let now = moment(); console.warn(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | WARN ]", ...arg)}
  global.logger.error = function(...arg) { let now = moment(); console.error(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | ERROR]", ...arg)}
  global.logger.trace = function(...arg) { if (global.config.debugMode) {let now = moment(); console.trace(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | TRACE]", ...arg)}}

  global.logger.req = function(...arg) { let now = moment(); console.log(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" |  REQ ] ", ...arg)}
  global.logger.resp = function(...arg) { let now = moment(); console.log(now.format('YYYY-MM-DD HH:mm:ss.SSS (Z)'), "["+hostname+" | RESP ] ", ...arg)}

  if (!success) {
    logger.error("Failure to access couchbase server. Server startup failure.");
    return;
  }

  logger.log('global config loaded: ', !!global.config && !!global.config.sg);

  if (!global.documentCache) {
    global.documentCache = {}
  }

  if (!global.pdfToken) {
    global.pdfToken = new Map();
  }

  const app = express();

  const PORT = global.config.host.port;
  //session expiration in mins
  const sessionTimeOut = 17; // 15 mins session timeout + 2 mins buffer
  let sessOptions = {
    name: 'vssid',
    secret: global.config.sessionSecret,
    resave: false,
    rolling: true,
    saveUninitialized: true,
    // expires: new Date(Date.now() + sessionTimeOut * 60 * 1000),
    cookie: {
      //domain: '121web.com', // define if any
      maxAge: sessionTimeOut * 60 * 1000,
      path: '/',
      httpOnly: true, // enable http only
      secure: true // true for https
    }
  }

  if (global.MODE == MODE_TYPE.PROD && global.config.redis.port && global.config.redis.path) {
    logger.log('Use Redis for Session store.')
    var redisStore = require('connect-redis')(session);
    var redis   = require("redis");
    var redisClient  = redis.createClient(global.config.redis.port, global.config.redis.path);
    global.redisClient = redisClient;
    sessOptions.store = new redisStore({logErrors: true, client: redisClient});
  } else {
    if (!global.tempShareStorage) {
      global.tempShareStorage = {};
    }
    global.redisClient = {
      set: function(key, value) {
        global.tempShareStorage[key] = value;
      },
      get: function(key, cb) {
        cb(false, global.tempShareStorage[key]);
      }
    }
  }

  var sessionStore = null;

  //const samlconfig = require('./bz/saml/config/config')['axa'];
  if (global.config.saml) {
    // var privateKey  = fs.readFileSync('./saml_server.key', 'utf8');
    // var certificate = fs.readFileSync('./saml_server.crt', 'utf8');
    // var credentials = {key: privateKey, cert: certificate};

    var samlconfig = {
      app: {
        name: 'AXASG EASE 2',
        port: global.config.host.port || 3000
      },
      passport: {
        strategy: 'saml',
        saml: {
          path: global.config.saml.path,
          entryPoint: global.config.saml.host,
          issuer: 'passport-saml',
          cert: global.config.saml.cert,
          privateCert: fs.readFileSync('./saml_privkey.pem', 'utf8'),
          encryptedSAML: true,
          identifierFormat:"urn:oasis:names:tc:SAML:2.0:nameid-format:transient"
        }
      }
    };

    var passport = require('passport');
    //require('./bz/saml/config/passport')(passport, samlconfig);
    let SamlStrategy = require('passport-saml').Strategy;
    passport.serializeUser(function (user, done) {
      done(null, user);
    });

    passport.deserializeUser(function (user, done) {
      done(null, user);
    });

    passport.use(new SamlStrategy(
      {
    //    path: config.passport.saml.path,
        path: samlconfig.passport.saml.path,
        entryPoint: samlconfig.passport.saml.entryPoint,
        issuer: samlconfig.passport.saml.issuer,
        cert: samlconfig.passport.saml.cert
      },
      function (profile, done) {
        return done(null,
          {
            id: profile.uid,
            email: profile.email,
            displayName: profile.cn,
            firstName: profile.givenName,
            lastName: profile.sn
          });
      })
    );
  }

  var mainHandle = function(req, res) {
      var reqd = domain.create();
      reqd.add(req);
      reqd.add(res);
      reqd.on('error', function(er) {
        var ts_hms = new Date();
        //ts_hms.format("%Y-%m-%d %H:%M:%S");
        logger.error(ts_hms + ' [Domain] Error:', req.url, '\n', er.stack || er);
        try {
          res.writeHead(500);
          res.end('Error occurred, sorry.');
        } catch(er) {
          logger.error('[Domain] Error sending 500:', req.url, '\n', er.stack || er);
        }
      });
      app(req, res);
    }

  serverDomain.run(function() {
    // server is created in the scope of serverDomain
    var server = http.createServer(mainHandle);

    if (process.env.NODE_ENV == 'development') {
      logger.log('start hotdeploy:');
      const webpack = require( 'webpack');
      const webpackDevMiddleware = require( 'webpack-dev-middleware');
      const webpackHotMiddleware = require( 'webpack-hot-middleware');
      const webpackConfig = require('./webpack.config.development');
      const compiler = webpack(webpackConfig);

      app.use(webpackDevMiddleware(compiler, {
        publicPath: webpackConfig.output.publicPath,
        stats: {
          colors: true
        }
      }));

      app.use(webpackHotMiddleware(compiler));
    }

    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
      extended: true,
      limit: '50mb'
    }));
    app.post('/setAttachmentSigned', function(req, res, next) {
      ser.setAttachmentSigned(req, res, next);
    });
    app.set('trust proxy', 1)
    app.use(session(sessOptions));
    app.use(cookieParser());

    // FIXME: temporary disable for embed pdf viewer, should uncomment below
    // app.use(helmet());
    // app.use(helmet.contentSecurityPolicy({
    //   // Specify directives as normal.
    //   directives: {
    //     defaultSrc: ["'self'"],
    //     scriptSrc: ["'self'", "'unsafe-eval'", "'unsafe-inline'"],
    //     connectSrc: ["'self'", global.config.host.socket],
    //     styleSrc: ["'self'", "'unsafe-inline'"],
    //     imgSrc: ["'self'", "data:", "blob:"],
    //     childSrc: ["'self'"],
    //     fontSrc: ["'self'"],
    //     sandbox: ['allow-forms', 'allow-scripts', 'allow-same-origin', 'allow-modals', 'allow-popups'],
    //     frameSrc: [global.config.signdoc.host],
    //     objectSrc: ["'self'", "data:", "blob:"]
    //   },

    //   // Set to true if you only want browsers to report errors, not block them
    //   reportOnly: false,

    //   // Set to true if you want to blindly set all headers: Content-Security-Policy,
    //   // X-WebKit-CSP, and X-Content-Security-Policy.
    //   setAllHeaders: false,

    //   // Set to true if you want to disable CSP on Android where it can be buggy.
    //   disableAndroid: true,

    //   // Set to false if you want to completely disable any user-agent sniffing.
    //   // This may make the headers less compatible but it will be much faster.
    //   // This defaults to `true`.
    //   browserSniff: true
    // }))

    // comment to enable Cache for static file, noCache will be set after load static files
    // app.use(helmet.noCache());
    //Passport initialization

    app.use(function(req, res, next) {
      function afterResponseFinish() {
        try {
          res.removeListener('finish', afterResponseFinish);
          res.removeListener('close', afterResponseClose);
          // action after response.
          var t0 = req.headers['eab-t0'];
          var tmp = t0.split(',');
          var t1 = process.hrtime([Number(tmp[0]),Number(tmp[1])]);
          var token = req.headers['eab-token'];
          var action = '';
          if (req.body && req.body.action)
            action = ' >> action=' + req.body.action;
          logger.resp('FINISH: ' + token + action + ' | elapsed ' + getMS(t1) + 'ms');
        } catch(err) {
          logger.error('FINISH: ' + err.toString());
        }
      }

      function afterResponseClose() {
        try {
          res.removeListener('finish', afterResponseFinish);
          res.removeListener('close', afterResponseClose);
          // action after response.
          var t0 = req.headers['eab-t0'];
          var tmp = t0.split(',');
          var t1 = process.hrtime([Number(tmp[0]),Number(tmp[1])]);
          var token = req.headers['eab-token'];
          var action = '';
          if (req.body && req.body.action)
            action = ' >> action=' + req.body.action;
          logger.resp('CLOSED: ' + token + action + ' | elapsed ' + getMS(t1) + 'ms | connection was terminated before response.end');
        } catch(err) {
          logger.error('CLOSED: ' + err.toString());
        }
      }

      var token = SecureRandom(16);
      try {
        res.on('finish', afterResponseFinish);
        res.on('close', afterResponseClose);
        // action before request
        req.headers['eab-token'] = token;
        res.setHeader('eab-token', token);
        req.headers['eab-t0'] = process.hrtime().toString();
        logger.req('REQUEST: ' + token + ' | ' + req.method + ' | ' + req.path + ' | ' + clientIP(req) + ' | ' + req.sessionID + ' | ' + precisionRound(req.socket.bytesRead / 1024, 2) + ' KB');
      } catch(err) {
        logger.error(err.toString());
      }
      next();
    });

    if (passport) {
      app.use(passport.initialize());
      app.use(passport.session());

      app.post(global.config.saml.path,
        passport.authenticate(samlconfig.passport.strategy,
        {
          successRedirect: './',
          failureRedirect: global.config.host.saml,  //SiteMinder URL
          failureFlash: true
        }),
        function (req, res) {
          logger.log("[SAML] 0 user=", req.user);
          // req.redirect('./');
        }
      );
    }

    app.get(ser.getFavicon());

    app.use((req, resp, next) => {
      resp.setHeader('X-Frame-Options', 'SAMEORIGIN');
      resp.setHeader('X-Content-Type-Options', 'nosniff');
      resp.setHeader('Strict-Transport-Security', "max-age=31536000; includeSubDomains");
      resp.setHeader('X-XSS-Protection', "1; mode=block");
      // resp.setHeader('X-FRAME-OPTIONS', 'DENY');
      // resp.setHeader('Content-Security-Policy', "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';");
      resp.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
      resp.setHeader('Pragma', 'no-cache');
      resp.setHeader('Expires', 0);
      next();
    })

    // handle getting static file
    app.use('/', function(req, resp, next) {
      // disable to use IF-NONE-MATCH
      // resp.setHeader('Cache-Control', 'max-age=86400');

      if (req.url == '/') {
        if (req.user) {
          req.session.samlUser = req.user;
          logger.log("[SAML] 1 user=", req.sessionID, req.path, req.user);
        }

        logger.log("INFO: getting index");
        var url = ""
        if (global.MODE == MODE_TYPE.PROD) {
          var hosts = "";
          if (typeof global.config.host.path == "string") {
            hosts = global.config.host.path;
          } else {
            for(var i in global.config.host.path) {
              hosts += (hosts?",":"") + global.config.host.path[i];
            }
          }
          logger.log("hosts:", global.config.host.path, hosts);
          resp.setHeader("Access-Control-Allow-Origin", hosts);
          // Request headers you wish to allow
          resp.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          // Set to true if you need the website to include cookies in the requests sent
          resp.setHeader('Access-Control-Allow-Credentials', true);
          url = path.join(__dirname, 'prod', "index.html")
        } else {
          url = path.join(__dirname, 'dist', "index.html")
        }
        resp.sendFile(url);
      } else if (req.url.indexOf('.')>0 && req.url.indexOf('getSignDocResult') < 0) {
        logger.log("INFO: getting file:", req.url);

        // As per AXA IT request, making static file for disable no-cache
        resp.setHeader('Cache-Control', 'max-age=2592000');
        resp.removeHeader('Pragma');
        resp.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());

        if (global.MODE == MODE_TYPE.PROD) {
          url = path.join(__dirname, 'prod' + req.url.replace('/prod', ''))
          // if (req.url.indexOf('worker.js') > 0) {
          //   resp.set('Content-Type', 'text/javascript')
          // } else
          if (req.url.indexOf('.js')>0) {
            url = url + '.gz';
            resp.set('Content-Encoding', 'gzip');
            resp.set('Content-Type', 'text/javascript')
          }
        } else {
          url = path.join(__dirname, 'dist' + req.url.replace('/dist', ''));
        }
        resp.sendFile(url);
      } else {
        next();
      }
    });

    app.get('/static/:page', function(req, res, next) {
      logger.log('INFO: Receive GET static page for :', req.params.page);
      var page = req.params.page;
      if (page && ser[page]) {
        ser[page](req, res)
      }
    })

    app.get('/gba/:token', function(req, res, next){
      res.setHeader('Cache-Control', 'max-age=2592000');   //override No-Cache if material from DB. 30 Days = 2,592,000 Seconds
      res.removeHeader('Pragma');
      res.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());
      ser.getBinaryAttachment(req, res, next);
    });

    app.use(helmet.noCache());

    var io = require('socket.io')(server, {
      cookie: false
    });
    io.on('connection', function(socket) {
      logger.log('INFO: Socket connection!!!', !!socket);
      socket.on('message', function(data) {
        // logger.log('on message:', !!data);
        if (data.path && data.action) {
          sessionStore.get(data.sid, function(err, sess) {
            if (err || !sess) {
              // logger.log('get session err:', err);
              socket.emit('message', {success: false});
            } else {
              // logger.log("get session:", sess, err);
              ser.handleSocketMsg(data.path.replace('/',''), data, sess, socket);
            }
          })
        }
      });
    });

    app.get('/goApproval', function(req, res, next) {
      ser.initApprovalPage(req, res);
      next();
    });

    app.get('/logout', function(req, res) {
      ser.logout(req, res);
    });

    app.get('/getSignDocResult', function(req, res, next) {
      ser.getSignDocResult(req, res, next);
    })

    app.get('/gp/:token', (req, res, next) => {
      ser.getAttachmentByToken(req, res, next);
    });

    app.get('/getAttachmentPdf/:token', function(req, res, next){
      ser.getAttachmentPdfByToken(req, res, next);
    });

    // app.use(csurf({ cookie: true }));    // for csrf checking

    // app.use(function (err, req, res, next) {
    //   if (err.code !== 'EBADCSRFTOKEN') return next(err)
    //   // handle CSRF token errors here
    //   res.status(403)
    //   res.send('Invalid Request! Please reload the page and login agin.');
    // })

    app.get('/init', function(req, res) {
      ser.initApp(req, res);
    });

    app.post('/:path', function(req, resp, next) {
      let _host = req.headers.host;
      logger.log('INFO: Receive POST request for :', req.sessionID, req.params.path);
      // get the instance of session store
      if (!sessionStore && req.sessionStore) {
        sessionStore = req.sessionStore;
      }
      try {
        ser.handleRequest(req, resp, next);
      } catch (err) {

        let expct = 'Handle Request Exception:'+ (!!req && !!req.params && req.params.path) + " - "
        + (!!req && req.params && req.body.action) + " != ser: " + !!ser + "\n";

        logger.error(expct, err.stack || err);

        try {
          resp.status(500).sent("Unexpected exception.").end()
        } catch (e) {
          // ignore case response already end.
        }
      }
    });

    app.use((err, req, res, next) => {
      var reqtoken = req.headers['eab-token'];
      var errmsg = 'Internal Server Error.  Please contact administrator.';
      var unexperrmsg = 'Unexcepted Server Error.  Please contact administrator.';

      try {
        logger.error('Global Error: token('+reqtoken+') >> ' + err.stack);
        
        if (req.xhr) {
          res.status(500).send({ error: errmsg, errtoken: reqtoken });
        } else {
          res.status(500);
          res.render('error', { error: errmsg, errtoken: reqtoken });
        }
      } catch(err) {
        res.status(500).send({ error: unexperrmsg, errtoken: reqtoken });
      }
    });

    server.listen( PORT, function() {
      logger.log('HTTP Listening at port:', PORT);
    });
  });

  var stream = require('stream');

  global.uploadBase64 = function(options, data, cb) {
    var resp = '';
    var req = http.request(options, (res) => {
      res.on('data', (chunk) => {
        resp += chunk;
      });
      res.on('end', () => {
        if (typeof cb == 'function') {
          if (resp) {
            try {
              logger.log("uploadComplete!");
              resp = JSON.parse(resp)
              cb(resp);
            } catch (e) {
              logger.error("uploadBase64 exception:", resp);
              cb(false);
            }
          } else {
            logger.log('ERROR: uploadAttachmentByBase64:', resp);
            cb(false);
          }
        }
      })
    })
    var bufferStream = new stream.PassThrough();
    bufferStream.end(Buffer.from(data, 'base64'))
    // bufferStream.end(new Buffer(data, "base64"));
    bufferStream.pipe(req);
  };

  function SecureRandom(size) {
    let uuidv4 = require('uuid/v4');
    let min = 1;
    let max = 33;
    let random = '';
    let output = '';
  
    while (output.length < size){
      random = uuidv4();
      for (var i=0; i<5; i++) {
          var pos = Math.random() * (max - min) + min;
          var tmpChar = random.substr(pos, 1);
          tmpChar = tmpChar.toUpperCase();
          random = random.substr(0, pos-1) + tmpChar + random.substr(pos+1);
      }
      output += random.replace(/[^a-zA-Z0-9]+/g, '');
    }
    return output.substr(0, size);
  }

  function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  }

  function getMS(hrtime) {
    return precisionRound(Number((hrtime[0] * 1e9 + hrtime[1]) * Math.pow(10, -6), 8), 3);
  }

  function clientIP(req) {
    return (req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress).split(',')[0];   
  }

});

process.on('uncaughtException', function (err) {
  try {
    console.warn("[Danger] Uncaught Exception is happened!!!");
    console.error(err.stack);
  } catch(e) {
    console.error(e);
  }
});