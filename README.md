# 121 Web

Powered by EAB Systems, All right reserved 2016.

For development
// build client side code
> npm run build-web

// start up the development server without hot deploy
> npm run server

// start up the development server with hot deploy
> npm run dev

For production with uglify node and client side code
// build code
> npm run build

// start up the server
> npm run pro
