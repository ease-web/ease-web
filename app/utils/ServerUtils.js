const io = require('socket.io-client');
const CryptoJS = require("crypto-js");

import { TimeoutSecond } from '../constants/ConfigConstants';
import { logout, showLoading, hideLoading, showErrorMsg, LOGOUT } from '../actions/GlobalActions';

import {
  resetSession
} from '../actions/sessions';

import {
  getStrSignature
} from '../../common/CommonUtils'
import { setTimeout } from 'timers';

window.callServer = function(context, actionPath, data, callback, keepLoading) {

  let {
    store
  } = context;

  showLoading(context);

  let {
    app
  } = context.store.getState()

  let {
    yekasrs,
    yekasr,
    csrfToken,
    langMap
  } = app

  setTimeout(function() {

    showLoading(context);

    let xhttp = new XMLHttpRequest();
    //set timeout
    xhttp.timeout = 120 * 1000;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4) { // done
        if (xhttp.status == 200 && xhttp.responseText) {
          let {
            app
          } = context.store.getState()

          let res = null
          if (yekasr && typeof xhttp.responseText == 'string') {
            res = yekasr.decrypt(xhttp.responseText, 'json');
          } else {
            res = JSON.parse(xhttp.responseText);
          }

          let validRequest = true
          if (app.skey && res) {
            if (!res.p0 || !res.p1 || getStrSignature(res.p1, app.skey) != res.p0) {
              validRequest = false;
            } else {
              res = res.p1;
            }
          } else {
            if (!res || res.p0 || res.p1) {
              validRequest = false;
            }
          }

          if (validRequest) {
            // store.getState().app.csrfToken = res.csrfToken;
            resetSession(context,false);
            if (callback && typeof callback == 'function') {
              callback(res);
            }
          } else {
            showErrorMsg(context, {
              errorMsg: getLocalizedText(langMap, res.error || "error.invaid_request", "Invalid action, Please login again."),
              action: function() {
                logout(context, true);
              }
            });
            if (res.logout) {
              setTimeout(()=>{
                logout(context, true);
              }, 10000)
            }
          }

          if (!res.keepLoading && !keepLoading) {
            hideLoading(context);
          }
        } else {
          let errorMsg;
          var onlineProcID = null;
          try {
            if (xhttp.body) {
              var eabToken = xhttp.getResponseHeader('eab-token');
              if (eabToken)
                onlineProcID = '\n[online process ' + eabToken + ']';
            }
          } catch(err) {
            logger.error(err);
          }
          switch (xhttp.status) {
            case 504:
              errorMsg = 'Connection timeout (504), please try it later.';
              break;
            default:
              errorMsg = 'Connection error (' + xhttp.status + '), please try it later.';
          }
          if (data && data.action) {
            errorMsg += '\n(path = ' + actionPath + ', action = ' + data.action + ')';
          }
          try {
            if (onlineProcID)
              errorMsg += onlineProcID;
          } catch(err) {
            logger.error(err);
          }

          hideLoading(context);
          showErrorMsg(context, errorMsg);
        }
      }
    };

    xhttp.onerror = function(e){
      hideLoading(context);
    }

    xhttp.ontimeout = function (e) {
      hideLoading(context);
    };

    if (!data) {
      data = "";
    }

    xhttp.open("POST", '.' + actionPath, true);

    xhttp.setRequestHeader("Content-type", "application/json");
    // xhttp.setRequestHeader("X-XSRF-TOKEN", csrfToken) // for csrf checking

    if (yekasrs) {
      // encode with public key of server
//      data = yekasrs.encrypt(data, 'base64');
//      xhttp.send('{"d0":"'+data+'"}');
      // modify request data encryption approach
      // assume [data] must be JSON object
      // (1) using AES to encrypt [data]
      // (2) using RSA to encrypt [AES key]
      // (3) [AES key] will be put into JSON object [dk]
      var aeskey = window.aesGen256Key();
      var aesdata = window.aesEncrypt(JSON.stringify(data), aeskey);
      aeskey = yekasrs.encrypt(aeskey, 'base64');
      xhttp.send('{"dk":"'+aeskey+'","d0":"'+aesdata+'"}');
    } else {
      if (typeof(data) == 'object') {
        data = JSON.stringify(data);
      }
      xhttp.send(data);
    }
  }, 100);
};

window.connectSocket = function(actionPath, store, router, data, callback) {
  var parsed = parseuri(window.location.href);
  var url = parsed.protocol + "://";
  url = url + parsed.host;
  if (parsed.port)
	  url = url + ":" + parsed.port;
  url = url + parsed.path;
  var socket = io.connect(url, {
    forceNew: true,
    reconnection: false,
    upgrade: true,
    transports: ['websocket','polling'],
    secure:true
  });
  data.sid = store.getState().app.sid;
  data.path = actionPath;
  socket.on('connect', function() {
    socket.emit('message', data);
  });

  socket.on('message', function(respData) {
    if (respData) {
      callback(respData);
    } else {
      callback({success: false});
    }
    socket.close()
  })

  socket.on('connect_error', function(result) {
    callback({success: false, error: result})
    socket.close();
  });
  socket.on('connect_timeout', function() {
    callback({success: false})
    socket.close();
  });
};

window.genFilePath = function(app, docId, attId, filename) {
  let {
    version,
    build
  } = app

  // var iterations = 1000;
  // var keySize = 64;
  // var ivSize = 32;
  // var salt = CryptoJS.enc.Utf8.parse(build);
  // var output = CryptoJS.PBKDF2(version, salt, {
  //     keySize: (keySize+ivSize)/32,
  //     iterations: iterations
  // });
  // output.clamp();
  // var key = CryptoJS.lib.WordArray.create(output.words.slice(0, keySize/32));
  // var iv = CryptoJS.lib.WordArray.create(output.words.slice(keySize/32));

  var hashStr = CryptoJS.SHA256(version + build);
  hashStr = hashStr.toString(CryptoJS.enc.Hex);
  var key = hashStr.substr(5, 32);
  var iv = hashStr.substr(41, 16);

  let params = [docId, attId];
  if (filename) {
    params.push(filename);
  }
  
  // return window.location.origin + '/gba/'
  // + encodeURIComponent(CryptoJS.AES.encrypt(params.join(','), key, {iv: iv}));

  // As per AXA IT request, making encrypted value static for disable no-cache
  var crypto = require('crypto');
  var cipher = crypto.createCipher('aes256', hashStr);
  var crypted = cipher.update(params.join(','), 'utf8', 'hex');
  crypted += cipher.final('hex');
  var cbPath = encodeURIComponent(crypted);

  var dummy = CryptoJS.HmacSHA1(version + build, "EABSystems");   // Dummy parameter for cache control on URL
  return window.location.origin + '/gba/' + cbPath + '?' + dummy;
};

window.genAttPath = (token) => {
  return window.location.origin + '/gp/' + token;
};

// prase object to para string
window.convertObj2ParaStr = function(obj) {
  let para = '';
  for (let key in obj) {
    if (obj[key]) {
      para += (para ? '&' : '') + (key + '=' + obj[key]);
    }
  }
  return para;
};


window.setCookie = function(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

window.getCookie = function(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//====================== RSA (Start) ======================
window.SecureRandom = function(size) {
  let uuidv4 = require('uuid/v4');
  let min = 1;
  let max = 33;
  let random = '';
  let output = '';

  while (output.length < size){
    random = uuidv4();
    for (var i=0; i<5; i++) {
        var pos = Math.random() * (max - min) + min;
        var tmpChar = random.substr(pos, 1);
        tmpChar = tmpChar.toUpperCase();
        random = random.substr(0, pos-1) + tmpChar + random.substr(pos+1);
    }
    output += random.replace(/[^a-zA-Z0-9]+/g, '');
  }
  return output.substr(0, size);
};

window.aesGen256Key = function() {
  return window.SecureRandom(32);     // 1 char = 8 bits
}

window.aesEncrypt = function(data, key) {
  let ciphertext = null;

  try {
    if (data != null && key != null) {
      ciphertext = CryptoJS.AES.encrypt(data, key);
    } else {
      throw Error('empty data or key');
    }
  } catch (ex) {
    throw ex;
  }
  return ciphertext.toString();
}

window.aesDecrypt = function(ciphertext, key) {
  let decrypted = null;

  try {
    decrypted = CryptoJS.AES.decrypt(ciphertext, key);
  } catch (ex) {
    throw ex;
  }
  return decrypted.toString(CryptoJS.enc.Utf8);
}
//====================== RSA (End) ======================
