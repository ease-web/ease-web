import {ADD_ATTACHMENT, REMOVE_ATTACHMENT, INIT_ATTACHMENTS} from '../actions/attachments';
import {SAVE_PROFILE} from '../actions/client';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = function() {
  return {
    items: {}
  }
  // completed: false
}

export default function attachments(state = getInitState(), action) {
  switch (action.type) {
    case ADD_ATTACHMENT:
        var {id, type, value} = action.item;
        state.items[id]={type, value};
        return state;
    
    case REMOVE_ATTACHMENT:
        var {id} = action.item;
        state.items[id] = undefined;
        return state;
    case SAVE_PROFILE:
    case INIT_ATTACHMENTS:
    case LOGOUT:
        return getInitState(); 
    default:
        return state;
  }
}
