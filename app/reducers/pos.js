import { POS_CHANGE_PAGE, SHOW_APPLICATION, POS_EAPP, POS_PROPOSAL, POS_QUOTATION } from '../actions/application';
import { POS_SHIELD_EAPP, INIT_SHIELD_APPLICATION, SHOW_SHIELD_APPLICATION, SHOW_SHIELD_SIGNATURE, SHOW_SHIELD_PAYMENT, SHOW_SHIELD_SUBMISSION } from '../actions/shieldApplication';
import { NEW_QUOTATION, CLOSE_QUOTATION, REQUOTE } from '../actions/quotation';
import { OPEN_PROPOSAL, CLOSE_PROPOSAL } from '../actions/proposal';
import { INIT_CLIENT_CHOICE, POS_CLIENT_CHOICE } from '../actions/clientChoice';
import { OPEN_FE, POS_FE, CLOSE_FE, UPDATE_FNA, UPDATE_FE, UPDATE_PDA, INIT_NEEDS } from '../actions/needs';
import { SAVE_PROFILE, UPDATE_PROFILE, UPDATE_DEPENDANTS_PROFILE } from '../actions/client';
import { INIT_SUPPORT_DOCUMENTS , CLOSE_SUPPORT_DOCUMENTS} from '../actions/supportDocuments';
import { INIT_SUPPORT_DOCUMENTS_SHIELD} from '../actions/shieldSupportDocuments';
import { LOGOUT } from '../actions/GlobalActions';

var getInitState = function() {
    return {
        currentPage: null,
        openSuppDocsPage: false,
        showFnaInvalidFlag: false
    }
}

export default function pos(state = getInitState(), action) {
    switch (action.type) {
        case POS_CHANGE_PAGE:
            return Object.assign({}, state, {
                currentPage: action.page
            });
        case INIT_SHIELD_APPLICATION:
        case SHOW_SHIELD_APPLICATION:
        case SHOW_SHIELD_SIGNATURE:
        case SHOW_SHIELD_PAYMENT:
        case SHOW_SHIELD_SUBMISSION:
            return Object.assign({}, state, {
              currentPage: POS_SHIELD_EAPP
            });
        case SHOW_APPLICATION:
            return Object.assign({}, state, {
              currentPage: POS_EAPP
            });
        case NEW_QUOTATION:
        case REQUOTE:
          return Object.assign({}, state, {
            currentPage: POS_QUOTATION
          });
        case CLOSE_QUOTATION:
        case CLOSE_PROPOSAL:
        case CLOSE_FE:
          return Object.assign({}, state, {
            currentPage: 'index'
          });
        case OPEN_PROPOSAL:
          return Object.assign({}, state, {
            currentPage: POS_PROPOSAL
          });
        case INIT_CLIENT_CHOICE:
          return Object.assign({}, state, {
            currentPage: POS_CLIENT_CHOICE
          });
        case INIT_SUPPORT_DOCUMENTS:
          return Object.assign({}, state, {
            // currentPage: POS_SUPPORT_DOCUMENTS
            openSuppDocsPage: true,
            isShield: false
          });
        case INIT_SUPPORT_DOCUMENTS_SHIELD:
          return Object.assign({}, state, {
            openSuppDocsPage: true,
            isShield: true
          });
        case CLOSE_SUPPORT_DOCUMENTS:
          return Object.assign({}, state, {
            openSuppDocsPage: false
          });
         case OPEN_FE:
          return Object.assign({}, state, {
            currentPage: POS_FE
          });
        case LOGOUT:
          return getInitState();  // reset the state
        case SAVE_PROFILE:
        case UPDATE_PROFILE:
        case UPDATE_FNA:
        case UPDATE_FE:
        case UPDATE_PDA:
        case INIT_NEEDS:
          return Object.assign({}, state, {
            showFnaInvalidFlag: action.showFnaInvalidFlag
          })
        default:
            return state;
    }
}
