import {OPEN_BROCHURE, CLOSE_BROCHURE} from '../actions/brochure';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    open: false,
    data: null,
    product: null
  };
};

export default function brochure(state = getInitState(), action) {
  switch (action.type) {
    case OPEN_BROCHURE:
      return Object.assign({}, state, {
        open: true,
        data: action.data,
        product: action.product
      });
    case CLOSE_BROCHURE:
      return Object.assign({}, state, {
        open: false,
        data: null,
        product: null
      });
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
