import {
  SESSION_ALERT_OPEN,
  SESSION_ALERT_CLOSE,
  UPDATE_SESSION_TIMER,
  RESET_SESSION_TIMER,
  REDUCE_SESSION_TIME,
  SESSION_TIME_OUT
} from '../actions/sessions';

import {
  LOGOUT
} from '../actions/GlobalActions';

import {
  SessionExpiry_Min
} from '../constants/ConfigConstants.js';

var getInitState = function() {
  return {
    open: false,
    timestamp: 0,
    countdown: SessionExpiry_Min
  }
};

export default function sessions(state = getInitState(), action) {
  var now = (new Date()).getTime();
  switch (action.type) {
    case RESET_SESSION_TIMER:
      return Object.assign({}, state, {
        open: false,
        timestamp: now,
        countdown: SessionExpiry_Min
      });

    case SESSION_ALERT_OPEN:
      var diffFromLastSessionTime_Min = (now - state.timestamp) / 60000;
      var newCountDown = Math.floor(SessionExpiry_Min - diffFromLastSessionTime_Min);

      return Object.assign({}, state, {
        open: true,
        countdown: newCountDown
      });

    case SESSION_ALERT_CLOSE:
      return Object.assign({}, state, {
        open: false
      });
    case LOGOUT:
    case SESSION_TIME_OUT:
      window.loggedIn = false;
      return getInitState()
    default:
      return state;
  }
}
