import * as _ from 'lodash';

import {
  INIT_SHIELD_APPLICATION,
  SHOW_SHIELD_APPLICATION,
  VALIDATE_SHIELD_APPLICATION,
  SAVE_SHIELD_APPLICATION,
  SHOW_SHIELD_SIGNATURE,
  UPDATE_SHIELD_SIGNATURE_URL,
  SHOW_SHIELD_PAYMENT,
  SAVE_SHIELD_PAYMENT,
  SAVE_SHIELD_SUBMISSION,
  SHOW_SHIELD_SUBMISSION,
  DO_SHIELD_SUBMISSION,
  HIDE_SHIELD_SIGNATURE_EXPIRY_WARNING,
  HIDE_SHIELD_WARNING,
  SHOW_SHIELD_ERROR,
  HIDE_SHIELD_ERROR
} from '../actions/shieldApplication';
import {
  DO_SHIELD_PAYMENT,
  RELOAD_SHIELD_PAYMENT
} from '../actions/payment';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments';
import { POS_CHANGE_PAGE } from '../actions/application';
import { LOGOUT } from '../actions/GlobalActions';
import SystemConstants from '../constants/SystemConstants';

const getInitState = () => {
  return {
    stepper: {
      sections: {
        items: []
      },
      index: {
        current: 0,
        completed: -1,
        active: 0
      },
      enableNextStep: false
    },
    template: {},
    application: {},
    signature: {
      isFaChannel: false,
      signingTabIdx: -1,
      isSigningProcess:[],
      numOfTabs: 0,
      pdfStr:[],
      attUrls:[],
      isSigned: [],
      agentSignFields:[],
      clientSignFields:[],
      signDocConfig: {
        auths:[],
        docts:[],
        ids:[],
        postUrl:'',
        resultUrl:'',
        dmsId:''
      }
    },
    warningMsg: {
      showDialog: false,
      showSignExpiryWarning: false,
      msgCode: 0
    },
    errorMsg: {
      showDialog: false,
      msg: ''
    }
  };
};

export default function shieldApplication(state = getInitState(), action) {
  switch (action.type) {
    case DO_SHIELD_PAYMENT:
      return Object.assign({}, state, {
        application: action.application
      });
    case RELOAD_SHIELD_PAYMENT:
      return Object.assign({}, state, {
        application: action.application
      });
    case INIT_SHIELD_APPLICATION:
      return Object.assign({}, state, {
        stepper: action.stepper,
        template: action.template,
        application: action.application
      });
    case SHOW_SHIELD_APPLICATION:
      return Object.assign({}, state, {
        stepper: action.stepper,
        template: action.template,
        application: action.application,
        warningMsg: Object.assign({},
          _.get(action, 'warningMsg', state.warningMsg),
          {showDialog: _.get(action, 'warningMsg', state.warningMsg).msgCode > 0})
      });
    case SHOW_SHIELD_SIGNATURE:
      return Object.assign({}, state, {
        stepper: action.stepper,
        signature: action.signature,
        template: {},
        application: action.application,
        warningMsg: Object.assign({}, action.warningMsg, {showDialog: action.warningMsg.msgCode > 0})
      });
    case SHOW_SHIELD_PAYMENT:
      return Object.assign({}, state, {
        stepper: action.stepper,
        template: action.template,
        application: action.application,
        warningMsg: Object.assign({},
          _.get(action, 'warningMsg', state.warningMsg),
          {showDialog: _.get(action, 'warningMsg', state.warningMsg).msgCode > 0})
      });
    case SHOW_SHIELD_SUBMISSION:
      return Object.assign({}, state, {
        stepper: action.stepper,
        template: action.template,
        application: action.application,
        warningMsg: Object.assign({},
          _.get(action, 'warningMsg', state.warningMsg),
          {showDialog: _.get(action, 'warningMsg', state.warningMsg).msgCode > 0})
      });
    case VALIDATE_SHIELD_APPLICATION:
      return Object.assign({}, state, {
        stepper: action.stepper
      });
    case SAVE_SHIELD_SUBMISSION:
    case SAVE_SHIELD_APPLICATION:
    case SAVE_SHIELD_PAYMENT:
      return Object.assign({}, state,
        {application: action.application},
        action.errorMsg ? {errorMsg: action.errorMsg} : {}
      );
    case UPDATE_SHIELD_SIGNATURE_URL:
      let pdfStr = _.cloneDeep(state.signature.pdfStr);
      let attUrls = _.cloneDeep(state.signature.attUrls);
      _.forEach(action.newAttUrls, (newAtt) => {
        let { index, attUrl } = newAtt;
        attUrls[index] = attUrl;
        pdfStr[index] = '';
      });
      return Object.assign({}, state, {
        signature: Object.assign({}, state.signature, {isSigned: action.isSigned}, {isSigningProcess:action.isSigningProcess}, {pdfStr}, {attUrls}, {signingTabIdx: action.nextTabIdx}),
      });
    case DO_SHIELD_SUBMISSION:
      return Object.assign({}, state, {
        stepper: action.stepper,
        template: action.template,
        application: action.application
      });
    case CLOSE_SUPPORT_DOCUMENTS:
      let newApplication = _.cloneDeep(state.application);
      if (_.isBoolean(action.isMandDocsAllUploaded)) {
        newApplication.isMandDocsAllUploaded = action.isMandDocsAllUploaded;
      }
      return Object.assign({}, state, {
        application: newApplication
      });
    case HIDE_SHIELD_SIGNATURE_EXPIRY_WARNING:
      return Object.assign({}, state, {
        warningMsg: Object.assign({}, state.warningMsg, {
          showSignExpiryWarning: false
        })
      });
    case HIDE_SHIELD_WARNING:
      return Object.assign({}, state, {
        warningMsg: Object.assign({}, state.warningMsg, {
          showDialog: false,
          msgCode: 0
        })
      });
    case SHOW_SHIELD_ERROR:
      return Object.assign({}, state, {
        errorMsg: {
          showDialog: true,
          msg: action.msg
        }
      });
    case HIDE_SHIELD_ERROR:
      return Object.assign({}, state, {
        errorMsg: {
          showDialog: false,
          msg: ''
        }
      });
    case POS_CHANGE_PAGE:
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
