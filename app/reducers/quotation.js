import * as _ from 'lodash';

import {
  NEW_QUOTATION,
  CLOSE_QUOTATION,
  QUOTATION_RELOAD,
  REQUOTE,
  LOAD_AVAILABLE_FUNDS,
  ALLOC_FUNDS
} from '../actions/quotation';
import { QUOTATION_UPDATE_CLIENTS } from '../actions/shieldQuotation';
import { SHOW_APPLICATION } from '../actions/application';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    quotation: null,
    planDetails: null,
    inputConfigs: null,
    quotationErrors: null,
    quotWarnings: null, // show in the middle as a warning, no logic
    availableFunds: null,
    availableInsureds: null
  };
};

export default function quotation(state = getInitState(), action) {
  switch (action.type) {
    case SHOW_APPLICATION:
      return Object.assign({}, state, {
        quotation: action.quotation
      });
    case NEW_QUOTATION:
      return Object.assign({}, state, {
        quotation: action.quotation,
        planDetails: action.planDetails,
        inputConfigs: action.inputConfigs,
        quotWarnings: action.quotWarnings,
        quotationErrors: action.quotationErrors,
        availableInsureds: action.availableInsureds
      });
    case REQUOTE:
      return Object.assign({}, state, {
        quotation: action.quotation,
        planDetails: action.planDetails,
        inputConfigs: action.inputConfigs,
        quotWarnings: action.quotWarnings,
        availableInsureds: action.availableInsureds,
        quotationErrors: action.quotationErrors
      });
    case QUOTATION_RELOAD:
      return Object.assign({}, state, {
        quotation: action.quotation,
        quotWarnings: action.quotWarnings,
        inputConfigs: action.inputConfigs,
        quotationErrors: action.quotationErrors
      });
    case LOAD_AVAILABLE_FUNDS:
      return Object.assign({}, state, {
        availableFunds: action.availableFunds
      });
    case ALLOC_FUNDS:
      return Object.assign({}, state, {
        quotation: action.quotation,
        quotationErrors: action.quotationErrors
      });
    case QUOTATION_UPDATE_CLIENTS:
      return Object.assign({}, state, {
        availableInsureds: action.availableInsureds
      });
    case CLOSE_QUOTATION:
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
