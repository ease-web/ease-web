import { INIT_SUBMISSION_PAGE, SERVER_ERROR, CLOSE_SUBMISSION_PAGE } from '../actions/submission';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments'
import ConfigConstants from '../constants/ConfigConstants';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    template:{},
    values:{},
    isMandDocsAllUploaded: false, 
    isSubmitted: false
  }
}

export default function submission(state = getInitState(), action) {
  switch(action.type) {
    case INIT_SUBMISSION_PAGE:
      return Object.assign({}, state, {
        template: action.template,
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted
      });
    case CLOSE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        template: action.template,
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted
      });
    case SERVER_ERROR:
      return Object.assign({}, state, {
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted
      });
    case CLOSE_SUBMISSION_PAGE:
    case LOGOUT:
      return getInitState();
    default:
      return state;
  }
}


