export const QUOTATION_UPDATE_CLIENTS = 'QUOTATION_UPDATE_CLIENTS';

import * as _ from 'lodash';

import {QUOTATION_RELOAD} from './quotation';

export function updateClients(context, errorCallback) {
  const {store} = context;
  const {quotation} = store.getState().quotation;
  window.callServer(context, '/quot', {
    action: 'updateClients',
    quotation
  }, (resp) => {
    if (resp && resp.success) {
      if (resp.errorMsg) {
        errorCallback && errorCallback(resp.errorMsg);
      } else {
        store.dispatch({
          type: QUOTATION_UPDATE_CLIENTS,
          availableInsureds: resp.availableInsureds
        });
      }
    }
  });
}

export function selectInsured(context, cid, covClass) {
  const {store} = context;
  const {quotation} = store.getState().quotation;
  window.callServer(context, '/quot', {
    action: 'selectInsured',
    quotation,
    cid,
    covClass
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: QUOTATION_RELOAD,
        quotation: resp.quotation,
        inputConfigs: resp.inputConfigs,
        quotationErrors: resp.errors
      });
    }
  });
}

export function updateRiders(context, covCodes, cid, callback, errorCallback) {
  const {store} = context;
  const {quotation} = store.getState().quotation;
  window.callServer(context, '/quot', {
    action: 'updateShieldRiderList',
    quotation: quotation,
    newRiderList: covCodes,
    cid: cid
  }, (resp) => {
    if (resp && resp.success) {
      if (resp.errorMsg) {
        errorCallback && errorCallback(resp.errorMsg);
      } else {
        store.dispatch({
          type: QUOTATION_RELOAD,
          quotation: resp.quotation,
          inputConfigs: resp.inputConfigs,
          quotationErrors: resp.errors
        });
        callback && callback();
      }
    }
  });
}
