export const UPDATE_AGENT_INFO = 'UPDATE_AGENT_INFO';
export const READ_ALL_MESSAGE = 'READ_ALL_MESSAGE';

export function getAgent(context, id, callback) {
  window.callServer(context, '/agent', {
    action: 'getAgentProfile',
    id: id
  },function(resp) {
    if (!resp.error){
      callback(resp);
    }
  });
}

export function updateAgentProfilePic(context, type, data, callback) {
  window.callServer(context, '/agent', {
    action: 'updateProfilePic',
    type: type,
    data: data
  }, (resp) => {
    context.store.dispatch({
      type: UPDATE_AGENT_INFO,
      lastUpdateDate: new Date()
    });
    callback && callback();
  });
}

export function readAllMessages(context) {
  window.callServer(context, '/agent', {
    action: 'readAllNotifications'
  }, (resp) => {
    context.store.dispatch({
      type: READ_ALL_MESSAGE
    });
  });
}