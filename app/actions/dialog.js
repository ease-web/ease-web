export const CLOSE_ERROR_DIALOG = 'CLOSE_ERROR_DIALOG';
export const SERVER_ERROR = 'SERVER_ERROR';
export const CLOSE_MSG_DIALOG = 'CLOSE_MSG_DIALOG';
export const OPEN_MSG_DIALOG = 'OPEN_MSG_DIALOG';


export function closeErrorDialog(store) {
  store.dispatch({
    type: CLOSE_ERROR_DIALOG
  });
}

export function closeMsgDialog(store) {
  store.dispatch({
    type: CLOSE_MSG_DIALOG
  });
}
