export const OPEN_QUICK_QUOTES = 'OPEN_QUICK_QUOTES';
export const LOAD_QUICK_QUOTES = 'LOAD_QUICK_QUOTES';
export const CLOSE_QUICK_QUOTES = 'CLOSE_QUICK_QUOTES';

export function openHistory(context) {
  const {store} = context;
  store.dispatch({
    type: OPEN_QUICK_QUOTES
  });
  queryQuickQuotes(context);
}

export function queryQuickQuotes(context) {
  const {store} = context;
  let proposer = store.getState().client.profile;
  window.callServer(context, '/quot', {
    action: 'queryQuickQuotes',
    pCid: proposer.cid
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: LOAD_QUICK_QUOTES,
        quickQuotes: resp.quickQuotes
      });
    }
  });
}

export function deleteQuickQuotes(context, quotIds, callback) {
  const {store} = context;
  let proposer = store.getState().client.profile;
  window.callServer(context, '/quot', {
    action: 'deleteQuickQuotes',
    pCid: proposer.cid,
    quotIds: quotIds
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: LOAD_QUICK_QUOTES,
        quickQuotes: resp.quickQuotes
      });
      callback && callback();
    }
  });
}

export function closeHistory(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_QUICK_QUOTES
  });
}
