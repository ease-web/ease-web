export const INIT_SUBMISSION_PAGE = 'INIT_SUBMISSION_PAGE';
export const SERVER_ERROR = 'SERVER_ERROR';
export const CLOSE_SUBMISSION_PAGE = 'CLOSE_SUBMISSION_PAGE';

export function initSubmission(template, values, isMandDocsAllUploaded, isSubmitted) {
  return {
    type: INIT_SUBMISSION_PAGE,
    template,
    values,
    isMandDocsAllUploaded,
    isSubmitted
  };
}

export function handleError(values, isMandDocsAllUploaded, isSubmitted) {
  return {
    type: SERVER_ERROR,
    values,
    isMandDocsAllUploaded,
    isSubmitted
  };
}

export function initSubmitStore(context, docId) {
  let {
    store,
    // router
  } = context;
  let {app, client} = store.getState();

  window.callServer(context, '/application', {
    action: 'getSubmissionTemplateValues',
    docId: docId
  }, function(resp) {
    if (resp.success) {
      store.dispatch(initSubmission(resp.template, resp.values, resp.isMandDocsAllUploaded, resp.isSubmitted));
    } else {
      store.dispatch(handleError(resp.values, resp.isMandDocsAllUploaded, resp.isSubmitted));
    }
  });
}

export function doAppFormSubmission(context, docId, values) {
  let {store} = context;
  let {app, client, approval} = store.getState();

  window.callServer(
    context,
    '/application',{
      action: 'appFormSubmission',
      docId: docId,
      agentProfile: app.agentProfile,
      clientProfile: client.profile,
      values: values
    }, function(resp) {
      if (resp.success) {
        store.dispatch(initSubmission(resp.template, resp.values, resp.isMandDocsAllUploaded, resp.isSubmitted));
      } else {
        store.dispatch(handleError(resp.values, resp.isMandDocsAllUploaded, resp.isSubmitted));
      }
    }
  );

}

export function closeSubmissionPage(context) {
  let {
    store
  } = context;

  store.dispatch({
    type: CLOSE_SUBMISSION_PAGE
  });
}
