import * as _ from 'lodash';

import {formatDate} from '../../common/DateUtils';

export const OPEN_PROFILE = 'OPEN_PROFILE';
export const SAVE_PROFILE = 'SAVE_PROFILE';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_ALL_PROFILES = 'UPDATE_ALL_PROFILES';
export const UPDATE_CONTACT_LIST = 'UPDATE_CONTACT_LIST';
export const UPDATE_DEPENDANTS_PROFILE = 'UPDATE_DEPENDANTS_PROFILE';
export const CLEAR_PROFILE = 'CLEAR_PROFILE';
export const UPDATE_ADDRESS_BY_PC = 'POSTAL_CODE_UPDATED';

export function getProfile(context, docId, callback){
  let {
    store
    // , router
  } = context;
  window.callServer(
    context,
    '/client',
    {
      action: 'getProfile',
      docId: docId
    }, function(resp){
      if (resp && resp.success){
        if (resp.profile.cid){
          store.dispatch({
            type: UPDATE_PROFILE,
            profile: resp.profile,
            showFnaInvalidFlag: resp.showFnaInvalidFlag
          });
          callback(true);
        }
        else {
          callback(false);
        }
      }
    }
  );
}

export function getFamilyProfile(context, docId, callback){
  // let {store, router} = context;

  window.callServer(
    context,
    '/client',
    {
      action: 'getProfile',
      docId: docId
    }, function(resp){
      if (resp && resp.success){
        callback(resp.profile);
      }
    }
  );
}

export function deleteTrustIndividual(context, callback){
  let {store,
    // router
  } = context;
  let {profile} = store.getState().client;

  window.callServer(
    context,
    '/client',
    {
      action: 'deleteTrustIndividual',
      cid: profile.cid
    }, function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: SAVE_PROFILE,
          profile: resp.profile,
          showFnaInvalidFlag: resp.showFnaInvalidFlag
        });
        callback();
      }
    }
  );
}

export function saveTrustedIndividual(context, tiInfo, confirm, callback){
  let {store} = context;
  let {attachments, client} = store.getState();
  let {tiPhoto} = attachments.items;
  let {cid} = client.profile;
  window.callServer(
    context,
    '/client',
    {
      action: 'saveTrustedIndividual',
      cid,
      tiInfo,
      tiPhoto,
      confirm
    }, function(resp){
      if (resp && resp.success){
        if (resp.code){
          callback(resp.code);
        }
        else {
          store.dispatch({
            type: SAVE_PROFILE,
            profile: resp.profile,
            showFnaInvalidFlag: resp.showFnaInvalidFlag
          });
          callback();
        }
      }
    }
  );
}

export function saveFamilyMember(context, docId, profile, confirm, callback){
  let {store,
    // router
  } = context;
  let {attachments, client} = store.getState();
  let photo = attachments.items.photo;

  if (profile && profile.dob) {
    profile.dob = formatDate(new Date(profile.dob));
    let dob = new Date(profile.dob);
    profile.age = window.calcAge(dob);
  }

  let pCid = _.get(client, 'profile.cid');

  window.callServer(
    context,
    '/client',
    {
      action: 'saveFamilyMember',
      profile,
      photo,
      fid: docId,
      cid: pCid,
      confirm
    }, function(resp){
      if (resp && resp.success){
        //update dependant's Profile
        if (resp.code){
          callback(resp.code);
        } else {
          let dependantProfiles = _.cloneDeep(client.dependantProfiles);
          dependantProfiles[resp.fid] = resp.fProfile;
          let { invalid } = store.getState().appList;
          store.dispatch({
            type: SAVE_PROFILE,
            profile: resp.profile,
            dependantProfiles,
            invalid: handleInvalidObj(invalid, resp.invalid, resp.profile),
            showFnaInvalidFlag: resp.showFnaInvalidFlag
          });
          callback();
        }
      }
    }
  );
}

// TODO::: should not update app count here, should go along with apply on backend..
// deplicated
export function updateProfileAppCount(context, applicationCount, callback){
  let {store
    // , router
  } = context;
  let profile = _.cloneDeep(store.getState().client.profile);
  if (profile) {
    profile.applicationCount = applicationCount;
    saveProfile(context, profile, false, true, ()=>{
      updateContactList(context, false, callback);
    });
  } else {
    callback({success: false});
  }
}

function handleInvalidObj (invalid, invalidFlag = {}, client) {
  if (invalidFlag.fna) {
    invalid.fna = true;
  }
  if (invalidFlag.initApplications) {
    invalid.initApplications = true;
  }
  return invalid
}

export function saveProfile(context, profile, isNew, confirm, callback){
  let {store
    // , router
  } = context;
  let {attachments} = store.getState();
  let photo = attachments.items.photo;
  let tiPhoto = attachments.items.tiPhoto;

  if (profile && profile.dob) {
    profile.dob = formatDate(new Date(profile.dob));
    let dob = new Date(profile.dob);
    profile.age = window.calcAge(dob);
  }

  if (!profile.cid){
    isNew = true;
  }

  window.callServer(
    context,
    '/client',
    {
      action: isNew ? 'addProfile' : 'saveProfile',
      profile,
      photo,
      tiPhoto,
      confirm
    }, function(resp){
      if (resp && resp.success){
        if (isNew){
          profile.cid = resp.profile.cid;
        }
        if (resp.code){
          callback(resp.code);
        }
        else {
          let { invalid } = store.getState().appList;
          store.dispatch({
            type: SAVE_PROFILE,
            profile: resp.profile,
            invalid: handleInvalidObj(invalid, resp.invalid, resp.profile),
            showFnaInvalidFlag: resp.showFnaInvalidFlag
          });
          callback();
        }
      }
    }
  );
}

export function deleteProfile(context, profile, callback){
  let {
    store
    // , router
  } = context;

  window.callServer(
    context,
    '/client',
    {
      action: 'deleteProfile',
      cid: profile.cid
    }, function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_PROFILE,
          profile: {}
        });
        updateContactList(context);
        callback();
      }
    }
  );
}

export function updateContactList(context, isClear, callback){
  let {
    store
    // , router
  } = context;

  if (isClear){
    store.dispatch({
      type:CLEAR_PROFILE
    });
  }


  window.callServer(
    context,
    '/client',
    {
      action: 'getContactList'
    }, function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_CONTACT_LIST,
          contactList: resp.contactList
        });
        if (_.isFunction(callback)){
          callback();
        }
      }
    }
  );
}

export function unlinkRelationship(context, fid, callback){
  let {
    store
    // , router
  } = context;

  let {
    client
  } = store.getState();

  let pCid = _.get(client, 'profile.cid');

  window.callServer(
    context,
    '/client',
    {
      action: 'unlinkRelationship',
      cid: pCid,
      fid: fid
    }, function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_PROFILE,
          profile: resp.profile,
          showFnaInvalidFlag: resp.showFnaInvalidFlag
        });
        callback();
      }
    }
  );
}

export function getAllDependantsProfile(context, callback){
  let {store
    // , router
  } = context;
  let {profile} = store.getState().client;

  window.callServer(
    context,
    '/client',
    {
      action: 'getAllDependantsProfile',
      cid: profile.cid
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_DEPENDANTS_PROFILE,
          dependantProfiles: resp.dependantProfiles
        });
      }
    }
  );
}

export function updateStoreProfile(context, profileArr){
  let {
    store
  } = context;
  let {client: {profile, dependantProfiles}} = store.getState();

  let updatedfProfile = _.cloneDeep(profile) || {};
  let updatedDepProfiles = _.cloneDeep(dependantProfiles) || {};

  _.forEach(profileArr, p => {
    if (p && p.cid && updatedfProfile.cid && p.cid === updatedfProfile.cid){
      updatedfProfile = p;
    } else if (p && p.cid && updatedDepProfiles && !_.isEmpty(updatedDepProfiles) && updatedDepProfiles[p.cid]){
      updatedDepProfiles[p.cid] = p;
    }
  });
  store.dispatch({
    type: UPDATE_ALL_PROFILES,
    dependantProfiles: updatedDepProfiles,
    profile: updatedfProfile
  });
}

export function getAddressByPostalCode(context, postalCode, callback){
  // let {
  //   store
  // } = context;
  let fileCode;

  fileCode = getPostalCodeFileName(postalCode);

  if (fileCode){
	//postalCode = truncateLeadingZeroPC(postalCode);
    window.callServer(
      context,
      '/client',
      {
        action: 'getAddressByPostalCode',
        pC: postalCode,
        fileName: fileCode
      },
      function(resp){
        if (resp && resp.success){
          /**store.dispatch({
            type: UPDATE_ADDRESS_BY_PC,
            postalBldgNo: resp.BLDGNO || '',
            postalStName: resp.STREETNAME || '',
            postalBldgName: resp.BLDGNAME || '',
            postalCode: postalCode
          });*/
          callback({success: true, bldgNo: _.toString(resp.BLDGNO) || '', streetName: _.toString(resp.STREETNAME) || '', bldgName: _.toString(resp.BLDGNAME) || ''});
        } else {
          callback({success: false, bldgNo: '', streetName: '', bldgName: ''});
        }
      }
    );
  } else {
    /**store.dispatch({
      type: UPDATE_ADDRESS_BY_PC,
      postalBldgNo: '',
      postalStName: '',
      postalBldgName: '',
      postalCode: postalCode
    });*/
    callback({success: false, bldgNo: '', streetName: '', bldgName: ''});
  }
}

let getPostalCodeFileName = function(code){
  let covPostalCode = (code && code.length === 6) ? Number(_.toString(code).substr(0,2)) : Number('0' + _.toString(code).substr(0,1));
  let resultFilename;
  let mappingTable = {
    0: '_00_19',
    19: '_20_29',
    29: '_30_39',
    39: '_40_49',
    49: '_50_59',
    59: '_60_69',
    69: '_70_79',
    79: '_80_89'
  };
  if (!_.isNumber(covPostalCode) || code.length !== 6) {
    return undefined;
  }
  let keyNumber;
  _.forEach(Object.keys(mappingTable).sort(function(a, b) {return b - a;}), obj =>{
    keyNumber = Number(obj);
    if (covPostalCode > keyNumber && !resultFilename){
      resultFilename =  mappingTable[obj];
    }
  });
  return resultFilename;
};

let truncateLeadingZeroPC = function(code) {
  if (code.toString().substr(0,1) === '0' && code.length === 6){
    return Number(code.toString().substr(1,5));
  } else {
    return code;
  }
};
