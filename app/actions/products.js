export const UPDATE_PRODUCTS = 'UPDATE_PRODUCTS';
export const UPDATE_DEPENDANT_LIST = 'UPDATE_DEPENDANT_LIST';
export const CLEAR_PRODUCTS = 'CLEAR_PRODUCTS';
export const QUOT_CLOSE_CONFIRM = 'QUOT_CLOSE_CONFIRM';
export const QUOT_CLOSE_ERROR = 'QUOT_CLOSE_ERROR';

export function queryProducts(context, ccy, insuredCid, quickQuote, callback) {
  const {store} = context;
  let proposer = store.getState().client.profile;
  window.callServer(context, '/product', {
    action: 'queryProducts',
    insuredCid: insuredCid ? insuredCid : proposer.cid,
    proposerCid: insuredCid ? proposer.cid : null,
    quickQuote: quickQuote,
    params: {
      ccy: ccy
    }
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: UPDATE_PRODUCTS,
        prodCategories: resp.prodCategories,
        error: resp.error,
        ccy: ccy,
        insuredCid: insuredCid
      });
      callback && callback();
    }
  });
}

export function getDependantList(context, quickQuote, callback) {
  const {store} = context;
  let cid = store.getState().client.profile.cid;
  window.callServer(context, '/product', {
    action: 'getDependantList',
    cid: cid,
    quickQuote: quickQuote
  }, (resp) => {
    store.dispatch({
      type: UPDATE_DEPENDANT_LIST,
      dependants: resp.dependants
    });
    callback && callback();
  });
}

export function closeConfirm(context) {
  const {store} = context;
  store.dispatch({
    type: QUOT_CLOSE_CONFIRM
  });
}

export function closeError(context) {
  const {store} = context;
  store.dispatch({
    type: QUOT_CLOSE_ERROR
  });
}

export function closeProductPage(context) {
  const {store} = context;
  store.dispatch({
    type: CLEAR_PRODUCTS
  });
}
