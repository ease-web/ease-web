import * as _ from 'lodash';

export const ADD_ATTACHMENT = 'ADD_ATTACHMENT';
export const REMOVE_ATTACHMENT = 'REMOVE_ATTACHMENT';
export const INIT_ATTACHMENTS = 'INIT_ATTACHMENTS';

export function addAttachment(context, fileName, fileBase64, type, callback){
  let {store} = context;
  store.dispatch({
    type: ADD_ATTACHMENT,
    item: {
      id: fileName,
      type,
      value: window.trimBase64(fileBase64)
    }
  });

  if (_.isFunction(callback)) {
    callback();
  }
}

export function removeAttachment(context, fileName, callback){
  let {store} = context;
  store.dispatch({
      type: ADD_ATTACHMENT,
      item: fileName
  });

  if (_.isFunction(callback)) {
    callback();
  }
}

export const getAttachment = function(context, docId, attId, cb) {
  let {store} = context;
  window.callServer(
    context,
    '/file',
    {
      action: 'getAttachment',
      docId,
      attId
    },
    function(resp){
      if (resp && resp.success){
        cb(resp);
      }
    }
  );
};
