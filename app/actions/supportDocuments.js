export const INIT_SUPPORT_DOCUMENTS = 'INIT_SUPPORT_DOCUMENTS';
export const POS_SUPPORT_DOCUMENTS = 'POS_SUPPORT_DOCUMENTS';
export const SHOW_DRAG_DROP_ZONE = 'SHOW_DRAG_DROP_ZONE';
export const UPDATE_SUPPORT_DOCUMENTS = 'UPDATE_SUPPORT_DOCUMENTS';
export const EXCEED_MAX_FILES_SIZE = 'EXCEED_MAX_FILES_SIZE';
export const CLOSE_SUPPORT_DOCUMENTS = 'CLOSE_SUPPORT_DOCUMENTS';
export const UPDATE_VIEWED_LIST = 'UPDATE_VIEWED_LIST';
export const POS_MAIN = 'index';

const fileType = require('file-type');

import * as _ from 'lodash';

export function goSupportDocuments(context, appId, appStatus = 'APPLYING', userChannel = 'agent'){
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action:'getSupportDocuments',
      applicationId:appId,
      appStatus: appStatus
    }, function(resp){
      if (resp.success){
        store.dispatch({
          type: INIT_SUPPORT_DOCUMENTS,
          template:resp.template,
          values:resp.values,
          suppDocsAppView:resp.suppDocsAppView,
          appStatus: appStatus,
          viewedList: resp.viewedList,
          userChannel: userChannel,
          isReadOnly: resp.isReadOnly,
          defaultDocNameList: resp.defaultDocNameList,
          tokensMap: resp.tokensMap
        });
      }
    }
  );
}

export function viewedFile(context, applicationId, attachmentId, isSupervisorChannel, updateReviewList, callback) {
  window.callServer(
    context,
    '/application',
    {
      action: 'viewedSuppDocsFile',
      applicationId: applicationId,
      attachmentId: attachmentId,
      isSupervisorChannel: isSupervisorChannel,
      updateReviewList
    },
    (resp)=>{
      callback(resp);
    }
  );
}

export function upsertFile(context, res, applicationId, attachmentValues, valueLocation, tabId, suppDocsValues, viewedList, isSupervisorChannel, rootValues) {
  let {store} = context;

  window.callServer(
    context,
    '/application',
    {
      action: 'upsertSuppDocsFile',
      attachment: res.imageUrl,
      applicationId: applicationId,
      attachmentValues: attachmentValues,
      valueLocation:valueLocation,
      tabId: tabId,
      suppDocsValues: suppDocsValues,
      viewedList: viewedList,
      isSupervisorChannel: isSupervisorChannel,
      rootValues
    },
    (resp)=>{
      if (resp.success && !resp.showTotalFilesSizeLargeDialog){
        store.dispatch({
          type: UPDATE_SUPPORT_DOCUMENTS,
          values: resp.values,
          viewedList: resp.viewedList,
          pendingSubmitList: resp.pendingSubmitList,
          tokensMap: resp.tokensMap
        });
      } else if (resp.success && resp.showTotalFilesSizeLargeDialog) {
        store.dispatch({
          type: EXCEED_MAX_FILES_SIZE,
          showTotalFilesSizeLargeDialog: resp.showTotalFilesSizeLargeDialog
        });
      }
    }
  );
}

export function deleteFileAction(context, applicationId, attachmentId, valueLocation, tabId) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'deleteSupportDocumentFile',
      applicationId: applicationId,
      attachmentId: attachmentId,
      valueLocation: valueLocation,
      tabId: tabId
    },
    (resp)=>{
      if (resp.success){
        store.dispatch({
          type: UPDATE_SUPPORT_DOCUMENTS,
          values: resp.supportDocuments.values
        });
      }
    }
  );
}

export function deleteDocumentAction(context, applicationId, documentId, tabId){
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'deleteSupportDocument',
      applicationId: applicationId,
      documentId: documentId,
      tabId: tabId
    },
    (resp)=>{
      store.dispatch({
        type: UPDATE_SUPPORT_DOCUMENTS,
        values: resp.values
      });
    }
  );
}

export function addOtherDocument(context, values, docName, appId, tabId, rootValues, cb){
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'addOtherDocument',
      values: values,
      docName: docName,
      appId: appId,
      tabId: tabId,
      rootValues
    },
    (resp)=>{
      cb(resp);
      if (resp.success && !resp.duplicated) {
        store.dispatch({
          type: UPDATE_SUPPORT_DOCUMENTS,
          values: resp.values
        });
      }
    }
  );
}

export function updateOtherDocumentName(context, appId, tabId, documentId, newName, rootValues, cb){
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'updateOtherDocumentName',
      appId,
      tabId,
      documentId,
      newName,
      rootValues
    },
    (resp)=>{
      cb(resp);
      if (resp.success && !resp.duplicated) {
        store.dispatch({
          type: UPDATE_SUPPORT_DOCUMENTS,
          values: resp.values
        });
      }
    }
  );
}

export function isShowDragDropZone(context, isShow, valueLocation){
  let {store} = context;
  store.dispatch({
    type: SHOW_DRAG_DROP_ZONE,
    isShow: isShow,
    valueLocation: valueLocation
  });
}

export function closeSuppDocs(context, template, values, appId){
    let {store} = context;
    window.callServer(
        context,
        '/application',
        {
          action: 'closeSuppDocs',
          template,
          values,
          appId
        },
        (resp)=>{
          store.dispatch({
            type: CLOSE_SUPPORT_DOCUMENTS,
            template: resp.template,
            values: resp.values,
            isMandDocsAllUploaded: resp.isMandDocsAllUploaded,
            isSubmitted: false,
            page: POS_MAIN
          });
        }
    );
}

export function updateViewedList(context, appId, viewedList) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'updateSuppDocsViewedList',
      appId: appId,
      viewedList: viewedList
    },
    (resp)=>{
      if (resp.success) {
        store.dispatch({
          type: UPDATE_VIEWED_LIST,
          viewedList: viewedList
        });
      }
    }
  );
}

export function submitSuppDocsFiles(context, appId, values, viewedList, isSupervisorChannel, callback) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'submitSuppDocsFiles',
      appId: appId,
      values: values,
      viewedList: viewedList,
      isSupervisorChannel:isSupervisorChannel
    },
    (resp) => {
      if (resp.success){
        store.dispatch({
          type: UPDATE_SUPPORT_DOCUMENTS,
          values: values,
          viewedList: viewedList,
          pendingSubmitList: resp.pendingSubmitList
        });
        callback({
          sendHealthDeclarationNoti: resp.sendHealthDeclarationNoti
        });
      }
    }
  );
}

export function checkSuppDocsProperties(context, appId, callback) {
  // let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'checkSuppDocsProperties',
      appId
    },
    callback
  );
}
