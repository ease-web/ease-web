export const INIT_SUPPORT_DOCUMENTS_SHIELD = 'INIT_SUPPORT_DOCUMENTS_SHIELD';

export function goShieldSupportDocuments(context, appId, appStatus = 'APPLYING', userChannel = 'agent'){
  let {store} = context;
  window.callServer(
    context,
    '/shieldApplication',
    {
      action:'showSupportDocments',
      applicationId:appId,
      appStatus: appStatus
    }, function(resp){
      if (resp.success){
        store.dispatch({
          type: INIT_SUPPORT_DOCUMENTS_SHIELD,
          template:resp.template,
          values:resp.values,
          suppDocsAppView:resp.suppDocsAppView,
          appStatus: appStatus,
          viewedList: resp.viewedList,
          userChannel: userChannel,
          isReadOnly: resp.isReadOnly,
          defaultDocNameList: resp.defaultDocNameList,
          tokensMap: resp.tokensMap
        });
      }
    }
  );
}
