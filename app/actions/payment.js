export const INIT_PAYMENT_PAGE = 'INIT_PAYMENT_PAGE';
export const SERVER_ERROR = 'SERVER_ERROR';
export const PAYMENT_SUCCESS = 'PAYMENT_SUCCESS';
export const UPDATE_PAYMENT = 'UPDATE_PAYMENT';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const CLOSE_PAYMENT_PAGE = 'CLOSE_PAYMENT_PAGE';
export const DO_PAYMENT = 'DO_PAYMENT';
export const DO_SHIELD_PAYMENT = 'DO_SHIELD_PAYMENT';
export const RELOAD_PAYMENT = 'RELOAD_PAYMENT';
export const RELOAD_SHIELD_PAYMENT = 'RELOAD_SHIELD_PAYMENT';
export const PAYMENT_DONE = 'PAYMENT_DONE';
import * as _ from 'lodash';

import { SHOW_APP_ERR_MSG } from './GlobalActions';


export function initPayment(template, values, isMandDocsAllUploaded, isSubmitted) {
  return {
    type: INIT_PAYMENT_PAGE,
    template,
    values,
    isMandDocsAllUploaded,
    isSubmitted,
    updatePayment: true
  };
}

export function paymentSuccess() {
  return {
    type: PAYMENT_SUCCESS,
    updatePayment: true
  };
}

export function updatePayment(values, application) {
  return {
    type: UPDATE_PAYMENT,
    values,
    application,
    updatePayment: true
  };
}

export function handleError(errorMsg) {
  return {
    type: SHOW_APP_ERR_MSG,
    errorMsg: {
      errorMsg,
      isError: true
    },
    updatePayment: true
  };
}

export function initPayStore(context, docId, cb) {
  let {
    store
    // , router
  } = context;

  window.callServer(context, '/application', {
    action: 'getPaymentTemplateValues',
    docId: docId
  }, function(resp) {

    if (resp.success) {
      if (cb) {
        cb(resp);
      } else {
        store.dispatch(initPayment(resp.template, resp.values, resp.isMandDocsAllUploaded, resp.isSubmitted));
      }
    } else {
      store.dispatch(handleError(resp.id));
    }
  });
}

export function updateInitPaymentStatus(context, docId) {
  let {
    store,
    // router
  } = context;
  window.callServer(context, '/application', {
    action: 'updateApplicationInitPaymentStatus',
    docId: docId
  }, function(resp) {
    if (resp.success) {
      store.dispatch(paymentSuccess());
    } else {
      store.dispatch(handleError(resp.id));
    }
  });
}

let _updatePaymentMethods = function(context, docId, values, stepperIndex, updateStepper, cb) {
  let { store } = context;
  let { appForm } = store.getState();

  window.callServer(context, '/application', {
    action: 'updatePaymentMethods',
    docId: docId,
    values: values,
    stepperIndex: stepperIndex,
    updateStepper: updateStepper
  }, function(resp) {
    cb(resp);
    if (resp.success) {
      let application = window.isEmpty(resp.application) ? appForm.application : resp.application;
      store.dispatch(updatePayment(values, application));
    } else {
      store.dispatch(handleError(resp.id));
    }
  });
};

export function updatePaymentMethods(context, docId, values, stepperIndex, updateStepper, cb) {
  _updatePaymentMethods(context, docId, values, stepperIndex, updateStepper, cb);
}

export function closePaymentPage(context) {
  let { store } = context;

  store.dispatch({
    type: CLOSE_PAYMENT_PAGE,
    updatePayment: true
  });
}

export function startPayment(isShield, context, changedValues) {
  let {
    store,
    // router,
    // stepperIndex
  } = context;

  let { shieldApplication, payment, appForm } = store.getState();
  let { values } = payment;
  let { application } = appForm;
  let applicationId = _.get(application, 'id');

  if (isShield) {
    values = changedValues;
    applicationId = _.get(shieldApplication, 'application.id');
  }

  let newWindow = window.open('', 'EASE-Payment-Tab', 'location=no,top=100,toolbar=no,status=no,width=800,height=600', true);

  if (newWindow && !newWindow.closed) {
    window.paymentWindow = newWindow;

    window.callServer(context, '/application', {
      action: 'getPaymentUrl',
      docId: applicationId,
      values: values,
      isShield: isShield
    }, (result) => {
      if (result && result.url && result.url !== 'undefined' && isShield) {
        // For Shield Product
        if (newWindow && !newWindow.closed) {
          newWindow.location = result.url;
        } else {
          newWindow = window.open('', 'EASE-Payment-Tab', 'location=no,top=100,toolbar=no,status=no,width=800,height=600', true);
          window.paymentWindow = newWindow;
        }

        function checkChild() {
          if (newWindow.closed) {
            _checkShieldPaymentStatus(context, 'updateStatusAfterClose');
            store.dispatch({
              type: PAYMENT_DONE,
              updatePayment: true
            });
          } else {
            setTimeout(checkChild, 2000);
          }
        }

        store.dispatch({
          type: DO_SHIELD_PAYMENT,
          application: result.application,
          paymentWindow: newWindow,
          updatePayment: true
        });

        setTimeout(checkChild, 2000);
      } else if (result && result.url && result.url !== 'undefined') {
        // let newWindow = window.open(result.url, "EASE-Payment-Tab", "location=no,top=100,toolbar=no,status=no,width=800,height=600", true);

        if (newWindow && !newWindow.closed) {
          newWindow.location = result.url;
        } else {
          newWindow = window.open('', 'EASE-Payment-Tab', 'location=no,top=100,toolbar=no,status=no,width=800,height=600', true);
          window.paymentWindow = newWindow;
        }

        function checkChild() {
          if (newWindow.closed) {
            _checkPaymentStatus(context, 'updateStatusAfterClose');
            store.dispatch({
              type: PAYMENT_DONE,
              updatePayment: true
            });
          } else {
            setTimeout(checkChild, 2000);
          }
        }

        store.dispatch({
          type: DO_PAYMENT,
          values: result.application.payment,
          paymentWindow: newWindow,
          updatePayment: true
        });

        setTimeout(checkChild, 2000);
      } else {
        store.dispatch(handleError('Failure to get connect with Payment Gateway.'));
        newWindow.close();
      }
    });
  } else {
    store.dispatch(handleError('Please disable pop up blocking and try again.'));
  }
}

let _checkPaymentStatus = function(context, situration) {
  let {
    store,
    // router,
    // stepperIndex
  } = context;

  let {
    values
  } = store.getState().payment;

  let {
    application
  } = store.getState().appForm;

  let applicationId = _.get(application, 'id');

  window.callServer(context, '/application', {
    action: 'checkPaymentStatus',
    trxNo: values.trxNo,
    appId: applicationId
  }, (result) => {
    if (result && result.success) {
      let { paymentValues } = result;
      if (values.subseqPayMethod !== paymentValues.subseqPayMethod) {
        paymentValues.subseqPayMethod = values.subseqPayMethod;
      }

      store.dispatch({
        type: RELOAD_PAYMENT,
        values: paymentValues,
        updatePayment: true
      });
    } else {
      // store.dispatch(handleError('failure to get payment url'));
    }
  });
};

let _checkShieldPaymentStatus = (context, resetLoading) => {
  let { store } = context;

  let values, applicationId;

  let { shieldApplication } = store.getState();

  values = _.get(shieldApplication, 'application.payment');
  applicationId = _.get(shieldApplication, 'application.id');

  window.callServer(context, '/shieldApplication', {
    action: 'checkPaymentStatus',
    trxNo: values.trxNo,
    appId: applicationId
  }, (result) => {
    if (result && result.success) {
      let { application } = result;

      store.dispatch({
        type: RELOAD_SHIELD_PAYMENT,
        application: application,
        updatePayment: true
      });
    } else {
      // store.dispatch(handleError('failure to get payment url'));
    }
  });
};

export function checkPaymentStatus(isShield, context, resetLoading) {
  if (isShield) {
    _checkShieldPaymentStatus(context, resetLoading);
  } else {
    _checkPaymentStatus(context, resetLoading);
  }
}
