export const INIT_CLIENT_CHOICE = 'INIT_CLIENT_CHOICE';
export const UPDATE_CLIENT_CHOICE = 'UPDATE_CLIENT_CHOICE';
export const SERVER_ERROR = 'SERVER_ERROR';

export const POS_CLIENT_CHOICE = 'POS_CLIENT_CHOICE';


export function initClientChoiceStore(context, quotCheckedList, isReadOnly) {
  let {
    store
  } = context;
  let {client} = store.getState();
  let clientId = client.profile.cid;

  callServer(
    context,
    '/clientChoice',
    {action: 'clientChoice', quotCheckedList: quotCheckedList, isReadOnly:isReadOnly, clientId:clientId},
    function(resp){
      store.dispatch(initClientChoice(
        resp.template,
        resp.values,
        resp.isRecommendationCompleted,
        resp.isBudgetCompleted,
        resp.isAcceptanceCompleted,
        resp.clientChoiceStep
      ));
    }
  );
}

export function updateClientChoiceCB(context, stepperIndex, values, isDone, isSaveOnly, callback) {
  let {
    store
  } = context;

  let {client} = store.getState();
  let clientId = client.profile.cid;

  callServer(
    context,
    '/clientChoice',
    {action: 'updateQuotationClientChoice', stepperIndex:stepperIndex, values:values, isSaveOnly:isSaveOnly, clientId:clientId},
    function(resp){
      if (resp.success) {
        callback();
        store.dispatch(updateClientChoice(
          values,
          resp.isRecommendationCompleted,
          resp.isBudgetCompleted,
          resp.isAcceptanceCompleted,
          resp.clientChoiceStep,
          isDone
        ));
      } else {
        store.dispatch(handleError(resp.error));
      }
    }
  );
}

export function initClientChoice(template, values, isRecommendationCompleted, isBudgetCompleted, isAcceptanceCompleted, clientChoiceStep) {
  return {
    type: INIT_CLIENT_CHOICE,
    template,
    values,
    isRecommendationCompleted,
    isBudgetCompleted,
    isAcceptanceCompleted, 
    clientChoiceStep
  }
}

export function updateClientChoice(values, isRecommendationCompleted, isBudgetCompleted, isAcceptanceCompleted, clientChoiceStep, isDone) {
  return {
    type: UPDATE_CLIENT_CHOICE,
    values,
    isRecommendationCompleted,
    isBudgetCompleted,
    isAcceptanceCompleted,
    clientChoiceStep,
    isDone
  }
}

export function handleError(errorMsg) {
  return {
    type: SERVER_ERROR,
    errorMsg
  };
}
