
export const GO_ESIGN = 'GO_ESIGN_FROM_APPLICATION';
export const GO_SUPPORT_DOCS = 'GO_SUPPORT_DOCS';
export const UPDATE_APPLICATION_LIST = 'UPDATE_APPLICATION_LIST';
export const FNA_NOT_COMPLETED = 'FNA_NOT_COMPLETED';
export const DECLARATION_CHECK = 'DECLARATION_CHECK';
export const APPLICATION_SELECT_PAGE = 'APPLICATION_SELECT_PAGE';
export const SHOW_APPLICATION = 'SHOW_APPLICATION';
export const GET_Q_PDF = 'GET_Q_PDF';
export const ADD_ATTACHMENT = 'ADD_ATTACHMENT';
export const BACK_TO_QUOTE = 'BACK_TO_QUOTE';
export const GO_UPLOAD = 'GO_UPLOAD';
export const CLOSE_UPLOAD = 'CLOSE_UPLOAD';
export const OPEN_SINGED_VIEW = 'OPEN_SINGED_VIEW';
export const CLOSE_SINGED_VIEW = 'CLOSE_SINGED_VIEW';
export const DELETE_DOC = 'DELETE_DOC';
export const INCOMPLETE_CLIENT_CHOICE = 'INCOMPLETE_CLIENT_CHOICE';
export const POS_CHANGE_PAGE = 'POS_CHANGE_PAGE';
export const GET_SUPPORT_DOCUMENTS = 'GET_SUPPORT_DOCUMENTS';
export const POS_EAPP = 'eApp';
export const POS_PROPOSAL = 'proposal';
export const POS_QUOTATION = 'quotation';
export const POS_MAIN = 'index';
export const SUPPORT_DOCUMENTS = 'SUPPORT_DOCUMENTS';
export const UPDATE_QUOT_CHECKED_LIST = 'UPDATE_QUOT_CHECKED_LIST';
export const INIT_APPLICAITON_SUMMARY = 'INIT_APPLICAITON_SUMMARY';
export const OPEN_APPSUM_EMAIL_PAGEONE = 'OPEN_APPSUM_EMAIL_PAGEONE';
export const CLOSE_APPSUM_EMAIL_PAGEONE = 'CLOSE_APPSUM_EMAIL_PAGEONE';
export const OPEN_APPSUM_EMAIL_PAGETWO = 'OPEN_APPSUM_EMAIL_PAGETWO';
export const CLOSE_APPSUM_EMAIL_PAGETWO = 'CLOSE_APPSUM_EMAIL_PAGETWO';
export const UPDATE_POLICY_NUMBER = 'UPDATE_POLICY_NUMBER';
export const DELETE_APPLICATION_LIST = 'DELETE_APPLICATION_LIST';
export const DISMISS_EAPP_CROSSAGE_PROMPT = 'DISMISS_EAPP_CROSSAGE_PROMPT';

// import { LOAD_QUOTATION } from './quotation';
import { LOADING, HIDE_APP_ERR_MSG, SHOW_APP_ERR_MSG } from './GlobalActions';
import * as _ from 'lodash';
import * as cActions from './client';
import {formatDate} from '../../common/DateUtils';

//No use
// export function getPolicyNumber(context, appId, menuIndex, cb){
//   window.callServer(
//     context,
//     '/application',
//     {action: 'getApplicationPolicyNumber', applicationId: appId},
//     function(response){
//       if (response.success){
//         var {store} = context;
//         var {
//           appForm,
//           // client
//         } = store.getState();

//         let application = appForm.application;
//         application.policyNumber = response.policyNumber;
//         application._rev = response._rev;
//         let res = {
//           application: application,
//           type: UPDATE_POLICY_NUMBER
//         };
//         store.dispatch(res);
//       } else {
//         if (cb) {
//           cb();
//         }
//       }
//     }
//   );
// }

export function initAppSummary(context){
  var {store} = context;
  let res = {
    type: INIT_APPLICAITON_SUMMARY
  };
  store.dispatch(res);
}

export function backToPosMain(context){
  var {
    store
  } = context;
  let res = {
    type: POS_CHANGE_PAGE,
    page: POS_MAIN
  };
  store.dispatch(res);
}

export function goAppFormPage(context, quotationId){
  var {
    store
  } = context;
  let res = {
    type: POS_CHANGE_PAGE,
    page: POS_EAPP
  };
  let {
    quotation
  } = store.getState();
  let quotationDoc = {};
  if (quotation && quotation.quotation) {
    quotationDoc = quotation.quotation;
  }
  quotationDoc._id = quotationId;
  res.quotation = quotationDoc;
  store.dispatch(res);
}

export function saveQuotCheckedList(context, quotCheckedList){
  let {store} = context;
  store.dispatch({
    type: UPDATE_QUOT_CHECKED_LIST,
    quotCheckedList:quotCheckedList
  });
}

export function saveFormValues(context, appId, clientId, cValues, genPDF, extra, cb){
  var {
      store
  } = context;

  var {
    appForm
  } = store.getState();

  if (!appId){
    appId = appForm.application._id || appForm.application.id;
  }

  if (appForm.application.isStartSignature){
    if (cb){
        cb();
    }
  } else {
      if (!clientId) {
        // clientId = appForm.application.iCid;
        clientId = appForm.application.pCid;
      }

  callServer(
      context,
      '/application',
      {
        action: 'saveForm',
        applicationId: appId,
        clientId : clientId,
        values: cValues,
        genPDF: genPDF,
        extra: extra
      },
      function(response){

        if (extra.toGetPolicyNumber) {
          if (response.policyNumber){
              let application = appForm.application;
              application.policyNumber = response.policyNumber;
              application.applicationForm.values = cValues;
              store.dispatch({
                type: UPDATE_POLICY_NUMBER,
                application:application
              });
          } else {
            store.dispatch({
              type: SHOW_APP_ERR_MSG,
              errorMsg: {
                isError: true,
                errorMsg: 'Policy number exhausted. Please contact IT support (REF code: 00002)'
              }
            });
          }
        }

          if (response.updIds && response.updIds.length > 0){
              let _cValues = cValues;
              let upArry = [];
              if (_cValues.proposer && _cValues.proposer.personalInfo && response.updIds.indexOf(_cValues.proposer.personalInfo.cid)>-1 ){
                  upArry.push(_cValues.proposer.personalInfo);
              }
              if (_cValues.insured && _cValues.insured.length > 0){
                  for (let laIndex = 0; laIndex < _cValues.insured.length; laIndex++){
                      let curLa = _cValues.insured[laIndex];
                      if (curLa.personalInfo && response.updIds.indexOf(curLa.personalInfo.cid)){
                          upArry.push(curLa);
                      }
                  }
              }
              if (upArry.length > 0 ){
                  let updClient = function(){
                      cActions.updateStoreProfile(context, upArry);
                  }
                  setTimeout(updClient, 1000);
              }
          }

              if (cb) {
                cb(response);
              }
          }
        );

    }
}

export function goApplication(context, applicationId) {
  var {
    store
  } = context;

  window.callServer(
    context,
    '/application',
    {action: 'goApplication', id: applicationId},
    function(resp){
      let result = {
        type                    : SHOW_APPLICATION,
        showSignExpiryWarning   : resp.showSignExpiryWarning,
        application             : resp.application,
        template                : resp.template,
        crossAgeObj             : resp.crossAgeObj
      };
      store.dispatch(result);
    }
  );
}

export function apply(context, quotationId) {
  var {store} = context;
  let {quotation, approval} = store.getState();
  let quotationDoc = _.get(quotation, 'quotation') || {};
  quotationDoc._id = quotationId;

  window.callServer(
    context,
    '/application',
    {
      action: 'apply',
      id: quotationId
    },
    function(resp){
      if (resp && resp.success) {
        if (resp.isInvalidated) {
          store.dispatch({
            type: FNA_NOT_COMPLETED,
            isFNACompleted: false,
            appList: resp.appList
          });
        } else {
          let result = {
            type: SHOW_APPLICATION,
            application:resp.application,
            template:resp.template,
            quotation: quotationDoc
          };
          store.dispatch(result);
        }
      } else {
        // handle error
      }
    }
  );
}

export function validateValues(item, values) {
  var errorMsg = null;
  var result = true;
  if (item && values) {
    // var value = values[item.id];
    var error = window.validate(item, values);
    result = !error;

    //validation of total share in beneficiaries
    if (result && item.id === 'beneficiary'){
      let B1 = values.B1;
      let totalShare = 0;
      if (B1){
        for (let i = 0; i < B1.length; i++){
          totalShare += B1[i].bShare;
        }
        if (totalShare !== 100){
          result = false;
          errorMsg = 'error.beneficiary.totalShare';
        }
      }
    }
  } else {
    result = false;
  }
  return {valid: result, errorMsg: errorMsg};
}

export function declarationSelect(store, key, value){
  store.dispatch({
    type: DECLARATION_CHECK,
    key: key,
    value: value,
  });
}

export function selectPage(store, index){
  store.dispatch({
    type: APPLICATION_SELECT_PAGE,
    selected : index,
  });
}

export function back2Proposal(store) {
  store.dispatch({
    type: BACK_TO_QUOTE
  });
}

export function goESign(application, store, router) {
  var {
    attachments
  } = store.getState().application;

  setTimeout(function() {
    window.connectSocket('/app', store, router, {
      action: 'genESignPDF',
      application: application,
      attachments: attachments,
      nonGN: store.getState().app.nonGN,
    }, function(resp) {
      if (resp.success) {
        store.dispatch({
          type: GO_ESIGN,
          application: resp.application,
          pdfSummary: resp.fnaPdf,
          pdfProposal: resp.proposalPdf,
          pdfApplication: resp.appPdf
        });
      } else {
          // TODO:: handle error
      }
    });
  }, 10);

  store.dispatch({
    type: LOADING
  });
}

export function  getAppListView(context, profile, bundleId){
  var {
    store
  } = context;

    window.callServer(
      context,
      '/application',
      {
          action: 'getAppListView',
          profile: profile,
          bundleId: bundleId
      },
      function(resp){
        if (resp.success) {
          store.dispatch({
            type: UPDATE_APPLICATION_LIST,
            bundleId: bundleId,
            applicationsList: resp.applicationsList,
            pdaMembers: resp.pdaMembers,
            clientChoiceFinished: resp.clientChoiceFinished,
            quotCheckedList: resp.quotCheckedList,
            agentChannel: resp.agentChannel,
            isFNACompleted: resp.isFNACompleted,
            isFromCreateProposal: resp.isFromCreateProposal
          });
        }
      }
    );
}

export function closeUpload(store){
  store.dispatch({
    type: CLOSE_UPLOAD
  });
}

export function submitDoc(context, attachments){
  var id = store.getState().appList.targetAppId;

  var {
    store
  } = context;

  window.callServer(
    context,
    '/app',
    {action: 'submitDoc', id: id, attachments: attachments},
    function(resp){
      store.dispatch({
        type: CLOSE_UPLOAD
      });
    }
  );
}

export function getSignedDocs(context, id){
  var {
      store
  } = context;

  window.callServer(
    context,
    '/app',
    {action: 'getSignedDocs', id: id},
    function(resp){
      if (resp.success){
          store.dispatch({
            type: OPEN_SINGED_VIEW,
            docs: resp.docs,
          });
      }
    }
  );
}
export function CloseSignedDocs(store){
  store.dispatch({
    type: CLOSE_SINGED_VIEW
  });
}

export function deleteApplications(context, deleteItemsSelectedArray, applicationsList,
  clientChoiceFinished, rollbackBundle){
    var {store} = context;

    callServer(
        context,
        '/application',
        {
          action: 'deleteApplications',
          deleteItemsSelectedArray: deleteItemsSelectedArray,
          applicationsList: applicationsList,
          rollbackBundle: rollbackBundle
        }
        , function(resp){
            if (resp.success === true){
                store.dispatch({
                    type: DELETE_APPLICATION_LIST,
                    applicationsList: resp.result,
                    quotCheckedList: resp.quotCheckedList,
                    clientChoiceFinished: false
                })
            } else {
                store.dispatch({
                    type: DELETE_APPLICATION_LIST,
                    applicationsList: resp.result,
                    quotCheckedList: resp.quotCheckedList,
                    clientChoiceFinished: clientChoiceFinished
                })
            }
        });
}

let getEmailContent = function (content, params, embedImgs) {
  let ret = content;
  _.each(params, (value, key) => {
    ret = ret.replaceAll('{{' + key + '}}', value);
  });
  // _.each(embedImgs, (value, key) => {
  //   ret = ret.replace(new RegExp('cid:' + key, 'g'), 'data:image/png;base64,' + value);
  // });
  return ret;
};

export function openEmail(context, selectedApps, selectedAttachments, quotIds){
  const {store, langMap} = context;
  const {app, client,
      // proposal
  } = context.store.getState();
  window.callServer(context, '/application', {
      action: 'getEmailTemplate'
  }, (resp) => {
    if (resp && resp.success) {
      let {clientSubject, clientContent, agentSubject, agentContent, embedImgs} = resp;
      let params = {
          agentName: app.agentProfile.name,
          agentContactNum: app.agentProfile.mobile,
          clientName: client.profile.fullName
      };
      let emails = [];
      for (var appKey in selectedApps){
        if (selectedApps[appKey] === true){
          let attachments = [];
          for (var attKey in selectedAttachments[appKey]){
            if (selectedAttachments[appKey][attKey] === true){
              attachments.push({
                id: attKey,
                title: window.getLocalizedText(langMap, 'proposal.tab.' + attKey)
              });
            }
          }
          emails.push({
            id: 'agent_' + appKey,
            type:'agent',
            name: window.getLocalizedText(langMap, 'email.recipient.agent'),
            toAddrs: [app.agentProfile.email],
            subject: agentSubject,
            content: getEmailContent(agentContent, params, embedImgs),
            attachments: attachments,
            embedImgs: embedImgs,
            quotId: quotIds[appKey],
            appId: appKey
          });
          emails.push({
            id: 'client_' + appKey,
            type: 'client',
            name: window.getLocalizedText(langMap, 'email.recipient.client'),
            toAddrs: [client.profile.email],
            subject: clientSubject,
            content: getEmailContent(clientContent, params, embedImgs),
            attachments: attachments,
            embedImgs: embedImgs,
            quotId: quotIds[appKey],
            appId: appKey
          });
        }
      }
      store.dispatch({
        type: OPEN_APPSUM_EMAIL_PAGETWO,
        emails: emails
      });
    }
  });
}

export function closeEmail(context){
  let {store} = context;
  store.dispatch({
    type:CLOSE_APPSUM_EMAIL_PAGETWO
  });
}

export function openAppSumEmail(context){
    let {store} = context;
    window.callServer(
        context,
        '/application',
        {
            action: 'getAppSumEmailAttKeys'
        },
        (resp) => {
            store.dispatch({
                type: OPEN_APPSUM_EMAIL_PAGEONE,
                attKeysMap: resp.success ? resp.attKeysMap : []
            });
        }
    );
}

export function closeAppSumEmail(context){
  let {store} = context;
  store.dispatch({
    type: CLOSE_APPSUM_EMAIL_PAGEONE
  });
}

export function sendEmail(context, emails){
  let {store} = context;
  let {client} = store.getState();
  let cid = client.profile.cid;
  window.callServer(
    context,
    '/application',
    {
      action:'sendApplicationEmail',
      emails: emails,
      cid:cid
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: CLOSE_APPSUM_EMAIL_PAGETWO
        });
      }
    }
  );
}

export function confirmAppPaid(context, appId, cb) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action:'confirmAppPaid',
      appId: appId
    },
    (resp) => {
      if (resp && resp.success) {
        cb(true);
      } else {
        cb(false);
      }
    }
  );

}

export function incompleteClientChoice(context){
  let {
    store
  } = context;
  store.dispatch({
      type: INCOMPLETE_CLIENT_CHOICE
  })
}

export function setSignExpiryShown(context){
  let { store } = context;
  let { appForm } = store.getState();

  callServer(
      context,
      '/application',
      {
          action:'setSignExpiryShown',
          appId: appForm.application.id
      },
      (resp) => {
        store.dispatch({
          type: HIDE_APP_ERR_MSG
        });
      }
  );
}

export function dismissCrosseAppAgePrompt(context) {
  let { store } = context;
  store.dispatch({
    type: DISMISS_EAPP_CROSSAGE_PROMPT
  });
}

