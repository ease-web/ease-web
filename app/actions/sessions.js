import {
  SessionExpiry_Min,
  SessionAlertMin,
  SessionIntervalCheck_Millsec
} from '../constants/ConfigConstants.js';

import {
  showErrorMsg,
  logout
} from './GlobalActions';

import _isFunction from 'lodash/fp/isFunction';

export const RESET_SESSION_TIMER = 'RESET_SESSION_TIMER';
export const UPDATE_SESSION_TIMER = 'UPDATE_SESSION_TIMER';
export const SESSION_ALERT_OPEN = 'SESSION_ALERT_OPEN';
export const SESSION_ALERT_CLOSE = 'SESSION_ALERT_CLOSE';
export const REDUCE_SESSION_TIME = 'REDUCE_SESSION_TIME';
export const SESSION_TIME_OUT = 'SESSION_TIME_OUT';

export function extendSession(context, callback){
  window.callServer(
    context,
    '/auth',
    {
      action: 'extendSession',

    },
    function(resp){
      context.store.dispatch({
        type: RESET_SESSION_TIMER
      });
      if (_isFunction(callback)){
        callback();
      }
    }
  );
}

export function resetSession(context, createNewTimer){
  let {
    store,
  } = context;

  if (createNewTimer)  {
    setInterval(function(){
      checkSessionTimeout(context);
    }, SessionIntervalCheck_Millsec);
  }
  store.dispatch({
    type: RESET_SESSION_TIMER
  });
}

const checkSessionTimeout = function(context) {
  let {
    store
  } = context;
  let {
    timestamp
  } = store.getState().sessions;
  var now = (new Date()).getTime();
  var diffFromLastSessionTime_Min = (now - timestamp) / 60000;

  if ( (timestamp && (diffFromLastSessionTime_Min >= SessionExpiry_Min ))) {
    showErrorMsg(context, {
      errorMsg: 'Session expired, please login again.',
      action: function() {
        logout(context, true);
      }
    });
    setTimeout(function() {
      logout(context, true);
    }, 10000);
  }
  else if ( (timestamp && diffFromLastSessionTime_Min >= SessionAlertMin )) {
    store.dispatch({
      type: SESSION_ALERT_OPEN
    });
  }
};

export function closeSessionAlert(context){
  context.store.dispatch({
    type: SESSION_ALERT_CLOSE
  });
}
