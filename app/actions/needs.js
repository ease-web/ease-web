// import {showLoading} from './GlobalActions';
// import {saveTrustedIndividual} from './client';
import * as _ from 'lodash';
import * as _s from './sessions';

export const UPDATE_FNA = 'UPDATE_FNA';
export const UPDATE_PDA = 'UPDATE_PDA';
export const UPDATE_PDA_ERROR = 'UPDATE_PDA_ERROR';
export const UPDATE_FE = 'UPDATE_FE';
export const INIT_NEEDS = 'INIT_NEEDS';
export const GENERATE_FNA_REPORT = 'GENERATE_FNA_REPORT';
export const OPEN_EMAIL = 'OPEN_FNAREPORT_EMAIL';
export const CLOSE_EMAIL = 'CLOSE_FNAREPORT_EMAIL';
export const CLOSE_EMAILADDR_DIALOG = 'CLOSE_FNAREPORT_EMAILADDR_DIALOG';
export const OPEN_EMAILADDR_DIALOG = 'OPEN_FNAREPORT_EMAILADDR_DIALOG';
export const OPEN_FE = 'OPEN_FINANCIAL_EVALUATION';
export const CLOSE_FE = 'CLOSE_FINANCIAL_EVALUATION';
export const POS_FE = 'financial_evaluation';
export const RESET_NEEDS = 'RESET_NEEDS';
export const CLEAR_FNA_REPORT = 'CLEAR_FNA_REPORT';

export function clearFNAReport(context){
  context.store.dispatch({
    type: CLEAR_FNA_REPORT
  })
}

export function savePDA(context, pda, confirm, callback){
  let {store
    // , router
  } = context;
  let {needs, client} = store.getState();
  let tiInfo;

  if (!window.isEmpty(pda.tiInfo) && !window.isEqual(pda.tiInfo, client.profile.trustedIndividuals)){
    tiInfo = _.cloneDeep(pda.tiInfo);
  }
  else {
    tiInfo = _.cloneDeep(client.profile.trustedIndividuals);
  }
  delete pda.tiInfo;
  if (!window.isEmpty(tiInfo) || !window.isEqual(pda, needs.pda)){
    let pCid = _.get(client, 'profile.cid');
    window.callServer(
      context,
      '/needs',
      {
        action: 'savePDA',
        cid: pCid,
        pda,
        tiInfo,
        confirm
      },
      function(resp){
        if (resp && resp.success){
          if (resp.code){
            pda.tiInfo = tiInfo;
            callback(resp.code)
          } else {
            store.dispatch({
              type: UPDATE_PDA,
              profile: resp.profile,
              pda: resp.pda,
              fe: resp.fe,
              fna: resp.fna,
              showFnaInvalidFlag: resp.showFnaInvalidFlag
            });
            callback();
          }
        }
      }
    );
  }
  else {
    callback();
  }
}

export function saveFE(context, fe, confirm, callback){
  let {store
    // , router
  } = context;
  let {needs, client} = store.getState();

  if (window.isEqual(needs.fe, fe)){
    _s.extendSession(context, callback);
  } else {
    let pCid = _.get(client, 'profile.cid');
    window.callServer(
      context,
      '/needs',
      {
        action: 'saveFE',
        cid: pCid,
        fe,
        confirm
      },
      function(resp){
        if (resp && resp.success){
          if (resp.code){
            callback(resp.code)
          } else {
            store.dispatch({
              type: UPDATE_FE,
              profile: resp.profile,
              fe: resp.fe,
              fna: resp.fna,
              showFnaInvalidFlag: resp.showFnaInvalidFlag
            });
            callback();
          }
        }
      }
    );
  }
}

export function saveFNA(context, fna, confirm, callback){
  let {store
    // , router
  } = context;
  let {needs, client} = store.getState();

  if (window.isEqual(needs.fna, fna)){
    _s.extendSession(context, callback);
  } else {
    let pCid = _.get(client, 'profile.cid');
    window.callServer(
      context,
      '/needs',
      {
        action: 'saveFNA',
        cid: pCid,
        fna,
        confirm
      },
      function(resp){
        if (resp && resp.success){
          if (resp.code){
            callback(resp.code)
          } else {
            store.dispatch({
              type: UPDATE_FNA,
              profile: resp.profile,
              fna: resp.fna,
              showFnaInvalidFlag: resp.showFnaInvalidFlag
            });
            callback();
          }
        }
      }
    );
  }
}

export const initNeeds = function(context, callback){
  let {store
    // , router
  } = context;
  let {profile} = store.getState().client;

  window.callServer(
    context,
    '/needs',
    {
      action: 'initNeeds',
      cid: profile.cid
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: INIT_NEEDS,
          dependantProfiles: resp.dependantProfiles,
          pda: resp.pda,
          fe: resp.fe,
          fna: resp.fna,
          needsSummary: resp.needsSummary,
              showFnaInvalidFlag: resp.showFnaInvalidFlag
        });
      }
    }
  );
};

export const getFNAReport = function(context) {
  let {store
    // , router
  } = context;
  let {profile} = store.getState().client;
  window.callServer(
    context,
    '/needs',
    {
      action: 'generateFNAReport',
      cid: profile.cid,
      profile
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: GENERATE_FNA_REPORT,
          fnaReport: {
            pdfstr: resp.fnaReport,
            attachmentName: 'FNA Report'
          }
        });
      }
    }
  );
};

export function openFNAEmailDialog(context) {
  const {store, langMap} = context;
  const {app, client} = context.store.getState();
  window.callServer(context,
    '/needs', {
      action: 'getFNAEmailTemplate'
    }, (resp) => {
      if (resp && resp.success) {
        let {agentSubject, agentContent, clientSubject, clientContent, embedImgs} = resp;
        agentContent = agentContent.replaceAll('{{agentName}}', app.agentProfile.name);
        agentContent = agentContent.replaceAll('{{agentContactNum}}', app.agentProfile.mobile);
        let agContent = agentContent.replaceAll('{{recipientName}}', app.agentProfile.name);

        clientContent = clientContent.replaceAll('{{agentName}}', app.agentProfile.name);
        clientContent = clientContent.replaceAll('{{agentContactNum}}', app.agentProfile.mobile);
        let recipientContent = clientContent.replaceAll('{{recipientName}}', client.profile.fullName);

        let emails = [{
          id: 'agent',
          name: window.getLocalizedText(langMap, 'email.recipient.agent'),
          toAddrs: [app.agentProfile.email],
          subject: agentSubject,
          content: agContent,
          embedImgs: embedImgs
        }, {
          id: 'client',
          name: window.getLocalizedText(langMap, 'email.recipient.client'),
          toAddrs: [client.profile.email],
          subject: clientSubject,
          content: recipientContent,
          embedImgs: embedImgs
        }];
        store.dispatch({
          type: OPEN_EMAIL,
          emails: emails
        });
      }
    });
}

export function openEmailAddrInputDialog(context) {
  const {store} = context;
  store.dispatch({
    type: OPEN_EMAILADDR_DIALOG
  });
}

export function openEmail(context) {
  const {client} = context.store.getState();
  if (_.get(client, 'profile.email', '').length === 0) {
    openEmailAddrInputDialog(context);
  } else {
    openFNAEmailDialog(context);
  }
}

export function skipEnterEmailAddr(context){
  closeEmailAddrInputDialog(context);
  openFNAEmailDialog(context);
}

export function closeEmail(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_EMAIL
  });
}

export function closeEmailAddrInputDialog(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_EMAILADDR_DIALOG
  });
}

export function sendEmail(context, emails, skipEmailInReport) {
  const {store} = context;
  const {profile} = store.getState().client;
  const {agentProfile} = store.getState().app;
  window.callServer(context, '/needs', {
    action: 'emailFnaReport',
    emails: emails,
    profile,
    agentProfile,
    skipEmailInReport
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: CLOSE_EMAIL
      });
    }
  });
}

export function goFE(context) {
    var {
        store
    } = context;

  store.dispatch({
    type: OPEN_FE
  });
}

export function closeFE(context) {
    var {
        store
    } = context;

  store.dispatch({
    type: CLOSE_FE
  });
}

export function resetNeedstoEmpty(context){
    var {
        store
    } = context;

    store.dispatch({
      type: RESET_NEEDS,
      dependantProfiles: {},
      pda: {},
      fe: {},
      fna: {},
      needsSummary: {}
    });
}
