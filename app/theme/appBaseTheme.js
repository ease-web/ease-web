const Colors = require('material-ui/styles/colors');
const ColorManipulator = require('material-ui/utils/colorManipulator');
const Spacing = require('material-ui/styles/spacing').default;
const MobileSpacing = require('./mobileSpacing').default;
const zIndex = require('material-ui/styles/zIndex');
const getMuiTheme = require('material-ui/styles/getMuiTheme').default;

const Orienatation = {
  PORTRAIT: 1,
  LANDSCAPE: 2,
};

module.exports = {
  spacing: Spacing,
  fontFamily: '"SourceSansPro", Arial, Helvetica',
  zIndex: zIndex,
  palette: {
    // NOTE: lucia primary1Color from 103184 to 00008f; primary5Color from 22b250 to 1cc54e
    primary1Color: "#00008f",
    primary2Color: "#3054ae",
    primary3Color: "#ec4d33",
    primary4Color: "rgb(250, 250, 250)",
    primary5Color: "#1cc54e",

    secondary1Color: "#FFFFFF",
    secondary2Color: "#103184",

    selectedBackgroundColor:"rgba(0,0,0,0.1)",
    panelBackgroundColor: '#FAFAFA',
    selectColor: "#ff1721",
    borderColor: "#cac4c4",

    accent1Color: Colors.pinkA200,
    accent2Color: Colors.grey100,
    accent3Color: Colors.grey500,
    textColor: '#333333',
    text1Color: '#7f7f7f',
    text2Color: '#999999',

    alternateTextColor: "#103184",
    canvasColor: Colors.white,
    //NOTE lucia borderColor: Colors.faintBlack, //"rgba(0,0,0,.12)", //_colors2.default.grey300,
    disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
    disabledColor2: "rgba(255,255,255,0.5)",
    disabledUnderlineColor: 'rgba(35,31,32,0.26)',
    pickerHeaderColor: Colors.cyan500,
    clockCircleColor: ColorManipulator.fade(Colors.darkBlack, 0.07),
    labelColor: '#333333',
    tableHeaderColor: '#7f7f7f',
    fixedLabelColor: Colors.lightBlack,
    errorColor: '#F44336',
    // NOTE: lucia alertColor from ec4d33 to ff1721
    alertColor: '#ff1721',
    //NOTE: lucia warningColor from f6a623 to ff1721
    warningColor: '#ff1721',
    background2Color: '#FAFAFA',
    hoverBackground: Colors.minBlack,
    // NOTE: lucia shadowColor from cdcdcd to 9c9c9c
    shadowColor: '#9c9c9c',
  },

  theme: false,
  getTheme: function(refresh) {
    if (!this.theme || refresh) {
      var width = this.windowWidth = window.innerWidth;
      var height = this.windowHeight = window.innerHeight;
      var mobileSize = 400;
      if (window.isMobile.phone || width <= mobileSize || height <= mobileSize) {
        this.spacing = MobileSpacing;
      } else {
        this.spacing = Spacing;
      }

      var theme = getMuiTheme(this);

      if (window.isMobile.phone || height <= mobileSize || width <= mobileSize) {
        theme.isMobile = true;
      } else {
        theme.isMobile = false;
      }

      theme.isPortrait = (width>height*1.3)?false:true,
      // windowWidth: width,
      // windowHeight: height
      theme.button.textTransform = 'none';

      theme.radioButton.checkedColor = this.palette.primary3Color;

      theme.checkbox.checkedColor = this.palette.primary3Color;
      theme.checkbox.boxColor = this.palette.text1Color;

      theme.flatButton.textColor = this.palette.primary2Color;
      theme.flatButton.primaryTextColor = this.palette.primary2Color;
      theme.flatButton.secondaryTextColor = this.palette.alternateTextColor;

      theme.toggle.trackOffColor = Colors.grey400
      theme.toggle.trackOnColor = ColorManipulator.fade(this.palette.primary3Color, 0.5);
      theme.toggle.thumbOnColor = this.palette.primary3Color;

      //theme.raisedButton.textColor = this.palette.primary2Color;
      theme.raisedButton.primaryTextColor = this.palette.primary4Color;
      theme.raisedButton.primaryColor = this.palette.primary2Color;

      theme.raisedButton.secondaryTextColor = this.palette.secondary2Color;
      theme.raisedButton.secondaryColor = this.palette.secondary1Color;
      theme.raisedButton.disabledTextColor = this.palette.disabledColor2;

      theme.toolbar.backgroundColor = this.palette.primary1Color;
      theme.toolbar.iconColor = this.palette.primary2Color;
      theme.toolbar.hoverColor = this.palette.primary2Color;
      theme.toolbar.menuHoverColor = this.palette.primary2Color;
      theme.toolbar.height = this.spacing.desktopToolbarHeight;

      theme.textField.fixedLabelColor = this.palette.fixedLabelColor;
      theme.textField.floatingLabelColor = this.palette.fixedLabelColor;
      theme.textField.disabledTextColor = this.palette.textColor;
      theme.textField.focusColor = this.palette.alternateTextColor;
      theme.textField.errorColor = this.palette.alertColor;

      theme.dialog.bodyColor = this.palette.textColor;
      theme.tabs.backgroundColor = "transparent";
// NOTE: lucia datePicker.textColor from alternateTextColor to secondary1Color
      theme.datePicker.textColor = this.palette.secondary1Color;
      theme.datePicker.selectTextColor = this.palette.secondary1Color;

      theme.stepper.iconColor = this.palette.primary3Color;

      // theme.tabs.selectedTextColor = ColorManipulator.fade(this.palette.primary1Color, 0.87);
      // theme.tabs.textColor = this.palette.text1Color;
      theme.inkBar.backgroundColor = "#FFAABB";


      theme.tabs.textColor = this.palette.primary2Color;
      theme.tabs.selectedTextColor = this.palette.primary2Color;

      theme.radioButton.checkedColor = this.palette.primary1Color;
      theme.radioButton.disabledColor = this.palette.primary1Color;
      theme.checkbox.boxColor = this.palette.primary1Color;
      theme.checkbox.checkedColor = this.palette.primary1Color;
      theme.checkbox.disabledColor = this.palette.primary1Color;

      theme.slider.trackColor = this.palette.shadowColor;
      theme.slider.handleColorZero = this.palette.primary1Color;

      theme.inkBar.backgroundColor = this.palette.primary3Color;

      theme.menuItem.selectedTextColor = this.palette.primary1Color;

      theme.snackbar.actionColor = this.palette.primary4Color;
      theme.snackbar.textColor = this.palette.primary4Color;
      theme.snackbar.backgroundColor = this.palette.primary2Color;

      this.theme = theme;
    }
    return this.theme;
  }
};
