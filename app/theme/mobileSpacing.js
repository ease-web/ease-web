"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  iconSize: 24,
  desktopGutter: 16,
  desktopGutterMore: 24,
  desktopGutterLess: 8,
  desktopGutterMini: 4,
  desktopKeylineIncrement: 64,
  desktopDropDownMenuItemHeight: 32,
  desktopDropDownMenuFontSize: 15,
  desktopDrawerMenuItemHeight: 48,
  desktopSubheaderHeight: 48,
  desktopToolbarHeight: 60
};
