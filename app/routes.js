import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import LoginPage from './containers/HomePage';
import MainPage from './containers/Landing';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={LoginPage} />
    <Route name="login" path="login" component={LoginPage} />
    <Route name="land" path="land" component={MainPage} />
  </Route>
);
