import React from 'react';

import EABComponent from '../../Component';
const styles = require('../../Common.scss');
const companyIntroImg = require('../../../img/company_intro.jpg');

export default class Main extends EABComponent {

  constructor(props) {
    super(props);
  }

  render() {
    const {companyInfo} = this.context.store.getState().app;
    return (
      <div className={styles.CompanyIntro}>
        <div className={styles.IntroContainer}>
          <div
            className={styles.IntroImg}
            style={{
              backgroundImage: 'url(' + companyIntroImg + ')',
              backgroundSize: 'contain',
              backgroundPosition: 'center',
              backgroundRepeat: 'no-repeat'
            }}
          />
          <div className={styles.IntroText}>
            {companyInfo.companyIntro}
          </div>
        </div>
      </div>
    );
  }
}
