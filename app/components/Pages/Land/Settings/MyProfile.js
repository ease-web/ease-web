import React from 'react';
import {Avatar} from 'material-ui';

import EABComponent from '../../../Component';
import styles from '../../../Common.scss';
import {getIcon} from '../../../Icons/index';
import EABAvatar from '../../../CustomViews/Avatar';

import {updateAgentProfilePic} from '../../../../actions/agent';

export default class MyProfile extends EABComponent {

  constructor(props) {
    super(props);
  }

  getProfilePic() {
    const {attachments} = this.context.store.getState();
    return attachments.items.agentProfilePic;
  }

  render() {
    const {langMap, muiTheme, store} = this.context;
    const {agentProfile,lastUpdateDate} = store.getState().app;
    const {name, position} = agentProfile;
    let values = { agentProfilePic: this.getProfilePic() || 1 };
    return (
      <div className={styles.CardContainer}>
        <div style={{ margin: 'auto', width: '80%', maxWidth: 720 }}>
          <div style={{ fontSize: '24px', marginTop: 24, color: muiTheme.palette.primary1Color }}>
            {window.getLocalizedText(langMap, 'settings.my_profile')}
          </div>
          <div className={styles.Card}>
            <div style={{ display: 'flex', padding: '16px 0' }}>
              <div style={{ flexBasis: 84, display: 'flex', justifyContent: 'center' }}>
                <EABAvatar
                  docId={'UX_' + agentProfile.profileId}
                  template={{ id: 'agentProfilePic', type: 'photo' }}
                  width={56}
                  icon={getIcon('person', '#FFFFFF')}
                  values={values}
                  lastUpdateDate={lastUpdateDate}
                  changedValues={values}
                  handleChange={(id, lastModified) => {
                    let profilePic = this.getProfilePic();
                    let type = profilePic && profilePic.type;
                    let data = profilePic && profilePic.value;
                    updateAgentProfilePic(this.context, type, data, () => {
                      this.setState({});
                    });
                  }}
                  style={{ cursor: 'pointer' }}
                />
              </div>
              <div style={{ flexGrow: 1, display: 'flex', flexDirection: 'column' }}>
                <div className={styles.CardRowFieldLarge}>{name}</div>
                <div className={styles.CardRowFieldSmall}>{position}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
