import React from 'react';
import EABComponent from '../../Component';
import * as _ from 'lodash';

export default class Notifications extends EABComponent {

  render() {
      const { notifications } = this.props;
    return (
      <div>
        {
            _.map(notifications, notification => {
                return (
                    <div>{notification.message}</div>
                );
            })
        }
      </div>
    );
  };
}
