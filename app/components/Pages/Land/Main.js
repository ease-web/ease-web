import React from 'react';
import Events from 'material-ui/utils/events.js'

import Sliders from './Sliders';
import Menu from './Menu';
import ClientProfile from '../../Dialogs/ClientProfile';
import Search from './Search';
import Settings from './Settings/Settings';
import AboutEase from './AboutEase/AboutEase';
import WarningDialog from '../../Dialogs/WarningDialog';
import Notifications from './Notifications';

import {
  showErrorMsg
} from '../../../actions/GlobalActions';

import {readAllMessages} from '../../../actions/agent';

import Pos from '../Pos/index';

import EABComponent from '../../Component'

import * as _ from 'lodash';

import appTheme from '../../../theme/appBaseTheme.js';
import styles from '../../Common.scss';

import Constants from '../../../constants/SystemConstants'

import Approval from '../Pos/Approval/index';


class Main extends EABComponent {

  constructor(props) {
      super(props);

      this.state = Object.assign({}, this.state, {
        profile: {},
        editDialogTemplate: {},
        openApprovalPage: undefined,
        initOpenApprovalPage: undefined,
        notifications: [],
        showNotificationsDialog: false
      })
  };

  componentWillMount() {
      this.storeListener();
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    const {isFAFirm} = this.context.store.getState().app;
    //this.pos.open(0);
    let buttonItems;
    if (isFAFirm) {
      buttonItems = undefined;
    } else {
      buttonItems = [{
          type: 'iconButton',
          icon: 'search',
          action: () => {
            this.openClientView()
          }
        },{
          type: 'iconButton',
          icon: 'add',
          action: ()=>{
            this.clientProfile.openDialog({}, true);
          }
        }
      ];
    }
    
    this.context.initAppbar({
      menu: {
        icon: 'menu',
        action: ()=>{
          this.menu.open();
        }
      },
      title: getLocalizedText(this.context.langMap, 'introduction.title.page_1'),
      itemsIndex: 0,
      items:[
        buttonItems
      ]
    })

    let {
      agentProfile
    } = this.context.store.getState().app;

    // check the manager exists
    if (agentProfile.role != '04' && !agentProfile.managerCode) {
      showErrorMsg(this.context, {
        hasTitle: false,
        errorMsg: "You won't be able to submit case as there is no supervisor information linked to you. Please contact your firm's administrator."
      });
    }
  }

  componentWillUnmount() {

    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
        let newState = {};
        let {
          client,
          approval,
          app
        } = this.context.store.getState();
        let {
          profile,
          template
        } = client;

        if(!isEqual(template.editDialog, this.state.editDialogTemplate)){
          newState.editDialogTemplate = template.editDialog
        }

        if(!isEqual(approval.openApprovalPage, this.state.openApprovalPage)){
          newState.openApprovalPage = approval.openApprovalPage;
        }

        if(!isEqual(app.initOpenApprovalPage, this.state.initOpenApprovalPage)){
          newState.initOpenApprovalPage = app.initOpenApprovalPage;
        }

        if (!isEqual(app.showNotificationsDialog, this.state.showNotificationsDialog)) {
          newState.showNotificationsDialog = app.showNotificationsDialog;
          newState.notifications = app.notifications;
        }

        if(!isEmpty(newState)){
          this.setState(newState);
          this.curState = this.state;
        }
    }
  }

  openClientView = (viewAll) => {
    this.search.open(viewAll);
  };

  openSettings = () => {
    this.settings.open();
  };

  openAboutEase = () => {
    this.aboutEase.open();
  };

  openApproval = () => {
    let {langMap} = this.context;
    this.mainApproval.open(window.getLocalizedText(langMap, 'menu.eapproval.pendingforapproval', 'Pending for Approval'), 'A');
  };

  openWorkBench = () => {
    let {langMap} = this.context;
    this.mainApproval.open(window.getLocalizedText(langMap, 'menu.eapproval.workbench ', 'Workbench'), 'W');
  };

  shouldComponentUpdate(nextProp, nextState) {
    if (nextProp != this.props || nextState != this.curState) {
      return true;
    }
    return false;
  }

  handleClose = () => {
    readAllMessages(this.context);
  }

  render() {

    let {
      profile,
      editDialogTemplate,
      openApprovalPage,
      initOpenApprovalPage,
      showNotificationsDialog,
      notifications
    } = this.state;

    const {store} = this.context;
    const {approval: {currentTemplate}} = store.getState();

    if((openApprovalPage || initOpenApprovalPage) && currentTemplate !== 'W') {
      this.openApproval();
    }

    return (
      <div style={{ height: 'calc(100% - 64px)' }}>
        <Sliders/>
        <Menu ref={ref=>{this.menu=ref}} data={this.state.menu}
          openClientView={() => this.openClientView(true)} openSettings={this.openSettings} openApproval={this.openApproval} openWorkBench={this.openWorkBench} openAboutEase={this.openAboutEase} />
        <Search ref={ref=>{this.search=ref}} zIndex={300} onContactItemClick={()=>{this.pos.open(Constants.FEATURES.CP);}}/>
        <ClientProfile template={editDialogTemplate} isNew ref={ref=>{this.clientProfile=ref}}
          onSubmit={()=>{_.indexOf(_.get(this.context.store.getState(), 'app.agentProfile.features'), 'FNA') > -1 ? this.pos.open(Constants.FEATURES.FNA) :  this.pos.open(Constants.FEATURES.CP)}} />
        <Pos ref={ref=>{this.pos=ref}} zIndex={400}/>
        <Settings ref={ref=>{this.settings=ref}} />
        <AboutEase ref={ref=>{this.aboutEase=ref}} />
        <Approval key="MainApproval" ref={ref=>{this.mainApproval=ref}} />
        <WarningDialog
            open={showNotificationsDialog}
            hasTitle={false}
            msgElement={<Notifications notifications={notifications} />}
            onClose={this.handleClose}
        />
      </div>
    );
  }
}

export default Main;
