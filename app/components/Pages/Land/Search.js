import React from 'react';
import PropTypes from 'prop-types';
import {Tabs, Tab} from 'material-ui';
import * as _ from 'lodash';

import FloatingPage from '../FloatingPage';
import AppbarPage from '../AppbarPage';
import Appbar from '../../CustomViews/AppBar';

import SearchContact from './SearchContact';

import EABComponent from '../../Component';

import {updateContactList} from '../../../actions/client';
import styles from '../../Common.scss';

const initState = {
  slideIndex: 0
}

class Main extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, _.cloneDeep(initState));
  };

  componentDidMount() {
    //this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.context.updateAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          this.context.closePage();
        }
      }
    })
  }


  getChildContext() {
    return {
      goTab: this.handleChange
    }
  }

  handleChange = (value) => {
   this.setState({
     slideIndex: value,
   });
  }

  init = (viewAll) => {
    updateContactList(this.context, true, ()=>{
      if (viewAll) {
        this.refs.tab0.viewAll();
      }
      this.context.focusSearchValue();
    });
    this.context.updateSearchValue("");
    this.setState(_.cloneDeep(initState))
  }

  render() {
    let {muiTheme, pageOpen} = this.context;
    let {slideIndex} = this.state;

    if(!pageOpen){
      return <div/>
    }

    return (
      <div className={styles.Page}>
        <div className={styles.Tabs}>
          <Tabs
            key="profile-tabs"
            id={'Tabs'}
            onChange={this.handleChange}
            value={slideIndex}
          >
            <Tab key="tab-contact" label={<div className={styles.TabLabel}>Contacts</div>} value={0}/>
         </Tabs>
       </div>
        <SearchContact key="context-contact" ref="tab0"/>
      </div>
    );
  }

}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  goTab: PropTypes.func
})

class Search extends EABComponent {

  getChildContext() {
    return {
      onContactItemClick: this.props.onContactItemClick
    }
  }

  open=(viewAll)=>{
    this.floatingPage.open();
    this.main.init(viewAll);
  }

  close=()=>{
    this.floatingPage.close();
  }

  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage ref={(ref)=>{this.searchClientAppbar=ref}} search showShadow={false}>
          <Main ref={(ref)=>{this.main=ref}}/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

Search.childContextTypes = Object.assign({}, Search.childContextTypes, {
  onContactItemClick: PropTypes.func
});


export default Search;
