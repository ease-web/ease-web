import React from 'react';

import EABComponent from '../../../Component';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

import styles from '../../../Common.scss';
import eab from "../../../../img/eab.png";

export default class AboutEase extends EABComponent {

  open() {
    this.updateAppbar();
    this.floatingPage.open();
  }

  close() {
    this.floatingPage.close();
  }

  updateAppbar() {
    const {langMap} = this.context;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'back',
        action: () => {
          this.close();
        }
      },
      title: window.getLocalizedText(langMap, 'about.title')
    });
  }

  render() {
    const {langMap} = this.context;
    return (
      <FloatingPage ref={ref => { this.floatingPage = ref; }}>
        <AppbarPage ref={ref => { this.appbarPage = ref; }}>           
          <div className={styles.easeContainer}>
              <div className={styles.logoContainer}>
                  <div><img src={eab} alt="eab" /></div>
              </div>
              <div>
                  <p><span className={styles.easeContent} style={{fontSize:18}} >EASE (Electronic AXA Submission Experience) is powered by EAB Systems’ 121 System.</span></p>
                  <p><span className={styles.easeContent}>Copyright EAB Systems (Hong Kong) Limited. All rights reserved.</span></p>
                  <p>121 System is the property of EAB Systems (Hong Kong) Limited and is protected by copyright and other intellectual property laws and treaties. 121 System is licensed and may not be used without permission. All rights in and title to 121 System are expressly reserved by EAB Systems (Hong Kong) Limited.</p>
                  <p>121 System is an integrated sales support system that provides an effective solution for your sales force and customer needs. Product modules include Financial Needs Analysis, Policy Illustration, Application (Proposal Submission and Approval with eSignature), Workbench, Reports, Needs Configurator, Product Configurator, Campaign Management, Channel Management and Audit/Analytics. 121 System is scalable and extensible, and can operate online or offline on multiple platforms including iOS and the web.</p>
              </div>
          </div>
        </AppbarPage>
      </FloatingPage>
    );
  }

}
