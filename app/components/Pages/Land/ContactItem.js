import React from "react";
import {Card} from "material-ui";

import {getIcon} from '../../Icons/index';
import EABAvatar from '../../CustomViews/Avatar';
import EABComponent from '../../Component';

import * as _c from '../../../utils/client';

import styles from "../../Common.scss";

class ContactItem extends EABComponent {
  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(this.props.item, nextProps.item)) {
      return false;
    }
    return true;
  }

  render(){
    let {store, lang} = this.context;
    let {template} = store.getState().client;
    let subFontColor = "DDDDDD";
    let subRowStyle = {color:subFontColor, fontSize: '12px', lineHeight: '16px'};
    let {item} = this.props;
    let {id, fullName='', mobileNo='', idDocType='', idDocTypeOther='', idCardNo='', email='', applicationCount=0, photo} = item;
    let idDocTypeTitle = (idDocType === 'other' && idDocTypeOther) || _c.getIdDocTypeTitle(template.editDialog, item, lang);
    var initial = _c.getAvatarInitial(item);
    return (
      <div style={{display: 'flex', padding: '16px 0px', cursor: 'pointer'}} onTouchTap={()=>{this.props.onItemClick(id)}}>
        <div style={{flexBasis: 84, display: 'flex', justifyContent: 'center'}}>
          <EABAvatar width={56} docId={id} changedValues={{photo: photo?'photo':"", initial: initial}} values={{photo: photo?'photo':"", initial: initial}} template={{id: 'photo', type: 'photo'}} disabled icon={getIcon("person", "#FFFFFF")}/>
        </div>
        <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
          <div className={styles.CardRowFieldLarge}>{fullName}</div>
          {idDocType?<div className={styles.CardRowFieldSmall}>{idDocTypeTitle}: {idCardNo}</div>:null}
          <div className={styles.CardRowFieldSmall}>{mobileNo}</div>
          <div className={styles.CardRowFieldSmall}>{email}</div>
        </div>
        <div style={{flexBasis: 120, display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'flex-end', marginRight: '24px'}}>
          <div style={{fontSize: '32px', lineHeight: '40px'}}>{applicationCount}</div>
          <div className={styles.CardRowFieldSmall}>applications</div>
        </div>
      </div>
    )
  }
}

export default ContactItem;
