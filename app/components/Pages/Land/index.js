import React, { PropTypes } from 'react';
import Events from 'material-ui/utils/events.js';

import EABComponent from '../../Component';
import FloatingPage from '../FloatingPage';
import AppbarPage from '../AppbarPage';
import Main from './Main';
import appTheme from '../../../theme/appBaseTheme';

import {logout} from '../../../actions/GlobalActions';

export default class Land extends EABComponent {

  static childContextTypes = {
    optionsMap: PropTypes.object,
    langMap: PropTypes.object,
    lang: PropTypes.string,
    muiTheme: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      isLogin: false,
      langMap: {},
      lang: 'en',
      muiTheme: appTheme.getTheme()
    });
  }

  getChildContext() {
    return {
      optionsMap: this.state.optionsMap,
      langMap: this.state.langMap,
      lang: this.state.lang,
      muiTheme: this.state.muiTheme
    }
  }

  componentDidMount() {
    this._handleResize();
    Events.on(window, 'resize', this._handleResize.bind(this));
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
    this.floatingPage.open();
  }

  componentWillMount() {
    let {
      app
    } = this.context.store.getState();

    if (!app.isLogin) {
       this.context.router.history.replace('/');
    }
  }

  componentWillUnmount() {
    Events.off(window, 'resize', this._handleResize.bind(this));

    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
    logout(this.context);
  }

  storeListener = () =>{
    if (this.unsubscribe){
      let {optionsMap, langMap, lang} = this.context.store.getState().app;
      let newState = {};

      if(!isEqual(this.state.optionsMap, optionsMap) && !isEmpty(optionsMap)){
        newState.optionsMap = optionsMap;
      }

      if(!isEqual(this.state.langMap, langMap) && !isEmpty(langMap)){
        newState.langMap = langMap;
      }

      if(!isEqual(this.state.lang, lang) && !isEmpty(lang)){
        newState.lang = lang;
      }

      if(!isEmpty(newState))
        this.setState(newState);
    }
  }

  _handleResize=()=>{
    if (this.unsubscribe) {
      this.setState({
        muiTheme: appTheme.getTheme(true),
      });
    }
  }

  render() {
    let {
      app
    } = this.context.store.getState();

    if (app.isLogin) {
      return (
        <FloatingPage ref={ref => {this.floatingPage = ref;}}>
          <AppbarPage id="mainAppbar">
            <Main />
          </AppbarPage>
        </FloatingPage>
      );
    } else {
      return null;
    }
  }
}
