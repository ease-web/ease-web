import React from 'react';
import EABInputComponent from '../CustomViews/EABInputComponent';
import PropTypes from 'prop-types';
import mui, {FlatButton, Step, Stepper, StepButton, StepLabel} from 'material-ui';
import * as _ from 'lodash';

import styles from '../Common.scss';
import {getIcon} from '../Icons/index';

class StepperPage extends EABInputComponent {
  constructor(props) {
      super(props);
      this.state = {
        index: 0,
        active: 0,
        completed: -1,
        headers:[],
        onSave: null,
        onSaveChangeStep: null,
        onGoStepComplete: null
      };
  };

  getChildContext() {
    return {
      goStep: this.goStep,
      stepperIndex: this.state.index,
      updateStepper: this.updateStepper
    }
  }

  updateStepper=(stepper)=>{
    let newState = Object.assign(this.state, stepper);
    if(stepper.completed>-1 && stepper.completed != newState.headers.length -1){
      if(_.get(stepper, `headers[${stepper.completed+1}].disabled`)){
        newState.active=stepper.completed;
      }
      else {
        newState.active=stepper.completed+1;
      }
      
    }
    if(stepper.completed === -1){
      newState.active = 0;
    }
    this.setState(Object.assign(this.state, stepper));
  }

  goStep=(targetIndex, isComplete)=>{
    let {active, index, onSave, onSaveChangeStep, completed, headers, onGoStepComplete, validStep} = this.state;

    //do nothing if target index = current index
    if(targetIndex === index)
      return;
    
    //validate
    // let validateFunc = headers[index].validate;
    // let valid = false;
    // if(_.isFunction(validateFunc)){
    //   valid = validateFunc();
    // }

    // if(valid === true){
      let newState={index: targetIndex};

      if(_.isNumber(validStep) && targetIndex > completed && validStep > completed){
        completed = completed + 1;
        newState.completed = completed;
        if(headers.length > completed){
          if(completed + 1 < headers.length && !_.get(headers[completed + 1], "disabled")){
            active = completed + 1;
          }
          else {
            active = completed;
          }
          newState.active = active;
        }
      }

      //control which step is active
      if(isComplete && index > active){
        newState.active = targetIndex;
      }
      else if(targetIndex > active){
        newState.active = targetIndex;
      }

      //control which step is completed
      if(isComplete && index > completed){
        newState.completed = index;
      }

      let exec = () =>{
        let _exec2 = () =>{
          if(_.isFunction(onGoStepComplete)){
            onGoStepComplete(newState.index);
          }
        }
        this.setState(newState, _exec2);
      }

      if(_.isFunction(onSave)){
        onSave(exec)
      }else if (_.isFunction(onSaveChangeStep)) {
        onSaveChangeStep(newState.index, exec);
      }else{
        exec();
      }

      // if(_.isFunction(onGoStepComplete)){
      //   onGoStepComplete(newState.index);
      // }
    // }
    // else{
    //   this.setState({errorMsg: getLocalText(this.context.lang, valid.errorMsg)});
    // }
    
  }

  getHeaders=()=>{
    let {headers, index, active, completed, onSave} = this.state;
    let { lang, muiTheme } = this.context;
    let iconColor = muiTheme.palette.primary1Color;
    let inactiveColor = muiTheme.palette.accent3Color;

    let CompleteIcon = getIcon('checkCircle', iconColor);
    let activeIcon =  getIcon('lens', iconColor);
    let inactiveIcon =  getIcon('lens', inactiveColor);
    let StepIcon = (label, Icon) => {
      return  (
        <div style={{ position: 'relative' }}>
          {Icon}
          <div className={styles.LensIconText}>{label}</div>
        </div>
      )
    }
    
    let stepperHeaders = [];
    let icon;
    _.forEach(headers, (header, index)=>{
      let {id, title, disabled} = header;
      (index<=active) ? icon = activeIcon : icon = inactiveIcon;
      stepperHeaders.push(
        <Step 
          key={`step-${id}`} 
          disabled={disabled || index>active} 
          active={index<=active} 
          completed={index<=completed}
        > 
          <StepButton
            icon={(index<=completed) ? CompleteIcon : StepIcon(index + 1, icon)}
            onClick={()=>{
              if(index<=active){
                this.goStep(index);
              }
            }}
          >         
          {getLocalText(lang, title)}
          </StepButton>
        </Step>
      )
    });
   
    return stepperHeaders;
  }

  render() {
    let {
      active,
      errorMsg
    } = this.state;

    let headers = this.getHeaders();

    return (
      <div className={styles.FlexColumnNoWrapBasicContainer} style={{height: '100%'}}>
        <div className={styles.Stepper}>
          <Stepper activeStep={active}>
            {headers}
          </Stepper>
        </div>
        <div className={styles.Divider}></div>
        {this.props.children}
      </div>

    );
  }
}

StepperPage.contextTypes={
  router: PropTypes.object,
  store: PropTypes.object,
  muiTheme: PropTypes.object,
  langMap: PropTypes.object,
  lang: PropTypes.string
}

StepperPage.childContextTypes = {
  goStep: PropTypes.func,
  stepperIndex: PropTypes.number,
  updateStepper: PropTypes.func
}

export default StepperPage;
