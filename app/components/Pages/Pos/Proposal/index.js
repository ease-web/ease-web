import React, {PropTypes} from 'react';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import AppbarPage from '../../AppbarPage';
import FloatingPage from '../../FloatingPage';
import Illustration from './Illustration';
import Proposal from './Proposal';
import EmailDialog from '../../../Dialogs/EmailDialog';
import WarningDialog from '../../../Dialogs/WarningDialog';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';

import {closeProposal, sendEmail, closeEmail, closeErrorMsg, closeConfirmMsg} from '../../../../actions/proposal';

export default class ProposalPage extends EABComponent {

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    goTab: PropTypes.func
  });

  static childContextTypes = {
    onCloseProposal: PropTypes.func
  };

  getChildContext() {
    return {
      onCloseProposal: this.onCloseProposal
    };
  }

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      pageId: null,
      pdfData: null,
      illustrateData: null,
      quotation: null,
      planDetails: null,
      canRequote: false,
      canRequoteInvalid: false,
      canClone: false,
      canEmail: false,
      emailOpen: false,
      emails: null,
      errorMsg: null,
      confirmMsg: null,
      onConfirm: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  storeListener = () => {
    const {store} = this.context;
    const proposal = store.getState().proposal;
    let newState = {};
    _.each(this.state, (val, key) => {
      if (!_.isEqual(val, proposal[key])) {
        newState[key] = proposal[key];
      }
    });
    if (!_.isEmpty(newState)) {
      this.setState(newState, () => {
        this.onChangePage(proposal.pageId);
        if (proposal.pageId) {
          this.floatingPage.open();
        } else {
          this.floatingPage.close();
        }
      });
    }
  };

  onCloseProposal = () => {
    closeProposal(this.context);
  }

  onChangePage = (pageId) => {
    if (pageId) {
      this.appbarPage.initAppbar(this[pageId].getAppbarData());
    }
  }

  onSend(emails) {
    sendEmail(this.context, emails);
  }

  onClose() {
    closeEmail(this.context);
  }

  render() {
    const {
      pageId,
      pdfData,
      illustrateData,
      quotation,
      planDetails,
      canRequote,
      canRequoteInvalid,
      canClone,
      canEmail,
      isCreateProposal,
      errorMsg,
      confirmMsg,
      onConfirm,
      emailOpen,
      emails
    } = this.state;
    let bpDetail = planDetails && quotation && planDetails[quotation.baseProductCode];
    let canIllustrate = bpDetail && bpDetail.supportIllustration === 'Y';
    return (
      <FloatingPage ref={ref=>{this.floatingPage = ref;}}>
        <AppbarPage ref={ref=>{this.appbarPage = ref;}}>
          {
            pageId === 'proposal' ? (
              <Proposal
                ref={ref=>{this.proposal = ref;}}
                pdfData={pdfData}
                canRequote={canRequote}
                canRequoteInvalid={canRequoteInvalid}
                canClone={canClone}
                canIllustrate={canIllustrate}
                canEmail={canEmail}
                isCreateProposal={isCreateProposal}
                quickQuote={quotation && quotation.quickQuote}
              />
            ) :
            pageId === 'illustration' ? (
              <Illustration
                ref={ref=>{this.illustration = ref;}}
                illustrateData={illustrateData}
                quotation={quotation}
                planDetails={planDetails}
              />
            ) : null
          }
          <input id="qid" type="hidden" value={(quotation && quotation.id) || ''}/>
          <EmailDialog
            open={emailOpen}
            emails={emails}
            onSend={(emails) => this.onSend(emails)}
            onClose={() => this.onClose()}
          />
          <WarningDialog
            open={!!errorMsg}
            isError
            msg={errorMsg}
            onClose={() => closeErrorMsg(this.context)}
          />
          <ConfirmDialog
            open={!!confirmMsg}
            confirmMsg={confirmMsg}
            onClose={() => closeConfirmMsg(this.context)}
            onConfirm={() => onConfirm && onConfirm()}
          />
        </AppbarPage>
      </FloatingPage>
    );
  }

}
