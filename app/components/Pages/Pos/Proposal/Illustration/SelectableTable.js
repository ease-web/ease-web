import React, {PropTypes} from 'react';
import * as _ from 'lodash';

import styles from '../../../../Common.scss';
import EABComponent from '../../../../Component';

export default class SelectableTable extends EABComponent {

  static propTypes = {
    colNames: PropTypes.array,
    rowNames: PropTypes.array,
    data: PropTypes.array,
    currencySign: PropTypes.string,
    selectedRow: PropTypes.number,
    onSelectRow: PropTypes.func
  };

  componentDidUpdate(prevProps) {
    const {selectedRow} = this.props;
    if (selectedRow !== prevProps.selectedRow) {
      const {table, tableContainer} = this;
      const tr = table.children && table.children[selectedRow];
      if (tr && (
        tr.offsetTop < tableContainer.scrollTop ||
        tr.offsetTop > tableContainer.scrollTop + tableContainer.offsetHeight - tr.offsetHeight
      )) {
        tableContainer.scrollTop = tr.offsetTop - tableContainer.offsetHeight / 2;
      }
    }
  }

  _getHeaders() {
    const {colNames} = this.props;
    return _.map(colNames, (title, colIndex) => {
      return (
        <td
          key={colIndex}
          style={{
            width: colIndex === 0 ? '10%' : '30%',
            textAlign: colIndex === 0 ? 'left' : 'right'
          }}
        >
          {title}
        </td>
      );
    });
  }

  _getRows() {
    const {rowNames, data, selectedRow, onSelectRow, currencySign} = this.props;
    return _.map(data, (row, rowIndex) => {
      return (
        <tr
          key={rowIndex}
          className={selectedRow === rowIndex ? styles.active : null}
          onTouchTap={() => onSelectRow && onSelectRow(rowIndex)}
        >
          <td style={{ width: '10%', textAlign: 'left' }}>
            {rowNames[rowIndex]}
          </td>
          {_.map(row, (val, colIndex) => {
            return (
              <td key={'col-' + rowIndex + '-' + colIndex} style={{ width: '30%', textAlign: 'right' }}>
                {window.getCurrency(val, currencySign, 0)}
              </td>
            );
          })}
        </tr>
      );
    });
  }

  render() {
    return (
      <div className={styles.IllChartTable}>
        <div className={styles.ChartTableHeader}>
          <table className={styles.headerTable}>
            <tr>{this._getHeaders()}</tr>
          </table>
        </div>
        <div ref={ref => { this.tableContainer = ref; }} className={styles.ChartTableContent}>
          <table ref={ref => { this.table = ref; }}>{this._getRows()}</table>
        </div>
      </div>
    );
  }
}
