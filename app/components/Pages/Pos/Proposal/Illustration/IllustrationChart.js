import React, {PropTypes} from 'react';
import * as _ from 'lodash';

import styles from '../../../../Common.scss';
import EABComponent from '../../../../Component';

const Chart = require('chart.js');
Chart.defaults.global.pointHitDetectionRadius = 1;

export default class IllustrationChart extends EABComponent {

  static propTypes = {
    colNames: PropTypes.array,
    rowNames: PropTypes.array,
    data: PropTypes.array,
    currencySign: PropTypes.string,
    selectedRow: PropTypes.number,
    onSelectRow: PropTypes.func,

    chartColors: PropTypes.array,
    bubbleTxtSize: PropTypes.number,
    chartHeight: PropTypes.number
  };

  static defaultProps = {
    chartColors: ['rgba(0,79,181,1)', 'rgba(89,179,55,1)', 'rgba(243,146,0,1)'],
    bubbleTxtSize: 20,
    chartHeight: 200
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedRow: props.selectedRow
    };
  }

  _getChartWidth() {
    return this.container.offsetWidth;
  }

  componentDidMount() {
    const iframe = document.createElement('iframe');
    iframe.width = '100%';
    iframe.height = '0';
    iframe.style.border = 0;
    iframe.onload = () => {
      iframe.contentWindow.onresize = () => {
        const {data, rowNames} = this.props;
        const {selectedRow} = this.state;
        this.drawChart();
        this.drawBar(selectedRow / (data.length - 1) * this._getChartWidth(), rowNames[selectedRow]);
      };
    };
    this.container.appendChild(iframe);

    this.barCanvas.addEventListener('touchstart', (event) => {
      var rect = event.target.getBoundingClientRect();
      var x = event.targetTouches[0].pageX - rect.left;
      x = Math.max(0, x);
      x = Math.min(x, this._getChartWidth());
      this.onClickCanvas(x);
      event.preventDefault();
      event.stopPropagation();
      return false;
    });

    this.barCanvas.addEventListener('touchmove', (event) => {
      var rect = event.target.getBoundingClientRect();
      var x = event.targetTouches[0].pageX - rect.left;
      x = Math.max(0, x);
      x = Math.min(x, this._getChartWidth());
      this.onClickCanvas(x);
      event.preventDefault();
      event.stopPropagation();
      return false;
    });

    this.barCanvas.addEventListener('mousedown', (event) => {
      this.isPressed = true;
      this.onClickCanvas(event.offsetX);
    });
    this.barCanvas.addEventListener('mousemove', (event) => {
      if (this.isPressed) {
        this.onClickCanvas(event.offsetX);
      }
    });
    this.barCanvas.addEventListener('mouseup', (event) => {
      this.isPressed = false;
    });
    this.barCanvas.addEventListener('mouseout', (event) => {
      this.isPressed = false;
    });

    const {data, selectedRow, rowNames} = this.props;
    this.drawChart();
    this.drawBar(selectedRow / (data.length - 1) * this._getChartWidth(), rowNames[selectedRow]);
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      selectedRow: newProps.selectedRow
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const {data, rowNames} = this.props;
    const {selectedRow} = this.state;
    if (!_.isEqual(prevProps.data, data)) {
      this.drawChart();
    }
    if (!_.isEqual(prevState.selectedRow, selectedRow)) {
      this.drawBar(selectedRow / (data.length - 1) * this._getChartWidth(), rowNames[selectedRow]);
    }
  }

  onClickCanvas(x) {
    const {rowNames, data, onSelectRow} = this.props;
    const selectedRow = Math.floor((data.length - 1) * x / this._getChartWidth());
    this.setState({
      selectedRow: selectedRow
    }, () => {
      this.drawBar(x, rowNames[selectedRow]);
      onSelectRow && onSelectRow(selectedRow);
    });
  }

  drawChart() {
    const {rowNames, data, chartColors, chartHeight} = this.props;
    const colValueList = [];
    _.each(data, (illData, rowIndex) => {
      _.each(illData, (val, colIndex) => {
        if (rowIndex === 0) {
          colValueList.push([]);
        }
        colValueList[colIndex].push(val);
      });
    });
    const datasets = _.map(colValueList, (colValues, colIndex) => {
      return {
        fill: false,
        backgroundColor: chartColors[colIndex],
        borderWidth: 2,
        borderColor: chartColors[colIndex],
        borderDash: [],
        borderDashOffset: 0.0,
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointHoverBackgroundColor: 'rgba(0,0,0,1)',
        pointHoverBorderColor: 'rgba(0,0,0,1)',
        pointHoverBorderWidth: 0,
        pointRadius: 0,
        pointHitRadius: 0,
        data: colValues
      };
    });
    const canvas = this.chartCanvas;
    canvas.width = this._getChartWidth();
    canvas.height = chartHeight;
    const ctx = canvas.getContext('2d');
    return new Chart(ctx, {
      type: 'line',
      data: {
        labels: rowNames,
        datasets: datasets
      },
      options: {
        scales: {
          xAxes: [{ display: false }],
          yAxes: [{ display: false }],
        },
        title: { display: false },
        legend: { display: false },
        responsive: false
      }
    });
  }

  roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke === 'undefined') {
      stroke = true;
    }
    if (typeof radius === 'undefined') {
      radius = 5;
    }
    if (typeof radius === 'number') {
      radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
      var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
      for (var side in defaultRadius) {
        radius[side] = radius[side] || defaultRadius[side];
      }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
      ctx.fill();
    }
    if (stroke) {
      ctx.stroke();
    }
  }

  drawBar(newX, age) {
    const cvs = this.barCanvas;
    const {bubbleTxtSize, chartHeight} = this.props;

    cvs.width = this._getChartWidth();
    cvs.height = chartHeight;

    const cvsHeight = cvs.height;
    const context = cvs.getContext('2d');
    const barWidth = 12.5;
    const barHeight = chartHeight * 0.75;

    context.clearRect(0, 0, cvs.width, cvsHeight);

    context.fillStyle = 'rgba(150, 150, 150, .15)';
    context.globalAlpha = 0.75;
    let barXBgn = Math.max(newX, 0);
    let isBeyondRight = false;
    if (barXBgn + barWidth / 2 >= cvs.width) {
      barXBgn = cvs.width - barWidth;
      isBeyondRight = true;
    }

    context.fillRect(barXBgn, cvsHeight - barHeight, barWidth, barHeight);

    context.globalAlpha = 1;
    const bubbleYOffset = 10;
    const bubbleBoxWidth = 60;
    const bubbleBoxHeight = bubbleBoxWidth * 0.6;
    let bubbleBoxXBgn = barXBgn + (barWidth - bubbleBoxWidth) / 2;

    bubbleBoxXBgn = Math.max(bubbleBoxXBgn,0);
    if (isBeyondRight) {
      bubbleBoxXBgn = cvs.width - bubbleBoxWidth;
    }
    const bubbleBoxRound = 5;
    const barYBgn = (cvsHeight - barHeight) - bubbleYOffset;
    context.strokeStyle = 'rgba(0, 0, 0, 0)';
    context.fillStyle = 'rgba(150, 150, 150, .15)';
    context.lineWidth = 0.001;
    this.roundRect(context, bubbleBoxXBgn, barYBgn - bubbleBoxHeight, bubbleBoxWidth, bubbleBoxHeight, bubbleBoxRound, true);

    const triangleWidth = barWidth * 0.5;
    context.lineWidth = 0.001;
    context.beginPath();
    context.moveTo(barXBgn + (barWidth - triangleWidth) / 2 , barYBgn);
    context.lineTo(barXBgn + (barWidth - triangleWidth) / 2 + triangleWidth / 2, barYBgn + bubbleYOffset);
    context.lineTo(barXBgn + (barWidth - triangleWidth) / 2 + triangleWidth, barYBgn);
    context.closePath();
    context.fill();

    context.font = bubbleTxtSize + 'px Arial';
    context.fillStyle = 'black';
    context.globalAlpha = 1;
    const txtLen = context.measureText(age).width;
    context.fillText(age, bubbleBoxXBgn + (bubbleBoxWidth - txtLen) / 2, barYBgn - (bubbleBoxHeight - bubbleTxtSize) / 2);
  }

  _getChartLabels() {
    const {colNames, data, chartColors, currencySign} = this.props;
    const {selectedRow} = this.state;
    const valEls = _.map(data && data[selectedRow], (val, index) => {
      return (
        <div className={styles.IllHeaderItem}>
          <div className={styles.IllHeaderItemVal}>{window.getCurrency(val, currencySign, 0)}</div>
          <div className={styles.IllHeaderItemName}>
            <font style={{ color: chartColors[index] }}>{colNames[index + 1]}</font>
          </div>
        </div>
      );
    });
    return (
      <div className={styles.IllHeaderRight}>
        {valEls}
      </div>
    );
  }

  render() {
    const {langMap} = this.context;
    const {rowNames, chartHeight} = this.props;
    const {selectedRow} = this.state;
    return (
      <div
        ref={ref => { this.container = ref; }}
        className={styles.IllChartContainer}
        style={{ overflow: 'hidden'}}
      >
        <div className={styles.IllChartHeader}>
          <div className={styles.IllHeaderLeft}>
            <div className={styles.IllRow}>
              <div className={styles.IllRowItem}>
                <font className={styles.IllHeaderAge}>
                  {rowNames[selectedRow]}
                </font>
                <font className={styles.IllHeaderAgeUnit}>
                  {window.getLocalizedText(langMap, 'benefitIllustration.chart.age')}
                </font>
              </div>
            </div>
          </div>
          {this._getChartLabels()}
        </div>
        <div style={{ position: 'relative', width: '100%', height: chartHeight, margin: '0 auto' }}>
          <canvas
            ref={ref => { this.chartCanvas = ref; }}
            style={{ zIndex: 0 }}
          />
          <canvas
            ref={ref => { this.barCanvas = ref; }}
            style={{ position: 'absolute', left: 0, top: 0, zIndex: 1 }}
          />
        </div>
      </div>
    );
  }

}
