import React, { PropTypes } from 'react';
import EABComponent from '../../../Component';

import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

import DynColumn from '../../../DynViews/DynColumn';
import Main from './main';

import styles from '../../../Common.scss';

import {getPendingForApprovalCaseListLength, resetFilter, openApprovalPage, resetFilterAndCloseApprovalPage} from '../../../../actions/approval';
import _isEqual from 'lodash/fp/isEqual';

import WarningDialog from '../../../Dialogs/WarningDialog';

import AppForm from '../Applications/AppForms';

import SupportDocPage from '../Applications/UploadDocument/index';

import * as _ from 'lodash';

export default class approval extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
          showPendingForApprovalDialog: false,
          waitingForPendingApprovalCount: 0,
          currentPage: '',
          openApplication: false,
          openSuppDocsPage: false
        });
    }

    componentDidMount(){
      this.unsubscribe = this.context.store.subscribe(this.storeListener);
      this.storeListener();
  }

    storeListener= ()=>{
      if(this.unsubscribe){
        let {store} = this.context;
        let { pos} = store.getState();
        let newState = {};

        if(pos.currentPage && this.state.currentPage !== pos.currentPage){
          newState.currentPage =  pos.currentPage;
        }

        if(!_.isEqual(this.state.openSuppDocsPage, pos.openSuppDocsPage)) {
            newState.openSuppDocsPage = pos.openSuppDocsPage;
        }

        if(!window.isEmpty(newState)){
          this.setState(newState);
        }
      }
  }

  componentWillUnmount(){
      if(isFunction(this.unsubscribe)){
          this.unsubscribe();
          this.unsubscribe = undefined;
      }
  }

    open(title, action) {
        this.updateAppbar(title);
        this.main.init(action);
        this.floatingPage.open();
        this.setState({
          openApplication: true
        })
    }

    close() {
        this.floatingPage.close();
        resetFilterAndCloseApprovalPage(this.context, false);
        this.setState({
          openApplication: false
        });
    }

    handleClose = () => {
        this.setState({showPendingForApprovalDialog: false});
    };

    onClickApproval = () => {
        openApprovalPage(this.context, true);
        this.setState({
            showPendingForApprovalDialog: false
        });
    };

    componentWillMount() {
        const {app} = this.context.store.getState();
        const {agentProfile} = app;
        getPendingForApprovalCaseListLength(this.context, (length) => {
            if (_isEqual(length, 0) || !agentProfile.allowToEnterApproval) {
                return;
            }
            this.setState({
                showPendingForApprovalDialog: true,
                waitingForPendingApprovalCount: length
            });
        });
    }

    updateAppbar(title) {
        const {langMap} = this.context;
        this.appbarPage.initAppbar({
        menu: {
            icon: 'back',
            action: () => {
            this.close();
            }
        },
        title: title,
        itemsIndex: 0,
        items:[
            [
                {
                    id: 'resetFilter',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.resetfilter', 'Reset All Filter'),
                    action: ()=>{
                        resetFilter(this.context);
                    }
                }
            ]
        ]
        });
    }
    render() {
      let {currentPage, openApplication, openSuppDocsPage} = this.state;
      let renderItem;
      let secItem, supportDoc;
      if(openSuppDocsPage && openApplication){
        supportDoc = <SupportDocPage isShield={this.context.store.getState().pos.isShield || false}/>;
      }
        renderItem = (<FloatingPage ref={ref => { this.floatingPage = ref; }}>
                    <AppbarPage ref={ref => { this.appbarPage = ref; }}>
                        <Main ref={ref => {this.main = ref;}} openApplication={openApplication}/>
                        {supportDoc}
                    </AppbarPage>
                    <WarningDialog
                        open={this.state.showPendingForApprovalDialog}
                        hasTitle={false}
                        msgElement={<PendingForApprovalCountMessage waitingForPendingApprovalCount={this.state.waitingForPendingApprovalCount} onClickApproval={this.onClickApproval} />}
                        onClose={this.handleClose}
                    />
                </FloatingPage>)

        return (
          <div>{renderItem}</div>
        );
    }
}

class PendingForApprovalCountMessage extends EABComponent {

    static propTypes = {
        waitingForPendingApprovalCount: PropTypes.number,
        onClickApproval: PropTypes.func
    };

    render() {
        const { waitingForPendingApprovalCount, onClickApproval } = this.props;
        return (
            <div>
                You have {waitingForPendingApprovalCount} case(s) pending for approval, please <a href="#" onClick={onClickApproval}>click here</a> to view.
            </div>
        );
    }
}
