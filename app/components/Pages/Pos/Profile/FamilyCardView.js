import React  from 'react';
import {IconButton} from 'material-ui';

import {getIcon} from '../../../Icons/index';
import * as _ from 'lodash';

import styles from '../../../Common.scss';
import ClientProfile from '../../../Dialogs/ClientProfile';
import {getFamilyProfile} from '../../../../actions/client';
import EABComponent from '../../../Component';
import EABAvatar from '../../../CustomViews/Avatar';

import {getAllDependantsProfile} from '../../../../actions/client';
import {getAvatarInitial} from '../../../../utils/client';

class FamilyMemberItem extends EABComponent{

  getRelationship=(dependant)=>{
    let {relationship, relationshipOther} = dependant;
    let {optionsMap, langMap, lang} = this.context;
    let {options} = optionsMap.relationship;

    if(relationship === 'OTH'){
      return relationshipOther;
    }
    else {
      let opt = options.find(option=>option.value === relationship) || '';
      return getLocalText(lang, opt && opt.title)
    }
  }

  handleItemClick=()=>{
    let {profile={}, dependant, onItemClick} = this.props;    
    let {relationship, relationshipOther} = dependant;
    profile.relationship = relationship;
    profile.relationshipOther = relationshipOther;

    onItemClick(_.cloneDeep(profile), false);
  }

  render(){
    let {profile={}, dependant} = this.props;
    let {cid, fullName='', photo} = profile;
    let relationshipTitle = this.getRelationship(dependant);
    let {muiTheme} = this.context;
    let iconColor = muiTheme.palette.primary1Color;
    let names = fullName.split(" ");
    var initial = getAvatarInitial(profile);

    return(
      <div className={styles.CardRow} onTouchTap={this.handleItemClick}>
        <div className={styles.Avatar}>
          <EABAvatar width={56} docId={cid} changedValues={{photo, initial}} values={{photo, initial}} template={{id:'photo', type: 'photo'}} disabled icon={getIcon('person', iconColor)}/>
        </div>
        <div className={styles.CardRowSingleColumn}>
          <div className={styles.CardRowFieldLarge}>
            <span>{fullName}</span> 
            { !relationshipTitle ? <span>{ getIcon('warning', 'red') }</span> : null }
          </div>
          <div className={styles.CardRowFieldSmall}>{relationshipTitle}</div>
        </div>
      </div>
    )
  }
}

class FamilyCardView extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        template: {},
        dependantProfiles: {},
        dependants:[]
      })
  };

  componentWillMount() {
    getAllDependantsProfile(this.context);
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener = () =>{
    if(this.unsubscribe){
      let {template, profile, dependantProfiles} = this.context.store.getState().client;
      let newState = {};
      let isUpd = false;

      if(profile){
        if(!isEqual(this.state.dependants, profile.dependants)){
          newState.dependants = profile.dependants;
          isUpd = true;
        }
      }
      
      if(!isEqual(this.state.dependantProfiles, dependantProfiles)){
        newState.dependantProfiles = dependantProfiles;
        isUpd = true;
      }


      if(!isEqual(this.state.template, template.familyEditDialog)){
        newState.template = template.familyEditDialog;
        isUpd = true;
      }
      
      if(isUpd)
        this.setState(newState);
    }
  }

  renderItems = () =>{
    let {muiTheme, langMap, lang} = this.context;
    let {dependants, dependantProfiles} = this.state;

    let items = [];

    _.forEach(dependants, (dependant, index)=>{
      items.push(
        <FamilyMemberItem 
          key={`family-member-item-${index}`}
          profile={dependantProfiles[dependant.cid]} 
          dependant={dependant} 
          onItemClick={this.openProfileDialog} 
        />
      )
    })

    return items;
  }

  openProfileDialog=(profile, isNew)=>{
    this.clientProfile.openDialog(profile, isNew);
  }

  render(){
	  let {muiTheme, langMap, lang} = this.context;
    let {template} = this.state;
    let {style} = this.props;

    let items = this.renderItems();
    let iconColor = muiTheme.palette.primary1Color;

    return (
      <div className={styles.Card} style={style}>
        <ClientProfile mandatoryId={['dob']} isFamily ref={ref=>{this.clientProfile=ref}}/>
        <div className={styles.CardPadding}>
          <div className={styles.profileOnly}>
            <div className={styles.profileOnlyTitle}>
              {getLocalText(langMap, "Family Members")}
            </div>
            <div>
              <IconButton onTouchTap={()=>{this.openProfileDialog({}, true)}}>{getIcon("add", iconColor)}</IconButton>
            </div>
          </div>
          {items}
        </div>
      </div>
	  )
  }
}
export default FamilyCardView;
