import React from 'react';
import {IconButton} from 'material-ui';

import DisplayCardView from '../../../CustomViews/DisplayCardView';
import EABAvatar from '../../../CustomViews/Avatar';
import ClientProfile from '../../../Dialogs/ClientProfile';
import FamilyCardView from './FamilyCardView';
import TrustIndividualCardView from './TrustIndividualCardView';
import {getIcon} from '../../../Icons/index';

import EABComponent from '../../../Component';
import * as _ from 'lodash';
import * as $ from '../../../../actions/GlobalActions';
import {getAvatarInitial} from '../../../../utils/client';
import styles from '../../../Common.scss';

class Main extends EABComponent {
  constructor(props, context) {
      super(props);
      let {template} = context.store.getState().client;
      this.state = Object.assign({}, this.state, {
        template: template.profileDisplayLayout,
        profile: {}
      })
  };

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.context.updateAppbar({
      items:[[]]
    })
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let {client} = this.context.store.getState();
      let {profile={}, dependantProfiles=[]} = client;
      let newProfile = _.cloneDeep(profile);

      let newState = {};
      if(!isEqual(this.state.profile, profile)){
        newState.profile = profile;
      }

      if(!isEmpty(newState)){
        this.setState(newState);
      }

      this.context.updateAppbar({
        title: profile.fullName
      })
    }

  }

  getValueFromTemplate=(template, id)=>{
    let {lang} = this.context;
    if(template.id === id){
      return getLocalText(lang, template.title);
    }
    else if(template.itmes){
      let itemsLength = template.items.length;
      for(let i=0; i<itemsLength; i++){
        let result = this.getValueFromTemplate(template.items[i], id);
        if(result === ""){
          return getLocalText(lang, template.title);
        }
        else if(_.isString(result)){
          return result;
        }
      }
    }
  }

  render() {
    let {muiTheme} = this.context;
    let {profile, template} = this.state;
    let {cid, photo, title='', fullName='', othName='', hanyuPinyinName=''} = profile;

    let name = title + (title?' ':'') + fullName// + (othName? ','+othName: '') + (hanyuPinyinName? `(${hanyuPinyinName})`:'');
    let iconColor = muiTheme.palette.primary1Color;
    let names = fullName.split(" ");
    var initial = getAvatarInitial(profile);

    let profileTitle = (
      <div style={{display: 'flex', padding: '0px 24px', alignItems: 'center'}}>
        <div style={{flexBasis: '80px', alignItems: 'center'}}>
          <EABAvatar
            docId={profile.cid}
            width={56}
            icon={getIcon('person', '#FFFFFF')}
            template={{id: 'photo'}}
            changedValues={{photo: profile.photo, initial}}
            values={{photo: profile.photo, initial}}
            disabled
          />
        </div>
        <div style={{fontSize: '16px', flex: 1}}>
          {name}
        </div>
        <div style={{alignSelf: 'flex-start'}}>
          <IconButton onTouchTap={()=>{this.clientProfile.openDialog(profile, false);}}>
            {getIcon('edit', iconColor)}
          </IconButton>
        </div>
      </div>
    )

    return (
      <div style={{flexGrow: 1}}>
        <ClientProfile
          values={profile}
          ref={ref=>{this.clientProfile=ref}}
          onDelete={()=>{
            this.context.closePage();
          }}
        />
        <DisplayCardView template={template} value={profile} style={{margin: '24px auto', width: 720}} title={profileTitle}/>
        <FamilyCardView style={{marginTop: '24px', margin: '24px auto', width: 720}}/>
        <TrustIndividualCardView style={{marginTop: '24px', margin: '24px auto', width: 720}}/>
      </div>

    );
  }
}

export default Main;
