import React, { PropTypes } from 'react';
import * as _ from 'lodash';
import {RaisedButton} from 'material-ui';

import EABComponent from '../../../Component';
import styles from '../../../Common.scss';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import WarningDialog from '../../../Dialogs/WarningDialog';
import ClientProfile from '../../../Dialogs/ClientProfile';
import Products from './Products';
import QuickQuoteHistory from '../QuickQuote/QuickQuoteHistory';

import {queryProducts, getDependantList, closeConfirm, closeError, closeProductPage} from '../../../../actions/products';
import {quotProduct} from '../../../../actions/quotation';
import {openHistory} from '../../../../actions/quickQuote';

import * as _c from '../../../../utils/client';
import Constants from '../../../../constants/SystemConstants';

export default class Main extends EABComponent {

  static propTypes = {
    quickQuote: PropTypes.bool
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    goTab: PropTypes.func
  });

  static childContextTypes = {
    selectProduct: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.ccy = null;
    this.insured = null;
    this.state = Object.assign({}, this.state, {
      dependants: [],
      prodCategories: [],
      error: null,
      confirmMsg: null,
      errorMsg: null,
      openQuickQuoteHistory: false
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !window.isEqual(this.state, nextState);
  }

  getChildContext() {
    return {
      selectProduct: this.selectProduct
    };
  }

  componentDidMount() {
    const {store} = this.context;
    if (!this.unsubscribe) {
      this.unsubscribe = store.subscribe(() => {
        const {products, quickQuote} = store.getState();
        const dependantUpdated = !window.isEqual(this.state.dependants, products.dependants);
        this.setState({
          dependants: products.dependants,
          prodCategories: products.prodCategories,
          error: products.error,
          confirmMsg: products.confirmMsg,
          errorMsg: products.errorMsg,
          openQuickQuoteHistory: quickQuote.open
        }, () => {
          if (dependantUpdated) {
            this.updateAppbarMenus();
          }
        });
      });
      this.loadDependants(() => {
        this.queryProducts();
      });
    }
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
    closeProductPage(this.context);
  }

  onTabChange() {
    this.updateAppbarMenus();
  }

  updateAppbarMenus=()=>{
    const {insured, ccy} = this;
    const {quickQuote} = this.props;
    const {store, optionsMap, lang, langMap} = this.context;
    const agentProfile = store.getState().app.agentProfile;

    let insuredOptions = [];
    insuredOptions.push({ value: null, text: window.getLocalizedText(langMap, 'products.insured.you') });
    _.each(this.state.dependants, (dependant) => {
      insuredOptions.push({
        value: dependant.cid,
        text: dependant.fullName
      });
    });

    let ccyOptions = [];
    _.each(optionsMap.ccy.currencies, (currency) => {
      if (currency.compCode === agentProfile.compCode) {
        _.each(currency.options, (option) => {
          ccyOptions.push({
            value: option.value,
            text: window.getLocalText(lang, option.title)
          });
        });
      }
    });

    let appbarBtns = [];
    if (quickQuote) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'products.quickQuote.history'),
        action: () => {
          openHistory(this.context);
        }
      });
    }
    appbarBtns.push({
      type: 'menu',
      menuValue: insured ? insured : null,
      options: insuredOptions,
      onChange: (value) => {
        this.insured = value;
        this.updateAppbarMenus();
        this.queryProducts();
      }
    });
    appbarBtns.push({
      type: 'menu',
      menuValue: ccy ? ccy : ccyOptions.length && ccyOptions[0].value,
      options: ccyOptions,
      onChange: (value) => {
        this.ccy = value;
        this.updateAppbarMenus();
        this.queryProducts();
      }
    });

    this.context.initAppbar({
      title: store.getState().client.profile.fullName,
      itemsIndex: 0,
      items: [appbarBtns]
    });
  }

  loadDependants = (callback) => {
    getDependantList(this.context, this.props.quickQuote, callback);
  };

  queryProducts = () => {
    queryProducts(this.context, this.ccy, this.insured, this.props.quickQuote, () => {
      if (this.state.error === 'missingProposerInfo' || this.state.error === 'missingInsuredInfo') {
        this._openProfileForEdit();
      }
    });
  };

  selectProduct = (productId) => {
    const {quickQuote} = this.props;
    const {store} = this.context;
    let {client, products} = store.getState();

    let pCid = client.profile.cid;
    let iCid = products.insuredCid || pCid;
    let ccy = products.ccy;
    quotProduct(this.context, productId, iCid, pCid, ccy, quickQuote);
  };

  confirmSelectProduct = () => {
    let productId = this.context.store.getState().products.selectedProductId;
    this.selectProduct(productId);
  };

  getContent() {
    const {goTab, langMap} = this.context;
    const {prodCategories, error} = this.state;
    if (error) {
      var msg1 = null;
      var msg2 = null;
      var action = null;
      if (error === 'missingInsuredInfo' || error === 'missingProposerInfo') {
        msg1 = window.getLocalizedText(langMap, 'products.valid.profile');
        action = <RaisedButton label={window.getLocalizedText(langMap, 'products.valid.editProfile')} secondary onTouchTap={() => this._openProfileForEdit()} />;
      } else if (error === 'incompleteFNA') {
        msg1 = window.getLocalizedText(langMap, 'products.valid.fna.title1');
        msg2 = window.getLocalizedText(langMap, 'products.valid.fna.title2');
        action = <RaisedButton label={window.getLocalizedText(langMap, 'products.valid.editFNA')} secondary onTouchTap={() => goTab(Constants.FEATURES.FNA)} />;
      } else if (error === 'noData') {
        msg1 = window.getLocalizedText(langMap, 'products.valid.noData');
      } else {
        msg1 = error;
      }
      return (
        <div className={styles.FullHeightFlexColumnNoWrapContainer + ' ' + styles.Products}>
          {msg1 ? <div className={styles.TextTitle + ' ' + styles.marginBottom}>{msg1}</div> : null}
          {msg2 ? <div className={styles.subTextTitle + ' ' + styles.marginBottom}>{msg2}</div> : null}
          {action}
        </div>
      );
    } else {
      return <Products prodCategories={prodCategories} />;
    }
  }

  _getProposerProfile() {
    const {client} = this.context.store.getState();
    return client.profile;
  }

  _getInsuredProfile() {
    const {client} = this.context.store.getState();
    let profile = client.dependantProfiles && client.dependantProfiles[this.insured];
    if (!profile) {
      return {};
    }
    let dependant = _.find(client.profile.dependants, d => d.cid === profile.cid);
    return {
      relationship: dependant && dependant.relationship,
      relationshipOther: dependant && dependant.relationshipOther,
      ...profile
    };
  }

  _openProfileForEdit() {
    if (this.state.error === 'missingInsuredInfo') {
      if (this.insured === null) {
        this.pClientProfile.openDialog(this._getProposerProfile(), false);
      } else {
        this.iClientProfile.openDialog(this._getInsuredProfile(), false);
      }
    } else if (this.state.error === 'missingProposerInfo') {
      this.pClientProfile.openDialog(this._getProposerProfile(), false);
    }
  }

  isSpouseInsured() {
    const {client} = this.context.store.getState();
    let profile = client.dependantProfiles && client.dependantProfiles[this.insured];
    if (!profile) {
      return false;
    }
    let dependant = _.find(client.profile.dependants, d => d.cid === profile.cid);
    return dependant && dependant.relationship === "SPO"? true: false;
  }

  render() {
    const {quickQuote} = this.props;
    const {openQuickQuoteHistory, confirmMsg, errorMsg} = this.state;
    let hasFNA = _.indexOf(_.get(this.context.store.getState(), 'app.agentProfile.features'), 'FNA') > -1? true: false;

    return (
      <div style={{ width: '100%', /* NOTE: lucia newly added*/ display: 'flex', justifyContent: 'center', flexDirection: 'row', textAlign: 'center', }}>
        {this.getContent()}
        <ClientProfile
          ref={ref => { this.iClientProfile = ref; }}
          isFamily
          afterFNA
          values={this._getInsuredProfile()}
          mandatoryId={_c.genProfileMandatory(quickQuote? Constants.FEATURES.QQ: Constants.FEATURES.FQ, hasFNA, false, this.isSpouseInsured())}
          onSubmit={() => this.queryProducts()}
        />
        <ClientProfile
          ref={ref => { this.pClientProfile = ref; }}
          values={this._getProposerProfile()}
          mandatoryId={_c.genProfileMandatory(quickQuote? Constants.FEATURES.QQ: Constants.FEATURES.FQ, hasFNA, true, false)}
          onSubmit={() => this.queryProducts()}
        />
        {quickQuote && openQuickQuoteHistory ? <QuickQuoteHistory /> : null}
        <ConfirmDialog
          open={!!confirmMsg}
          confirmMsg={confirmMsg}
          onClose={() => closeConfirm(this.context)}
          onConfirm={() => this.confirmSelectProduct()}
        />
        <WarningDialog
          open={!!errorMsg}
          msg={errorMsg}
          onClose={() => closeError(this.context)}
        />
      </div>
    );
  }

}
