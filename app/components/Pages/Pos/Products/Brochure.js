import React, {PropTypes} from 'react';

import DynSizePDFViewer from '../../../CustomViews/DynSizePDFViewer';
import AppbarPage from '../../AppbarPage';
import FloatingPage from '../../FloatingPage';

import EABComponent from '../../../Component';

import {closeBrochure} from '../../../../actions/brochure';
import {quotProduct} from '../../../../actions/quotation';

import {getProductId} from '../../../../../common/ProductUtils';

class Brochure extends EABComponent {

  static propTypes = {
    quickQuote: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      data: null,
      product: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(() => {
      const brochure = store.getState().brochure;
      if (this.state.open !== brochure.open) {
        if (brochure.open) {
          this.open();
        } else {
          this.close();
        }
        this.setState({
          open: brochure.open,
          data: brochure.data,
          product: brochure.product
        }, this.updateAppbar);
      }
    });
    this.updateAppbar();
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  updateAppbar=()=>{
    const {lang, langMap, store} = this.context;
    const {quickQuote} = this.props;
    const {product} = this.state;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'close',
        action: () => {
          closeBrochure(this.context);
        }
      },
      title: product && window.getLocalText(lang, product.covName),
      itemsIndex: 0,
      items: [
        [{
          type: 'flatButton',
          title: window.getLocalizedText(langMap, 'products.brochure.quote'),
          action: () => {
            let {client, products} = store.getState();
            let proposerCid = client.profile.cid;
            let insuredCid = products.insuredCid;
            let ccy = products.ccy;

            let iCid = insuredCid ? insuredCid : proposerCid;
            let pCid = proposerCid;
            quotProduct(this.context, getProductId(product), iCid, pCid, ccy, quickQuote, () => closeBrochure(this.context));
          }
        }]
      ]
    });
  }

  open =()=> {
    this.floatingPage.open();
  }

  close =()=> {
    this.floatingPage.close();
  }

  render() {
    const {data} = this.state;
    return (
      <FloatingPage ref={ref=>{ this.floatingPage = ref; }}>
        <AppbarPage ref={ref=>{ this.appbarPage = ref; }}>
          <DynSizePDFViewer pdf={data} scale={2} />
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default Brochure;
