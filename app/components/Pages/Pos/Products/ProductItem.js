import React, {PropTypes} from 'react';

import {getProductThumbnailUrl} from '../../../../utils/PlanUtils';
import {getProductId} from '../../../../../common/ProductUtils';

import EABComponent from '../../../Component';
import styles from '../../../Common.scss';

export default class ProductItem extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    product: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    selectProduct: PropTypes.func
  });

  shouldComponentUpdate(nextProps) {
    return this.props.product.covCode !== nextProps.product.covCode;
  }

  render() {
    const {store, selectProduct} = this.context;
    const {product} = this.props;
    let thumbnailSrc = getProductThumbnailUrl(store.getState().app, product);
    return (
      <div className={styles.ProductItems}>
        <div onTouchTap={() => selectProduct(getProductId(product))}>
          <img id={product.covCode} style={{height: 283, border: "1px solid #CCCCCC", cursor: 'pointer' }} src={thumbnailSrc} />
        </div>
        <div className={styles.ProductName}>
          <span>{product.covName.en}</span>
        </div>
      </div>
    );
  }
}
