import React from 'react';
import PropTypes from 'prop-types';
import DynNeedPage from '../../../../DynViews/DynNeedPage';
import {Dialog, FlatButton} from 'material-ui';

import EABComponent from '../../../../Component';
import {getIcon} from '../../../../Icons/index';

import * as _ from 'lodash';
import * as _n from '../../../../../utils/needs';
import * as _v from '../../../../../utils/Validation';
import * as _s from '../../../../../actions/sessions';

import {saveFNA} from '../../../../../actions/needs';
import styles from '../../../../Common.scss';


class Main extends EABComponent {
  constructor(props, context) {
      super(props);

      let {needs} = context.store.getState();
      let template = needs.template.fnaForm;
      this.state = Object.assign({}, this.state, {
        template,
        changedValues: {},
        values: {},
        error: {},
        errorCode: null,
        showUpdDialog: false
      })
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.context.initAppbar({
      menu: {
        title: 'SAVE',
        action: ()=>{
          this.onSave(this.onClose);
        }
      },
      items:[[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        disabled: true
      }],[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        action: ()=>{
          this.context.goStep(this.context.stepperIndex+1, true);
        }
      }],
      [{
        id: "done",
        type: "flatButton",
        title: {
          "en": "DONE"
        },
        disabled: true
      }],[{
        id: "done",
        type: "flatButton",
        title: {
          "en": "DONE"
        },
        action: ()=>{
          this.state.changedValues.completed = true;
          this.onSave(this.onClose);
        }
      }]]
    });

    this.context.updateStepper({
      onSave: this.onSave.bind(this),
      onGoStepComplete: this.onGoStepComplete.bind(this)
    });
    this.storeListener();
  }

  componentWillUnmount() {
    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let {store} = this.context;
      let {needs} = store.getState();
      let newState = {};

      if(!isEqual(this.state.values, needs.fna)){
        newState.values = _.cloneDeep(needs.fna);
        newState.changedValues = _.cloneDeep(needs.fna);
      }

      if(!isEmpty(newState)){
        this.setState(newState);
      }
    }
  }

  init=()=>{
    this.state.hasChange = false;
    let {needs} = this.context.store.getState();
    let changedValues = _.cloneDeep(needs.fna);
    this.validate(changedValues, true);
  }

  onGoStepComplete=(index)=>{
    let {error, template, changedValues} = this.state;
    let {lastIndex} = this.getStepperPageIndex(error);
    let valid = !_.at(error, `sections[${index}].code`)[0];
    if (index <=4 && changedValues.lastStepIndex < index) {
      changedValues.lastStepIndex = index;
      this.state.hasChange = true;
      if (valid) {
        this.context.updateStepper({completed: index, active: index+1});
      }
    } 
    let {title} = template.items[index] || {};
    this.context.updateAppbar({title, itemsIndex: lastIndex===index? (valid?3:2) : (valid? 1: 0)})
    this.validate(this.state.changedValues);
  }

  getStepperPageIndex=(error)=>{
    let {items} = this.state.template;
    let len = items.length;
    let lastIndex = len-1, completed = len;
    for(let i=len-1; i>=0; i--){
      if(_.at(error, `sections[${i}].code`)[0]){
        completed = i-1;
      }
    }
    return {completed, lastIndex};
  }

  

  validate=(changedValues, init)=>{
    let {needs} = this.context.store.getState();
    let {template, index, values} = this.state;
    if(init){
      values = _.cloneDeep(needs.fna);
    }
    let error = {};
    _n.validateFNA(this.context, template, changedValues, error);
    let {completed: _completed, lastIndex} = this.getStepperPageIndex(error);
    let completed = _completed;
    if(_.isNumber(changedValues.lastStepIndex)){
      if(completed > changedValues.lastStepIndex){
        completed = changedValues.lastStepIndex;
      }
    }
    else {
      completed = -1;
    }

    let headers = [];
    _.forEach(template.items, (item, i)=>{
      let {title, disabled} = item;
      disabled = disabled || _.at(error, `sections[${i}].skip`)[0];
      headers.push({id: i, title, disabled});
    })

    let stepper = {completed, headers, validStep: _completed};
    if(init){
     stepper.index= completed + 1>= headers.length? headers.length - 1: completed + 1;
      if(!(_completed >=4 && changedValues.lastStepIndex >=4) && _completed > changedValues.lastStepIndex){
        changedValues.lastStepIndex = changedValues.lastStepIndex + 1;
        this.state.hasChange = true;
        stepper.completed = stepper.completed + 1;
      }
      stepper.active = _completed + 1;
    }
    let {lastStepIndex: lsIdx = 0} = changedValues;
    let currentIndex = init? lsIdx<0? 0: lsIdx:this.context.stepperIndex;
    if(currentIndex > lastIndex){
      currentIndex = lastIndex;
    }
    let {title} = template.items[currentIndex];
    let valid = !_.at(error, `sections[${currentIndex}].code`)[0]; 

    this.context.updateAppbar({title, itemsIndex: lastIndex===currentIndex? (valid?3:2) : (valid? 1: 0)})
    this.context.updateStepper(stepper);
    
    this.setState({changedValues, values, template, error}, ()=>{
      if(init){
        this.validate(changedValues);
      }
    });
  }

  rollback = () =>{
    let {store} = this.context;
    let {fna} = store.getState().needs;
    this.state.hasChange = false;
    this.state.showUpdDialog = false;
    this.validate(_.cloneDeep(fna), false);
  }

  getContent=()=>{
    let {stepperIndex, langMap, lang} = this.context;
    let { template, changedValues, values, error, langCode, showUpdDialog, errorCode} = this.state;
    if(!checkExist(template, 'items'))
      return;
    let field=template.items[stepperIndex];

    const updActions = [
      (
        <FlatButton
          key="profileDialog-updFM-cancel-btn"
          label="No"
          onTouchTap={this.rollback}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updFM-confirm-btn"
          label="Yes"
          onTouchTap={()=>{
            this.onSave(this.state.callback, true);
          }}
        />
      )
    ]
    
    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <Dialog open={showUpdDialog} actions={updActions}>
          {_v.getErrorMsg(null, errorCode, lang)}
        </Dialog>
        <DynNeedPage
          template={field}
          changedValues={changedValues}
          error={error}
          values={values}
          docId={"fnaForm"}
          onChange={this.onChange}
          validate={this.validate}
          onSave={this.onSave}
        />
      </div>
    )
  }

  onChange=(revisit)=>{
    let currentIndex = this.context.stepperIndex;
    let {changedValues} = this.state;
    if(currentIndex !== 4 && changedValues.productType && !revisit){
      changedValues.productType.prodType = null;
      
      if (_.get(changedValues, 'raSection.owner')) {
        changedValues.raSection.owner.init = true;
        changedValues.raSection.isValid = false;
      }
      if (_.get(changedValues, 'ckaSection.owner')) {
        changedValues.ckaSection.owner.init = true;
        changedValues.ckaSection.isValid = false;
      }
    }
    changedValues.lastStepIndex = currentIndex;
    //changedValues.isCompleted = false;
    this.state.hasChange = true;
  }

  onClose=()=>{
    this.context.onPageClose();
    this.context.closePage();
  }

  onSave=(callback, confirm = false)=>{
    let {changedValues, values, hasChange, error} = this.state;
    if(changedValues.lastStepIndex > 4){
      changedValues.lastStepIndex = 4;
    }
    //check is FNA complete
    changedValues.isCompleted = !error.code && changedValues.lastStepIndex >= 4? true: false;
    
    if(hasChange || changedValues.lastStepIndex != values.lastStepIndex || changedValues.isCompleted != values.isCompleted){
      this.setState({showUpdDialog: false, errorCode: null}, ()=>{
        saveFNA(this.context, this.state.changedValues, confirm, (code)=>{
          if(code){
            this.setState({
              errorCode: code,
              showUpdDialog: true,
              callback
            })
          }
          else{
            this.setState({showUpdDialog: false, errorCode: null, hasChange: false, callback: null}, callback);
          }
        });
      });
    }
    else {
      this.state.hasChange = false;
      _s.extendSession(this.context, callback);
    }
  }

 
  render() {
    let {pageOpen} = this.context;

    if(!pageOpen){
      return <div/>
    }

    let content = this.getContent();

    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column'}}>
        {content}
      </div>
    );
  }
}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  rootTemplate: PropTypes.object
});

export default Main;
