export const CNA_FORM = {
	"type": "layout",
	"needsLandingTitle": [{
			"id": "learnMore",
			"title": {
				"en": "Learn how life insurance can benefit you"
			},
			"content": {
				"en": "Life insurance can help you fulfil the needs of your loved ones and\nyour personal ambitions."
			},
			"button": {
				"en": "Learn More"
			}
		},
		{
			"id": "recommendProducts",
			"title": {
				"en": "Our life insurance plans can help you achieve your goals."
			},
			"content": {
				"en": "We're happy to recommend products to you based on your financial evaluation,\nneeds analysis and risk assessment"
			},
			"button": {
				"en": "View Suitable Products"
			}
		}
	],
	"needsLandingContent": [
		{
			"template": "pdaForm",
			"title": {
				"en": "Personal Data Acknowledgement"
			},
			"content": {
				"en": "Provide your consent on your personal data usage before starting your Financial Needs Analysis."
			},
			"contentLastCompleted": {
				"en": "Last completed on "
			},
			"buttonStart": {
				"en": "Start"
			},
			"buttonContinue": {
				"en": "Continue"
			},
			"buttonView": {
				"en": "Edit"
			},
			"expMonths": 15,
			"bgImage": "needsPersonalData",
			"bgColor": "#BFCFC8",
			"id": "personalData",
			"type": "personalData"
		},
		{
			"template": "feForm",
			"type": "tabSection",
			"title": {
				"en": "Financial Evaluation"
			},
			"content": {
				"en": "Tell us about your finances so we can help you plan for the future."
			},
			"contentLastCompleted": {
				"en": "Last completed on "
			},
			"buttonStart": {
				"en": "Start"
			},
			"buttonContinue": {
				"en": "InComplete"
			},
			"buttonView": {
				"en": "Edit"
			},
			"expMonths": 8,
			"bgImage": "needsFinancialEvaluation",
			"bgColor": "#E6E2DF",
			"id": "financialEvaluation"
		},
		{
			"template": "fnaForm",
			"title": {
				"en": "Needs Analysis"
			},
			"content": {
				"en": "Find out how much coverage you need to meet your objectives."
			},
			"contentLastCompleted": {
				"en": "Last completed on "
			},
			"buttonStart": {
				"en": "Start"
			},
			"buttonContinue": {
				"en": "Continue"
			},
			"buttonView": {
				"en": "Edit"
			},
			"expMonths": 10,
			"bgImage": "needsAnalysis",
			"bgColor": "#D1DCE6",
			"id": "needsAnalysis",
			"type": "needsAnalysis"
		}
	],
	"conceptSelling": {
		"id": "NeedsConceptSelling",
		"categories": [{
				"id": "finanPlanning",
				"title": {
					"en": "Financial Planning",
					"zh-Hant": ""
				},
				"items": [{
						"id": "finanPlanning1",
						"boxRegularWidth": 261,
						"file": "Financial Planning 1",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning2",
						"boxRegularWidth": 261,
						"file": "Financial Planning 2",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning3",
						"boxRegularWidth": 261,
						"file": "Financial Planning 3",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning4",
						"boxRegularWidth": 261,
						"file": "Financial Planning 4",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 4",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning5",
						"boxRegularWidth": 261,
						"file": "Financial Planning 5",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 5",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning6",
						"boxRegularWidth": 261,
						"file": "Financial Planning 6",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 6",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning7",
						"boxRegularWidth": 261,
						"file": "Financial Planning 7",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 7",
							"zh-Hant": ""
						}
					},
					{
						"id": "finanPlanning8",
						"boxRegularWidth": 261,
						"file": "Financial Planning 8",
						"fileType": "image/png",
						"fileDesc": {
							"en": "finanPlanning image 8",
							"zh-Hant": ""
						}
					}
				],
				"seq": 1
			},
			{
				"id": "protect",
				"title": {
					"en": "Life Protection",
					"zh-Hant": ""
				},
				"items": [{
						"id": "protect1",
						"boxRegularWidth": 261,
						"file": "Life Protection 1",
						"fileType": "image/pdf",
						"fileDesc": {
							"en": "protect image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "protect2",
						"boxRegularWidth": 261,
						"file": "Life Protection 2",
						"fileType": "image/pdf",
						"fileDesc": {
							"en": "protect image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "protect3",
						"boxRegularWidth": 261,
						"file": "Life Protection 3",
						"fileType": "image/pdf",
						"fileDesc": {
							"en": "protect image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "protect4",
						"boxRegularWidth": 261,
						"file": "Life Protection 4",
						"fileType": "image/pdf",
						"fileDesc": {
							"en": "protect image 4",
							"zh-Hant": ""
						}
					}
				],
				"seq": 2
			},
			{
				"id": "critIllness",
				"title": {
					"en": "Critical Illness",
					"zh-Hant": ""
				},
				"items": [{
						"id": "critIllness1",
						"boxRegularWidth": 261,
						"file": "Critical Illness 1",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness2",
						"boxRegularWidth": 261,
						"file": "Critical Illness 2",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness3",
						"boxRegularWidth": 261,
						"file": "Critical Illness 3",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness4",
						"boxRegularWidth": 261,
						"file": "Critical Illness 4",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 4",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness5",
						"boxRegularWidth": 261,
						"file": "Critical Illness 5",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 5",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness6",
						"boxRegularWidth": 261,
						"file": "Critical Illness 6",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 6",
							"zh-Hant": ""
						}
					},
					{
						"id": "critIllness7",
						"boxRegularWidth": 261,
						"file": "Critical Illness 7",
						"fileType": "image/png",
						"fileDesc": {
							"en": "critIllness image 7",
							"zh-Hant": ""
						}
					}
				],
				"seq": 3
			},
			{
				"id": "childEducate",
				"title": {
					"en": "Education",
					"zh-Hant": ""
				},
				"items": [{
						"id": "childEducate1",
						"boxRegularWidth": 261,
						"file": "Education 1",
						"fileType": "image/png",
						"fileDesc": {
							"en": "childEducate image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "childEducate2",
						"boxRegularWidth": 261,
						"file": "Education 2",
						"fileType": "image/png",
						"fileDesc": {
							"en": "childEducate image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "childEducate3",
						"boxRegularWidth": 261,
						"file": "Education 3",
						"fileType": "image/png",
						"fileDesc": {
							"en": "childEducate image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "childEducate4",
						"boxRegularWidth": 261,
						"file": "Education 4",
						"fileType": "image/png",
						"fileDesc": {
							"en": "childEducate image 4",
							"zh-Hant": ""
						}
					},
					{
						"id": "childEducate5",
						"boxRegularWidth": 261,
						"file": "Education 5",
						"fileType": "image/png",
						"fileDesc": {
							"en": "childEducate image 5",
							"zh-Hant": ""
						}
					}
				],
				"seq": 4
			},
			{
				"id": "retirement",
				"title": {
					"en": "Retirement",
					"zh-Hant": ""
				},
				"items": [{
						"id": "retirement1",
						"boxRegularWidth": 261,
						"file": "Retirement 1",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "retirement2",
						"boxRegularWidth": 261,
						"file": "Retirement 2",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "retirement3",
						"boxRegularWidth": 261,
						"file": "Retirement 3",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "retirement4",
						"boxRegularWidth": 261,
						"file": "Retirement 4",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 4",
							"zh-Hant": ""
						}
					},
					{
						"id": "retirement5",
						"boxRegularWidth": 261,
						"file": "Retirement 5",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 5",
							"zh-Hant": ""
						}
					},
					{
						"id": "retirement6",
						"boxRegularWidth": 261,
						"file": "Retirement 6",
						"fileType": "image/png",
						"fileDesc": {
							"en": "retirement image 6",
							"zh-Hant": ""
						}
					}
				],
				"seq": 5
			},
			{
				"id": "saving",
				"title": {
					"en": "Savings",
					"zh-Hant": ""
				},
				"items": [{
						"id": "saving1",
						"boxRegularWidth": 261,
						"file": "Savings 1",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 1",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving2",
						"boxRegularWidth": 261,
						"file": "Savings 2",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 2",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving3",
						"boxRegularWidth": 261,
						"file": "Savings 3",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 3",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving4",
						"boxRegularWidth": 261,
						"file": "Savings 4",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 4",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving5",
						"boxRegularWidth": 261,
						"file": "Savings 5",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 5",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving6",
						"boxRegularWidth": 261,
						"file": "Savings 6",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 6",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving7",
						"boxRegularWidth": 261,
						"file": "Savings 7",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 7",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving8",
						"boxRegularWidth": 261,
						"file": "Savings 8",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 8",
							"zh-Hant": ""
						}
					},
					{
						"id": "saving9",
						"boxRegularWidth": 261,
						"file": "Savings 9",
						"fileType": "image/png",
						"fileDesc": {
							"en": "saving image 9",
							"zh-Hant": ""
						}
					}
				],
				"seq": 5
			}
		]
	}
}
