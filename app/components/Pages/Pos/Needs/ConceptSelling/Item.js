import React from 'react';
import {FlatButton, List, ListItem} from 'material-ui';
import * as _ from 'lodash';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import {getIcon} from '../../../../Icons/index';
import ImageView from '../../../../CustomViews/ImageView';
import EABComponent from '../../../../Component';

import styles from '../../../../Common.scss';

class Main extends EABComponent {
  constructor(props) {
      super(props);
      let {template} = props;
      let attId = null;

      if(template && template.items){
          attId = template.items[0].id;
      }

      this.state = Object.assign({}, this.state, {
        index: 0,
        attId: attId
      })
  };

  componentDidMount() {
    this.context.initAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          if(this.state.fullScreen){
            this.setState({
              fullScreen: false
            })
          }else{
              this.context.closePage();
          }
        }
      },
      items:[[{
        id: "previous",
        icon: "previous",
        type: "iconButton",
        disabled: true
      },{
        id: "next",
        icon: "next",
        type: "iconButton",
        action: ()=>{
          this.getAttachmentByIndex(this.state.index+1)
        }
      }],[{
        id: "previous",
        icon: "previous",
        type: "iconButton",
        action: ()=>{
          this.getAttachmentByIndex(this.state.index-1)
        }
      },{
        id: "next",
        icon: "next",
        type: "iconButton",
        action: ()=>{
          this.getAttachmentByIndex(this.state.index+1)
        }
      }],[{
        id: "previous",
        icon: "previous",
        type: "iconButton",
        action: ()=>{
          this.getAttachmentByIndex(this.state.index-1)
        }
      },{
        id: "next",
        icon: "next",
        type: "iconButton",
        disabled: true
      }]]
    })
  }


  componentWillReceiveProps(nextProps){
    if(!isEqual(this.state.template, nextProps.template) && checkExist(nextProps, 'template.items')){
      this.setState({
        attId: nextProps.template.items[0].id,
        index: 0
      });
      this.context.updateAppbar({
        itemsIndex: 0
      })
    }
  }

  getAttachmentByIndex=(index)=>{
    let {items} = this.props.template;

    this.setState({
      attId: this.props.template.items[index].id,
      index: index
    });

    if(index == 0){
      this.context.updateAppbar({
        itemsIndex: 0
      })
    }else if(index == items.length-1){
      this.context.updateAppbar({
        itemsIndex: 2
      })
    }else{
      this.context.updateAppbar({
        itemsIndex: 1
      })
    }
  }

  getMenuItems=()=>{
    let {
      langMap,
      lang,
      muiTheme,
      store
    } = this.context;


    let {
      template
    } = this.props;
    //return null if template is not exist
    if(!checkExist(template, 'items'))
      return ;

    let iconColor = muiTheme.palette.alternateTextColor;

    let menuItems = [];
    let items = template.items;
    _.forEach(items, (item, i)=>{
      let {id, file, fileDesc} = item;
      menuItems.push(
        <ListItem
          key={`concept-selling-${template.id}-menu-item-${id}`}
          onTouchTap={()=>{
            this.getAttachmentByIndex(i);
          }}
          primaryText={getLocalText(lang, fileDesc)}
          leftIcon={
            <ImageView
              style={{height: '40px', width: '32px', top: '4px', margin: 0, paddingRight: '16px'}}
              imageStyle={{width: '100%', height: '40px'}}
              docId={'CNAForm'}
              attId={id}
            />
          }
        />
      )
    })

    return menuItems;
  }

  render() {
    let {pageOpen} = this.context;
    let {attId, fullScreen} = this.state;

    if(!pageOpen){
      return <div/>
    }
    let menuItems = this.getMenuItems();

    return (
      <div style={{display: 'flex', flexGrow: 1}}>
        <List className={styles.Menu} style={{display: fullScreen?'none':'block'}}>
          {menuItems}
        </List>
        <ImageView
          onItemClick={()=>{
            this.setState({
              fullScreen: true
            })
            this.context.updateAppbar({
              itemsIndex: 1
            })
          }}
          style={{flexGrow: 1, cursor: 'pointer'}}
          docId={"CNAForm"}
          attId={attId}
        />
      </div>
    )
  }
}

class Item extends EABComponent {
  constructor(props) {
      super(props);
      this.state = {
        template: {}
      };
  };

  open=(template)=>{
    this.setState({template: template});
    this.appbarPage.updateAppbar({title: getLocalText(this.context.lang, template.title)});
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }
  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage ref={(ref)=>{this.appbarPage=ref;}}>
          <Main template={this.state.template} {...this.props}/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default Item;
