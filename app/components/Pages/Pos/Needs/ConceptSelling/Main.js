import React from 'react';
import {FlatButton, List, ListItem} from 'material-ui';
import * as _ from 'lodash';
import EABComponent from '../../../../Component';

import {getIcon} from '../../../../Icons/index';
import Item from './Item';
import ImageView from '../../../../CustomViews/ImageView';

import styles from '../../../../Common.scss';

class Main extends EABComponent {
  constructor(props) {
      super(props);
      Object.assign({}, this.state);
  };

  componentDidMount() {
    this.context.initAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          this.context.closePage();
        }
      },
      title: {
        "en": "Concept selling"
      }
    })
  }

  getMenuItems=()=>{
    let {langMap, lang, muiTheme, store} = this.context;
  let {needs} = store.getState();

    //return null if template is not exist
    if(!checkExist(needs, 'template.cnaForm.conceptSelling'))
      return;

    let iconColor = muiTheme.palette.alternateTextColor;

    let menuItems = [];
    let {categories} = needs.template.cnaForm.conceptSelling;

    _.forEach(categories, (category, i)=>{
      let template = category;
      let {id, title} = template;
      menuItems.push(
        <ListItem
          key={`concept-selling-menu-item-${id}`}
          primaryText={getLocalText(lang, title)}
          leftIcon={
            //getIcon("folder", iconColor)
            <ImageView
              style={{height: '40px', width: '32px', top: '4px', margin: 0, paddingRight: '16px'}}
              imageStyle={{width: '100%', height: '40px'}}
              docId={'CNAForm'}
              attId={id}
            />
          }
          onTouchTap={()=>{
            this.itemPage.open(template);
          }}
        />
      )
    })

    return menuItems;
  }


  render() {

    let {pageOpen} = this.context;

    if(!pageOpen){
      return <div/>
    }

    let menuItems = this.getMenuItems();

    return (
      <div id={'ConceptSelling'} style={{display: 'flex', flexGrow: 1}}>
        <Item ref={(ref)=>{this.itemPage=ref;}} onClose={()=>{this.context.closePage()}}/>
        <List className={ styles.Menu } style={{zIndex: 1}}>
          {menuItems}
        </List>
      </div>

    );
  }
}

export default Main;
