import React from 'react';
import {FlatButton} from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import NeedsSummary from './NeedsSummary';
import EABComponent from '../../../../Component';

import styles from '../../../../Common.scss';

class NSMain extends EABComponent {
  open=()=>{
    this.NeedsSummary.init();
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }
  
  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage>
            <NeedsSummary ref={(ref)=>{this.NeedsSummary=ref;}}/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default NSMain;