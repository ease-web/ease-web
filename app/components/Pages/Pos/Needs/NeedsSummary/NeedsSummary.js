import React from 'react';

import PropTypes from 'prop-types';

import EABComponent from '../../../../Component';
import NeedsSummaryList from '../../../../CustomViews/NeedsSummaryList';
import DynNeedPage from '../../../../DynViews/DynNeedPage';
import * as _n from '../../../../../utils/needs';

import styles from '../../../../Common.scss';

import * as _ from 'lodash';


class NeedsSummary extends EABComponent {
  constructor(props, context) {
    super(props);

    let {needs} = context.store.getState();
    let errors = this.checkingError(needs);
    this.state = Object.assign({}, this.state, {
      values: _.cloneDeep(needs.fna),
      errors,
      itemTemplate: {}
    })
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null
    }
  }

  checkingError(needs){
    let {fna} = needs;
    let error = {};

    let values = _.cloneDeep(fna);
   /**  let valid = _n.validateFNA(this.context, needs.template.fnaForm, values, error);*/

    return error;
  }

  storeListener=()=>{
    let {store} = this.context;
    let {needs} = store.getState();
    let {errors} = this.state;

    let newState = {};
    let itemTemplate = {};

    if(_.isObject(needs) && !isEqual(this.state.values, needs.fna)){
      newState.values = _.cloneDeep(needs.fna);
      newState.errors = _.cloneDeep(this.checkingError(needs));
    }

    if (_.isObject(needs) && _.isObject(needs.fna)  && _.isObject(needs.needsSummary) &&  needs.needsSummary && needs.fna.isCompleted && needs.needsSummary.completed) {
      itemTemplate = needs.needsSummary.completed;
      newState.itemTemplate = _.cloneDeep(itemTemplate);
    }else if (_.isObject(needs) && _.isObject(needs.fna)  && _.isObject(needs.needsSummary) &&  needs.needsSummary && !needs.fna.isCompleted && needs.needsSummary.inCompleted){
      itemTemplate = needs.needsSummary.inCompleted;
      itemTemplate.errors = errors;
      newState.itemTemplate = _.cloneDeep(itemTemplate);
    }

    if(!isEmpty(newState)){
      this.setState(newState);
    }

  }

  init=()=>{
    this.context.initAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          this.context.onPageClose();
          this.context.closePage();
        }
      },
      title: {
        "en": "Needs Summary"
      },
      itemsIndex: 0,
      items:[]
    });
  }

  render() {
    let {values, errors,itemTemplate} = this.state;


    if (isEmpty(values)) {
      return (<div></div>);
    }

    return (
      <div className={styles.PanelBackgroundColor} style={{display: 'flex', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <DynNeedPage
          template={itemTemplate}
          docId={"fnaForm"}
          changedValues={values}
          values={values}
        />
      </div>
    );
  }

}

NeedsSummary.contextTypes = Object.assign(NeedsSummary.contextTypes, {
  onPageClose: PropTypes.func
})

NeedsSummary.childContextTypes = Object.assign({}, NeedsSummary.childContextTypes, {
  rootTemplate: PropTypes.object
});

export default NeedsSummary;
