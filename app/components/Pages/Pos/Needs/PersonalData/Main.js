import React from 'react';
import PropTypes from 'prop-types';
import {Dialog, FlatButton, Step, Stepper, StepButton} from 'material-ui';
import DynNeedPage from '../../../../DynViews/DynNeedPage';
import EABComponent from '../../../../Component';
import {getIcon} from '../../../../Icons/index';

import * as _ from 'lodash';
import * as _n from '../../../../../utils/needs';
import * as _v from '../../../../../utils/Validation';
import * as _s from '../../../../../actions/sessions';
import {savePDA} from '../../../../../actions/needs';

import styles from '../../../../Common.scss';

class Main extends EABComponent {
  constructor(props, context) {
      super(props);

      let {needs} = context.store.getState();
      let template = needs.template.pdaForm;

      this.state = Object.assign({}, this.state, {
        template,
        changedValues: {},
        values: {},
        error: {},
        errorCode: null,
        showUpdDialog: false,
        lastHeaderIndex: 0
      })
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.context.initAppbar({
      menu: {
        title: 'SAVE',
        action: ()=>{
          this.onSave(this.onClose);
        }
      },
      items:[[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        disabled: true
      }],[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        action: ()=>{
          this.context.goStep(this.context.stepperIndex+1, true);
        }
      }],
      [{
        id: "done",
        type: "flatButton",
        title: {
          "en": "DONE"
        },
        disabled: true
      }],[{
        id: "done",
        type: "flatButton",
        title: {
          "en": "DONE"
        },
        action: ()=>{
          this.state.changedValues.completed = true;
          this.onSave(this.onClose);
        }
      }]],
      "title": {
        "en":"Personal Data Acknowledgement"
      }
    })
    this.context.updateStepper({
      onSave: this.onSave.bind(this),
      onGoStepComplete: this.onGoStepComplete.bind(this)
    });
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let {store} = this.context;
      let {needs} = store.getState();
      let newState = {};

      if(!isEqual(this.state.values, needs.pda)){
        newState.values = _.cloneDeep(needs.pda);
        newState.changedValues = _.cloneDeep(needs.pda);
      }

      if(!isEmpty(newState)){
        this.setState(newState);
      }
    }
  }

  init=()=>{
    let {needs} = this.context.store.getState();
    let changedValues = _.cloneDeep(needs.pda);
    this.state.hasChange = false;
    this.validate(changedValues, true);    
  }

  onClose=()=>{
    this.context.onPageClose();
    this.context.closePage();
  }

  onSave=(callback, confirm = false)=>{
    if(this.state.hasChange || this.state.changedValues.lastStepIndex != this.state.values.lastStepIndex){
      //changedValues complete only when no error code and currently at last page
      if(!this.state.changedValues.isCompleted && !this.state.error.code && this.state.changedValues.lastStepIndex == this.state.lastHeaderIndex){
        this.state.changedValues.isCompleted = true;
      }
      savePDA(this.context, this.state.changedValues, confirm, (code)=>{
        if(code){
          this.setState({
            errorCode: code,
            showUpdDialog: true,
            callback
          })
        }
        else{
          this.setState({showUpdDialog: false, errorCode: null, hasChange: false, callback: null}, callback);
        }
      });
    }
    else {
      this.state.hasChange = false;
      _s.extendSession(this.context, callback);
    }
    
  }

  onGoStepComplete=(index)=>{
    let {error, changedValues} = this.state;
    let {lastIndex} = this.getStepperPageIndex(error);
    let valid = !_.at(error, `sections[${index}].code`)[0];  
    if(changedValues.lastStepIndex < index){
      changedValues.lastStepIndex = index;
      this.state.hasChange = true;
    }
    this.context.updateAppbar({itemsIndex: lastIndex===index? (valid?3:2) : (valid? 1: 0)})
  }

  getStepperPageIndex=(error)=>{
    let completed = 0, lastIndex = 0, validationPass = true;

    _.forEach(_.at(error, 'sections')[0], (sErr, j)=>{
      if(!sErr.skip){
        lastIndex = Number(j);
      }
      if(!sErr.code && !sErr.skip && validationPass){
        completed = Number(j); 
      }else if (validationPass) {
        completed = Number(j) - 1;
        validationPass = false;
      }
    })

    return {completed, lastIndex};
  }

  validate=(changedValues, init)=>{
    let {needs} = this.context.store.getState();
    let {template, index, values} = this.state;
    if(init){
      values = _.cloneDeep(needs.pda);
    }
    let error = {};
    _n.validatePDA(this.context, template, changedValues, error);
    //changedValues.isCompleted = error.code?false:true;
    let {completed: _completed, lastIndex} = this.getStepperPageIndex(error);
    let completed = _completed;
    if(_.isNumber(changedValues.lastStepIndex)){
      if(completed > changedValues.lastStepIndex){
        completed = changedValues.lastStepIndex;
      }
    }
    else {
      completed = -1;
    }

    let headers = [], lastHeaderIndex = 0;
    _.forEach(template.items, (item, i)=>{
      let {title, disabled} = item;
      disabled = disabled || _.at(error, `sections[${i}].skip`)[0];
      if(!disabled){
        lastHeaderIndex = i;
      }
      headers.push({id: i, title, disabled});
    })

    let stepper = {completed, headers, validStep: _completed};
    if(init){
      stepper.index= completed + 1>= headers.length? headers.length - 1: headers[completed+1].disabled?completed: completed + 1;
      if(_completed > changedValues.lastStepIndex){ 
        changedValues.lastStepIndex = changedValues.lastStepIndex + 1;
        this.state.hasChange = true;
        stepper.completed = stepper.completed + 1;
      }
      //if first time init and auto select myself
      if(!changedValues.lastStepIndex && !_.get(error, 'sections[0].code')){
        stepper.completed = 0;
      }
      //get the inital active step
      stepper.active = 0;
      for(let i = 0; i < 3; i++){
        if(!error.sections[i].code && !error.sections[i+1].disabled && !error.sections[i+1].skip && i <= stepper.completed){
          stepper.active = i+1;
        }
      }
    }
    let {lastStepIndex: lsIdx = 0} = changedValues;
    let currentIndex = init?(lsIdx<0? 0: lsIdx):this.context.stepperIndex
    let valid = !_.at(error, `sections[${currentIndex}].code`)[0]; 
    this.context.updateAppbar({itemsIndex: lastIndex<=currentIndex? (valid?3:2) : (valid? 1: 0)})
    this.context.updateStepper(stepper);
    this.setState({changedValues, values, template, error, lastHeaderIndex});
  }
  
  onChange = () =>{
    let currentIndex = this.context.stepperIndex;
    let {changedValues} = this.state;
    changedValues.lastStepIndex = currentIndex;
    changedValues.isCompleted = false;
    this.state.hasChange = true;
  }

  rollback = () =>{
    let {store} = this.context;
    let {pda} = store.getState().needs;
    this.state.hasChange = false;
    this.state.showUpdDialog = false;
    this.validate(_.cloneDeep(pda), false);
  }

  getContent=()=>{
    let {stepperIndex, lang, langMap, store} = this.context;
    let { template, changedValues, values, error, errorCode, showUpdDialog} = this.state;
    if(!checkExist(template, 'items'))
      return;
    let field=template.items[stepperIndex];

    const updActions = [
      (
        <FlatButton
          key="profileDialog-updFM-cancel-btn"
          label="No"
          onTouchTap={this.rollback}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updFM-confirm-btn"
          label="Yes"
          onTouchTap={()=>{
            this.onSave(this.state.callback, true);
          }}
        />
      )
    ]
    
    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <Dialog open={showUpdDialog} actions={updActions}>
          {_v.getErrorMsg(null, errorCode, lang)}
        </Dialog>
        <DynNeedPage
          template={field}
          changedValues={changedValues}
          error={error}
          values={values}
          onChange={this.onChange}
          validate={this.validate}
        />
      </div>
    )
  }

  render() {
    let {pageOpen} = this.context;

    if(!pageOpen){
      return <div/>
    }

    let content = this.getContent();

    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column'}}>
        {content}
      </div>
    );
  }
}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  rootTemplate: PropTypes.object
});

export default Main;
