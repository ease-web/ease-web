import React from 'react';
import {FlatButton} from 'material-ui';

import _cloneDeep from 'lodash/fp/cloneDeep';
import _get from 'lodash/fp/get';
import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import FNAReport from './FNAReport';
import EABComponent from '../../../../Component';
import EmailDialog from '../../../../Dialogs/EmailDialog';
import EmailAddressDialog from '../../../../Dialogs/EmailAddressDialog';

import styles from '../../../../Common.scss';
import {sendEmail, closeEmail, closeEmailAddrInputDialog, skipEnterEmailAddr, openFNAEmailDialog} from '../../../../../actions/needs';
import {saveProfile} from '../../../../../actions/client';

class FNAMain extends EABComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      emailOpen: false,
      emailAddrInputDialogOpen: false,
      emails: null,
      pdfStr: '',
      skipEmailInReport: false
    });
  }

  open=()=>{
    this.FNAReport.init();
    this.floatingPage.open();
    this.state.skipEmailInReport = false;
  }

  close=()=>{
    this.floatingPage.close();
  }

  onSend(emails) {
    sendEmail(this.context, emails, this.state.skipEmailInReport);
  }

  onClose(){
    closeEmail(this.context);
  }

  pdfStrOnChange(pdfStr) {
    this.setState({pdfStr});
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(() => {
      const needs = store.getState().needs;
      let newState = {};
      
      if (this.state.emailOpen !== needs.emailOpen) {
        newState.emailOpen = needs.emailOpen;
      }
      if (this.state.emails !== needs.emails) {
        newState.emails = needs.emails;
      }
      if (this.state.emailAddrInputDialogOpen !== needs.emailAddrInputDialogOpen) {
        newState.emailAddrInputDialogOpen = needs.emailAddrInputDialogOpen;
      }
      if (!isEmpty(newState)) {
        this.setState(newState);
      }
    });
  }

  componentWillUnmount (){
    this.unsubscribe && this.unsubscribe();
  }

  getEmailAddrInputDialog() {
    const {emailAddrInputDialogOpen} = this.state;

    return (
      <EmailAddressDialog 
        open={emailAddrInputDialogOpen}
        onSave={(emailAddr) => {
          this.onSavingEmailAddr(emailAddr); 
          this.state.skipEmailInReport = true;
        }}
        onClose={() => this.onEmailAddrInputDialogClose()}
        onSkip={() => this.onSkipEnteringEmailAddr()}
      />
    );
  }

  onSkipEnteringEmailAddr() {
    skipEnterEmailAddr(this.context);
  }

  onSavingEmailAddr(emailAddr) {
    const {client} = this.context.store.getState();
    let profile = _cloneDeep(_get('profile', client));
    profile.email = emailAddr;
    saveProfile(this.context, profile, false, true, () => {
      closeEmailAddrInputDialog(this.context);
      openFNAEmailDialog(this.context);
    });
  }

  onEmailAddrInputDialogClose() {
    closeEmailAddrInputDialog(this.context);
  }

  getEmailDialog() {
    const {langMap} = this.context;
    const {emailOpen, emails, pdfStr} = this.state;
    const {app, client} = this.context.store.getState();

    if (emails) {
      emails.forEach((email) => {
        email.attachments
      });
    }
    let emailTemplates = [];
    if (emails) {
      emails.forEach((email) => {
        emailTemplates.push({
          ...email,
          attachments: [{
            id: 'fnaReport',
            title: getLocalizedText(langMap, 'FNA_Report')
          }]
        });
      });
    }
    return (
      <EmailDialog open={emailOpen} disableAttCheckbox={true} emails={emailTemplates} onSend={(emails) => this.onSend(emails)} onClose={() => this.onClose()} />
    );
  }
  
  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage>
            <FNAReport ref={(ref)=>{this.FNAReport=ref;}}/>
            {this.getEmailDialog()}
            {this.getEmailAddrInputDialog()}
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default FNAMain;