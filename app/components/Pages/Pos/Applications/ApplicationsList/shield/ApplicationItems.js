import React from 'react';
import styles from '../../../../../Common.scss';
import EABComponent from '../../../../../Component';
import IconPopup from '../../../../../CustomViews/IconPopup';
import * as actions from '../../../../../../actions/application';
import * as shieldActions from '../../../../../../actions/shieldApplication';
// import {goSupportDocuments} from '../../../../../../actions/supportDocuments';
import {goShieldSupportDocuments} from '../../../../../../actions/shieldSupportDocuments';
import {getProposalByQuotId} from '../../../../../../actions/quotation';
import MultiClientProfile from '../../../../../Dialogs/MultiClientProfile';
import WarningDialog from '../../../../../Dialogs/WarningDialog';
import ConfirmDialog from '../../../../../Dialogs/ConfirmDialog';
import * as _ from 'lodash';
import {FlatButton,Paper,Checkbox, Dialog,Popover, IconButton} from 'material-ui';
import {getIcon} from '../../../../../Icons';
import { getAppListView } from '../../../../../../actions/application';
import { requoteInvalid } from '../../../../../../actions/quotation';

import * as _c from '../../../../../../utils/client';
import Constants from '../../../../../../constants/SystemConstants';
import eAppConstants from '../../../../../../../common/EAppConstants';

class ApplicationItems extends EABComponent {

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state,{
            inited: false,
            error: null,
            warningMsg: null,
            confirmMsg: null,
            onConfirm: null,
            idClickCount: 0,
            showApplicationId: false,
            anchorEl: null,
            planListExpanded: false
        });
    }

    componentDidMount() {
        const {store} = this.context;
        if (!this.inited) {
        this.unsubscribe = store.subscribe(() => {
            if (this.unsubscribe) {
                const products = store.getState().products;
                let newState = {};
                if (!window.isEqual(this.state.error, products.error)) {
                    newState.error = products.error;
                }
                if (!_.isEmpty(newState)) {
                    this.setState(newState);
                }
            }
        });
        this.inited = true;
        }
    }

    componentWillUnmount(){
        this.unsubscribe();
        this.unsubscribe = null;
    };

    componentWillReceiveProps(nextProps){
        let insuredList;
        if (_.get(nextProps.values, 'type') === 'masterApplication') {
            insuredList = _.get(nextProps.values, 'quotation.insureds', []);
        } else {
            insuredList = _.get(nextProps.values, 'insureds', []);
        }

        if (_.keys(insuredList).length < 2) {
            this.setState({planListExpanded: true});
        }
    }

    validateClientProfile = (method, needOpenDialog = false) =>{
        let { appList, client, pos } = this.context.store.getState();
        let { invalid } = appList;
        let { profile } = client;
        let {values, isFNACompleted} = this.props;
        let {insureds, pCid} = values;
        let arrCid = _.keys(insureds);
        if (!_.includes(arrCid, pCid)) {
            arrCid.push(pCid);
        }

        if (!pos.showFnaInvalidFlag) {
            if (invalid.initApplications) {
                getAppListView(this.context, profile);
            }else if (this.multiClientProfile.validateValues(arrCid)) {
                if (method === 'goAppForm'){
                    this.goAppForm();
                } else if (method === 'goApplication'){
                    this.goApplication();
                }
            } else if (needOpenDialog) {
                this.multiClientProfile.openDialog(arrCid);
            }
        } else {
            // isFNACompleted is false
            this.setState({
              warningMsg: 'Cannot apply/continue application, please check previous processes.'
            });
        }
    }

    clientChoiceTicked = (applicationId, isInputChecked) => {
        this.props.clientChoiceCallback(applicationId, isInputChecked);
    }

    goAppForm = () => {
        shieldActions.applyAppForm(this.context, this.props.values.id);
        // actions.apply(this.context, this.props.values.id);
    }

    goUploadDocument = () => {
        goShieldSupportDocuments(this.context, this.props.values.id, 'SUBMITTED');
    }

    goViewProposal = () => {
        let application = this.props.values;
        let type = application.type;
        let id = type === 'quotation' ? application.id : application.quotationDocId;
        getProposalByQuotId(this.context, id);
    }

    requoteInvalid = () => {
      let {values} = this.props;
      let type = values.type;
      let id = type === 'quotation' ? values.id : type === 'application' ? values.quotationDocId : '';
      requoteInvalid(this.context, id, false,
        (confirmMsg) => {
          this.setState({
            confirmMsg,
            onConfirm: () => {
              this.setState({
                confirmMsg: null,
                onConfirm: null
              });
              requoteInvalid(this.context, id, true);
            }
          });
        },
        (errorMsg) => {
          this.setState({ warningMsg: errorMsg });
        }
      );
    }

    goApplication = () => {
        shieldActions.continueApplication(this.context, this.props.values.id);
        // actions.goApplication(this.context, this.props.values.id);
    }

    confirmAppPaid = () => {
        var self = this;

        shieldActions.confirmAppPaid(this.context, this.state.confirmPaidAppID, (result) => {
            self.setState({
              confirmMsg: null,
              onConfirm: null,
              confirmPaidAppID: ''
            });
            if (result) {
                const {client, appList} = this.context.store.getState();
                setTimeout(function() {
                    if (client && client.profile) {
                        actions.getAppListView(self.context, client.profile, appList.bundleId);
                    }
                }, 100);
            }
        });
    }

    getInsuredProfileFromStore(iCid){
        const {client} = this.context.store.getState();
        return client.dependantProfiles[iCid];
    }

    getProfileFromStore(){
        const {client} = this.context.store.getState();
        return client.profile;
    }

    getPersonName = (pName, iName, pCid, iCid) => {
        let nameLabel = '';
        if (iName) {
            if (pName) {
                if (iName === pName && pCid === iCid){
                    nameLabel = iName;
                } else {
                    nameLabel = pName + ' , ' + iName;
                }
            } else {
                nameLabel = iName;
            }
        }
        return nameLabel;
    }

    getPlansDetail = () => {
        var {lang, optionsMap} = this.context;
        const ccy = 'SGD';
        var plansDetail = [];
        // let payMode = 'quotation.payMode_' + paymentMode;
        let insuredList;
        if (_.get(this.props.values, 'type') === 'masterApplication') {
            insuredList = _.get(this.props.values, 'quotation.insureds', []);
        } else {
            insuredList = _.get(this.props.values, 'insureds', []);
        }
        let index = 0;

        _.forEach(insuredList, (insured) => {
            let planList = _.get(insured, 'plans');
            _.forEach(planList, (plan, planIndex) => {
                // Carlton TODO:
                // Fill in 'policyType' in second coloum
                // fourth coloum: benefit Amount
                // sixth coloum: premium mode
                let benefitAmount;
                // let planPremium = planIndex === 0 ? _.get(insured, 'policyOptions.axaShield') : plan.premium;
                let planPremium = _.get(insured, 'plans.' + planIndex + '.premium');
                let fontWeight = planIndex === 0 ? '500' : '400';
                const policyTypeName = _.get(optionsMap, 'productLine.options[' + _.get(plan, 'productLine') + '].title.' + lang, '');

                switch (_.get(plan, 'covClass')) {
                    case 'A':
                        benefitAmount = 'Plan A';
                        break;
                    case 'B':
                        benefitAmount = 'Plan B';
                        break;
                    case 'C':
                        benefitAmount = 'Standard Plan';
                }
                plansDetail.push(
                    <tr key={'appItem_' + index}>
                        <td key={'planName_' + index} style={planIndex === 0 ? {fontWeight: fontWeight, paddingLeft: 12} : {paddingLeft: 12}}>{window.getLocalText(lang, plan.covName)}</td>
                        <td key={'policyType' + index} style={{textAlign: 'right'}}>{policyTypeName}</td>
                        <td key={'iFullName' + index} style={{textAlign: 'right'}}  >{_.get(insured, 'iFullName')}</td>
                        <td key={'benefitAmount' + index} style={{textAlign: 'right'}}>{benefitAmount}</td>
                        <td key={'premium' + index} style={{textAlign: 'right'}}>{window.getCurrencyByCcy(planPremium, ccy, 2)}</td>
                        <td key={'premiumMode' + index} style={{paddingLeft: '140px', textAlign: 'left'}}>RP</td>
                    </tr>
                );
                index++;
            });
        });
        return plansDetail;
    }

    getHeaderButtons = () => {
        let self = this;
        let {muiTheme} = this.context;
        let {values, clientChoiceDisabled, isSelectedProposal, clientChoiceFinished, onItemSelecting, agentChannel} = this.props;
        let {type, appStatus, id, isInitialPaymentCompleted, isMandDocsAllUploaded} = values;
        let {langMap} = this.context;

        const requoteBtn = (
          <FlatButton
            label={window.getLocalizedText(langMap, 'proposal.btn.requote')}
            disabled={onItemSelecting}
            className={styles.FlatButton}
            onClick={() => this.requoteInvalid()}
          />
        );

        // The rightest button
        var firstButton;
        if (type === 'application' || type === 'masterApplication') {
            if (appStatus === 'SUBMITTED') {
                firstButton = <FlatButton
                                label={'SUPPORTING DOCS'}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goUploadDocument()}
                                icon={isMandDocsAllUploaded ? null : getIcon('warning', muiTheme.palette.warningColor)}
                            />
            }
            else if (appStatus === 'APPLYING') {
                firstButton = <FlatButton label={getLocalizedText(langMap, 'application.continue')}
                                disabled = {onItemSelecting || ((!clientChoiceFinished) && agentChannel == 'AGENCY')}
                                className={styles.FlatButton}
                                onClick={()=>this.validateClientProfile('goApplication', true)}/>
            }
            else if (appStatus === 'INVALIDATED') {
                firstButton = <FlatButton
                    label={'SUPPORTING DOCS'}
                    disabled={onItemSelecting}
                    className={styles.FlatButton}
                    onClick={()=>this.goUploadDocument()}
                />
                // firstButton = <FlatButton
                //     label={getLocalizedText(langMap, 'application.view_proposal')}
                //     disabled={onItemSelecting}
                //     className={styles.FlatButton}
                //     onClick={()=>this.goViewProposal()}
                // />
            }
            else if (appStatus === 'INVALIDATED_SIGNED') {
                firstButton =   <div style={{padding: '14px 24px', display:'inline-block'}}>
                                    <Checkbox
                                        label={getLocalizedText(langMap, 'Paid')}
                                        style={{
                                            fontSize: 14,
                                            fontWeight: 500
                                        }}
                                        labelStyle={{
                                            marginLeft: '-10px',
                                            color: muiTheme.palette.primary2Color
                                        }}
                                        disabled = {isInitialPaymentCompleted}
                                        checked = {isInitialPaymentCompleted}
                                        onCheck={
                                            function(event, isInputChecked) {
                                                if (isInputChecked) {
                                                    self.setState({
                                                      confirmMsg: window.getLocalizedText(langMap, 'application_list.paid.confirm'),
                                                      confirmPaidAppID: this.appId,
                                                      onConfirm: () => self.confirmAppPaid()
                                                    });
                                                }
                                            }.bind({appId: values.id})
                                        }
                                    />
                                </div>
            }
        }
        // In the PROPOSED Tab
        else if (type === 'quotation') {
            if (appStatus === 'INVALIDATED' || appStatus === 'INVALIDATED_SIGNED') {
                firstButton = <FlatButton
                                label={window.getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}
                                />
            } else {
                firstButton = <FlatButton
                    label={window.getLocalizedText(langMap, 'application.apply')}
                    disabled={onItemSelecting || (agentChannel == 'AGENCY' && (!isSelectedProposal || !clientChoiceFinished ))}
                    className={styles.FlatButton}
                    onClick={()=>this.validateClientProfile('goAppForm', true)}
                />
            }
        }

        var secondButton;
        if (type === 'application' || type === 'masterApplication') {
            if (appStatus === 'APPLYING') {
                secondButton = <FlatButton label={window.getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}/>
            }
            else if (appStatus === 'INVALIDATED_SIGNED') {
                secondButton = <FlatButton
                    label={'SUPPORTING DOCS'}
                    disabled={onItemSelecting}
                    className={styles.FlatButton}
                    onClick={()=>this.goUploadDocument()}
                />
                // secondButton = <FlatButton
                //                     label={getLocalizedText(langMap, 'application.view_proposal')}
                //                     disabled={onItemSelecting}
                //                     className={styles.FlatButton}
                //                     onClick={()=>this.goViewProposal()}
                //                 />
            }
            else if (appStatus === 'INVALIDATED') {
              secondButton = requoteBtn;
            }
        }
        // In the PROPOSED Tab
        else if (type === 'quotation' && appStatus !== 'INVALIDATED_SIGNED' && appStatus !== 'INVALIDATED') {
                secondButton = <FlatButton label={window.getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}/>
        }
        var thirdButton = null;
        // In the PROPOSED Tab
        if (type === 'quotation') {
          if (appStatus !== 'INVALIDATED_SIGNED' && appStatus !== 'INVALIDATED') {
            // checked = {isSelectedProposal}
            thirdButton = this.props.isAtTabAll || agentChannel === 'FA' ?
                null
                : <div style={{padding: '14px 12px', display:'inline-block'}}>
                    <Checkbox label={window.getLocalizedText(langMap, 'application.choice')}
                        style={{
                            fontSize: 14,
                            fontWeight: 500
                        }}
                        labelStyle={{
                            marginLeft: '-10px',
                            color: muiTheme.palette.primary2Color
                        }}
                        disabled = {onItemSelecting || clientChoiceDisabled}
                        defaultChecked = {isSelectedProposal}
                        onCheck={(event, isInputChecked) => {
                            this.clientChoiceTicked(id, isInputChecked);
                        }}
                    />
                </div>
          } else {
            thirdButton = requoteBtn;
          }
        }
        return {
            'firstButton':firstButton,
            'secondButton':secondButton,
            'thirdButton':thirdButton
        };
    }

    handleShowIdOnClick = (e) => {
      let {idClickCount, showApplicationId} = this.state;
      let {type, policyNumber} = this.props.values;
      const SHOW_ID_CLICK_COUNT = 10;

      if (type === 'application' || type === 'masterApplication') {
        idClickCount += 1;
        this.setState({
          idClickCount: idClickCount,
          showApplicationId: idClickCount >= SHOW_ID_CLICK_COUNT && !_.isEmpty(policyNumber),
          anchorEl: e.currentTarget
        });
      }
    }

    handleHideIdOnClick = () => {
      this.setState({
        idClickCount: 0,
        showApplicationId:false
      });
    }

    headerView = () => {
        let {lang, langMap, muiTheme} = this.context;
        let {values,onItemSelecting,itemDeleteCallback,disabledDelete} = this.props;
        let {id, policyNumber, plans, type, appStatus, invalidateReason, pCid, iCid, iName, pName} = values;
        // let sNo = (appStatus && appStatus== getLocalizedText(langMap, 'application_list.submitted') && typeof(sequenceNo) !== "undefined" && sequenceNo !== null ) ? (getLocalizedText(langMap, 'application_list.sequence_no', 'Policy No.') + ": " + values.sequenceNo) : "";

        let covName = window.getLocalizedText(langMap, 'application_list.product_name_shield');
        let {firstButton, secondButton, thirdButton} = this.getHeaderButtons();
        let isApplyingApp = appStatus === 'APPLYING';
        // paymentMethod hardcoded for SHIELD
        const paymentMethod = 'RP';

        let nameLabel;
        if (type === 'application' || type === 'masterApplication' || type === 'quotation'){
            nameLabel = this.getPersonName(pName, iName, pCid, iCid);
        }
        let insuredList;
        if (_.get(this.props.values, 'type') === 'masterApplication') {
            insuredList = _.get(this.props.values, 'quotation.insureds', []);
        } else {
            insuredList = _.get(this.props.values, 'insureds', []);
        }
        // NOTE: lucia
        return (
            <div style={{widht: '100%',display: 'flex',flexDirection: 'row',
                lineHeight: '24px',padding: '8px ' + muiTheme.spacing.desktopGutterLess + 'px 0 ' + muiTheme.spacing.desktopGutter + 'px',
                position: 'relative'}}
            >
                {/*Delete SelectingModeIcon*/}
                {onItemSelecting ?
                    <div style={{padding: '12px 0', display:'inline-block'}}>
                        <Checkbox
                          disabled={disabledDelete}
                          onCheck={(event, value) => {itemDeleteCallback(id, isApplyingApp, value);}}/>
                    </div>
                : null}
                {/*Basic Product Name*/}
                {/*(Application Number)*/}
                {/*(Proposer Name/Life Insured name)*/}
                <div style={{width: 400, display:'flex', flexDirection:'column', flex: 1}}>
                    <div style={{display:'flex', alignItems: 'center', fontWeight: '500', color: muiTheme.palette.primary2Color}} onClick={this.handleShowIdOnClick}>
                        {window.getLocalText(lang, covName)}{' (' + paymentMethod + ')' }{' (' + id + ')'}{_.get(values, 'approvalStatus') ? ('  --  ' + _.get(values, 'approvalStatus')) : ''}
                        {_.keys(insuredList).length > 1 ?
                            <IconButton
                                onTouchTap={() => this.setState({planListExpanded: !this.state.planListExpanded})}
                            >
                                {this.state.planListExpanded ?
                                    getIcon('expandLess', muiTheme.palette.primary2Color)
                                    : getIcon('expandMore', muiTheme.palette.primary2Color)}
                            </IconButton>
                            : null}
                        {invalidateReason ? (
                          <IconPopup
                            style={{ padding: '0 12px' }}
                            content={(
                              <div style={{ display: 'flex', alignItems: 'center' }}>
                                {getIcon('info', muiTheme.palette.primary2Color)}
                                <span style={{ marginLeft: '16px' }}>{window.getLocalizedText(langMap, 'application.invalidate.reasons.' + invalidateReason)}</span>
                              </div>
                            )}
                          />
                        ) : null}
                        {<Popover
                          open={this.state.showApplicationId}
                          anchorEl={this.state.anchorEl}
                          anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                          targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
                          onRequestClose={this.handleHideIdOnClick}
                          style={{overflow:'hidden', fontWeight: '500', color: muiTheme.palette.primary2Color}}
                          >
                            <div style={{overflow:'hidden'}}>
                              <span style={{opacity: 1}}>{window.getLocalText(lang, covName)} </span>
                              <span>({id})</span>
                            </div>
                          </Popover>
                        }
                        {values[eAppConstants.PAYMENTTRANSFERRED] ? '  --  TRANSFERRED' : null}
                    </div>
                    <div style={{display:'inline-block', color:'gray', fontSize:'14px'}}>{nameLabel}</div>
                </div>
                {/*firstButton is the rightest button*/}
                <div style={{position:'absolute',right:12, top: 8, zIndex:10}}>
                    {thirdButton}
                    {secondButton}
                    {firstButton}
                </div>
            </div>
        );
    }

    contentView = () => {
        let {langMap} = this.context;
        let plansDetail = this.getPlansDetail();

        return (
            <table key={"table"} className={styles.Table} style={{width: 'calc(100% - 48px)', margin: '24px 24px 0 24px', borderBottom: '1px solid #c4c4c4'}}>
                <thead>
                <tr key={"htr"} className={styles.TableHeader}>
                    <th key={"h1"} style={{width: '200px', paddingLeft: 12}}>Plan Names</th>
                    <th key={"h2"} style={{width: '100px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.policy_type')}</th>
                    <th key={"h3"} style={{width: '200px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.life_assured')}</th>
                    <th key={"h4"} style={{width: '150px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.benefit_amount')}</th>
                    <th key={"h5"} style={{width: '220px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.premium_shield') + ' (incl. Medishield Life)'}</th>
                    <th key={"h6"} style={{paddingLeft: '40px', textAlign: 'left'}}>{window.getLocalizedText(langMap, 'application_list.premium_mode_shield')}</th>
                </tr>
                </thead>
                <tbody>
                    {plansDetail}
                </tbody>
            </table>
        );
    }

    footerView = () => {
        const ccy = 'SGD';
        let {langMap} = this.context;
        let {values} = this.props;
        var {lastUpdateDate, paymentMode, policyOptions} = values;
        let payMode = window.getLocalizedText(langMap, 'application_list.payMode_' + paymentMode);
        let quotation;
        if (_.get(this.props.values, 'type') === 'masterApplication') {
            quotation = _.get(this.props.values, 'quotation', {});
        } else {
            quotation = this.props.values;
        }
        // Carlton TODO: 1.get totPremium from quotation when quotation is ready
        // 2. get medisavePremium
        // 3. get cashPremium
        // let medisavePremium = _.get(quotation, 'totCpfPortion');
        let medisavePremium = _.get(quotation, 'totMedisave');
        let cashPremium = _.get(quotation, 'totCashPortion');
        let totPremium = _.get(quotation, 'totPremium');

        lastUpdateDate = lastUpdateDate ? (new Date(lastUpdateDate)).format('dd mmm yyyy HH:MM:ss') : '';
        // let lastUpdateDateLabelWidth = 'calc(100% - 1170px)';
        let lastUpdateDateLabelWidth = '190px';
        return (
            <div style={{ width: 'calc(100% - 72px)', margin: '0 24px', lineHeight: '48px', fontSize: '14px', padding: '0 12px 0 12px'}}>
                <div className={styles.SubContent} style={{display: 'inline-block', width: lastUpdateDateLabelWidth}}>{lastUpdateDate}</div>
                <div style={{display: 'inline-block', width: '120px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.total_medisave_shield')}</div>
                <div style={{display: 'inline-block', width: '100px', textAlign: 'right'}}>{window.getCurrencyByCcy(medisavePremium, ccy, 2)}</div>
                <div style={{display: 'inline-block', width: '150px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.total_cash_shield')}</div>
                <div style={{display: 'inline-block', width: '100px', textAlign: 'right'}}>{window.getCurrencyByCcy(cashPremium, ccy, 2)}</div>
                <div style={{display: 'inline-block', width: '280px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.total_premium_shield') + ' (incl. Medishield Life)'}</div>
                <div style={{display: 'inline-block', width: '100px', textAlign: 'right'}}>{window.getCurrencyByCcy(totPremium, ccy, 2)}</div>
            </div>
        );
    }

    render() {
        let self = this;
        let {isSelectedProposal} = this.props;
        let clientChoiceTickedStyle = {
            width: '100%',
            margin: '0 0 6px',
            backgroundColor:'rgba(0, 0, 143, 0.07)',
            border: '1px solid rgba(0, 0, 143px, 0.17)'
        };
        let {
            langMap,
            store
        } = this.context;

        let {
            agentProfile = {}
        } = store.getState().app;
        let features = agentProfile.features || "";
        let hasFNA = features.indexOf("FNA") > -1? true: false;

        let insuredList;
        if (_.get(this.props.values, 'type') === 'masterApplication') {
            insuredList = _.get(this.props.values, 'quotation.insureds', []);
        } else {
            insuredList = _.get(this.props.values, 'insureds', []);
        }
        let planListExpanded = _.keys(insuredList).length < 2 || this.state.planListExpanded;

        return (
            <div>
                <div>
                    <Paper style={isSelectedProposal ? clientChoiceTickedStyle :{width: '100%', margin: '0 0 6px'}}>
                        <MultiClientProfile
                            ref={ref => { this.multiClientProfile = ref; }}
                            pMandatoryId={_c.genProfileMandatory(Constants.FEATURES.EAPP, hasFNA, true, false)}
                            iMandatoryId={_c.genProfileMandatory(Constants.FEATURES.EAPP, hasFNA, false, false)}
                            onSubmit={()=>{this.validateClientProfile('goAppForm');}}
                        />
                        {this.headerView()}
                        {planListExpanded ? this.contentView() : null}
                        {planListExpanded ? this.footerView() : null}
                    </Paper>
                </div>
                <WarningDialog
                  open={!!this.state.warningMsg}
                  msg={this.state.warningMsg}
                  onClose={() => this.setState({ warningMsg: null })}
                />
                <ConfirmDialog
                    open={!!this.state.confirmMsg}
                    confirmMsg={this.state.confirmMsg}
                    onClose={() => {
                        self.setState({
                          confirmMsg: null,
                          confirmPaidAppID: '',
                          onConfirm: null
                        });
                    }}
                    onConfirm={() => this.state.onConfirm && this.state.onConfirm()}
                />
            </div>
        );
    }
}

export default ApplicationItems;
