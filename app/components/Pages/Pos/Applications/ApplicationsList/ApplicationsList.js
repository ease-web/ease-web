import React, { PropTypes} from 'react';
import {IconButton,Tabs,Tab,FlatButton,Dialog} from 'material-ui';
import * as _ from 'lodash';
import {deleteApplications, incompleteClientChoice, saveQuotCheckedList} from '../../../../../actions/application';
import {initClientChoiceStore} from '../../../../../actions/clientChoice';
import {updateProfileAppCount} from '../../../../../actions/client';

import styles from '../../../../Common.scss';
import SwipeableViews from 'react-swipeable-views';
import EABComponent from '../../../../Component';
import ApplicationItems from './ApplicationItems';
import ShieldItem from './shield/ApplicationItems';

import {getIcon} from '../../../../Icons';

class ApplicationsList extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state,{
            tabSelected: props.template.isFromCreateProposal === true? 1: 0,
            onItemSelecting: false,
            deleteItemsSelectedArray:[],
            deleteApplyingAppCount:0,
            deleteButtonDisabled: true,
            quotCheckedList: props.template.quotCheckedList,
            clientChoiceDisabled: props.template.clientChoiceDisabled,
            showDeleteDialog:false,
        });
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
      if (!_.isEqual(this.props.template, nextProps.template)) {
        let { clientChoiceDisabled, quotCheckedList } = nextProps.template;
        this.setState({
          quotCheckedList,
          clientChoiceDisabled
        });
      }
    }

    handleChange = (value) => {
        this.setState({tabSelected: value})
    };

    selectButtonClicked = () => {
        this.setState({
            onItemSelecting: true
        });
    }

    deleteButtonClicked = () => {
        var {deleteItemsSelectedArray} = this.state;
        if (deleteItemsSelectedArray.length>0) {
            this.setState({showDeleteDialog:true});
        } else {
            this.closeButtonClicked();
        }
    }

    confirmDeleteApps = () => {
        var {deleteItemsSelectedArray, deleteApplyingAppCount} = this.state;
        let rollbackBundle = false;
        let {clientChoiceFinished} = this.props.template;
        let {itemsTemplate} = this.props.template;
        let {allItems} = itemsTemplate;
        if (itemsTemplate.applyingItems.length === deleteApplyingAppCount) {
            rollbackBundle = true;
        }
        deleteApplications(this.context, deleteItemsSelectedArray, allItems,
            clientChoiceFinished, rollbackBundle);
        this.setState({
            deleteItemsSelectedArray:[],
            deleteApplyingAppCount:0,
            onItemSelecting: false,
            showDeleteDialog: false
        });
    }

    closeButtonClicked = () => {
        this.setState({
            onItemSelecting: false,
            deleteItemsSelectedArray:[],
            deleteApplyingAppCount:0,
            deleteButtonDisabled: true
        });
    }

    openClientChoice = () => {
        let {quotCheckedList, clientChoiceDisabled} = this.state;
        let proposedItemIds = this.proposedItemIds;

        initClientChoiceStore(this.context, quotCheckedList, clientChoiceDisabled);
        saveQuotCheckedList(this.context, quotCheckedList);
    }

    clientChoicesTicked = (applicationId, isInputChecked) => {
        let {clientChoiceFinished} = this.props.template;
        var {quotCheckedList} = this.state;

        if (clientChoiceFinished) {
            this.setState({'clientChoiceFinished':false});
            incompleteClientChoice(this.context);
        }

        let index = quotCheckedList.findIndex((item) => { return item.id == applicationId });
        if (index > -1) { quotCheckedList[index].checked = isInputChecked; }

        this.setState({'quotCheckedList': quotCheckedList})
    }

    appItemDelete = (itemId, isApplyingApp, checked) => {
        var {deleteItemsSelectedArray, deleteApplyingAppCount} = this.state;

        if (checked) {
            deleteItemsSelectedArray.push(itemId);
            if (isApplyingApp) deleteApplyingAppCount += 1;
        } else {
            var index = deleteItemsSelectedArray.indexOf(itemId);
            if(index != -1) {
                deleteItemsSelectedArray.splice(index, 1);
                if (isApplyingApp) deleteApplyingAppCount -= 1;
            }
        }
        if (deleteItemsSelectedArray.length > 0) {
            this.setState({
                deleteButtonDisabled: false,
                deleteApplyingAppCount: deleteApplyingAppCount
            });
        } else {
            this.setState({
                deleteButtonDisabled: true,
                deleteApplyingAppCount: deleteApplyingAppCount
            });
        }
    }

    genApplicationsList = () => {
        let {clientChoiceDisabled, onItemSelecting, quotCheckedList, tabSelected} = this.state;
        let {clientChoiceFinished, itemsTemplate, agentChannel, isFNACompleted} = this.props.template;
        let {isFullySigned, proposedItemIds} = itemsTemplate;
        this.allItems = [];
        this.proposedItems = [];
        this.applyingItems = [];
        this.submitedItems = [];
        this.invalidatedSignedItems = [];
        this.invalidatedItems = [];
        this.proposedItemIds = proposedItemIds;
        
        if (itemsTemplate.proposedItems){
            itemsTemplate.proposedItems.forEach((item, index)=>{
                let isSelectedProposal = false;
                if (quotCheckedList){
                    let quotChecked = quotCheckedList.find((quotItem)=>{
                        return quotItem.id === item.id;
                    });
                    if (quotChecked) isSelectedProposal = quotChecked.checked;
                }

                if (_.get(item, 'quotType') === 'SHIELD') {
                    this.proposedItems.push(
                        <ShieldItem
                            key={'proposedItems' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceDisabled={clientChoiceDisabled}
                            isSelectedProposal={isSelectedProposal}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            applyCallback={(quotationId) => {this.openAppFormsPage(quotationId);}}
                            clientChoiceCallback={(applicationId, isInputChecked)=>{this.clientChoicesTicked(applicationId, isInputChecked);}}
                            applyingRecordsCount={itemsTemplate.applyingItems.length}
                            disabledDelete={agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                    this.allItems.push(
                        <ShieldItem
                            key={'allItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceDisabled={true}
                            isSelectedProposal={isSelectedProposal}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            applyCallback={(quotationId) => {this.openAppFormsPage(quotationId);}}
                            clientChoiceCallback={(applicationId, isInputChecked)=>{this.clientChoicesTicked(applicationId, isInputChecked);}}
                            isAtTabAll={true}
                            applyingRecordsCount={itemsTemplate.applyingItems.length}
                            disabledDelete={agentChannel === 'FA' && item.isFullySigned || agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                } else {
                    this.proposedItems.push(
                        <ApplicationItems
                            key={'proposedItems' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceDisabled={clientChoiceDisabled}
                            isSelectedProposal={isSelectedProposal}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            applyCallback={(quotationId) => {this.openAppFormsPage(quotationId);}}
                            clientChoiceCallback={(applicationId, isInputChecked)=>{this.clientChoicesTicked(applicationId, isInputChecked);}}
                            applyingRecordsCount={itemsTemplate.applyingItems.length}
                            disabledDelete={agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                    this.allItems.push(
                        <ApplicationItems
                            key={'allItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceDisabled={true}
                            isSelectedProposal={isSelectedProposal}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            applyCallback={(quotationId) => {this.openAppFormsPage(quotationId);}}
                            clientChoiceCallback={(applicationId, isInputChecked)=>{this.clientChoicesTicked(applicationId, isInputChecked);}}
                            isAtTabAll={true}
                            applyingRecordsCount={itemsTemplate.applyingItems.length}
                            disabledDelete={agentChannel === 'FA' && item.isFullySigned || agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                }
            });
        }
        if (itemsTemplate.applyingItems){
            itemsTemplate.applyingItems.forEach((item, index)=>{
                
                if (item.type === 'masterApplication') {
                    this.applyingItems.push(
                        <ShieldItem
                            key={'applyingItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            disabledDelete={agentChannel === 'FA' && item.isFullySigned || agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                } else {
                    this.applyingItems.push(
                        <ApplicationItems
                            key={'applyingItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            clientChoiceFinished={clientChoiceFinished}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            disabledDelete={agentChannel === 'FA' && item.isFullySigned || agentChannel !== 'FA' && isFullySigned}
                            agentChannel={agentChannel}
                            isFNACompleted={isFNACompleted}
                        />
                    );
                }
            });
        }
        if (itemsTemplate.submitedItems){
            itemsTemplate.submitedItems.forEach((item, index)=>{
                if (item.type === 'masterApplication') {
                    this.submitedItems.push(
                        <ShieldItem
                            key={'submitedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            uploadDocumentCallback= {() => {this.uploadDocPage.open();}}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                } else {
                    this.submitedItems.push(
                        <ApplicationItems
                            key={'submitedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            itemDeleteCallback={(appId, isApplyingApp, checked)=>{this.appItemDelete(appId, isApplyingApp, checked)}}
                            uploadDocumentCallback= {() => {this.uploadDocPage.open();}}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                }
            });
        }
        if (itemsTemplate.invalidatedSignedItems){
            itemsTemplate.invalidatedSignedItems.forEach((item, index)=>{
                if (item.type === 'masterApplication' || _.get(item, 'quotType') === 'SHIELD' ) {
                    this.invalidatedSignedItems.push(
                        <ShieldItem
                            key={'invalidatedSignedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                } else {
                    this.invalidatedSignedItems.push(
                        <ApplicationItems
                            key={'invalidatedSignedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                }
            });
        }
        if (itemsTemplate.invalidatedItems){
            itemsTemplate.invalidatedItems.forEach((item, index)=>{
                if (item.type === 'masterApplication' || _.get(item, 'quotType') === 'SHIELD' ) {
                    this.invalidatedItems.push(
                        <ShieldItem
                            key={'invalidatedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                } else {
                    this.invalidatedItems.push(
                        <ApplicationItems
                            key={'invalidatedItem' + index}
                            status={item.appStatus}
                            values={item}
                            onItemSelecting={onItemSelecting}
                            disabledDelete={true}
                            agentChannel={agentChannel}/>
                    );
                }
            });
        }

        //proposed items have inserted before
        this.allItems = this.allItems.concat(this.applyingItems,  this.submitedItems, this.invalidatedSignedItems, this.invalidatedItems);

        // Filter by 'Recommended For You' selection
        // var filteredArray = [];
        // var filteredArray = this.allItems.filter((item)=>{return item.props.values.iCid == selectedCid});
        // this.allItems = filteredArray;
        // filteredArray = this.proposedItems.filter((item)=>{return item.props.values.iCid == selectedCid})
        // this.proposedItems = filteredArray;
        // filteredArray = this.applyingItems.filter((item)=>{return item.props.values.iCid == selectedCid})
        // this.applyingItems = filteredArray;
        // filteredArray = this.submitedItems.filter((item)=>{return item.props.values.iCid == selectedCid})
        // this.submitedItems = filteredArray;
        // filteredArray = this.invalidatedItems.filter((item)=>{return item.props.values.iCid == selectedCid})
        // this.invalidatedItems = filteredArray;
    }
// NOTE: LUCia
    applicationsListTabBarView = () => {
        var {muiTheme,langMap} = this.context;
        let {clientChoiceFinished, itemsTemplate, agentChannel} = this.props.template;
        let {onItemSelecting, clientChoiceDisabled, quotCheckedList, tabSelected} = this.state;
        let tabStyle = {
            color: muiTheme.baseTheme.palette.primary2Color,
            textTransform: 'none',
            // borderBottom: '1px solid #e4e4e4',
            borderRight: '1px solid #e4e4e4'
        };
        let appCount = this.allItems.length;
        let deleteActions = [
            <FlatButton
                label='Cancel'
                primary={true}
                onTouchTap={()=>this.setState({showDeleteDialog:false})}
            />,
            <FlatButton
                label='Delete'
                secondary={true}
                onTouchTap={this.confirmDeleteApps}
            />
        ];
        let anyProposalIsSelected = false;
        var inkLeft = ['0%', '16%', '32%', '48%', '64%', '84%'];
        quotCheckedList = quotCheckedList || [];

        quotCheckedList.forEach((item)=>{
            if (item.checked === true) anyProposalIsSelected = true;
        });

        return (
            <div style={{width: '100%', overflowX:"auto", overflowY:'hidden'}}>
            <div style={onItemSelecting ? { minWidth: '720px' } : { display: 'flex' }}>
                {/*Applications Type Tabs Bar*/}
                {onItemSelecting?
                    <div style={{display: 'inline-block', alignItems: 'center', width: 300}}>
                        <IconButton onTouchTap = {()=>this.closeButtonClicked()}>
                            {getIcon('close', muiTheme.palette.primary1Color, {marginLeft:'24px'})}
                        </IconButton>
                        <div style={{fontWeight:'bold', lineHeight:'48px', marginLeft: 24, display: 'inline-block', verticalAlign: 'top'}}>
                            {this.state.deleteItemsSelectedArray.length} Selected
                        </div>
                    </div>
                    :
                      <div style={{ flex: 1, overflowX: 'auto' }}>
                        <Tabs onChange={this.handleChange}
                            value={tabSelected}
                            style={{ width: '900px' }}
                            inkBarStyle={{
                                width: tabSelected === 4 ? "20%" : "16%" ,
                                left: inkLeft[tabSelected],
                                height: 4
                            }}>
                            <Tab label={window.getLocalizedText(langMap, 'application_list.all')+" ("+this.allItems.length+")"} style={{width: '16%'}} buttonStyle={tabStyle} value={0} />
                            <Tab label={window.getLocalizedText(langMap, 'application_list.proposed')+" ("+this.proposedItems.length+")"} style={{width: '16%'}} buttonStyle={tabStyle} value={1} />
                            <Tab label={window.getLocalizedText(langMap, 'application_list.applying')+" ("+this.applyingItems.length+")"} style={{width: '16%'}} buttonStyle={tabStyle} value={2} />
                            <Tab label={window.getLocalizedText(langMap, 'application_list.submitted')+" ("+this.submitedItems.length+")"} style={{width: '16%'}} buttonStyle={tabStyle} value={3} />
                            <Tab label={window.getLocalizedText(langMap, 'application_list.invalidatedSigned')+" ("+this.invalidatedSignedItems.length+")"} style={{width: '20%'}} buttonStyle={tabStyle} value={4} />
                            <Tab label={window.getLocalizedText(langMap, 'application_list.invalidated')+" ("+this.invalidatedItems.length+")"} style={{width: '16%'}} buttonStyle={tabStyle} value={5} />
                        </Tabs>
                      </div>
                }

                {/*CLIENT CHOICE button*/}
                <div style={{margin:0, padding: '0px 24px', float:'right',
                    height: '48px', display:"inline-block", verticalAlign:'top', textAlign: 'right'}}>
                    {/*Warning Icon*/}
                    {tabSelected === 1 && agentChannel === 'AGENCY' ? appCount === 0 || quotCheckedList.length === 0 ? null : (onItemSelecting || clientChoiceFinished) ? null : getIcon('warning', muiTheme.palette.warningColor, {padding:'13px 0', marginRight: '-25px'}) : null}
                    {/*CLIENT CHOICE button*/}
                    {tabSelected === 1 && agentChannel == 'AGENCY'? appCount ===0 ? null : onItemSelecting ? null:
                            <FlatButton
                                disabled = {anyProposalIsSelected?false:true}
                                label='RECOMMENDATION'
                                className={styles.FlatButton}
                                onTouchTap={() => this.openClientChoice()}/>
                        :null}
                    {/*SELECT button*/}
                    {onItemSelecting ?
                        <FlatButton label='DELETE SELECTED'
                                disabled={this.state.deleteButtonDisabled}
                                className={styles.FlatButton}
                                onTouchTap={()=>this.deleteButtonClicked()}/>
                        : <FlatButton label='DELETE'
                            disabled={tabSelected > 2}
                            className={styles.FlatButton}
                            onTouchTap={()=>this.selectButtonClicked()}/>}
                    <Dialog
                        title="WARNING"
                        open={this.state.showDeleteDialog}
                        actions={deleteActions}
                    >
                        Are you sure to delete the selected application(s)?
                    </Dialog>
                </div>
            </div>
            </div>
        );
    }

    applicationsListView = () => {
        var {langMap} = this.context;
        let ItemsListStyle = {}
        let emptyMsgStyle = {width: '100%', lineHeight:'64px', textAlign: 'center'};
        return (
            <SwipeableViews index={this.state.tabSelected}
                            onChangeIndex={this.handleChange}
                            containerStyle={{ height: 'calc(100vh - 351px)', WebkitOverflowScrolling: 'touch' }}
                            style={{ overflowY: 'hidden' }}>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.allItems.length?this.allItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.proposedItems.length?this.proposedItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.applyingItems.length?this.applyingItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.submitedItems.length?this.submitedItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.invalidatedSignedItems.length?this.invalidatedSignedItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
                <div className={ styles.SwipeableContent } style={ItemsListStyle}>
                    {this.invalidatedItems.length?this.invalidatedItems:<div style={emptyMsgStyle}>{window.getLocalizedText(langMap, 'application_list.empty_msg')}</div>}
                </div>
            </SwipeableViews>
        );
    }

    render() {
        this.genApplicationsList();
        return (
            <div style={{zIndex:8, position:'relative'}}>
                <div key="applicationsList"
                    style={{
                        display: 'flex',
                        flexDirection: 'column'}}>
                    {this.applicationsListTabBarView()}
                    {this.applicationsListView()}
                </div>
            </div>
        );
    }
}

export default ApplicationsList;
