import React from 'react';
import styles from '../../../../Common.scss';
import EABComponent from '../../../../Component';
import IconPopup from '../../../../CustomViews/IconPopup';
import * as actions from '../../../../../actions/application';
import {goSupportDocuments} from '../../../../../actions/supportDocuments';
import {getProposalByQuotId} from '../../../../../actions/quotation';
import ClientProfile from '../../../../Dialogs/ClientProfile';
import WarningDialog from '../../../../Dialogs/WarningDialog';
import ConfirmDialog from '../../../../Dialogs/ConfirmDialog';
import * as _ from 'lodash';
import {FlatButton,Paper,Checkbox, Dialog,Popover} from 'material-ui';
import {getIcon} from '../../../../Icons';
import { getAppListView } from '../../../../../actions/application';
import { requoteInvalid } from '../../../../../actions/quotation';

import * as _c from '../../../../../utils/client';
import Constants from '../../../../../constants/SystemConstants';
import eAppConstants from '../../../../../../common/EAppConstants';
import SystemConstants from '../../../../../constants/SystemConstants';

class ApplicationItems extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state,{
            inited: false,
            error: null,
            warningMsg: null,
            confirmMsg: null,
            onConfirm: null,
            idClickCount: 0,
            showApplicationId: false,
            anchorEl: null
        });
    }

    componentDidMount() {
        const {store} = this.context;
        if (!this.inited) {
        this.unsubscribe = store.subscribe(() => {
            if (this.unsubscribe) {
                const products = store.getState().products;
                let newState = {};
                if (!isEqual(this.state.error, products.error)) {
                    newState.error = products.error;
                }
                if (!_.isEmpty(newState)) {
                    this.setState(newState);
                }
            }
        });
        this.inited = true;
        }
    }

    componentWillUnmount(){
        this.unsubscribe();
        this.unsubscribe = null;
    };

    componentWillReceiveProps(){

    }

    validateClientProfile = (method) =>{
        let {values} = this.props;
        let { appList, client, pos } = this.context.store.getState();
        let { invalid } = appList;
        let { profile } = client;
        let {iCid, pCid} = values;
        let pProfile = this.getProfileFromStore();
        if (!pos.showFnaInvalidFlag) {
            if (invalid.initApplications) {
                getAppListView(this.context, profile);
            }else if (iCid === pCid) {
                if (this.clientProfile.validateValues(pProfile)){
                    if (method === 'goAppForm'){
                        this.goAppForm();
                    } else if (method === 'goApplication'){
                        this.goApplication();
                    }
                }
                else {
                    this.clientProfile.openDialog(pProfile, false);
                }
            } else {
                let iProfile = this.getInsuredProfileFromStore(iCid);
                if (this.clientProfile.validateValues(pProfile)){
                    if (this.insuredClientProfile.validateValues(iProfile)){
                        if (method === 'goAppForm'){
                            this.goAppForm();
                        } else if (method === 'goApplication'){
                            this.goApplication();
                        }
                    } else {
                        this.insuredClientProfile.openDialog(iProfile, false);
                    }
                } else {
                    this.clientProfile.openDialog(pProfile, false);
                }
            }
        } else {
            // isFNACompleted is false
            this.setState({
              warningMsg: 'Cannot apply/continue application, please check previous processes.'
            });
        }
    }

    clientChoiceTicked = (applicationId, isInputChecked) => {
        this.props.clientChoiceCallback(applicationId, isInputChecked);
    }

    goAppForm = () => {
        actions.apply(this.context, this.props.values.id);
    }

    goUploadDocument = () => {
        goSupportDocuments(this.context, this.props.values.id, 'SUBMITTED');
    }

    goViewProposal = () => {
        let application = this.props.values;
        let type = application.type;
        let id = type === 'quotation'?application.id: type === 'application' ? application.quotationDocId : '';
        getProposalByQuotId(this.context, id);
    }

    requoteInvalid = () => {
      let {values} = this.props;
      let type = values.type;
      let id = type === 'quotation' ? values.id : type === 'application' ? values.quotationDocId : '';
      requoteInvalid(this.context, id, false,
        (confirmMsg) => {
          this.setState({
            confirmMsg,
            onConfirm: () => {
              this.setState({
                confirmMsg: null,
                onConfirm: null
              });
              requoteInvalid(this.context, id, true);
            }
          });
        },
        (errorMsg) => {
          this.setState({ warningMsg: errorMsg });
        }
      );
    }

    goApplication = () => {
        actions.goApplication(this.context, this.props.values.id);
    }

    confirmAppPaid = () => {
        var self = this;

        actions.confirmAppPaid(this.context, this.state.confirmPaidAppID, (result) => {
            self.setState({
              confirmMsg: null,
              onConfirm: null,
              confirmPaidAppID: ''
            });
            if (result) {
                const {client, appList} = this.context.store.getState();
                setTimeout(function() {
                    if (client && client.profile) {
                        actions.getAppListView(self.context, client.profile, appList.bundleId);
                    }
                }, 100);
            }
        });
    }

    getInsuredProfileFromStore(iCid){
        const {client} = this.context.store.getState();
        return client.dependantProfiles[iCid];
    }

    getProfileFromStore(){
        const {client} = this.context.store.getState();
        return client.profile ;
    }

    getPersonName = (pName, iName, pCid, iCid) => {
        let nameLabel = '';
        if (iName) {
            if (pName) {
                if (iName === pName && pCid === iCid){
                    nameLabel = iName;
                } else {
                    nameLabel = pName + ' , ' + iName;
                }
            } else {
                nameLabel = iName;
            }
        }
        return nameLabel;
    }

    getHeader4Title = (showConst, paymentMode,langMap) => {
        if (showConst === SystemConstants.PREAPP.SUMMARY.SHOW_MODAL_PREM) {
            switch (paymentMode) {
                case 'A':
                    return window.getLocalizedText(langMap, 'application_list.annualPremium');
                case 'S':
                    return window.getLocalizedText(langMap, 'application_list.semiAnnualPremium');
                case 'Q':
                    return window.getLocalizedText(langMap, 'application_list.quarterlyPremium');
                case 'M':
                    return window.getLocalizedText(langMap, 'application_list.monthlyPremium');
            }
        } else if (showConst === SystemConstants.PREAPP.SUMMARY.SHOW_RETIRE_INCOME) {
          return window.getLocalizedText(langMap, 'application_list.retirementIncome');
        } else {
            return window.getLocalizedText(langMap, 'application_list.sum_assured');
        }
    }

    getPlansDetail(plans, paymentMode, ccy, showConst4, showConst5){
        var {lang} = this.context;
        var plansDetail = [];
        let policyTerm, premTermDesc;
        let mapping = {
          yrs69: '69 Years'
        };
        for (var i in plans){
            let plan = plans[i];
            policyTerm = plan.polTermDesc || plan.policyTerm;
            premTermDesc = plan.premTermDesc || mapping[plan.premTerm];
            if (plan){
                let covCode = plan.covCode || plan.docId;
                let prem = paymentMode === 'A' ? plan.yearPrem + (_.get(plan.tax, 'yearTax') || 0):
                    paymentMode === 'S' ? plan.halfYearPrem + (_.get(plan.tax, 'halfYearTax') || 0):
                        paymentMode === 'Q' ? plan.quarterPrem + (_.get(plan.tax, 'quarterTax') || 0):
                            paymentMode === 'M' ? plan.monthPrem + (_.get(plan.tax, 'monthTax') || 0):
                                plan.premium;
                let tdKeySAorQP, tdKeyccy;
                let tdValueSAorQP, tdValuePremorAnnualPrem;

                if (showConst4 === SystemConstants.PREAPP.SUMMARY.SHOW_MODAL_PREM) {
                    tdKeySAorQP = 'appItemQP';
                    switch (paymentMode) {
                        case 'A':
                            tdValueSAorQP = window.getCurrencyByCcy(plan.yearPrem, ccy, 2);
                            break;
                        case 'S':
                            tdValueSAorQP = window.getCurrencyByCcy(plan.halfYearPrem, ccy, 2);
                            break;
                        case 'Q':
                            tdValueSAorQP = window.getCurrencyByCcy(plan.quarterPrem, ccy, 2);
                            break;
                        case 'M':
                            tdValueSAorQP = window.getCurrencyByCcy(plan.monthPrem, ccy, 2);
                            break;
                    }
                } else if (showConst4 === SystemConstants.PREAPP.SUMMARY.SHOW_RETIRE_INCOME) {
                  tdKeySAorQP = 'appItemRI';
                  tdValueSAorQP = plan.retirementIncome ? window.getCurrencyByCcy(plan.retirementIncome, ccy, 0) : ' - ';
                } else {
                    tdKeySAorQP = 'appItemSA';
                    tdValueSAorQP = plan.saViewInd === 'Y' ? window.getCurrencyByCcy(plan.sumInsured, ccy, 0) : ' - ';
                }

                if (showConst5 === SystemConstants.PREAPP.SUMMARY.SHOW_ANNUAL_PREM) {
                    tdValuePremorAnnualPrem = plan.yearPrem ? window.getCurrencyByCcy(plan.yearPrem, ccy, 2) : ' - ';
                } else {
                    tdValuePremorAnnualPrem = prem ? window.getCurrencyByCcy(prem, ccy, 2) : ' - ';
                }

                plansDetail.push(
                    <tr key={"appItem" + covCode + i}>
                        <td key={"appItemName" + covCode + i} style={{fontWeight:'500', paddingLeft: 12}}>{window.getLocalText(lang, plan.covName)}</td>
                        <td key={"appItemPolicyTerm" + covCode + i} style={{textAlign: 'right'}}>{policyTerm}</td>
                        <td key={"appItemPremiumTerm" + covCode + i} style={{textAlign: 'right'}}>{premTermDesc}</td>
                        <td key={tdKeySAorQP + covCode + i} style={{textAlign: 'right'}}>{tdValueSAorQP}</td>
                        <td key={"appItemCcy" + covCode + i} style={{textAlign: 'right', paddingRight: 12}}>{tdValuePremorAnnualPrem}</td>
                    </tr>
                );
            }
        }
        return plansDetail;
    };

    getHeaderButtons = () => {
        let self = this;
        let {muiTheme} = this.context;
        let {values, clientChoiceDisabled, isSelectedProposal, clientChoiceFinished, onItemSelecting, agentChannel} = this.props;
        let {type, appStatus, id, isInitialPaymentCompleted, isMandDocsAllUploaded} = values;
        let {langMap} = this.context;

        const requoteBtn = (
          <FlatButton
            label={window.getLocalizedText(langMap, 'proposal.btn.requote')}
            disabled={onItemSelecting}
            className={styles.FlatButton}
            onClick={() => this.requoteInvalid()}
          />
        );

        // The rightest button
        var firstButton;
        if (type === 'application') {
            if (appStatus === 'SUBMITTED') {
                firstButton = <FlatButton
                                label={'SUPPORTING DOCS'}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goUploadDocument()}
                                icon={isMandDocsAllUploaded ? null : getIcon('warning', muiTheme.palette.warningColor)}
                            />
            }
            else if (appStatus === 'APPLYING') {
                firstButton = <FlatButton label={window.getLocalizedText(langMap, 'application.continue')}
                                disabled = {onItemSelecting || ((!clientChoiceFinished) && agentChannel == 'AGENCY')}
                                className={styles.FlatButton}
                                onClick={()=>this.validateClientProfile('goApplication')}/>
            }
            else if (appStatus === 'INVALIDATED') {
                firstButton = <FlatButton
                    label={'SUPPORTING DOCS'}
                    disabled={onItemSelecting}
                    className={styles.FlatButton}
                    onClick={()=>this.goUploadDocument()}
                />
                // firstButton = <FlatButton
                //     label={getLocalizedText(langMap, 'application.view_proposal')}
                //     disabled={onItemSelecting}
                //     className={styles.FlatButton}
                //     onClick={()=>this.goViewProposal()}
                // />
            }
            else if (appStatus === 'INVALIDATED_SIGNED') {
                firstButton =   <div style={{padding: '14px 24px', display:'inline-block'}}>
                                    <Checkbox
                                        label={window.getLocalizedText(langMap, 'Paid')}
                                        style={{
                                            fontSize: 14,
                                            fontWeight: 500
                                        }}
                                        labelStyle={{
                                            marginLeft: '-10px',
                                            color: muiTheme.palette.primary2Color
                                        }}
                                        disabled = {isInitialPaymentCompleted}
                                        checked = {isInitialPaymentCompleted}
                                        onCheck={
                                            function(event, isInputChecked) {
                                                if (isInputChecked) {
                                                    self.setState({
                                                      confirmMsg: window.getLocalizedText(langMap, 'application_list.paid.confirm'),
                                                      confirmPaidAppID: this.appId,
                                                      onConfirm: () => self.confirmAppPaid()
                                                    });
                                                }
                                            }.bind({appId: values.id})
                                        }
                                    />
                                </div>
            }
        }
        // In the PROPOSED Tab
        else if (type === 'quotation') {
            if (appStatus === 'INVALIDATED' || appStatus === 'INVALIDATED_SIGNED') {
                // firstButton = <FlatButton
                //     label={'SUPPORTING DOCS'}
                //     disabled={onItemSelecting}
                //     className={styles.FlatButton}
                //     onClick={()=>this.goUploadDocument()}
                // />
                firstButton = <FlatButton
                                label={getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}
                                />
            } else {
                firstButton = <FlatButton
                    label={getLocalizedText(langMap, 'application.apply')}
                    disabled={onItemSelecting || (agentChannel == 'AGENCY' && (!isSelectedProposal || !clientChoiceFinished ))}
                    className={styles.FlatButton}
                    onClick={()=>this.validateClientProfile('goAppForm')}
                />
            }
        }

        var secondButton;
        if (type === 'application') {
            if (appStatus === 'APPLYING') {
                secondButton = <FlatButton label={getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}/>
            }
            else if (appStatus === 'INVALIDATED_SIGNED') {
                secondButton = <FlatButton
                    label={'SUPPORTING DOCS'}
                    disabled={onItemSelecting}
                    className={styles.FlatButton}
                    onClick={()=>this.goUploadDocument()}
                />
                // secondButton = <FlatButton
                //                     label={getLocalizedText(langMap, 'application.view_proposal')}
                //                     disabled={onItemSelecting}
                //                     className={styles.FlatButton}
                //                     onClick={()=>this.goViewProposal()}
                //                 />
            }
            else if (appStatus === 'INVALIDATED') {
              secondButton = requoteBtn;
            }
        }
        // In the PROPOSED Tab
        else if (type === 'quotation' && appStatus !== 'INVALIDATED_SIGNED' && appStatus !== 'INVALIDATED') {
                secondButton = <FlatButton label={getLocalizedText(langMap, 'application.view_proposal')}
                                disabled={onItemSelecting}
                                className={styles.FlatButton}
                                onClick={()=>this.goViewProposal()}/>
        }
        var thirdButton = null;
        // In the PROPOSED Tab
        if (type === 'quotation') {
          if (appStatus !== 'INVALIDATED_SIGNED' && appStatus !== 'INVALIDATED') {
            // checked = {isSelectedProposal}
            thirdButton = this.props.isAtTabAll || agentChannel == 'FA'?null:
                <div style={{padding: '14px 12px', display:'inline-block'}}>
                    <Checkbox label={getLocalizedText(langMap, 'application.choice')}
                        style={{
                            fontSize: 14,
                            fontWeight: 500
                        }}
                        labelStyle={{
                            marginLeft: '-10px',
                            color: muiTheme.palette.primary2Color
                        }}
                        disabled = {onItemSelecting || clientChoiceDisabled}
                        defaultChecked = {isSelectedProposal}
                        onCheck={(event, isInputChecked) => {
                            this.clientChoiceTicked(id, isInputChecked);
                        }}
                    />
                </div>
          } else {
            thirdButton = requoteBtn;
         }
      }
        return {
            'firstButton':firstButton,
            'secondButton':secondButton,
            'thirdButton':thirdButton
        };
    }

    handleShowIdOnClick = (e) => {
      let {idClickCount, showApplicationId} = this.state;
      let {type, policyNumber} = this.props.values;
      const SHOW_ID_CLICK_COUNT = 10;

      if (type === 'application') {
        idClickCount += 1;
        this.setState({
          idClickCount: idClickCount,
          showApplicationId: idClickCount >= SHOW_ID_CLICK_COUNT && !_.isEmpty(policyNumber),
          anchorEl: e.currentTarget
        });
      }
    }

    handleHideIdOnClick = () => {
      this.setState({
        idClickCount: 0,
        showApplicationId:false
      });
    }

    headerView = () => {
        let {lang, langMap, muiTheme} = this.context;
        let {values,onItemSelecting,itemDeleteCallback,disabledDelete} = this.props;

        //Modify by Alex Tse for CR49 to add paymentMethod from values start****
        //let {id, policyNumber, plans, type, appStatus, invalidateReason, pCid, iCid, iName, pName} = values;
        let { id, policyNumber, plans, paymentMethod, type, appStatus, invalidateReason, pCid, iCid, iName, pName } = values;
        //Modify by Alex Tse for CR49 to add paymentMethod from values End****


        // let sNo = (appStatus && appStatus== getLocalizedText(langMap, 'application_list.submitted') && typeof(sequenceNo) !== "undefined" && sequenceNo !== null ) ? (getLocalizedText(langMap, 'application_list.sequence_no', 'Policy No.') + ": " + values.sequenceNo) : "";

        let covName = _.get(plans[0], 'covName') || '';
        let buttons = this.getHeaderButtons();
        let {firstButton, secondButton, thirdButton} = buttons;
        let isApplyingApp = appStatus === 'APPLYING';

        let nameLabel;
        if (type === 'application' || type === 'quotation'){
            nameLabel = this.getPersonName(pName, iName, pCid, iCid);
        }
// NOTE: lucia
        return (
            <div style={{widht: '100%',display: 'flex',flexDirection: 'row',
                lineHeight: '24px',padding: '8px ' + muiTheme.spacing.desktopGutterLess + 'px 0 ' + muiTheme.spacing.desktopGutter + 'px',
                position: 'relative'}}
            >
                {/*Delete SelectingModeIcon*/}
                {onItemSelecting ?
                    <div style={{padding: '12px 0', display:'inline-block'}}>
                        <Checkbox
                          disabled={disabledDelete}
                          onCheck={(event, value) => {itemDeleteCallback(id, isApplyingApp, value);}}/>
                    </div>
                : null}
                {/*Basic Product Name*/}
                {/*(Application Number)*/}
                {/*(Proposer Name/Life Insured name)*/}
                <div style={{width: 400, display:'flex', flexDirection:'column', flex: 1}}>
                    <div style={{display:'flex', alignItems: 'center', fontWeight: '500', color: muiTheme.palette.primary2Color}} onClick={this.handleShowIdOnClick}>
                        {window.getLocalText(lang, covName)}{' (' + paymentMethod + ')' }{' (' + (policyNumber || id) + ')'}{_.get(values, 'approvalStatus') ? ('  --  ' + _.get(values, 'approvalStatus')) : ''}
                        {invalidateReason ? (
                          <IconPopup
                            style={{ padding: '0 12px' }}
                            content={(
                              <div style={{ display: 'flex', alignItems: 'center' }}>
                                {getIcon('info', muiTheme.palette.primary2Color)}
                                <span style={{ marginLeft: '16px' }}>{window.getLocalizedText(langMap, 'application.invalidate.reasons.' + invalidateReason)}</span>
                              </div>
                            )}
                          />
                        ) : null}
                        {<Popover
                          open={this.state.showApplicationId}
                          anchorEl={this.state.anchorEl}
                          anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                          targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
                          onRequestClose={this.handleHideIdOnClick}
                          style={{overflow:'hidden', fontWeight: '500', color: muiTheme.palette.primary2Color}}
                          >
                            <div style={{overflow:'hidden'}}>
                              <span style={{opacity: 1}}>{window.getLocalText(lang, covName)} </span>
                              <span>({id})</span>
                            </div>
                          </Popover>
                        }
                        {values[eAppConstants.PAYMENTTRANSFERRED] ? '  --  TRANSFERRED' : null}
                    </div>
                    <div style={{display:'inline-block', color:'gray', fontSize:'14px'}}>{nameLabel}</div>
                </div>
                {/*firstButton is the rightest button*/}
                <div style={{position:'absolute',right:12, top: 8, zIndex:10}}>
                    {thirdButton}
                    {secondButton}
                    {firstButton}
                </div>
            </div>
        );
    }

    contentView = () => {
        let {langMap, muiTheme} = this.context;
        let {values} = this.props;
        let {plans, paymentMode, ccy} = values;

        const replaceSAtoModalPremPlans = ['AWT','PUL'];
        const replacePremiumtoAnnualPremiumPlans = ['AWT','PUL'];
        const replaceSAtoRetireIncomePlans = ['RHP'];
        let replaceSAtoModalPrem = replaceSAtoModalPremPlans.indexOf(_.get(plans, '[0].covCode')) > -1;
        let replacePremiumtoAnnualPrem = replacePremiumtoAnnualPremiumPlans.indexOf(_.get(plans, '[0].covCode')) > -1;
        let replaceSAtoRetireIncome = replaceSAtoRetireIncomePlans.indexOf(_.get(plans, '[0].covCode')) > -1;
        let showColumn4Const = SystemConstants.PREAPP.SUMMARY.SHOW_SA;
        let showColumn5Const = SystemConstants.PREAPP.SUMMARY.SHOW_PREM;
        if (replaceSAtoModalPrem) {
          showColumn4Const = SystemConstants.PREAPP.SUMMARY.SHOW_MODAL_PREM;
        } else if (replaceSAtoRetireIncome) {
          showColumn4Const = SystemConstants.PREAPP.SUMMARY.SHOW_RETIRE_INCOME;
        }
        if (replacePremiumtoAnnualPrem) {
          showColumn5Const = SystemConstants.PREAPP.SUMMARY.SHOW_ANNUAL_PREM;
        }

        let header4Title = this.getHeader4Title(showColumn4Const, paymentMode, langMap);
        // let ccySymbol = ccy === 'SGD' ? 'S$ ' : ccy;
        let plansDetail = plans && plans.length ? this.getPlansDetail(plans, paymentMode, ccy, showColumn4Const, showColumn5Const) : [];

        let header5Title = (showColumn5Const === SystemConstants.PREAPP.SUMMARY.SHOW_ANNUAL_PREM) ? window.getLocalizedText(langMap, 'application_list.annualPremium') :
        window.getLocalizedText(langMap, 'application_list.premium').replace('[@@payMode]', window.getLocalizedText(langMap, 'application_list.payMode_' + paymentMode));

        return (
            <table key={"table"} className={styles.Table} style={{width: 'calc(100% - 48px)', margin: '24px 24px 0 24px', borderBottom: '1px solid #c4c4c4'}}>
                <thead>
                <tr key={"htr"} className={styles.TableHeader}>
                    <th key={"h1"} style={{paddingLeft: 12}}>Plan Names</th>
                    <th key={"h2"} style={{width: '150px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.policy_term')}</th>
                    <th key={"h3"} style={{width: '150px', textAlign: 'right'}}>{window.getLocalizedText(langMap, 'application_list.premium_mode')}</th>
                    <th key={"h4"} style={{width: '150px', textAlign: 'right'}}>{header4Title}</th>
                    <th key={"h5"} style={{width: '150px', textAlign: 'right', paddingRight: 12}}>{header5Title}</th>
                </tr>
                </thead>
                <tbody>
                    {plansDetail}
                </tbody>
            </table>
        );
    }

    footerView = () => {
        let {langMap} = this.context;
        let {values} = this.props;
        var {lastUpdateDate, paymentMode, totPremium, ccy} = values;
        let payMode = window.getLocalizedText(langMap, 'application_list.payMode_' + paymentMode);
        const policyOptions = _.get(values, 'policyOptions') ||  _.get(values, 'quotation.policyOptions');
        let rspAmount = _.get(policyOptions, 'rspAmount') || _.get(policyOptions, 'rspAmt', 0);
        let rspPayFreq = _.get(policyOptions, 'rspPayFreq');
        if (rspPayFreq === 'A') {
            rspPayFreq = 'annual';
        } else if (rspPayFreq === 'S') {
            rspPayFreq = 'semiAnnual';
        } else if (rspPayFreq === 'Q') {
            rspPayFreq = 'quarterly';
        } else if (rspPayFreq === 'M') {
            rspPayFreq = 'monthly';
        }
        let topUpAmt = _.get(policyOptions, 'topUpAmt');

        lastUpdateDate = lastUpdateDate ? (new Date(lastUpdateDate)).format('dd mmm yyyy HH:MM:ss') : '';

        return (
            <div
            style={{
                width: 'calc(100% - 72px)',
                margin: '0 24px',
                height: '48px',
                lineHeight: '48px',
                fontSize: '14px',
                padding: '0 12px 0 12px'
            }}
            >
            <div className={styles.SubContent} style={{display: 'inline-block', float: 'left'}}>{lastUpdateDate}</div>
                <div style={{display: 'inline-block', width: '100px', textAlign: 'right', float: 'right'}}>{window.getCurrencyByCcy(totPremium, ccy, 2)}</div>
                <div style={{display: 'inline-block', width: '180px', textAlign: 'right', float: 'right'}}>{paymentMode === 'undefined' ? '' : window.getLocalizedText(langMap, 'application_list.total_premium').replace('[@@payMode]', payMode)}</div>
                {rspAmount > 0 ? <div style={{display: 'inline-block', width: '100px', textAlign: 'right', float: 'right'}}>{window.getCurrencyByCcy(rspAmount, ccy, 2)}</div> : null}
                {rspAmount > 0 ? <div style={{display: 'inline-block', width: '180px', textAlign: 'right', float: 'right'}}>{window.getLocalizedText(langMap, 'application_list.rspAmount').replace('[@@rspPayFreq]', window.getLocalizedText(langMap, 'application_list.rspPayFreq_' + rspPayFreq))}</div> : null}
                {topUpAmt > 0 ? <div style={{display: 'inline-block', width: '100px', textAlign: 'right', float: 'right'}}>{window.getCurrencyByCcy(topUpAmt, ccy, 2)}</div> : null}
                {topUpAmt > 0 ? <div style={{display: 'inline-block', width: '180px', textAlign: 'right', float: 'right'}}>{window.getLocalizedText(langMap, 'application_list.topUp_single_premium')}</div> : null}
            </div>
        );
    }

    render() {
        let self = this;
        let {isSelectedProposal} = this.props;
        let clientChoiceTickedStyle = {
            width: '100%',
            margin: '0 0 6px',
            backgroundColor:'rgba(0, 0, 143, 0.07)',
            border: '1px solid rgba(0, 0, 143px, 0.17)'
        };
        let {
            langMap,
            store
        } = this.context;

        let {
            agentProfile = {}
        } = store.getState().app;
        let features = agentProfile.features || "";
        let hasFNA = features.indexOf("FNA") > -1? true: false;

        return (
            <div>
                <div>
                    <Paper style={isSelectedProposal ? clientChoiceTickedStyle : {width: '100%', margin: '0 0 6px'}}>
                        <ClientProfile
                            ref={ref => { this.clientProfile = ref; }}
                            values={this.getProfileFromStore()}
                            isApp
                            mandatoryId={_c.genProfileMandatory(Constants.FEATURES.EAPP, hasFNA, true, false)}
                            onSubmit={()=>{this.validateClientProfile('goAppForm')}}
                        />
                        <ClientProfile
                            ref={ref => { this.insuredClientProfile = ref; }}
                            values={this.getInsuredProfileFromStore()}
                            isInsured
                            isFamily
                            isApp
                            mandatoryId={_c.genProfileMandatory(Constants.FEATURES.EAPP, hasFNA, false, false)}
                            onSubmit={()=>{this.validateClientProfile('goAppForm')}}
                        />
                        {this.headerView()}
                        {this.contentView()}
                        {this.footerView()}
                    </Paper>
                </div>
                <WarningDialog
                  open={!!this.state.warningMsg}
                  msg={this.state.warningMsg}
                  onClose={() => this.setState({ warningMsg: null })}
                />
                <ConfirmDialog
                    open={!!this.state.confirmMsg}
                    confirmMsg={this.state.confirmMsg}
                    onClose={() => {
                        self.setState({
                          confirmMsg: null,
                          confirmPaidAppID: '',
                          onConfirm: null
                        });
                    }}
                    onConfirm={() => this.state.onConfirm && this.state.onConfirm()}
                />
            </div>
        );
    }
}

export default ApplicationItems;
