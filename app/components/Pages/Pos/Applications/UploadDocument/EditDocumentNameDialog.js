import React, {PropTypes} from 'react';
import EABComponent from '../../../../Component';
import {Dialog, TextField, FlatButton} from 'material-ui';
import Appbar from '../../../../CustomViews/AppBar';
import styles from '../../../../Common.scss';
import {updateOtherDocumentName} from '../../../../../actions/supportDocuments';
import * as _ from 'lodash';

class EditDocumentNameDialog extends EABComponent {

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
            open: false,
            appbar: this.getAppBar(true),
            docName:'',
            containsRegExp: false,
            docNameDuplicated: false
        });
    }

    getAppBar = (doneDisabled) => {
        return ({
            menu:{
                type:'flatButton',
                title:{
                    'en':'Cancel'
                },
                action: ()=>{
                    this.closeDialog();
                }
            },
            title:'Edit Document Name',
            items:[
                [{
                    type:'flatButton',
                    title:{
                        'en': 'Done'
                    },
                    disabled: doneDisabled,
                    action: ()=>{
                        this.submit();
                    }
                }]
            ]
        });
    };
    updateAppBar = (doneDisabled) => {
        this.setState({
            appbar: this.getAppBar(doneDisabled)
        });
    }

    handleTextChange = (event)=>{
        if (event.target.value.length === 0 || event.target.value === _.get(this.props, 'documentInfo.currentName')) {
            this.updateAppBar(true);
            this.setState({
                containsRegExp:false,
                docName: event.target.value
            });
        } else {
            // Do alphanumberic checking
            let patt = /[^a-z0-9 ]/i;
            let containsRegExp = patt.test(event.target.value);
            if (containsRegExp) {
                this.updateAppBar(true);
                this.setState({
                    containsRegExp:true,
                    docName: event.target.value
                });
            } else {
                this.updateAppBar(false);
                this.setState({
                    containsRegExp:false,
                    docName: event.target.value
                });
            }
        }
    };

    openDialog=(infoDic)=>{
        this.setState({
            open: true
        });
    }

    closeDialog=()=>{
        this.setState({
            open: false,
            docName:'',
            containsRegExp: false
        });
        this.updateAppBar(true);
    }

    submit = () =>{
        const {appId, tabId, documentId, rootValues} = this.props.documentInfo;
        const {docName} = this.state;

        updateOtherDocumentName(this.context, appId, tabId, documentId, docName, rootValues, (resp)=>{
            if (resp.duplicated) {
                this.setState({docNameDuplicated: true});
            }
            this.closeDialog();
        });

    }

    render() {
        let {appbar ,open, containsRegExp} = this.state;
        const {currentName} = this.props.documentInfo;

        return (
            <div>
                <Dialog
                    title="WARNING"
                    open={this.state.docNameDuplicated}
                    actions={[
                        <FlatButton
                            label='OK'
                            primary={true}
                            onTouchTap={()=>this.setState({docNameDuplicated: false})}
                        />
                    ]}>
                    There is already a document with the same name in this application. Please use another name.
                </Dialog>

                <Dialog titleClassName={styles.OtherDocDialogtitle} open={open} bodyStyle={{padding: '24px'}}
                    title={<div style={{padding:'0px'}}><Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA', paddingLeft: '0px !important'}}/><div className={styles.Divider}/></div>}
                >
                    <TextField
                        defaultValue = {currentName}
                        onChange = {this.handleTextChange}
                        maxLength = '20'
                    />
                    {containsRegExp ? <div style={{fontSize:'10px',color:'red'}}>Invalid letter</div> : null}
                </Dialog>
            </div>
        );
    }
}

export default EditDocumentNameDialog;
