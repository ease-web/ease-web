import React from 'react';
import * as _ from 'lodash';
import {IconButton, Dialog, FlatButton} from 'material-ui';
import {getIcon} from '../../../../../Icons/index';
import EABComponent from '../../../../../Component';
import styles from '../../../../../Common.scss';
// import ImageView from '../../../../CustomViews/ImageView.js';
import SuppDocsImageView from './SuppDocsImageView.js';
// import ReadOnlyPDFViewer from '../../../../CustomViews/ReadOnlyPDFViewer';
import EmbedPDFViewer from '../../../../../CustomViews/EmbedPDFViewer';
import SignDocViewer from '../../../../../CustomViews/SignDocViewer';
import {deleteFileAction, viewedFile} from '../../../../../../actions/supportDocuments.js';

class FileRow extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
            openDeleteWarning: false,
            openSysDocsPreview: false,
            sysDocsPreviewData: {},
            token: props.token ? props.token : '',
            signDocConfig:{},
            isSigned: false
        });
    }

    deleteFile = (applicationId, attachmentId, tabId) => {
        let {sectionId, subSectionId} = this.props;
        var valueLocation = {
            'sectionId' : sectionId,
            'subSectionId' : subSectionId
        };
        deleteFileAction(this.context, applicationId, attachmentId, valueLocation, tabId);
        this.setState({openDeleteWarning:false});
    }

    fileNameOnClick = (applicationId, attachmentId, sectionId, reviewDisabled, isSupervisorChannel, isReadOnly) => {
        let {viewedList} = this.props;
        // if (viewedList.hasOwnProperty(attachmentId) && !isReadOnly && !reviewDisabled) {
        if (!isReadOnly && !reviewDisabled) {
            viewedFile(this.context, applicationId, attachmentId, isSupervisorChannel, true, (resp) => {
                if (resp.success === true) {
                    viewedList[attachmentId] = true;
                    this.setState({
                        viewedList: viewedList,
                        openSysDocsPreview: true,
                        // sysDocsPreviewData: resp.data
                        // sysDocsPreviewData: genFilePath(this.context.store.getState().app, resp.docId, attachmentId)
                        // sysDocsPreviewData: resp.attUrl,
                        token: resp.token,
                        signDocConfig: resp.signDocConfig,
                        isSigned: resp.isSigned
                    });
                } else {
                    this.setState({openSysDocsPreview: true});
                }
            });
        } else {
            viewedFile(this.context, applicationId, attachmentId, isSupervisorChannel, false, (resp) => {
                if (resp.success === true) {
                    this.setState({
                        openSysDocsPreview: true,
                        // sysDocsPreviewData: resp.data
                        // sysDocsPreviewData: genFilePath(this.context.store.getState().app, resp.docId, attachmentId)
                        // sysDocsPreviewData: resp.attUrl,
                        token: resp.token,
                        signDocConfig: resp.signDocConfig,
                        isSigned: resp.isSigned
                    });
                } else {
                    this.setState({openSysDocsPreview: true});
                }
            });
        }
    }

    getViewLabel = (viewedList, fileId, isSupervisorChannel, isReadOnly) => {
        if (viewedList.hasOwnProperty(fileId) && isSupervisorChannel && !isReadOnly) {
            if (viewedList[fileId]) {
                return <div style={{flex: 1}}>Reviewed</div>;
            } else {
                return <div style={{flex: 1, color: 'red'}}>Pending to review</div>;
            }
        } else {
            return <div style={{flex: 1}}/>;
        }
    }

    genFile = (applicationId, attachmentId, disabled, value,
                sectionId, appStatus, viewedList, reviewDisabled, isSupervisorChannel,
                isReadOnly, pendingSubmitList) => {
        var {muiTheme, langMap} = this.context;
        let cursorStyle = reviewDisabled ? 'not-allowed' : 'pointer';
        let isFileInPendingSubmitList = _.includes(pendingSubmitList, attachmentId);
        let submitLabel = appStatus === 'SUBMITTED' ?
            window.getLocalizedText(langMap, 'application.submitted_date_on') + ' '
            : window.getLocalizedText(langMap, 'application.uploaded_date_on') + ' ';
        //TO-DO: remove input param and put the variable under following
        let {token} = this.props;

        return (
            <div className={styles.Field}>
                <SuppDocsImageView  id={attachmentId} key={attachmentId} token={token} docId={applicationId} attId={attachmentId} fileType={value.fileType} style={{width:'36px', height:'36px', fontSize:'12px'}} canScroll={false}/>
                    <div style={{flex: 1, padding:'6px 24px'}}>
                        <div className={styles.Content} style={{display: 'block', height: 20, lineHeight: '20px', cursor:cursorStyle}}
                            onClick={()=>this.fileNameOnClick(applicationId, attachmentId, sectionId, reviewDisabled, isSupervisorChannel, isReadOnly)}>{value.title}</div>
                        <div className={styles.SubContent} style={{display: 'flex', height:14, marginTop: '2px'}}>
                            {sectionId === 'sysDocs' ? null : <div style={{flex: 1}}>{'File size: ' + value.fileSize}</div>}
                            {sectionId === 'sysDocs' ?
                                null :
                                isFileInPendingSubmitList ?
                                    <div style={{flex: 1, color: 'red'}}>
                                        {disabled !== 'true' ? 'Pending to upload' : null}
                                    </div>
                                    :
                                    <div style={{flex: 1}}>
                                        {disabled !== 'true' ? submitLabel + value.feUploadDate : null}
                                    </div>
                            }
                            {this.getViewLabel(viewedList, value.id, isSupervisorChannel, isReadOnly)}
                        </div>
                    </div>
                {disabled !== 'true' && appStatus !== 'SUBMITTED' ?
                    <IconButton
                        onTouchTap={()=>this.setState({openDeleteWarning:true})}
                        disabled={isReadOnly}
                    >
                        {getIcon('delete', muiTheme.palette.labelColor)}
                    </IconButton> : null}
            </div>
        );
    }

    render(){
        let {applicationId, attachmentId, disabled, value, tabId, sectionId, appStatus,
            viewedList, reviewDisabled, isSupervisorChannel, isReadOnly, pendingSubmitList} = this.props;
        let {signDocConfig, token, isSigned} = this.state;
        let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        let android = /android/i.test(navigator.userAgent) && !window.MSStream;
        // <PDFViewer pdf={this.state.sysDocsPreviewData} pageWidth={720} />:
        // autoScrollBodyContent={true}
        return (
            <div>
                <Dialog
                    title="WARNING"
                    open={this.state.openDeleteWarning}
                    actions={[
                        <FlatButton
                            label='Cancel'
                            primary={true}
                            onTouchTap={()=>this.setState({openDeleteWarning:false})}
                        />,
                        <FlatButton
                            label='Confirm'
                            primary={true}
                            onTouchTap={()=>this.deleteFile(applicationId, attachmentId, tabId)}
                        />
                    ]}>
                    Do you confirm to delete?
                </Dialog>
                <Dialog
                    className = {styles.FixWrongDialogPadding}
                    open={this.state.openSysDocsPreview}
                    contentStyle={{width:'100%', maxWidth:'none'}}
                    actions={[
                        <FlatButton
                            label='Done'
                            primary={true}
                            onTouchTap={()=>this.setState({openSysDocsPreview:false})}
                        />
                    ]}
                >
                    <div style={{ height: 'calc(100vh - 204px)', display: 'flex' }}>
                        {value.fileType === 'application/pdf' ?
                            ((iOS || android) && !window.isEmpty(signDocConfig) ?
                                <div style={{ display: 'flex', flexDirection: 'column', flexGrow: 1 }} >
                                    <SignDocViewer
                                        id={signDocConfig.docid}
                                        visible={true}
                                        signed={isSigned}
                                        eSignBorder={false}
                                        postUrl={signDocConfig.postUrl}
                                        pdfUrl={signDocConfig.attUrl + '/' + token}
                                        auth={signDocConfig.auth}
                                        docts={signDocConfig.docts}
                                        dmsId={signDocConfig.dmsId}
                                        signFieldList={[]}
                                        resultUrl={signDocConfig.resultUrl}
                                        onHandleSign={(resultJSON) => {
                                            // this.handleSign(resultJSON, i);
                                        }}
                                    />
                                </div> : <EmbedPDFViewer url={window.genAttPath(token)} isHideToolBar={true} />) :
                            <SuppDocsImageView id={attachmentId} key={attachmentId} token={token} docId={applicationId} attId={attachmentId} fileType={value.fileType} style={{width: '720px'}} canScroll={true}/>}
                    </div>
                </Dialog>
                {this.genFile(applicationId, attachmentId, disabled, value, sectionId,
                    appStatus, viewedList, reviewDisabled, isSupervisorChannel,
                    isReadOnly, pendingSubmitList)}
            </div>
        );
    }
}


export default FileRow;
