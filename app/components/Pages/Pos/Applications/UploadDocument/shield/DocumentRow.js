import React from 'react';
import * as _ from 'lodash';
import {IconButton, Dialog, FlatButton} from 'material-ui';
import {getIcon} from '../../../../../Icons/index';
import EABComponent from '../../../../../Component';
import FileUpload from '../../../../../CustomViews/FileUpload.js';
import ToolTips from '../../../../../CustomViews/ToolTips';
import FileRow from './FileRow.js';
import EditDocumentNameDialog from '../EditDocumentNameDialog';
import {isShowDragDropZone, deleteDocumentAction} from '../../../../../../actions/supportDocuments.js';
import styles from '../../../../../Common.scss';

const MAXFILESCOUNT = 8;

// SubSection is DocumentRow, e.g. proposer's Pass...
class DocumentRow extends EABComponent {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
            openDeleteWarning: false,
            openEditNameWindow: false
        });
    }

    addFile = () =>{
        let {sectionId, template} = this.props;
        var valueLocation = {
            "sectionId":sectionId,
            "subSectionId":template.id
        }
        isShowDragDropZone(this.context, true, valueLocation);
    }

    deleteDocument = (applicationId, documentId, tabId) => {
        deleteDocumentAction(this.context, applicationId, documentId, tabId);
        this.setState({openDeleteWarning:false});
    }

    documentNameOnClick = () => {
        this.EditDocumentNameDialog.openDialog();
    };

    genItems(applicationId, subSection, sectionId, disabled, values, tabId, appStatus,
         viewedList, reviewDisabled, isSupervisorChannel, isReadOnly, pendingSubmitList){
        var changedSubSection = [];
        var {muiTheme} = this.context;
        var fileUploadValues = {'id': applicationId};
        let isShowWarningIcon = false;
        let subSectionId = subSection.id;
        let subSectionValues = {};
        //TO-DO: remove input param and put the variable under following
        let {tokensMap} = this.props;

        if (sectionId === 'otherDoc') {
            subSectionValues = values.otherDoc.values;
        } else {
            subSectionValues = values[sectionId];
        }
        let subSectionValue = subSectionValues[subSectionId];

        // if (subSection.id === 'axasam' || subSection.id === 'chequeCashierOrder' || subSection.id === 'teleTransfer' || subSection.id === 'cash') {
            if (_.isEmpty(values.mandDocs[subSection.id])) {
                if (sectionId === 'mandDocs') {
                    isShowWarningIcon = true;
                }
            }
        // }

        if (subSectionValue) {
            let reachMaximumFiles = subSectionValue.length === MAXFILESCOUNT;
            
            if (subSection.type === 'subSection'){
                var valueLocation = {
                    "sectionId":sectionId,
                    "subSectionId":subSection.id
                }
                changedSubSection.push(
                    <div className={styles.SubTitle}
                        style={{marginBottom: '6px', background: 'rgba(0, 0, 143, 0.07)', fontSize: '16px'}}>
                        <div style={{display:'flex', flexGrow:1, alignItems: 'center'}}
                            onClick={sectionId === 'otherDoc' ? this.documentNameOnClick : null}
                        >
                            {isShowWarningIcon ? getIcon('warning',  muiTheme.palette.warningColor, {paddingRight: '12px'}) : null}
                            {subSection.title}
                            {subSection.toolTips ? <ToolTips hints={subSection.toolTips}/> : null}
                        </div>
                        <div style={{
                            position: 'absolute',
                            top: 0,
                            right: '24px',
                            width: '96px',
                            textAlign: 'right'
                        }}>
                            {sectionId === 'otherDoc' && appStatus !== 'SUBMITTED' ?
                                <IconButton 
                                    onTouchTap={()=>this.setState({openDeleteWarning:true})}
                                    disabled={isReadOnly}>
                                    {getIcon('delete', muiTheme.palette.labelColor)}
                                </IconButton> :
                                null}
                            <IconButton 
                                onTouchTap={this.addFile}
                                disabled={isReadOnly || reachMaximumFiles}
                            >
                                    {getIcon('add', muiTheme.palette.labelColor)}
                            </IconButton>
                        </div>
                    </div>
                );

                if(reachMaximumFiles){
                    changedSubSection.push(
                        <div style={{fontSize:'12px',color:'red',padding:'0px 0px 0px 24px'}}>
                            Maximum upload {MAXFILESCOUNT} files
                        </div>
                    );
                }

                if (subSectionId === 'pNric' || subSectionId === 'pPass'  || subSectionId === 'pPassport' || subSectionId === 'pPassportWStamp' ||
                    subSectionId === 'thirdPartyID' || subSectionId === 'iNric' || subSectionId === 'iPass' || subSectionId === 'iPassport' || 
                    subSectionId === 'iPassportWStamp'){
                    changedSubSection.push(
                        <div style={{fontSize:'10px',color:'red',padding:'0px 0px 0px 24px'}}>
                            By uploading an image of the ID you are certifying that this is a true copy of the original.
                        </div>
                    );
                }

                if (this.props.dragDropZone.isShow){
                    if (this.props.dragDropZone.valueLocation.sectionId === sectionId && this.props.dragDropZone.valueLocation.subSectionId === subSection.id){
                        changedSubSection.push(
                            <div style={{width: 'calc(100% - 48px)', margin:'auto'}}>
                                <FileUpload
                                    valueLocation={valueLocation}
                                    template = {{}}
                                    allFilesValues = {values}
                                    rootValues={this.props.rootValues}
                                    values = {fileUploadValues}
                                    changedValues={fileUploadValues}
                                    style={{height:48}}
                                    tabId={tabId}
                                    viewedList={viewedList}
                                    isSupervisorChannel={isSupervisorChannel}
                                />
                            </div>
                        );
                    }
                }
            }

            if (subSection.type == 'subSection'){
                subSectionValue.forEach((item)=>{
                    changedSubSection.push(
                        <FileRow 
                            key={item.id}
                            applicationId={applicationId}
                            attachmentId={item.id}
                            disabled={disabled}
                            value={item}
                            sectionId={sectionId}
                            subSectionId={subSectionId}
                            tabId={tabId}
                            appStatus={appStatus}
                            viewedList={viewedList}
                            reviewDisabled={reviewDisabled}
                            isSupervisorChannel={isSupervisorChannel}
                            isReadOnly={isReadOnly}
                            pendingSubmitList={pendingSubmitList}
                            token={tokensMap[item.id]}
                        />
                    );
                });
            } else {
                // System Document
                changedSubSection.push(
                    <FileRow 
                        key={subSectionValue.id}
                        applicationId={applicationId}
                        attachmentId={subSectionValue.id}
                        disabled={disabled}
                        value={subSectionValue}
                        sectionId={sectionId}
                        subSectionId={subSectionId}
                        tabId={tabId}
                        appStatus={appStatus}
                        viewedList={viewedList}
                        reviewDisabled={reviewDisabled}
                        isSupervisorChannel={isSupervisorChannel}
                        isReadOnly={isReadOnly}
                        pendingSubmitList={pendingSubmitList}
                    />
                );
            }

        }
        return changedSubSection;
    }

    render(){
        var {applicationId,template,sectionId,disabled,values, tabId, appStatus,
            viewedList, reviewDisabled, isSupervisorChannel, isReadOnly,
            pendingSubmitList} = this.props;
        return(
            <div>
                <Dialog
                    title="WARNING"
                    open={this.state.openDeleteWarning}
                    actions={[
                        <FlatButton
                            label='Cancel'
                            primary={true}
                            onTouchTap={()=>this.setState({openDeleteWarning:false})}
                        />,
                        <FlatButton
                            label='Confirm'
                            primary={true}
                            onTouchTap={()=>this.deleteDocument(applicationId, template.id, tabId)}
                        />
                    ]}>
                    Do you confirm to delete?
                </Dialog>
                <EditDocumentNameDialog
                    ref={ref=>{this.EditDocumentNameDialog = ref}}
                    documentInfo={{
                        currentName: template.title,
                        appId: applicationId,
                        tabId,
                        documentId:template.id,
                        rootValues: this.props.rootValues
                    }}
                />
                {this.genItems(applicationId, template ,sectionId, disabled, values, tabId, appStatus,
                    viewedList, reviewDisabled, isSupervisorChannel, isReadOnly, pendingSubmitList)}
            </div>
        );
    }
}
export default DocumentRow;
