import React, { Component, PropTypes } from 'react';
import mui from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import Signature from '../Signature/index';

import EABComponent from '../../../../Component';

export default class index extends EABComponent {

  open=()=>{
    this.refs.floatingPage.open();
  }

  close=()=>{
    this.refs.floatingPage.close();
  }

  render() {
    return (
      <FloatingPage ref="floatingPage">
        <AppbarPage>
          <Signature/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}