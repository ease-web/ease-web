import React from 'react';
import { FontIcon } from 'material-ui';

import EABComponent from '../../../../Component';

const signIcon = require('../../../../../img/sign.png');

class SignatureTab extends EABComponent {
  constructor(props) {
    super(props);
  }
  render() {

    const { name, selected, checked, totalTabs, text, isSigningProcess, onHandleChange } = this.props;

    // Style for selected Tab
    const onStyle = {
      backgroundColor:'#FFFFFF',
      display:'inline-block',
      padding:'0px 20px',
      textAlign:'left',
      borderTop:'1px solid rgba(0,0,0,.12)',
      borderRight:'1px solid rgba(0,0,0,.12)',
      borderLeft:'1px solid rgba(0,0,0,.12)',
      width:'calc(100% / ' + totalTabs + ' - 42px )',
      height:'100%'
    };

    // Style for non-selected Tab
    const offStyle = {
      backgroundColor:'#FAFAFA',
      width:'calc(100% / ' + totalTabs + ' - 42px )',
      height:'100%',
      padding:'0px 20px',
      display:'inline-block',
      color:'#6e6e6e',
      textAlign:'left',
      cursor: 'pointer',
      border:'1px solid rgba(0,0,0,.12)',
    };

    //Style for check Icon
    const fontIconStyle = {
      width:'24px',
      height:'24px',
      padding:'8px'
    };

    const signProcessIconStyle = {
      padding:8,
      float:'right'
    };

    //Style for Tab text
    const txtStyle = {
      display:'inline-block',
      width:'calc(100% - ' + (24 + 2 * 8) + 'px - ' + (isSigningProcess? (24 + 2 * 8): 0) + 'px)',
      verticalAlign: 'top',
      color:'rgba(0,0,0,.54)',
      fontSize:14,
      lineHeight:3,
      //for text ... style
      whiteSpace: 'nowrap',
      overflow:'hidden',
      textOverflow: 'ellipsis'
    };

    return (
      <div
        key={name}
        style={selected ? onStyle : offStyle}
        onClick={onHandleChange}>
        {
          checked ?
            (<FontIcon color={'green'} className="material-icons" style={fontIconStyle} >check_box</FontIcon>) :
            (<FontIcon key="sn-bt-outpb" color={'#010101'} className="material-icons" style={fontIconStyle} >check_box_outline_blank</FontIcon>)
        }
        <div style={txtStyle}>{text}</div>
        {
          isSigningProcess ?
            (<img src={signIcon} style={signProcessIconStyle} />) :
            (null)
        }
      </div>
    );
  }
}

export default SignatureTab;