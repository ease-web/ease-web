import React, { Component } from 'react';
import * as _ from 'lodash';

import SignatureTab from './Tab';
import SignDocViewer from '../../../../CustomViews/SignDocViewer';
import PDFViewer from '../../../../CustomViews/PDFViewer';
import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';

import * as actions from '../../../../../actions/shieldApplication';


class ShieldSignature extends EABComponent {
  constructor(props, context) {
    super(props);
    let { shieldApplication } = context.store.getState();
    let { signature } = shieldApplication;

    this.state = Object.assign({}, this.state, {
      selectedTab: signature.signingTabIdx,
      isSigningProcess: signature.isSigningProcess,
      isSigned: signature.isSigned
    });
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.validate();
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  componentDidUpdate(prevProps, prevState) {
    let {isSigned} = this.state;
    if (!window.isEqual(prevState.isSigned, isSigned)) {
      this.validate();
    }
  }

  storeListener=()=>{
    let newState = {};
    let { shieldApplication } = this.context.store.getState();
    let { isSigningProcess, isSigned } = this.state;

    if (!window.isEqual(isSigningProcess, shieldApplication.signature.isSigningProcess)) {
      newState.isSigningProcess = shieldApplication.signature.isSigningProcess;
      newState.selectedTab = shieldApplication.signature.signingTabIdx;
    }

    if (!window.isEqual(isSigned, shieldApplication.signature.isSigned)) {
      newState.isSigned = shieldApplication.signature.isSigned;
    }

    if (!window.isEmpty(newState)) {
      this.setState(newState);
    }
  };

  handleChangeTab = (selectedTabId) => {
    this.setState({selectedTab: selectedTabId});
  };

  handleSign = (resultJSON, tabIdx) => {
    actions.getSignatureUpdatedUrl(this.context, resultJSON, tabIdx);
  };

  //Set signature field by search tag retrieve from CB
  genSignFieldCmdList = (tabIdx, arraySignFields) => {
    if (arraySignFields.length === 0 || tabIdx >= arraySignFields.length) {
      return [];
    }

    let signFieldList = [];
    _.forEach(arraySignFields[tabIdx], (signField, i) => {
      let { searchTag, name, page, x, y, offsetx, offsety, width, height } = signField;
      if (searchTag && searchTag.length > 0) {
        signFieldList.push(
          'name=' + searchTag + '|' +
          'type=formfield|subtype=signature|required=true|' +
          'searchtext=' + searchTag + '|' +
          'width=' + Math.round(width * SystemConstants.PAGE_SIZE.A4.WIDTH) + '|' +
          'height=' + Math.round(height * SystemConstants.PAGE_SIZE.A4.HEIGHT) + '|' +
          (offsetx ? 'offsetx=' + Math.round(offsetx * SystemConstants.PAGE_SIZE.A4.WIDTH) + '|' : '') +
          (offsety ? 'offsety=' + Math.round(offsety * SystemConstants.PAGE_SIZE.A4.HEIGHT) + '|' : '') +
          'tooltip=Signature|friendlyname=' + name + '|' +
          'archive_action=lock_all_if_signed'
        );
      } else if (page > 0) {
        signFieldList.push(
          'name=' + name + '|' +
          'type=formfield|subtype=signature|required=true|' +
          'page=' + page + '|' +
          'left=' + Math.round(x * SystemConstants.PAGE_SIZE.A4.WIDTH) + '|' +
          'bottom=' + Math.round(y * SystemConstants.PAGE_SIZE.A4.HEIGHT) + '|' +
          'width=' + Math.round(width * SystemConstants.PAGE_SIZE.A4.WIDTH) + '|' +
          'height=' + Math.round(height * SystemConstants.PAGE_SIZE.A4.HEIGHT) + '|' +
          'tooltip=Signature|friendlyname=' + name + '|' +
          'archive_action=lock_all_if_signed'
        );
      }
    });

    return signFieldList;
  }

  getSignDocSignField = (tabIdx) => {
    let { shieldApplication } = this.context.store.getState();
    let { signature } = shieldApplication;

    const { agentSignFields, clientSignFields } = signature;

    let agentSignFieldList = this.genSignFieldCmdList(tabIdx, agentSignFields);
    let clientSignFieldsList = this.genSignFieldCmdList(tabIdx, clientSignFields);
    // let textFieldsList = this.genTextFieldCmdListBySearchTag(tabIdx, textFields);
    let signFieldList = [...agentSignFieldList, ...clientSignFieldsList];

    return signFieldList;
  }

  getMaxNumberOfTabs = (totalTabs) => {
    if (totalTabs > 4 && totalTabs <= 6 || totalTabs >= 9) {
      return 3;
    } else {
      return 4;
    }
  }

  validate = () => {
    let {shieldApplication} = this.context.store.getState();
    let {isSigned} = this.state;
    let isSignMandatory = isSigned.slice(0, shieldApplication.signature.isFaChannel ? 2 : 3);
    let isCompleted = isSigned.length > 0 && isSigned.indexOf(false) < 0;
    let enableNextStep = isSignMandatory.length > 0 && isSignMandatory.indexOf(false) < 0;

    actions.validate(this.context, isCompleted, enableNextStep);
  }

  render() {
    let { store, langMap } = this.context;
    let { shieldApplication } = store.getState();
    let { signature } = shieldApplication;

    let { isFaChannel, numOfTabs, pdfStr, attUrls, signDocConfig } = signature;
    let { auths, docts, ids, postUrl, resultUrl, dmsId } = signDocConfig;

    let {
      selectedTab,
      isSigned,
      isSigningProcess
    } = this.state;

    let tabText = [];
    if (!isFaChannel) {
      tabText.push(window.getLocalizedText(langMap, 'signature.tab.needs', 'Financial needs analysis'));
    }
    tabText.push(window.getLocalizedText(langMap, 'proposal.proposal_title', 'Proposal'));

    let proposerFullName = _.get(shieldApplication.application, 'applicationForm.values.proposer.personalInfo.fullName');
    let phPostfix = proposerFullName ? ' (' + proposerFullName + ')' : '';
    tabText.push(window.getLocalizedText(langMap, 'application.form_title', 'Application Form') + phPostfix);
    let insured = _.get(shieldApplication.application, 'applicationForm.values.insured');
    if (insured && insured.length > 0) {
      _.forEach(insured, (la, index) => {
        let fullName = _.get(la, 'personalInfo.fullName');
        let laPostfix = fullName ? ' (' + fullName + ')' : '';
        tabText.push(window.getLocalizedText(langMap, 'application.form_title', 'Application Form') + laPostfix);
      });
    }

    const maxTabsInOneRow = this.getMaxNumberOfTabs(numOfTabs);
    const tabsRow = _.floor(numOfTabs / maxTabsInOneRow) + (numOfTabs % maxTabsInOneRow === 0 ? 0 : 1);

    let tabs = [];
    for (let i = 0; i < numOfTabs; i ++) {
      let currRow = _.floor(i / maxTabsInOneRow) + 1;

      tabs.push(
        <SignatureTab
          key={'tabBox' + i}
          name={'tabBox' + i}
          selected={(selectedTab === i)}
          checked={isSigned[i]}
          isSigningProcess={isSigningProcess[i]}
          totalTabs={currRow ===  tabsRow ? numOfTabs - (tabsRow - 1) * maxTabsInOneRow : maxTabsInOneRow}
          onHandleChange={() => this.handleChangeTab(i)}
          text={tabText[i]}
        />
      );
    }

    let signDocViewers = [];
    for (let i = 0; i < numOfTabs; i ++) {
      signDocViewers.push(
        <SignDocViewer
          key={'signDocView' + i}
          id={ids[i]}
          visible={selectedTab === i && (isSigned[i] || isSigningProcess[i])}
          signed={isSigned[i]}
          eSignBorder={true}
          postUrl={postUrl}
          pdfUrl={attUrls[i]}
          auth={auths[i]}
          docts={docts[i]}
          dmsId={dmsId}
          signFieldList={this.getSignDocSignField(i)}
          resultUrl={resultUrl}
          onHandleSign={(resultJSON) => {
            this.handleSign(resultJSON, i);
          }}
        />
      );
    }

    let readonlyContent =  pdfStr[selectedTab].length === 0 ?
      (
        <div style={{textAlign:'center', marginTop:'30px'}}>
          Please complete the Life Assured's details in the Application to proceed
        </div>
      ) : (
        <PDFViewer
          pdf={pdfStr[selectedTab]}
          containerStyle={{
            width: '100%',
            textAlign: 'center',
            overflowY: 'auto',
            flexGrow: 1,
            marginTop: 40 * tabsRow + 'px'
          }}
        />
      );

    return (
      <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column', overflowY: 'hidden'}}>
        <div key="tabsWrapper" style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', zIndex: '1000'}}>
          {tabs}
        </div>
        <div style={{display: 'flex', flexDirection: 'column', flexGrow: 1}} >
          {signDocViewers}
          {
            !isSigningProcess[selectedTab] && !isSigned[selectedTab] ?
              <div style={{
                borderTop:'0',
                borderLeft:'1px solid rgba(0,0,0,.12)',
                borderRight:'1px solid rgba(0,0,0,.12)',
                borderBottom:'1px solid rgba(0,0,0,.12)',
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1
              }}
              >
                {readonlyContent}
              </div> : null
          }
        </div>
      </div>
    );
  }
}

export default ShieldSignature;
