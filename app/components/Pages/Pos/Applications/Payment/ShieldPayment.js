import React from 'react';
import EABComponent from '../../../../Component';
import DynColumn from '../../../../DynViews/DynColumn';
import * as actions from '../../../../../actions/shieldApplication';
import _getOr from 'lodash/fp/getOr';

import * as _ from 'lodash';
import * as _v from '../../../../../utils/Validation';
import SystemConstants from '../../../../../constants/SystemConstants';
import { RELOAD_SHIELD_PAYMENT } from '../../../../../actions/payment';

class ShieldPayment extends EABComponent {
  constructor(props, context) {
    super(props);
    let { shieldApplication } = context.store.getState();
    let { application } = shieldApplication;
    this.state = {
      changedValues: _getOr({}, 'payment', application),
      error:{}
    };
  }

  componentDidUpdate(prevProps, prevState) {
    let {changedValues} = this.state;
    if (!window.isEqual(prevState.changedValues, changedValues)) {
      this.validate(changedValues);
    }
  }

  componentDidMount() {
    this.validate(this.state.changedValues);
    this.unsubscribe = this.context.store.subscribe(this.storeListener);

  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
    }
  }

  storeListener=()=>{
    let { store } = this.context;
    let { shieldApplication, payment = {} } = store.getState();
    let { application } = shieldApplication;
    let { changedValues} = this.state;
    let newState = {};

    if (!window.isEqual(changedValues, _getOr({}, 'payment', application)) && payment.updatePayment) {
      newState.changedValues = _getOr({}, 'payment', application);
    }

    if (!_.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  _checkPaymentComplete = () => {
    let {
      changedValues
    } = this.state;

    if (changedValues.initPayMethod || changedValues.totCashPortion === 0) {
      var payMethod = changedValues.initPayMethod;
      var trxStatus = changedValues.trxStatus;
      var trxTimestamp = changedValues.trxTime;
      var trxNo = changedValues.trxNo;

      if (_.findIndex(['crCard', 'eNets', 'dbsCrCardIpp'], (ele) => {return ele === payMethod;}) >= 0) {
        if (trxNo && trxStatus === 'Y' && trxTimestamp) {
          return true;
        }
      } else {
        return true;
      }
    }
    return false;
  }

  validate=(changedValues)=>{
    let { optionsMap, langMap, store } = this.context;
    let { shieldApplication } = store.getState();
    let { template, application } = shieldApplication;
    let { handleChangedValues } = this.props;

    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);

    //error.code

    let isCompleted = !_.isNumber(error.code) && this._checkPaymentComplete();
    changedValues.isCompleted = isCompleted;

    let enableNextStep = isCompleted && application.isAppFormProposerSigned && application.isAppFormInsuredSigned.indexOf(false) < 0;

    handleChangedValues(changedValues);

    actions.validate(this.context, isCompleted, enableNextStep);
    this.setState({error});
  }

  render() {
    let { shieldApplication } = this.context.store.getState();
    let { template, application } = shieldApplication;
    let { handleChangedValues } = this.props;
    let { changedValues, error } = this.state;

    return (
      <DynColumn
        customStyle={{width: '1000px', margin: '0 auto'}}
        onMenuItemChange={this.onMenuItemChange}
        isApp
        template={template}
        changedValues={changedValues}
        error={error}
        values={changedValues}
        validate={this.validate}
        completeChangedValues={() => {
          handleChangedValues(changedValues);
        }}
        // updateCompleteMenu={this.updateCompleteMenu}
      />
    );
  }
}

export default ShieldPayment;
