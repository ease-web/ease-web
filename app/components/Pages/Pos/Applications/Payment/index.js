import React, { Component, PropTypes } from 'react';
import PaymentMain from './Main';
import EABComponent from '../../../../Component';

class PaymentIndex extends EABComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <PaymentMain ref='payMain' updateAppBarTitle={this.props.updateAppBarTitle}/>
    );
  }
}

export default PaymentIndex;
