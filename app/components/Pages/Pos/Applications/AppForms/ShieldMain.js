import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';
import DynColumn from '../../../../DynViews/DynColumn';
import {getIcon} from '../../../../Icons/index';
import * as actions from '../../../../../actions/shieldApplication';
import {goShieldSupportDocuments} from '../../../../../actions/shieldSupportDocuments';

import * as _ from 'lodash';

class ShieldMain extends EABComponent {
  constructor(props) {
    super(props);
    this.state = {
      sections: {},
      currentIndex: -1,
      completedIndex: -1,
      activeIndex: 0,
      enableNextStep: false,
      changedValues: {},
      showErrorMsg: false,
      showWarningMsg: false,
      showSignExpiryWarning: false,
      submitStatus: '',
      isMandDocsAllUploaded: false
    };
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);

    let appbarItems = this.getAppBarItems();
    let appbarTitle = this.getAppBarTitle();
    let stepper = {
      onSaveChangeStep: this.onSaveChangeStep.bind(this),
      onGoStepComplete: this.onGoStepComplete.bind(this),
    };

    this.context.updateAppbar(Object.assign(appbarItems, {appbarTitle}));
    this.context.updateStepper(stepper);
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    let {stepperIndex, store} = this.context;
    let {shieldApplication} = store.getState();
    let {sections, currentIndex, completedIndex, activeIndex, enableNextStep, submitStatus, isMandDocsAllUploaded} = this.state;
    let newStepper = {};
    let newAppBar = {};

    if (!window.isEqual(prevState.sections, sections)) {
      let headers = [];
      _.forEach(sections.items, (item, i)=>{
        let {title} = item;
        headers.push({id: i, title});
      });

      newStepper = Object.assign({}, newStepper, {headers});
    }

    if (prevState.currentIndex !== currentIndex ||
      prevState.completedIndex !== completedIndex ||
      prevState.activeIndex !== activeIndex ||
      prevState.enableNextStep !== enableNextStep ||
      prevState.submitStatus !== submitStatus ||
      prevState.isMandDocsAllUploaded !== isMandDocsAllUploaded
    ) {
      newAppBar = Object.assign({}, newAppBar,
        {title:this.getAppBarTitle(currentIndex)},
        {itemsIndex:this.getAppBarItemsIndex(currentIndex, enableNextStep, isMandDocsAllUploaded, submitStatus)}
      );

      /**
       *  Update Stepper
       */
      newStepper = Object.assign({}, newStepper, {
        index: currentIndex,
        completed: completedIndex,
        active: activeIndex
      });
    }

    if (!window.isEmpty(newAppBar)) {
      this.context.updateAppbar(newAppBar);
    }
    if (!window.isEmpty(newStepper)) {
      this.context.updateStepper(newStepper);
    }
  }

  storeListener=()=>{
    let { shieldApplication } = this.context.store.getState();
    let { sections, currentIndex, completedIndex, activeIndex, enableNextStep, showWarningMsg, showSignExpiryWarning, showErrorMsg, submitStatus, isMandDocsAllUploaded } = this.state;
    let newState = {};

    if (!window.isEqual(sections, shieldApplication.stepper.sections)) {
      newState.sections = shieldApplication.stepper.sections;
    }

    if (currentIndex !== shieldApplication.stepper.index.current) {
      newState.currentIndex = shieldApplication.stepper.index.current;
    }

    if (completedIndex !== shieldApplication.stepper.index.completed) {
      newState.completedIndex = shieldApplication.stepper.index.completed;
    }

    if (activeIndex !== shieldApplication.stepper.index.active) {
      newState.activeIndex = shieldApplication.stepper.index.active;
    }

    if (enableNextStep !== shieldApplication.stepper.enableNextStep) {
      newState.enableNextStep = shieldApplication.stepper.enableNextStep;
    }

    if (submitStatus !== shieldApplication.application.submitStatus) {
      newState.submitStatus = shieldApplication.application.submitStatus;
    }

    if (isMandDocsAllUploaded !== shieldApplication.application.isMandDocsAllUploaded) {
      newState.isMandDocsAllUploaded = shieldApplication.application.isMandDocsAllUploaded;
    }

    if (showWarningMsg !== shieldApplication.warningMsg.showDialog) {
      newState.showWarningMsg = shieldApplication.warningMsg.showDialog;
    }

    if (showSignExpiryWarning !== shieldApplication.warningMsg.showSignExpiryWarning) {
      newState.showSignExpiryWarning = shieldApplication.warningMsg.showSignExpiryWarning;
    }

    if (showErrorMsg !== shieldApplication.errorMsg.showDialog) {
      newState.showErrorMsg = shieldApplication.errorMsg.showDialog;
    }

    if (!_.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  getAppBarItems=()=>{
    let {muiTheme} = this.context;
    let {shieldApplication} = this.context.store.getState();

    let btnSupportDocsWarning = {
      id: 'eappSupDocs',
      type: 'flatButton',
      title: {
        'en':'SUPPORTING DOCS'
      },
      flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
      action: ()=>{
        this.goSupportingDocs(shieldApplication.application.id);
      }
    };

    let btnSupportDocsCompleted = {
      id: 'eappSupDocs',
      type: 'flatButton',
      title: {
        'en':'SUPPORTING DOCS'
      },
      action: ()=>{
        this.goSupportingDocs(shieldApplication.application.id);
      }
    };

    let btnSave = {
      id: 'eappSave',
      type: 'flatButton',
      title: {
        'en':'SAVE'
      },
      action: ()=>{
        this.saveOnClick();
      }
    };

    let btnNextDisabled = {
      id: 'next',
      type: 'flatButton',
      title: {
        'en':'NEXT'
      },
      'disabled':true
    };

    let btnNextEnabled = {
      id: 'next',
      type: 'flatButton',
      title: {
        'en':'NEXT'
      },
      action: ()=>{
        this.nextOnClick();
      }
    };

    let btnSubmitDisabled = {
      id: 'next',
      type: 'flatButton',
      title: {
        'en':'SUBMIT'
      },
      'disabled':true
    };

    let btnSubmitEnabled = {
      id: 'next',
      type: 'flatButton',
      title: {
        'en':'SUBMIT'
      },
      action: ()=>{
        this.submitOnClick();
      }
    };

    let btnDone = {
      id: 'next',
      type: 'flatButton',
      title: {
        'en':'Done'
      },
      action: ()=>{
        this.closePage();
      }
    };

    return {
      menu: {
        icon: 'close',
        action: ()=>{
          this.closePage();
        }
      },
      items: [
        //Step: eApp / ePay itemsIndex: 0
        [btnSupportDocsWarning, btnSave, btnNextDisabled],
        //Step: eApp / ePay itemsIndex: 1
        [btnSupportDocsWarning, btnSave, btnNextEnabled],
        //Step: eApp / ePay itemsIndex: 2
        [btnSupportDocsCompleted, btnSave, btnNextDisabled],
        //Step: eApp / ePay itemsIndex: 3
        [btnSupportDocsCompleted, btnSave, btnNextEnabled],
        //Step: eSign itemsIndex: 4
        [btnSupportDocsWarning, btnNextDisabled],
        //Step: eSign itemsIndex: 5
        [btnSupportDocsWarning, btnNextEnabled],
        //Step: eSign itemsIndex: 6
        [btnSupportDocsCompleted, btnNextDisabled],
        //Step: eSign itemsIndex: 7
        [btnSupportDocsCompleted, btnNextEnabled],
        //Step: eSubmit itemsIndex: 8
        [btnSupportDocsWarning, btnSubmitDisabled],
        //Step: eSubmit itemsIndex: 9
        [btnSupportDocsCompleted, btnSubmitDisabled],
        //Step: eSubmit itemsIndex: 10
        [btnSupportDocsCompleted, btnSubmitEnabled],
        //Step: eSubmit itemsIndex: 11
        [btnDone]
      ]
    };
  }

  getAppBarItemsIndex=(index, completed, isMandDocsAllUploaded, submitStatus)=>{
    let itemsIndex = -1;
    //set default itemsIndex for next step
    switch (index) {
      case SystemConstants.EAPP.STEP.APPLICATION:
      case SystemConstants.EAPP.STEP.PAYMENT:
        if (completed) {
          itemsIndex = isMandDocsAllUploaded ? 3 : 1;
        } else {
          itemsIndex = isMandDocsAllUploaded ? 2 : 0;
        }
        break;
      case SystemConstants.EAPP.STEP.SIGNATURE:
        if (completed) {
          itemsIndex = isMandDocsAllUploaded ? 7 : 5;
        } else {
          itemsIndex = isMandDocsAllUploaded ? 6 : 4;
        }
        break;
      case SystemConstants.EAPP.STEP.SUBMISSION:
        if (submitStatus === 'SUCCESS') {
          itemsIndex = 11;
        } else if (completed) {
          itemsIndex = isMandDocsAllUploaded ? 10 : 8;
        } else {
          itemsIndex = isMandDocsAllUploaded ? 9 : 8;
        }

        break;
      default:
        itemsIndex = -1;
    }
    return itemsIndex;
  }

  getAppBarTitle = (index) => {
    let title = '';
    let { shieldApplication } = this.context.store.getState();

    switch (index) {
      case 0:
        title = 'Application';
        break;
      case 1:
        title = 'Signature';
        break;
      case 2:
        title = 'Payment';
        break;
      case 3:
        title = 'Submission';
        break;
      default:
        title = 'Application';
        break;
    }

    if (shieldApplication.application) {
      title += ' (' + (shieldApplication.application.id) + ')';
    }
    return title;
  }

  getCrossAgeWarningMsg = (msgCode) => {
    let msg = '';
    let title = '';
    switch (msgCode) {
      case SystemConstants.EAPP.CROSSAGE_STATUS.WILL_CROSSAGE:
        title = 'REMINDER';
        msg = 'Life Insured\'s age will change in less than 14 days. This may result in additional requirements. Please consider to opt for backdating option (if applicable) or regenerate the Illustration once age has changed.';
        break;
      case SystemConstants.EAPP.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED:
        title = 'REMINDER';
        msg = 'Life Insured\'s age has changed. Please consider to opt for backdating option (if applicable) or regenerate the Illustration.';
        break;
      case SystemConstants.EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED:
        title = 'WARNING';
        msg = 'Life Insured\'s age has changed. This may result in additional requirements.';
        break;
      default:
        title = '';
        msg = '';
        break;
    }

    return {msg, title};
  }

  onGoStepComplete=(nextIndex)=>{
  }

  onSaveChangeStep=(nextIndex, callback)=>{
    let { shieldApplication } = this.context.store.getState();
    let { changedValues } = this.state;
    let changeStep = this.context.stepperIndex !== nextIndex;

    if (changeStep) {
      actions.goNextStep(this.context, shieldApplication.application.id, this.context.stepperIndex, nextIndex, changedValues, callback);
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION) {
      actions.saveAppForm(this.context, shieldApplication.application.id, changedValues, false, callback);
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.PAYMENT) {
      actions.savePaymentMethods(this.context, shieldApplication.application.id, changedValues, callback);
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.SUBMISSION) {
      actions.saveSubmissionValues(this.context, shieldApplication.application.id, changedValues, callback);
    } else {
      callback();
    }
  }

  closePage=()=>{
    if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION 
      || this.context.stepperIndex === SystemConstants.EAPP.STEP.PAYMENT
      || this.context.stepperIndex === SystemConstants.EAPP.STEP.SUBMISSION) {
      // let changed = this.state.completed !== 0;

      this.onSaveChangeStep(this.context.stepperIndex, ()=>{
        actions.closePage(this.context);
      });
    } else {
      actions.closePage(this.context);
    }
  }

  goSupportingDocs=(appId)=>{
    this.onSaveChangeStep(this.context.stepperIndex, ()=>{
      goShieldSupportDocuments(this.context, appId);
    });
  }

  saveOnClick=(callback)=>{
    this.onSaveChangeStep(this.context.stepperIndex, ()=>{});
  }

  nextOnClick=()=>{
    let nextStepperIndex = this.context.stepperIndex + 1;
    this.onSaveChangeStep(nextStepperIndex, ()=>{
      this.onGoStepComplete(nextStepperIndex);
    });
  }

  submitOnClick=()=>{
    let { shieldApplication } = this.context.store.getState();
    let { changedValues } = this.state;
    actions.performSubmission(this.context, shieldApplication.application.id, changedValues, shieldApplication.template);
  }

  render() {
    let { shieldApplication } = this.context.store.getState();
    let { warningMsg, errorMsg } = shieldApplication;
    let { sections, currentIndex, showWarningMsg, showSignExpiryWarning, showErrorMsg } = this.state;
    let crossAgeWarning = this.getCrossAgeWarningMsg(warningMsg.msgCode);

    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', height: '100%'}}>
        <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
          {
            _.get(sections, `items[${currentIndex}]`) ?
              <DynColumn
                ref='dynCol'
                customStyle={{ flexGrow: 1 }}
                template={sections.items[currentIndex]}
                handleChangedValues={(changedValues) => {
                  this.setState({changedValues});
                }}
              />
              : null
          }
        </div>
        <Dialog
          title='Error'
          open={showErrorMsg}
          actions={[
            <FlatButton
              label='OK'
              primary={true}
              onTouchTap={()=>actions.hideErrorMessage(this.context)}
            />
          ]}
        >
          {errorMsg.msg}
        </Dialog>
        <Dialog
          title={crossAgeWarning.title}
          open={showWarningMsg}
          actions={[
            <FlatButton
              label='OK'
              primary={true}
              onTouchTap={() => {
                if (warningMsg.msgCode === SystemConstants.EAPP.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED) {
                  actions.invalidateApplication(this.context, shieldApplication.application.id);
                } else {
                  actions.hideWarningMessage(this.context);
                }
              }}
            />
          ]}
        >
          {crossAgeWarning.msg}
        </Dialog>
        <Dialog
          title={'WARNING'}
          open={showSignExpiryWarning}
          actions={[
            <FlatButton
              label='OK'
              primary={true}
              onTouchTap={() => {
                actions.setSignExpiryShown(this.context);
              }}
            />
          ]}
        >
          The customer’s signature will expire in less than 7 days. Please submit the case.
        </Dialog>
      </div>
    );
  }
}

export default ShieldMain;
