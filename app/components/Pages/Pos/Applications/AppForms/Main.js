import React from 'react';
import {FlatButton, Dialog} from 'material-ui';
import * as actions from '../../../../../actions/application';
import * as signActions from '../../../../../actions/signature';
import {goSupportDocuments} from '../../../../../actions/supportDocuments';
import styles from '../../../../Common.scss';
import * as _ from 'lodash';
import {getIcon} from '../../../../Icons/index';
import PropTypes from 'prop-types';
import DynColumn from '../../../../DynViews/DynColumn';
import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';
import {
  showErrorMsg
} from '../../../../../actions/GlobalActions';
import {
  setSignExpiryShown, dismissCrosseAppAgePrompt
} from '../../../../../actions/application';

import * as _v from '../../../../../utils/Validation';

class Main extends EABComponent {
  constructor(props, context) {
      super(props);
      let {appForm} = context.store.getState();

      this.state = Object.assign({}, this.state, {
        template: appForm.template,
        index: appForm.application.stepperIndex || 0,
        changedValues: appForm.application.applicationForm.values,
        isMandDocsAllUploaded: false,
        showCrossedAgeSignedMsg: false,
        isBackDate: 'N'
      });
  }

  getChildContext() {
    return {
      validRefValues: this.state.changedValues
    };
  }

  componentDidMount() {
    let self = this;

    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    let {
      appForm,
      pos
    } = this.context.store.getState();
    let appbar = this.getAppBarItems();
    let stepper = {
      onSaveChangeStep: this.onSaveChangeStep.bind(this),
      onGoStepComplete: this.onGoStepComplete.bind(this),
      'index': appForm.application.appStep || 0
    };

    if (appForm.showSignExpiryWarning) {
      showErrorMsg(this.context, {
        errorMsg: "The customer’s signature will expire in less than 7 days. Please submit the case.",
        hasTitle: true,
        isError: false,
        action: function() {
          setSignExpiryShown(self.context);
        }
      });
    }
    let changedValues = _.cloneDeep(appForm.application.applicationForm.values);
    this.updateAppBarTitle(SystemConstants.EAPP.STEP.APPLICATION);
    this.validate(changedValues, true, appbar, stepper);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.isMandDocsAllUploaded != this.state.isMandDocsAllUploaded || prevState.hasPolicyNumber != this.state.hasPolicyNumber) {
      this.validateAfterInitStore();
    }
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  validateAfterInitStore=()=>{
    let {
      changedValues
    } = this.state;

    this.validate(changedValues, false);
  }

  updateAppBarTitle = (index) => {
    let title = '';
    let { appForm, pos } = this.context.store.getState();

    switch (index) {
      case 0:
        title = 'Application';
        break;
      case 1:
        title = 'Signature';
        break;
      case 2:
        title = 'Payment';
        break;
      case 3:
        title = 'Submission';
        break;
      default:
        title = 'Application';
        break;
    }

    if (pos.currentPage === 'eApp' && appForm.application) {
      title += ' (' + (appForm.application.policyNumber || appForm.application.id) + ')';
    }

    this.context.updateAppbar({title});
  }

  getAppBarItems=()=>{
    let {muiTheme} = this.context;
    let {
      appForm,
      pos
    } = this.context.store.getState();
    var self = this;

    let appbar = {
      menu: {
        icon: 'close',
        action: ()=>{
          self.closePage();
        }
      },
      items:[
        //Step: eApp / ePay
        //itemsIndex: 0
        //SUPPORTING DOCS: warning
        //SAVE: enable
        //NEXT: disable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
            action: ()=>{
              self.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'eappSave',
            type: 'flatButton',
            title: {
              'en':'SAVE'
            },
            action: ()=>{
              self.save();
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en':'NEXT'
            },
            'disabled':true
          }
        ],
        //Step: eApp / ePay
        //itemsIndex: 1
        //SUPPORTING DOCS: warning
        //SAVE: enable
        //NEXT: enable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
            action: ()=>{
              self.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'eappSave',
            type: 'flatButton',
            title: {
              'en':'SAVE'
            },
            action: ()=>{
              self.save();
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en':'NEXT'
            },
            action: ()=>{
              self.nextOnClick();
            }
          }
        ],
        //Step: eApp / ePay
        //itemsIndex: 2
        //SUPPORTING DOCS: complete
        //SAVE: enable
        //NEXT: disable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            action: ()=>{
              self.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'eappSave',
            type: 'flatButton',
            title: {
              'en':'SAVE'
            },
            action: ()=>{
              self.save();
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en':'NEXT'
            },
            'disabled':true
          }
        ],
        //Step: eApp / ePay
        //itemsIndex: 3
        //SUPPORTING DOCS: complete
        //SAVE: enable
        //NEXT: enable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            action: ()=>{
              self.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'eappSave',
            type: 'flatButton',
            title: {
              'en':'SAVE'
            },
            action: ()=>{
              self.save();
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en':'NEXT'
            },
            action: ()=>{
              self.nextOnClick();
            }
          }
        ],
        //Step: eSign
        //itemsIndex: 4
        //SUPPORTING DOCS: warning
        //SAVE: none
        //NEXT: disable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
            action: ()=>{
              this.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en': 'NEXT'
            },
            disabled: true
          }
        ],
        //Step: eSign
        //itemsIndex: 5
        //SUPPORTING DOCS: warning
        //SAVE: none
        //NEXT: enable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
            action: ()=>{
              this.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en': 'NEXT'
            },
            action: ()=>{
              this.nextOnClick();
            }
          }
        ],
        //Step: eSign
        //itemsIndex: 6
        //SUPPORTING DOCS: complete
        //SAVE: none
        //NEXT: disable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            action: ()=>{
              this.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en': 'NEXT'
            },
            disabled: true
          }
        ],
        //Step: eSign
        //itemsIndex: 7
        //SUPPORTING DOCS: complete
        //SAVE: none
        //NEXT: enable
        [
          {
            id: 'eappSupDocs',
            type: 'flatButton',
            title: {
              'en':'SUPPORTING DOCS'
            },
            action: ()=>{
              this.goSuppDocs(appForm.application.id);
            }
          },
          {
            id: 'next',
            type: 'flatButton',
            title: {
              'en': 'NEXT'
            },
            action: ()=>{
              this.nextOnClick();
            }
          }
        ],
        //Step: eSubmit
        //itemsIndex: 8
        //SUPPORTING DOCS: warning
        //SAVE: none
        //NEXT: none
        //SUBMIT: disable
        [
          {
            id: 'suppDocs',
            type: 'flatButton',
            title: {
              'en': 'SUPPORTING DOCS'
            },
            flatButtonIcon: getIcon('warning', muiTheme.palette.warningColor),
            action: () => {
              goSupportDocuments(this.context, appForm.application.id, 'appFormSubmission');
            }
          },
          {
            id: 'submit',
            type: 'flatButton',
            title: {
              'en': 'SUBMIT'
            },
            disabled: true
          }
        ],
        //Step: eSubmit
        //itemsIndex: 9
        //SUPPORTING DOCS: complete
        //SAVE: none
        //NEXT: none
        //SUBMIT: disable
        [
          {
            id: 'suppDocs',
            type: 'flatButton',
            title: {
              'en': 'SUPPORTING DOCS'
            },
            action: () => {
              goSupportDocuments(this.context, appForm.application.id, 'appFormSubmission');
            }
          },
          {
            id: 'submit',
            type: 'flatButton',
            title: {
              'en': 'SUBMIT'
            },
            disabled: true
          }
        ],
        //Step: eSubmit
        //itemsIndex: 10
        //SUPPORTING DOCS: complete
        //SAVE: none
        //NEXT: none
        //SUBMIT: enable
        [
          {
            id: 'suppDocs',
            type: 'flatButton',
            title: {
              'en': 'SUPPORTING DOCS'
            },
            action: () => {
              goSupportDocuments(this.context, appForm.application.id, 'appFormSubmission');
            }
          },
          {
            id: 'submit',
            type: 'flatButton',
            title: {
              'en': 'SUBMIT'
            },
            action:() => {
              this.refs.dynCol.refs.submission.refs.submitMain.submitOnClick();
            }
          }
        ],
        //Step: eSubmit
        //itemsIndex: 11
        //SUPPORTING DOCS: complete
        //SAVE: none
        //NEXT: none
        //SUBMIT: none
        //DONE: enable
        [
          {
            id: 'done',
            type: 'flatButton',
            title: {
              'en': 'DONE'
            },
            action: () => {
              self.closePage();
            }
          }
        ]
      ]
    };

    return appbar;
  }

  getAppBarItemsIndex=(completed, isMandDocsAllUploaded)=>{
    let itemsIndex = -1;
    // if (completed === 0) {
    if (completed >= 0) {
      itemsIndex = isMandDocsAllUploaded ? 3 : 1;
    } else {
      itemsIndex = isMandDocsAllUploaded ? 2 : 0;
    }
    return itemsIndex;
  }

  closePage=()=>{
    let self = this;
    let cb = ()=>{
      actions.backToPosMain(self.context);
    }

    let { appForm } = this.context.store.getState();


    if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION) {
      let changed = this.state.completed !== 0;
      actions.saveFormValues(this.context, appForm.id, null, this.state.changedValues, false, {resetAppStep: changed && appForm.application.appStep === SystemConstants.EAPP.STEP.SIGNATURE }, cb);
    } else {
      cb();
    }
  }

  nextOnClick = () => {
    this.context.goStep(this.context.stepperIndex + 1, true);
    this.setState({valid: false, isNotFormPage: true});
  }

  onMenuItemChange = (cb, _getPolicyNumber) => {
    let {
      store
    } = this.context;

    let {
      appForm
    } = store.getState();

    if (!isEqual(appForm.application.applicationForm.values, this.state.changedValues)) {
      actions.saveFormValues(this.context, appForm.id, null, this.state.changedValues, false, { toGetPolicyNumber: _getPolicyNumber }, cb);
    } else {
      cb();
    }

    this.validate(this.state.changedValues, false);
  }

  goSuppDocs=(appId)=>{
    let {store} = this.context;
    let {appForm} = store.getState();

    if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION) {
      actions.saveFormValues(this.context, appForm.id, null, this.state.changedValues, false, {}, () => {
        goSupportDocuments(this.context, appId);
      });
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.PAYMENT) {
      this.refs.dynCol.refs.payment.refs.payMain.goSuppDocs(appId);
    } else {
      goSupportDocuments(this.context, appId);
    }
  }

  checkMenuError = (menuError) =>{
    let result = true;
    if(menuError){
      if(menuError.proposer){
        if(menuError.proposer.code)
          result = false;
      }
      if(menuError.insured){
        for (let cid in menuError.insured) {
          if (menuError.insured[cid] && menuError.insured[cid].code) {
            result = false;
          }
        }
      }
    }

    return result;
  }

updateCompleteMenu = (error, values) =>{
  if(!values.completedMenus){
    values.completedMenus = [];
  }
  let completedMenus = values.completedMenus;
  let menus = values.menus || [];
  for(let mIndex = 0; mIndex<menus.length; mIndex++){
    let menu = menus[mIndex];
    if(error && error[menu] && error[menu].code){
      let cmIndex = completedMenus.indexOf(menu);
      if(cmIndex > -1){
        completedMenus.splice(cmIndex, 1);
      }
    }else{
      if(completedMenus.indexOf(menu) == -1){
        completedMenus.push(menu);
      }
    }
  }
}

checkMenuChecked = () =>{
  let values = this.state.changedValues;
  let checkedMenu = values.checkedMenu;
  let menus = values.menus;
  if(!checkedMenu || checkedMenu.length == 0){
    return false
  }else{
    for(let mIndex = 0; mIndex<menus.length; mIndex++){
      if(checkedMenu.indexOf(menus[mIndex]) == -1){
        if (menus[mIndex] != "menu_plan") {
          return false;
        }
      }
    }
  }

  return true;
}

getStepperPageIndex = (error) => {
  let { store } = this.context;
  let { appForm } = store.getState();

  // check policy number exists
  if (!appForm.application.policyNumber) {
    if (error.menu_insure && !error.menu_insure.code) {
      error.menu_insure.code = 711;
      if (error.menu_insure.proposer) {
        error.menu_insure.proposer.code = 711;
      }
    } else if (!error.menu_insure && error.menu_declaration && !error.menu_declaration.code) {
      error.menu_declaration.code = 711;
      if (error.menu_declaration.proposer) {
        error.menu_declaration.proposer.code = 711;
      }
    }
  } else {
    if (error.menu_insure && error.menu_insure.code === 711) {
      error.menu_insure.code = false;
      if (error.menu_insure.proposer) {
        error.menu_insure.proposer.code = false;
      }
    } else if (!error.menu_insure && error.menu_declaration && error.menu_declaration.code === 711) {
      error.menu_declaration.code = false;
      if (error.menu_declaration.proposer) {
        error.menu_declaration.proposer.code = false;
      }
    }
  }

  let isCompleted = false;
  let checkMenuError = this.checkMenuError;
  isCompleted = checkMenuError(error.menu_declaration) && checkMenuError(error.menu_insure) && checkMenuError(error.menu_person) && checkMenuError(error.menu_plan) && checkMenuError(error.menu_residency);
  let menuChecked = this.checkMenuChecked();
  isCompleted = isCompleted && menuChecked;

  let completed = {};
  switch (appForm.application.appStep) {
    case SystemConstants.EAPP.STEP.APPLICATION:
      completed = { completed: isCompleted ? 0 : -1 };
      break;
    case SystemConstants.EAPP.STEP.SIGNATURE:
      if (appForm.application.isFullySigned) {
        completed = { completed: appForm.application.appStep };
      } else if (appForm.application.isStartSignature) {
        completed = { completed: appForm.application.appStep - 1 };
      } else  {
        completed = { completed: isCompleted ? 0 : -1 };
      }
      break;
    default:
      completed = { completed: appForm.application.appStep - 1 };
      break;
  }

  return completed;
}

validate=(changedValues, init, appbar={}, stepper={})=>{
  let {appForm} = this.context.store.getState();
  let {index, values, isMandDocsAllUploaded} = this.state;
  let template = appForm.template;
  let {optionsMap, langMap } = this.context;
  if(!values){
    values = _.cloneDeep(appForm.application.applicationForm.values);
  }
  let error = {};
  _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error)
  changedValues.isCompleted = error.code?false:true;
  let stepperCompleted = this.getStepperPageIndex(error);
  let currentIndex = init?0:this.context.stepperIndex;
  let activeIndex = stepper.index || 0;
  let valid = !_.at(error, `sections[${currentIndex}].code`)[0];

  let headers = [];
  _.forEach(template.items, (item, i)=>{
    let {title, disabled} = item;
    disabled = disabled || _.at(error, `sections[${i}].skip`)[0];
    headers.push({id: i, title, disabled});
  })

  let stepperChange = init ? {active: stepperCompleted.completed == 0 && activeIndex == 0? activeIndex + 1: activeIndex}: {};

  this.context.updateAppbar(Object.assign(appbar, {itemsIndex:this.getAppBarItemsIndex(stepperCompleted.completed, isMandDocsAllUploaded)}));

  this.context.updateStepper(Object.assign(stepper, {headers}, stepperCompleted, stepperChange));

  this.setState({completed: stepperCompleted.completed, changedValues, values, template, error:error, inited:true, hasPolicyNumber: appForm.application.policyNumber, isNotFormPage : (stepper.index != 0)});
}


  save = () => {
    if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION) {
      let {
        store
      } = this.context;

      let {
        appForm
      } = store.getState();

      actions.saveFormValues(this.context, appForm.id, null, this.state.changedValues, false, {}, () => { });
    } else {
      //for payment page
      this.onSaveChangeStep(this.context.stepperIndex, ()=>{});
    }
  }

  onGoStepComplete=(nextIndex)=>{
    let {error, isMandDocsAllUploaded} = this.state;
    let valid = !_.at(error, `sections[${nextIndex}].code`)[0];
    let itemsIndex = -1;

    //set default itemsIndex for next step
    switch (nextIndex) {
      case SystemConstants.EAPP.STEP.APPLICATION:
        itemsIndex = this.getAppBarItemsIndex(valid ? 0 : -1, isMandDocsAllUploaded);
        break;
      case SystemConstants.EAPP.STEP.SIGNATURE:
        itemsIndex = isMandDocsAllUploaded ? 6 : 4;
        break;
      case SystemConstants.EAPP.STEP.PAYMENT:
        itemsIndex = isMandDocsAllUploaded ? 2 : 0;
        break;
      case SystemConstants.EAPP.STEP.SUBMISSION:
        itemsIndex = isMandDocsAllUploaded ? 9 : 8;
        break;
      default:
        itemsIndex = -1;
    }

    this.context.updateAppbar(Object.assign(this.getAppBarItems(), {itemsIndex}));
    this.updateAppBarTitle(nextIndex);
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let {store} = this.context;
      let {appForm} = store.getState();
      let newState = {};

      if(appForm.application.policyNumber && !this.state.hasPolicyNumber){
        this.context.updateAppbar({title : "Application (" + appForm.application.policyNumber +")"})
        newState.hasPolicyNumber = true;
      }

      if(!isEqual(this.state.values, appForm.application.applicationForm.values)){
        newState.values = _.cloneDeep(appForm.application.applicationForm.values);
      }

      if(!isEqual(this.state.template, appForm.template)){
        newState.template = appForm.template;
      }

      if(this.state.isMandDocsAllUploaded != appForm.isMandDocsAllUploaded) {
        newState.isMandDocsAllUploaded = appForm.isMandDocsAllUploaded;
      }

      if (this.state.showCrossedAgeSignedMsg !== appForm.crossAgeObj.showCrossedAgeSignedMsg) {
        newState.showCrossedAgeSignedMsg = appForm.crossAgeObj.showCrossedAgeSignedMsg;
      }

      if (this.state.isBackDate != appForm.isBackDate) {
        if (!_.isEmpty(appForm.changedValues)) {
          newState.isBackDate = appForm.isBackDate;
          newState.changedValues = _.cloneDeep(appForm.changedValues);
        }
      }

      if (!isEmpty(newState)){
        this.setState(newState);
      }
    }
  }

  onSaveChangeStep=(nextIndex, callback)=>{
    let {
      store
    } = this.context;

    let {
      appForm,
      signature
    } = store.getState();

    if (this.context.stepperIndex === SystemConstants.EAPP.STEP.APPLICATION && nextIndex === SystemConstants.EAPP.STEP.SIGNATURE){
      actions.saveFormValues(this.context, appForm.id, null, this.state.changedValues, !appForm.application.isStartSignature, {formCompleted: true}, () => {
        callback();
      });
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.SIGNATURE && nextIndex === SystemConstants.EAPP.STEP.PAYMENT) {
      let isAllSigned = signature.isSigned.length > 0 && signature.isSigned.indexOf(false) < 0;
      if (isAllSigned) {
        signActions.updateAppStep(this.context, appForm.application.id, this.context.stepperIndex, ()=>{
          callback();
        });
      } else {
        callback();
      }
    } else if (this.context.stepperIndex === SystemConstants.EAPP.STEP.PAYMENT) {
      this.refs.dynCol.refs.payment.refs.payMain.saveBeforeChangeStepperIndex(nextIndex, callback);
    } else {
      callback();
    }
  }

  checkAddr = (tabValues) => {
    let {store} = this.context;
    let {appForm} = store.getState();
    if(appForm.application.isStartSignature)
      return false;
    let cValues = this.state.changedValues;
    let ph = cValues.proposer;
    let pPersonal = ph.personalInfo;
    let lPersonal = tabValues.personalInfo;
    if(pPersonal.addrCountry == lPersonal.addrCountry){
      if(pPersonal.addrCountry == "R51"){
        return pPersonal.addrCity == lPersonal.addrCity;
      }else{
        return true;
      }
    }

  }

  copyAddr = (tabValues) => {

    let cValues = this.state.changedValues;
    let ph = cValues.proposer;
    let pPersonal = ph.personalInfo;
    let lPersonal = tabValues.personalInfo;

    if(!cValues.insured){
      cValues.insured = []
    }

    let update = false;
    for(let i = 0; i < cValues.insured.length; i++){
      if(lPersonal.cid == cValues.insured[i].personalInfo.cid){
        cValues.insured[i].personalInfo.addrCity = pPersonal.addrCity;
        cValues.insured[i].personalInfo.addrBlock = pPersonal.addrBlock;
        cValues.insured[i].personalInfo.addrEstate = pPersonal.addrEstate;
        cValues.insured[i].personalInfo.addrStreet = pPersonal.addrStreet;
        cValues.insured[i].personalInfo.postalCode = pPersonal.postalCode;
        cValues.insured[i].personalInfo.unitNum = pPersonal.unitNum;
        update = true;
        break;
      }
    }
    if(update){
      this.setState({changedValues:cValues})
    }
  }

  crossAgeDialog = () => {
    let {showCrossedAgeSignedMsg} = this.state;
    if (!showCrossedAgeSignedMsg) {
      return null;
    }

    let crossAgeText = '';

    let {appForm} = this.context.store.getState();
    if (appForm.crossAgeObj.proposerCrossed && appForm.crossAgeObj.insuredCrossed) {
      crossAgeText = 'Life Insured\'s and Proposer\'s';
    } else {
      crossAgeText = appForm.crossAgeObj.insuredCrossed ? 'Life Insured\'s' : 'Proposer\'s';
    }
    crossAgeText += ' age has changed. This may result in additional requirements.';

    return (
      <Dialog
        title='WARNING'
        open={showCrossedAgeSignedMsg}
        actions={[
            <FlatButton
            label='OK'
            primary={true}
            onTouchTap={()=>{
              dismissCrosseAppAgePrompt(this.context);
              this.setState({showCrossedAgeSignedMsg: false});
            }}
            />
        ]}
      >
          {crossAgeText}
      </Dialog>
    );
  };

  getContent=()=>{
    let {stepperIndex} = this.context;
    // if(stepperIndex == 0){
    //   let { error } = this.state;
    //   let { completed } = this.getStepperPageIndex(error);
    //   let appbar = this.getAppBarItems();

      // setTimeout(()=>{
      //   this.context.updateAppbar(Object.assign(appbar, {itemsIndex:this.getAppBarItemsIndex(completed)}));
      // }, 100)
    // }
    let { template, changedValues, values, error} = this.state;
    values = values || changedValues;
    if(!checkExist(template, 'items'))
      return;
    let field=template.items[stepperIndex];

    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <DynColumn
          ref='dynCol'
          customStyle = {{flexGrow: 1}}
          stepperIndex = {stepperIndex}
          onMenuItemChange={this.onMenuItemChange}
          isApp = {true}
          template={field}
          changedValues={changedValues}
          error={error}
          values={values}
          validate={this.validate}
          updateCompleteMenu={this.updateCompleteMenu}
          extra = {{checkAddr: this.checkAddr, copyAddr: this.copyAddr}}
          updateAppBarTitle = {this.updateAppBarTitle}
          ccy={_.get(changedValues, 'proposer.extra.ccy', 'SGD')}
        />
      </div>
    )
  }


  render() {

    let {
      template,
      changedValues,
      inited
    } = this.state;

    if (!template || !changedValues || !inited) {
      return null;
    }

    let content = this.getContent();
    return (
      <div style={{display: 'flex', flexGrow: 1, flexDirection: 'column', height: '100%'}}>
        {this.crossAgeDialog()}
        {content}
      </div>
    );
  }
}

Main.contextTypes = Object.assign(Main.contextTypes, {
  onPageClose: PropTypes.func
})

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  rootTemplate: PropTypes.object,
  validRefValues:PropTypes.object
});

export default Main;
