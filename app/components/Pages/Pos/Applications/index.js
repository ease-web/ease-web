import React from 'react';
import * as _ from 'lodash';
import {MenuItem, SelectField} from 'material-ui';
import ApplicationsList from './ApplicationsList/ApplicationsList';
import {getAppListView, initAppSummary, openAppSumEmail, closeEmail, sendEmail} from '../../../../actions/application';
import {quotProduct, validateProductForQuotation} from '../../../../actions/quotation';
import {closeConfirm, closeError} from '../../../../actions/products';
import {getProductThumbnailUrl} from '../../../../utils/PlanUtils';
import {getProductId} from '../../../../../common/ProductUtils';
import EABComponent from '../../../Component';
import AppSumEmailDialog from '../../../Dialogs/AppSumEmailDialog';
import AppSumEmailPreviewDialog from '../../../Dialogs/AppSumEmailPreviewDialog';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import WarningDialog from '../../../Dialogs/WarningDialog';
import styles from '../../../Common.scss';

class index extends EABComponent {
    constructor(props) {
        super(props);
        this.inited = false;
        this.state = Object.assign({}, this.state,{
            applicationsList: [],
            selectedRecommendPerson: 1,
            productsList:[],
            clientChoiceFinished:false,
            pdaMembers:[],
            cid:'',
            quotCheckedList:[],
            appSumEmailOpen: false,
            attKeysMap: [],
            emailOpen: false,
            emails: null,
            isFullySigned: false,
            agentChannel: 'AGENCY',
            isFNACompleted: true,
            isFromCreateProposal: false,
            errorMsg: null,
            confirmMsg: null
        });
    }

    componentDidMount() {
        let {store} = this.context;
        if (!this.inited) {
            this.unsubscribe = this.context.store.subscribe(()=>{
                const {appList} = store.getState();
                if (!this.currentState && appList) {
                    this.currentState = appList;
                    this.setState(appList);
                } else if (!_.isEqual(this.currentState, appList)) {
                    this.state.initing = false;
                    this.currentState = appList;
                    if(appList.isCreateProposal != null){
                        this.state.isFromCreateProposal = appList.isCreateProposal;
                    }
                    if (appList.needUpdateAppList) {
                        this.state.initing = true;
                        getAppListView(this.context, client.profile);
                    } else {
                        this.setState(appList);
                    }
                }
            });
            const client = store.getState().client;
            this.setState({
                initing: true,
                cid:client.profile.cid
            });
            getAppListView(this.context, client.profile);
            this.inited = true;
        }
    }

    componentWillUnmount(){
        this.unsubscribe && this.unsubscribe();
        initAppSummary(this.context);
    }

    onTabChange() {
        this.updateAppbarMenus();
    }

    productImageClicked = (productId, proposerId, insuredId, ccy) => {
        validateProductForQuotation(this.context, productId, insuredId, proposerId,(resp)=>{
            if (resp.success === true){
                quotProduct(this.context, productId, insuredId, proposerId, ccy, false);
            }
        });
    }

    selectProduct = (productId) => {
        const {store} = this.context;
        let {client, products} = store.getState();

        let pCid = client.profile.cid;
        let iCid = products.insuredCid || pCid;
        let ccy = products.ccy;
        quotProduct(this.context, productId, iCid, pCid, ccy, false);
    };

    confirmSelectProduct = () => {
        let productId = this.context.store.getState().products.selectedProductId;
        this.selectProduct(productId);
    };

    filterByPda = (applicationsList) => {
        return applicationsList.filter((item)=>{
            let proposedCid = _.get(item, 'iCidMapping') || _.get(item, 'insureds') || {};
            let proposedCidKeys = Object.keys(proposedCid);
            if (proposedCidKeys.length > 0) {
                return proposedCidKeys.indexOf(this.getSelectedCid()) > -1;
            } else {
                return item.iCid === this.getSelectedCid();
            }
        });
    }

    classifyApplications = () => {
        let {applicationsList} = this.state;
        // let appListFilteredByPda = this.filterByPda(applicationsList);
        let proposedItems = [];
        let applyingItems = [];
        let submitedItems = [];
        let invalidatedSignedItems = [];
        let invalidatedItems = [];
        let proposedItemIds = [];
        let isFullySigned = false;
        let isStartSignature = false;

        if (applicationsList) {
            applicationsList.sort((a,b)=>{
                let aTime = a.lastUpdateDate;
                let bTime = b.lastUpdateDate;
                if (aTime<bTime)return 1;
                if (aTime>bTime)return -1;
                return 0;
            });
            applicationsList.forEach((item)=>{
                if (item.type === 'quotation'){
                    if (item.appStatus === 'INVALIDATED') {
                        invalidatedItems.push(item);
                    } else {
                        proposedItems.push(item);
                        proposedItemIds.push(item.id);
                    }
                } else if (item.type === 'application' || item.type === 'masterApplication'){
                    isFullySigned = isFullySigned || item.isFullySigned;
                    if (item.appStatus === 'APPLYING') {
                        isStartSignature = isStartSignature || item.isStartSignature;
                        applyingItems.push(item);
                    } else if (item.appStatus === 'SUBMITTED'){
                        isStartSignature = isStartSignature || item.isStartSignature;
                        submitedItems.push(item);
                    } else if (item.appStatus === 'INVALIDATED_SIGNED'){
                        invalidatedSignedItems.push(item);
                    } else if (item.appStatus === 'INVALIDATED'){
                        invalidatedItems.push(item);
                    }
                }
            });
        }

        let itemsTemplate = {
            allItems: applicationsList,
            proposedItems,
            applyingItems,
            submitedItems,
            invalidatedSignedItems,
            invalidatedItems,
            proposedItemIds,
            isStartSignature,
            isFullySigned
        };
        return itemsTemplate;
    }

    getProductsList = () => {
        let {applicationsList} = this.state;
        let filteredAppList = this.filterByPda(applicationsList);
        let {store} = this.context;
        // var covCodes = [];
        var productsList = [];
        let ccy = filteredAppList ? filteredAppList.ccy : '';
        _.forEach(filteredAppList, (application)=>{
            let {plans = [], baseProductCode, baseProductName, iCid, pCid} = application;
            const covName = baseProductName || _.get(plans, '[0].covName');
            if (_.findIndex(productsList, pro => { return pro.covCode === baseProductCode;}) === -1){
                productsList.push({
                    covCode: baseProductCode,
                    covName,
                    iCid,
                    pCid,
                    ccy
                });
            }
        });

        // Get Products image
        for (var i in productsList){
            let product = productsList[i];
            productsList[i].thumbnailSrc = getProductThumbnailUrl(store.getState().app, product);
        }
        return productsList;
    }

    updateAppbarMenus=()=>{
        const {selectedBundle} = this;
        const {client} = this.context.store.getState();
        let options = _.map(client.profile.bundle, (bundle) => {
          return {
            value: bundle.id,
            text: bundle.isValid ? 'CURRENT APPLICATIONS' : bundle.id
          };
        }).reverse();
        let dValue = _.get(_.find(_.get(client, 'profile.bundle'), bundle=>bundle.isValid), 'id');
        this.context.initAppbar({
        title: 'Applications',
        itemsIndex: 0,
        items:[
                [{
                    type: 'flatButton',
                    title: 'EMAIL',
                    disabled: false,
                    action: () => {
                        // handle email button clicked
                        openAppSumEmail(this.context);
                    }
                },
                {
                    type: 'menu',
                    menuValue: selectedBundle ? selectedBundle : dValue,
                    options: options,
                    onChange: (bundleId) => {
                        this.selectedBundle = bundleId;
                        this.updateAppbarMenus();
                        getAppListView(this.context, client.profile, bundleId);
                    }
                }]
            ]
        });
    }

    handleSelectedCidChange = (event, index, value) => {
        this.setState({selectedRecommendPerson:value});
    };

    productsListView = () => {
        let pdaMembers = this.state.pdaMembers;
        let {muiTheme, lang} = this.context;

        var recommendPersons = [];
        if (pdaMembers){
            pdaMembers.forEach((item, index) => {
                recommendPersons.push(<MenuItem value={index+1} primaryText={'Proposed for ' + item.fullName} />);
            });
        }

        let productsList = this.getProductsList();
        var productsListView = [];

        if (typeof productsList !== 'undefined' && productsList.length > 0) {
            for (var i in productsList){
                let product = productsList[i];
                let productId = getProductId(product);
                productsListView.push(
                    <div style= {{paddingLeft:'24px', width:180, position:'relative'}}>
                        <img src={product.thumbnailSrc} alt="Image Not Shown"
                            onClick = {()=>this.productImageClicked(productId, product.pCid, product.iCid, product.ccy)}
                            style={{
                                width:180,
                                height:74,
                                position:'relative',
                                boxShadow: '0 2px 2px #CDCDCD',
                                cursor: 'pointer'
                            }}/>
                        <div className={ styles.EllipsisText}
                            style={{
                                width:180,
                                fontSize:14,
                                padding: '10px 0',
                                lineHeight: '18px',
                                color: muiTheme.baseTheme.palette.labelColor
                            }}>{window.getLocalText(lang, product.covName)}</div>
                    </div>
                );
            }
        } else {
            productsListView.push(
                <div style={{textAlign: 'center', verticalAlign: 'middle', lineHeight: '90px', paddingLeft: '40px'}}> NO PROPOSED PRODUCT</div>
            );
        }

        return (
            <div className={ styles.Card } style={{
                    width:'100%',
                    margin: 0,
                    zIndex: 9
                }}>
                <SelectField key="selectedRecommendPerson"
                    style={{
                        lineHeight: '48px',
                        height: '48px',
                        margin: '9px 24px 0',
                        width: 'auto',
                        position: 'absolute'
                    }}
                    labelStyle={{
                        color: muiTheme.baseTheme.palette.primary1Color
                    }}
                    underlineStyle={{display:'none'}}
                    value={this.state.selectedRecommendPerson}
                    onChange={this.handleSelectedCidChange}>
                    {recommendPersons}
                </SelectField>
                <div style={{display:'flex', flexWrap: 'no-wrap', overflowX:'auto', paddingBottom:'10px', paddingTop:'59px', height: '114px'}}>
                    {productsListView}
                </div>
            </div>
        )
    }

    getSelectedCid = () => {
        var selectedCid = this.state.cid;
        let pdaMembers = this.state.pdaMembers;
        if (pdaMembers && pdaMembers.length != 0){
            selectedCid=pdaMembers[this.state.selectedRecommendPerson-1].cid
        }
        return selectedCid;
    }

    getContent = () => {
        var {clientChoiceFinished, isFNACompleted, isFromCreateProposal, quotCheckedList, applicationsList, agentChannel} = this.state;
        if (this.state.initing) return null;
        if (!applicationsList || applicationsList.length === 0) return this.initPage();
        let itemsTemplate = this.classifyApplications();
        let {isStartSignature} = itemsTemplate;

        let applicationsListTemplate = {
            itemsTemplate,
            'selectedCid': this.getSelectedCid(),
            clientChoiceFinished,
            clientChoiceDisabled: isStartSignature,
            quotCheckedList,
            agentChannel,
            isFNACompleted,
            isFromCreateProposal: isFromCreateProposal
        }
        return (
            <div>
                {this.productsListView()}
                <ApplicationsList template={applicationsListTemplate}/>
            </div>
        );
    }

    initPage = () => {
        return (
            <div style={{ height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                <div>No Proposed Records</div>
            </div>
        );
    }

    getAppSumEmailDialog = () => {
        let {appSumEmailOpen, applicationsList, attKeysMap} = this.state;
        // Carlton TODO: handle shield product for AppSumEmailDialog
        return (
            <AppSumEmailDialog open={appSumEmailOpen} appList={applicationsList} attKeysMap={attKeysMap}/>
        );
    };

    onSend(emails){
        sendEmail(this.context, emails);
    }

    onClose(){
        closeEmail(this.context);
    }

    getEmailDialog() {
        const {emailOpen, emails} = this.state;
        return (
            <AppSumEmailPreviewDialog open={emailOpen} emails={emails} onSend={(emails) => this.onSend(emails)} onClose={() => this.onClose()}/>
        );
    }

    render() {
        const {confirmMsg, errorMsg, bundleId} = this.state;
        if (bundleId !== this.selectedBundle) {
            this.selectedBundle = bundleId;
            this.updateAppbarMenus();
        }
        return (
            <div style={{ width: '100%' }}>
                {this.getContent()}
                {this.getAppSumEmailDialog()}
                {this.getEmailDialog()}
                <ConfirmDialog
                    open={!!confirmMsg}
                    confirmMsg={confirmMsg}
                    onClose={() => closeConfirm(this.context)}
                    onConfirm={() => this.confirmSelectProduct()}
                />
                <WarningDialog
                    open={!!errorMsg}
                    msg={errorMsg}
                    onClose={() => closeError(this.context)}
                />
            </div>
        );
    }
}

export default index;
