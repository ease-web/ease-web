import React    from 'react';
import * as _   from 'lodash';

import Modal from '../../../Modal';
import EABComponent from '../../../Component';
import QuotationPage from '../Quotation/index';
import ProposalPage from '../Proposal/index';
import SupportDocPage from '../Applications/UploadDocument/index';
import AppForm from '../Applications/AppForms';
import ClientChoice from '../Applications/ClientChoice';
import FinancialEvaluation from '../Needs/FinancialEvaluation/index';

import * as appActions from '../../../../actions/application';
import * as shieldAppActions from '../../../../actions/shieldApplication';
import {POS_CLIENT_CHOICE} from '../../../../actions/clientChoice';

import styles from '../../../Common.scss';

class Main extends EABComponent {

  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        currentPage: 'index'
      });
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
      let {
        store
      } = this.context;

      let {
        pos
      } = store.getState();
      if (pos.currentPage && this.state.currentPage !== pos.currentPage) {
        this.setState({currentPage: pos.currentPage});
      }
  }

  render() {
    let {
     currentPage
    } = this.state;

    if (!currentPage || currentPage === 'index') {
      return <div/>;
    }

    let pageCont = null;
    if (currentPage !== 'index') {
      if (currentPage === 'quotation') {
        pageCont = <QuotationPage />;
      } else if (currentPage === shieldAppActions.POS_SHIELD_EAPP) {
        pageCont = <AppForm isShield={true}/>;
      } else if (currentPage === 'proposal') {
        pageCont = <ProposalPage />;
      } else if (currentPage === appActions.POS_EAPP) {
        pageCont = <AppForm />;
      } else if (currentPage === POS_CLIENT_CHOICE){
        pageCont = <ClientChoice />;
      } else if (currentPage === 'POS_SUPPORT_DOCUMENTS') {
        pageCont = <SupportDocPage isShield={this.context.store.getState().pos.isShield || false}/>;
      } else if (currentPage === 'financial_evaluation') {
        pageCont = <FinancialEvaluation />;
      }
    }
    return (
      <Modal>
        <div className={styles.secIndex}>
          {pageCont}
        </div>
      </Modal>
    );
  }
}


export default Main;
