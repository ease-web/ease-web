import * as _ from 'lodash';

const getCoexistRules = (quotation, bpDetail) => {
  if (bpDetail.coexist) {
    return _.filter(bpDetail.coexist, (coex) => {
      return (
        (!coex.country || coex.country === '*' || coex.country === quotation.iResidence) &&
        (!coex.dealerGroup || coex.dealerGroup === '*' || coex.dealerGroup === quotation.agent.dealerGroup) &&
        (!coex.gender || coex.gender === '*' || coex.gender === quotation.iGender) &&
        (!coex.smoke || coex.smoke === '*' || coex.smoke === quotation.iSmoke) &&
        (!coex.ageFr || coex.ageFr <= quotation.iAge) &&
        (!coex.ageTo || coex.ageTo >= quotation.iAge)
      );
    });
  }
  return [];
};

const getInclusiveRiders = (covCode, quotation, bpDetail) => {
  let inclusiveRiders = [];
  _.each(getCoexistRules(quotation, bpDetail), (coex) => {
    if (coex.shouldCoExist === 'Y') {
      if (coex.groupA.indexOf(covCode) >= 0) {
        inclusiveRiders = inclusiveRiders.concat(coex.groupB);
      }
      if (coex.groupB.indexOf(covCode) >= 0) {
        inclusiveRiders = inclusiveRiders.concat(coex.groupA);
      }
    }
  });
  return _.filter(inclusiveRiders, c => c !== covCode);
};

const getExclusiveRiders = (covCode, quotation, bpDetail) => {
  let exclusiveRiders = [];
  _.each(getCoexistRules(quotation, bpDetail), (coex) => {
    if (coex.shouldCoExist === 'N') {
      if (coex.groupA.indexOf(covCode) >= 0) {
        exclusiveRiders = exclusiveRiders.concat(coex.groupB);
      }
      if (coex.groupB.indexOf(covCode) >= 0) {
        exclusiveRiders = exclusiveRiders.concat(coex.groupA);
      }
    }
  });
  return _.filter(exclusiveRiders, c => c !== covCode);
};

const getRequiredRiders = (covCode, quotation, bpDetail) => {
  let requiredRiders = [];
  _.each(getCoexistRules(quotation, bpDetail), (coex) => {
    if (coex.shouldCoExist === 'REQUIRE') {
      if (coex.groupB.indexOf(covCode) >= 0) {
        requiredRiders = requiredRiders.concat(coex.groupA);
      }
    }
  });
  return requiredRiders;
};

const getDisabledRiderList = (selectedCovCodes, quotation, bpDetail, riderList) => {
  const disabledRiders = [];
  _.each(riderList, (rider) => {
    const exclusiveRiders = getExclusiveRiders(rider.covCode, quotation, bpDetail);
    const selectedExclusiveRiders = _.find(selectedCovCodes, covCode => exclusiveRiders.indexOf(covCode) > -1);

    const requiredRiders = getRequiredRiders(rider.covCode, quotation, bpDetail);
    const hasRequiredRiders = _.isEmpty(requiredRiders) || _.find(selectedCovCodes, covCode => requiredRiders.indexOf(covCode) > -1);

    if (selectedExclusiveRiders || !hasRequiredRiders) {
      disabledRiders.push(rider.covCode);
    }
  });
  return disabledRiders;
};

const removeInclusiveRiders = (covCode, selectedCovCodes, quotation, bpDetail, riderList) => {
  const inclusiveRiders = getInclusiveRiders(covCode, quotation, bpDetail);
  return _.filter(selectedCovCodes, c => inclusiveRiders.indexOf(c) === -1);
};

const removeDisabledRiders = (selectedCovCodes, quotation, bpDetail, riderList) => {
  const disabledRiders = getDisabledRiderList(selectedCovCodes, quotation, bpDetail, riderList);
  return _.filter(selectedCovCodes, c => disabledRiders.indexOf(c) === -1);
};

module.exports = {
  getInclusiveRiders,
  getDisabledRiderList,
  removeInclusiveRiders,
  removeDisabledRiders
};
