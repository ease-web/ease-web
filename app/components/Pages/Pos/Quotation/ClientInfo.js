import React, { PropTypes} from 'react';
import Avatar from 'material-ui/Avatar';
import * as _ from 'lodash';

import {getAvatarInitial} from '../../../../utils/client';

import EABComponent from '../../../Component';
import styles from '../../../Common.scss';

export default class ClientInfo extends EABComponent {

  static propTypes = {
    role: PropTypes.string,
    profile: PropTypes.object,
    singleLine: PropTypes.bool,
    style: PropTypes.object
  };

  static defaultProps = {
    singleLine: false
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      useInitial: false
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.profile.cid !== nextProps.profile.cid ||
      this.props.profile.age !== nextProps.profile.age ||
      this.state.useInitial !== nextState.useInitial;
  }

  _getRole(role) {
    return window.getLocalizedText(this.context.langMap, 'quotation.role_' + role);
  }

  _getAvatar(profile) {
    const {store} = this.context;
    const {useInitial} = this.state;
    return (
      <Avatar
        src={useInitial ? null : window.genFilePath(store.getState().app, profile.cid, 'photo')}
        size={56}
        backgroundColor='RGBA(0, 0, 0, 0.1)'
        onError={() => {
          this.setState({
            useInitial: true
          });
        }}
      >
        {useInitial ? getAvatarInitial(profile) : null}
      </Avatar>
    );
  }

  _getGender(gender) {
    return window.getLocalizedText(this.context.langMap, gender === 'M' ? 'profile.gender.male' : 'profile.gender.female');
  }

  _getAge(age) {
    return age + ' ' + window.getLocalizedText(this.context.langMap, 'profile.years_old');
  }

  _getSmoke(smoke) {
    return window.getLocalizedText(this.context.langMap, smoke === 'Y' ? 'profile.smoking_habit.smoker' : 'profile.smoking_habit.non_smoker');
  }

  _getOccupation(occupation) {
    let occ = _.find(this.context.optionsMap.occupation.options, (option) => option.value === occupation);
    return occ && window.getLocalText(this.context.lang, occ.title);
  }

  _getResidence(residence) {
    let cty = _.find(this.context.optionsMap.residency.options, (option) => option.value === residence);
    return cty && window.getLocalText(this.context.lang, cty.title);
  }

  render() {
    const {role, profile, singleLine, style} = this.props;
    const {fullName, gender, age, smoke, occupation, residence} = profile || {};
    const infoLines = [];
    if (singleLine) {
      const infos = [];
      infos.push(this._getGender(gender));
      infos.push(this._getAge(age));
      infos.push(this._getSmoke(smoke));
      infos.push(this._getOccupation(occupation));
      infos.push(this._getResidence(residence));
      infoLines.push(
        <div key="info">{_.join(_.filter(infos, i => !!i), ', ')}</div>
      );
    } else {
      const infos1 = [];
      infos1.push(this._getGender(gender));
      infos1.push(this._getAge(age));
      infos1.push(this._getSmoke(smoke));
      infoLines.push(
        <div key="info1">{_.join(_.filter(infos1, i => !!i), ', ')},</div>
      );
      const infos2 = [];
      infos2.push(this._getOccupation(occupation));
      infos2.push(this._getResidence(residence));
      infoLines.push(
        <div key="info2">{_.join(_.filter(infos2, i => !!i), ', ')}</div>
      );
    }
    return (
      <div className={styles.ClientInfoContainer} style={style}>
        <div className={styles.RoleTitle}>{this._getRole(role)}</div>
        <div className={styles.PhotoText}>
          {this._getAvatar(profile)}
          <div className={styles.ClientInfo}>
            <div className={styles.FullName}>{fullName}</div>
            <div className={styles.Others}>{infoLines}</div>
          </div>
        </div>
      </div>
    );
  }

}
