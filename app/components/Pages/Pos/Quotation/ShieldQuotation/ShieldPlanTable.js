import React, {Component, PropTypes} from 'react';
import {Paper, SelectField, MenuItem, IconButton} from 'material-ui';
import * as _ from 'lodash';

import {getIcon} from '../../../../Icons/index';
import EABComponent from '../../../../Component';
import NumericField from '../../../../CustomViews/NumericField';
import styles from '../../../../Common.scss';

export default class PlanTable extends Component {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func,
    handleRemovePlan: PropTypes.func
  });

  _genOptions(list) {
    const {lang} = this.context;
    return _.map(list, (item) => (
      <MenuItem
        key={item.value}
        value={item.value}
        primaryText={window.getLocalText(lang, item.title)}
      />
    ));
  }

  _getPlanComponent = (plan) => {
    const {lang, optionsMap, handleRemovePlan, handleConfigChange} = this.context;
    const {planDetails, quotation, inputConfigs} = this.props;
    const planDetail = planDetails[plan.covCode];
    const inputConfig = inputConfigs[plan.covCode];
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    let removeBtn = (
      <IconButton onTouchTap={() => handleRemovePlan(quotation.iCid, plan)}>
        {getIcon('removeCircle', 'red')}
      </IconButton>
    );
    return (
      <tr key={plan.covCode} className={styles.PlanInfoRow}>
        <td>{plan.covCode === quotation.baseProductCode ? null : removeBtn}</td>
        <td className={styles.PlanName}>{window.getLocalText(lang, plan.covName)}</td>
        <td>
          <SelectField
            fullWidth
            disabled={!inputConfig || !inputConfig.canEditPaymentMethod}
            value={plan.paymentMethod}
            onChange={(e, index, value) => {
              plan.paymentMethod = value;
              handleConfigChange();
            }}
            underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
            style={{ verticalAlign: 'bottom' }}
            labelStyle={{ color: 'black' }}
          >
            {this._genOptions(inputConfig && inputConfig.paymentMethodList)}
          </SelectField>
        </td>
        <td>
          <SelectField
            fullWidth
            disabled={!inputConfig || !inputConfig.canEditPayFreq}
            value={plan.payFreq}
            onChange={(e, index, value) => {
              plan.payFreq = value;
              handleConfigChange();
            }}
            underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
            style={{ verticalAlign: 'bottom' }}
            labelStyle={{ color: 'black' }}
          >
            {this._genOptions(inputConfig && inputConfig.payFreqList)}
          </SelectField>
        </td>
        <td>
          <NumericField
            id={quotation.iCid + '-premium'}
            fullWidth
            disabled={!inputConfig || !inputConfig.canEditPremium}
            sign={sign}
            maxIntDigit={10}
            decimalPlace={2}
            value={plan.premium}
          />
          {!_.isEmpty(planDetail.premPostfix) ? (
            <div style={{ textAlign: 'right', height: '20px' }}>
              {window.getLocalText(lang, planDetail.premPostfix)}
            </div>
          ) : null}
        </td>
      </tr>
    );
  };

  render() {
    const {langMap, optionsMap} = this.context;
    const {style, quotation} = this.props;
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);

    let bpEl;
    let riderEls = [];
    _.each(quotation.plans, (plan) => {
      let el = this._getPlanComponent(plan);
      if (plan.covCode === quotation.baseProductCode) {
        bpEl = el;
      } else {
        riderEls.push(el);
      }
    });

    let bpGroupHeader = (
      <tr className={styles.GroupHeader}>
        <td colSpan="5">
          {window.getLocalizedText(langMap, 'quotation.plans.basicPlan')}
        </td>
      </tr>
    );

    let riderGroupHeader = null, riderGroupFooter;
    if (riderEls.length > 0) {
      riderGroupHeader = (
        <tr className={styles.GroupHeader}>
          <td colSpan="5">
            {window.getLocalizedText(langMap, 'quotation.plans.riders')}</td>
        </tr>
      );
      riderGroupFooter = (
        <tr className={styles.GroupFooter}>
          <td colSpan="4" style={{ textAlign:'right' }}>
            {window.getLocalizedText(langMap, 'quotation.plans.subtotal')}
          </td>
          <td style={{ textAlign: 'right' }}>
            {window.getCurrency(quotation.totRiderPrem, sign, 2)}
          </td>
        </tr>
      );
    }

    return (
      <Paper style={style}>
        <table className={styles.QuotTable + ' ' + styles.ShieldPlanTable}>
          <colgroup>
            <col className={styles.ColRowBtn}/>
            <col className={styles.ColPlanName}/>
            <col className={styles.ColInput}/>
            <col className={styles.ColInput}/>
            <col className={styles.ColInput}/>
          </colgroup>
          <thead>
            <tr>
              <th colSpan="2">
                {window.getLocalizedText(langMap, 'quotation.plans.covName')}
              </th>
              <th>
                {window.getLocalizedText(langMap, 'quotation.plans.paymentMethod')}
              </th>
              <th>
                {window.getLocalizedText(langMap, 'quotation.plans.paymentFrequency')}
              </th>
              <th className={styles.ColRight}>
                {window.getLocalizedText(langMap, 'quotation.plans.premium')}
              </th>
            </tr>
          </thead>
          <tbody>
            {bpGroupHeader}
            {bpEl}
            {riderGroupHeader}
            {riderEls}
            {riderGroupFooter}
          </tbody>
        </table>
      </Paper>
    );
  }

}

