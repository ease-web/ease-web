import React, {PropTypes} from 'react';
import * as _ from 'lodash';
import {Paper, DatePicker, SelectField, MenuItem, Checkbox, Dialog, FlatButton} from 'material-ui';

import EABComponent from '../../../Component';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';

import ConfigConstants from '../../../../constants/ConfigConstants';
import DateUtils from '../../../../../common/DateUtils';
import ProductUtils from '../../../../../common/ProductUtils';

export default class QuotationConfigs extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func,
    handleResetQuot: PropTypes.func,
    confirmAction: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      errorMsg: null
    });
  }

  _getCcyOptions() {
    const {quotation, planDetails} = this.props;
    const {optionsMap, lang, store} = this.context;
    const {agentProfile} = store.getState().app;
    let basicPlan = planDetails[quotation.baseProductCode];
    let options = [];
    if (basicPlan && basicPlan.currencies) {
      let ccyOptions = _.find(optionsMap.ccy.currencies, c => c.compCode === agentProfile.compCode);
      let currencies = _.find(basicPlan.currencies, c => c.country === '*' || c.country === quotation.iResidence);
      if (currencies && currencies.ccy) {
        _.each(ccyOptions.options, (ccyOption) => {
          if (currencies.ccy.indexOf(ccyOption.value) > -1) {
            options.push(
              <MenuItem
                key={ccyOption.value}
                value={ccyOption.value}
                primaryText={window.getLocalText(lang, ccyOption.title)}
              />
            );
          }
        });
      }
    }
    return options;
  }

  _getPaymentModeOptions() {
    const {quotation, inputConfigs} = this.props;
    const inputConfig = inputConfigs[quotation.baseProductCode];
    let options = [];
    if (inputConfig) {
      _.each(inputConfig.payModes, (payMode) => {
        options.push(
          <MenuItem
            key={payMode.mode}
            value={payMode.mode}
            primaryText={window.getLocalizedText(this.context.langMap, 'quotation.payMode_' + payMode.mode)}
          />
        );
      });
    }
    return options;
  }

  _onChangePayMode(value) {
    const {langMap, handleResetQuot, handleConfigChange, confirmAction} = this.context;
    const {quotation, inputConfigs} = this.props;
    const inputConfig = inputConfigs[quotation.baseProductCode] || {};
    const payMode = _.find(inputConfig.payModes, pm => pm.mode === quotation.paymentMode);
    if (payMode && payMode.onChange) {
      if (!payMode.onChange.to || payMode.onChange.to === value || payMode.onChange.to.indexOf(value) > -1) {
        if (payMode.onChange.action === 'resetQuot') {
          confirmAction(window.getLocalizedText(langMap, 'quotation.warning.resetQuot'), () => {
            const params = payMode.onChange.params || {};
            const {keepPolicyOptions, keepPlans, keepFunds} = params;
            quotation.paymentMode = value;
            handleResetQuot(true, keepPolicyOptions, keepPlans, keepFunds);
          });
          return;
        }
      }
    }
    quotation.paymentMode = value;
    handleConfigChange();
  }

  validateRiskCommDate(value) {
    const {langMap} = this.context;
    const {planDetails, quotation} = this.props;
    var effDate = planDetails[quotation.baseProductCode].effDate;
    let riskCommenDate = value;
    let today =  new Date();
    var errorMsg = '';
    if (riskCommenDate - today > 0) {
      errorMsg = window.getLocalizedText(langMap, 'quotation.error.backDatebeyondToday');
    } else {
      let monthDiff = 12 * (today.getFullYear() - riskCommenDate.getFullYear() ) + (today.getMonth() - riskCommenDate.getMonth() );
      if (today.getDate() - riskCommenDate.getDate() > 0) {
        monthDiff = monthDiff + 1;
      }
      if (monthDiff > 6) {
        errorMsg = window.getLocalizedText(langMap, 'quotation.error.backDateOlderThanSixMonths');
      } else if (effDate && riskCommenDate - new Date(effDate) < 0) {
        errorMsg = window.getLocalizedText(langMap, 'quotation.error.backDateBeforeEffDate').replace('[@@effDate]', new Date(effDate).format('dd-mm-yyyy'));
      } else {
        errorMsg = this.validateEntryAge(value);
      }
    }
    return errorMsg;
  }

  validateEntryAge(targetDate) {
    const {langMap} = this.context;
    const {quotation, planDetails} = this.props;
    let bpDetail = planDetails[quotation.baseProductCode];
    let entryAge = _.find(bpDetail.entryAge, ea => ea.country === '*' || quotation.iResidence);
    if (entryAge) {
      if (entryAge.ominAge) {
        let dob = new Date(quotation.pDob);
        let age = ProductUtils.getAgeByUnit(entryAge.ominAgeUnit, targetDate, dob);
        if (age < entryAge.ominAge) {
          return window.getLocalizedText(langMap, 'quotation.error.backdate.ownerMinAge');
        }
      }
      if (entryAge.iminAge) {
        let dob = new Date(quotation.iDob);
        let age = ProductUtils.getAgeByUnit(entryAge.iminAgeUnit, targetDate, dob);
        if (age < entryAge.iminAge) {
          return window.getLocalizedText(langMap, 'quotation.error.backdate.insuredMinAge');
        }
      }
    }
  }

  getConfigFields() {
    const {handleConfigChange, langMap} = this.context;
    const {quotation, planDetails} = this.props;
    let bpDetail = planDetails[quotation.baseProductCode];
    let configFields = [];
    configFields.push(
      <SelectField
        id="ccy"
        key="ccy"
        fullWidth
        value={quotation.ccy}
        onChange={(e, index, value) => {
          quotation.ccy = value;
          handleConfigChange();
        }}
        floatingLabelText={window.getLocalizedText(langMap, 'quotation.currency')}
      >
        {this._getCcyOptions()}
      </SelectField>
    );
    configFields.push(
      <SelectField
        id="payMode"
        key="payMode"
        fullWidth
        value={quotation.paymentMode}
        onChange={(e, index, value) => this._onChangePayMode(value)}
        floatingLabelFixed
        hintText={window.getLocalizedText(langMap, 'quotation.select')}
        floatingLabelText={window.getLocalizedText(langMap, 'quotation.payment_mode')}
      >
        {this._getPaymentModeOptions()}
      </SelectField>
    );
    configFields.push(
      <Checkbox
        key="backdate"
        label={window.getLocalizedText(langMap, 'quotation.backdate')}
        disabled={bpDetail.allowBackdate !== 'Y'}
        checked={quotation.isBackDate === 'Y'}
        onCheck={(event, value) => {
          quotation.isBackDate = value ? 'Y' : 'N';
          handleConfigChange();
        }}
        iconStyle={{ fill: 'black' }}
      />
    );
    if (quotation.isBackDate === 'Y') {
      configFields.push(
        <DatePicker
          id="riskCommenDate"
          key="riskCommenDate"
          fullWidth
          value={quotation.riskCommenDate ? DateUtils.parseDate(quotation.riskCommenDate) : new Date()}
          onChange={(e, value) => {
            var errorMsg = this.validateRiskCommDate(value);
            if (errorMsg) {
              this.setState({ errorMsg: errorMsg });
            } else {
              quotation.riskCommenDate = DateUtils.formatDate(value);
              handleConfigChange();
            }
          }}
          formatDate={(date) => _.isDate(date) ? date.format(ConfigConstants.DateFormat) : ''}
          floatingLabelText={window.getLocalizedText(langMap, 'quotation.riskCommenDate')}
          style={{ overflow: 'hidden' }}
        />
      );
    }
    return _.map(configFields, (field, index) => (
      <div key={index} style={{ width: '25%' }}>
        <div style={{ margin: '0 24px' }}>
          {field}
        </div>
      </div>
    ));
  }

  render() {
    const {langMap, muiTheme} = this.context;
    const {style} = this.props;
    const {confirmMsg, onConfirm} = this.state;
    return (
      <Paper
        style={{
          boxShadow: muiTheme.palette.shadowColor + ' 1px 1px 4px',
          padding: '12px 0',
          backgroundColor: '#FAFAFA',
          display: 'flex',
          alignItems: 'center',
          ...style
        }}
      >
        {this.getConfigFields()}
        <Dialog
          modal
          open={!!this.state.errorMsg}
          onRequestClose={() => this.setState({ errorMsg: null })}
          actions={
            <FlatButton
              label={window.getLocalizedText(langMap, 'BUTTON.OK', 'OK')}
              onTouchTap={() => this.setState({ errorMsg: null })}
            />
          }
        >
          {this.state.errorMsg}
        </Dialog>
        {confirmMsg ? (
          <ConfirmDialog
            open={!!confirmMsg}
            confirmMsg={confirmMsg}
            onClose={() => this.setState({ confirmMsg: null, onConfirm: null })}
            onConfirm={() => onConfirm && onConfirm()}
          />
        ) : null}
      </Paper>
    );
  }

}
