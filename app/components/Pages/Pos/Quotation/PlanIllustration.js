import React, {Component, PropTypes} from 'react';
import {Paper, IconButton} from 'material-ui';
import * as _ from 'lodash';

import {getIcon} from '../../../Icons/index';
import EABComponent from '../../../Component';

import styles from '../../../Common.scss';

class PlanIllustration extends Component {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleRemovePlan: PropTypes.func
  });

  _getCurrencyLabel(value, decimals, sign) {
    if (_.isNumber(value)) {
      return window.getCurrency(value, sign, decimals);
    }
    return '-';
  }

  _getInputFormat(inputs, ccy) {
    for (var i in inputs) {
      if (inputs[i].ccy === ccy) {
        return inputs[i];
      }
    }
    return (inputs && inputs[0]) || {};
  }

  _isCompulsory(covCode) {
    const {quotation, planDetails} = this.props;
    let isCompulsory;
    if (covCode === quotation.baseProductCode) {
      isCompulsory = true;
    } else {
      let basicPlan = planDetails[quotation.baseProductCode];
      let rider = _.find(basicPlan.riderList, r => r.covCode === covCode);
      if (rider && rider.condition && rider.condition[0]) {
        return rider.condition[0].compulsory === 'Y';
      }
      return false;
    }
    return isCompulsory;
  }

  _getPlanIllustrationComponent = (plan, config, requireGST) => {
    const {lang, optionsMap, handleRemovePlan} = this.context;
    const {inputConfigs, quotation} = this.props;
    const inputConfig = inputConfigs[config.covCode];
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);

    let removeBtn = (
      <IconButton onTouchTap={() => handleRemovePlan(plan)}>
        {getIcon('removeCircle', 'red')}
      </IconButton>
    );

    const pDec = this._getInputFormat(plan.premInput).decimal || 2;
    const saDec = this._getInputFormat(plan.saInput).decimal || 0;

    let sumInsured = !inputConfig || !inputConfig.canViewSumAssured ? '-' : this._getCurrencyLabel(config.sumInsured, saDec, sign);

    if (requireGST) {
      return (
        <tr key={plan.covCode} className={styles.PlanInfoRow}>
          <td>{this._isCompulsory(plan.covCode) ? null : removeBtn}</td>
          <td>{window.getLocalText(lang, plan.covName)}</td>
          <td className={styles.PlanInfoccyColStyle}>{sumInsured}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel((config.monthPrem || 0) + ((config.tax && config.tax.monthTax) || 0), pDec)} </td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel((config.quarterPrem || 0) + ((config.tax && config.tax.quarterTax) || 0), pDec)}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel((config.halfYearPrem || 0) + ((config.tax && config.tax.halfYearTax) || 0), pDec)}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel((config.yearPrem || 0) + ((config.tax && config.tax.yearTax) || 0), pDec)}</td>
        </tr>
      );
    } else {
      return (
        <tr key={plan.covCode} className={styles.PlanInfoRow}>
          <td>{this._isCompulsory(plan.covCode) ? null : removeBtn}</td>
          <td>{window.getLocalText(lang, plan.covName)}</td>
          <td className={styles.PlanInfoccyColStyle}>{sumInsured}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel(config.monthPrem, pDec, sign)}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel(config.quarterPrem, pDec, sign)}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel(config.halfYearPrem, pDec, sign)}</td>
          <td className={styles.PlanInfoccyColStyle}>{this._getCurrencyLabel(config.yearPrem, pDec, sign)}</td>
        </tr>
      );
    }
  };

  render() {
    const {langMap} = this.context;
    const {style, quotation, planDetails} = this.props;
    const bpDetails = planDetails[quotation.baseProductCode];
    const requireGST = (bpDetails && bpDetails.gstInd && bpDetails.gstInd === 'Y');

    let bpEl;
    let riderEls = [];
    _.each(quotation.plans, (plan) => {
      let el = this._getPlanIllustrationComponent(planDetails[plan.covCode], plan, requireGST);
      if (plan.covCode === quotation.baseProductCode) {
        bpEl = el;
      } else {
        riderEls.push(el);
      }
    });

    let bpGroupHeader = (
      <tr className={styles.GroupHeader}>
        <td colSpan="7">{window.getLocalizedText(langMap, 'quotation.plans.basicPlan')}</td>
      </tr>
    );
    let riderGroupHeader = null;
    if (riderEls.length > 0) {
      riderGroupHeader = (
        <tr className={styles.GroupHeader}>
          <td colSpan="7">{window.getLocalizedText(langMap, 'quotation.plans.riders')}</td>
        </tr>
      );
    }

    return (
      <Paper style={style}>
        <table className={styles.QuotTable + ' ' + styles.QuotPlanIllTable}>
          <colgroup>
            <col className={styles.ColRowBtn} />
            <col className={styles.ColPlanName}/>
            <col className={styles.ColSA}/>
            <col className={styles.ColPrem}/>
            <col className={styles.ColPrem}/>
            <col className={styles.ColPrem}/>
            <col className={styles.ColPrem}/>
          </colgroup>
          <thead>

            <tr>
              <th colSpan="2">{window.getLocalizedText(langMap, 'quotation.plans.covName')}</th>
              <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.sumInsured')}</th>
              <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.monthPrem')}</th>
              <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.quarterPrem')}</th>
              <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.halfYearPrem')}</th>
              <th className={styles.ColRight}>{window.getLocalizedText(langMap, 'quotation.plans.yearPrem')}</th>
            </tr>
          </thead>
          <tbody>
            {bpGroupHeader}
            {bpEl}
            {riderGroupHeader}
            {riderEls}
          </tbody>
        </table>
      </Paper>
    );
  }

}

export default PlanIllustration;
