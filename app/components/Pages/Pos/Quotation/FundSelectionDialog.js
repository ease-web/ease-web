import React, { PropTypes} from 'react';
import { IconButton, RaisedButton, SelectField, MenuItem } from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import NumericField from '../../../CustomViews/NumericField';
import {getIcon} from '../../../Icons/index';
import AvailableFundDialog from './AvailableFundDialog';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import TableDialog from '../../../Dialogs/TableDialog';
import SaveDialog from '../../../Dialogs/SaveDialog';

import styles from '../../../Common.scss';

import { queryFunds, allocFunds } from '../../../../actions/quotation';

export default class FundSelectionDialog extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    availableFunds: PropTypes.array,
    hasTopUpAlloc: PropTypes.bool, //has user select top-up option
    showTopUpAlloc: PropTypes.bool //is product has top up option or not
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      openAvailableList: false,
      confirmMsg: null,
      onConfirm: null,
      openDisclaimer: null,
      onCloseDisclaimer: null,
      riskProfile: null,
      invOpt: null,
      portfolio: null,
      fundAllocs: {},
      topUpAllocs: {}
    });
  }

  openDialog = () => {
    const {quotation, hasTopUpAlloc} = this.props;
    let {invOpt = 'mixedAssets', portfolio, funds} = quotation.fund || {};
    let fundAllocs = {};
    let topUpAllocs = {};
    _.each(funds, (fund) => {
      fundAllocs[fund.fundCode] = fund.alloc;
      if (hasTopUpAlloc){
        topUpAllocs[fund.fundCode] = fund.topUpAlloc;
      }
    });
    queryFunds(this.context, invOpt, () => {
      this.setState({
        open: true,
        riskProfile: quotation.extraFlags.fna && quotation.extraFlags.fna.riskProfile,
        invOpt: invOpt,
        portfolio: portfolio,
        fundAllocs: fundAllocs,
        topUpAllocs
      });
    });
  };

  closeDialog = () => {
    this.setState({ open: false });
  };

  saveFunds() {
    const {quotation, hasTopUpAlloc} = this.props;
    const {invOpt, portfolio, fundAllocs, topUpAllocs} = this.state;
    allocFunds(this.context, quotation, invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs, () => {
      this.setState({ open: false });
    });
  }

  _getTotalFundAlloc() {
    return _.reduce(this.state.fundAllocs, (sum, alloc) => sum + (alloc || 0), 0);
  }

  _getTotalTopUpAlloc() {
    return _.reduce(this.state.topUpAllocs, (sum, alloc) => sum + (alloc || 0), 0);
  }

  _hasValidPortfolio() {
    const {hasTopUpAlloc} = this.props;
    const {invOpt, fundAllocs, topUpAllocs} = this.state;
    if (invOpt === 'buildPortfolio') {
      let model = this._getCurrentPortfolioModel();
      if (model) {
        let selectedFunds = this._getSelectedFunds();
        return !_.find(model.groups, (group) => {
          let funds = _.filter(selectedFunds, f => group.riskRatings.indexOf(f.riskRating) > -1);
          let groupTotal = _.reduce(funds, (sum, fund) => sum + (fundAllocs[fund.fundCode] || 0), 0);
          let groupTopUpTotal = _.reduce(funds, (sum, fund) => sum + (topUpAllocs[fund.fundCode] || 0), 0);
          return groupTotal !== group.percentage || (hasTopUpAlloc && groupTopUpTotal !== group.percentage);
        });
      }
      return false;
    }
    return true;
  }

  _changeInvOpt(invOpt) {
    if (invOpt === 'buildPortfolio') {
      this.setState({
        openDisclaimer: true,
        onCloseDisclaimer: () => this._confirmChangeInvOpt(invOpt)
      });
    } else {
      this._confirmChangeInvOpt(invOpt);
    }
  }

  _confirmChangeInvOpt(invOpt) {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            fundAllocs: {},
            topUpAllocs: {},
            invOpt: invOpt
          }, () => {
            queryFunds(this.context, invOpt);
          });
        }
      });
    } else {
      this.setState({ invOpt }, () => queryFunds(this.context, invOpt));
    }
  }

  _changePortfolio(portfolio) {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmCleanList'),
        onConfirm: () => {
          this.setState({
            fundAllocs: {},
            topUpAllocs: {},
            portfolio: portfolio
          });
        }
      });
    } else {
      this.setState({
        fundAllocs: {},
        topUpAllocs: {},
        portfolio: portfolio
      });
    }
  }

  _confirmDeleteAllTopUp(){
    const {langMap} = this.context;
    const {topUpAllocs, fundAllocs} = this.state;
    if (!_.isEmpty(topUpAllocs)) {
      let newTopUpAlloc = {};
      for(var fundCode in fundAllocs){
        newTopUpAlloc[fundCode] = 0;
      }
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDeleteAllTopUp'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            topUpAllocs: newTopUpAlloc
          });
        }
      });
    }
  }

  _confirmDeleteAll() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(fundAllocs)) {
      let newFundAllocs = {};
      if(showTopUpAlloc && hasTopUpAlloc){
        for(var fundCode in topUpAllocs){
          newFundAllocs[fundCode] = 0;
        }
      }
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, showTopUpAlloc && hasTopUpAlloc? 'quotation.fund.list.confirmDeleteAllRp': 'quotation.fund.list.confirmDeleteAll'),
        onConfirm: () => {
          this.setState({
            confirmMsg: null,
            fundAllocs: newFundAllocs
          });
        }
      });
    }
  }

  _confirmDeleteGroup(funds) {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    if (!_.isEmpty(funds) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      this.setState({
        confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDeleteGroup'),
        onConfirm: () => {
          let updatedFunds = {}, updatedTopUps = {};
          _.each(fundAllocs, (alloc, fundCode) => {
            if (!_.find(funds, f => f.fundCode === fundCode)) {
              updatedFunds[fundCode] = alloc;
            }
          });
          _.each(topUpAllocs, (alloc, fundCode) => {
            if (!_.find(funds, f => f.fundCode === fundCode)) {
              updatedTopUps[fundCode] = alloc;
            }
          });
          this.setState({
            confirmMsg: null,
            fundAllocs: updatedFunds,
            topUpAllocs: updatedTopUps
          });
        }
      });
    }
  }

  _confirmDelete(fund) {
    const {langMap} = this.context;
    const {fundAllocs, topUpAllocs} = this.state;
    this.setState({
      confirmMsg: window.getLocalizedText(langMap, 'quotation.fund.list.confirmDelete'),
      onConfirm: () => {
        let updatedFunds = {};
        let updatedTopUps = {};
        _.each(fundAllocs, (alloc, fundCode) => {
          if (fundCode !== fund.fundCode) {
            updatedFunds[fundCode] = alloc;
          }
        });
        _.each(topUpAllocs, (alloc, fundCode) => {
          if (fundCode !== fund.fundCode) {
            updatedTopUps[fundCode] = alloc;
          }
        });
        this.setState({
          confirmMsg: null,
          fundAllocs: updatedFunds,
          topUpAllocs: updatedTopUps
        });
      }
    });
  }

  _updateSelectedFunds(selectedFunds) {
    this.setState({
      fundAllocs: selectedFunds.fundAllocs,
      topUpAllocs: selectedFunds.topUpAllocs,
      openAvailableList: false
    });
  }

  _getFundGroupDesc(group) {
    const {optionsMap, lang} = this.context;
    let riskRatingsDesc = _.join(_.map(group.riskRatings, rr => window.getOptionTitle(rr, optionsMap.riskRating, lang)), ' / ');
    return group.percentage + '% ' + riskRatingsDesc;
  }

  _getPortfolioModelDesc(riskProfile, modelTitle, groups) {
    const {lang, langMap} = this.context;
    let riskProfileDesc = window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + riskProfile);
    let modelDesc = window.getLocalText(lang, modelTitle);
    let riskDesc = _.join(_.map(groups, (group) => this._getFundGroupDesc(group)), ', ');
    return riskProfileDesc + ' | ' + modelDesc + ' (' + riskDesc + ')';
  }

  _getPortfolioList() {
    const {lang, langMap, optionsMap} = this.context;
    const {riskProfile, portfolio} = this.state;
    let portfolioOptions = _.filter(optionsMap.portfolioModels.portfolios, p => p.riskProfile <= riskProfile);
    let menuItems = [];
    if (portfolioOptions) {
      _.each(portfolioOptions, (portfolioOption) => {
        _.each(portfolioOption.models, (model) => {
          menuItems.push(
            <MenuItem
              key={model.id}
              value={model.id}
              primaryText={model.any ? window.getLocalText(lang, model.title) : this._getPortfolioModelDesc(portfolioOption.riskProfile, model.title, model.groups)}
            />
          );
        });
      });
    }
    return (
      <SelectField
        autoWidth
        value={portfolio}
        errorText={!portfolio && window.getLocalizedText(langMap, 'form.err.required')}
        onChange={(e, index, value) => {
          this._changePortfolio(value);
        }}
      >
        {menuItems}
      </SelectField>
    );
  }

  _getFundListItem(fund) {
    const {lang, optionsMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    let fundName = window.getLocalText(lang, fund.fundName);
    let assetClassDesc = window.getOptionTitle(fund.assetClass, optionsMap.assetClass, lang);
    let riskRatingDesc = window.getOptionTitle(fund.riskRating, optionsMap.riskRating, lang);
    return (
      <tr key={fund.fundCode}>
        <td>{fundName}</td>
        <td>{fund.ccy}</td>
        <td>{assetClassDesc}</td>
        <td>{riskRatingDesc}</td>
        <td>
          <NumericField
            id="fundAlloc"
            fullWidth
            maxIntDigit={3}
            maxValue={100}
            minValue={0}
            value={fundAllocs[fund.fundCode]}
            onChange={(e, value) => {
              fundAllocs[fund.fundCode] = value;
            }}
            onBlur={() => {
              this.setState({});
            }}
          />
        </td>
        {hasTopUpAlloc ? (
          <td>
            <NumericField
              id="topUpAlloc"
              fullWidth
              maxIntDigit={3}
              maxValue={100}
              minValue={0}
              value={topUpAllocs[fund.fundCode]}
              onChange={(e, value) => {
                topUpAllocs[fund.fundCode] = value;
              }}
              onBlur={() => {
                this.setState({});
              }}
            />
          </td>
         ) : null
        }
        <td>
          <IconButton onTouchTap={() => this._confirmDelete(fund)}>
            {getIcon('removeCircle', 'red')}
          </IconButton>
        </td>
      </tr>
    );
  }

  _getSelectedFunds() {
    const {availableFunds, hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    return _.filter(availableFunds, f => fundAllocs[f.fundCode] !== undefined || (hasTopUpAlloc && topUpAllocs[f.fundCode] !== undefined));
  }

  _getCurrentPortfolioModel() {
    const {optionsMap} = this.context;
    let {portfolio} = this.state;
    let model;
    _.each(optionsMap.portfolioModels.portfolios, p => {
      _.each(p.models, m => {
        if (m.id === portfolio) {
          model = m;
        }
      });
    });
    return model;
  }

  _getFundListItemsWithGrouping() {
    const {langMap} = this.context;
    const {hasTopUpAlloc} = this.props;
    const {fundAllocs, topUpAllocs} = this.state;
    let model = this._getCurrentPortfolioModel();
    if (model) {
      if (model.any) {
        return this._getFundListItems();
      }
      let items = [];
      let selectedFunds = this._getSelectedFunds();
      _.each(model.groups, (group, index) => {
        let funds = _.filter(selectedFunds, f => group.riskRatings.indexOf(f.riskRating) > -1);
        let groupTotal = _.reduce(funds, (sum, fund) => sum + (fundAllocs[fund.fundCode] || 0), 0);
        let groupTopUpTotal = _.reduce(funds, (sum, fund) => sum + (topUpAllocs[fund.fundCode] || 0), 0);
        items.push(
          <tr key={'group-' + index} className={styles.GroupHeader}>
            <td colSpan="3">{this._getFundGroupDesc(group)}</td>

            <td colSpan="2">
              <div style={{ display: 'flex'}}>
                <div style={{textAlign: 'right', width: '40%'}}>{window.getLocalizedText(langMap, 'quotation.fund.list.percentage')}</div>
                {groupTotal != group.percentage ?<div style={{textAlign: 'right', color:'red', width: '40%'}}>{groupTotal}</div>:
              <div style={{textAlign: 'right', color:'black', width: '40%'}}>{groupTotal}</div>}
              </div>
              {groupTotal != group.percentage ?
              <div style={{textAlign: 'right', width: '80%'}}>
                 <label style={{ flex: 1, textAlign: 'right', color: 'red' }}>
                 {window.getLocalizedText(langMap, 'quotation.fund.info.totalAllocNeeds') + group.percentage + '%'}
                  </label>
              </div>: null}
            </td>
            {hasTopUpAlloc ? <td/ > : null}

            <td>
              <IconButton onTouchTap={() => this._confirmDeleteGroup(funds)}>
                {getIcon('removeCircle', 'red')}
              </IconButton>
            </td>
          </tr>
        );
        _.each(funds, (fund) => {
          items.push(this._getFundListItem(fund));
        });
      });
      return items;
    }
  }

  _getFundListItems() {
    return _.map(this._getSelectedFunds(), fund => this._getFundListItem(fund));
  }

  _getFundList() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    return (
      <table className={styles.SelectedFundList + ' ' + styles.QuotTable}>
        <colgroup>
          <col className={styles.FundNameCol} />
          <col className={styles.ColGroupA} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          <col className={styles.ColGroupB} />
          {hasTopUpAlloc? <col className={styles.ColGroupB} />: null}
          <col className={styles.ColGroupC} />
        </colgroup>
        <thead>
          <tr>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.fundName')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.ccy')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.assetClass')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.riskRating')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.fund.list.alloc')}</th>
            {hasTopUpAlloc? <th>{window.getLocalizedText(langMap, 'quotation.fund.list.topUpAlloc')}</th>: null}
            <th />
          </tr>
        </thead>
        <tbody>
          {this.state.invOpt === 'buildPortfolio' ? this._getFundListItemsWithGrouping() : this._getFundListItems()}
        </tbody>
      </table>
    );
  }

  _getInfoContainer() {
    const {langMap} = this.context;
    const {hasTopUpAlloc, showTopUpAlloc} = this.props;
    const {riskProfile, invOpt} = this.state;
    let totalAlloc = this._getTotalFundAlloc();
    let totalTopUpAlloc = this._getTotalTopUpAlloc();
    let disableSave = totalAlloc !== 100 || (hasTopUpAlloc && totalTopUpAlloc !== 100) || !this._hasValidPortfolio();

    return (
      <div className={styles.InfoContainer}>
        {riskProfile ? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.assessedRiskProfile')}</div>
            <div className={styles.InfoValue}>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.' + riskProfile)}</div>
          </div>
        ) : null}
        <div className={styles.InfoItem}>
          <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.chooseInvOpt')}</div>
          <div className={styles.InfoValue}>
            <SelectField
              value={invOpt}
              onChange={(e, index, value) => {
                this._changeInvOpt(value);
              }}
            >
              <MenuItem value={'mixedAssets'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.mixedAssets')} />
              {riskProfile ? <MenuItem value={'buildPortfolio'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.buildPortfolio')} /> : null}
              <MenuItem value={'optimizeToProfile'} primaryText={window.getLocalizedText(langMap, 'quotation.fund.invOpt.optimizeToProfile')} />
            </SelectField>
          </div>
        </div>
        {riskProfile ? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{invOpt === 'buildPortfolio' ? window.getLocalizedText(langMap, 'quotation.fund.info.portfolio') : ''}</div>
            <div className={styles.InfoValue}>{invOpt === 'buildPortfolio' ? this._getPortfolioList() : null}</div>
          </div>
        ) : null}
        <div className={styles.InfoItem}>
          <div className={styles.InfoWithMessage}>
            <div className={styles.InfoRow}>
              <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, showTopUpAlloc? 'quotation.fund.info.totalPremiumAlloc': 'quotation.fund.info.totalAlloc')}</div>
              <div className={styles.InfoValue} style={{ display: 'flex', alignItems: 'center' }}>
                <label style={{ flex: 1, textAlign: 'center', color: totalAlloc !== 100 ? 'red' : null }}>{totalAlloc + '%'}</label>
                <IconButton onTouchTap={() => this._confirmDeleteAll()}>
                  {getIcon('removeCircle', 'red')}
                </IconButton>
              </div>
            </div>
            <div className={styles.InfoRow}>
            <div className={styles.InfoTitle}></div>
            <div className={styles.InfoValue}>
              {disableSave ? (
                <label style={{ flex: 1, textAlign: 'left', color: 'red' }}>
                  Total fund allocation is not equal to 100%
                </label>
              ) : null}
              </div>
            </div>
          </div>
        </div>
        {hasTopUpAlloc ?(
          <div className={styles.InfoItem}/>
        ): null}
        {hasTopUpAlloc? (
          <div className={styles.InfoItem}>
            <div className={styles.InfoTitle}>{window.getLocalizedText(langMap, 'quotation.fund.info.totalTopUpAlloc')}</div>
            <div className={styles.InfoValue} style={{ display: 'flex', alignItems: 'center' }}>
              <label style={{ flex: 1, textAlign: 'center', color: totalAlloc !== 100 ? 'red' : null }}>{totalTopUpAlloc + '%'}</label>
              <IconButton onTouchTap={() => this._confirmDeleteAllTopUp()}>
                {getIcon('removeCircle', 'red')}
              </IconButton>
            </div>
          </div>
        ): null}
      </div>
    );
  }

  render() {
    const {langMap} = this.context;
    const {availableFunds, hasTopUpAlloc, showTopUpAlloc} = this.props;
    const {openAvailableList, confirmMsg, onConfirm, openDisclaimer, onCloseDisclaimer, riskProfile, invOpt, fundAllocs, topUpAllocs} = this.state;
    let portfolioModel = this._getCurrentPortfolioModel();
    let filterRiskRatings = null;
    if (invOpt === 'buildPortfolio' && portfolioModel) {
      if (portfolioModel.any) {
        filterRiskRatings = [1, 2, 3, 4, 5];
      } else {
        filterRiskRatings = [];
        _.each(portfolioModel.groups, (group) => {
          filterRiskRatings = filterRiskRatings.concat(group.riskRatings);
        });
      }
    }
    return (
      <SaveDialog
        open={this.state.open}
        title={window.getLocalizedText(langMap, 'quotation.fund.fundSelection.title')}
        disableSave={this._getTotalFundAlloc() !== 100 || (hasTopUpAlloc && this._getTotalTopUpAlloc() !== 100) || !this._hasValidPortfolio()}
        onSave={() => this.saveFunds()}
        onClose={() => this.closeDialog()}
        className={styles.FundDialog}
        bodyStyle={{height: '100% !important', minHeight:'1200px !important' }}
      >
        <div style={{ minWidth: 880, padding: 24 }}>
          {this._getInfoContainer()}
          {this._getFundList()}
           {this._getSelectedFunds().length == 0 ? (<p style={{ textAlign: 'center', color:'grey' }}>{window.getLocalizedText(langMap, 'quotation.fund.message.nofundtitle')}</p>) : null}
          <div className={styles.BtnContainer}>
            <RaisedButton
              primary
              disabled={invOpt === 'buildPortfolio' && !portfolioModel}
              label={window.getLocalizedText(langMap, 'quotation.fund.btn.addFund')}
              onTouchTap={() => this.setState({ openAvailableList: true })}
            />
          </div>
          {riskProfile ? (
            <div className={styles.RiskNote}>{window.getLocalizedText(langMap, 'quotation.fund.list.riskNote')}</div>
          ) : null}
          <AvailableFundDialog
            open={openAvailableList}
            onClose={() => this.setState({ openAvailableList: false })}
            onSave={(selectedFunds) => this._updateSelectedFunds(selectedFunds)}
            riskProfile={riskProfile}
            invOpt={invOpt}
            availableFunds={availableFunds}
            filterRiskRatings={portfolioModel && filterRiskRatings}
            fundAllocs={fundAllocs}
            topUpAllocs={topUpAllocs}
            hasTopUpAlloc={hasTopUpAlloc}
            showTopUpAlloc={showTopUpAlloc}
          />
          <ConfirmDialog
            open={!!confirmMsg}
            confirmMsg={confirmMsg}
            onClose={() => this.setState({ confirmMsg: null })}
            onConfirm={() => {
              onConfirm && onConfirm();
              this.setState({ confirmMsg: null });
            }}
          />
          <TableDialog
            open={!!openDisclaimer}
            className={styles.FundDialog}
            onClose={() => {
              this.setState({
                openDisclaimer: false,
                onCloseDisclaimer: null
              }, () => onCloseDisclaimer && onCloseDisclaimer());
            }}
          />
        </div>
      </SaveDialog>
    );
  }

}
