import React from 'react';
import PropTypes from 'prop-types';
import {Divider,
  IconButton,
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle,
  CardText,
  Dialog,
  FlatButton,
  Avatar}                   from 'material-ui';

import * as _               from 'lodash';

import {getIcon}            from '../Icons/index';

import EABTextField         from '../CustomViews/TextField.js';
import EABTextArea          from '../CustomViews/TextArea.js';
import RadioButtonGroup     from '../CustomViews/RadioButtonGroup';
import TextSelection        from '../CustomViews/TextSelection';
import EABViewOnlyLabel     from '../CustomViews/ViewOnlyLabel';
import EABViewOnlyField     from '../CustomViews/ViewOnlyField';
import EABPickerField       from '../CustomViews/PickerField';
import EABDatePickerField   from '../CustomViews/DatePickerField';
import EABMenu              from '../CustomViews/Menu';
import EABTabs              from '../CustomViews/Tabs';
import EABAvatar            from '../CustomViews/Avatar';
import List                 from '../CustomViews/List';
import EABTable             from '../CustomViews/Table.js';
import EABTableView         from '../CustomViews/TableView.js';
import EABCheckboxGroup     from '../CustomViews/CheckBoxGroup';
import EABCB                from '../CustomViews/CheckBox.js';
import EABPopover           from '../CustomViews/Popover'
import EABCheckBoxPopup     from '../CustomViews/CheckBoxPopup';
import EABButtonPopup       from '../CustomViews/ButtonPopup';
import EABButton            from '../CustomViews/Button';
import EABToolTips          from '../CustomViews/ToolTips';
import styles               from '../Common.scss';
import EABComponent         from '../Component';
import Signature            from '../Pages/Pos/Applications/Signature/index'
import Payment              from '../Pages/Pos/Applications/Payment/index'
import Submission           from '../Pages/Pos/Applications/Submission/index';
import ShieldAppForm        from '../Pages/Pos/Applications/AppForms/ShieldAppForm';
import ShieldSignature      from '../Pages/Pos/Applications/Signature/ShieldSignature';
import ShieldPayment        from '../Pages/Pos/Applications/Payment/ShieldPayment';
import ShieldSubmission     from '../Pages/Pos/Applications/Submission/ShieldSubmission';
import MonthYearPicker      from '../CustomViews/MonthYearPicker'
import ShowValue            from '../CustomViews/ShowValue'
import SATable              from '../CustomViews/SATable'
import ErrorMessage         from '../CustomViews/ErrorMessage';
import * as _v              from '../../utils/Validation';
import * as needsActions    from '../../actions/needs.js';
import CaseStatus           from '../CustomViews/CaseStatus';
import CommentBlock         from '../CustomViews/CommentBlock';
import CommentReason        from '../CustomViews/CommentReason';
import ToolTips             from '../CustomViews/ToolTips';
import DocAction            from '../CustomViews/DocAction';
import DynColFileUpload     from '../CustomViews/DynColFileUpload';
import DynColFilePreview    from '../CustomViews/DynColFilePreview';
import MultipleDisplay      from '../CustomViews/MultipleDisplay';
import moment from 'moment';
import { MomentDateTimeFormatForApproval,
  DateTimeFormat3,
  MOMENT_TIME_ZONE}         from '../../constants/ConfigConstants';
import forEach              from 'lodash/forEach';
import _getOr               from 'lodash/fp/getOr';

import {getAvatarInitial}   from '../../utils/client';

class DynColumn extends EABComponent {
  constructor(props) {
    super(props);

    this.state = Object.assign({}, this.state, {
      values: props.values,
      changedValues: props.changedValues,
      error: props.error,
      template: props.template,
      pCDialogOpen: false
    });
  }

  openPostalCodeErrDialog= () => {
    this.setState({pCDialogOpen: true});
  }

  validate=()=>{
    if (_.isFunction(this.props.validate)) {
      this.props.validate(this.state.changedValues);
    }
  }

  getStyle = () =>{
    let fhboxItemStyle = {
      flexGrow: 1
    }
    return {
      fhboxItemStyle
    }
  }

  displayHtml = (content) =>{
    return content;
  }

  getBlockTitle = (title, template) =>{
    let bTitleCls = template.titleType == "normal" ? null : styles.sectionLabel;
    let bTitleSty = template.titleType == "normal" ? {marginTop: '30px'} : {};
    let bStye = template.titleType == "normal" ? {marginTop: '30px'} : {color: this.context.muiTheme.palette.primary2Color};

    return (
      <div className={bTitleCls} style={bTitleSty}>
        <label key={"sectionLabel"+(this.runningId++)}
          style={bStye}>
          {title}
        </label>
      </div>
    );
  }

  componentWillReceiveProps(nextProps){
    let {template, values, changedValues, error} = this.props;
    let {template: nTemplate, values: nValues, changedValues: nChangedValues, error: nError} = nextProps;
    let isUpd = false;

    if(!isEqual(template, nTemplate)){
      template = nTemplate;
      isUpd = true;
    }
    if(!isEqual(values, nValues)){
      values = nValues;
      changedValues = nChangedValues;
      isUpd = true;
    }
    if(!isEqual(changedValues, nChangedValues)){
      changedValues = nChangedValues;
      isUpd = true;
    }

    if(!isEqual(error, nError)){
      error = nError;
      isUpd = true;
    }

    if(isUpd){
      this.setState({
        template,
        values,
        changedValues,
        error,
        showSpouseDialog: false
      })
    }
  }

  renderItems = (fields, template, values, changedValues, error={}, key, onItemChange, tabValues, skipValidCheck) =>{
    if(tabValues && tabValues instanceof Object){
      this.state.tabValues = tabValues.values;
    }

    if(!checkExist(template, 'items'))
      return;
    template.items.map((item, index)=>{
      this.renderItem(fields, item, values, changedValues, error, index, key, onItemChange, skipValidCheck);
   })
 }

  renderItem = (items, iTemplate, values, changedValues, error={}, index, key, onItemChange, skipValidCheck) =>{
    let {muiTheme, langMap, lang, validRefValues, store, optionsMap} = this.context;
    let {app} = store.getState();
    let { agentProfile } = app;
    let {showDiff, changeActionIndex} = this.props;
    let self = this;

    // if it is a new change get default value if changed value is empty
    if (_.isString(iTemplate.id)
        && !checkExist(changedValues, iTemplate.id)
        && iTemplate.value
        && _.toUpper(iTemplate.type) != 'DATEPICKER'
        && changedValues[iTemplate.id] !== ''){
      changedValues[iTemplate.id] = iTemplate.value;
    }

    setMandatory(iTemplate, changedValues);
    setDisable(iTemplate, changedValues, this.state.tabValues, optionsMap);
    setPicker(iTemplate, changedValues, optionsMap);
    // setValueTrigger(iTemplate, changedValues, this.state.tabValues);

    if (!showCondition(this.state.tabValues || validRefValues, iTemplate, values,
      changedValues, this.state.changedValues, this.showCondItems, this.context)) {
      //as it's hidden, all values should be removed
      resetHiddenFieldValues(iTemplate,changedValues);
      return;
    }

    let handleChange=(id, value)=>{
      if (_.isFunction(this.props.handleOnChange)){
        this.props.handleOnChange(id, value);
      }
      if(_.isFunction(this.props.onChange)){
        this.props.onChange();
      }
      if (changedValues[id] !== value) {
        changedValues[id] = value;
        setValueTrigger(iTemplate, changedValues, this.state.values);

        if (_.isFunction(this.props.validate) && !skipValidCheck) {
          this.validate();
        }
        if (_.isFunction(this.props.completeChangedValues)) {
          this.props.completeChangedValues();
        }
      }
    }

    let handleBlur = (id, value, updateAddr)=>{
      if (changedValues[id] !== value) {
        changedValues[id] = value;
      }

      if (_.isFunction(self.props.validate) && !skipValidCheck){
        self.validate();
      }
      if (_.isFunction(this.props.completeChangedValues)) {
        this.props.completeChangedValues();
      }
    }

    let title = null;
    if (iTemplate.title) {
      title = getLocalizedText(langMap, iTemplate.title);

      if (iTemplate.tips){
        let tipText = iTemplate.tips.text;
        let titles = title.split(tipText);
        title = (<div>
                    <p style={styles.floatingTitle}>
                      <span>{titles[0]}</span><u>{tipText}</u><span>{titles[1]}</span>
                      {iTemplate.mandatory ? <span className={styles.mandatoryStar}>*</span>:null}
                    </p>
                  </div>
                )
     }
     else {
        title = (
          <div>
            <span style={styles.floatingTitle}>{title}</span>
            {iTemplate.mandatory ? <span className={styles.mandatoryStar}>*</span>:null}
          </div>
        )
      }
    };

    if (!iTemplate.type) {return null;}

    let viewType = iTemplate.type.toUpperCase();
    let __id = key+'_' + index + '_'+ iTemplate.id;

    if (viewType == "LIST"){
      items.push(
        <List
          ref={ref=>{this[iTemplate.id]=ref}}
          key={"ctn_"+ iTemplate.id}
          template={iTemplate}
          index={index}
          values={values}
          error={error}
          changedValues={changedValues}
          requireShowDiff={showDiff}
          changeActionIndex={changeActionIndex}
          rootValues={this.state.values}
          handleChange={handleChange}
          renderItemsFunc={this.renderItem}/>
      );

    }

    else if (viewType == "DIVIDER"){
      items.push(<Divider style={iTemplate.style || {marginTop:"10px", marginBottom:"10px"}}/>)
    }

    else if (viewType == "SATABLE"){

      items.push(<SATable
        template={iTemplate}
        values={values}
        changedValues={changedValues}
        error={error}
        readonly = {iTemplate.readonly}
        validRefValues={this.state.tabValues}
        renderItemsFunc={this.renderItem}
        handleChange={handleChange}
        presentation={iTemplate.presentation}
      />);

    }

    else if (viewType == "GOFNA"){
      let self = this;
      let goFNA = function(){
        needsActions.goFE(self.context);
      }

      let gofna = <div className={styles.alignRight}><p className={styles.border} onClick={goFNA} style={{cursor:'pointer', background: "#00008f", color: '#ffffff'}}>Edit Existing Insurance Portfolio at FNA</p></div>

      if (!iTemplate.disabled) {
        items.push(gofna);
      }
    }

    else if (viewType === 'SIGNATURE') {
      items.push(
        <Signature key='signature' ref='signature' updateAppBarTitle={this.props.updateAppBarTitle} />
      );
    }

    else if (viewType === 'PAYMENT') {
      items.push(
        <Payment key='payment' ref='payment' updateAppBarTitle={this.props.updateAppBarTitle} />
      );
    }

    else if (viewType === 'SUBMISSION') {
      items.push(
        <Submission key='submission' ref='submission' updateAppBarTitle={this.props.updateAppBarTitle} />
      );
    }

    else if (viewType === 'SHIELDAPPFORM') {
      items.push(
        <ShieldAppForm key='shieldAppForm' handleChangedValues={this.props.handleChangedValues} />
      );
    }

    else if (viewType === 'SHIELDSIGNATURE') {
      items.push(
        <ShieldSignature key='shieldSignature' />
      );
    }

    else if (viewType === 'SHIELDPAYMENT') {
      items.push(
        <ShieldPayment key={`${__id}-ShieldPayment`} handleChangedValues={this.props.handleChangedValues} />
      );
    }

    else if (viewType === 'SHIELDSUBMISSION') {
      items.push(
        <ShieldSubmission key={`${__id}-ShieldSubmission`} handleChangedValues={this.props.handleChangedValues} />
      );
    }

    else if (viewType == "HEADER") {
      items.push(
        <div key={__id+'-header'} className={styles.PageSectionTitle + ' ' + styles.ClientChoicePanelTitleColor }>
          {_.toUpper(getLocalText(lang, iTemplate.title))}
        </div>
      )
    }

    else if (viewType == "PAPER") {
      let _fields = [];
      let pageStyle = iTemplate.style && iTemplate.style.toLowerCase() == "flex"? {display: 'flex', margin: '24px auto', width: '100%'}: {};

      this.renderItems(_fields, iTemplate, values, changedValues, error, key, onItemChange);
      let fieldsItem = [];
      _fields.forEach((field, index, fields) => {
        fieldsItem.push(
          <div style={{width: '100%', lineHeight: '32px', height: '32px', textAlign: (iTemplate.showCol) ? undefined :  'center'}}>
            {field}
          </div>
        )
      })
      items.push(
        <Card style={{margin: '12px 0px', fontWeight: '600'}}>
          <CardText>
            <div id={(iTemplate.showCol) ? 'CardinColView': 'CardinRowView'} style={{display: 'flex', justifyContent: 'space-between', flexDirection: (iTemplate.showCol) ? 'column': 'row', height: iTemplate.noFixedHeight ? null : (iTemplate.showCol) ? '110px': '70px'}}>
              {fieldsItem}
            </div>
          </CardText>
        </Card>
      )
    }

    else if (viewType === 'CASECARD'){
      let cardfields = [];
      this.renderItems(cardfields, iTemplate, values, changedValues, error, key, onItemChange);
      items.push(
        <Card style={{position: 'relative', cursor: 'pointer'}}
          onTouchTap={()=>{(iTemplate.onClick) ? iTemplate.onClick(changedValues) : undefined}}>
          <CardText style={{display: 'flex'}}>
              {cardfields}
          </CardText>
          <Divider inset={true}/>
        </Card>
      )
    }

    else if (viewType === 'CARDBLOCK') {
      let cardBlockFields = [];
      this.renderItems(cardBlockFields, iTemplate, values, changedValues, error, key, onItemChange);
      items.push(
        <div className={styles.displayInlineBlock + ' ' + styles.SectionPaddingLeftRight} style={iTemplate.style}>
          {cardBlockFields}
        </div>
      );
    }

    else if (viewType === 'CARDNAMEDITEM') {
      let cItemValue = changedValues[iTemplate.id];

      if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'include' && changedValues[_.get(iTemplate, 'Mapping.id')]){
        cItemValue = cItemValue + ' (' + changedValues[_.get(iTemplate, 'Mapping.id')] + ')';
      } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'isEmpty' && isEmpty(changedValues[_.get(iTemplate, 'Mapping.value')])){
        cItemValue = _.get(iTemplate, 'Mapping.display');
      } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'isEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] === changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')]){
        cItemValue = _.get(iTemplate, 'Mapping.display');
      } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'notEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] !== changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')] && _.get(iTemplate, 'Mapping.display')) {
        cItemValue = _.get(iTemplate, 'Mapping.display');
      } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'notEqual' && changedValues[_.get(iTemplate, 'Mapping.valuePair[0]')] === changedValues[_.get(iTemplate, 'Mapping.valuePair[1]')] && _.get(iTemplate, 'Mapping.display')) {
        cItemValue = changedValues[iTemplate.id];
      } else if (iTemplate.Mapping && _.get(iTemplate, 'Mapping.method') === 'checkManagerisMyself' && agentProfile.agentCode === changedValues['managerId']) {
        cItemValue = _.get(iTemplate, 'Mapping.display');
      }

      if (iTemplate.subType && iTemplate.subType.toUpperCase() === 'APPROVAL_DATE' && cItemValue && cItemValue !== '-' ) {
        cItemValue = moment(cItemValue).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
      }

      items.push(
        <div className={styles.StandardLineHeight} style={{display: 'flex'}}>
          <div className={styles.displayInlineBlock + ' ' + styles.fontWeight600} style={{minWidth: '240px', width: '240px', verticalAlign: 'top'}}>
            {iTemplate.name}:
          </div>
          <div className={styles.displayInlineBlock}>
            {cItemValue}
          </div>
        </div>
      )
    }

    else if (viewType === 'CASESTATUS') {
      items.push(
        <CaseStatus template={iTemplate} changedValues={changedValues}/>
      )
    }

    else if (viewType === 'STATUSSECTIONNAME'){
      items.push(
        <div className={styles.StatusSectionName}>{iTemplate.name}</div>
      )
    }

    else if (viewType === 'ICON'){
      items.push(
        <Avatar
          icon={getIcon(iTemplate.name, '#FAFAFA')}
        />
      );
    }

    else if (viewType == "PARAGRAPH"){
      var paragClassName;
      let parStyleObj;
      if (iTemplate.paragraphType == 'warning') {
        paragClassName = styles.alignCenter + ' ' + styles.ShowAlertColor + ' ' + styles.bold;
      }

      if (iTemplate.paragraphType == 'warningWithNote') {
         paragClassName = styles.alignCenter + ' ' + styles.ShowAlertColor;
      }

      if (iTemplate.content && iTemplate.paragraphType !== 'pda') {
        iTemplate.content = iTemplate.content.replaceAll('<p>', '<p id="warningnote">');
      }

      if (iTemplate.paragraphType === 'replaceableContent') {
        if (_.isArray(values[iTemplate.id])) {
          _.forEach(values[iTemplate.id], (replaceContent, i) => {
            iTemplate.content = iTemplate.content.replace('{{' + i + '}}', replaceContent);
          });
        }
      }

      parStyleObj = (iTemplate.noMargin) ? {} : {margin: '15px 0'};

      items.push(
        <div
          className={paragClassName}
          style={Object.assign(iTemplate.style || {}, parStyleObj, {textAlign: iTemplate.align})}>
          <b>{iTemplate.title}</b>
          <div
            key={iTemplate.id}
            dangerouslySetInnerHTML={{__html: this.displayHtml(iTemplate.content)}}>
          </div>
        </div>
      );
    }

    else if (viewType == "LAYOUT"){
      var thisItems = iTemplate.items;
      var thisSubItems = []
      for (let thisItem in thisItems){
        this.renderItems(thisSubItems, thisItem, values, changedValues, error, key, onItemChange);
     }
      items.push(thisSubItems);
    }

    else if (viewType == 'SECTION'){
      let _fields = [];
      this.renderItems(_fields, iTemplate, values, changedValues, error, key, onItemChange);
      if(_fields.length){
        items.push(
          <div className={styles.Section} key={__id}
            onDrop={(e)=>{e.preventDefault()}}
            onDragOver={(e)=>{e.preventDefault()}}>
            <div className={styles.Title}>{getLocalText(lang, iTemplate.title)}</div>
            {_fields}
          </div>
        )
     }
    }

    else if (viewType == 'SEARCHMENUSECTION'){
      let _fields = [];
      this.renderItems(_fields, iTemplate, values, changedValues, error, key, onItemChange);
      if(_fields.length){
        items.push(
          <div className={styles.Section} key={__id}
            onDrop={(e)=>{e.preventDefault()}}
            onDragOver={(e)=>{e.preventDefault()}}>
            <div className={styles.StatusSectionName}>{getLocalText(lang, iTemplate.title)}</div>
            {_fields}
          </div>
        )
     }
    }

    else if (viewType == 'CARDCONTAINER'){
      let _fields = [];
      if (iTemplate.title) {
        _fields.push(
          <div className={styles.CardSectionTitle}>
            {window.getLocalText(lang, iTemplate.title)}
          </div>
        );
      }
      this.renderItems(_fields, iTemplate, values, changedValues, error, key, onItemChange);
      let cardStyle = {};
      if (iTemplate.searchMenu === true){
        cardStyle = {padding: (iTemplate.noPadding) ? '0px' : '24px', background: '#f9f9f9'};
      } else {
        cardStyle = {padding: (iTemplate.noPadding) ? '0px' : '24px 48px', maxWidth: '1200px', margin: 'auto', width: '100%'};
      }
      if(_fields.length){
        items.push(
          <Card style={cardStyle}>
            {_fields}
          </Card>
        )
        items.push(
          <div style={{padding: '12px'}}/>
        )
     }
    }

    else if (viewType == 'TEXT' || viewType == 'TEXTFIELD') {
      items.push(
        <EABTextField
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {"ctn_"+ iTemplate.id + index}
          template = {iTemplate}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error = {error}
          requireShowDiff = {this.props.showDiff}
          changeActionIndex = {this.props.changeActionIndex}
          rootValues = {this.state.values}
          openPostalCodeErr={this.openPostalCodeErrDialog}
          handleChange= {(id, value) => {
            if(_.isFunction(this.props.onChange)){
              this.props.onChange();
            }
            if((iTemplate.allowZero && changedValues[id] === 0) || changedValues[id] !== value){
              changedValues[id] = value;
              if(_.isFunction(self.props.validate) && !skipValidCheck){
                if (self.reloadTimeout) {
                  clearTimeout(self.reloadTimeout);
                }
                self.reloadTimeout = setTimeout(self.validate, 1000);
              }
              // if (_.isFunction(this.props.completeChangedValues)) {
              //   this.props.completeChangedValues();
              // }
            }
          }}
          handleBlur = {(id, value, updateAddr)=>{
            if(id && value && updateAddr)
              changedValues[id] = value;

            if(_.isFunction(self.props.validate) && !skipValidCheck){
              if (self.reloadTimeout) {
                clearTimeout(self.reloadTimeout);
                self.reloadTimeout = false;
              }
              self.validate()
            }
            // if (_.isFunction(this.props.completeChangedValues)) {
            //   this.props.completeChangedValues();
            // }
          }}
          />
      );
      if (iTemplate.subType === 'postalcode'){
        items.push(
          <Dialog
            actions={[
              <FlatButton
                label={window.getLocalizedText(langMap, 'app.ok')}
                primary
                onTouchTap={() => this.setState({ pCDialogOpen: false })}
              />
            ]}
            modal={true}
            open={this.state.pCDialogOpen}
            style={{zIndex: 10000}}
          >
            {window.getLocalizedText(langMap, 'The postal code you have just entered is not found in our database')}
          </Dialog>
        )
      }
    }

    else if (viewType == 'TEXTAREA'){
      items.push(
        <EABTextArea
          ref={ref=>this[iTemplate.id]=ref}
          key={__id}
          template={iTemplate}
          values={values}
          changedValues={changedValues}
          error={error}
          disabled={iTemplate.disabled}
          handleChange={handleChange}
          handleBlur={handleBlur}
          headerTitleStyle={iTemplate.headerTitleStyle}
          headerTitleContainerStyle={iTemplate.headerTitleContainerStyle}
        />
      )
    }

    else if (viewType == '12BOX'){
      let titleNode = null;
      if(iTemplate.title){
        titleNode = (
          <div>
            <label key={"label"+(this.runningId++)} className="Label">
              {getLocalizedText(langMap, getLocalText(lang, iTemplate.title))}
              {iTemplate.mandatory? <span key="s" style={{color:muiTheme.baseTheme.palette.errorColor}}>&nbsp;*</span>: null}
            </label>
          </div>
        );
      }

      let subItems = [];
      for (let si in iTemplate.items){
        let _item = iTemplate.items[si];
        this.renderItem(subItems, _item, values, changedValues, error, si, key, handleChange);
      }

      let style = this.getStyle();
      let _subItems = [];

      var column = iTemplate.presentation || (subItems.length>1?"Column2":"Column1");

      items.push(
        <div className={styles.DetailsItem + ' ' + styles.h12box}>
          <div key= {"12box"+(this.runningId++)}
            className={styles[column]}
            onDrop={(e)=>{e.preventDefault()}}
            onDragOver={(e)=>{e.preventDefault()}}>
              {titleNode}
              {subItems}
          </div>
        </div>
      )
    }

    else if (viewType == 'SUBTEXT'){
      let content = iTemplate.content;
      let _arryToReplce = content.split(/[<>]/);

      let strToDisplay = content;
      let arryToReplce = [];
      let subIndex = 1;
      while(subIndex < _arryToReplce.length){
        arryToReplce.push(_arryToReplce[subIndex]);
        subIndex = subIndex + 2;
     }

      for(let sub = 0; sub < arryToReplce.length; sub ++){
        let replceStr = arryToReplce[sub];
        let _cValues = changedValues;
        let hasAt = "";
        if(replceStr.indexOf("@") != -1){
          _cValues = this.state.tabValues;
          replceStr = replceStr.replace("@", "");
          hasAt = "@";
        }


        let subValue = getValueFmRootByRef(_cValues, replceStr, null);
        if (subValue || !(replceStr == 'b' || replceStr == '/b' || replceStr.indexOf('span') > -1)) {
          // if(subValue == 'en')
          //   subValue = 'English';
          let options = getFieldFmTemplate(this.state.template, replceStr.split("/"), 0, "options");

          if (options && options.length) {
            for (var o in options) {
              if (options[o].value == subValue) {
                subValue = getLocalText(lang, options[o].title);
                break;
              }
            }
          }

          if(!subValue)
            subValue = "";

          strToDisplay = strToDisplay.replace("<" + hasAt + replceStr + ">", subValue);
        }
     }

      items.push(<div className={styles.StdLineHeight}  dangerouslySetInnerHTML={{__html:strToDisplay}}></div>);
    }

    else if (viewType == 'FBOX'){
      let subItems = [];
      for (let si in iTemplate.items){
        let _item = iTemplate.items[si];
        this.renderItem(subItems, _item, values, changedValues, error, si, key, handleChange);
      }

      let _subItems = [];

      for (let j = 0; j <  subItems.length; j++) {
        let flexBoxItemStyle = (j == subItems.length - 1) ? styles.flexBoxItem :  null;
        _subItems.push(<div key={subItems[j]['key'] + "wrap"} className={flexBoxItemStyle}>{subItems[j]}</div>)
      }
      let titleNode = null;
      if(iTemplate.title){
        titleNode = (
          <label key={"label"+(this.runningId++)} className={styles.ReadOnlyFieldFloatingLabel + ' ' + styles.ShowSubContentColor}>
            {getLocalizedText(langMap, getLocalText(lang, iTemplate.title))}
            {iTemplate.mandatory? <span key="s" style={{color:muiTheme.baseTheme.palette.errorColor}}>&nbsp;*</span>: null}
          </label>
        );
     }

      items.push(
        <div className={styles.DetailsItem + ' ' + styles.h12box}>
          <div className={styles.ReadOnlyFieldRoot}>
            {titleNode}
            <div key= {"fbox"+(this.runningId++)} className={styles.flexBox}>
              {_subItems}
            </div>
          </div>
        </div>
      )

    }

    else if (viewType == 'HBOX' ){
      let titleNode = null;
      let hError = null;
      if(iTemplate.key){
        if(error[iTemplate.key]){
          let errMsg = _v.getErrorMsg(iTemplate, error[iTemplate.key].code, this.context.lang);
          hError = <ErrorMessage template={iTemplate}  message={errMsg} style={{top: '0px', left: '0px', textAlign:'left'}}/>
        }
      }

      if(iTemplate.title){
        titleNode = (
          <div className={styles.hboxLabel}>
            <label key={"label"+(this.runningId++)} className={"Label " + (iTemplate.shadow)  ? styles.ShowSubContentColor : ''} style={_.toUpper(iTemplate.subType) === 'TEXT' ? { color: muiTheme.palette.primary2Color} : {}}>
              {window.getLocalizedText(langMap, window.getLocalText(lang, iTemplate.title))}
              {iTemplate.mandatory? <span key="s" style={{color:muiTheme.baseTheme.palette.errorColor}}>&nbsp;*</span>: null}
            </label>

          </div>
        );
      }

      let subItems = [];
      for (let si in iTemplate.items){
        let _item = iTemplate.items[si];
        this.renderItem(subItems, _item, values, changedValues, error, si, key, handleChange);
      }

      let style = this.getStyle();
      let _subItems = [];
      let styleClass;

      var column = iTemplate.presentation || "Column2";

      items.push(
        <div key= {"hbox"+(this.runningId++)}
          className={styles[column] +" " + styles.Box}
          onDrop={(e)=>{e.preventDefault()}}
          onDragOver={(e)=>{e.preventDefault()}}>
            {titleNode}
            {hError}
            {subItems}
        </div>
      )
    }

    else if (viewType == "BLOCK" || viewType == "TABLE-BLOCK" || viewType == "TABLE-BLOCK-T"){

      var blkItems = [];
      if(viewType != "TABLE-BLOCK" ){
        var templateTitle = iTemplate.title;
        if(templateTitle){
          templateTitle = getLocalizedText(langMap, templateTitle);
          var blkTitle = null;
          if(viewType =="BLOCK")
            blkTitle = this.getBlockTitle(templateTitle, iTemplate);
          else
            blkTitle = <div style={{margin:'15px 0'}} className={styles.viewTitle}>{iTemplate.title}{iTemplate.mandatory ? <span className={styles.mandatoryStar}>*</span>:null}</div>

          if(iTemplate.tips){
            //title with tips
            blkItems.push(<div className={styles.tableView}>
                            <div className={styles.tableViewItem}>{blkTitle}</div>
                            <div className={styles.tableViewItem}>
                              <EABToolTips hints={iTemplate.tips.title}
                                           iconType={iTemplate.tips.icon} />
                              {/* <ToolTips hints={iTemplate.tips.title}/> */}
                              {/**<IconButton
                                iconClassName="material-icons"
                                tooltip={iTemplate.tips.title}
                                iconStyle={{color:'#103184'}}
                                tooltipStyles={iTemplate.tooltipStyles || {height:'150px', width:'400px'}}
                                className={styles.iconButton}
                              >
                                {iTemplate.tips.icon}
                              </IconButton>*/}
                            </div>
                          </div>)
         }else if(iTemplate.laAddress){
          //title with copy address
          let _self = this;
          let showCopy = false;
          let extra = _self.props.extra;
          if(extra && extra.checkAddr){
            showCopy = extra.checkAddr(_self.state.tabValues);
          }

          let cpStyle = {
            fontSize: '12px',
            lineHeight: '12px',
            marginTop: '12px',
            marginBottom: '0px',
            marginLeft: '12px',
            cursor:'pointer',
            background: "#00008f",
            color: '#ffffff'
          }

          blkItems.push(<div className={styles.tableView}>
            <div className={styles.tableViewItem}>{blkTitle}</div>
            <div className={styles.tableViewItem}>
              {
                showCopy ?
                  <p
                    className={styles.border}
                    onClick={() => {
                      if (extra && extra.checkAddr) {
                        extra.copyAddr(_self.state.tabValues);
                        this.validate();
                      }
                    }}
                    style={cpStyle}>Populate Residential Address from Proposer</p> :
                  null
              }
            </div>
          </div>)
         }else{
            blkItems.push(blkTitle)
         }
       }
     }
     let bkErr = error[iTemplate.id || iTemplate.key];
     if(bkErr && iTemplate.mandatory){
        let errMSg = _v.getErrorMsg(iTemplate, bkErr.code, this.context.lang);
        blkItems.push(<div className={styles.ErrorMsgContainer}>{_.isString(errMSg)?<ErrorMessage template={iTemplate}  message={errMSg} style={{top: '0px', left: '0px', textAlign:'left'}}/>: null}</div>)
     }

      let id = iTemplate.id;
      let key = id || iTemplate.key;

      if(id && !changedValues[id])
        changedValues[id] = {}
      if(id && !values[id])
        values[id] = {}

      let bItems = [];
      bItems.push(blkItems);
      _.forEach(iTemplate.items, (_item, si)=>{
        this.renderItem(bItems, _item, id?values[id]:values, id?changedValues[id]:changedValues, key?error[key]: error, __id + si);
     });
     items.push(<div style={iTemplate.style}>{bItems}</div>);
    }

    else if (viewType == "MONTHYEARPICKER"){
      items.push(
        <MonthYearPicker
          ref = {iTemplate.id}
          key = {__id}
          template = {iTemplate}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error={error}
          rootValues = {this.state.values}
          handleChange= {handleChange}/>
      );
    }

    else if (viewType == "QRADIOGROUP"){
      let titleNode = null;
      let options = iTemplate.options;

      items.push(
        <RadioButtonGroup
          style={{}}
          defaultStyle={true}
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {__id}
          template = {iTemplate}
          options = {options}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error = {error}
          rootValues = {this.state.values}
          handleChange= {handleChange}/>
      );
    }

    else if (viewType == "TEXTSELECTION"){
      items.push(
        <div key={__id} style={{padding: '0px 24px'}}>
          <TextSelection
            id={__id}
            ref={ref=>this[iTemplate.id]=ref}
            template={iTemplate}
            values={values}
            changedValues={changedValues}
            error={error}
            handleChange = {handleChange}
          />
        </div>
      )
    }

    else if (viewType == 'LABEL') {
      let showInOneRow = iTemplate.showInOneRow;
      let hideTitle = iTemplate.hideTitle;
      let cardReadOnly = iTemplate.cardReadOnly;
      let iTemplateStyle = iTemplate.style;
      let labelStyle = {};
      let floatingLabelStyle = {};

      if (showInOneRow) {
        labelStyle = Object.assign({}, {marginRight:'0px', display:'inline', marginTop: '0px'});
        floatingLabelStyle = Object.assign({}, {marginRight:'33px'});
      }
      if (hideTitle) {
        labelStyle = {};
        floatingLabelStyle = Object.assign({}, {display:'none'});
      }

      items.push(
        <EABViewOnlyLabel
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {__id}
          template = {iTemplate}
          index = {index}
          values = {changedValues}
          error = {error}
          rootValues = {this.state.values}
          labelStyle = {labelStyle}
          iTemplateStyle={iTemplateStyle}
          floatingLabelStyle = {floatingLabelStyle}
          showInOneRow={showInOneRow}
          cardReadOnly = {cardReadOnly}
          />)
    }

    else if (viewType == 'READONLY') {
      let showInOneRow = iTemplate.showInOneRow;
      let hideTitle = iTemplate.hideTitle;
      let cardReadOnly = iTemplate.cardReadOnly;
      let iTemplateStyle = iTemplate.style;
      let cardReadonlyStyle = {};
      let labelStyle = {};
      let floatingLabelStyle = {};
      let {ccy} = this.props;

      if (showInOneRow) {
        labelStyle = Object.assign({}, {marginRight:'0px', display:'inline', marginTop: '0px'});
        floatingLabelStyle = Object.assign({}, {marginRight:'33px'});
      }
      if (hideTitle) {
        labelStyle = {};
        floatingLabelStyle = Object.assign({}, {display:'none'});
      }
      if (cardReadOnly) {
        cardReadonlyStyle = Object.assign({}, {color: muiTheme.palette.textColor, fontWeight: '600'});
      }

      items.push(
        <EABViewOnlyField
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {__id}
          template = {iTemplate}
          index = {index}
          values = {changedValues}
          error = {error}
          rootValues = {this.state.values}
          cardReadonlyStyle = {cardReadonlyStyle}
          labelStyle = {labelStyle}
          iTemplateStyle={iTemplateStyle}
          floatingLabelStyle = {floatingLabelStyle}
          showInOneRow={showInOneRow}
          ccy={ccy}
          />)
    }

    else if (viewType == 'QTITLE') {
      let qtItems = [];
      let title = iTemplate.replaceTitle ? getValueFmRootByRef(this.state.tabValues, iTemplate.replaceTitle.id, null) : iTemplate.title;

      let titleSpan = iTemplate.needToHighlight ? (
        <span key={iTemplate.id + 'titleHighlight'} style={iTemplate.style || {}}>
          {_.split(title, '{{')[0]}
          <span style={{ textDecoration: 'underline' }}>
            {_.split(_.split(title, '{{')[1], '}}')[0]}
          </span>
          {_.split(title, '}}')[1]}
        </span>
      ) : (<span key={iTemplate.id + 'titleNormal'} style={iTemplate.style || {}}>{title}</span>);

      qtItems.push(titleSpan);
      if(iTemplate.subTitle){
        qtItems.push(<span key={iTemplate.id + 'subTitle'} className={styles.qSubTitle}>{iTemplate.subTitle}</span>)
      }

      let qtStyle = {textAlign: (iTemplate.align) ? (iTemplate.align) : 'left'}
      if(iTemplate.style){
        qtStyle = Object.assign({}, qtStyle, iTemplate.style);
      }

      items.push(
        <div className={styles.QTitle} key={iTemplate.id} style={qtStyle}>{qtItems}</div>
        )
    }

    else if (viewType == 'SPAN'){
      items.push(<span>{iTemplate.title}</span>)
    }

    else if (viewType == 'HEADING') {
      var heading = changedValues[iTemplate.id]||"";
      heading = getLocalText(lang, heading);
      items.push(
        <div className={styles.heading} key={iTemplate.id} style={{color: this.context.muiTheme.palette.primary2Color}}>{heading}</div>
        )
    }

    else if (viewType === 'TABLESECTIONHEADING') {
      items.push(
        <div className={styles.TableSectionHeading} key={iTemplate.id}>{_.get(iTemplate, 'title')}</div>
      )
    }

    else if (viewType == 'PICKER'){
      var picker = (
        <EABPickerField
          id = {iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          itemId={__id}
          key = {__id}
          template = {iTemplate}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error = {error}
          handleChange = {handleChange}
        />
      );

      var pickerItem = picker;
      items.push(pickerItem);

    }

    else if (viewType == 'POPOVER'){
      items.push(
        <EABPopover
          id={iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          renderItems={this.renderItems}
          mode={muiTheme.isPortrait?'portrait':'landscape'}
          disabled={iTemplate.disabled}
          changedValues={changedValues}
          error={error}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType == 'CHECKBOXGROUP') {
      items.push(
        <EABCheckboxGroup
          id={iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          changedValues={changedValues}
          error={error}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType == 'CHECKBOX') {
      let eabCheckbox = (
        <EABCB
          id={iTemplate.id}
          ref={iTemplate.id}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          changedValues={changedValues}
          tabValues={this.state.tabValues}
          rootTemplate={this.state.template}
          labelStyle={iTemplate.contentStyle}
          error={error}
          handleChange={handleChange}
        />
      );

      if (iTemplate.hidden) {
        items.push(
          <div style={{display:'none'}}>
            {eabCheckbox}
          </div>
        )
     } else {
        items.push(eabCheckbox);
     }
    }

    else if (viewType == 'CHECKBOXPOPUP') {
      items.push(
        <EABCheckBoxPopup
          id={iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          changedValues={changedValues}
          error={error}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType == 'DATEPICKER'){
      items.push(
        <EABDatePickerField
          id = {iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {__id}
          template = {iTemplate}
          index = {index}
          values = {values}
          mode = {muiTheme.isPortrait?'portrait':'landscape'}
          disabled = {iTemplate.disabled}
          changedValues = {changedValues}
          error={error}
          handleChange = {handleChange}
          />
      );
    }

    else if (viewType == 'PAYMENTBUTTON') {
      items.push(<EABButtonPopup
        key={__id}
        template={iTemplate}
        values={values}
        changedValues={changedValues}
        style={{verticalAlign: 'bottom'}}
        handleChange={handleChange}
        />);
    }

    else if (viewType == 'BUTTON') {
      items.push(
        <EABButton
          key={__id}
          template={iTemplate}
          values={values}
          changedValues={changedValues}
          style={{verticalAlign: 'bottom'}}
          handleChange={handleChange}
        />
      );
    }

    else if (viewType == 'PHOTO') {
      let initial;
      if( iTemplate.profilePhoto){
        initial = getAvatarInitial({firstName: changedValues['firstName'], lastName: changedValues['lastName']});
        changedValues.initial = initial;
        values.initial = initial;
      }
      items.push(
        <EABAvatar
          id = {iTemplate.id}
          ref={ref=>{this[iTemplate.id]=ref}}
          key={__id}
          values={values}
          changedValues={changedValues}
          error={error}
          template={iTemplate}
          disabled={iTemplate.disabled}
          width={56}
          docId={this.props.docId || changedValues.cid}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType == "MENU"){

      let checkedMenu = [];
      let _menus = [];

      if(this.props.isApp){
        if(!changedValues.checkedMenu)
          changedValues.checkedMenu = [];

        checkedMenu = changedValues.checkedMenu;

        if(!changedValues.menus)
          changedValues.menus = [];

        _menus = changedValues.menus;
      }

      let landingMenuIndex = 0;
      if(this.props.isApp && this.props.isShield) {
        let mError = this.props.error;

        _.each(_menus, (value, index)=> {
          if ((mError[value] && mError[value].code) || checkedMenu.indexOf(value) == -1 || _.get(this.props, 'changedValues.completedMenus', []).indexOf(value) === -1) {
            landingMenuIndex = index;
            return false;
          }
        })

        if(this.props.updateCompleteMenu){
          this.props.updateCompleteMenu(mError, changedValues);
        }
      } else if(this.props.isApp){
        // if(checkedMenu.indexOf("menu_person") == -1){
        //   checkedMenu.push("menu_person");
        // }
        let mError = this.props.error;

        for(let mIndex = 0 ; mIndex < _menus.length; mIndex++){
          let tMenu = _menus[mIndex];
          if(checkedMenu.indexOf(tMenu) == -1 || (mError[tMenu] && mError[tMenu].code)) {
            landingMenuIndex = mIndex;
            // checkedMenu.push(tMenu); // comment by sam
            break;
          }
        }

        if(this.props.updateCompleteMenu){
          this.props.updateCompleteMenu(mError, changedValues);
        }
      }

      let menuTemplate = [];
      iTemplate.items.map((menuSection)=>{


        let msObj = {title: menuSection.title};
        let msItems = [];

        //add validate function to items;

        var validate = null;
        menuSection.items.map((menuItem, i)=>{
          if(menuItem.key && this.props.isApp){
            if(_menus.indexOf(menuItem.key) == -1){
              _menus.push(menuItem.key);
            }
          }
          msItems.push({
            title: menuItem.title,
            template: menuItem,
            id: menuItem.id
          })
        })
        msObj.items = msItems;
        menuTemplate.push(msObj)
      })

      items.push(
        <EABMenu
          isAppMenu={this.props.isApp}
          landingIndex={landingMenuIndex}
          checkedMenu={checkedMenu}
          onMenuItemChange = {this.props.onMenuItemChange}
          key={__id}
          template={menuTemplate}
          renderItems={this.renderItems}
          changedValues={changedValues}
          error={error}
          values={values}
          showCondItems={this.showCondItems}
          handleChange={handleChange}
          skipCheck = {true}
        />
      )
    }

    else if (viewType == "TABS"){
      let tabsTemplate = [];

      let addTabs = (subType, tab) => {
        let proposer = changedValues.proposer;
        let insured = changedValues.insured;
        let phPersonalInfo = proposer.personalInfo;
        if (subType.toUpperCase() == "PROPOSER") {
          tabsTemplate.push({
            id: tab.id,
            title: proposer.personalInfo.firstName,
            template: tab,
            validate: () => { return true; }
          })

        } else if (subType.toUpperCase() == "INSURED") {

          for (var la in insured) {
            let _laPersonInfo = insured[la].personalInfo;

            tabsTemplate.push({
              id: _laPersonInfo.cid,
              title: _laPersonInfo.firstName,
              template: tab,
              validate: () => { return true; }
            })

          }
        }

      }

      if (iTemplate.items) {
        let viewSubType = _.get(iTemplate, 'subType', '').toLowerCase();

        let proposer = changedValues.proposer;
        let insured = changedValues.insured;

        if (viewSubType === 'servergenerated') {
          _.forEach(iTemplate.items, (tab, tabIndex) => {
            let subType = tab.subType;
            if (subType.toUpperCase() == 'PROPOSER') {
              tabsTemplate.push({
                id: tab.id,
                title: proposer.personalInfo.firstName,
                template: tab,
                validate: () => { return true; }
              })

            } else if (subType.toUpperCase() == 'INSURED') {
              let proposerTabCount = _.filter(iTemplate.items, (t) => { return _.get(t, 'subType', '').toUpperCase() == 'PROPOSER'; }).length;
              let insuredIndex = proposerTabCount > 0 ? tabIndex - 1: tabIndex;
              let _laPersonInfo = insured[insuredIndex].personalInfo;

              tabsTemplate.push({
                id: _laPersonInfo.cid,
                title: _laPersonInfo.firstName,
                template: tab,
                validate: () => { return true; }
              })
            }
          });
        } else {
          //generate tab here
          iTemplate.items.map((tab, tabIndex) => {
            let subType = tab.subType;
            if (!subType) {
              tabsTemplate.push({
                id: tab.id,
                title: tab.title,
                template: tab,
                validate: () => { return true; }
              })
            } else {
              addTabs(subType, tab);
            }
          })
        }


        items.push(
          <EABTabs
            key={__id}
            template={tabsTemplate}
            changedValues={changedValues}
            error={error}
            values={values}
            showCondItems={this.showCondItems}
            renderItems={this.renderItems}
            skipCheck={true}
            handleChange={onItemChange}
            isApp={this.props.isApp}
          />
        )
      }
    }

    else if (viewType == 'TABLE') {

      items.push(
        <EABTable
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {"ctn_"+ iTemplate.id}
          template = {iTemplate}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error={error}
          changeActionIndex = {this.props.changeActionIndex}
          completeChangedValues = {this.props.completeChangedValues}
          handleChange = {handleChange}
          validate = {this.validate}
          values={values}
          renderItems={this.renderItems}
          ccy={this.props.ccy || 'SGD'}/>
      );
    }

    else if (viewType == 'TABLEVIEW') {
      items.push(
        <EABTableView
          ref={ref=>{this[iTemplate.id]=ref}}
          key = {"ctn_"+ iTemplate.id}
          template = {iTemplate}
          index = {index}
          values = {values}
          changedValues = {changedValues}
          error = {error}
          handleChange = {handleChange}
          renderItems={this.renderItems}/>
      );
    }

    else if (viewType === 'COMMENTBOX') {
      items.push(
        <CommentBlock
          key={__id}
          template={iTemplate} changedValues={changedValues}
        />
      );
    }

    else if (viewType === 'COMMENTREASON'){
      items.push(
        <CommentReason
          key={__id}
          template={iTemplate}
          changedValues={changedValues}
          comment=''
        />
      )
    }

    else if (viewType === 'FILEUPLOAD') {
      items.push(
        <DynColFileUpload
          key={__id}
          template={iTemplate}
          values={changedValues}
          changedValues={changedValues}
          allFilesValues={0}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType === 'PREVIEWOBJ'){
      items.push(
        <DynColFilePreview
          key={__id}
          template={iTemplate}
          values={changedValues}
          changedValues={changedValues}
          allFilesValues={0}
          handleChange={handleChange}
        />
      )
    }

    else if (viewType === 'TEXTONLY'){
      let content;
      let  value = changedValues[iTemplate.id];
      let stampedValue = changedValues[iTemplate.stampedValue];
      if (_.toUpper(iTemplate.subType) === 'APPROVAL_DATE' && value) {
        value =  moment(value).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
      } else if (_.toUpper(iTemplate.subType) === 'EXPIRYDATE' && value && stampedValue) {
        value = moment(stampedValue).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
      } else if (_.toUpper(iTemplate.subType) === 'EXPIRYDATE' && value) {
        value = moment(value).add(_getOr(14, 'sysParameterConstant.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION', app), 'd').utcOffset(MOMENT_TIME_ZONE).format(DateTimeFormat3);
      }
      if (iTemplate.Mapping){
        _.forEach(Object.keys(iTemplate.Mapping), obj=>{
          if (_.split(iTemplate.Mapping[obj], ',').indexOf(changedValues[obj]) > -1){
            content = value
          }
        });
      }  else if (iTemplate.defaultValue) {
        content = iTemplate.defaultValue;
      } else if (_.toLower(iTemplate.subType) === 'currency') {
        value = window.getCurrencyByCcy(value, 'SGD', _.get(iTemplate, 'decimal', 2));
        content = value;
      } else {
        content = (iTemplate.prefix) ? iTemplate.prefix + ' ' + value : value;
      }

      if (iTemplate.DisableMapping) {
        forEach(Object.keys(iTemplate.DisableMapping), obj =>{
          if (iTemplate.DisableMapping[obj].indexOf(changedValues[obj]) > -1){
            content = "";
          }
        });
      }

      let textOnlyClassName;
      if (iTemplate.warning) {
        textOnlyClassName = styles.warningFont + ' ' + styles.tunc800;
      } else if (iTemplate.subType === 'titleOnly') {
        textOnlyClassName = '';
      } else if (iTemplate.subType === 'leftAlign') {
        textOnlyClassName = styles.TableLeftAlignRow;
      }  else if (iTemplate.presentation === 'rightAlignRiderPrem') {
        textOnlyClassName = styles.TableRightAlignRow;
      } else if (iTemplate.onHoldReason) {
        textOnlyClassName = styles.subTextTitle + ' ' + styles.tuncOnHoldReason;
      } else {
        textOnlyClassName = styles.subTextTitle + ' ' + styles.tunc800;
      }

      if (iTemplate.bold) {
        textOnlyClassName += ' ' + styles.bold;
      }

      if (iTemplate.padding12TopBottom) {
        textOnlyClassName += ' ' + styles.padding12TopBottom;
      }

      if (iTemplate.padding12Top) {
        textOnlyClassName += ' ' + styles.padding12Top;
      }

      if (iTemplate.subType === 'titleOnly'){
          content = iTemplate.title;
      }

      // <span style={{paddingRight: '30px'}}>{(iTemplate.presentation === 'rightAlignRiderPrem') ? iTemplate.prefix : undefined}</span>{content}
      items.push(
        <div className={textOnlyClassName} style={{lineHeight: '1.5em', wordWrap: 'break-word'}}>
          {(iTemplate.presentation === 'rightAlignRiderPrem') ? iTemplate.prefix : undefined}
          {content}
        </div>
      )
    }

    else if (viewType === 'DOCACTION') {
      items.push(
        <DocAction
          key={__id}
          template={iTemplate}
          changedValues={changedValues}
          values={changedValues}
        />
      );
    }

    else if (viewType === 'MULTIPLEDISPLAY') {
      items.push(
        <MultipleDisplay
          key={__id}
          template={iTemplate}
          changedValues={changedValues}
          values={changedValues}
        />
      )
    }

    else if (viewType == 'EMPTYDIV'){
      items.push(<div style={{minHeight:iTemplate.minHeight}} ></div>);
    }

    else if (viewType === 'TOOLTIP') {
      items.push(
        <div className={styles.DetailsItem}>
          <EABToolTips
            containerStyle={iTemplate.style}
            hints={iTemplate.title}
            iconType={iTemplate.icon} />
        </div>
      );
    }
  }

  render(){

	  let {
      muiTheme,
      langMap,
      lang
   } = this.context;

    let {
      style,
      title
   } = this.state;

    let {
      template,
      changedValues,
      error,
      values
   } = this.state;

    let { overwideStyle } = this.props;

    let fields = [];
    this.runningId = 0;

    this.renderItems(fields, template, values, changedValues, error, template.id);

    return (
      <div
        className={styles[this.props.customStyleClass]}
        style={Object.assign({display: 'flex', flexDirection: 'column'}, this.props.customStyle)}
        onDrop={(e)=>{e.preventDefault()}}
        onDragOver={(e)=>{e.preventDefault()}}>
        {fields}
      </div>
    );
 }
}

DynColumn.propTypes = Object.assign({}, DynColumn.propTypes, {
  docId: PropTypes.string
})

export default DynColumn;
