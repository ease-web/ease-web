import React, {PropTypes} from 'react';
import {Dialog, Toolbar, ToolbarGroup, IconButton, ToolbarTitle, FlatButton, Checkbox, List, ListItem, Divider} from 'material-ui';
import * as _ from 'lodash';

import styles from '../Common.scss';
import EABInputComponent from '../CustomViews/EABInputComponent';
import {getIcon} from '../Icons/index';

// import {closeEmail} from '../../actions/proposal';

export default class AppSumEmailPreviewDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    emails: PropTypes.array,
    onSend: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      selectedRecipients: {},
      selectedAttachments: {},
      selectedRecipientsFilter: {},
      changed: false
    });
  }

  componentDidMount() {
    this.init(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    let recipients = {};
    let attachments = {};
    let agentDisabled = true, clientDisabled = true;
    let emails = props.emails;
    let {changed, selectedRecipients, selectedRecipientsFilter, selectedAttachments} = this.state;

    for (var i in emails) {
      if (emails[i].type === 'agent' && emails[i].toAddrs.length > 0 && _.join(emails[i].toAddrs) !== '') {
        agentDisabled = false;
        break;
      }
    }

    for (var i in emails) {
      if (emails[i].type === 'client' && emails[i].toAddrs.length > 0 && _.join(emails[i].toAddrs) !== '') {
        clientDisabled = false;
        break;
      }
    }

    _.each(props.emails, (email) => {
      recipients[email.id] = _.join(email.toAddrs).length > 0;
      attachments[email.id] = {};
      _.each(email.attachments, (attachment) => {
        attachments[email.id][attachment.id] = true;
      });
    });

    // _.each(props.emails, (email) => {
    //   recipients[email.id] = true;
    //   attachments[email.id] = {};
    //   _.each(email.attachments, (attachment) => {
    //     attachments[email.id][attachment.id] = true;
    //   });
    // });

    let initChange = window.isEmpty(recipients) && window.isEmpty(attachments);

    let newState = Object.assign({},
      (initChange ? {changed:false} : {}),
      {
        agentDisabled: agentDisabled,
        clientDisabled: clientDisabled
      },
      (changed && !initChange ? {selectedRecipients} : {selectedRecipients: recipients}),
      (changed && !initChange ? {selectedRecipientsFilter} : {
          selectedRecipientsFilter: {
            'agent': !agentDisabled,
            'client': !clientDisabled
          }
        }
      ),
      (changed && !initChange ? {selectedAttachments} : {selectedAttachments: attachments})
    );

    this.setState(newState);
  }

  closeDialog() {
    let {onClose} = this.props;
    onClose();
  }

  sendEmail() {
    const {onSend, emails} = this.props;
    const {selectedRecipients, selectedAttachments} = this.state;
    if (onSend && emails) {
      let emailsToSend = [];
      emails.forEach((email) => {
        if (selectedRecipients[email.id]) {
          var attachmentIds = [];
          if (email.attachments) {
            email.attachments.forEach((attachment) => {
              if (selectedAttachments[email.id][attachment.id]) {
                attachmentIds.push(attachment.id);
              }
            });
          }
          emailsToSend.push({
            id: email.id,
            to: email.toAddrs,
            title: email.subject,
            content: email.content,
            attachmentIds: attachmentIds,
            embedImgs: email.embedImgs,
            quotId:email.quotId,
            appId: email.appId,
            receiverType:email.type
          });
        }
      });
      onSend(emailsToSend);
    } else {
      this.closeDialog();
    }
  }

  getEmailAddrs() {
    const {selectedRecipients} = this.state;
    let emailAddrs = [];
    _.each(selectedRecipients, (value, key) => {
      if (value) {
        emailAddrs.push(key);
      }
    });
    return emailAddrs;
  }

  handleCheckBoxChange=(person, value)=>{
    let {emails} = this.props;
    let {selectedRecipients, selectedRecipientsFilter} = this.state;
    selectedRecipientsFilter[person] = value;
    emails.forEach((email)=>{
      if (email.type === person){
          selectedRecipients[email.id] = value;
      }
    });
    this.setState({selectedRecipients, selectedRecipientsFilter, changed:true});
  }

  getRecipientOptions() {
    const {emails} = this.props;
    let {selectedRecipients, selectedRecipientsFilter, agentDisabled, clientDisabled} = this.state;
    let recipientFilters = [];

    recipientFilters.push(
        <Checkbox
            key={'appSumEmailPreview-agent'}
            label={'Agent'}
            checked={selectedRecipientsFilter['agent']}
            disabled={agentDisabled}
            onCheck={(event, value) => {
              this.handleCheckBoxChange('agent', value);
            }}
            style={{ display: 'inline-block'}}
        />
    );
    recipientFilters.push(
        <Checkbox
            key={'appSumEmailPreview-client'}
            label={'Client'}
            checked={selectedRecipientsFilter['client']}
            disabled={clientDisabled}
            onCheck={(event, value) => {
              this.handleCheckBoxChange('client', value);
            }}
            style={{ display: 'inline-block'}}
        />
    );

    return (
      <div className={styles.Card}>
        <div style={{ padding: '16px', display: 'flex' }}>
            {recipientFilters}
        </div>
      </div>
    );
  }

  getAttachmentList(email) {
    const {muiTheme} = this.context;
    const {selectedAttachments} = this.state;
    const {disableAttCheckbox} = this.props;
    if (email.attachments) {
      let items = email.attachments.map((attachment) => {
        let checkbox = (
          <Checkbox
            key={attachment.id}
            disabled={disableAttCheckbox}
            checked={selectedAttachments[email.id][attachment.id]}
            onCheck={(event, value) => {
              selectedAttachments[email.id][attachment.id] = value;
              this.setState({selectedAttachments, changed:true});
            }}
          />
        );
        return (
          <ListItem key={attachment.id} primaryText={attachment.title} leftIcon={getIcon('pdf', muiTheme.palette.primary2Color)} rightToggle={checkbox} />
        );
      });
      return <List style={{ fontWeight: 'bold' }}>{items}</List>;
    }
    return null;
  }

  getEmailContentWithImgs(email) {
    let ret = email.content;
    _.each(email.embedImgs, (value, key) => {
      ret = ret.replaceAll(`<img src="cid:${key}">`, `<img src="data:image/png;base64,${value}">`);
    });
    return ret;
  }

  getEmailList() {
    const {emails} = this.props;
    const {selectedRecipients} = this.state;
    let emailEls = [];
    if (emails) {
      emails.forEach((email) => {
        if (selectedRecipients[email.id]) {
          emailEls.push(
            <div key={'email-' + email.id} className={styles.Card}>
              <div style={{ minHeight: '16px', padding: '16px', fontWeight: 'bold' }}>{_.join(email.toAddrs, ', ')}</div>
              <Divider />
              <div style={{ minHeight: '16px', padding: '16px', fontWeight: 'bold' }}>{email.subject}</div>
              <Divider />
              <div style={{ whiteSpace: 'pre-wrap', padding: '16px' }} dangerouslySetInnerHTML={{__html: this.getEmailContentWithImgs(email)}} />
              <Divider />
              {this.getAttachmentList(email)}
            </div>
          );
        }
      });
    }
    return emailEls;
  }

  render() {
    const {muiTheme, langMap} = this.context;
    let fontColor = muiTheme.palette.primary1Color;
    let emailList = this.getEmailList();
    let title = (
      <div style={{padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this.closeDialog()}>
              {getIcon('close', fontColor)}
            </IconButton>
            <ToolbarTitle text={window.getLocalizedText(langMap, 'email')} style={this.getStyles().toolBarTitle} />
          </ToolbarGroup>
          <ToolbarGroup style={{ display: 'flex', paddingRight: 16, paddingTop: 2}}>
            <FlatButton disabled={emailList.length === 0} label={window.getLocalizedText(langMap, 'email.btn.send')} onTouchTap={() => this.sendEmail()} labelStyle={this.getStyles().toolBarActionFlatBtn} />
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );
    return (
      <Dialog
        open={this.props.open}
        autoScrollBodyContent
        title={title}
      >
        {this.getRecipientOptions()}
        {emailList}
      </Dialog>
    );
  }

}
