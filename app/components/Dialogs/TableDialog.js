import React, { PropTypes } from 'react';
import { Dialog, RaisedButton, Toolbar, ToolbarGroup, IconButton } from 'material-ui';
import styles from '../Common.scss';
import {getIcon} from '../Icons/index';
import EABInputComponent from '../CustomViews/EABInputComponent';

export default class ConfirmDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    className: PropTypes.string,
  };

  render() {
    const {muiTheme, langMap} = this.context;
    const {open, onClose, className} = this.props;
    let fontColor = muiTheme.palette.primary1Color;

    let titleBar = (
        <div style={{ padding: 0, margin: 0, background: '#FAFAFA' }}>
          <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
            <ToolbarGroup style={{ paddingLeft: '16px' }}>
              <IconButton onTouchTap={() => onClose && onClose()}>
                {getIcon('close', fontColor)}
              </IconButton>
            </ToolbarGroup>
          </Toolbar>
          <div className={styles.Divider} />
        </div>
      );
    return (
      <Dialog
        autoScrollBodyContent
        repositionOnUpdate={false}
        autoDetectWindowHeight={false}
        modal={false}
        bodyStyle={{ overflow: 'auto', padding: 0 }}
        open={open}
        contentClassName={className}
        title={titleBar}
      >
      <table className={styles.ToolTipTable}>
        <thead>
          <tr>
            <th>{window.getLocalizedText(langMap, 'quotation.tooltip.table.riskprofilemodel')}</th>
            <th>{window.getLocalizedText(langMap, 'quotation.tooltip.table.suggestminhorizon')}</th>
          </tr>
        </thead>
        <tbody>
        <tr className={styles.hightlight}>
          <td>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.1')}</td>
          <td>3 years</td>

        </tr>
        <tr style={{background: 'white'}}>
          <td>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.2')}</td>
          <td>5 years</td>
        </tr>
        <tr >
          <td>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.3')}</td>
          <td>7 years</td>
        </tr>
        <tr style={{background: 'white'}}>
          <td>{window.getLocalizedText(langMap, 'quotation.fund.riskProfile.4')}</td>
          <td>10 years</td>
        </tr>
        </tbody>
      </table>

      <p style= {{textAlign:'left', color:fontColor, padding: '0px 30px 0px 30px'}}>
      When using the model portfolio allocations, you should consider adhering to the minimum investment time horizon to minimise the likelihood of a loss to less than 10%
      </p>
        <div style={{textAlign: 'center', color: fontColor, padding: '10px'}}>
            <RaisedButton
              primary
              label={window.getLocalizedText(langMap, 'app.ok')}
            onTouchTap={() => onClose && onClose()}
            />
        </div>
      </Dialog>
    );
  }

}
