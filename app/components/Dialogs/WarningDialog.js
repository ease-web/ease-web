import React, { PropTypes } from 'react';
import { Dialog, FlatButton } from 'material-ui';

import EABComponent from '../Component';

export default class WarningDialog extends EABComponent {

  static propTypes = {
    open: PropTypes.bool,
    isError: PropTypes.bool,
    msg: PropTypes.string,
    msgElement: PropTypes.element,
    onClose: PropTypes.func,
    hasTitle: PropTypes.bool
  };

  static defaultProps = {
    isError: false,
    hasTitle: true
  };

  render() {
    const {langMap} = this.context;
    const {open, isError, msg, onClose, 
      style,
      hasTitle, msgElement, title} = this.props;
    return (
      <Dialog
        title={hasTitle ? window.getLocalizedText(langMap, isError ? 'app.error' : 'app.warning', isError ? 'ERROR' : 'WARNING') : (title || "")}
        modal
        actions={[
          <FlatButton
            label={window.getLocalizedText(langMap, 'app.ok', 'OK')}
            primary
            onTouchTap={(e) => onClose && onClose(e)}
          />
        ]}
        open={!!open}
        onRequestClose={(e) => onClose && onClose(e)}
        style={style}
      >
        {msg ? msg : msgElement}
      </Dialog>
    );
  }

}
