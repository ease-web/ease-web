import React from 'react';
import * as _ from 'lodash';
import EABInputComponent from '../CustomViews/EABInputComponent';
import {Toolbar, ToolbarGroup, IconButton, ToolbarTitle, FlatButton, Dialog, Checkbox, Divider} from 'material-ui';
import styles from '../Common.scss';
import {getIcon} from '../Icons/index';
import {closeAppSumEmail, openEmail} from '../../actions/application';
import SystemConstants from '../../constants/SystemConstants';
import CommonFunc from '../../CommonFunctions';

export default class AppSumEmailDialog extends EABInputComponent {

    constructor(props){
        super (props);
        this.state = Object.assign({}, this.state, {
            appList:[],
            selectedApps:{},
            selectedAttachments:{},
            attKeysMap: [],
            quotIds:{} // key value pair, key is quotId/appId, value is quotId
        });
    }

    componentDidMount(){
        this.init(this.props);
    }

    componentWillReceiveProps(props){
        this.init(props);
    }

    init(props){
        let {appList, attKeysMap} = props;
        let apps = {};
        let atts = {};
        let quotIds = {};
        if (appList && typeof appList !== 'undefined'){
            appList.forEach((item)=>{
                if (item.type === SystemConstants.DOCTYPE.QUOTATION){
                    quotIds[item.id] = item.id;
                } else if (item.type === SystemConstants.DOCTYPE.APP || item.type === SystemConstants.DOCTYPE.MASTERAPP){
                    quotIds[item.id] = item.quotation.id;
                }
                let attKeys = this.getAttKeys(item.baseProductCode, attKeysMap);
                apps[item.id] = false;
                atts[item.id] = {};
                for (let i in attKeys) {
                    atts[item.id][attKeys[i].id] = false;
                }
            });
        }
        this.setState({
            selectedApps: apps,
            selectedAttachments: atts,
            quotIds: quotIds,
            open: open,
            attKeysMap: attKeysMap,
        });
    }

    getAttKeys = (productCode, attKeysMap=[]) => {
        for (let i in attKeysMap) {
            if (attKeysMap[i].productCode && attKeysMap[i].productCode.indexOf(productCode) !== -1) {
                return (attKeysMap[i].attKeys);
                break;
            }
        }
    }

    closeDialog = () => {
        closeAppSumEmail(this.context);
    }

    onPreviewBtnTap = () => {
        let {selectedApps, selectedAttachments, quotIds} = this.state;
        for (var key in selectedApps){
            if (selectedApps[key] === true) {
                let hasAttSelected = false;
                for (var attKey in selectedAttachments[key]){
                    if (selectedAttachments[key][attKey] == true){
                        hasAttSelected = true;
                    }
                }
                if (hasAttSelected === false){
                    selectedApps[key] = false;
                } else {
                    openEmail(this.context, selectedApps, selectedAttachments, quotIds);
                }
            }
        }
        this.closeDialog();
    }

    getToolbarView = () => {
        const {muiTheme, langMap} = this.context;
        let {selectedApps, selectedAttachments} = this.state;
        let fontColor = muiTheme.palette.primary1Color;
        return(
            <div style={{padding: 0, margin: 0, background: '#FAFAFA' }}>
                <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
                <ToolbarGroup style={{ paddingLeft: '16px' }}>
                    <IconButton onTouchTap={() => this.closeDialog()}>
                    {getIcon('close', fontColor)}
                    </IconButton>
                    <ToolbarTitle text={window.getLocalizedText(langMap, 'application.email')} style={this.getStyles().toolBarTitle} />
                </ToolbarGroup>
                <ToolbarGroup style={{ display: 'flex', paddingRight: 16, paddingTop: 2}}>
                    <FlatButton label={window.getLocalizedText(langMap, 'application.email.preview')} onTouchTap={this.onPreviewBtnTap} labelStyle={this.getStyles().toolBarActionFlatBtn} />
                </ToolbarGroup>
                </Toolbar>
                <div className={styles.Divider} />
            </div>
        );
    }

    getAppsList = () => {
        const {muiTheme, lang} = this.context;
        let {appList} = this.props;
        let {selectedApps} = this.state;
        let appsView = [];
        let covName = '';
        let nameLabel, iName, pName, id, policyNumber;
        if (appList && typeof appList != 'undefined'){
            appList.forEach((item, index)=>{
                iName = item.iName;
                pName = item.pName;
                id = item.id;
                policyNumber = item.policyNumber;
                covName = item.baseProductName;
                if (item.plans && item.plans[0]) {
                  covName = item.plans[0].covName;
                }
                nameLabel = CommonFunc.getNameListStringForAllInsured(item);
                appsView.push(
                    <div key={'appsView-' + index}
                        style={{
                        widht: '100%',
                        lineHeight: '24px',
                        padding: '8px ' + muiTheme.spacing.desktopGutterLess +'px 0 '+'8px',
                        position: 'relative'
                        }}>
                        <Divider />
                        <div style={{display:'inline-block', verticalAlign:'top'}}>{window.getLocalText(lang, covName)}{' (' + (policyNumber || id) + ')'}</div>
                        <div style={{color:'gray', fontSize:'14px', marginRight: '42px'}}>{nameLabel}</div>
                        <div style={{
                            position:'absolute',
                            right:12, top: 24, zIndex:10}}>
                            <Checkbox
                                checked={selectedApps[item.id]}
                                onCheck={(event, value)=>{
                                    selectedApps[item.id] = value;
                                    this.setState({});
                                }}/>
                        </div>
                        <Divider />
                    </div>
                );
                appsView.push(this.getAttachmentsList(item));
            });
        }
        return appsView;
    }

    getAttachmentsList = (app) => {
        let {selectedApps, selectedAttachments, attKeysMap} = this.state;
        if (selectedApps[app.id] == true) {
            const {muiTheme} = this.context;
            let attachmentsView = [];

            let attachmentFunc = function(attId, attName){
                selectedAttachments[app.id][attId] = true;
                return (
                    <div key={'attachmentsView-' + app.id + '-' + attId}
                        className={styles.PanelBackgroundColor}
                        style={{
                            widht: '100%',
                            height: muiTheme.spacing.desktopSubheaderHeight,
                            lineHeight: muiTheme.spacing.desktopSubheaderHeight+'px',
                            padding: '8px '+ muiTheme.spacing.desktopGutterLess +'px 0 ' + muiTheme.spacing.desktopGutter+'px',
                            position: 'relative'
                            }}>
                        <div style={{display:'inline-block', verticalAlign:'top'}}>{attName}</div>
                        <div style={{
                            position:'absolute',
                            right:12, top: 20, zIndex:10}}>
                            <Checkbox
                                defaultChecked={true}
                                onCheck={(event,value)=>{
                                    selectedAttachments[app.id][attId] = value;
                                }}/>
                        </div>
                    </div>
                );
            }

            let productCode = app.baseProductCode;
            let attKeys = this.getAttKeys(productCode, attKeysMap);

            for (let i in attKeys) {
                attachmentsView.push(attachmentFunc(attKeys[i].id, attKeys[i].title));
            }
            return (attachmentsView);
        } else {
            return null;
        }
    }

    render(){
        return (
            <Dialog
                open={this.props.open}
                autoScrollBodyContent
                title={this.getToolbarView()}
            >
                {this.getAppsList()}
            </Dialog>
        );
    }
}
