import React, {PropTypes} from 'react';
import {Dialog, Toolbar, ToolbarGroup, IconButton, ToolbarTitle, FlatButton, Checkbox, List, ListItem, Divider} from 'material-ui';
import * as _ from 'lodash';

import styles from '../Common.scss';
import EABInputComponent from '../CustomViews/EABInputComponent';
import {getIcon} from '../Icons/index';

export default class EmailDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    emails: PropTypes.array,
    onSend: PropTypes.func,
    onClose: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      selectedRecipients: {},
      selectedAttachments: {}
    });
  }

  componentDidMount() {
    this.init(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.open, nextProps.open) || !_.isEqual(this.props.emails, nextProps.emails)) {
      this.init(nextProps);
    }
  }

  init(props) {
    let recipients = {};
    let attachments = {};
    _.each(props.emails, (email) => {
      recipients[email.id] = _.join(email.toAddrs).length > 0;
      attachments[email.id] = {};
      _.each(email.attachments, (attachment) => {
        attachments[email.id][attachment.id] = true;
      });
    });
    this.setState({
      selectedRecipients: recipients,
      selectedAttachments: attachments
    });
  }

  closeDialog() {
    let {onClose} = this.props;
    // closeEmail(this.context);
    onClose();
  }

  sendEmail() {
    const {onSend, emails} = this.props;
    const {selectedRecipients, selectedAttachments} = this.state;
    if (onSend && emails) {
      let emailsToSend = [];
      emails.forEach((email) => {
        if (selectedRecipients[email.id]) {
          var attachmentIds = [];
          if (email.attachments) {
            email.attachments.forEach((attachment) => {
              if (selectedAttachments[email.id][attachment.id]) {
                attachmentIds.push(attachment.id);
              }
            });
          }
          emailsToSend.push({
            id: email.id,
            to: email.toAddrs,
            title: email.subject,
            content: email.content,
            attachmentIds: attachmentIds,
            embedImgs: email.embedImgs
          });
        }
      });
      onSend(emailsToSend);
    } else {
      this.closeDialog();
    }
  }

  getRecipientOptions() {
    const {emails} = this.props;
    const {selectedRecipients} = this.state;
    let recipientEls = _.map(emails, (email) => {
      return (
        <Checkbox
          key={'email-' + email.id}
          label={email.name}
          disabled={_.join(email.toAddrs).length === 0}
          checked={selectedRecipients[email.id]}
          onCheck={(event, value) => {
            selectedRecipients[email.id] = value;
            this.setState({});
          }}
          style={{ display: 'inline-block' }}
        />
      );
    });
    return (
      <div className={styles.Card}>
        <div style={{ padding: '16px', display: 'flex' }}>
          {recipientEls}
        </div>
      </div>
    );
  }

  getAttachmentList(email) {
    const {muiTheme} = this.context;
    const {selectedAttachments} = this.state;
    const {disableAttCheckbox} = this.props;
    if (email.attachments) {
      let items = email.attachments.map((attachment) => {
        let checkbox = (
          <Checkbox
            key={attachment.id}
            disabled={disableAttCheckbox}
            checked={selectedAttachments[email.id][attachment.id]}
            onCheck={(event, value) => {
              selectedAttachments[email.id][attachment.id] = value;
              this.setState({});
            }}
          />
        );
        return (
          <ListItem key={attachment.id} primaryText={attachment.title} leftIcon={getIcon('pdf', muiTheme.palette.primary2Color)} rightToggle={checkbox} />
        );
      });
      return <List style={{ fontWeight: 'bold' }}>{items}</List>;
    }
    return null;
  }

  getEmailContentWithImgs(email) {
    let ret = email.content;
    _.each(email.embedImgs, (value, key) => {
      ret = ret.replaceAll(`src='cid:${key}'`, `src="data:image/png;base64,${value}"`);
      ret = ret.replaceAll(`src="cid:${key}"`, `src="data:image/png;base64,${value}"`);
    });
    return ret;
  }

  getEmailList() {
    const {emails} = this.props;
    const {selectedRecipients} = this.state;
    let emailEls = [];
    if (emails) {
      emails.forEach((email) => {
        if (selectedRecipients[email.id]) {
          emailEls.push(
            <div key={'email-' + email.id} className={styles.Card}>
              <div style={{ minHeight: '16px', padding: '16px', fontWeight: 'bold' }}>{_.join(email.toAddrs, ', ')}</div>
              <Divider />
              <div style={{ minHeight: '16px', padding: '16px', fontWeight: 'bold' }}>{email.subject}</div>
              <Divider />
              <div style={{ whiteSpace: 'pre-wrap', padding: '16px' }} dangerouslySetInnerHTML={{__html: this.getEmailContentWithImgs(email)}} />
              <Divider />
              {this.getAttachmentList(email)}
            </div>
          );
        }
      });
    }
    return emailEls;
  }

  render() {
    const {muiTheme, langMap} = this.context;
    let fontColor = muiTheme.palette.primary1Color;
    let emailList = this.getEmailList();
    let title = (
      <div style={{padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this.closeDialog()}>
              {getIcon('close', fontColor)}
            </IconButton>
            <ToolbarTitle text={window.getLocalizedText(langMap, 'email')} style={this.getStyles().toolBarTitle} />
          </ToolbarGroup>
          <ToolbarGroup style={{ display: 'flex', paddingRight: 16, paddingTop: 2}}>
            <FlatButton disabled={emailList.length === 0} label={window.getLocalizedText(langMap, 'email.btn.send')} onTouchTap={() => this.sendEmail()} labelStyle={this.getStyles().toolBarActionFlatBtn} />
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );
    return (
      <Dialog
        open={this.props.open}
        autoScrollBodyContent
        title={title}
      >
        {this.getRecipientOptions()}
        {emailList}
      </Dialog>
    );
  }

}
