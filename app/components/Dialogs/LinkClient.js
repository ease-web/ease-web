import React from 'react';
import PropTypes from 'prop-types';
import {TextField, Toolbar, ToolbarTitle, ToolbarGroup, IconButton, FlatButton, Card} from 'material-ui';

import EABTextField from '../CustomViews/TextField.js';
import EABPickerField from '../CustomViews/PickerField';
import EABAvatar from '../CustomViews/Avatar';
import EABDatePickerField from '../CustomViews/DatePickerField';

import * as _ from 'lodash';
import {getFamilyProfile} from '../../actions/client';
import * as _c from '../../utils/client';

import {getIcon} from '../Icons/index';

import EABComponent from '../Component';
import styles from '../Common.scss';

class ContactItem extends EABComponent {

  render(){
    let {lang, store} = this.context;
    let {template} = store.getState().client;
    let {item} = this.props;
    let {id, mobileNo='', idDocType='', idDocTypeOther='', idCardNo='', email='', fullName=''} = item;
    let idDocTypeTitle = idDocTypeOther || _c.getIdDocTypeTitle(template.editDialog, item, lang);
    let subFontColor = "DDDDDD";
    let subRowStyle = {color:subFontColor, fontSize: '12px', lineHeight: '16px'};
    let initial = _c.getAvatarInitial(item);

    return (
      <div style={{display: 'flex', padding: '16px 0px', cursor: 'pointer'}} onTouchTap={()=>{this.props.onItemClick(id)}}>
        <div style={{flexBasis: 84, display: 'flex', justifyContent: 'center'}}>
          <EABAvatar width={56} docId={id} changedValues={{photo: 'photo', initial}} values={{photo: 'photo', initial}} template={{id: 'photo', type: 'photo'}} disabled icon={getIcon("person", "#FFFFFF")}/>
        </div>
        <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
          <div style={{fontSize: '14px', lineHeight: '18px'}}>{fullName}</div>
          {idDocType?<div style={subRowStyle}>{idDocTypeTitle}: {idCardNo}</div>:null}
          <div style={subRowStyle}>{mobileNo}</div>
        </div>
      </div>
    )
  }
}


class Main extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        searchValue: props.searchValue || '',
        viewAll: props.viewAll || false
      })
  };

  componentWillReceiveProps(nextProps) {
    let newState = {};

    if(!isEqual(this.state.searchValue, nextProps.searchValue)){
      newState.searchValue = nextProps.searchValue;
    }

    if(!isEqual(this.state.viewAll, nextProps.viewAll)){
      newState.viewAll = nextProps.viewAll;
    }

    if (!isEmpty(newState) || newState.searchValue === '') {
      this.setState(newState);
    }
  }

  renderItems = () =>{
    let items = [];
    let client = this.context.store.getState().client;

    let {filterDependants} = this.props;

    let {contactList, profile} = client;
    if(!contactList || !profile)
      return;

    let {viewAll, searchValue} = this.state
    let _contactList = _.cloneDeep(contactList);
    let searchValues = _.toUpper(this.state.searchValue).split(" ").filter(text=>!isEmpty(_.trim(text)));

    let contactCnt = 0;
    let contacts = _.orderBy(_contactList, [contact=>_.toUpper(contact.fullName)], 'asc');
    _.forEach(contacts, (item, index)=>{
      if (contactCnt > 50) {
        return; // temp fix: show top 50 only, better use something like virtual scroll
      }
       let {id, idCardNo='', fullName='', mobileNo='', email=''} = item;
       if(id !== profile.cid && (!filterDependants || _.findIndex(profile.dependants, dependant=>dependant.cid==id)==-1)){
          let found = false;
          _.forEach(searchValues, _searchValue=>{
            if(_.toUpper(idCardNo).indexOf(_searchValue)>-1 || _.toUpper(fullName).indexOf(_searchValue)>-1 ||
                _.toUpper(mobileNo).indexOf(_searchValue)>-1 || _.toUpper(email).indexOf(_searchValue)>-1){
                  found = true;
                }
          })

          if(viewAll || (searchValue.length>1 && found)){
              if(!isEmpty(items)){
                items.push(
                  <div key={"search-contact-divider-"+index} style={{width: 'calc(100% - 84px)', marginLeft: '84px', height: '2px', background: '#DDDDDD'}}/>
                )
              }
              items.push(
                <ContactItem
                  key={"search-contact-"+item.id}
                  item={item}
                  onItemClick={(cid)=>{
                    getFamilyProfile(this.context, cid, (profile)=>{
                      this.props.onItemClick(profile)
                    })
                  }}
                />
              )
              contactCnt++;
            }
       }
    })

    return items;
  }



  render() {
    let {
      muiTheme,
      router,
      langMap
    } = this.context;

    let items = this.renderItems();

    return (
      <div>
        <div className={styles.Form} style={{width: 'calc(100% - 48px)', padding: '24px', height: '400px'}}>
          {items}
        </div>
      </div>
    );
  }
}

export default Main;
