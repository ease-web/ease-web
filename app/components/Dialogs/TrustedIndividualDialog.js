import React from 'react';
import {TextField, Dialog, Divider, Toolbar, ToolbarTitle, ToolbarGroup, IconButton, FlatButton} from 'material-ui';

import LinkClient from './LinkClient';
import DynColumn from '../DynViews/DynColumn';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';
import {getIcon} from '../Icons/index';

import {
  saveTrustedIndividual,
  deleteTrustIndividual
} from '../../actions/client';

import {getImage} from '../../actions/GlobalActions';

import EABInputComponent from '../CustomViews/EABInputComponent';
import styles from '../Common.scss';

class Main extends EABInputComponent {
  constructor(props, context) {
      super(props);
      let {values={}} = props;
      let {tiTemplate: template} = context.store.getState().client.template;

      this.state = Object.assign({}, this.state, {
        runningId: 0,
        pageIndex: 0,
        valid: false,
        values,
        changedValues: _.cloneDeep(values),
        error: {},
        showDeleteDialog: false,
        showUpdTIDialog: false,
        template,
        searchValue: '',
        viewAll: false
      });
  };

  openDialog=(value={})=>{
    let newState = {open:true, valid: false, pageIndex: 0, showDeleteDialog: false, showUpdTIDialog: false};
    newState.values = value;
    newState.changedValues = _.cloneDeep(value)

    if(isEmpty(value)){
      newState.showDelete = false;
    }else{
      newState.showDelete = true;
    }

    this.setState(newState);
  }

  closeDialog=()=>{
    this.setState({open: false, showDeleteDialog: false, showUpdTIDialog: false});
  }

  componentWillReceiveProps(nextProps){
    let newState = {
      valid: false
    }

    this.setState(newState)
  }

  validate=(skipChanges)=>{
    let {validRefValues, langMap, optionsMap} = this.context;
    let {changedValues, values, template} = this.state;
    let error = {};
    let valid = (skipChanges || !isEqual(changedValues, values)) && _v.exec(template, optionsMap, langMap, changedValues, changedValues, validRefValues, error);
    this.setState({valid, error});
    return valid;
  }

  submit=(confirm)=>{

    let {
      onSubmit
    } = this.props;

    saveTrustedIndividual(
      this.context,
      this.state.changedValues,
      confirm,
      (code)=>{
        if(code){
          this.setState({
            errorCode: code,
            showUpdTIDialog: true
          })
        }
        else {
          this.closeDialog();
          if(typeof onSubmit == "function"){
            onSubmit();
          }
        }
      }
    )
  }

  handleChange = (id, value) =>{
    this.state.changedValues[id] = value;
    this.validate();
  }

  getValueFromProfile=(tiProfile)=>{
    let {store} = this.context;
    let {profile} = store.getState().client;
    let {template} = this.state;

    let newValue = {cid: tiProfile.cid};
    this.getValues(tiProfile, profile, newValue, template);

    this.setState({values: _.cloneDeep(newValue), changedValues: _.cloneDeep(newValue), valid: true, pageIndex: 0});
  }

  getValues=(tiProfile, profile, newValue, template)=>{
    let items = _.at(template, 'items')[0];
    _.forEach(items, item=>{
      //loop if template has items
      this.getValues(tiProfile, profile, newValue, item);
      let {id} = item;
      if(id === 'relationship'){
        let {dependants} = profile;
        let dependant = dependants.find(dependant=>dependant.cid === tiProfile.cid);
        if(!isEmpty(dependant))
          newValue[id] = dependant.relationship;
      }
      else if(id === 'tiPhoto' && profile.photo){
        newValue[id]=tiProfile.cid;
      }
      else{
        newValue[id] = tiProfile[id];
      }
    })
  }

  generatePage0ToolBar() {
    let {muiTheme, store, langMap} = this.context;
    let _style = {paddingLeft: '16px'};
    let iconColor = muiTheme.palette.alternateTextColor;

    let {error} = this.state;
    let disabled = _.isEmpty(error) || !!error.code;

    return  <Toolbar className= {styles.Toolbar} style={{background: '#FAFAFA', boxShadow: 'none'}}>
        <ToolbarGroup style={_style}>
          <IconButton onTouchTap={()=>{this.closeDialog()}}>
            {getIcon("close", iconColor)}
          </IconButton>
          <ToolbarTitle text="Trusted individual" style={{color: iconColor, paddingLeft: '16px' }}/>
        </ToolbarGroup>
        <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
          {
            <FlatButton
              label={"PICK"}
              labelStyle={this.getStyles().LabelinButton}
              onTouchTap={()=>{this.setState({pageIndex:1});}}
            />
          }
          <FlatButton
            label={"SAVE"}
            labelStyle={(disabled) ? this.getStyles().LabelinDisabledButton : this.getStyles().LabelinButton}
            style={{margin: 0}}
            onTouchTap={()=>{this.submit()}}
            disabled={disabled}
          />
        </ToolbarGroup>
      </Toolbar>
    }

  generatePage1ToolBar() {
    let {
      muiTheme,
      router,
      langMap
    } = this.context;

    let _style = {
      paddingLeft: '16px',
      flex: 1
    }
    let iconColor = muiTheme.palette.alternateTextColor;

    let searchBar = (
      <TextField
        id="search"
        key="searchBar"
        ref="searchBar"
        hintText={getLocalizedText(langMap, "search.contact.hintText", "Please Enter number")}
        fullWidth={true}
        value={this.state.searchValue}
        onChange={(e)=>{
            this.setState({searchValue: e.target.value, viewAll: false});
        }}
      />
     );

    return <Toolbar className= {styles.Toolbar} style={{background: '#FAFAFA', boxShadow: 'none'}}>
      <ToolbarGroup style={_style}>
        <IconButton onTouchTap={()=>{
          this.setState({pageIndex: 0})
        }}>
          {getIcon("back", iconColor)}
        </IconButton>
        <ToolbarTitle text={searchBar}style={{paddingLeft: '16px', flex: 1}}/>
      </ToolbarGroup>
      <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
        {!isEmpty(this.state.searchValue)?
          <FlatButton
            label={"CLEAR"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({searchValue: "" })}}
          />:
          <FlatButton
            label={"VIEW ALL"}
            style={{margin: 0}}
            labelStyle={{ fontWeight: 'bold', color: iconColor }}
            onTouchTap={()=>{this.setState({viewAll: true})}}
          />
        }
      </ToolbarGroup>
    </Toolbar>
  }

  generatePage1Content(sV, viewAll){
    return <LinkClient
      key="dialog-link-client"
      onBack={()=>{
        this.setState({pageIndex: 0})
      }}
      searchValue={sV}
      viewAll={viewAll}
      onItemClick={(profile)=>{
        this.getValueFromProfile(profile);
        this.validate(true);
      }}/>
  }


  render() {
    let {muiTheme, store, langMap, lang} = this.context;
    let {profile} = store.getState().client;
    let {template, open, valid, values, changedValues, error, showDelete, showDeleteDialog, showUpdTIDialog, errorCode} = this.state;

    const updTIActions = [
      (
        <FlatButton
          key="profileDialog-updTI-cancel-btn"
          label={getLocalizedText(langMap, "BUTTON.CANCEL", "Cancel")}
          onTouchTap={()=>{this.setState({showUpdTIDialog: false})}}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updFM-confirm-btn"
          label={getLocalizedText(langMap, "BUTTON.CONFIRM", "Confirm")}
          onTouchTap={()=>{
            this.submit(true);
          }}
        />
      )
    ]

    const deleteActions = [
      (
        <FlatButton
          key="tiDialog-delete-confirm-btn"
          label={getLocalizedText(langMap, "BUTTON.YES", "Yes")}
          onTouchTap={()=>{
            this.setState({error: {}});
            deleteTrustIndividual(this.context, ()=>{
              this.closeDialog();
              if(_.isFunction(this.props.onDelete)){
                this.props.onDelete();
              }
            });
          }}
        />
      ),
      (
        <FlatButton
          key="tiDialog-delete-cancel-btn"
          label={getLocalizedText(langMap, "BUTTON.NO", "No")}
          onTouchTap={()=>{this.setState({showDeleteDialog: false})}}
        />
      )
    ]
    let deleteButton=(
      <div style={{width: '100%', textAlign: 'right', paddingRight: '24px'}}>
        <Dialog open={showDeleteDialog} actions={deleteActions}>
          {getLocalizedText(langMap, "TIDIALOG.DELETE_MSG", "Do you want to clear all data of trusted individual?")}
        </Dialog>
        <FlatButton
          label={"CLEAR"}
          labelStyle={{ fontWeight: 'bold', color: "#FF0000" }}
          onTouchTap={()=>{this.setState({showDeleteDialog: true})}}
        />
      </div>
    )


    let page0 = (
      <div key="page0">
        <Dialog open={showUpdTIDialog} actions={updTIActions}>
          {_v.getErrorMsg(null, errorCode, lang)}
        </Dialog>
        <DynColumn
          template={template}
          values={values}
          changedValues={changedValues}
          error={error}
          validate={this.validate}
          docId={profile.cid}
        />
          {showDelete?deleteButton:null}
      </div>
    )

    return (
      <Dialog open={!!open} className={styles.FixWrongDialogPadding} bodyClassName={(this.state.pageIndex===0) ? styles.TrustIndividualDialog : styles.LinkProfileDialog}
      title={(this.state.pageIndex===0) ?
          this.generatePage0ToolBar()
          :
          this.generatePage1ToolBar()
          } autoScrollBodyContent={true}  autoDetectWindowHeight={true}>
        {this.state.pageIndex===0?page0:this.generatePage1Content(this.state.searchValue, this.state.viewAll)}
      </Dialog>
    );
  }
}

export default Main;
