import React, { Component, PropTypes } from 'react';

export default class EABComponent extends Component{
    static contextTypes = {
        router: PropTypes.object,
        store: PropTypes.object,
        muiTheme: PropTypes.object,
        appbar: PropTypes.object,
        updateSearchValue: PropTypes.func,
        updateAppbar: PropTypes.func,
        initAppbar: PropTypes.func,
        closePage: PropTypes.func,
        langMap: PropTypes.object,
        lang: PropTypes.string,
        goStep: PropTypes.func,
        stepperIndex: PropTypes.number,
        updateStepper: PropTypes.func,
        pageOpen: PropTypes.bool,
        optionsMap: PropTypes.object,
        validRefValues: PropTypes.object,
        goTab: PropTypes.func,
        focusSearchValue: PropTypes.func
    }
}