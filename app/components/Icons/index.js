import React from 'react';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import BackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import UseIcon from 'material-ui/svg-icons/action/assignment';
import UploadIcon from 'material-ui/svg-icons/file/file-upload';
import EditIcon from 'material-ui/svg-icons/image/edit';
import SaveIcon from 'material-ui/svg-icons/content/save';
import CancelIcon from 'material-ui/svg-icons/navigation/cancel';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import SettingIcon from 'material-ui/svg-icons/action/settings';
import AddIcon from 'material-ui/svg-icons/content/add';
import AddCircleOutlineIcon from 'material-ui/svg-icons/content/add-circle';
import RemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle';
import RemoveIcon from 'material-ui/svg-icons/content/remove';
import WorkbenchIcon from 'material-ui/svg-icons/action/assignment-turned-in';
import SignOutIcon from 'material-ui/svg-icons/action/power-settings-new';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import PersonIcon from 'material-ui/svg-icons/social/person';
import AddPersonIcon from 'material-ui/svg-icons/social/person-add';
import SearchIcon from 'material-ui/svg-icons/action/search';
import PhoneIcon from 'material-ui/svg-icons/communication/phone';
import MailIcon from 'material-ui/svg-icons/communication/email';
import DirectionIcon from 'material-ui/svg-icons/maps/directions';
import FolderIcon from 'material-ui/svg-icons/file/folder';
import PreviousIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import NextIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import OrderIcon from 'material-ui/svg-icons/action/reorder';
import CheckCircleIcon from 'material-ui/svg-icons/action/check-circle';
import InfoIcon from 'material-ui/svg-icons/action/info';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import LensIcon from 'material-ui/svg-icons/image/lens';
import PDFIcon from 'material-ui/svg-icons/image/picture-as-pdf';
import DescriptionIcon from 'material-ui/svg-icons/action/description';
import ImageIcon from 'material-ui/svg-icons/image/image';
import ContentPasteIcon from 'material-ui/svg-icons/content/content-paste';
import BuildIcon from 'material-ui/svg-icons/action/build';
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import ExpandLessIcon from 'material-ui/svg-icons/navigation/expand-less';
import FirstIcon from 'material-ui/svg-icons/navigation/first-page';
import LastIcon from 'material-ui/svg-icons/navigation/last-page';

export const getIcon = (iconName, iconColor, style = {}) => {
  switch (iconName) {
    case 'menu':
      return <MenuIcon color={iconColor} style={ style }/>
    case 'back':
      return <BackIcon color={iconColor} style={ style }/>
    case 'close':
      return <CloseIcon color={iconColor} style={ style }/>
    case 'edit':
      return <EditIcon color={iconColor} style={ style }/>
    case 'people':
      return <PeopleIcon color={iconColor} style={ style }/>
    case 'setting':
      return <SettingIcon color={iconColor} style={ style }/>
    case 'add':
      return <AddIcon color={iconColor} style={ style }/>
    case 'remove':
      return <RemoveIcon color={iconColor} style={ style }/>
    case 'workbench':
      return <WorkbenchIcon color={iconColor} style={ style }/>
    case 'logout':
      return <SignOutIcon color={iconColor} style={ style }/>
    case 'person':
      return <PersonIcon color={iconColor} style={ style }/>
    case 'addPersion':
      return <AddPersonIcon color={iconColor} style={ style }/>
    case 'search':
      return <SearchIcon color={iconColor} style={ style }/>
    case 'phone':
      return <PhoneIcon color={iconColor} style={ style }/>
    case 'mail':
      return <MailIcon color={iconColor} style={ style }/>
    case 'direction':
      return <DirectionIcon color={iconColor} style={ style }/>
    case 'folder':
      return <FolderIcon color={iconColor} style={ style }/>
    case 'previous':
      return <PreviousIcon color={iconColor} style={ style }/>
    case 'next':
      return <NextIcon color={iconColor} style={ style }/>
    case 'order':
      return <OrderIcon color={iconColor} style={ style }/>
    case 'addCircleOutline':
      return <AddCircleOutlineIcon color={iconColor} style={ style }/>
    case 'RemoveCircleOutline':
      return <RemoveCircleOutline color={iconColor} style={ style }/>
    case 'info':
      return <InfoIcon color={iconColor} style={ style }/>
    case 'checkCircle':
      return <CheckCircleIcon color={iconColor} style={ style }/>
    case 'removeCircle':
      return <RemoveCircle color={iconColor} style={ style }/>;
    case 'delete':
      return <DeleteIcon color={iconColor} style={ style }/>;
    case 'warning':
      return <WarningIcon color={iconColor} style={ style }/>;
    case 'lens'  :
      return <LensIcon color={iconColor} style={ style }/>
    case 'pdf':
      return <PDFIcon color={iconColor} style={ style }/>;
    case 'save':
      return <SaveIcon color={iconColor} style={ style }/>;
    case 'description':
      return <DescriptionIcon color={iconColor} style={ style }/>;
    case 'image':
      return <ImageIcon color={iconColor} style={ style }/>;
    case 'contentPaste':
      return <ContentPasteIcon color={iconColor} style={ style }/>;
    case 'build':
      return <BuildIcon color={iconColor} style={ style }/>;
    case 'expandMore':
      return <ExpandMoreIcon color={iconColor} style={ style }/>;
    case 'expandLess':
      return <ExpandLessIcon color={iconColor} style={ style }/>;
    case 'first':
      return <FirstIcon color={iconColor} style={ style }/>;
    case 'previous':
      return <PreviousIcon color={iconColor} style={ style }/>;
    case 'next':
      return <NextIcon color={iconColor} style={ style }/>;
    case 'last':
      return <LastIcon color={iconColor} style={ style }/>;
    default:
  }
}
