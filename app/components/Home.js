import React, {Component, PropTypes} from 'react';
import {TextField, RaisedButton, Dialog, FlatButton} from 'material-ui';
import Landing from '../components/Pages/Land/index.js';

import {initApp, login, acceptTerm, rejectTerm} from '../actions/home';

const appTheme = require('../theme/appBaseTheme.js');

class Home extends Component {

  static propTypes = {
    app: PropTypes.object,
    inited: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      muiTheme: appTheme.getTheme()
    };
  }

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    };
  }

  componentDidMount() {
    if (!this.context.store.getState().app.inited) {
      initApp(this.context);
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      this.props.app.inited !== nextProps.app.inited ||
      this.props.app.isLogin !== nextProps.app.isLogin ||
      this.props.app.showTerms !== nextProps.app.showTerms ||
      this.props.app.requireSAML !== nextProps.app.requireSAML ||
      !window.isEqual(this.props.app.tncContent, nextProps.app.tncContent) ||
      !window.isEqual(this.props.app.langMap, nextProps.app.langMap)
    );
  }

  login = () => {
    login(this.context, {
      userName: this.refs.un.getValue(),
      password: this.refs.pw.getValue()
    });
  }

  acceptTerm = () => {
    acceptTerm(this.context);
  }

  rejectTerm = () => {
    rejectTerm(this.context);
  }

  render() {
    let {
      isLogin,
      showTerms,
      tncContent,
      langMap,
      inited,
      requireSAML
    } = this.props.app;

    if (isLogin) {
      return <Landing/>
    } else if (showTerms) {
      return <div key="homePage" style={{height:"100vh", width:"100%"}}>
        <Dialog
          style={{width: '100%', minWidth: "400px", height: "80vh"}}
          open={true}
          title={window.getLocalizedText(langMap, 'tnc')}
        >
        <div style={{height: 'calc(100vh - 228px)', overflowY: 'auto'}}>
          <div style={{width: '100%'}} dangerouslySetInnerHTML={{__html: tncContent}}></div>
          <div style={{width: '100%'}}>

            <FlatButton
              primary
              label={window.getLocalizedText(langMap, 'terms_of_use.decline')}
              onTouchTap={this.rejectTerm}
              style={{float:'left'}}
            />
            <RaisedButton
              primary
              label={window.getLocalizedText(langMap, 'terms_of_use.accept')}
              onTouchTap={this.acceptTerm}
              style={{float:'right'}}
            />
          </div>
        </div>
      </Dialog>
      </div>
    } else if (!inited) {
      return <div key="homePage"> Initializing ... </div>
    } else if (requireSAML) {
      return <div key="homePage"> Redirecting ... </div>
    } else {
      return <div key="homePage" style={{height:"100vh", width:"100%", display:"flex", justifyContent:"center", alignItems:"center"}}>
        <div style={{
            width: "256px",
            display: "flex",
            flexDirection: "column"}}>
        <h1>Welcome to EASE</h1>
        <TextField
          ref="un"
          floatingLabelText="User name"
          defaultValue=""
        />
        <TextField
          ref="pw"
          type="password"
          floatingLabelText="Password"
        />
        <RaisedButton
          label={"Login"}
          primary={true}
          onTouchTap={this.login}
        />
        </div>
      </div>
    }
  }
}

Home.contextTypes = {
    router: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired,
    children: PropTypes.array
};

Home.childContextTypes = {
  muiTheme: PropTypes.object
};

export default Home;
