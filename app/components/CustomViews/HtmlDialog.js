import React from 'react';
import PropTypes from 'prop-types';
import {Paper, TextField, RaisedButton, FontIcon,
  Dialog, Toolbar, ToolbarGroup, ToolbarTitle, IconButton} from 'material-ui';
import EABComponent from '../Component';
import appTheme from '../../theme/appBaseTheme';
import styles from '../Common.scss';

class HtmlDialog extends EABComponent {

  onRequestClose(){
    this.props.handleOpen()
  }

  displayHtml(html){
    return html;
  }

  render() {
    let {muiTheme} = this.context
    const _titleStyle = {
      background: muiTheme.baseTheme.palette.primary2Color,
      color: muiTheme.baseTheme.palette.alternateTextColor,
      padding: '15px'
    }
    let {id, open, title, handleOpen, titleStyle= _titleStyle} = this.props;

    let alpha = {listStyleType: 'lower-alpha'}
    let alphastyle={listStyleType: 'lower-alpha'}
    let subTitleStyle={fontWeight:'bold', margin:'15px 0 0 0'}
    let numberStyle={listStyleType: 'decimal', margin: 0}

    if (muiTheme.isMobile) {

      if (!open) return <div key={"dlg-"+id} />

      return (
        <div key={"dlg-" + id}
          className={ styles.FloatingPage }>
          <Toolbar key={"tb-"+id} className={ styles.Toolbar }>
            <ToolbarTitle text={title} style={{width: '100%'}}/>
          </Toolbar>
          <div key={'ctr-'+id}
            className={ styles.Content }
            style={{
              paddingTop: muiTheme.baseTheme.spacing.desktopGutter,
              paddingBottom: muiTheme.baseTheme.spacing.desktopGutter,
              paddingLeft: muiTheme.baseTheme.spacing.desktopGutter,
              paddingRight: 0, overflowY:'auto'
            }}
          >
            <div key={'ctn-'+id} id={"ctn-"+id}
              style={{width: 'calc(100% - 32px)'}}
              dangerouslySetInnerHTML={{__html: self.displayHtml(self.props.content)}} 
            />
          </div>
        </div>
      )
    } else {
      return (
         <Dialog
            key={"dlg-" + id}
            className={styles.Dialog}
            modal={false}
            open={open}
            title={title}
            titleStyle={titleStyle}
            style={{padding: '0px'}}
            overlayStyle={{padding: '0px'}}
            autoScrollBodyContent={true}
            bodyStyle={{paddingTop:"15px"}}
            onRequestClose={handleOpen}
          >
            <div key={'ctn-'+id} id={"ctn-"+id} dangerouslySetInnerHTML={{__html: self.displayHtml(self.props.content)}} />
          </Dialog>
      );
    }
  }
}

HtmlDialog.propTypes = Object.assign({}, HtmlDialog.propTypes, {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleOpen: PropTypes.func.isRequired,
  content: PropTypes.string.isRequired,
  titleStyle: PropTypes.object,
})

export default HtmlDialog;
