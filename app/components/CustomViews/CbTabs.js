import React, { Component, PropTypes}  from 'react';
import mui, {Tabs, Tab, FontIcon, Checkbox} from 'material-ui';
import styles from '../Common.scss';

import EABComponent from '../Component';
import {
  getIcon
} from '../Icons/index';
import * as _ from 'lodash';
import * as _n from '../../utils/needs';

class CbTab extends EABComponent {
  constructor(props) {
    super(props);
  }
  render() {
    let { muiTheme } = this.context;
    const {name, error={}, selected, checked, totalTabs, text, isSigningProcess, onHandleChange, onCbchange} = this.props;
    let warningColor = muiTheme.palette.warningColor;
    return (
      <div className={styles.FlexNoWrapContainer + ' ' +  styles.CBTabs} style={{height: '100%'}}>
        <div className={selected ? styles.SelectedTab + ' CheckBox' : styles.UnSelectedTab + ' CheckBox'} style={{paddingRight: '0px', paddingLeft: '5px'}}>
          <Checkbox checked={checked} onCheck={onCbchange}/>
        </div>
        <div
          key={name}
          className={selected ? styles.SelectedTab : styles.UnSelectedTab}
          onClick={onHandleChange}
          style={Object.assign({}, {paddingLeft: '0px', width: this.props.width + 'px'})}
        >
          <div className={styles.EllipsisTextStyle}>
            <div>{text}</div>
            <div style={{paddingTop: '10px', paddingLeft: '10px'}}>{error.code?getIcon('warning', warningColor): null}</div>
          </div>
        </div>
      </div>
    );
  }
}

class CbTabs extends EABComponent {

  constructor(props) {
      super(props);
      //find the first LA for revisit
      let { values = {} } = props;
      let index = _.findIndex(props.template, tabItem => {
        if (tabItem.relationship === 'OWNER' && _.get(values, 'owner.isActive')) {
          return true;
        } else if (tabItem.relationship === 'SPOUSE' && _.get(values, 'spouse.isActive')) {
          return true;
        } else if (tabItem.relationship === 'DEPENDANTS') {
          return _.find(values.dependants, d => d.isActive);
        }
        return false;
      }) + 1;
      
      this.state={
        template: props.template,
        selectedIndex: index < 1 ? 1 : index
      }
  };

  componentWillReceiveProps(nextProps){
    if(!isEqual(this.props.template, nextProps.template)){
      let {selectedIndex} = this.state;
      let {id: cid} = this.getSelectedItem(selectedIndex);
      let index = _.findIndex(nextProps.template, template=>template.id==cid)+1;
      let newState = {
        template: nextProps.template,
        selectedIndex: index<1?1:index
      };

      this.setState(newState);
    }
  }


  getSelectedItem=(index)=>{
    let {template} = this.state;
    if(template){
      return template[index-1];
    }
  }

  initValue=(id, value, relationship)=>{
    let cValue = null;
    if(relationship){
      if(relationship == "OWNER"){
        cValue = value.owner;
      }
      else if(relationship == "SPOUSE"){
        cValue = value.spouse;
      }
      else if(relationship == "DEPENDANTS"){
        if(!checkExist(value, 'dependants')){
          value.dependants = [];
        }
        let index = -1;
        value.dependants.filter((dependant, i)=>{
          if(dependant.cid == id)
            index = i;
        })
        cValue = value.dependants[index];
      }
      if(cValue.init || cValue.init==null){
        cValue.init = false;
        this.props.onChange(true);
        this.props.validate();
      }
    }

    return cValue;
  }

  onCbChange =(index, e, isInputChecked, tabVal, cbTabUpdate) =>{
    this.onChange(index, isInputChecked, cbTabUpdate, tabVal);
  }

  onChange=(index, isInputChecked, cbTabUpdate, tabVal=[])=>{
    let {validRefValues} = this.context;
    //don't do anything if index = current index
    let {selectedIndex, changedValues} = this.state;
    let {rootValues, values, skipCheck, onTabChange} = this.props;
    let {id, relationship } = tabVal;

    if (_.isBoolean(isInputChecked)){
      if(typeof this.props.tabCheckedChange == "function"){
        this.props.tabCheckedChange({id, relationship}, isInputChecked);
      }
    }
    if(index == this.state.selectedIndex && !cbTabUpdate)
      return;

    //pass only when current item is validate
    let currentItem = this.getSelectedItem(selectedIndex);
    if(validate(currentItem.template, changedValues, null, values, rootValues, validRefValues) && !this.props.skipCheck)
      this.setState({selectedIndex: selectedIndex});
    else{
      if(_.isFunction(onTabChange)){
        onTabChange(()=>{
          this.setState({selectedIndex: index})
        })
      }else {
        this.setState({selectedIndex: index})
      }
    }
  }



  getItem=()=>{
    let { selectedIndex } = this.state;
    let {validRefValues} = this.context;
    let { rootValues, renderItems, values={}, changedValues, showCondItems } = this.props;

    let item=[];
    let {
      template,
      id,
       relationship // for needs
     } = this.getSelectedItem(selectedIndex);

    var cValue = this.initValue(id, changedValues, relationship);
    let error = this.getError(template, id);

    if(_.isFunction(renderItems)){
      renderItems(item, template, values, cValue, error, id, this.props.handleChange, relationship);
    }

    return item;


  }

  getError=(template, id)=>{
    let {error} = this.props;
    let {subType} = template, err;
    let {relationshipType: rType} = _n;
    subType = _.toLower(subType);
    if(subType && [rType.OWNER, rType.SPOUSE, 'proposer'].indexOf(subType)>-1){
      err = error[subType];
    }
    else if(subType && [rType.DEPENDANTS, 'insured'].indexOf(subType)>-1){
      err = error[subType] && error[subType][id];
    }
    return err;
  }

  render(){
	  let { muiTheme, router, lang, langMap } = this.context;

    let { selectedIndex, template } = this.state;

    let {changedValues} =this.props;
    let item = this.getItem();

    let iconColor = muiTheme.palette.alternateTextColor;
    let fontColor = muiTheme.palette.alternateTextColor;
    let TabStyle = {
      color: fontColor,
      padding: '0px 12px'
    };

    let tabs = [];
    let cbState, selProfile;
    let selectedCBtabstate;
    let templateTitleArr =[];
    _.forEach(template, obj => {
      templateTitleArr.push(obj.title);
    })
    let longestStrLegnth = longestStringinArray(templateTitleArr);
    let calcTabsPixel = longestStrLegnth.length * 8;
    let cbAndWarningWidth = 25;
    let fullLengthofTab = calcTabsPixel + cbAndWarningWidth;
    if (calcTabsPixel < 180){
      calcTabsPixel = 180;
      fullLengthofTab = 180 + cbAndWarningWidth;
    }


    template.map((tab, index, template)=>{
      let {relationship} = tab;
      relationship = _.toLower(relationship);
      if(tab.template){
        if (tab.relationship.toLowerCase() != 'dependants'){
          selProfile = changedValues[relationship]
      } else {
        selProfile = _.find(changedValues[relationship], d=>d.cid===tab.id);
      }
      let err = this.getError(tab.template, tab.id);
      cbState = (_.isUndefined(selProfile)) ? false : selProfile.isActive;
      if (selectedIndex - 1 === index) selectedCBtabstate = cbState;
      tabs.push(
        <CbTab
          key={"tabBox" + index}
          name={"tabBox" + index}
          selected={(selectedIndex - 1 === index)}
          checked={cbState}
          totalTabs={template.length}
          error={err}
          width={fullLengthofTab}
          onHandleChange={(e)=> {e.preventDefault(); this.onChange(index + 1, undefined, undefined, undefined)}}
          onCbchange={(e, isInputChecked, value) => { this.onCbChange(index + 1, e, isInputChecked, tab, true)}} text={tab.title}
        />
      );
      }
    })

    let minHeight;
     if ( window.matchMedia('(min-width: '+ (template.length * (fullLengthofTab + 75) + 360).toString() + 'px)').matches){
      minHeight = {minHeight: '48px'};
     } else {
       minHeight = {minHeight: '58px'};
     }

    return (
      <div className={styles.Page}>
       <div className={styles.Tabs + ' ' + styles.CbTabs} style={Object.assign({}, {height: 48} , minHeight )}>
        <div style={{display: 'flex', boxSizing: 'border-box'}}>
          {tabs}
        </div>
       </div>
       <div className={styles.Tab} style={{flexGrow: 1, overflowY: 'auto', padding: '24px', paddingBottom: 51}}>
         {
           (selectedCBtabstate) ? item :
           <div id={'disablePanel'} style={{cursor: 'no-drop'}}>
             <div style={{pointerEvents: 'none'}}>{item}</div>
          </div>
         }
       </div>
      </div>
	  )
  }
}
export default CbTabs;
