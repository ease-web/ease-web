import React, {PropTypes} from 'react';

import EABComponent from '../Component';

export default class EmbedPDFViewer extends EABComponent {

  static propTypes = {
    url: PropTypes.string,
    pdf: PropTypes.string,
    style: PropTypes.object,
    isHideToolBar: PropTypes.bool
  };

  static defaultProps = {
    isHideToolBar: false
  };

  getUrl() {
    const {url, pdf, isHideToolBar} = this.props;
    let dataUrl = null;
    if (url) {
      dataUrl = url;
      if (isHideToolBar) {
        dataUrl += '#toolbar=0&navpanes=0&scrollbar=0';
      }
    } else if (pdf) {
      dataUrl = 'data:application/pdf;base64,' + pdf;
    }
    return dataUrl;
  }

  onOpenPdf(dataUrl) {
    window.open(dataUrl);
  }

  _onMouseClick(event) {
    if (event.button === 2) { // prevent right click
      event.preventDefault();
      return false;
    }
  }

  _getContent(dataUrl) {
    if (window.isIPad()) {
      return (
        <div style={{ height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <div>
            Please <a href="#" onClick={() => this.onOpenPdf(dataUrl)}>click here</a> to view.
          </div>
        </div>
      );
    } else {
      return (
        <object
          ref={ref => { this.embedObj = ref; }}
          type="application/pdf"
          data={dataUrl}
          style={{ width: '100%', height: '100%' }}
          onMouseDown={(event) => this._onMouseClick(event)}
          onMouseUp={(event) => this._onMouseClick(event)}
        />
      );
    }
  }

  render() {
    const {style} = this.props;
    const dataUrl = this.getUrl();
    return (
      <div style={{ width: '100%', height: '100%', overflowY: 'hidden', ...style }}>
        {dataUrl ? this._getContent(dataUrl) : null}
      </div>
    );
  }

}
