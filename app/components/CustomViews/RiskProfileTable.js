import React from 'react';
import ReactDOM from 'react-dom';

import styles from '../Common.scss';

import EABInputComponent from './EABInputComponent';

class RiskProfileTable extends EABInputComponent{
    
    constructor(props) {
        super(props);
        this.data = [
            { id: 1, name: 'Conservative', description: 'You require a very secure income with little fluctuation in capital value. You do not accept much downside and may need the capital in the next few years. Your risk tolerance is low.' },
            { id: 2, name: 'Moderately Conservative', description: 'You require a stable income and expect small fluctuations in value to gain modest capital growth.' },
            { id: 3, name: 'Balanced', description: 'You desire a reasonable stable income stream, but also desire a steady growth in capital value. You are prepared for fluctuations to achieve reasonable capital growth over the medium term.' },
            { id: 4, name: 'Moderately Aggressive', description: 'You have little or no need for ongoing income from your investments. Your investment focus is to achieve capital growth with no need to access capital in the long term. You are prepared to accept fluctuations in capital value to achieve longer term wealth accumulation.' },
            { id: 5, name: 'Aggressive', description: 'You have no need for ongoing income from your investments. Your investment focus is to achieve higher levels of capital growth with no need to access capital for the long term. You are prepared to accept wide fluctuations in capital value and may be prepared to invest additional sums during market downturn.' },
        ];
    }

    render() {
        return (
            <div className={styles.rpContainer} >
                <div className={styles.rpTitle} >Risk Profile descriptions:</div>
                <div className={styles.rpTable} >
                    <tr>
                        <th className={styles.rpProfile} >Profile</th>
                        <th className={styles.rpDescription} >Description of Investor</th>
                    </tr>
                    {this.data.map((data) => 
                        <tr key={data.id}>
                            <td className={styles.dataName} >{data.name}</td>
                            <td className={styles.dataDescription} >{data.description}</td>
                        </tr>
                    )}
                </div>
            </div>
        );
    }

}

export default RiskProfileTable;