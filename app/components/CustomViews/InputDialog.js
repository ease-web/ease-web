let React = require('react');
let mui = require('material-ui');
let FlatButton = mui.FlatButton;
let FontIcon = mui.FontIcon;
let IconButton = mui.FontIcon;
let RaisedButton = mui.RaisedButton;
let Dialog = mui.Dialog;
let appTheme = require('../../theme/appBaseTheme.js');
import EABInputComponent from './EABInputComponent.js';
import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

class InputDialog extends EABInputComponent{
  constructor(props) {
    super(props);
  }

  handleSave = () => {
    if(this.validation()){
      this.props.onSaveClick();
    }
  }

  handleClose = () => {
    this.props.onCancelClick();
  }

  validation = (props) => {
    if (!props) props = this.props;

    let {
      template,
      values,
      style,
      changedValues,
      onSave,
      genItems
    } = props;

    let {optionsMap, langMap} = this.context;
    let error = {};

    for(let i = 0; i < template.items.length; i++){
      _v.exec(template.items[i], optionsMap, langMap, changedValues, changedValues, changedValues, error)
    }

    let hasError = false;
    for(let j in error){
      if(!hasError && error[j].code){
        hasError = true;
      }
    }

    this.setState({
      error : error,
      hasError: hasError
    });
    return !hasError;
  }

  componentWillMount() {
    if (this.props.open) {
      this.validation();
    }
  }

  componentWillReceiveProps(newProps, newState) {
    if (newProps.open) {
      this.validation(newProps);
    }
  }

  render() {

    if(!this.props.open) return null;

    let {
      template,
      values,
      style,
      changedValues,
      onSave,
      genItems
    } = this.props;
    if(!changedValues) changedValues = {};

    let {
      id,
      title,
      disabled
    } = template;

    var newTemplate = _.cloneDeep(template);
    newTemplate.type = "BLOCK";
    var items = [];
    this.props.genItems(items, template, changedValues, changedValues, this.state.error || {}, this.props.index, null, true);

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Save"
        primary={true}
        keyboardFocused={true}
        disabled={this.state.hasError}
        onTouchTap={this.handleSave}
      />
    ];

    return (
      <Dialog
        key={"inpDlg_"+id}
          autoScrollBodyContent={true}
          title={template.title}
          actions={actions}
          modal={false}
          open={this.props.open}
        > {items}
        </Dialog>
    );
  }
}

export default InputDialog;
