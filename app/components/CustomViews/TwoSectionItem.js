import React from 'react';
import * as _ from 'lodash';
import EABTextField from './TextField.js';
import EABToolTips from './ToolTips.js';

import EABInputComponent from './EABInputComponent'
import styles from '../Common.scss';

const borderStyle = "1px solid #DADADA";

class TwoSectionItem extends EABInputComponent {
  renderItems = (template, values, changedValues) =>{
    let {lang} = this.context;
    let { rootValues, validRefValues } = this.props;
    let returnItems = [];
    

    template.items.map((section, index)=>{
        let fields = [];
        section.map((item, itemIndex) => {
          let { id, type, title, trigger, isOther, hints, fontWeight, fontSize, color, decimalNeeded = 0 } = item;

          if (trigger && !window.showCondition(validRefValues, item, values, changedValues, rootValues, null, this.context)) {
              //remove values if hidden
              return;
          }

          let inputTemplate = _.cloneDeep(item);
          
          // don't show title in textField
          inputTemplate.title = null;
          inputTemplate.style = _.assign(inputTemplate.style, {width: '50%'});

          let inputStyle = {bottom: '5px'}
          if (color) {
            inputStyle.color = color;
          }

          let _title = null;
          if (isOther) {
            let _id = item.id;
            let otherTitleTemplate = {
              id: `${item.id}Title`, 
              type: 'text',   
              mandatory: (changedValues[_id] && !changedValues[_id+"Title"]) || (changedValues[_id+"Title"] && !changedValues[_id])?true:false, 
              title: window.getLocalText(lang, item.title), 
              max: item.otherMax,
              style: {minWidth:'130px'}
            };
            
            _title = (
              <div style={{alignSelf: 'flex-end', fontWeight: fontWeight === 'bold' ? 'bold' : 'initial', width: '50%'}}>
                <EABTextField
                  key={`net-grow-${otherTitleTemplate.id}`}
                  id={otherTitleTemplate.id}
                  template={otherTitleTemplate}
                  changedValues={changedValues}
                  style={{paddingTop: '2px'}}
                  inputStyle={{bottom: '5px'}}
                  showRedLine={changedValues[_id] && !changedValues[_id+"Title"]}
                  validRefValues={this.props.validRefValues}
                  rootValues={this.props.rootValues}
                  handleChange={
                    (otherTitleId, value)=>{
                      changedValues[otherTitleId] = value;
                      this.props.handleChange(id, changedValues[id]);
                    }
                  }
                />
              </div>
            )
          } else {
            let _hints = null;
            if (hints) {
              _hints = (<EABToolTips hints={hints} containerStyle={{height: 'initial', vertialAlign: 'middle'}} Iconstyle={{padding: '0px 12px', height: 'initial'}}/>);
            }

            let titleStyle = {flex: 1, alignSelf: 'center', textAlign: 'left'};
            if (fontWeight) {
              titleStyle.fontWeight = 'bold';
            }

            if (fontSize) {
              titleStyle.fontSize = fontSize;
            }

            _title = (
              <div style={titleStyle}>
                <span>{window.getLocalText(lang, title)}</span>
                {_hints}
              </div>
            );
          }

          let _content = null;
          if (type === 'showValue') {
            let className = null;
            if (color === 'green') {
              className = styles.ShowGreenColor;
            } else if (color === 'red') {
              className = styles.ShowAlertColor;
            }

            let cValue = _.get(changedValues, item.id, 0);
            _content = (
              <div className={className} style={{alignSelf: 'center'}}>{window.getCurrency(cValue, '$', decimalNeeded)}</div>
            );
          } else {
            let { ShowGreenColor, ShowAlertColor, rightTextField } = this.getStyles();
            let inputStyle = _.assign({bottom: '5px'}, rightTextField);
            if (color) {
              inputStyle = _.assign(inputStyle, color === 'green' ? ShowGreenColor : ShowAlertColor);
            }

            _content = (
              <EABTextField
                  id={id}
                  key={`two-section-item-${id}`}
                  inputStyle={inputStyle}
                  template={inputTemplate}
                  changedValues={changedValues}
                  showRedLine={isOther && !changedValues[id] && changedValues[id+"Title"]}
                  values={values}
                  isCustom={true}
                  handleChange={this.props.handleChange}
                  rootValues={this.props.rootValues}
                  validRefValues={this.props.validRefValues}
              />
          );
          }

          fields.push(
            <div key={'two-section-item-section-' + itemIndex} style={{padding: '4px 12px', display: 'flex', height: '56px', alignItems: 'center'}}>
              {_title}
              {_content}
            </div>
          )
        });
        
        returnItems.push(
          <div key={`cash-item-${template.id}-${index}`} style={{borderTop: index>0? borderStyle:'', flex: index==template.items.length-1?1:'initial'}}>
            {fields}
          </div>
        )
      });

    return returnItems;
  }

  getWarnCode = function(template, changedValues){
    for (var i in template.items) {
      for (var j in template.items[i]) {
        if (changedValues[template.items[i][j].id]) {
          return false;
        }
      }
    }
    return true;
  }

  render() {
    let { lang } = this.context;
    let { template, changedValues, values } = this.props;
    let { title, header, warning, height, id, color } = template;
    let { showGreenColor, showRedColor } = this.getStyles();

    let items = this.renderItems(template, values, changedValues);
    // show warning code
    let warnCode = this.getWarnCode(template, changedValues);
    let labelStyle = _.assign({}, color === 'green' ? showGreenColor : color === 'red' ? showRedColor : {});
    
    return (
      <div style={{margin: '0px 12px 12px 12px', width: 'calc(50% - 24px)', minWidth: '320px', display: 'inline-block', fontSize: '16px', verticalAlign: 'top'}}>
        { 
            header ? (
            <div style={{border: borderStyle, flex: 1, display: 'flex', flexDirection: 'column', borderBottom: '0px'}}>
                <div className={styles.CashFlowItemDesc}>{window.getLocalText(lang, header)}</div>
                {<div className={styles.CashFlowItemErrorDesc}>{(warnCode) ? this.getFieldTitle(true, warning) : ''}</div>}
            </div>
            ) : null
        }
        <div style={{border: borderStyle, flex: 1, display: 'flex', height, flexDirection: 'column'}}>
            {items}
            {
              title ? (
                <div style={{display: 'flex', borderTop: borderStyle, justifySelf: 'flex-end', padding: '12px', fontSize: '20px'}}>
                  <span style={{flex: 1, textAlign: 'left'}}>{window.getLocalText(lang, title)}</span>
                  <span style={labelStyle}>{window.getCurrency(_.get(changedValues, id, 0), '$', 0)}</span>
                </div>
              ) : null
            }
        </div>
      </div>
    );
  }
}

export default TwoSectionItem;