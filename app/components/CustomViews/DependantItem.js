import React from 'react';
import * as _ from 'lodash';
import * as _c from '../../utils/client'
import {Paper} from 'material-ui';

import EABInputComponent from './EABInputComponent';

import EABAvatar from './Avatar';
import {getIcon} from '../Icons/index';
import styles from '../Common.scss';

class Item extends EABInputComponent{

  getRelationship=(value, relationshipOther)=>{
    let {optionsMap, langMap, lang} = this.context;

    if(checkExist(optionsMap, 'relationship.options')){
      let option = optionsMap.relationship.options.find(option=>option.value === value);
      return getLocalText(lang, option && option.title);
    }
    return "";
  }

  getMemberData=()=>{
    let {dependantProfiles} = this.context.store.getState().client;
    let {cid} = this.props.member;
    return dependantProfiles && dependantProfiles[cid];
  }

  render(){
    let {muiTheme, optionsMap, lang, store} = this.context;
    let {profile, dependantProfiles} = store.getState().client;
    let {selected, onCheck, docId, member} = this.props;
    
    if(isEmpty(member))
      return (<div key={"truested-inidividual-item"}/>)

    let {relationship, relationshipOther, cid} = member;
    let {photo} = dependantProfiles[cid];
    let name = _c.getClientName(profile, dependantProfiles, cid);
    let iconColor = muiTheme.palette.secondary1Color;
    let iconsubColor = muiTheme.palette.accent3Color

    let relationshipTitle = _c.getRelationTitle(profile, cid, optionsMap, lang);
    let paperStyle = {}
    selected? paperStyle = this.getStyles().selectedBtn : paperStyle = this.getStyles().notSelectedBtn;
    let initial;
    (dependantProfiles && cid) ? initial = _c.getAvatarInitial(dependantProfiles[cid]) : initial = undefined;

    return(
      <Paper onTouchTap={onCheck} style={Object.assign({}, paperStyle, this.getStyles().largeTextSelection )}>
        <div className={styles.Avatar}>
          <EABAvatar docId={docId} changedValues={{photo, initial}} values={{photo, initial}} template={{id:'photo', type:'photo'}} width={64} icon={getIcon('person', iconColor)} disabled/>
        </div>
        <div style={{flexGrow: 1, margin: 'auto'}}>
          <div className={styles.CardRowFieldLarge}>{name}</div>
          <div className={styles.CardRowFieldSmall} style={{color: iconsubColor}}>{relationshipTitle}</div>
        </div>
      </Paper>
    )
  }
}

export default Item;
