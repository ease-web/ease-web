let React = require('react');
   

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';

import {STATUS, APPRVOAL_STATUS_APPROVED, APPRVOAL_STATUS_REJECTED, APPRVOAL_STATUS_EXPIRED} from '../Pages/Pos/Approval/approvalStatus';


class CaseStatus extends EABInputComponent {
  constructor(props) {
    super(props);
  }

  transferStatusValue(value) {
    return STATUS[value];
  }

  render() {
    let {
      template
    } = this.props;


    let {
      id
    } = template;

    let cValue = this.getValue('');
    let statusColorClass;
    if (cValue === APPRVOAL_STATUS_APPROVED) {
      statusColorClass = styles.showGreenBackgroundColor;
    } else if (cValue === APPRVOAL_STATUS_REJECTED || cValue === APPRVOAL_STATUS_EXPIRED) {
      statusColorClass = styles.showRedBackgroundColor;
    } else {
      statusColorClass = styles.showOrangeBackgroundColor;
    }

    return (
        <div className={styles.CaseStatus + ' ' + statusColorClass + ' ' + styles.whitefont}>
            {this.transferStatusValue(cValue)}
        </div>
    );
  }
}

export default CaseStatus;