import React from 'react';
import {TextField, FlatButton, Dialog} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import * as _ from 'lodash';
import styles from '../Common.scss';

import {getAddressByPostalCode} from '../../actions/client';

class EABTextField extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      postalCode: ''
    });
  }

  onKeyPress=(e)=>{
    let {subType = ""} = this.props.template;
    subType = _.toLower(subType);
    let keyCode = e.which || e.keyCode;
    if(subType === "currency" && this.getValue(0)===0){
      e.target.value = "";
    }
  }

  onPaste=(e)=>{
    let {subType = ""} = this.props.template;
    if(subType === "currency" && this.getValue(0)===0){
      e.target.value = "";
    }
  }

  onChange=(e)=>{
    let value = this.handleValue(e.target.value);
    let {subType} = this.props.template;
    if(value != null){
      if(subType === 'currency'){
        this.input = e.target;
        this.init = this.getValue(0)===0?true:false;
        this.caretOffset = e.target.value.substring(e.target.selectionStart, e.target.value.length).replace(/[^0-9\.]+/g,"").length;
      }
      this.requestChange(value);
    }
  }

  closeDialog = function(){
    this.setState({open:false})
  }

  handleValue=(value)=>{
    let {template} = this.props;
    let {subType, decimal, allowZero, initZero, max, min} = template;
    let dValue = _.toLower(subType) === 'currency' ? 0 : '';
    let oValue = this.getValue(dValue);
    subType = _.toLower(subType);

    if(subType === "digit"){
      value = value.replace(/[^0-9]+/g,"");
    }

    if(['currency', 'number', 'percentage', 'allownegativepercentage','allownegative'].indexOf(subType)>-1){
      //remove all prefix first
      value = subType === 'currency'? value.replace(/[^0-9\.]+/g,""): value.replace(/[^0-9\.\-]+/g,"");
      //not allow . if number have no decimal
      if("number" == subType && !decimal && value.indexOf(".") > -1){
        return value.replace(".", "");
      }

      // if(!allowZero && this.getValue(0)===0){
      //   value = value.replace("0","");
      // }

      if(decimal){
        //not allow more than 1 .
        let _d = value.split(".");
        if(_d.length > 2){
          let _d2 = _d[0];
          _d.splice(0, 1);
          return _d2 + "." + _d.join("");
        }

        //remove number if over decimal
        if(_d[1] && _d[1].length > decimal){
          value = _d[0] + "." + _d[1].substr(0, decimal);
        }
      }

      /**
       * batch2 - issue 0031121 handle .1, 0.1, 0.01, -.9, -0.1, -0.02, etc - Kenny
       *          not use floor, as -0.111 -> -0.12 -> -0.13, etc
       */
      let sv = "", ni = 0, v = Number(value);
      if(max && !isNaN(v) && v > max){
        return oValue;
      }
      //handle special number
      if(isNaN(v) || v === 0){
        if(value === "" || value === "0"){
          return 0;
        }
        if(['allownegativepercentage','allownegative'].indexOf(subType)>-1 && value.charAt(ni)==="-"){
          sv += "-";
          ni++;
        }
        if(value.charAt(ni)==="0"){
          sv += "0";
          ni++;
        }
        if(value.charAt(ni)==="."){
          sv += (sv.length && sv.charAt(sv.length-1) === "0"? "": "0") + ".";
          ni++;
        }
        if(_.isNumber(decimal) && sv.indexOf(".") > -1){
          let len = sv.length - ni;
          for(let i = len > decimal? len: decimal; i > 0; i--){
            sv += value.charAt(ni++) === "0"? "0": "";
          }
        }
        return sv;
      }

      return isNaN(v)?null: v;

    }else if ('nonzeromintxtfield'== subType) {
      value = Number(value.replace(/[^0-9]+/g,""));
      if (!_.isNumber(max) || value<=max) {
        return value;
      }else {
        return null;
      }
    }else if ("number_leading_zero" == subType) {
      value = value.replace(/[^0-9]+/g,"");
      if(value.length <= template.max){
        return value;
      }else{
        return null;
      }
    } else if (subType === 'unit_no') {
      let _tValue = this.state.value || this.state.changedValues[template.id];
      if ((!_tValue || _tValue === 0 || (_tValue + '').length === 0) && (value + '').length === 1) {
        value = 'Unit ' + value;
      }
      if (value.length <= template.max) {
        this.state.value = value;
        return value;
      } else {
        return null;
      }
    }
    else{
      // if((!_.isNumber(min) || value.length >=min) && (!_.isNumber(max) || value.length<=max) ){
      if(!_.isNumber(max) || value.length<=max ){
        if(template.validation){
          let validation = template.validation;
          if(validation.type && validation.type == "sum"){
            if(validation.max){
              let lkValue = this.props.changedValues[validation.id] || "";
               if((value.length + lkValue.length) > validation.max)
                return null
            }
            if(validation.min){
              let lkValue = this.props.changedValues[validation.id] || "";
               if((value.length + lkValue.length) < validation.min)
                return null
            }
          }
        }
        return value;
      }else {
        return null;
      }
    }
  }

  onBlur=(e)=>{
    let {changedValues} = this.state;
    let {template, values} = this.props;
    let {id, masked, subType, mapping, countryAllowed} = template;
    // for mask sensitive fields
    if (masked == MASK_FIELD && values && changedValues[id] == "" && values[id]) {
      changedValues[id] = values[id];
      this.forceUpdate();
    } else {
      let value = this.handleValue(e.target.value);
      if (value && subType === 'postalcode' && mapping && countryAllowed && changedValues[countryAllowed] === 'R2' && this.state.postalCode !== value && value.length === 6){
        this.populatePostalCode(value, template, changedValues);
        this.setState({postalCode: value});
      }else if(value){
        this.handleBlur(value);
      }
    }
  }

  populatePostalCode(value, template, changedValues){
    //if (this.isEmptychangeValues(Object.values(template.mapping), changedValues)){
      let {langMap} = this.context
      getAddressByPostalCode(this.context, value, (resp) =>{
        let tempArr = _.split(template.mapping.bldgNo,',');
        changedValues[tempArr[0]] = resp.bldgNo.substr(0,tempArr[1] || resp.bldgNo.length);
        tempArr = _.split(template.mapping.bldgName,',');
        changedValues[tempArr[0]] = resp.bldgName.substr(0,tempArr[1] || resp.bldgName.length);
        tempArr = _.split(template.mapping.streetName,',');
        changedValues[tempArr[0]] = resp.streetName.substr(0,tempArr[1] || resp.streetName.length);
        this.handleBlur(template.id, value, true);
        if (resp.success === false && _.isFunction(this.props.openPostalCodeErr)){
          this.props.openPostalCodeErr();
        }
      });
  }

  isEmptychangeValues(keyArr, cV){
    let result = true;
    _.forEach(keyArr, obj =>{
      let changedValuesKey = _.split(obj, ',');
      let v = cV[changedValuesKey[0]] || '';
      if (!isEmpty(v.trim())){
        result = false;
        return false;
      }
    });
    return result;
  }

  focus=()=>{
    this[this.props.template.id] && this[this.props.template.id].focus();
  }

  onFocus=(e)=>{
    let {changedValues} = this.state;
    let {template, values} = this.props;
    let {id, masked} = template;
      // for mask sensitive fields
      if (masked == MASK_FIELD && changedValues[id] == values[id]) {
        changedValues[id] = "";
        this.forceUpdate();
      }
      let eLen = e.target.value? e.target.value.length: 0;
      e.target.selectionStart = eLen;
      e.target.selectionEnd = eLen;
    }

   render() {
    var self = this;
    let {validRefValues, muiTheme} = this.context;
    let {changedValues} = this.state;
    var {
      template,
      values,
      hintText,
      handleChange,
      focusColor,
      values,
      requireShowDiff,
      index,
      changeActionIndex,
      rootValues,
      autoWidth,
      labelStyle,
      iconStyle,
      fieldRef,
      isCustom,
      inputStyle,
      showRedLine
    } = this.props;

    let textFieldStyle = this.props.style;

    var {
      id,
      type,
      placeholder,
      presentation,
      disabled,
      options,
      masked,
      value,
      min,
      max,
      interval,
      title,
      mandatory,
      subType,
      ccy,
      allowZero,
      initZero,
      allowNegative,
      maxLength,
      Horizontal,
      halfColumn,
      style,
      fixedLabel
    } = template;

    var {
      lang, langMap
    } = this.context

    var cValue = self.getValue('');

    let dValue = initZero ? 0 : '';

    var cValue = self.getValue(dValue);

    // support app form
    if (options) {
      for (let i in options){
        var option = options[i];

        // to support app form's option
        if (!option.value) {
          if (option[cValue]) {
            cValue = getLocalText(lang, option[cValue])
            break
          }
        } else if (option.value == cValue){
          cValue = getLocalText(lang, option.title);
          break;
        }
      }
    }
    else if (_.toLower(subType) === 'currency') {
      if(!template.allowEmpty || (cValue && cValue != "")){
        if (ccy) {
          cValue = !_.isNumber(cValue) && allowZero?"": window.getCurrencyByCcy(cValue || 0, _.toUpper(ccy), 0);
        } else {
          cValue = !_.isNumber(cValue) && allowZero?"": window.getCurrency(cValue || 0, '$', 0);
        }
      }
    }
    else if (_.toLower(subType) === 'unit_no'){
      //cValue = cValue?'#'+cValue:'';
    }

    var errorMsg = this.getErrMsg();
    let iconColor = muiTheme.palette.primary1Color;

    if (Horizontal === true) {
      inputStyle = Object.assign(inputStyle, {textAlign: 'right'});
    }

    if(this.input){
      let ti = 0;
      if(this.init){
        this.input.value = cValue;
        this.input.selectionStart = cValue.length;
        this.input.selectionEnd = cValue.length;
      }
      else if(this.caretOffset){
        for(let a = cValue.length - 1; a > 0 ; a--){
          if("0123456789".indexOf(cValue.charAt(a))>-1){
            ti++;
            if(ti >= this.caretOffset){
              this.input.value = cValue;
              this.input.selectionStart = a;
              this.input.selectionEnd = a;
              this.caretOffset = null;
              a = 0;
            }
          }
        }
      }
    }

    let { hintStyle, errorStyle} = this.getStyles();
    if (inputStyle && inputStyle.textAlign === 'center') {
      errorStyle.textAlign = 'center';
      hintStyle.textAlign = 'center';
      hintStyle.width = '100%';
    }

    let underlineStyle = _.assign(!isCustom ? {bottom:'22px'} : {}, isCustom || Horizontal ? this.getStyles().disabledUnderline : {});
    if (showRedLine) {
      underlineStyle = _.assign(underlineStyle, {borderColor: 'red'});
    }


    let textField = (
      <TextField
          id = {id}
          ref = {ref=>this[id]=ref}
          type = {(_.toLower(subType)=="number" || allowNegative=== true)?"style":subType}
          className = {halfColumn ? styles.HalfColumn : isCustom ? null : styles.ColumnField}
          style={isCustom? Object.assign({}, this.getStyles().isCustomColumnField, style? style: {}) : Object.assign({}, this.getStyles().columnField, textFieldStyle) }
          disabled = { (masked == MASK_FIELD_IF_EXIST_AND_DISABLE && values && !isEmpty(values[id])) || disabled}
          hintText= { placeholder?getLocalText(lang, placeholder):null }
          hintStyle= { hintStyle }
          inputStyle={inputStyle}
          errorText= { errorMsg }
          maxLength={maxLength}
          floatingLabelFixed={fixedLabel}
          floatingLabelText = {(title && Horizontal !== true) ?this.getFieldTitle(mandatory, title):null}
          errorStyle = {errorStyle}
          floatingLabelStyle = { this.getStyles().floatingLabelStyle }
          floatingLabelShrinkStyle = {this.getStyles().floatingShrinkStyle}
          underlineStyle = {underlineStyle}
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          value = { cValue }
          min = {subType == 'number'?min:null}
          max = {subType == 'number'?max:null}
          step = {subType == 'number'?interval:null}
          autoComplete= {"off"}
          onFocus = {this.onFocus}
          onChange = {this.onChange}
          onBlur = {this.onBlur}
          onKeyPress = {this.onKeyPress}
          onPaste = {this.onPaste}
        />
    )

    if(isCustom){
      return textField;
    }
    else if (Horizontal === true) {
      return <div className={styles.FlexContainer}>
        <div style={{display: 'inline-block', padding: '28px 24px 0 0', flexBasis: '30%'}}>{this.getFieldTitle(mandatory, title)}</div>
        <div id={'TextFieldContainer'} className={ styles.DetailsItem + ' ' + styles.TextField} style={{flexBasis: '50%'}}>
          {textField}
        </div>
      </div>
    }
    else {
      return <div id={'TextFieldContainer'} className={ styles.DetailsItem + ' ' + styles.TextField + (presentation?' ' +styles[presentation]:"")} style={style}>
          {textField}
        </div>
    }


  }
}


export default EABTextField;
