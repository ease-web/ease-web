import React from 'react';
import PropTypes from 'prop-types';
import EABCompoent from '../Component';
import {Dialog, FlatButton} from 'material-ui';
import TextField from './TextField';

import styles from '../Common.scss'

import * as _v from '../../utils/Validation';


class ValueDialog extends EABCompoent {
  constructor(props){
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      valid: false,
    });
  }

  open=()=>{
    let template = _.cloneDeep(this.props.template);
    let {id, valuetoValidate} = template;
    if(valuetoValidate || valuetoValidate == 0){
      template.max = valuetoValidate;
    }

    this.setState({
      open: true,
      value: {
        [id]: _.cloneDeep(this.props.value)
      },
      template
    })
    this[id] && this[id].focus;
  }

  close=()=>{
    this.setState({
      error: undefined,
      open: false
    })
  }

  validate=(value)=>{
    let {template} = this.state;
    let {id, min=0, max=100, allowNegative, decimalNeeded} = template;
    let {optionsMap, langMap} = this.context;
    if(value !=='-' && value != '' && allowNegative === true && _.isNumber(value) ) {
      value = Number(value);
    }
    let error = {[id]: {}};
    let _value = {[id]:value};
    let valid = _v.validateText(template, optionsMap, langMap, _value, null, null, error[id])
    this.setState({
      value: _value,
      valid,
      error
    })
  }

  render(){
    let {langMap, lang} = this.context;
    let {value, valid, open, error, template={}} = this.state;
    let {handleChange} = this.props;
    let {id, subType, min, max, valuetoValidate, decimalNeeded, interval, allowZero,allowNegative} = template;

    let cancelBtn = (
      <FlatButton
        primary={true}
        label={getLocalizedText(langMap, "app.cancel")}
        onTouchTap={this.close}
      />
    )

    let okBtn = (
      <FlatButton
        disabled={!valid}
        primary={true}
        label={getLocalizedText(langMap, "app.ok")}
        onTouchTap={()=>{
          handleChange(value[id]); this.close();
        }}
      />
    )
    let actions = [cancelBtn, okBtn];
    let itemTemplate = {id, type: 'TEXT', subType, min, max, decimal: decimalNeeded, interval, allowZero,allowNegative};
    if(subType==='currency'){
      min = _.cloneDeep(getCurrency(min, '$', 0));
      max = _.cloneDeep(getCurrency(max, '$', 0));
    }
    else if(_.isString(subType) && subType.indexOf('percentage') > -1){
      min = min + '%';
      max = max + '%';
    }

    let textField = (
      <TextField
        id={id}
        ref={ref=>this[id]=ref}
        template={itemTemplate}
        changedValues={value}
        error={error}
        handleChange={(id, _value)=>{this.validate(_value)}}
      />
    )
    
    return(
      <Dialog
        title={getLocalizedText(langMap, "normal_slider.enter_data")}
        actions={actions}
        modal={false}
        contentStyle={{
          width: 320
        }}
        bodyClassName={styles.Column1}
        className={styles.Dialog}
        open={open}>
        {textField}
        {(error && valid == false) ? <div style={{position: 'relative', top: '20px'}}>{`${min} ~ ${max}`}</div> : 
        <div>{`${min} ~ ${max}`}</div>
        }
      </Dialog>
    )
  }
}

ValueDialog.propTypes = Object.assign({}, ValueDialog.propTypes, {
  template: PropTypes.object,
  handleChange: PropTypes.func
})

export default ValueDialog;