import React from 'react';
import { DatePicker, TextField } from 'material-ui';
import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import ConfigConstants from '../../constants/ConfigConstants';

class EABDatePicker extends EABInputComponent{
  constructor(props){
    super(props);
    let template = props;
    let value = props.changedValues[template.id];
    this.state = Object.assign( {}, this.state, {
      changedValues: props.changedValues || {},
      selectedDate: value?(new Date(value)):null
    });
  }

  componentWillReceiveProps(newProps, newState) {
    let template = newProps;
    let value = newProps.changedValues[template.id];
    this.setState({
      changedValues: newProps.changedValues || {},
      selectedDate: value?(new Date(value)):null
    });
  }

  triggerFocus(event) {
    event.target.focus();
  }

  toYear=(num=0)=>{
    return num*365*24*60*60*1000;
  }

  render(){
    let {lang, langMap, validRefValues} = this.context;
    let {changedValues, selectedDate} = this.state;
    let {template, rootValues, values, error, mode="landscape", handleChange, requireShowDiff, frDynNeedPage} = this.props;
    let {id, type, placeholder, min=3, max=3, minMonth=0, maxMonth=0, value=0, disabled, presentation=ConfigConstants.DateFormat, subType, title, mandatory, style, hasMargin, shadowLabel} = template;
    if(template.dateFormat){
      presentation = template.dateFormat
    }

    let marginR;
    if (title){
      marginR = undefined;
    } else if (hasMargin) {
      marginR = '24px';
    } else {
      marginR = '0px';
    }
    let floatingLabelShrinkStyle;
    if (shadowLabel) {
      floatingLabelShrinkStyle = this.getStyles().floatingShadowLabelStyle;
    } else {
      floatingLabelShrinkStyle = this.getStyles().floatingLabelStyle;
    }
    var self = this;
    let now = new Date()
    let tvalue = changedValues[id];
    let dDate = new Date(now);
    dDate.setYear(dDate.getFullYear() - value);
    let cValue = tvalue ? new Date():undefined;

    let errorMsg = this.getErrMsg();
    let minDate = new Date(now);
    minDate.setYear(minDate.getFullYear() - min);
    minDate.setMonth(minDate.getMonth() - minMonth);
    let maxDate = new Date(now);
    maxDate.setYear(maxDate.getFullYear() + max);
    maxDate.setMonth(maxDate.getMonth() + maxMonth);
    let datePickerField = <div className={styles.DetailsItem + ' ' + styles.DatePicker} style={Object.assign({}, {marginRight: marginR }, style ? style: {})}>
        <DatePicker
          key={id}
          //className={styles.ColumnField}
          mode={mode}
          floatingLabelText={this.getFieldTitle(mandatory, title)}
          floatingLabelShrinkStyle={this.getStyles().PickerfloatingShrinkStyle}
          floatingLabelStyle={floatingLabelShrinkStyle}
          errorStyle={this.getStyles().errorStyle}
          underlineStyle={{bottom:'22px'}}
          textFieldStyle={this.getStyles().textFieldLabel}
          underlineDisabledStyle={this.getStyles().disabledUnderline}
          inputStyle={{top: '0', marginTop: '0px'}}
          minDate={minDate}
          maxDate={maxDate}
          errorText={errorMsg}
          hintText= { placeholder?getLocalText(lang, placeholder):null }
          hintStyle= { this.getStyles().hintStyle }
          disabled={disabled}
          value={(tvalue)? new Date(tvalue) : selectedDate}
          onTouchTap={this.triggerFocus}
          onFocus={()=>{this.setState({selectedDate: dDate});}}
          formatDate = {
            (date)=>{
              return _.isDate(date)?date.format(presentation): "";
            }
          }
          onChange = {
            (e, value)=>{
              this.state.selectedDate = value;
              this.requestChange(value.getTime());
            }
          }
          onBlur = {
            (e)=>{
              this.handleBlur(cValue);
            }
          }
          />
          {/* textField for auto test */}
          <TextField id={id} style={{display:'inline-block', width: 20, height: 20, padding:0, margin: 0, position: 'absolute', opacity:0}}
            onChange={(e, value)=>{
              if (typeof value == "string") {
                value = new Date(value);
                if (self.atflag) {
                    clearTimeout(self.atflag);
                  }
                  self.atflag = setTimeout(function() {
                    this.setState({selectedDate: value})
                    this.requestChange(value.getTime());
                  }, 500);
                }
              }
            }
           />
      </div>
    return (
      (_.isUndefined(title) && mandatory) ?
          <div className={styles.FlexNoWrapContainer} style={style? style: {}}>{datePickerField}<span className={styles.mandatoryStar} style={{paddingRight: '24px'}}>*</span></div> :
          datePickerField
    );
  }
}

export default EABDatePicker;
