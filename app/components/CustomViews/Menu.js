import React from 'react';
import PropTypes from 'prop-types'
import {Subheader, Divider} from 'material-ui';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import EABComponent from '../Component';
import * as _ from 'lodash';
import _getOr from 'lodash/fp/getOr';

import {getIcon} from '../Icons/index';
import styles from '../Common.scss';
import ToolTips from './ToolTips';
import * as appActions from '../../actions/application.js';

let SelectableList = makeSelectable(List);

class Menu extends EABComponent {

  constructor(props) {
    super(props);
    let landingIndex = props.landingIndex;
    if(!landingIndex || landingIndex == 0 ){
      landingIndex = 1;
    } else {
      landingIndex = landingIndex + 1;
    }

    // mark the landing index as checked.. only here will be the final value.
    if (props.isAppMenu) {
      let checkedMenu = props.changedValues.checkedMenu
      let menus = props.changedValues.menus
      if (checkedMenu && menus && checkedMenu.indexOf(menus[landingIndex - 1]) == -1) {
        checkedMenu.push(menus[landingIndex - 1]);
      }
    }

    this.state = Object.assign( {}, this.state, {
      template: props.template,
      selectedIndex: landingIndex
    });
  };

  componentWillReceiveProps(nextProps){
    let {template, values, changedValues, error, id} = this.props;
    let {id: nId, template: nTemplate, values: nValues, changedValues: nChangedValues, error:nError} = nextProps;
    let {selectedIndex} = this.state;
    let isUpd = false;

    if(!isEqual(id, nId)){
      selectedIndex = 1;
    }

    if(!isEqual(values, nValues)){
      values = nValues;
      changedValues = nChangedValues;
      isUpd = true;
    }

    if(!isEqual(error, nError)){
      error = nError;
      isUpd = true;
    }

    if(!isEqual(template, nTemplate)){
      template = nTemplate;
      isUpd = true;
    }

    if(isUpd){
      this.setState({values, changedValues, template, selectedIndex});
    }
  }
  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let appForm = this.context.store.getState();
      if(appForm.menuIndex && appForm.menuIndex != this.state.selectedIndex){
        this.setState({selectedIndex: appForm.menuIndex});
      }
    }
  }


  getSections = (items, template)=>{
    let self = this;
    let index = 1;
    let {lang, validRefValues, muiTheme} = this.context;
    let {rootValues, values, changedValues, error} = this.props;

    template.map((section, i)=>{
      let {title} = section

      if(_.isNumber(i)){
        items.push(
          <Divider key={"menu-"+index+"-"+i+"-Divider"}/>
        )
      }

      if(title){
        items.push(
          <Subheader key={"menu-"+index+"-"+i+"-subheader"}>{getLocalText(lang, title)}</Subheader>
        )
      }

      section.items.map((item, j)=>{
          let {id, title, template} = item;
          let key = null;
          if(item.template){
            key = item.template.key;
          }

          //check is the item valid if item not validate before
          if(!changedValues[id]) changedValues[id] = {};
          let titleNode = (
            <div key={"tn-"+i +"-"+j} className={(template.hints || template.disabled) ? styles.ShowSubContentColor : undefined} style={{display: 'flex', alignItems: 'center'}}>
              {getLocalText(lang, title)}
              {template.hints? <ToolTips hints={template.hints}/>: null}
            </div>
          );

          //app form
          let checked = true;
          if(key && item.template.key && self.props.checkedMenu.indexOf(key) == -1){
            checked = false;
          }
          let menuChecked = _.get(changedValues, `${item.id}.isValid`) === false? false: (!(_.at(error, `${id||key}.code`)[0]) ) && checked;
          let icon = !item.template.skipCheck ? getIcon("checkCircle", menuChecked ? muiTheme.palette.primary1Color: muiTheme.palette.borderColor) : null;
          let leftAva = <div style={{top: template.hints? '32px' : '16px'}}>{icon}</div> ;

          items.push(
            <ListItem
              key={"menu-"+index+"-"+i+"-"+j+"-"+id}
              value={index++}
              disabled={template.disabled}
              primaryText={titleNode}
              leftAvatar={leftAva}
            />
          )
      })
    })
  }

  getSelectedItem=(index)=>{
    let {template} = this.state;
    if(template){
      for(let i in template){
        let items = template[i].items;
        let itemsLength = items.length;
        if(index>itemsLength){
          index -= itemsLength
        }else{
          return items[index-1];
        }
      }
    }
  }

  getTemplateKeyList=()=>{
    let {template} = this.state;
    let keyList = [];
    if(template){
      for(let i in template){
        let items = template[i].items;
        for (let j in items) {
          if (items[j].template && items[j].template.key) {
            keyList.push(items[j].template.key);
          }
        }
      }
    }
    return keyList;
  }

  updateAppMenu = (selectedIndex, cb) => {
    //check if
    let currentItem = this.getSelectedItem(selectedIndex);
    //set skipCheck menu
    if (currentItem.template) {
      let checkedMenu = this.props.checkedMenu;
      if (currentItem.template.key) {
        if (checkedMenu.indexOf(currentItem.template.key) == -1) {
          checkedMenu.push(currentItem.template.key);
        }
      }
    }

    let { appForm, shieldApplication } = this.context.store.getState();
    let keyList = this.getTemplateKeyList();

    let checkMenuItem = false;
    if (currentItem.template.shieldLayout) {
      let isPhSameAsLa = _.get(shieldApplication, 'application.applicationForm.values.proposer.extra.isPhSameAsLa', 'N');
      checkMenuItem = currentItem.template.key === 'menu_insure' || (isPhSameAsLa !== 'Y' && currentItem.template.key === 'menu_declaration');
    } else {
      checkMenuItem = (keyList.indexOf('menu_insure') > -1 && currentItem.template.key === 'menu_insure' || keyList.indexOf('menu_insure') < 0 && currentItem.template.key === 'menu_declaration');
    }

    if (currentItem.template.key && checkMenuItem) {
      let policyNumber;
      if (currentItem.template.shieldLayout) {
        policyNumber = _getOr(undefined, 'application.isPolicyNumberGenerated', shieldApplication);
      } else {
        policyNumber = _getOr(undefined, 'application.policyNumber', appForm);
      }

      if (policyNumber) {
        cb();
      } else {
        cb(true);
      }
    } else {
      cb();
    }
  }

  onChange=(event, index)=>{
    this.menuContent.scrollTop = 0;

    let self = this;
    //don't do anything if index = current index
    let {selectedIndex} = this.state;
    let {changedValues, rootValues, skipCheck, onMenuItemChange, validRefValues} = this.props;

    if(index === selectedIndex)
      return;

    //pass only when current item is validate
    //let currentItem = this.getSelectedItem(selectedIndex);

    let onchangeEvnt = (toGetPNumber) => {
      if(_.isFunction(onMenuItemChange)){
        onMenuItemChange((resp)=>{
          if (toGetPNumber && resp && resp.policyNumber) {
            let error = this.props.error;
            // reset the policy number missing error
            if (error && error.menu_insure && error.menu_insure.code == 711) {
              error.menu_insure.code = false;
            } else if (error && error.menu_declaration && error.menu_declaration.code == 711) {
              error.menu_declaration.code = false;
            }
          }
          self.setState({selectedIndex: index})
        }, toGetPNumber)
      }else{
        self.setState({selectedIndex: index})
      }
    }

    if(this.props.isAppMenu){
      this.updateAppMenu(index, onchangeEvnt);
    }else{
      onchangeEvnt();
    }

  }

  getItem=()=>{
    let {validRefValues} = this.context;
    let {selectedIndex} = this.state;
    let {renderItems, rootValues, values, changedValues, error} = this.props;

    //if no getItem function, return null
    if(!_.isFunction(renderItems))
      return;

    let item=[];
    let {template, id} = this.getSelectedItem(selectedIndex);
    renderItems(item, template, values && id? values[id]: values, id ? changedValues[id] : changedValues, error[id || template.key], id, this.props.handleChange);

    return item;
  };

  render(){
	  let {muiTheme, router, lang, langMap} = this.context;
    let {selectedIndex, template} = this.state;
    let { hasFixedBottom } = this.props;

    let items = [];
    this.getSections(items, template);
    let item = this.getItem();
    let hasTab = false;
    let itemTemplate = this.getSelectedItem(selectedIndex);
    if (itemTemplate && itemTemplate.template && itemTemplate.template.items &&
      _.isObject(itemTemplate.template.items.find((template) => {return template.type == 'tabs'}))){
        hasTab = true;
    }


    if(_.get(this.getSelectedItem(selectedIndex), "id")){
      this.props.changedValues.selectedMenuId = _.get(this.getSelectedItem(selectedIndex), "id");
    }
    return (
      <div style={{display: 'flex', flexGrow: 1, flexBasis: '100%'}}>
        <SelectableList
          key={"menu_sl"}
          value={selectedIndex}
          onChange={(event, index)=>{this.onChange(event, index)}}
          className={styles.Menu}
          selectedItemStyle={{backgroundColor: 'rgba(38, 150, 204, 0.1)'}}
          style={{marginBottom: (hasFixedBottom) ? 40 : undefined}}
        >
          {items}
        </SelectableList>
        <div ref={(ref) => {this.menuContent = ref;}} style={{flex: 1, alignItems: 'stretch', overflowY: 'auto' }}>
        <div style={{height: '100%', padding: (hasTab) ? '0px' : '0px 24px'}}>
          {item}
        </div>
        </div>
      </div>
	  )
  }
}

Menu.propTypes = Object.assign({}, Menu.propTypes, {
  renderItems: PropTypes.func,
  values: PropTypes.object,
  changedValues: PropTypes.object,
  template: PropTypes.array
})

export default Menu;
