import React from 'react';
import PropTypes from 'prop-types'
import EABComponent from '../Component';
import Title from './Title';
import ToolTips from './ToolTips';
import styles from '../Common.scss';

class HeaderTitle extends EABComponent {
    render(){
        let {style, labelStyle, title, mandatory, hints, customStyleClass, customHeaderStyle, template, isCustom} = this.props;

        return (
            <div className={(customStyleClass) ? customStyleClass : styles.headerTitleContainer}>
                <Title template={template} labelStyle={labelStyle} defaultStyle={this.props.defaultStyle} title={title} mandatory={mandatory} customHeaderStyle={customHeaderStyle} isCustom={isCustom}/>
                {hints?<ToolTips hints={hints}/>: null}
            </div>
        )
    }
}

HeaderTitle.propTypes = Object.assign({}, HeaderTitle.propTypes, {
    style: PropTypes.object, 
    labelStyle: PropTypes.object,
    title: PropTypes.any,
    mandatory: PropTypes.bool, 
    hints: PropTypes.any
})

export default HeaderTitle;