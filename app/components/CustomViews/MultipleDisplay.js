import React, { Component, PropTypes}  from 'react';

import EABInputComponent from './EABInputComponent';
import {getValueByView} from '../../actions/GlobalActions';
import {MomentDateTimeFormatForApproval, DateTimeFormat3, MOMENT_TIME_ZONE} from '../../constants/ConfigConstants';
import moment from 'moment';

import _get from 'lodash/fp/get';
import _isEmpty from 'lodash/fp/isEmpty';
import _toUpper from 'lodash/fp/toUpper';
import _trim from 'lodash/fp/trim';
import styles from '../Common.scss'

class MultipleDisplay extends EABInputComponent{

    constructor(props){
        super(props);
        this.state = Object.assign({},this.state,{
            header: '',
            content: '',
            footer: '',
            changedValues: props.changedValues
        });
    }

    genContent = () =>{

      let {header, content, footer} = this.state;
      return (<div>
        <div className={styles.StandardLineHeight}>{header}</div>
        <div className={styles.StandardLineHeight}>{content}</div>
        <div className={styles.StandardLineHeight + ' ' + styles.CommentDateLabel}>{footer}</div>
      </div>);
    }

  componentWillReceiveProps(nextProps) {
    let newState = {};

    if (!window.isEqual(this.state.changedValues, nextProps.changedValues)){
      newState.changedValues = Object.assign({}, nextProps.changedValues);
      if (!_isEmpty(nextProps.changedValues)) {
        this.getValue('header', nextProps.template, nextProps.changedValues);
        this.getValue('content', nextProps.template, nextProps.changedValues);
        this.getValue('footer', nextProps.template, nextProps.changedValues);
      }
    }

    if (!window.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  componentDidMount() {
    let newState = {};
    let {approval} = this.context.store.getState();
    if (_isEmpty(this.state.header) || _isEmpty(this.state.content) || _isEmpty(this.state.footer)){
      newState.changedValues = Object.assign({}, this.props.changedValues);
      if (!_isEmpty(this.props.changedValues)) {
        this.getValue('header', this.props.template, this.props.changedValues);
        this.getValue('content', this.props.template, this.props.changedValues);
        this.getValue('footer', this.props.template, this.props.changedValues);
      }
    }

    if (!window.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  transformValue = (template, value, prefix) =>{
    if (prefix === 'content' && _isEmpty(_trim(value))){
      value = '-';
    }

    if (_toUpper(_get(prefix+'Type', template)) === 'DATE' && value){
      value = moment(value).utcOffset(MOMENT_TIME_ZONE).format(MomentDateTimeFormatForApproval);
    }

    if (_get(prefix+'Prefix', template)) {
      value = _get(prefix+'Prefix', template) + ' ' + value;
    }
    return value;
  }

  getValue = (prefix, template, values) => {
    let mappingKey = prefix + 'ViewMapping';
    let newState ={};
    if (template[mappingKey]) {
      //Call server to get value
      let filterParam = this.genViewParam(template[mappingKey].param, values);
      getValueByView(this.context, template[mappingKey].view, filterParam, filterParam, (resp) =>{
        let value = '';
        if (resp.success && resp.result && resp.result.length > 0) {
          value = _get('result[0].'+ template[mappingKey].fieldValue, resp);
          value = this.transformValue(template, value, prefix);
          newState[prefix] = value;
          this.setState(newState);
        } else {
          value = this.transformValue(template, value, prefix);
          newState[prefix] = value;
          this.setState(newState);
        }
      });
    } else {
      newState[prefix] = this.transformValue(template, _get(template[prefix], values), prefix);
      this.setState(newState);
    }
  }

  genViewParam = (params, values) =>{
    let results = '["01"';
    params.forEach(obj => {
      results = results + ',"' + values[obj] + '"';
    });
    results = results + ']';
    return results;
  }

  render(){
      let items = this.genContent();
      return (
          <div style={{paddingTop: '12px'}}>
              {items}
          </div>
      )
  }
}

export default MultipleDisplay;

