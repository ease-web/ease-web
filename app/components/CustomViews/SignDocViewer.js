import React, { Component } from 'react';

import EABComponent from '../Component';

class SignDocViewer extends EABComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      initPostRequest:false,
      afterSignPostRequest:false
    })
  }

  updateSignDocForm = () => {
    let { id, postUrl, pdfUrl, resultUrl, dmsId, visible, signed, signFieldList } = this.props;
    let { initPostRequest, afterSignPostRequest } = this.state;

    const postRequestReady = (
      id != "" &&
      postUrl != "" &&
      pdfUrl != "" &&
      resultUrl != "" &&
      dmsId != "" &&
      typeof signFieldList != 'undefined');

    if (postRequestReady && !signed && visible && !initPostRequest) {
      this.postRequestToSignDoc();
      this.setState({initPostRequest:true});
    }
    else if (postRequestReady && signed && visible && !afterSignPostRequest) {
      this.postRequestToSignDoc();
      this.setState({afterSignPostRequest:true});
    }
  }

  componentDidUpdate() {
    this.updateSignDocForm();
  }

  componentDidMount() {
    this.updateSignDocForm();
  }

  postRequestToSignDoc = () => {
    this.signDocForm.submit();
  }

  handleIframeOnLoad = (proxy, event) => {
    const location = this.showPdfFrame.contentWindow.location;
    const url = location.href;
    let hash = [];
    if (location.origin + location.pathname == this.props.resultUrl) {
      let resultJson = {};
      let hashes = url.slice(url.indexOf("?") + 1).split("&");
      for (let i = 0; i < hashes.length; i ++) {
        hash = hashes[i].split("=");
        resultJson[hash[0]] = hash[1];
      }
      this.props.onHandleSign(resultJson);
    }
  }

  render() {    
    let { visible, id, postUrl, pdfUrl, resultUrl, auth, docts, dmsId, signFieldList, eSignBorder } = this.props;
    let iframeVisibleStyle = {
      flexGrow: 1,
      borderTop:eSignBorder ? '0' : '1px solid rgba(0,0,0,.12)',
      borderRight:'1px solid rgba(0,0,0,.12)',
      borderLeft:'1px solid rgba(0,0,0,.12)',
      borderBottom:'1px solid rgba(0,0,0,.12)',
      position:'relative',
      overflow:'hidden'
    };

    let iframeHiddenStyle = {
      width:0,
      height:0,
      border:0
    };

    let divShowStyle = {
      display: 'flex',
      flexGrow: 1,
      flexDirection: 'column'
    }

    let divHideStyle = {
      height:'0px'
    }

    /*
     docid = [docId]_[AttId] / [quotationId]_[AttId]
     docurl = The url of PDF that will be signed
     resulturl = the url that SignDoc redirect to after user signs
     dmsid = for SignDoc to handle signed file, "de.softpro.sdweb.plugins.impl.FileDms", "de.softpro.sdweb.plugins.impl.ServletDms"
     cmd_X = to define signature field (X is integer)
     */

    return (
      <div key={id} style={visible? divShowStyle: divHideStyle}>
        <form
          ref={ref=>{this.signDocForm=ref}}
          name={id}
          action={postUrl}
          target={id}
          method="post"
        >
          <input type="hidden" name="docid" value={id} />
          <input type="hidden" name="docurl" value={pdfUrl} />
          <input type="hidden" name="resulturl" value={resultUrl} />
          <input type="hidden" name="auth" value={auth} />
          <input type="hidden" name="docts" value={docts} />
          <input type="hidden" name="dmsid" value={dmsId} />
          {
            signFieldList? (
              signFieldList.map((value, i) =>
                <input key={i} type="hidden" name={"cmd_" + (i + 1)} value={value} />
              )
            ) : null
          }
        </form>
        <iframe
          ref={ref=>{this.showPdfFrame=ref}}
          name={id}
          style={visible? iframeVisibleStyle: iframeHiddenStyle}
          onLoad={this.handleIframeOnLoad}
        >
        </iframe>
      </div>
    );
  }
}

export default SignDocViewer;