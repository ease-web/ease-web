import  React from 'react';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import IconButton from 'material-ui/IconButton';
import {goSupportDocuments} from '../../actions/supportDocuments';
import {goShieldSupportDocuments} from '../../actions/shieldSupportDocuments';
import { getIcon } from '../Icons/index';
import isFunction from 'lodash/isFunction';
import * as _ from 'lodash';

class DocAction extends EABInputComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
        approvalAccess: '',
        supCompleteFlag: false,
        approvalCase: {}
    })
  }

  componentDidMount(){
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    storeListener= ()=>{
        if(this.unsubscribe){
            let {store} = this.context;
            let {approval} = store.getState();
            let newState = {};

            if(!_.isEqual(this.state.approvalAccess, approval.currentTemplate)) {
                newState.approvalAccess = approval.currentTemplate;
            }

            if(!_.isEqual(this.state.supCompleteFlag, approval.supCompleteFlag)) {
                newState.supCompleteFlag = approval.supCompleteFlag;
            }

            if(!_.isEqual(this.state.approvalCase, approval.approvalCase)) {
                newState.approvalCase = approval.approvalCase;
            }

            if(!window.isEmpty(newState)){
                this.setState(newState);
            }
        }
    }

    componentWillUnmount(){
        if(isFunction(this.unsubscribe)){
            this.unsubscribe();
            this.unsubscribe = undefined;
        }
    }

  genContent() {
    let {langMap, store} = this.context;
    let {approval, app:{agentProfile}} = store.getState();
    let {
      template,
      changedValues
    } = this.props;

    let {supCompleteFlag} = this.state;

    let {approvalAccess} = this.state;
    let role;
    if (approvalAccess === 'A' || approval.canApprove) {
        role = 'supervisor';
    } else {
        role = 'agent';
    }

    let {
      id,
      title,
      iconName,
      subType
    } = template;
    let onclickFunc;
    if (subType === 'supportingDoc') {
        if (_.get(this.state.approvalCase, 'isShield')) {
            onclickFunc = goShieldSupportDocuments;
        } else {
            onclickFunc = goSupportDocuments;
        }
    }

    return <div key={id} id={id} className={styles.DetailsItem + ' ' + styles.h12box}>
        <div className={styles.subTextTitle + ' ' + styles.DynColLabel}>
          {window.getLocalizedText(langMap, title)}
        </div>
        <div style={{opacity: (supCompleteFlag) ? undefined : 0.3}}>
            <IconButton onClick={()=>{onclickFunc(this.context, changedValues.applicationId, 'SUBMITTED', role);}}>
            {getIcon(iconName, 'black')}
            </IconButton>
        </div>
    </div>;
  }


  render() {
    return (
      this.genContent()
    );
  }
}

export default DocAction;
