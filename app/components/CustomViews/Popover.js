import React from 'react';
import {TextField, Popover} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import EABCheckboxGroup from './CheckBoxGroup.js';
import EABTextField from './TextField.js';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

import styles from '../Common.scss';

class EABPopover extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      open: false
    });
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  }

  handleOnKeyDodwn = (event) => {
    // This prevents ghost click.
    //event.preventDefault();
    if (event.keyCode === 9){
      this.setState({
        open: false
      });
    }
  }

  handleRequestClose = () =>{
    this.setState({
      open: false,
    });
  }

  renderItems = (fields, template, values, changedValues, key) =>{
    let mandatory = false;
    _.forEach(template.items, (item, index)=>{
      if(this.renderItem(fields, item, values, changedValues, index, key)){
        mandatory = true;
      }
    })
    return mandatory;
  }

  renderItem = (items, iTemplate, values, changedValues, index, key) => {
    let {muiTheme, langMap, lang, validRefValues} = this.context;
    let {id='', value, type='', mandatory} = iTemplate;

    // if it is a new change get default value if changed value is empty
    if(id && !checkExist(changedValues, id) && value){
      changedValues[id] = value
    }

    // check show condition
    if (!showCondition(validRefValues, iTemplate, values, changedValues, this.props.rootValues)) {
      return;
    }

    if(!iTemplate.type)
      return null;

    let viewType = _.toUpper(type);
    let __id = key+'_' + index + '_'+ id;

    if (viewType === 'TEXT') {
      items.push(
        <EABTextField
          ref={ref=>{this[id]=ref}}
          key={__id}
          template={ iTemplate }
          index={index}
          values={values}
          changedValues={changedValues}
          rootValues={this.state.values}
          handleChange={this.props.handleChange}/>
      );
    }
    else if (viewType === 'CHECKBOXGROUP') {
      items.push(
        <EABCheckboxGroup
          id={id}
          ref={id}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          mode={"P"}
          changedValues={changedValues}
          handleChange={this.props.handleChange}
        />
      )
    }

    return mandatory;
  }

  getItemValue = (items=[], values, changedValues) => {
    let {lang, validRefValues} = this.context;
    let returnValue = []
    items.forEach((item) => {
      if (!showCondition(validRefValues, item, values, changedValues, this.props.rootValues)) {
        changedValues[item.id] = undefined;
      }else {
        let {id='', type=''} = item;
        let itemType = _.toUpper(type)
        let storedValues = changedValues[id];
        if (storedValues) {
          if (itemType == 'CHECKBOXGROUP') {
            // map the values to titles, eg 'zh, en' => 'Mandarin, English'
            // stored values is separeted by comma
            let valArr = storedValues.split(',');
            // filter 'other'
            let titleArr = valArr.filter(t=>t!='other').map((val) => {
              // find the options with the specific value
              let opt = item.options.find(option => option.value == val)
              return checkExist(opt, 'title.'+lang)?opt.title[lang]:'';
            }).filter(k=>k!='');
            if(!isEmpty(titleArr)) {
              returnValue.push(titleArr.join(', '));
            }
          } else if (itemType == 'TEXTFIELD' || itemType == 'TEXT') {
            if(!isEmpty(storedValues)) {
              returnValue.push(storedValues);
            }
          }
        }
      }
    });

    return returnValue.join(', ');
  }

  getItemErrMsg = () =>{
    let {template, error} = this.props;
    let {lang} = this.context;
    let msg = '';
    _.forEach(template.items, i=>{
      if(error[i.id] && error[i.id].code){
        msg = _v.getErrorMsg(template, error[i.id].code, lang)
      }
    })
    return msg;
  }

  render() {
    let {lang, langMap, validRefValues} = this.context;
    let {changedValues, anchorEl} = this.state;
    let {style, template, hintText, rootValues, values, handleChange, disabled} = this.props;
    let {id, title, mandatory, value, subType, placeholder, presentation} = template;

    let valueArr = [];
    let cbGroup = []
    let cValue = this.getItemValue(template.items, values, changedValues);
    if (cValue && typeof cValue == 'string') {
      valueArr = cValue.split(',');
    }

    let errorMsg = this.getItemErrMsg();

    // gen items
    let items = []
    let _m = this.renderItems(items, template, values, changedValues, id);

    return (
      <div
        className={ styles.DetailsItem + ' ' + styles.Popover }
        style={style}
      >
        <TextField
          key = {id}
          ref = {id}
          className = { styles.ColumnField }
          hintText= { placeholder?getLocalText(lang, placeholder):null }
          hintStyle= { { bottom:0, fontSize:'12px', lineHeight:'16px' }}
          errorText= { errorMsg }
          floatingLabelText = { this.getFieldTitle(mandatory || _m, title) }
          style= {this.getStyles().columnField}
          errorStyle = {this.getStyles().errorStyle}
          floatingLabelStyle = { this.getStyles().floatingLabelStyle }
          underlineStyle = { {bottom:'22px'} }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          value = { cValue }
          onFocus = { this.handleTouchTap }
          onTouchTap = {this.handleTouchTap}
          onKeyDown = {this.handleOnKeyDodwn}
        />
        <Popover
          style={{width:anchorEl && anchorEl.scrollWidth}}
          open={this.state.open}
          anchorEl={anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
        >
          {items}
        </Popover>
      </div>
    );
  }

}

export default EABPopover;
