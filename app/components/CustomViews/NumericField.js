import React, {Component, PropTypes} from 'react';
import {TextField} from 'material-ui';
import * as _ from 'lodash';

export default class NumericField extends Component {

  static propTypes = Object.assign({}, TextField.propTypes, {
    sign: PropTypes.string,
    maxIntDigit: PropTypes.number,
    decimalPlace: PropTypes.number,
    maxValue: PropTypes.number,
    minValue: PropTypes.number,
    inputStyle: PropTypes.object
  });

  static defaultProps = {
    maxIntDigit: 9,
    decimalPlace: 0
  };

  constructor(props) {
    super(props);
    const {value, sign} = props;
    this.editing = false;
    this.state = {
      value: value,
      displayValue: _.isNumber(value) ? this.getFormattedValue(value + '', sign) : (_.isString(value) ? value : '')
    };
  }

  componentWillReceiveProps(newProps) {
    const {value, sign} = newProps;
    if (this.state.value !== value || this.props.sign !== sign) {
      let caretPos = this.textField.input.selectionStart;
      this.setState({
        value: newProps.value,
        displayValue: _.isNumber(value) ? this.getFormattedValue(value + '', sign) : (_.isString(value) ? value : '')
      }, () => {
        this.textField.input.setSelectionRange(caretPos, caretPos);
      });
    }
  }

  propKeys = ['id', 'sign', 'maxIntDigit', 'decimalPlace', 'maxValue', 'minValue', 'inputStyle'];

  isValidFormat(value) {
    const {sign, maxIntDigit, decimalPlace} = this.props;
    if (sign) {
      value = value.replace(sign, '');
    }
    if (value.replace(/[0-9.,]/g, '').length) {
      return false;
    } else if (!value.match(/^[0-9,]*(.[0-9]*)?$/)) {
      return false;
    }
    let dpIndex = value.indexOf('.');
    let intPart = dpIndex > -1 ? value.substr(0, dpIndex) : value;
    intPart = intPart.replace(/[^0-9]/g, '');
    if (_.isNumber(maxIntDigit) && intPart.length > maxIntDigit) {
      return false;
    }
    if (dpIndex > -1) {
      let decimalPart = value.substr(dpIndex + 1);
      decimalPart = decimalPart.replace(/[^0-9]/g, '');
      if (_.isNumber(decimalPlace) && (decimalPlace === 0 || decimalPart.length > decimalPlace)) {
        return false;
      }
    }
    return true;
  }

  appendDecimalZeros(displayValue, decimalPlace) {
    let retVal = displayValue;
    if (!decimalPlace) {
      return retVal;
    }
    let dpIndex = displayValue.indexOf('.');
    let zerosToAdd = 0;
    if (dpIndex > -1) {
      zerosToAdd = decimalPlace - (displayValue.length - dpIndex) + 1;
    } else {
      retVal += '.';
      zerosToAdd = decimalPlace;
    }
    for (let i = 0; i < zerosToAdd; i++) {
      retVal += '0';
    }
    return retVal;
  }

  getFormattedValue(value, sign) {
    if (!value) {
      return null;
    }
    const {decimalPlace} = this.props;
    let dpIndex = value.indexOf('.');
    let intPart = dpIndex > -1 ? value.substr(0, dpIndex) : value;
    intPart = Number(intPart.replace(/[^0-9]/g, '')) + '';
    intPart = intPart.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    let formattedValue = intPart;
    if (dpIndex > -1 && _.isNumber(decimalPlace) && decimalPlace > 0) {
      let decimalPart = value.substr(dpIndex + 1);
      decimalPart = decimalPart.replace(/[^0-9]/g, '');
      formattedValue += '.' + decimalPart;
    }
    if (!this.editing && _.isNumber(decimalPlace)) {
      formattedValue = this.appendDecimalZeros(formattedValue, decimalPlace);
    }
    if (sign) {
      formattedValue = sign + formattedValue;
    }
    return formattedValue;
  }

  getActualValue(value) {
    return Number(value.replace(/[^0-9.]/g, ''));
  }

  getPropsForTextField() {
    let props = {};
    _.each(this.props, (value, key) => {
      if (this.propKeys.indexOf(key) === -1) {
        props[key] = value;
      }
    });
    return props;
  }

  render() {
    const {id, maxValue, minValue, inputStyle, onChange, onBlur, sign} = this.props;
    const {value, displayValue} = this.state;
    return (
      <TextField
        {...this.getPropsForTextField()}
        id={id}
        ref={ref => { this.textField = ref; }}
        inputStyle={{ textAlign: 'right', ...inputStyle }}
        value={displayValue}
        underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
        onChange={(event, val) => {
          this.editing = true;
          let nextVal = value, nextDisplayVal = displayValue || '';
          let caretOffset = val ? val.length - this.textField.input.selectionStart : 0;
          if (value === 0) {
            if (caretOffset === 0) { // input after 0 or replace the 0
              val = val.replace(/^([^0-9]*)0/, '$1');
            } else { // input before 0
              val = val.replace(/0$/, '');
              caretOffset = 0;
            }
          }
          if (_.isEmpty(val)) {
            nextVal = null;
            nextDisplayVal = '';
          } else if (this.isValidFormat(val)) {
            let actualVal = this.getActualValue(val);
            if ((!_.isNumber(maxValue) || actualVal <= maxValue) && (!_.isNumber(minValue) || actualVal >= minValue)) {
              nextVal = actualVal;
              nextDisplayVal = this.getFormattedValue(val, sign);
            }
          }
          this.setState({
            value: nextVal,
            displayValue: nextDisplayVal
          }, () => {
            let caretPos = nextDisplayVal.length - caretOffset;
            this.textField.input.setSelectionRange(caretPos, caretPos);
            if (value !== nextVal) {
              onChange && onChange(event, nextVal);
            }
          });
        }}
        onBlur={() => {
          this.editing = false;
          this.setState({
            displayValue: _.isNumber(this.state.value) ? this.getFormattedValue(this.state.value && this.state.value + '', sign) : this.state.value
          }, () => {
            onBlur && onBlur();
          });
        }}
      />
    );
  }

}
