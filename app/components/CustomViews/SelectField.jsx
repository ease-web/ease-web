import React from 'react';
import {SelectField, MenuItem, AutoComplete} from 'material-ui';
import styles from '../Common.scss'
import EABComponent from '../Component';
import * as _v from '../../utils/Validation';
import * as _ from 'lodash';

import AutoSuggest from './AutoSuggest';

// if the subtype is not specified, will use this value to decide the type of PICKER dynamically,
// if less than this value, will show SelectField, otherwise will show AutoComplete
const useAutoCompleteFieldLimit = 13;

class EABSelectField extends EABComponent {
  constructor(props){
    super(props);
    this.state=Object.assign({}, this.state, {
      isAutoCompleteMatch: true
    })
  }

  init = () =>{
    let {lang, langMap, optionsMap} = this.context;
    let {template, changedValues, resetValueItems} = this.props;
    let {id, options, min, max, value, subType, interval=1, placeholder, monthDependOn, order} = template;

    var oItems = [];
    let optionsTitleValueArray = [];
    this.keyMap = {};
    this.valueMap = {};
    this.optionsItems = oItems;
    this.optionsTitleValueArr = optionsTitleValueArray;

    let tempOptions = [];

    
    if (_.isString(options) && optionsMap[options]) {
      options = optionsMap[options].options
    }

    if (max === 'currentYear'){
      max = new Date().getFullYear();
    }

    if (monthDependOn && changedValues[monthDependOn] && changedValues[monthDependOn] === new Date().getFullYear()){
       max = new Date().getMonth() + 1;
    }
    
    if (!options && (_.isNumber(min)) && max) {
      options = [];
      if(order == "desc"){
        for (let i = max; i >= min; i-=interval) {
          options.push({
            title: ""+i,
            value: i
          })
        }
      }
      else {
        for (let i = min; i <= max; i+=interval) {
          options.push({
            title: ""+i,
            value: i
          })
        }
      }
    }

    for (let i in options){
      var option = options[i];

      // to support app form's option
      if (!option.value && option.value !== 0) {
        for (let optValue in option) {
          var t = option[optValue]
          option = {
            value: optValue,
            title: t
          }
          break;
        }
      }

      if (optionSkipTrigger(template, option, changedValues, resetValueItems)) {
        continue;
      }


      tempOptions.push(option);
      
    }
    _.forEach(tempOptions, option =>{
      var title = getLocalizedText(langMap, getLocalText(lang, option.title))

      if (this.getSelectFieldType(tempOptions, subType)) {
        if (option.value || option.value === 0) {
          oItems.push(<MenuItem
            key={option.value}
            value={option.value}
            primaryText={ title }
            className={styles.MenuItem}></MenuItem>);
        }
      } else {
        oItems.push(title);
        if (title === undefined || title === null) {
          title = option.value;
        }
        optionsTitleValueArray.push({title: title, value: option.value});
        this.keyMap[title] = option.value;
        this.valueMap[option.value] = title;
      }
    });
    
    this.optionsItems = oItems;
    this.optionsTitleValueArr = optionsTitleValueArray;
  }

  componentWillMount() {
    this.init();
  }

  onNewRequest = (value, index) =>{
    // this.focus();
    this.props.handleChange(null, this.keyMap[value] || value);
  }

  onBlurAutoCompleteField = (e) =>{
    var v = e.target.value;
    this.props.handleChange(null, this.keyMap[v] || v);
  }

  nameFilter = (searchText, key) => {
    var result = false;
    if (_.isString(searchText) && _.isString(key)) {
      result |= AutoComplete.fuzzyFilter(searchText, key);
    }
    return result;
  }

  /**
   * get the select field type by subType, or the items length 
   *
   * @param items - An array of values of select field
   * @param subType - Optional value from template

   * return the type of the select field, AutoComplete or SelectField
  */
  getSelectFieldType = (items, subType) =>{
    subType = _.toUpper(subType);
    if (subType == "AUTOCOMPLETE") {
      return false
    } 
    else if (subType == "SELECTFIELD") {
      return true
    } 
    else {
      return items.length < useAutoCompleteFieldLimit
    }
  }

  render() {
    var self = this;
    this.init(); //reset
    let {lang, langMap, validRefValues} = this.context;
    var rbs = [];
    var {
      template,
      rootValues, 
      values,
      error,
      title,
      changedValues,
      handleChange,
      onBlur,
      requireShowDiff,
      autoWidth,
      labelStyle,
      focusColor,
      fixfloatingLabel,
      ...other
    } = this.props;

    var {
      id,
      value,
      subType,
      options,
      placeholder,
      disabled,
      maxSearchResults
    } = template;

    if (_.isString(options)) {
      options = _.at(this.context.optionsMap, options);
      value = options && options.default;
    }
    var cValue = (id && changedValues && !isEmpty(changedValues[id]))?changedValues[id]:null;
    if (cValue instanceof Array) {
      if (cValue.length) {
        cValue = cValue[0]
      } else {
        cValue = ""
      }
    }
    if (isEmpty(cValue)) {
      if(isEmpty(changedValues)) changedValues={};
      cValue = changedValues[id] = !isEmpty(value)?value:"";
    }

    title = title?title:template.title;

    var errorMsg;
    errorMsg = _v.getErrorMsg(template, _.at(error, `${template.id}.code`)[0], lang);

    if (this.getSelectFieldType(this.optionsItems, subType)) {
      let selectField = <SelectField
        key={id}
        name={id}
        id={id}
        floatingLabelText = {_.isUndefined(template.title) ? undefined : title }
        value= { cValue }
        hintText= { getLocalText(lang, placeholder) }
        errorText = { errorMsg }
        fullWidth = { true }
        disabled={disabled}
        labelStyle = { labelStyle || {
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap'
        }}
        menuStyle = {{marginTop:0}}
        onChange= { (e, index, value)=>{
          {/* this.focus(); */}
          //Set time out to refous the select field button after material ui reset the tab to body
          setTimeout(
            function() {
              if (document.getElementById(id) && document.getElementById(id).getElementsByTagName('button') && document.getElementById(id).getElementsByTagName('button')[0] && document.getElementsByName(id) && document.getElementsByName(id).length === 1)
                document.getElementById(id).getElementsByTagName('button')[0].focus();
            }
          , 500);
          handleChange(id, value);
        }}
        onBlur={onBlur?onBlur:null}
        {...other}
        >{ this.optionsItems }</SelectField>
      return (
        (_.isUndefined(template.title) && template.mandatory) ? 
          <div className={styles.width100 + ' ' + styles.FlexNoWrapContainer}>{selectField}{title ? <span className={styles.mandatoryStar}>*</span> : null}</div> :
          selectField    
      );
    } else {
        if (this.valueMap[cValue]) {
          cValue = this.valueMap[cValue];
        }
        
        return (
          <AutoSuggest 
            id={id}
            classes={styles} 
            template={template} 
            ComponenthandleChange={this.onNewRequest} 
            cValue={cValue} 
            dataSource={this.optionsTitleValueArr}
            errorText = { errorMsg }
            maxSearchResults={maxSearchResults || 8}
            {...other}
            />
          )
    }
  }
}


export default EABSelectField;