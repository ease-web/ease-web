import React from 'react';
import {FlatButton} from 'material-ui';

import EABInputComponent from './EABInputComponent';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';

import styles from '../Common.scss';

class EABTextSelection extends EABInputComponent{
  getCheckBoxValues = (cbValues)=>{
    return cbValues.join(",")
  }

  render() {
    let {lang, muiTheme, validRefValues} = this.context;
    let {changedValues} = this.state;
    var {
      template,
      rootValues,
      values,
      handleChange,
      docId,
      isCustom
    } = this.props;

    var {
      id,
      options,
      title,
      hints,
      mandatory,
      disabled,
      subType,
      boxRegularWidth,
      allLang,
      isSingleSelect,
      placeholder="No Option",
      align,
      normalText,
      noPadding
    } = template;

    var cValue = this.getValue("");

    var valueArr = [];
    if (_.isString(cValue)) {
      valueArr = cValue.split(",");
    }

    var cbGroup = [];
    _.forEach(options, (option, i)=>{
      if (optionSkipTrigger(template, option, changedValues, this.props.resetValueItems)) {
        return;
      }

      let onSelected = ()=>{
        if(isSingleSelect){
          valueArr = [option.value];
          this.requestChange(this.getCheckBoxValues(valueArr));
        }else{
          let index = valueArr.indexOf(option.value);
          if(index > -1){
            valueArr.splice(index, 1);
          } else {
            valueArr.push(option.value);
          }
          this.requestChange(this.getCheckBoxValues(valueArr));
        }
      }

      let selected = (valueArr.indexOf(option.value.toString())>-1)? true:false;

      let backgroundColor =  muiTheme.palette.pageBackgroundColor;
      let fbStyle, fbLabelStyle;
      fbLabelStyle = {};
      let hoverColor;
      if (selected) {
        backgroundColor =  muiTheme.palette.primary2Color;
        fbStyle = this.getStyles().filledBtn;
        fbLabelStyle = (normalText) ? this.getStyles().normalfilledBtnLabel : this.getStyles().filledBtnLabel;
        hoverColor = backgroundColor;
      } else {
        fbStyle = this.getStyles().notSelectedBtn;
        fbLabelStyle = (normalText) ? this.getStyles().normalnotSelectedLabel : this.getStyles().notSelectedLabel;
        if (disabled  || option.disabled) fbStyle = Object.assign(fbStyle, {opacity: '0.1'});
      }

      //Customized for Personal Data Acknowledgement - Client's Accompaniment Text Selection Box
      if(allLang == "Y"){
        let langRows = [];
        for(let i in option.title){
          var styleClass;
          styleClass = styles.TextSelectionTitle;
          if(i == lang){
            styleClass = styles.TextSelectionBoldTitle
          }

          langRows.push(
            <div key={`ts-${option.value}-${i}`} className={styles.TextSelectionTitle}>
              {option.title[i]}
            </div>
          )

        }


        cbGroup.push(
          <FlatButton
            backgroundColor={ backgroundColor }
            hoverColor={hoverColor}
            key={`ts-${option.value}`}
            label={
              <div>
                {langRows}
              </div>
            }
            disabled={disabled || option.disabled}
            style={Object.assign({}, fbStyle, {width: '600px', height: '360px', top: '40px'})}
            labelStyle={fbLabelStyle}
            onTouchTap={onSelected}
          />
        )
      }else{
        cbGroup.push(
          <FlatButton
            backgroundColor={ backgroundColor }
            hoverColor={hoverColor}
            key={`ts-${option.value}`}
            label={getLocalText(lang, option.title)}
            disabled={disabled || option.disabled}
            labelStyle={fbLabelStyle}
            style={fbStyle}
            onTouchTap={onSelected}
          />
        )
      }
    })

    if (_.isEmpty(cbGroup)) {
      cbGroup.push(<label key={"placeholder"} className="CheckBox">{placeholder}</label>)
    }

    let errorMsg = this.getErrMsg();
    let header = null;
    if(allLang){
      let titleRows = [];
      for(let i in title){
        var styleClass;
        styleClass = styles.TextSelectionTitle;
        if(i == lang){
          styleClass = styles.TextSelectionBoldTitle;
        }

        titleRows.push(
          <div key={`ts-title=${id}-${i}`} className={styleClass}>
            {title[i]}
          </div>
        )

      }
      header = (
        <div>
          {titleRows}
        </div>
      )
    }
    else{
      let customHeaderStyle;
      if (align === 'center') {
        customHeaderStyle = styles.alignCenterheaderTitleStyle;
      }
      header = <HeaderTitle template={template} title={title} mandatory={mandatory} hints={hints} customHeaderStyle={customHeaderStyle}/>;
    }

    return (
      <div className={styles.FlexColumnContainer} style={{padding: (noPadding) ? undefined : '15px'}}>
        {header}
        <div className={styles.ErrorMsgContainer} style={{bottom: (allLang) ? '': '20px'}}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}</div>
        <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
          {cbGroup}
        </div>
      </div>
    );
  }
}

export default EABTextSelection;
