import React from 'react';
import {FlatButton, IconButton} from 'material-ui';
import appTheme from '../../theme/appBaseTheme.js';
import {getIcon} from '../Icons/index';
import EABTextField from './TextField.js';
import ErrorMessage from './ErrorMessage';
import * as _ from 'lodash';

import EABInputComponent from './EABInputComponent.js';
const thStyle = { 
  borderBottom:'1px solid '+appTheme.palette.borderColor,
  lineHeight: '24px', 
}

const tdStyle = {
  textAlign: 'left',
  padding: "8px 0"
}

class SATable extends EABInputComponent{

  constructor(props) {
    super(props);
  }


  getStItems = (index, array) =>{
    let { changedValues, validRefValues, readonly } = this.props;
    let channel = changedValues.channel || validRefValues.extra.channel;

    //FA = BROKER
    if ((channel === 'FA' || channel === 'BROKER')){
      return this.genTextViewRows(array, index, readonly);
    } else if (channel === 'AGENCY'){
      let disabled = array[0].indexOf('pending') < 0;
      return this.genTextViewRows(array, 1, readonly || disabled);
    } else {
      return null;
    }
  }

  genTextViewRows = (array, index, disabled)=>{
    let { values, changedValues } = this.props;

    //handle old data for exist / replace
    if (values.rop) {
      if (array[0].indexOf('exist') >= 0 || array[0].indexOf('replace') >= 0) {
        values = values.rop;
        changedValues = changedValues.rop;
      }
    }
    
    let rows = [];
    let sum = 0
    let style = tdStyle;
    for(let i = 0; i < array.length; i++){
        let value = changedValues[array[i]];
        if(!value || value == ''){
          value = 0
        }
        //sum +=  Number(value);
        let template = {
            "id": array[i],
            "max":9999999999,
            "type":"text",
            "subType":"currency",
            "min":0,
            "isCustom":true,
            "mandatory":true,
            "disabled":disabled
          }

        rows.push(
          <td style={style} key={"td" + index + "-" + i}>
            <EABTextField
              ref={ref => { this[array[i]] = ref }}
              key={array[i]}
              style={{ width: '120px' }}
              template={template}
              values={values}
              changedValues={changedValues}
              handleChange={this.props.handleChange} />
          </td>)
      }
      
      // let _tdStyle = Object.assign(style, {"width":"230px"})
      // rows.push(<td style={_tdStyle}>{getCurrency(sum, '$', 0)}</td>);
      return rows;
  }  

  render() {

    let { template, presentation, changedValues } = this.props;
    let { rop, havExtPlans, havAccPlans, havPndinApp, isProslReplace, channel } = changedValues;
    let rdRowStyle = tdStyle;
    var index = 1;
    let errorMsg = this.getErrMsg();

    let ext = null;
    let extArray = ["existLife", "existTpd", "existCi", "existPaAdb", "existTotalPrem"];
    if(havExtPlans && havExtPlans == "Y" && presentation.indexOf('havExtPlans') >= 0 || havAccPlans && havAccPlans == "Y" && presentation.indexOf('havAccPlans') >= 0){

      let eRows = this.getStItems(index, extArray);
      ext = <tr key={"r_"+index}>
          <td style={tdStyle}>1</td>
          <td style={tdStyle}>All Existing Policies</td>
          {eRows}
      </tr>

      index += 1;
    }else{
      for(let jj = 0; jj < extArray.length; jj++){
        changedValues[extArray[jj]] = 0;
      }
    }

    let pend = null;
    let pendArry = ["pendingLife", "pendingTpd", "pendingCi", "pendingPaAdb", "pendingTotalPrem"];
    if(havPndinApp && havPndinApp == "Y"){
      let pRows = this.getStItems(index, pendArry);
      pend = <tr key={"r_"+index}>
              <td style={tdStyle}>2</td>
              <td style={tdStyle}>All Pending Applications</td>
              {pRows}
             </tr>
      index += 1;
    }else{
      for(let j = 0; j < pendArry.length; j++){
        changedValues[pendArry[j]] = 0;
      }
    }

    let replce = null;
    let rpArray = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
    if(isProslReplace && isProslReplace =="Y"){
      let rpRows = this.getStItems(index,rpArray);
      replce = <tr key={"r_"+index}>
                <td style={{tdStyle}}>{3}</td>
                <td style={tdStyle}>Policy to be Replaced</td>
                {rpRows}
               </tr>
    }else{
      for(let ij = 0; ij < rpArray.length; ij++){
        changedValues[rpArray[ij]] = 0;
      }
    }

    if(!ext && !pend && !replce)
      return null;

    return (
		  <div>
            <table style={{borderTop:'1px solid ' + appTheme.palette.borderColor, borderBottom:'1px solid ' + appTheme.palette.borderColor,}}>
              <thead>
                <tr key={"header1"} style={{background: 'rgba(0, 0, 143, 0.07)', color: 'rgb(0, 0, 143)'}} >
                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '4%', padding: '0 24px'})}>Qn</td>
                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '24%', padding: '0 24px'})}>Type of Insurance Application</td>
                    <td colSpan='4' style={{textAlign:"center", padding: '10px 0'}}>Total Sum Assured (S$)</td>
                    <td rowSpan='2' style={Object.assign({},thStyle, {textAlign:"left", padding: '0 24px', width: '16%',})}>Total Annual Premium (S$)</td>
                </tr>
                <tr key={"header2"} style={{background: 'rgba(0, 0, 143, 0.07)', color: 'rgb(0, 0, 143)'}}>
                    <td style={Object.assign({},thStyle,{paddingLeft: 25, width: '14px'})}>Life</td>
                    <td style={Object.assign({},thStyle,{paddingLeft: 25, width: '14px'})}>TPD</td>
                    <td style={Object.assign({},thStyle,{paddingLeft: 25, width: '14px'})}>CI</td>
                    <td style={Object.assign({},thStyle,{paddingLeft: 25, width: '14px'})}>PA/ADB</td>
                </tr>
              </thead>
              <tbody>
                {ext}
                {pend}
                {replce}
              </tbody>
            </table>
            {_.isString(errorMsg) && errorMsg !== '' ? (<div style={{width:'100%'}}>{_.isString(errorMsg) ? <ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/> : null}</div>) : null}
      </div>
    )

// NOTE: lucia changes above
    // return (
		// <div>
    //         <table style={{borderBottom:'1px solid ' + appTheme.palette.borderColor}}>
    //             <tr key={"header1"}>
    //                 <td rowSpan='2' style={Object.assign({},thStyle, {width: '4%'})}>Qn</td>
    //                 <td rowSpan='2' style={Object.assign({},thStyle, {width: '24%'})}>Type of Insurance Application</td>
    //                 <td colSpan='4' style={{textAlign:"center", padding: '10px 0'}}>Total Sum Assured (S$)</td>
    //                 <td rowSpan='2' style={Object.assign({},thStyle, {textAlign:"center", width: '16%'})}>Total Annual Premium (S$)</td>
    //             </tr>
    //             <tr key={"header2"}>
    //                 <td style={Object.assign({},thStyle,{textAlign:"center", width: '14px'})}>Life</td>
    //                 <td style={Object.assign({},thStyle,{textAlign:"center", width: '14px'})}>TPD</td>
    //                 <td style={Object.assign({},thStyle,{textAlign:"center", width: '14px'})}>CI</td>
    //                 <td style={Object.assign({},thStyle,{textAlign:"center", width: '14px'})}>PA/ADB</td>
    //             </tr>
    //             {ext}
    //
    //             {pend}
    //
    //             {replce}
    //         </table>
    //     </div>
    // )
  }
}

export default SATable;
