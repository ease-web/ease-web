import React from 'react';
import EABInputComponent from './EABInputComponent';
import EABCompoent from '../Component';
import {FontIcon, Dialog, FlatButton, Popover} from 'material-ui';

import HeaderTitle from './HeaderTitle';
import TextField from './TextField.js';
import ErrorMessage from './ErrorMessage';

import appTheme from '../../theme/appBaseTheme';
import styles from '../Common.scss'

import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';

import * as _ from 'lodash';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={<div style={{width: 50, height: 30}}>{value}</div>}
      visible={true}
      placement="top"
      key={index}
      getTooltipContainer={()=>document.getElementById('riskSlidertooltipdiv')}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

class RiskSlider extends EABInputComponent{

  render(){
    let {muiTheme, lang, langMap, validRefValues} = this.context;
    let {changedValues} = this.state;
    let {template, rootValues, values} = this.props;
    let {id, min=0, max=100, interval, title, mandatory, prefix, hints, suffix, marks, align} = template;
    
    let defaultValue = 0;
    let cValue = this.getValue(defaultValue)
    let errorMsg = this.getErrMsg();

    return (
     <div style={{flexBasis: '100%', alignItems: 'center'}}>
       <div id='riskSlidertooltipdiv' style={{position: 'relative'}}></div>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints} customStyleClass={(align === 'left') ? styles.leftheaderTitleContainer : undefined}/>
        {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
        <div style={{display: 'flex', justifyContent: 'center'}}>
          <div style={{whiteSpace: "pre-wrap", textAlign:'center', display:'flex', alignItems:'center'}}>{prefix}</div>
            <div style={{flexBasis: '560px', margin:'50px'}}>
              <Slider
                defaultValue={defaultValue || 0}
                name="riskSlider"
                min={min}
                step={interval}
                max={max}
                value={cValue}
                handle={cValue > 0 ? handle : undefined}
                onChange={(value)=>{this.requestChange(value);}}
                onBeforeChange={(value)=>{this.requestChange(value);}}
                marks={marks}
                />
            </div> 
          <div style={{whiteSpace: "pre-wrap", textAlign:'center', display:'flex', alignItems:'center'}}>{suffix}</div>
        </div>     
      </div>
    )
  }
}
export default RiskSlider;