import React from 'react';
import * as _ from 'lodash';

import EABInputComponent from './EABInputComponent';
import EABTextField from './TextField.js'
import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import Card from 'material-ui/Card';

import styles from '../Common.scss';

class NetWorthChart extends EABInputComponent{
 
  getGainLossValue=(isGain)=>{
    let {changedValues} = this.state;
    let {template, error} = this.props;
    let cValue = 0;
    let {id, items: netItems} = template;

    let filterType = isGain?"gainSection": "lossSection";

    netItems.find(filterItem=>filterItem.type == filterType)
      .items.map((item)=>{
        let {id} = item;
        let value = changedValues[id] || 0;
        cValue = cValue + Number(value);
      })
    return cValue;
  }

  getNetValue=()=>{
    return this.getGainLossValue(true) - this.getGainLossValue(false);
  }

  genGainLossItems=(isGain)=>{
    let {lang, muiTheme} = this.context;
    let {changedValues} = this.state;
    let {template, error} = this.props;
    let returnItems = [];
    let {id, items: netItems} = template;

    let filterType = isGain?"gainSection": "lossSection";
    let sectionId = "";
    let desc, subHeaderStyleClassName;
    
    template.items.map((item)=>{
      if(item.type === (isGain?"gainSection": "lossSection")){
        sectionId = item.id;
      }

      if(item.type === (isGain?"gainSection": "lossSection") && checkExist(item, 'desc')){
        desc=getLocalText(lang, item.desc);
      }

      if (isGain) {
        subHeaderStyleClassName = 'NetWorthLeftHeader';
      } else {
        subHeaderStyleClassName = 'NetWorthRightHeader';
      }
    })

    returnItems.push(<div className={styles[subHeaderStyleClassName]}>
        {desc}
    </div>);

    netItems.find(filterItem=>filterItem.type == filterType)
      .items.map((item)=>{
          let _item = _.cloneDeep(item);
          _item.title = null;
          let {id:_id} = _item;
          let title = null;
          if(item.isOther){
            let otherTitleTemplate = {id: _id+"Title", type: 'text', mandatory: (changedValues[_id] && !changedValues[_id+"Title"]) || (changedValues[_id+"Title"] && !changedValues[_id])?true:false, title: 'Other(Please Specify)', max: item.otherMax};
            title = (
              <EABTextField
                key={"net-grow-" + _id+'-'+otherTitleTemplate.id}
                id={otherTitleTemplate.id}
                template={otherTitleTemplate}
                changedValues={changedValues}
                error={error}
                showRedLine={changedValues[_id] && !changedValues[_id+"Title"]}
                handleChange={
                  (otherTitleId, value)=>{
                    changedValues[otherTitleId] = value;
                    this.requestChange(this.getNetValue());
                  }
                }
              />
            )
          }else {
            title = (
              <div style={{maxWidth: '256px'}}>{getLocalText(lang, item.title)}</div>
            )
          }
          let errorMsg=_.at(error, `${_id}.msg`)[0];
          returnItems.push(
            <div key={"net-grow-"+_id} className={styles.FlexColumnNoWrapBasicContainer}>
              {title}
              {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
              <EABTextField
                id={item.id}
                template={_item}
                changedValues={changedValues}
                error={{}}
                isCustom={true}
                showRedLine={item.isOther && !changedValues[_id] && changedValues[_id+"Title"]}
                handleChange={
                  (itemId, value)=>{
                    if(value>=(_item.min||0) && (!_item.max || value<=_item.max)){
                      if(!isNaN(parseInt(value))){
                        changedValues[itemId] = parseInt(value);
                      }
                      else if(isEmpty(value)){
                        changedValues[itemId] = 0;
                      }

                      if(!isEmpty(sectionId)){
                          changedValues[sectionId] = this.getGainLossValue(isGain);
                      }
                      this.requestChange(this.getNetValue());
                    }
                  }
                }
                inputStyle={isGain? Object.assign(this.getStyles().rightTextField, {bottom: '5px'}) : Object.assign(this.getStyles().leftTextField, {bottom: '5px'})}
                style={{alignSelf: isGain?'flex-end': 'flex-start'}}
              />
            </div>
          )
        })

    return returnItems;
  }

  genNetBar = (isGain) =>{
    let {lang} = this.context;
    let {template} = this.props;
    let gain = this.getGainLossValue(true);
    let loss = this.getGainLossValue(false);
    let height = 580;
    let max = gain>loss?gain: loss;
    let value = isGain?gain: loss;
    let itemHeight = value===0? 0: height*(isGain?gain: loss)/max;
    let desc = null;
    // template.items.map((item)=>{
    //   if(item.type === (isGain?"gainSection": "lossSection") && checkExist(item, 'desc')){
    //     desc=getLocalText(lang, item.desc);
    //   }
    // })
    return (
      <div style={{flexBasis: '120px'}}>
        <div style={{display: 'flex', flexDirection: 'column', margin: '0px 12px', background: '#DADADA', justifyContent: 'flex-end', height: height+'px', width: '120px'}}>
          {itemHeight < 5?
              (
                <div style={{padding: '8px', textAlign: 'center', color: 'black'}}>
                  {getCurrency(isGain?gain:loss, '$', 0)}
                </div>
              ): null
          }
          <div style={{flexBasis: itemHeight+'px', background: isGain?'green': 'red'}}>
            {itemHeight > 5?
                (
                  <div style={{padding: '8px', textAlign: 'center', color: '#FFFFFF'}}>
                    {getCurrency(isGain?gain:loss, '$', 0)}
                  </div>
                ): null
            }
          </div>
        </div>
      </div>

    )
  }

  render() {
    let {lang, validRefValues} = this.context;
    let {changedValues} = this.state;
    let {template, rootValues, values, handleChange, disabled, docId} = this.props;
    let {id, title, hints, mandatory, desc} = template;
    let cValue = changedValues[id] = this.getNetValue();
    let errorMsg = this.getErrMsg();

    return (
      <div style={{maxWidth: 800, margin: '0 auto'}} >
        <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
        {typeof errorMsg == "string"?<ErrorMessage template={template} message={errorMsg}/>: null}
        <div style={{fontSize: '20px', paddingBottom: '12px', textAlign: 'center'}}>{getLocalText(lang, desc)}<span className={(cValue>=0)? styles.ShowGreenColor: styles.ShowAlertColor}>{getCurrency(cValue, '$', 0)}</span></div>
        <div style={{display: 'flex', flexWrap: 'nowrap', minWidth: 800, marginLeft: -62  }}>
          <Card containerStyle={{display: 'flex', padding: '24px'}}>
            <div className={styles.FlexColumnNoWrapContainer}>
              {this.genGainLossItems(true)}
            </div>
            {this.genNetBar(true)}
          </Card>
          <Card containerStyle={{display: 'flex', padding: '24px'}}>
            {this.genNetBar(false)}
            <div className={styles.FlexColumnNoWrapContainer}>
              {this.genGainLossItems(false)}
            </div>
          </Card>
        </div>
      </div>
    );
  }
}

export default NetWorthChart;