import React from 'react';
import PropTypes from 'prop-types';
import {FontIcon, Slider} from 'material-ui';

import EABInputComponent from './EABInputComponent';
import HeaderTitle from './HeaderTitle';
import TextField from './TextField.js';
import ErrorMessage from './ErrorMessage';
import ValueDialog from './ValueDialog';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

import appTheme from '../../theme/appBaseTheme';
import styles from '../Common.scss'

const one_coin = require("../../img/one_coin.png");
const three_coins = require("../../img/three_coins.png");

class NormalSlider extends EABInputComponent{

  onValueChange=(value)=>{
    // let {validRefValues} = this.context;
    // let {template, rootValues} = this.props;
    // let {id, warning} = template;
    // let errorCode;
    // if(warning){
    //   eval(`var execFunc = ${warning}; errorCode = execFunc(template, {${id}: value})`);
    // }
    // this.state.errorCode = errorCode;
    this.requestChange(value);
  }

  render(){
    let {muiTheme, lang, langMap, validRefValues} = this.context;
    let {changedValues, errorCode} = this.state;
    let {template, rootValues, values} = this.props;
    let {min=0, max=100, interval=1, title, mandatory, hints, value, subType} = template;

    let cValue = this.getValue((value || min))

    let errorMsg = this.getErrMsg() || _v.getErrorMsg(template, errorCode, lang);;

    let containerStyle;
    if (_.isUndefined(title)){
      containerStyle ={textAlign: 'center'};
    }

    return (
      <div className={styles.FlexContent} style={containerStyle}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
        {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
        <div className={styles.FlexContainer}>
            <div className={styles.NonInputBlueNum + ' ' + styles.alignCenter} onClick={()=>{this.dialog.open()}}>
              { subType==='currency'?getCurrency(cValue, '$', 0): cValue }
            </div>
        </div>
        <ValueDialog ref={(ref)=>{this.dialog=ref;}} template={template} value={cValue} handleChange={this.onValueChange}/>
      </div>
    )
// NOTE: lucia
    // return (
    //   <div className={styles.FlexContent} style={containerStyle}>
    //     <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
    //     {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
    //     <div className={styles.FlexContainer}>
    //       <img className={styles.SliderIcon} src={one_coin}/>
    //       <div className={styles.Slider}>
    //         <div className={styles.NonInputBlueNum + ' ' + styles.alignCenter} onClick={()=>{this.dialog.open()}}>
    //           { subType==='currency'?getCurrency(cValue, '$', 0): cValue }
    //         </div>
    //         <Slider
    //           style={{cursor: 'pointer'}}
    //           name="normalSlider"
    //           min={min}
    //           step={interval}
    //           max={max}
    //           value={cValue}
    //           disableFocusRipple={true}
    //           onChange={(e, value)=>{this.onValueChange(value)}}
    //           sliderStyle={{marginBottom: '16px'}}
    //           />
    //       </div>
    //       <img className={styles.SliderIcon} src={three_coins}/>
    //     </div>
    //     <ValueDialog ref={(ref)=>{this.dialog=ref;}} template={template} value={cValue} handleChange={this.onValueChange}/>
    //   </div>
    // )
  }
}
export default NormalSlider;
