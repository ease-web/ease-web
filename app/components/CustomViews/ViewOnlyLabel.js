let React = require('react');
let mui = require('material-ui');

import _get from 'lodash/fp/get';
import ReadOnlyLabel from './ReadOnlyLabel';
import EABComponent from './EABInputComponent.js';
import ConfigConstants from '../../constants/ConfigConstants';
import styles from '../Common.scss';
import moment from 'moment';

class ViewOnlyLabel extends EABComponent {

   constructor(props) {
      super(props);

      var {
        reference,
        options
      } = this.props.template;

      if (reference && !this.props.template.options) {
        var {
          template
        } = this.props;

        var ref = reference.replace("@", "/")
        var path = ref.split("/");
        var refField = getFieldFmTemplate(template, path, 0, "options");

        if (refField && refField.options) {
          return {
            options: refField.options
          };
        }
      }

    this.state = Object.assign({}, this.state, {
      options: options
    })
  }

  displayOptionsValue = (oValue) =>{
    let id = this.props.template.id;
    var {
      options
    } = this.state;

    let {
      optionsMap
    } = this.context;

    if (options) {
      if(typeof options === 'string' && optionsMap[options]){
        options = optionsMap[options].options; 
      }

      if(options instanceof Array){
        for (var o in options) {
          var opt = options[o];
          if (opt.value == oValue) {
            if(opt.title instanceof Object)
              oValue = getLocalText("en", opt.title);
            else
              oValue = opt.title;
          }
        }
      }
    }
    return oValue;
  }

  getValue = (oValue, template) =>{
    var subType = template.subType;
    if(!!subType)
      subType = subType.toUpperCase();

    if(subType === "DATE"){
      let dateValue = new Date(oValue);
      oValue = dateValue.format(ConfigConstants.DateFormat);
    }

    if (subType === 'ISOSTRING') {
      oValue =  moment(oValue).utcOffset(ConfigConstants.MOMENT_TIME_ZONE).format(ConfigConstants.DateTimeWithTime);
    }

    return oValue;

  }

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      cardReadOnly,
      iTemplateStyle,
      labelStyle,
      floatingLabelStyle, 
      ...others
    } = this.props;

    var {
      id,
      title,
      value, 
      subType, 
      ccy, 
      hideValueCondition, 
      showZeroValue,
      showHyphenZeroValue,
      showAbsoluteValue, 
      hightlightCondition, 
      decimal
    } = template;

    var {
      options
    } = this.state;

    let {
      muiTheme
    } = this.context;
    
    var oValue = null;
    let displayValue = _get(id, values);
    if(id && values && (displayValue || showZeroValue || showHyphenZeroValue)){
      //oValue = (id && values && displayValue)?displayValue:"";
      if (id && values) {
        if (_.toUpper(subType) === 'CURRENCY') {
          var dec = decimal? decimal: 0;
          var fomattedCurrency = getCurrencyByCcy(showAbsoluteValue? Math.abs(displayValue): displayValue, "SGD", dec);
          if (showHyphenZeroValue && !displayValue) {
            oValue = "-";
          } else if (hideValueCondition) {
            var hideIfBetween = _.toUpper(hideValueCondition.type) === 'HIDEIFBETWEEN' && hideValueCondition.min <= displayValue && displayValue <= hideValueCondition.max;
            var hideIfEqual = _.toUpper(hideValueCondition.type) === 'HIDEIFEQUAL' && hideValueCondition.value == displayValue;

            if (hideIfBetween || hideIfEqual) {
              oValue = "";
            } else {
              oValue = fomattedCurrency;
            }
          } else {
            oValue = fomattedCurrency;
          }
        } else if (displayValue) {

          if(displayValue instanceof Array){
            //value array 
            oValue = getLocalTextFromTextArray('en', displayValue, ', ');
          } else if(displayValue instanceof Object){
            //value Object 
            oValue = getLocalText('en', displayValue);
          } else {
            //s
            oValue = this.getValue(displayValue,template);
          }

        } else {
          oValue = "";
        }
      } else {
        oValue = "";
      }

      title = getLocalText('en', title);
      oValue = this.displayOptionsValue(oValue);

      if (cardReadOnly) {
        labelStyle = Object.assign({}, labelStyle, {color: muiTheme.palette.textColor});
        floatingLabelStyle = Object.assign({}, floatingLabelStyle, {color: muiTheme.palette.textColor});
      }
      
      if (hightlightCondition) {
        var highlightIfNotBetween = _.toUpper(hightlightCondition.type) === 'HIGHLIGHTIFNOTBETWEEN' && (displayValue <= hideValueCondition.min || hideValueCondition.max <= displayValue);

        if (highlightIfNotBetween) {
          labelStyle = Object.assign({}, labelStyle, {color:'red'});
          floatingLabelStyle = Object.assign({}, floatingLabelStyle, {color:'red'});
        }
      }
    }else if(value){
       oValue = value;
      if(displayValue)
        oValue = displayValue

      oValue = this.displayOptionsValue(oValue);
    }
    
    let leStyle=  (template.subType && template.subType.toUpperCase() == "QRADIOGROUP") ? {"height":"auto"} : {};
    leStyle.width = width?width:'100%';
    leStyle.backgroundColor = null;

    return <div
      id={"v-" + template.id || "rn"}
      >
        <ReadOnlyLabel
          key = {id}
          className = { "ColumnField" }
          style= {leStyle}
          iTemplateStyle={iTemplateStyle}
          floatingLabelText = { title }
          defaultValue = { oValue}
          labelStyle = { labelStyle }
          floatingLabelStyle = { floatingLabelStyle }
          {...others}
          />
      </div>
  }
}
export default ViewOnlyLabel;
