import React from 'react';
import EABInputComponent from './EABInputComponent';
import * as _ from 'lodash';
import styles from '../Common.scss';
class DuplicateSection extends EABInputComponent{
  render(){
    let {qNum, template, changedValues, id, relationship, renderItems, requestUpdate, dupPChangedValues, error} = this.props;
    let item = [];
    let handleChange = (id, value)=> {
      changedValues[id] = value;
      this.props.requestUpdate();
    }
    let assetItem = template.items.find((item)=>{return item.id == 'assets'});
    if (assetItem) assetItem.dupPChangedValues = dupPChangedValues;
    renderItems(item, template, changedValues, changedValues, error, id, handleChange, relationship, 1);
    return (
      <div>
        {item}
      </div>
    )
  }
}

class DuplicatePanel extends EABInputComponent{
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        options: props.options
      });
  }

  requestUpdate(){
    // Set default value in duplicate panel to allow add assets when there is no value entered before
    let defaultValue = [];
    if (_.filter(this.props.template.items, item => item.id === 'timeHorizon' || item.id === 'assets').length === 2) {
      defaultValue = [{assets: [{}], timeHorizon: '0'}];
    }
    let cValue = this.getValue(defaultValue);
    this.requestChange(cValue);
  }

  renderItems = () => {
    let {qNum, value, template, changedValues, handleChange, relationship, id, renderItems, error={}} = this.props;
    let item =[];
    let items =[];
    let dupPanelTemplate = ([]);

    let templateItem = [];

    let cValue = this.getValue([]);
    let triggerNo = changedValues[template.trigger.id]
    while(cValue.length < triggerNo){
       cValue.push({});
    }

    for(let i=0; i<triggerNo; i++){
      items.push(
          <div className={styles.Divider} style={{margin: '24px 0px'}}/>
      );
      items.push(
        <DuplicateSection error={_.get(error, `${template.id}.${i+1}`)} qNum={qNum+i} template={template} changedValues={cValue[i]} id={id} relationship={relationship} renderItems={renderItems} requestUpdate={this.requestUpdate.bind(this)} dupPChangedValues={cValue}/>
      );
    }

    return items;
  }

  render() {
    let {lang, langMap, muiTheme, store} = this.context;
    let {value, template, changedValues, handleChange, relationship, id, renderItems} = this.props;
    let {title,  mandatory,  hints,  yrofInvest} = template;
    let items = this.renderItems();
 	  let errorMsg = template.error && template.error.msg;

    return (
      <div style={{margin: '0px', padding: '0xp 12px', paddingBottom: '16px'}}>
        {items}
      </div>
    );
  }
}

export default DuplicatePanel;
