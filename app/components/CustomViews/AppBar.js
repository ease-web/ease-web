import React from 'react';
import PropTypes from 'prop-types';
import EABInputComponent from './EABInputComponent';
import {
  Toolbar,
  ToolbarTitle,
  ToolbarGroup,
  IconButton,
  FlatButton,
  TextField,
  DropDownMenu,
  MenuItem
} from 'material-ui';
import * as _ from 'lodash';
import {getIcon} from '../Icons/index';
import styles from '../Common.scss';

const initState = {
  title: '',
  itemsIndex: 0,
  items:[[]],
  searchValue: ''
}

class AppBar extends EABInputComponent {

  constructor(props) {
      super(props);
      if(props.template){
        this.state = Object.assign(_.cloneDeep(initState), props.template);
      }else{
          this.state = _.cloneDeep(initState);
      }
  };

  updateAppbar=(data)=>{
    let appbar = Object.assign(this.state, data);
    this.setState(appbar);
    return appbar;
  }

  initAppbar=(data)=>{
    let appbar = Object.assign(_.cloneDeep(initState), data);
    this.setState(appbar);
    return appbar;
  }

  updateSearchValue=(value)=>{
    this.setState({searchValue: value});
    return value;
  }

  focusSearchValue=()=>{
    this.searchBar.focus();
  }

  genActionItems = () => {
    let {items, itemsIndex=0} = this.state;
    if(isEmpty(items && items[itemsIndex])){
      return;
    }

    let {lang, langMap, muiTheme} = this.context;

    let actionItems = [];
    let _items = items[itemsIndex];
    let icolor;
    let iconColor = muiTheme.palette.primary2Color;
    let disableColor = muiTheme.palette.accent3Color;

    _items.forEach((item, index)=>{
      let {id, type, title, disabled, action, icon: iconName, flatButtonIcon} = item;
      title = _.isString(title)? getLocalizedText(langMap, title): title = getLocalText(lang, title);
      let _key = "appbar-button-"+id+'-'+index;

      if(type === 'iconButton'){
        (disabled) ? icolor = disableColor : icolor = iconColor;
        let icon = getIcon(iconName, icolor);
        actionItems.push(
          <IconButton
            key={_key}
            id={id}
            disabled={disabled}
            onTouchTap={action}
          >
            {icon}
          </IconButton>
        );
      }
      else if(type === 'flatButton'){
        actionItems.push(
          <FlatButton
            key={_key}
            id={id}
            label={title}
            style={{margin: 0}}
            labelStyle={(disabled) ? this.getStyles().LabelinDisabledButton : this.getStyles().LabelinButton}
            disabled = {disabled}
            onTouchTap={action}
            icon={flatButtonIcon}
          />
        );
      } else if (type === 'menu') {
        let {menuValue, options, onChange} = item;
        let optionsEl = _.map(options || [], (option) => {
          return <MenuItem key={option.value} value={option.value} primaryText={option.text} />;
        });
        actionItems.push(
          <DropDownMenu
            key={_key}
            id={id}
            value={menuValue}
            onChange={(event, index, value) => onChange(value)}
            labelStyle={this.getStyles().LabelinButton}
            selectedMenuItemStyle={this.getStyles().selectedMenuItemStyle}
          >
            {optionsEl}
          </DropDownMenu>
        );
      }
    })

    return actionItems;
  }

  componentWillReceiveProps(nextProps){
    if(!isEmpty(nextProps.template)){
      this.setState(Object.assign(this.state, nextProps.template));
    }
  }

  render(){
	  let {muiTheme, router, lang, langMap} = this.context;
    let {title, menu, onSearchChange, maxSearchLength, showShadow, searchValue} = this.state;
    let {style={}, search} = this.props;

    let iconColor = muiTheme.palette.primary1Color;
    let actionsItems = this.genActionItems();
    let menuButton = null;
    let styleClass;
    if(menu){
      if(menu.icon){
        menuButton = (
          <IconButton onTouchTap={menu.action}>
            {getIcon(menu.icon, iconColor)}
          </IconButton>
        )
      }
      else if(menu.title){
        menuButton = (
          /** batch2 - issue #0030808 - button font size are different - Kenny
          <FlatButton onTouchTap={menu.action} label={getLocalText(lang, menu.title)} style={{margin: 0}}/>
          */
          <FlatButton labelStyle={{fontSize: '16px'}} onTouchTap={menu.action} label={getLocalText(lang, menu.title)} style={{margin: 0}}/>
        )
      }

    }

      //note: lucia paddingLeft: '16px',
    let _style = {
      paddingLeft: '24px',
      justifyContent: 'flex-start',
      flex: 1
    }

    if(showShadow === false || this.props.showShadow === false){
      styleClass = styles.Toolbar + ' ' + styles.hideShadow;
    }

    title = _.isString(title)? getLocalizedText(langMap, title): getLocalText(lang, title);

    let searchBar = (
      <TextField
        id="appbarSearch"
        key="searchBar"
        ref={ref=>this.searchBar=ref}
        hintText={title}
        maxLength={maxSearchLength}
        fullWidth={true}
        value={this.state.searchValue}
        onChange={(e)=>{
          if(typeof onSearchChange == "function"){
            this.setState({searchValue: e.target.value});
            onSearchChange(this.state.searchValue, e.target.value);
          }
        }}
      />
    );

    return (

      <Toolbar className={(styleClass) ?  styleClass : styles.Toolbar} style={style}>
        <ToolbarGroup style={Object.assign(_style, style)}>
          {menuButton}
          <ToolbarTitle text={search?searchBar:title} id={search? 'searchBar': 'appBarTitle'}/>
        </ToolbarGroup>
        <ToolbarGroup className={styles.ToolbarGroup}>
          {actionsItems}
        </ToolbarGroup>
      </Toolbar>
	  )
  }
}

AppBar.propTypes={
  template: PropTypes.object,
  showShadow: PropTypes.bool
}

export default AppBar;
