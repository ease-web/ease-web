import React  from 'react';
import {Dialog,FlatButton} from 'material-ui';
import DropZone from './DropZone.jsx';
import {upsertFile, isShowDragDropZone, EXCEED_MAX_FILES_SIZE} from '../../actions/supportDocuments';

import EABComponent from '../Component';
import styles from '../Common.scss';

const CommonFunctions = require('../../CommonFunctions');

class FileUpload extends EABComponent{

    constructor(props){
        super(props);
        this.state = Object.assign({},this.state,{
            showMultiFilesDialog: false,
            showFileSizeLargeDialog: false,
            showFileTypeUnacceptable: false,
            showTotalFilesSizeLargeDialog: false
        });
    }

    componentDidMount() {
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    storeListener=()=>{
        let newState = {};
        let {store} = this.context;
        let {fileUpload} = store.getState();
        let {showTotalFilesSizeLargeDialog} = this.state;
        if (showTotalFilesSizeLargeDialog !== fileUpload.showTotalFilesSizeLargeDialog) {
          newState.showTotalFilesSizeLargeDialog = fileUpload.showTotalFilesSizeLargeDialog;
        }
        if (!window.isEmpty(newState)) {
            this.setState(newState);
        }
    }

    validateFile = (res, applicationId, allFilesValues, viewedList, fileType, fileSize) => {
        let fileName = CommonFunctions.getLocalTime().format('yyyymmdd') + '_' + res.file.name;
        // if (this.validateAllFiles(allFilesValues, fileSize, fileName)) {
            // if (!this.exceedMaxFileSize(fileSize)) {
                this.selectFileToUpload(res, applicationId, fileType, allFilesValues, viewedList, fileSize);
            // }
        // }
    }

    dropZoneOnDrop = (res, applicationId, allFilesValues, viewedList) => {

      if (res.isMultiFiles) {
        this.setState({showMultiFilesDialog: true});
        return;
      }

        let fileType = res.file.type;
        let self = this;
        if (fileType === 'image/png' || fileType === 'image/jpg' || fileType === 'image/jpeg') {
            if (!this.exceedMaxFileSize(res.file.size)) {
                window.imageResize(res.imageUrl, 1024, 768, function(result) {
                    if (result.success) {
                      const resizedFileObject={
                        file:{
                          ...res.file,
                          name:`${res.file.name.split('.')[0]}.png`,
                          size:atob(window.trimBase64(result.imageUrl)).length,
                          type:'image/png',
                        },
                        imageUrl:result.imageUrl
                      };
                      
                      self.validateFile(
                        resizedFileObject,
                        applicationId,
                        allFilesValues,
                        viewedList,
                        fileType,
                        resizedFileObject.file.size
                      )
                    }
                });
            }
        } else if (fileType === 'application/pdf') {
            if (!this.exceedMaxFileSize(res.file.size)) {
                this.validateFile(res, applicationId, allFilesValues, viewedList, fileType, res.file.size);
            }
        } else {
            this.setState({showFileTypeUnacceptable: true});
        }
    }

    closeDropZone = ()=>{
        isShowDragDropZone(this.context, false, {});
    }

    validateAllFiles = (values, inputFileSize, currentFileName) => {
        /**
         * 1. Validate allFilesTotal Size.
         */
        const MAXSIZE = 20 * 1048676;
        let totalSize = 0;

        if (values.mandDocs) {
            for (let key in values.mandDocs) {
                for (let i in values.mandDocs[key]) {
                    totalSize = totalSize + values.mandDocs[key][i].length;
                }
            }
        }

        if (values.optDoc) {
            for (let key in values.optDoc) {
                for (let i in values.optDoc[key]) {
                    totalSize = totalSize + values.optDoc[key][i].length;
                }
            }
        }

        if (values.otherDoc) {
            for (let key in values.otherDoc.values) {
                for (let i in values.otherDoc.values[key]) {
                    totalSize = totalSize +  values.otherDoc.values[key][i].length;
                }
            }
        }

        totalSize = totalSize + inputFileSize;
        if (totalSize > MAXSIZE) {
            this.setState({showTotalFilesSizeLargeDialog:true});
            return false;
        } else {
            return true;
        }
    }

    exceedMaxFileSize = (fileSize) => {
        const MAXSIZE = 3 * 1048576;
        if (fileSize > MAXSIZE){
            this.setState({showFileSizeLargeDialog:true});
            return true;
        }
        return false;
    }

    upsertAttachment = (res, applicationId, attachmentValues, suppDocsValues, viewedList) => {
        let {valueLocation, tabId, isSupervisorChannel} = this.props;
        upsertFile(this.context, res, applicationId, attachmentValues, valueLocation, tabId, suppDocsValues, viewedList, isSupervisorChannel, this.props.rootValues);
    }

    selectFileToUpload = (res, applicationId, fileType, suppDocsValues, viewedList, fileSize) => {
        let fileSizeLabel = fileSize>1048578?(fileSize/1048578).toFixed(1)+'MB':(fileSize/1024).toFixed(1)+'KB';
        let attachmentValues = {
            "fileName": res.file.name,
            "fileSize": fileSizeLabel,
            "fileType": fileType,
            "length": fileSize
        }

        //File type checking
        if (res.imageUrl && res.file.type.indexOf('image') != -1) {
            this.upsertAttachment(res, applicationId, attachmentValues, suppDocsValues, viewedList);
        } else {
            // pdf
            this.upsertAttachment(res, applicationId, attachmentValues, suppDocsValues, viewedList);
        }        
    }

    multiFilesDialog = () => {
      return (
        <Dialog
          open = {this.state.showMultiFilesDialog}
          title = {'NOTICE'}
          actions = {[
            <FlatButton
              label='OK'
              primary={true}
              onTouchTap={() => {
                this.setState({showMultiFilesDialog: false});
              }}
            />
          ]}>
          You have selected multiple files concurrently, please upload one file at a time.
        </Dialog>
      );
    };

    fileSizeLargeDialog = () => {
        return(
            <Dialog open={this.state.showFileSizeLargeDialog}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showFileSizeLargeDialog: false});
                            }}
                        />]
                        }
            >
                File too large!
            </Dialog>
        );
    }

    fileTypeUnacceptable = () => {
        return(
            <Dialog open={this.state.showFileTypeUnacceptable}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showFileTypeUnacceptable: false});
                            }}
                        />]
                        }
            >
                Invalid File Format.
            </Dialog>
        );
    }

    totalFilesSizeLargeDialog = () => {
        let {store} = this.context;
        return (
            <Dialog open={this.state.showTotalFilesSizeLargeDialog}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showTotalFilesSizeLargeDialog: false});
                                store.dispatch({
                                    type: EXCEED_MAX_FILES_SIZE,
                                    showTotalFilesSizeLargeDialog: false
                                });
                            }}
                        />]
                    }
            >
                You have exceeded the maximum file size!
            </Dialog>
        );
    }

    dialogViews = () => {
        return(
            <div>
              {this.multiFilesDialog()}
                {this.fileSizeLargeDialog()}
                {this.fileTypeUnacceptable()}
                {this.totalFilesSizeLargeDialog()}
            </div>
        );
    }

    render(){
        var {
            template,
            changedValues,
            values,
            allFilesValues,
            viewedList,
            ...others
        } = this.props;
        var {
            muiTheme
        } = this.context;

        let applicationId = values.id;
        let title, subTitle;
        if (template && template.title){
            title = template.title;
        }

        if (template && template.subTitle){
            subTitle = template.subTitle;
        }

        let dropZone = <DropZone 
                onDrop={(res)=>{this.dropZoneOnDrop(res, applicationId, allFilesValues, viewedList)}}
                {...others}
                accept=".png,.jpg,.pdf"
                forbidMultiFiles = {true}
                >
                {this.dialogViews()}
                <div style={{width:'100%', height:'48px', background:'lightGray', textAlign:'center'}}
                    id='dragdropzone'>
                    Please drag your file(s) here
                </div>
            </DropZone>

        return (
            (title) ? 
                <div>
                    <div>{title}</div>
                    <div className={styles.subTextTitle}>{subTitle}</div>
                    {dropZone}
                </div>
                :
                dropZone    
        )
    }
}

export default FileUpload;

