var React = require('react');
var SortableMixin = require('../../mixins/react-sortable-mixin.js');
let mui = require('material-ui');
//let SortableListActions = require('../../actions/SortableListActions.js');
//import sortListStyles from '../css/SortableList.css';

let SortableList = React.createClass({
  mixins: [SortableMixin],

  sortableOptions: { draggable: ".drag-elements",  handle: ".drag-area"},

  PropTypes: {
    id: React.PropTypes.string.isRequired,
    items: React.PropTypes.array.isRequired,
    disabledItems: React.PropTypes.array,
    changedValues: React.PropTypes.object.isRequired,
    horizontal: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    presentation: React.PropTypes.number,
    handleChange: React.PropTypes.func
  },

  getDefaultProps() {
    return {
      horizontal: false,
    }
  },

  componentWillUnmount() {

  },

  componentDidMount: function() {
    // Set items' data, key name `items` required
    this.setState({ items: this.props.items });
  },

  componentWillReceiveProps(newProps, newState) {
    this.setState({
      items: newProps.items,
      changedValues: newProps.changedValues
    });
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  getInitialState: function() {
      let {
        id,
        items,
        values,
        changedValues
      } = this.props;

      return {
          items: items,
          values: values,
          changedValues: changedValues
      };
  },
  handleSort: function (e) {
    let {
      changedValues
    } = this.props;
    if(e.oldIndex>e.newIndex){
      let temp = [];
      for(let k=e.newIndex; k<=e.oldIndex; k++){
        temp.push(changedValues[k]);
      }
      for(let m=0; m<temp.length-1;m++){
        let new_seq = e.newIndex+m+1;
        changedValues[new_seq]=temp[m];
        changedValues[new_seq].seq = (new_seq+1)*100;
      }
      changedValues[e.newIndex]=temp[temp.length-1];
      changedValues[e.newIndex].seq = (e.newIndex+1)*100;
    }else if(e.oldIndex<e.newIndex){
      let temp = [];
      for(let k=e.oldIndex; k<=e.newIndex; k++){
        temp.push(changedValues[k]);
      }
      for(let m=0; m<temp.length-1;m++){
        let new_seq = e.oldIndex+m;
        changedValues[e.oldIndex+m]=temp[m+1];
        changedValues[new_seq].seq = (new_seq+1)*100;
      }
      changedValues[e.newIndex]=temp[0];
      changedValues[e.newIndex].seq = (e.newIndex+1)*100;
    }
    //SortableListActions.reflesh();
    this.props.handleSort?this.props.handleSort(e):null;
  },

  _renderItems(items){

    let horizontalStyle={};
    let {
      horizontal,
      presentation,
      disabledItems,
      itemStyle,
      disabled,
      changedValues
    } = this.props;
    if(horizontal){
      let width = presentation? (100/presentation)+'%':'auto';
      horizontalStyle={
        display: 'inline-block',
        width: width
      }
    }

    let rows = [];
    for(let i=0; i<items.length; i++){
      let item = items[i];
      let {id} = changedValues[i];
      rows.push(
        <li
          className={((disabledItems && disabledItems.indexOf(i))>=0 || disabled)?"ignore-elements":"drag-elements"}
          key={`sl-${id}-${i}`}
          style={Object.assign(horizontalStyle, itemStyle)}>
          {item}
        </li>
      );
    }
    return rows;
  },

  render: function() {
    var self = this;
    let {
      id,
      style,
      horizontal,
      presentation,
      disabledItems,
      itemStyle,
      disabled,
      changedValues,
      isLargeStyle,
      ...others
    } = this.props;
    var _items = this._renderItems(this.state.items);

    let horizontalStyle={};
    if(horizontal){
      let width = presentation? (100/presentation)+'%':'auto';
      horizontalStyle={
        display: 'inline-block',
        width: width
      }
    }

    let genList = (items, style = itemStyle) => {
      let list = [];
      this.state.items.map((item, index)=>{
        let {id} = changedValues[index];
        list.push(
          <li 
            className={((disabledItems && disabledItems.indexOf(index))>=0 || disabled)?"ignore-elements":"drag-elements"}
            key={`sl-${id}`}
            style={Object.assign(horizontalStyle, style)}>
            {item}
          </li>
        )
      })
      return list;
    }
    let ulStyle = {listStyleType:'none', width: '100%', padding: 0, margin: '12px auto'};
    let liStyle;
    let liList = [];
    if (isLargeStyle) {
      liList = genList(this.state.items, liStyle);
    }else {
      ulStyle = {listStyleType:'none', width: '100%', marginBottom: '20px', overflow: 'hidden', display: 'flex', flexDirecion: 'row', flexWrap: 'wrap'};
      liStyle = {lineHeight: '1.5em', float: 'left', display: 'inline', width: '30%', padding: '10px', minWidth: '350px'};
      liList = genList(this.state.items, liStyle);
    }

    return (
      <ul style={ulStyle} >
        {liList}
      </ul>
    )
  }
});
module.exports = SortableList;
