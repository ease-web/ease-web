let React = require('react');
let mui = require('material-ui');

import ReadOnlyField from './ReadOnlyField';
import EABComponent from './EABInputComponent.js';
import ConfigConstants from '../../constants/ConfigConstants';
import styles from '../Common.scss';
import {formatDateWithTime, formatDateTimeWithPresent} from '../../../common/DateUtils';
import clone from 'lodash/clone';
import moment from 'moment';
import _getOr from 'lodash/fp/getOr';

import {STATUS} from '../Pages/Pos/Approval/approvalStatus';


class  ViewOnlyField extends EABComponent {

   constructor(props) {
      super(props);

      var {
        reference,
        options
      } = this.props.template;

      if (reference && !this.props.template.options) {
        var {
          template
        } = this.props;

        var ref = reference.replace("@", "/")
        var path = ref.split("/");
        var refField = getFieldFmTemplate(template, path, 0, "options");

        if (refField && refField.options) {
          return {
            options: refField.options
          };
        }
      }

    this.state = Object.assign({}, this.state, {
      options: options
    })
  }

  displayOptionsValue = (oValue) =>{
    var {
      options
    } = this.state;

    var {
      template,
      changedValues,
      resetValueItems
    } = this.props

    let id = template.id;

    let {
      optionsMap
    } = this.context;

    if (options) {
      if(typeof options === 'string' && optionsMap[options]){
        options = optionsMap[options].options;
      }

      if(options instanceof Array){
        for (var o in options) {
          var opt = options[o];

          if (optionSkipTrigger(template, opt, changedValues, resetValueItems)) {
            continue;
          }

          if (opt.value == oValue) {
            if(opt.title instanceof Object)
              oValue = getLocalText("en", opt.title);
            else
              oValue = opt.title;
          }
        }
      }
    }
    return oValue;
  }

  getValue = (oValue, template, stampedValue, values) =>{
    let { subType, dateFormat, mapping } = template;
    let { optionsMap } = this.context;
    let { store } = this.context;
    let { app } = store.getState();
    if(!!subType)
      subType = subType.toUpperCase();

    if(subType === "DATE"){
      let dateValue = new Date(oValue);
      oValue = dateValue.format(dateFormat? dateFormat: ConfigConstants.DateFormat);
    }

    if (subType === 'EXPIRYDATE' && stampedValue) {
      oValue = moment(stampedValue).utcOffset(ConfigConstants.MOMENT_TIME_ZONE).format(ConfigConstants.MomentDateTimeFormatForApproval);
    } else if( subType === 'EXPIRYDATE' ){
      oValue = moment(oValue).add(_getOr(14, 'sysParameterConstant.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION', app), 'd').utcOffset(ConfigConstants.MOMENT_TIME_ZONE).format(ConfigConstants.DateTimeFormat3);
    }

    if(values && mapping && values[mapping.mappingKey] && mapping.mappingValue && mapping.mappingValue.indexOf(values[mapping.mappingKey]) > -1){
      oValue = mapping.displayValue;
    }

    return oValue;

  }

  render() {
    var self = this;

    let {
      muiTheme,
      langMap
    } = this.context;

    let {
      template,
      width,
      values,
      cardReadonlyStyle,
      iTemplateStyle,
      labelStyle,
      floatingLabelStyle,
      ...others
    } = this.props;

    let {
      id,
      title,
      value,
      subType,
      // ccy,
      highLight,
      presentation,
      hideValueCondition,
      showZeroValue,
      showHyphenZeroValue,
      showAbsoluteValue,
      hightlightCondition,
      decimal,
      stampedValue
    } = template;

    let {
      options
    } = this.state;

    let oValue = null;

    let ccy = template.ccy || this.props.ccy || 'SGD';

    if(id && values && (values[id] || showZeroValue || showHyphenZeroValue)){
      //oValue = (id && values && values[id])?values[id]:"";
      if (id && values) {
        if (_.toUpper(subType) === 'CURRENCY') {
          var dec = decimal? decimal: 0;
          var fomattedCurrency = window.getCurrencyByCcy(showAbsoluteValue? Math.abs(values[id]): values[id], ccy, dec);
          if (showHyphenZeroValue && !values[id]) {
            oValue = "-";
          } else if (hideValueCondition) {
            var hideIfBetween = _.toUpper(hideValueCondition.type) === 'HIDEIFBETWEEN' && hideValueCondition.min <= values[id] && values[id] <= hideValueCondition.max;
            var hideIfEqual = _.toUpper(hideValueCondition.type) === 'HIDEIFEQUAL' && hideValueCondition.value == values[id];

            if (hideIfBetween || hideIfEqual) {
              oValue = "";
            } else {
              oValue = fomattedCurrency;
            }
          } else {
            if (values.paymentMode == "L" && id == "initBasicPrem") {
              oValue = fomattedCurrency + " (Single Premium)";
            } else {
              oValue = fomattedCurrency;
            }

            if (id === 'topupPrem') {
              oValue = `${fomattedCurrency} (${window.getLocalizedText(langMap, 'application_single_premium', 'Single Premium')})`;
            }
          }
        } else if (subType && subType.toUpperCase() === 'DATEWITHTIME' && values[id]) {
          let displayTime = new Date(values[id]);
          if (presentation) {
            oValue = clone(formatDateTimeWithPresent(displayTime, presentation));
          } else {
            oValue = clone(formatDateWithTime(displayTime));
          }
        } else if(subType && subType.toUpperCase() === 'APPROVALSTATUS' && values[id]) {
          oValue =  STATUS[values[id]];
        } else if (values[id]) {

          if(values[id] instanceof Array){
            //value array
            oValue = getLocalTextFromTextArray('en', values[id], ', ');
          } else if(values[id] instanceof Object){
            //value Object
            oValue = getLocalText('en', values[id]);
          } else {
            //s
            let sValue;
            sValue = values[stampedValue];
            oValue = this.getValue(values[id], template, sValue, values);
          }

        } else {
          oValue = "";
        }
      } else {
        oValue = "";
      }

      title = getLocalText('en', title);
      oValue = this.displayOptionsValue(oValue);

      labelStyle = Object.assign({}, labelStyle, cardReadonlyStyle);
      floatingLabelStyle = Object.assign({}, floatingLabelStyle, cardReadonlyStyle)

      var requireHighlight = false;
      if (hightlightCondition) {
        requireHighlight = _.toUpper(hightlightCondition.type) === 'HIGHLIGHTIFNOTBETWEEN' && (values[id] <= hideValueCondition.min || hideValueCondition.max <= values[id]);
      }

      if (requireHighlight || highLight) {
        labelStyle = Object.assign({}, labelStyle, {color: muiTheme.baseTheme.palette.alertColor});
        floatingLabelStyle = Object.assign({}, floatingLabelStyle, {color:muiTheme.baseTheme.palette.alertColor});
      }

    }else if(value){
       oValue = value;
      if(values[id])
        oValue = values[id]

      oValue = this.displayOptionsValue(oValue);
    }

    if(!oValue && template.emptyValue){
      oValue = template.emptyValue;
    }

    if (template.subPostfix) {
      oValue += template.subPostfix || '';
    }

    let leStyle=  (template.subType && template.subType.toUpperCase() == "QRADIOGROUP") ? {"height":"auto"} : {};
    leStyle.width = width?width:'100%';
    leStyle.backgroundColor = null;

    let vCls = styles.DetailsItem
    if(template.subType && template.subType.toUpperCase() == "QRADIOGROUP"){
      vCls = styles.radioItem;
    }

    if (template.subType && template.subType.toUpperCase() == "COUNTRYCODE"){
      vCls = styles.DetailsItemCountryCode;
    }

    if (template.subType && template.subType.toLowerCase() == 'multiline') {
      return <div
      id        ={"v-" + template.id || "rn"}
      className ={vCls + " " + styles.AutoHeight}
      >
        <ReadOnlyField
          key                 = {id}
          subType             = {template.subType}
          className           = { "ColumnField" }
          style               = {leStyle}
          iTemplateStyle      = {iTemplateStyle}
          floatingLabelText   = { title }
          defaultValue        = { oValue}
          labelStyle          = { labelStyle }
          floatingLabelStyle  = { floatingLabelStyle }
          {...others}
          />
      </div>
    } else {


      return <div
        id        = {"v-" + template.id || "rn"}
        className = {vCls}
        >
          <ReadOnlyField
            key                 = {id}
            subType             = {template.subType}
            className           = { "ColumnField" }
            style               = {leStyle}
            iTemplateStyle      = {iTemplateStyle}
            floatingLabelText   = { title }
            defaultValue        = { oValue}
            labelStyle          = { labelStyle }
            floatingLabelStyle  = { floatingLabelStyle }
            {...others}
            />
        </div>
    }
  }
}
export default ViewOnlyField;
