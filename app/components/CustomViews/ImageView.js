import React from "react";
import PropTypes from 'prop-types';
import {CircularProgress } from 'material-ui';
import EABComponent from '../Component';
import {getImage} from '../../actions/GlobalActions';
import styles from '../Common.scss';

class ImageView extends EABComponent {
  constructor(props) {
      super(props);
  };

  componentDidMount() {
    this.unmounted = false;
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  // loadImage=(docId, attId)=>{
  //   let {langMap} = this.context;
  //   if(!docId || !attId)
  //     return;

  //   this.setState({errMsg: null, image: null})

  //   getImage(
  //     this.context,
  //     docId,
  //     attId,
  //     (image)=>{
  //       if (!this.unmounted) {
  //         if(image){
  //           this.setState({
  //             image: image
  //           })
  //         } else {
  //           this.setState({
  //             errMsg: getLocalizedText(langMap, "Image cannot find")
  //           })
  //         }
  //       }
  //     }
  //   )
  // }

  render(){
    let {onItemClick, style, docId, attId, imageStyle, styleClass} = this.props;
    
    let url = genFilePath(this.context.store.getState().app, docId, attId);

    return (
      <div className={styles.ImageView + ' ' + styleClass} style={style} onTouchTap={typeof onItemClick == "function"?onItemClick:null}>        
        <img 
          key={`img-${docId}-${attId}`} 
          src={url}
          style={Object.assign({}, style, imageStyle)}
          alt={"Image not found"}
          />
      </div>
    )
  }
}

ImageView.propTypes = Object.assign({}, ImageView.propTypes, {
  onItemClick: PropTypes.func, 
  style: PropTypes.object, 
  imageStyle: PropTypes.object
})

export default ImageView;