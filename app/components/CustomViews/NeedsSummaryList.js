import React from 'react';
import {Paper} from 'material-ui';

import EABTextField from './TextField.js';
import ImageView from './ImageView';
import SortableList from './SortableList.js';
import EABInputComponent from './EABInputComponent';
import HeaderTitle from './HeaderTitle';
import {getIcon} from '../Icons/index';

import styles from '../Common.scss';

import * as _ from 'lodash';
import * as $ from '../../utils/GlobalUtils'
import * as _c from '../../utils/client'
import * as _na from '../../utils/needs';

class NeedsSummaryList extends EABInputComponent{
  constructor(props, context){
    super(props);
    let result;
    result = this.genOptions(props, context);
   // this.state.changedValues[props.template.id]=this.getOptValues(result[0]);
    
    this.state=Object.assign({}, this.state, {
      docId: props.docId || {},
      options: result[0],
      listNoforSurplus: result[1]
    })
  }

  genOptions(props, context){
    let {template, changedValues} = props;
    let {subType, groupId, seperateObj} = template
    let {needs, client} = context.store.getState();
    let {pda, fna, template: nTemplate} = needs;
    let {profile:p, dependantProfiles: dp} = client;
    let {dependants: d} = p;
    let {dependants: sd, applicant} = pda;
    let aspects = (fna.aspects) ? fna.aspects.split(",") : undefined;
    let cValue = fna[template.id];

    let listNoforSurplus;
    let fnaError = {};
    _na.validateFNA(context, needs.template.fnaForm, changedValues, fnaError);

    let seperateId = [];
    _.forEach(seperateObj, obj => {seperateId.push(obj.id)});

    let options = [];
    let depCid =[];
    let inCompleteFlag;
    _.forEach(aspects, a=>{
      let aValue = fna[a];
      let aError = fnaError[a];
      inCompleteFlag = false;
      //Selected the aspect but not yet fil the fna, initialize the value
      if (_.isUndefined(aValue) || _.isEmpty(aValue)){
        aValue = {};
        aValue.owner = p;
        aValue.spouse = _c.getSpouseProfile(context);
        aValue.dependants = [];
        _.forEach(aValue.owner.dependants, (dep) =>{
          if (_.isUndefined(aValue.spouce)) {
            aValue.dependants.push(client.dependantProfiles[dep.cid]);
          } else if(dep.cid != aValue.spouse.cid && client.dependantProfiles[dep.cid]){
            aValue.dependants.push(client.dependantProfiles[dep.cid]);
          }
        });
        inCompleteFlag = true;
      }
     // if(aValue){
        let aSec = _na.getAssetItem(nTemplate.fnaForm, a);
        let oid = _.get(aValue, 'owner.cid');
        if(aSec && _na.filterAspects(nTemplate.fnaForm, d, sd, applicant, a, oid, 'OWNER') && ((_.get(aValue, 'owner.isActive') !=false && fna.isCompleted == false) || (_.get(aValue, 'owner.isActive') ==true && fna.isCompleted == true)) && aValue.owner && _.isBoolean(_.get(aValue, 'owner.init'))){
          (_.isUndefined(_.get(aError, 'owner.code'))) ? inCompleteFlag = false: inCompleteFlag = true;
          //options.push(handleOption(p, dp, aSec, aValue.owner, inCompleteFlag));
          options = options.concat( _na.concatPriorityOption(p, dp, aSec, aValue.owner, seperateObj, seperateId, a, 'OWNER', inCompleteFlag));
        }
        let sid = _.get(aValue, 'spouse.cid');
        if(aSec && _na.filterAspects(nTemplate.fnaForm, d, sd, applicant, a, sid, 'SPOUSE') && ((_.get(aValue, 'spouse.isActive') !=false && fna.isCompleted == false) || (_.get(aValue, 'spouse.isActive') ==true && fna.isCompleted == true))  && aValue.spouse && _.isBoolean(_.get(aValue, 'spouse.init'))){
          (_.isUndefined(_.get(aError, 'spouse.code'))) ? inCompleteFlag = false: inCompleteFlag = true;
          //options.push(handleOption(p, dp, aSec, aValue.spouse, inCompleteFlag));
           options = options.concat( _na.concatPriorityOption(p, dp, aSec, aValue.spouse, seperateObj, seperateId, a, 'SPOUSE', inCompleteFlag));
        }
        if(aSec && !isEmpty(aValue.dependants)){
          _.forEach(aValue.dependants, de=>{
            if(_na.filterAspects(nTemplate.fnaForm, d, sd, applicant, a, de.cid, 'DEPENDANTS') && de && ((de.isActive !=false && fna.isCompleted == false) || (de.isActive ==true && fna.isCompleted == true)) && _.isBoolean(de.init)){
              (_.isUndefined(_.get(aError, 'dependants.' + de.cid + '.code')) && _.get(aError, 'dependants.' +de.cid + '.skip') !== true ) ? inCompleteFlag = false: inCompleteFlag = true;
              //options.push(handleOption(p, dp, aSec, de, inCompleteFlag));
              options = options.concat( _na.concatPriorityOption(p, dp, aSec, de, seperateObj, seperateId, a, 'OWNER', inCompleteFlag));
            }
          })
        }
     // }
    });

    let orderedCid = [{cid: p.cid}];
    let orderedDepCid =[];
    let tempOrderedDepCid = _.cloneDeep(dp) || [];
    _.forEach(Object.keys(tempOrderedDepCid || []), key =>{orderedDepCid.push(tempOrderedDepCid[key]);});
    let orderedOptions = [];
    orderedDepCid.sort(_na.sortProfiles());
    orderedCid = orderedCid.concat(orderedDepCid);

    if(groupId){
      let gids = groupId.split(",");
      _.forEach(gids, gid=>{
        let opts = [];
        for(let i=options.length-1; i>=0; i--){
          let opt = options[i];
          if(opt.aid===gid){
            opts.push(opt);
            options.splice(i,1);
          }
        }
        
        let tempOption;
        _.forEach(orderedCid, obj => {
          tempOption = _.find(opts, ['cid', obj.cid]);
          if (_.isObject(tempOption))
            orderedOptions.push(tempOption);
        });

        let cid="";
        let name="";
        if(orderedOptions.length){
          _.forEach(orderedOptions, opt=>{
            cid+= (cid !=""?",":"") + opt.cid;
            name+= (name!=""?", ":"") + opt.name;
          })
          options.push({
            id: gid,
            cid,
            name,
            aid: gid,
            aTitle: orderedOptions[0].aTitle,
            image: orderedOptions[0].image,
            inCompleteOption: orderedOptions[0].inCompleteOption
          });
        }
      })
    }
    
    listNoforSurplus = options.length;
    if(_.toUpper(subType) === 'SHORTFALL'){
      options = options.filter(o => (o.totShortfall < 0));
    }
    else if(_.toUpper(subType) === 'SURPLUS'){
      options = options.filter(o => o.totShortfall >= 0 || !o.totShortfall);
      listNoforSurplus = listNoforSurplus - options.length;
    }


    if(cValue){
      let _opts = [];
      _.forEach(cValue, c=>{
        let oi=_.findIndex(options, o=>((groupId && groupId.indexOf(c.aid)>-1) || o.cid===c.cid) && o.aid===c.aid);
        if(oi!=-1){
          _opts.push(options[oi]);
          options.splice(oi, 1);
        }
      })

      _.forEach(options, opt=>{
        _opts.push(opt);
      })
      options=_opts;

    }

    return [options, listNoforSurplus];
  }

  componentWillReceiveProps(nextProps){
    let result;
    if (!isEqual(this.props.changedValues, nextProps.changedValues)){
      result = this.genOptions(nextProps, this.context);
      this.setState({
        docId: nextProps.docId,
        changedValues: nextProps.changedValues,
        options: result[0]
      });
    }
  }

  getOptValues = (_options) =>{
    let value = [];
    _.forEach(_options, opt=>{
      value.push({
        cid: opt.cid,
        aid: opt.aid
      });
    })
    return value;
  }

  render() {
    let {lang, muiTheme, langMap} = this.context;
    let {docId, options, changedValues} = this.state;
    let {template, fnaError} = this.props;
    let {id, hints, title, desc, mandatory, subType, changeStyleLvl, type, inComplete, isLgStyle} = template;

    let alertColor, posColor;
    alertColor = muiTheme.palette.errorColor;
    posColor = muiTheme.palette.primary5Color;

    let cValue = this.getValue([]);
    //options = this._getOptionsByReference(template);
    let listNumberColor = muiTheme.palette.accent3Color;
    let textColor = muiTheme.palette.textColor;

    let items = [];
    _.forEach(options, (option, i, options)=>{
      
      let {id: oid, timeHorizon, totShortfall, image, name, aTitle, cid} = option;
      items.push(
          <div key={`prioity-list-${id}-${i}`} style={{display: 'flex', alignItems: 'center', minWidth: '300px'}}>
            <div style={{width: '15px', padding:'15px', color: listNumberColor, fontSize: '24px', fontWeight: '600', marginBottom: '16px'}}>
              {(inComplete) ? '-' : (_.toLower(subType)=='shortfall') ? i + 1 : this.state.listNoforSurplus + 1 + i}
            </div>
          
            <Paper style={Object.assign({minHeight: '100px'}, this.getStyles().paperStyle)}>
              <ImageView key={`prioity-image-${oid}`} style={{width: '120px', height: '80px'}} docId={docId} attId={image}/>
              <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingLeft: '24px'}}>
                <div>
                  <div className={styles.TextTitle + ' ' + styles.alignLeft + ' ' + styles.bold}>
                    {name}
                  </div>
                  <div className={styles.TextTitle + ' ' + styles.alignLeft + ' ' + styles.bold}>
                    {getLocalText(lang, aTitle)}
                  </div>
                </div>
                {(option.inCompleteOption) ? 
                  <div  style={{paddingRight: '40px', minWidth: '300px'}}>
                    <div className={styles.TextTitleFontSize + ' ' + styles.ShowAlertColor + ' ' + styles.bold}> {getLocalizedText(langMap, 'Incomplete')} </div>
                  </div>
                  :
                  <div style={{marginRight: 12, minWidth: (timeHorizon || _.isNumber(totShortfall)) ? '220px' : undefined}}>
                    {(timeHorizon || timeHorizon == 0)?
                      <div className={styles.TextTitle + ' ' +  styles.bold}>Time Horizon: {timeHorizon} years</div>: null
                    }
                    {((totShortfall || totShortfall == 0) && _.isNumber(totShortfall))?
                      <div className={styles.TextTitle + ' ' +  styles.bold}>{option.totShortfall < 0 ?'Shortfall: ': 'Surplus: '}<span style={{color: option.totShortfall < 0 ? alertColor: posColor}}>{getCurrency(Math.abs(totShortfall), '$', 2)}</span></div>: null
                    }
                  </div>
                }
              </div>
            </Paper>
          </div>
      );
    })

    let handleSort = (e) =>{
      e.preventDefault();
      this.requestChange(this.getOptValues(this.state.options));
    }

    return (
      (this.state.options.length > 0) ? 
      <div className={styles.FlexColumnNoWrapContainer + ' ' + styles.ShowMinWidth }>
        {(title) ? <HeaderTitle title={title} mandatory={mandatory} hints={hints}/> : undefined}
        {(desc &&  options && options.length > 0) ? <div className={styles.TextTitle + ' ' + styles.bold}>{getLocalText(lang, desc)}</div> : undefined}
        <div style={{width: 'calc(100% - 48px)'}}>
            <SortableList
              items={items}
              changedValues={options}
              handleSort={handleSort}
              itemStyle={{width: 'calc(100% - 48px)'}}
              isLargeStyle={true}
            />
        </div>
      </div> :
      <div></div>
    );
  }

}

export default NeedsSummaryList;