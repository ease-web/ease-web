import React, { Component, PropTypes}  from 'react';
import {
  Snackbar
} from 'material-ui';
import appTheme from'../../theme/appBaseTheme.js';
import '../Common.scss';

import {
  extendSession,
  closeSessionAlert
} from '../../actions/sessions';
import * as _ from 'lodash';
import Events from 'material-ui/utils/events.js'
let _mount = false;

class SessionAlert extends Component {
  constructor(props){
    super(props);
    this.state = {
      open: false,
      muiTheme: appTheme.getTheme(),
      countdown: 0
    }
  }

  static childContextTypes = {
    muiTheme: PropTypes.object,
  }

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  }

  static contextTypes = {
      store: PropTypes.object
  }

  componentDidMount() {
    this._handleResize();
    _mount = true;
    Events.on(window, 'resize', this._handleResize.bind(this));
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    _mount = false;
    Events.off(window, 'resize', this._handleResize.bind(this));

    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
    }

  }

  storeListener = () =>{
    if(_mount){
      let {
        sessions,
        app
      } = this.context.store.getState();
      let newState = {};

      let isUpdated = false;
      if (this.state.open !== sessions.open) {
        newState.open = sessions.open;
        isUpdated = true;
      }
      if(newState.open || newState.open!==false && this.state.open){
        if(!isEqual(this.state.countdown, sessions.countdown)){
          newState.countdown = sessions.countdown;
          isUpdated = true;
        }
      }
      if(isUpdated){
        this.setState(newState);
      }

    }
  }

  _handleResize(){
    if (this.state.mounted) {
      this.setState({
        muiTheme: appTheme.getTheme(true),
      });
    }
  }

  render(){
    let {
      store
    } = this.context;

	  let {
		  open,
      countdown
	  } = this.state;

    let {
      langMap
    } = store.getState().app;

    let extendText = 'EXTEND'

    let action = <span style={{border: '2px solid white', padding: '5px', boxSizing: 'border-box'}}>{extendText}</span>;
    let message = "Session will expire after XXX minutes.".replace("XXX", countdown + 1);

    return (
      <Snackbar
          open={open}
          message={message}
          action={action}
          autoHideDuration={600000}
          onActionTouchTap={()=>{extendSession(this.context);}}
          onRequestClose={()=>{closeSessionAlert(this.context)}}
      />
	  )
  }
}
export default SessionAlert;
