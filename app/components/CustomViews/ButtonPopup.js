import React, { Component } from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import * as _ from 'lodash';

import {
  startPayment,
  checkPaymentStatus
} from '../../actions/payment';

class PaymentButton extends EABInputComponent {
  constructor(props) {
    super(props);
    
    this.state = Object.assign({}, this.state, {
      status: false,
    })
  }


  handleButtonClick = () => {
    let { template, changedValues } = this.props;
    let isShield = _.get(template, 'isShield');
    startPayment(isShield, this.context, changedValues);
    this.setState({
      showDialog: true
    });
  }

  handleRefreshClick = () => {
    let { template } = this.props;
    let isShield = _.get(template, 'isShield');
    checkPaymentStatus(isShield, this.context, true);
  }

  componentWillMount() {
    this.setState({
      status: this.props.values["trxStatus"] || this.props.changedValues["trxStatus"],
      timestamp: this.props.values["trxStartTime"] || this.props.changedValues["trxStartTime"],
    })
  }

  componentWillReceiveProps(nextProps) {
    let status = nextProps.values["trxStatus"] || nextProps.changedValues["trxStatus"];
    let {
      paymentWindow
    } = this.context.store.getState().payment;

    let showDialog = paymentWindow && !paymentWindow.closed
    
    if (status) {
      this.state.status = status;
    }
    this.setState({
      showDialog: showDialog,
      timestamp: nextProps.values["trxStartTime"]
    })
  }

  componentWillUnmount() {
    this.unmounted = true;
  }
  
  _withInTimeout() {
    return !this.state.timestamp || (this.state.timestamp + 120 * 60000 > new Date().getTime());
  }


  render() {
    const {lang} = this.context;
    let {showDialog, status} = this.state;
    let {template, values, changedValues, style, handleChange} = this.props;

    let {title, id, disabled} = template;

    let inProgress = showDialog;

    return (
      <div className={styles.DetailsItem}
        style={Object.assign({}, style, template.style)}>
        {(!status || status == 'N' || status == 'C' || (status == 'I' && this._withInTimeout()))?<RaisedButton
          key={id}
          ref={id}
          id={id}
          label={status == 'I'?"Refresh":title}
          disabled={disabled || this.state.disabled || inProgress}
          primary={true}
          style={{marginTop:'20px'}}
          onTouchTap={status == 'I'?this.handleRefreshClick:this.handleButtonClick}
          />:null}
          {inProgress?<div key="PaymentLoadingBlock" id="PaymentLoadingBlock" style={{
              zIndex: 5000,
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100vh',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
            }}>
          <CircularProgress key="PaymentLoading" ref="PaymentLoading" title={"Payment in Progress"}/>
        </div>:null}
      </div>
    );
  }
}

export default PaymentButton;