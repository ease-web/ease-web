import React from 'react';
import PropTypes from 'prop-types'
import {FlatButton , Dialog} from 'material-ui';
import TextField from './TextField.js';
import EABInputComponent from './EABInputComponent';
import EABComponent from '../Component';
import EABTextField from './TextField.js'
import ValueDialog from './ValueDialog';
import ErrorMessage from './ErrorMessage';
import * as _ from 'lodash';

import styles from '../Common.scss'

class HorizontalBar extends EABInputComponent{
  render() {
    let {lang, muiTheme} = this.context;

    let {template, changedValues, handleChange, disabled, docId, selectField} = this.props;

    let {max, min, title} = template;
    let cValue = this.getValue(0);
    let colorPercentage = (cValue/max*100);
    let usedColorPercentage;
    let alreadyExistedDiv;

    let currentColor = muiTheme.palette.alertColor; 
    let usedColor = muiTheme.palette.alertColor;

    if (_.isNumber(template.usedPrecentage)) {
      currentColor = muiTheme.palette.primary5Color;
      usedColorPercentage = (template.usedPrecentage/max*100);
      alreadyExistedDiv = <div style={{flexBasis: `${usedColorPercentage}%`, background: usedColor}}/>
    }
    let errorMsg = template.error && template.error.msg;
    if (usedColorPercentage + colorPercentage > 100 || colorPercentage === Infinity){
      usedColorPercentage = 0;
      colorPercentage = 0;
      alreadyExistedDiv = <div style={{flexBasis: `${usedColorPercentage}%`, background: usedColor}}/>
      //errorMsg = 'Assets is over buget, please go to other section to adjust';
      template.valuetoValidate = 0;
      errorMsg = this.props.errorMsg;
    }
    let item = _.cloneDeep(template);
    item.title = null;
    item.type = 'text';
    return (
      <div>
        <div style={{display: 'flex', width: '880px', margin: '0px auto', marginBottom: '12px', alignItems: 'center'}}>
          {/*<ValueDialog ref={ref=>{this.dialog=ref}} template={template} value={cValue} handleChange={(value)=>{
            this.requestChange(value);
            }}/>*/}
          {(selectField) ? <div style={{color: 'black', margin: 'auto', paddingTop: '12px'}}>{selectField}</div> : <div style={{flexBasis: '40%'}}>{getLocalText(lang, title)}</div>}
          <div style={{flexBasis: '60%', background: '#DADADA', display: 'flex', height: '48px', cursor: 'pointer', position: 'relative'}}>
            <div style={{flexBasis: `${colorPercentage}%`, background: currentColor}}/>
            {alreadyExistedDiv}
            <div style={{position: 'absolute', left: '12px', bottom: '0px'}}>
              <EABTextField
                id={item.id}
                template={item}
                changedValues={{[item.id]: cValue}}
                error={{}}
                isCustom={true}
                handleChange={
                  (itemId, value)=>{
                    this.requestChange(value);
                  }
                }
                inputStyle={Object.assign(this.getStyles().leftTextField, {bottom: '5px'})}
                style={{alignSelf: 'flex-start'}}
              />
            </div>
          </div>
        </div>
        {(selectField && _.isString(errorMsg)) ? 
        <div style={{display: 'flex', width: '880px', margin: '0px auto', marginBottom: '12px', alignItems: 'center'}}>
          <div style={{flexBasis: '40%'}}></div>
          {<div style={{flexBasis: '60%'}}><ErrorMessage template={template} message={errorMsg}/></div>}
        </div>
        : _.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
      </div>
    );
  }
}

export default HorizontalBar;