import React from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import {RaisedButton} from 'material-ui';

import DependantItem from './DependantItem'
import ClientProfile from '../Dialogs/ClientProfile';
import EABInputComponent from './EABInputComponent';

import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';

import styles from '../Common.scss';

class DependantBox extends EABInputComponent {
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      depOpts: [],
      clientProfileTemplate: {}
    });
  }

  componentDidMount(){
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount(){
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener = () =>{
    if(this.unsubscribe){
      let state = this.context.store.getState();
      let newState = {};

      if(!isEqual(this.state.depOpts, state.client.profile.dependants)){
        newState.depOpts = state.client.profile.dependants;
      }

      if(!isEqual(this.state.clientProfileTemplate, state.client.template.familyEditDialog)){
        newState.clientProfileTemplate = state.client.template.familyEditDialog;
      }

      if(!isEmpty(newState))
        this.setState(newState);
    }
  }

  getCheckBoxValues = (cbValues) =>{
    return cbValues.join(",");
  }

  render() {
    let {lang, langMap, muiTheme, store, validRefValues} = this.context;
    let {depOpts=[], clientProfileTemplate, changedValues} = this.state;
    let {template, rootValues, values, handleChange, disabled, docId} = this.props;
    let {id, title, placeholder="No Option", isSingleSelect, hints, mandatory, max} = template;

    let valueArr = [];
    let cbGroup = [];
    let cValue = this.getValue("");    
    if (_.isString(cValue)) {
      valueArr = cValue.split(",");
    }

    //remove dependants which is not exist in profile
    let {dependants=[]} = store.getState().client.profile;
    let spouse = _.find(dependants, dependant=>dependant.relationship==='SPO') || {};
    let isSingle = changedValues.applicant === 'single';

    for(let i=valueArr.length-1; i>=0; i--){
      let value = valueArr[i];
      if((!isSingle && value === spouse.cid) || (!_.find(dependants, dependant=>dependant.cid===value))){
        valueArr.splice(i, 1);
      }
    }

    depOpts.forEach((option, index)=>{
      let {cid} = option;
      if(changedValues.applicant !== 'joint' || spouse.cid != cid){
        let onSelected = ()=>{
          if(isSingleSelect){
            valueArr = [cid];
            this.requestChange(this.getCheckBoxValues(valueArr));
          }else{
            let index = valueArr.indexOf(cid);
            if(index > -1){
              valueArr.splice(index, 1);
            } else {
              if(valueArr.length<max){
                valueArr.push(cid);
              }
            }
            this.requestChange(this.getCheckBoxValues(valueArr));
          }
        }

        cbGroup.push(
          <DependantItem
            key={`ic-${id}-${cid}`}
            ref={cid}
            docId={cid}
            member={option}
            selected = {valueArr.indexOf(cid) > -1}
            onCheck={onSelected}
          />
        )
      }
    })

    if (isEmpty(cbGroup)) {
      cbGroup.push(<div key={"placeholder"} className={styles.QuestionAnsewerPadding}>{placeholder}</div>)
    }

    let errorMsg = template.error && template.error.msg;
    let iconCOlor= muiTheme.palette.primary1Color;
   
    return (
      <div style={{paddingBottom: '30px'}}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints} isCustom={true} labelStyle={{textAlign: 'center'}}/>
        {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
        <div className={styles.FlexContainer + ' ' + styles.PageMaxWidth} style={{paddingTop: '14px'}}>
          {cbGroup}
        </div>
        <div className={styles.FlexContainer}>
          <ClientProfile isFamily={true} template={clientProfileTemplate} mandatoryId={['dob']}
          ref={ref=>{this.clientProfile=ref}}/>
          <RaisedButton primary={true} label={getLocalizedText(langMap, "CREATE FAMILY MEMBER")} onTouchTap={()=>{this.clientProfile.openDialog({}, true);}}/>
        </div>
      </div>
    );
  }

}

export default DependantBox;