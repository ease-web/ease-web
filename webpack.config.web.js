var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './app/index',

  devtool: 'source-map',

  output: {
    path: __dirname + '/dist/',
    filename: 'bundle.js'
    // publicPath: 'http://localhost:3000/dist/'
  },
  // entry: {
  //   bundle: './app/index',
  //   bz: './bz/index'
  // },
  //
  // output: {
  //   path: __dirname + '/dist',
  //   filename: '[name].js',
  //   // libraryTarget: 'commonjs2',
  //   publicPath: '../dist/'
  //   // publicPath: 'http://localhost:3000/dist/'
  // },

  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015', 'stage-0']
      }
    },
    {
      test: /\.global\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader'
      })
    },
    {
      test: /^((?!\.global).)*\.css$/,
      exclude: {
          test   : /node_modules/,
          exclude: /node_modules\/file-type/
      },
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
      })
    },
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: "css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader"
      })
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    }, {
      test: /\.(woff|woff2|eot|ttf)$/,
      loader: 'url-loader?limit=8192'
    },
    {
      test: /\.html$/,
      loader: 'html-loader?attrs[]=video:src'
    }, {
      test: /\.mp4$/,
      loader: 'url-loader?limit=100000&mimetype=video/mp4'
    }, {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        loader: 'url-loader?limit=8192'
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      // __DEV__: false,
      // __PROD__: true,
      // "process.env": {
      //   NODE_ENV: JSON.stringify('production')
      // }
      __DEV__: true,
      "process.env": {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   comments: false,
    //   compressor: {
    //     screw_ie8: true,
    //     warnings: false
    //   }
    // }),
    new ExtractTextPlugin({
      filename: 'style.css', allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: `${__dirname}/app/index.ejs`,
      filename: 'index.html',
      inject: 'body',
      title: "EASE - AXA Singapore dev"
    })
  ]
};
