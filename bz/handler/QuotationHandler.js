'use strict';

var _ = require('lodash');

const dao = require('../cbDaoFactory').create();
var bundleDao = require('../cbDao/bundle');
var prodDao = require('../cbDao/product');
var needDao = require('../cbDao/needs');
var quotDao = require('../cbDao/quotation');

const EmailUtils = require('../utils/EmailUtils');
const CommonFunctions = require('../CommonFunctions');
const TokenUtils = require('../utils/TokenUtils');
const QuotUtils = require('./quote/QuotUtils');
const ProposalUtils = require('./quote/ProposalUtils');
const {getQuotDriver, getProfile, getCompanyInfo, getFNAInfo, validateClient, validateFNAInfo, validateClientFNAInfo, checkAllowQuotProduct} = require('./quote/common');
const {formatDatetime, parseDatetime, dayDiff, parseDate} = require('../../common/DateUtils');
const {getCovCodeFromProductId} = require('../../common/ProductUtils');
const {getApplication} = require('../cbDao/application');

const logger = global.logger;
global.quotCache = {
  planDetails: {}
};

var queryQuickQuotes = function (data, session, callback) {
  const {pCid} = data;
  quotDao.queryQuickQuotes(session.agent.compCode, pCid, (quickQuotes) => {
    callback({
      success: true,
      quickQuotes: quickQuotes
    });
  });
};
module.exports.queryQuickQuotes = queryQuickQuotes;

module.exports.deleteQuickQuotes = function (data, session, callback) {
  const {quotIds, pCid} = data;
  Promise.all(_.map(quotIds, (quotId) => {
    return new Promise((resolve) => {
      quotDao.getQuotation(quotId, (quot) => {
        if (quot.pCid === pCid && session.agent.agentCode === quot.agent.agentCode) {
          quotDao.deleteQuotation(quot, false, resolve);
        } else {
          resolve();
        }
      });
    });
  })).then(() => {
    return quotDao.updateQuotViewIndex();
  }).then(() => {
    queryQuickQuotes({ pCid: pCid }, session, callback);
  }).catch((error) => {
    logger.error('Quotation :: deleteQuickQuotes :: failed to delete quick quotes\n', error);
    callback({ success: false });
  });
};

const checkAllowCreateQuot = (requireFNA, agent, pCid, confirm) => {
  if (!requireFNA) {
    return Promise.resolve(true);
  } else {
    return bundleDao.onCreateQuotation(agent, pCid, confirm).then((result) => {
      logger.log('Quotation :: checkAllowCreateQuot :: bundle is fully signed, require confirmation');
      return result && (!result.code || result.code === bundleDao.CHECK_CODE.VALID);
    });
  }
};

const getAvailableInsureds = (quotation, planDetails, requireFNA) => {
  const promises = [];
  promises.push(QuotUtils.getRelatedProfiles(quotation.pCid));
  if (requireFNA) {
    promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.PDA));
    promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.NA));
  }
  return Promise.all(promises).then(([profiles, pda, fna]) => {
    const quotDriver = getQuotDriver(quotation);
    let availableApplicants = profiles;
    if (quotDriver.getAvailableApplicants) {
      availableApplicants = quotDriver.getAvailableApplicants(quotation, planDetails, profiles, { pda, fna });
    }
    let eligibleClients = profiles;
    if (quotDriver.getEligibleClients) {
      eligibleClients = quotDriver.getEligibleClients(quotation, planDetails, profiles, { pda, fna });
    }
    let availableClassMap = {};
    if (quotDriver.getAvailableClasses) {
      availableClassMap = quotDriver.getAvailableClasses(quotation, planDetails, eligibleClients);
    }
    return _.map(availableApplicants, (profile) => {
      return {
        profile,
        availableClasses: availableClassMap[profile.cid],
        valid: validateClient(profile),
        eligible: !!_.find(eligibleClients, c => c === profile)
      };
    });
  });
};

module.exports.initQuotation = function(data, session, callback) {
  const {quickQuote, iCid, pCid, confirm, productId} = data;
  const ccy = data.params && data.params.ccy;
  const requireFNA = session.agent.channel.type === 'AGENCY' && !quickQuote;
  checkAllowCreateQuot(requireFNA, session.agent, pCid, confirm).then((allowCreate) => {
    if (!allowCreate) {
      callback({
        success: true,
        requireConfirm: true
      });
      return;
    }
    logger.log('Quotation :: initQuotation :: creating new quotation of product:', productId);
    var promises = [];
    promises.push(getProfile(iCid));
    promises.push(getProfile(pCid && pCid !== iCid ? pCid : null));
    promises.push(prodDao.getProductSuitability());
    promises.push(getCompanyInfo());
    if (requireFNA) {
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.FE));
      promises.push(needDao.getItem(pCid || iCid, needDao.ITEM_ID.NA));
    }
    return Promise.all(promises).then((args) => {
      let [insured, proposer, suitability, companyInfo, fe, fna] = args;
      proposer = proposer || insured;
      return getPlanDetails(session, productId, true).then((planDetails) => {
        var basicPlan = planDetails[getCovCodeFromProductId(productId)];
        if (basicPlan) {
          if (requireFNA) {
            let suitResult = getQuotDriver().checkProdSuitForQuot(suitability, { fna, fe }, basicPlan);
            if (suitResult !== true) {
              logger.log('Quotation :: initQuotation :: product is not allowed to be quoted');
              callback({
                success: true,
                errorMsg: suitResult,
                profile: proposer
              });
              return;
            }
          }
          let quotation;
          if (basicPlan.covCode === 'ASIM') { // TODO: check flag from planDetail
            logger.log('Quotation :: initQuotation :: profiles:', proposer.cid);
            quotation = QuotUtils.genShieldQuotation(basicPlan, session.agent, companyInfo, proposer, quickQuote);
          } else {
            logger.log('Quotation :: initQuotation :: profiles:', insured.cid, proposer.cid);
            quotation = QuotUtils.genQuotation(basicPlan, session.agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna);
          }
          if (!quotation.extraFlags) {
            quotation.extraFlags = {};
          }
          quotation.extraFlags.init = true;
          let calResult = JSON.parse(getQuotDriver(quotation).calculateQuotation(quotation, planDetails));
          quotation = session.quotation = calResult.quotation || quotation;
          delete quotation.extraFlags.init;
          const warnings = QuotUtils.getQuotationWarnings(basicPlan, quotation, suitability);
          return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
            callback({
              success: true,
              errors: calResult.errors,
              quotation: quotation,
              planDetails: getPlanDetails4Client(planDetails),
              inputConfigs: calResult.inputConfigs,
              quotWarnings: warnings,
              availableInsureds: availableInsureds,
              profile: proposer
            });
          });
        } else {
          callback({
            success: false,
            error: 'Basic plan details not found!'
          });
        }
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: initQuotation :: failed to init quotation\n', err);
    callback({ success: false });
  });
};


const prefetchIllustrationRates = function (planDetails) {
  let p = Promise.resolve();
  let hasPrefetch = false;
  _.each(planDetails, (planDetail, covCode) => {
    let prefetchRates;
    if (planDetail.formulas.getPrefetchIllustrationRates) {
      prefetchRates = getQuotDriver().runFunc(planDetail.formulas.getPrefetchIllustrationRates);
    }
    if (prefetchRates) {
      _.each(prefetchRates, (config, rateKey) => {
        logger.log('Quotation :: prefetchIllustrationRates :: caching rates:', covCode, rateKey);
        const {docId} = config;
        if (docId) {
          hasPrefetch = true;
          p = p.then(() => {
            return new Promise((resolve) => {
              dao.getDoc(docId, (doc) => {
                if (doc && doc.rates && global.quotCache.planDetails[covCode]) {
                  logger.log('Quotation :: prefetchIllustrationRates :: rates fetched:', docId);
                  global.quotCache.planDetails[covCode].rates[rateKey] = doc.rates;
                }
                resolve();
              });
            });
          });
        }
      });
    }
  });
  if (hasPrefetch) {
    global.quotCache.fetching = p;
    p = p.then(() => {
      logger.log('Quotation :: prefetchIllustrationRates :: rates prefetch completed');
      delete global.quotCache.fetching;
    });
  }
};

const _preparePlanDetails = function (compCode, productId, callback) {
  return new Promise((resolve) => {
    prodDao.getProduct(productId, function(basicPlan) {
      if (!basicPlan) {
        throw new Error('Basic plan not found');
      }
      logger.log('Quotation :: preparePlanDetails :: found basic plan detail:', basicPlan.covCode);
      resolve(basicPlan);
    });
  }).then((basicPlan) => {
    const planDetails = {};
    planDetails[basicPlan.covCode] = basicPlan;
    const promises = _.map(basicPlan.riderList, (rider) => {
      return new Promise((resolve) => {
        prodDao.getRiderByCovCode(compCode, rider.covCode, (riderDetail) => {
          if (riderDetail && riderDetail.covCode) {
            logger.log('Quotation :: preparePlanDetails :: found rider details:', rider.covCode);
            planDetails[riderDetail.covCode] = riderDetail;
          } else {
            logger.error('Quotation :: preparePlanDetails :: cannot find plan detail:', rider.covCode);
          }
          resolve();
        });
      });
    });
    return Promise.all(promises).then(() => planDetails);
  });
};

const getPlanDetails = (session, productId, noCache, requireIlluRates, callback) => {
  let baseProductCode = getCovCodeFromProductId(productId);
  if (noCache || !global.quotCache.planDetails[baseProductCode]) {
    return _preparePlanDetails(session.agent.compCode, productId).then((planDetails) => {
      prefetchIllustrationRates(planDetails);
      _.each(planDetails, (planDetail, covCode) => {
        logger.log('Quotation :: getPlanDetails :: update plan details in cache:', covCode);
        global.quotCache.planDetails[covCode] = planDetail;
      });
      return Promise.resolve(requireIlluRates && global.quotCache.fetching).then(() => {
        callback && callback(baseProductCode, planDetails);
        return planDetails;
      });
    });
  } else {
    const planDetails = {};
    const bpDetail = global.quotCache.planDetails[baseProductCode];
    planDetails[baseProductCode] = bpDetail;
    _.each(bpDetail.riderList, (rider) => {
      planDetails[rider.covCode] = global.quotCache.planDetails[rider.covCode];
    });
    return Promise.resolve(requireIlluRates && global.quotCache.fetching).then(() => {
      callback && callback(baseProductCode, planDetails);
      return planDetails;
    });
  }
};
module.exports.getPlanDetails = getPlanDetails;

/**
 * Remove unnecessary details for client side.
 *
 * @param {*} planDetails
 */
const getPlanDetails4Client = function (planDetails) {
  const details = {};
  _.each(planDetails, (planDetail, covCode) => {
    const detail = _.clone(planDetail);
    delete detail.rates;
    delete detail.formulas;
    details[covCode] = detail;
  });
  return details;
};

var quote = function(data, session, callback) {
  getPlanDetails(session, data.quotation.baseProductId).then((planDetails) => {
    // TODO:: add globalValidation !!
    var result = JSON.parse(getQuotDriver(data.quotation).calculateQuotation(data.quotation, planDetails));
    logger.log('Quotation :: quote :: finish calc');

    var finalQuot = result.quotation || data.quotation;
    finalQuot.lastUpdateDate = formatDatetime(new Date());

    var basicPlan = planDetails[getCovCodeFromProductId(data.quotation.baseProductId)];
    return prodDao.getProductSuitability().then((suitability) => {
      const warnings = QuotUtils.getQuotationWarnings(basicPlan, data.quotation, suitability);
      callback({
        success: !result.error && !data.error,
        errors: result.errors,
        quotWarnings: warnings,
        quotation: finalQuot,
        inputConfigs: result.inputConfigs
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: quote :: failed to quote\n', error);
    callback({ success: false });
  });
};
module.exports.quote = quote;

var getQuotation = function (data, session) {
  return new Promise((resolve, reject) => {
    if (data.quotation) {
      resolve(data.quotation);
    } else if (data.quotId) {
      if (session.quotation && session.quotation.id === data.quotId) {
        resolve(session.quotation);
      } else {
        quotDao.getQuotation(data.quotId, (quotation) => {
          resolve(quotation);
        });
      }
    } else {
      throw new Error('Cannot find quotation in session and request');
    }
  });
};

const prepareQuotPage = function (session, quotation, planDetails, requireFNA) {
  const bpDetail = planDetails[quotation.baseProductCode];
  return prodDao.getProductSuitability().then((suitability) => {
    const warnings = QuotUtils.getQuotationWarnings(bpDetail, quotation, suitability);
    return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
      return new Promise((resolve) => {
        quote({ quotation }, session, (result) => {
          resolve({
            success: result.success,
            errors: result.errors,
            quotation: result.quotation,
            planDetails: getPlanDetails4Client(planDetails),
            inputConfigs: result.inputConfigs,
            quotWarnings: warnings,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  });
};

module.exports.requote = function (data, session, cb) {
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductId, true).then((planDetails) => {
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
        cb(result);
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: requote :: failed to requote\n', error);
    cb({ success: false });
  });
};

const checkAllowCloneQuot = (quotation, bpDetail) => {
  const currentDate = new Date();
  if (dayDiff(currentDate, parseDatetime(quotation.createDate)) > 90) { // check if BI is older than 90 days
    logger.log('Quotation :: checkAllowCloneQuot :: quotation over 90 days');
    return Promise.resolve(false);
  }

  if (QuotUtils.hasCrossAge(quotation, bpDetail)) {
    logger.log('Quotation :: checkAllowCloneQuot :: proposer or insured age changed');
    return Promise.resolve(false);
  }

  return QuotUtils.getQuotClients(quotation).then((profiles) => {
    if (QuotUtils.hasProfileUpdate(quotation, profiles)) {
      logger.log('Quotation :: checkAllowCloneQuot :: client profile is updated');
      return false;
  }
    return new Promise((resolve) => {
      prodDao.canViewProduct(quotation.baseProductId, profiles[0], profiles[1], (product) => {
        if (!product) {
          logger.log('Quotation :: checkAllowCloneQuot :: product cannot be viewed');
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });
  });
};

module.exports.cloneQuotation = function (data, session, cb) {
  const {confirm} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductId, true).then((planDetails) => {
      const bpDetail = planDetails[quotation.baseProductCode];
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowCloneQuot(quotation, bpDetail).then((allowClone) => {
        if (!allowClone) {
          cb({ success: true, allowClone: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then((allowCreate) => {
          if (!allowCreate) {
            cb({ success: true, requireConfirm: true });
            return;
          }
          const currentDate = new Date();
          quotation.id = null;
          quotation.lastUpdateDate = formatDatetime(currentDate);
          quotation.createDate = formatDatetime(currentDate);
          return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
            cb(Object.assign(result, { allowClone: true }));
          });
        });
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: cloneQuotation :: Failed to clone quotation\n', err);
    cb({ success: false });
  });
};

const checkAllowRequoteInvalidated = function (agent, quotation, requireFNA) {
  return QuotUtils.getQuotClients(quotation).then((profiles) => {
    const invalidProfile = _.find(profiles, profile => !validateClient(profile));
    if (invalidProfile) {
      logger.log('Quotation :: checkAllowRequoteInvalidated :: mandatory fields are missing');
      return false;
    }
    return Promise.resolve(requireFNA && getFNAInfo(quotation.pCid)).then((fnaInfo) => {
      if (requireFNA && !validateFNAInfo(fnaInfo)) {
        logger.log('Quotation :: checkAllowRequoteInvalidated :: FNA is not completed');
        return false;
      }
      return checkAllowQuotProduct(agent, quotation.baseProductId, profiles[0], profiles[1], fnaInfo).then((allowQuot) => {
        if (!allowQuot) {
          logger.log('Quotation :: checkAllowRequoteInvalidated :: product not viewable to client');
          return false;
        }
        return true;
      });
    });
  });
};

module.exports.requoteInvalid = function (data, session, callback) {
  const {confirm} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductId, true).then((planDetails) => {
      const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
      return checkAllowRequoteInvalidated(session.agent, quotation, requireFNA).then((allowRequote) => {
        if (!allowRequote) {
          callback({ success: true, allowRequote: false });
          return;
        }
        return checkAllowCreateQuot(requireFNA, session.agent, quotation.pCid, confirm).then((allowCreate) => {
          if (!allowCreate) {
            callback({ success: true, requireConfirm: true });
            return;
          }
          const currentDate = new Date();
          quotation.id = null;
          quotation.lastUpdateDate = formatDatetime(currentDate);
          quotation.createDate = formatDatetime(currentDate);
          quotation.fund = null;
          return QuotUtils.updateClientFields(quotation, planDetails, requireFNA).then(() => {
            return prepareQuotPage(session, quotation, planDetails, requireFNA).then((result) => {
              callback(Object.assign(result, { allowRequote: true }));
            });
          });
        });
      });
    });
  }).catch((err) => {
    logger.error('Quotation :: requoteInvalidated :: Failed to requote\n', err);
    callback({ success: false });
  });
};

module.exports.resetQuot = function (data, session, callback) {
  const {keepConfigs, keepPolicyOptions, keepPlans, keepFunds} = data;
  getQuotation(data, session).then((quotation) => {
    if (!keepConfigs) {
      quotation.ccy = null;
      quotation.paymentMode = null;
      quotation.isBackDate = 'N';
    }
    if (!keepPolicyOptions) {
      _.each(quotation.policyOptions, (value, key) => {
        quotation.policyOptions[key] = null;
      });
    }
    if (!keepPlans) {
      quotation.plans = null;
    }
    if (!keepFunds) {
      quotation.fund = null;
    }
    quotation.extraFlags.reset = true;
    quote(data, session, (result) => {
      if (result.quotation && result.quotation.extraFlags) {
        delete result.quotation.extraFlags.reset;
      }
      callback(result);
    });
  }).catch((err) => {
    logger.error('Quotation :: resetQuot :: Failed to reset quotation\n', err);
    callback({ success: false });
  });
};

module.exports.updateRiderList = function (data, session, callback) {
  const {quotation, newRiderList} = data;
  const plans = [quotation.plans[0]];
  _.each(newRiderList, (covCode) => {
    if (covCode !== quotation.baseProductCode) {
      const existingPlan = _.find(quotation.plans, (plan) => plan.covCode === covCode);
      if (existingPlan) {
        plans.push(existingPlan);
      } else {
        plans.push({ covCode: covCode });
      }
    }
  });
  quotation.plans = plans;
  quote(data, session, callback);
};

module.exports.getFundDetails = function (data, session, cb) {
  let {productId, invOpt, paymentMethod} = data;
  if (!paymentMethod) {
    cb({
      success: true,
      funds: []
    });
    return;
  }
  getPlanDetails(session, productId).then((planDetails) => {
    let bpDetail = planDetails[getCovCodeFromProductId(productId)];
    return QuotUtils.getFunds(session.agent.compCode, bpDetail.fundList, paymentMethod, invOpt).then((funds) => {
      cb({
        success: true,
        funds: funds
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: getFundDetails : failed to get funds\n', error);
    cb({ success: false });
  });
};

module.exports.allocFunds = function(data, session, callback) {
  let {invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs} = data;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductId).then((planDetails) => {
      let paymentMethod = quotation.policyOptions.paymentMethod;
      let fundCodes = _.map(fundAllocs, (alloc, fundCode) => fundCode);
      fundCodes = _.filter(fundCodes, funcCode => (fundAllocs[funcCode] || (hasTopUpAlloc && topUpAllocs[funcCode])));
      return QuotUtils.getFunds(session.agent.compCode, fundCodes, paymentMethod, invOpt).then((funds) => {
        let selectedFunds = _.map(funds, (fund) => {
          return {
            fundCode: fund.fundCode,
            fundName: fund.fundName,
            alloc: fundAllocs[fund.fundCode],
            topUpAlloc: hasTopUpAlloc ? topUpAllocs[fund.fundCode] : null
          };
        });
        quotation.fund = {
          invOpt: invOpt,
          portfolio: invOpt === 'buildPortfolio' ? portfolio : null,
          funds: selectedFunds
        };
        quote(data, session, callback);
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: allocFunds :: failed to allocate funds\n', error);
    callback({ success: false });
  });
};

module.exports.updateClients = (data, session, callback) => {
  getQuotation(data, session).then((quotation) => {
    const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
    return Promise.resolve(!requireFNA || validateClientFNAInfo(quotation.pCid)).then((completed) => {
      if (!completed) {
        callback({
          success: true,
          errorMsg: 'FNA is invalidated. Please visit and complete FNA again.'
        });
        return;
      }
      return getPlanDetails(session, quotation.baseProductId).then((planDetails) => {
        return getAvailableInsureds(quotation, planDetails, requireFNA).then((availableInsureds) => {
          callback({
            success: true,
            availableInsureds: availableInsureds
          });
        });
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: updateClients :: failed to update client info\n', error);
    callback({ success: false });
  });
};

module.exports.selectInsured = (data, session, callback) => {
  const {cid, covClass} = data;
  getQuotation(data, session).then((quotation) => {
    if (covClass) {
      const promises = [];
      promises.push(getProfile(quotation.pCid));
      promises.push(getProfile(cid));
      promises.push(getCompanyInfo());
      return Promise.all(promises).then(([proposer, insured, companyInfo]) => {
        return getPlanDetails(session, quotation.baseProductId).then((planDetails) => {
          const basicPlan = planDetails[quotation.baseProductCode];
          quotation.insureds[cid] = QuotUtils.genBasicQuotation(basicPlan, session.agent, companyInfo, proposer, insured);
          quote({ quotation: quotation }, session, (result) => {
            const resultQuot = result.quotation || quotation;
            resultQuot.insureds[cid].plans[0].covClass = covClass;
            quote({ quotation: resultQuot }, session, callback);
          });
        });
      });
    } else {
      delete quotation.insureds[cid];
      quote(data, session, callback);
    }
  }).catch((error) => {
    logger.error('Quotation :: selectInsured :: failed to select insured\n', error);
    callback({ success: false });
  });
};

module.exports.updateShieldRiderList = (data, session, callback) => {
  const {quotation, newRiderList, cid} = data;
  const requireFNA = session.agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  const subQuot = quotation.insureds[cid];
  const plans = [subQuot.plans[0]];
  _.each(newRiderList, (covCode) => {
    if (covCode !== subQuot.baseProductCode) {
      const existingPlan = _.find(subQuot.plans, (plan) => plan.covCode === covCode);
      if (existingPlan) {
        plans.push(existingPlan);
      } else {
        plans.push({
          covCode: covCode
        });
      }
    }
  });
  subQuot.plans = plans;
  if (requireFNA && plans.length > 1) {
    needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then((fe) => {
      if (_.get(fe, 'owner.aRegPremBudget')) {
        quote(data, session, callback);
      } else {
        callback({
          success: true,
          errorMsg: 'Cash Premium (RP Budget) is required for selected riders for AXA Shield.'
        });
      }
    });
  } else {
    quote(data, session, callback);
  }
};

const saveQuotation = (session, quotation) => {
  return new Promise((resolve) => {
    logger.log('Quotation :: saveQuotation :: saving quotation');
    if (!quotation) {
      throw new Error('Quotation is empty');
    }
    quotation.type = 'quotation';
    quotation.lastUpdateDate = formatDatetime(new Date());
    if (!quotation.id) {
      quotDao.genQuotationId(session.agent.agentCode, (id) => {
        quotation.id = id;
        resolve(true);
      });
    } else {
      resolve(false);
    }
  }).then((isNewQuotation) => {
    return new Promise((resolve) => {
      if (quotation.clientChoice) {
        delete quotation.clientChoice;
      }
      quotDao.upsertQuotation(quotation.id, quotation, (result) => {
        resolve(result);
      });
    }).then((result) => {
      if (!result || !result.rev) {
        throw new Error('Failed to get rev from saving quotation');
      }
      logger.log('Quotation :: saveQuotation :: saved quotation');
      session.quotation = quotation;
      return Promise.resolve(
        quotation.quickQuote || bundleDao.onSaveQuotation(session, quotation.pCid, quotation.id, isNewQuotation)
      ).then(() => result.rev);
    });
  });
};

const saveAttachment = (quotation, attId, rev, data) => {
  return quotDao.upsertAttachment(quotation.id, attId, rev, data).then((result) => {
    if (result && result.rev) {
      logger.log('Quotation :: saveAttachment :: saved proposal');
      return result.rev;
    } else {
      throw new Error('Failed to get rev from saving proposal');
    }
  });
};

const handleSave = (session, quotation, attachments) => {
  let promise;
  if (quotation.quickQuote) {
    promise = saveQuotation(session, quotation);
  } else {
    promise = bundleDao.updateStatus(quotation.pCid, bundleDao.BUNDLE_STATUS.HAVE_BI).then((bundle) => {
      quotation.bundleId = bundle && (bundle.id || bundle._id);
      return saveQuotation(session, quotation);
    });
  }
  _.each(attachments, (att) => {
    logger.log('Quotation :: handleSave :: saving attachment:', att.saveAttId);
    promise = promise.then((rev) => {
      return saveAttachment(quotation, att.saveAttId, rev, att.data);
    });
  });
  return promise;
};

const getProposalFuncPerms = (session, quotation, readonly) => {
  logger.log('Quotation :: getProposalFuncPerms :: checking for function permissions');
  if (readonly) {
    return Promise.resolve({
      requote: false,
      clone: false
    });
  }
  if (quotation.quickQuote) {
    return Promise.resolve({
      requote: false,
      clone: true
    });
  } else {
    return bundleDao.getProposalFuncPerms(quotation.pCid, quotation.id, session.agent.channel.type === 'FA');
  }
};

const validateQuotation = (agent, quotation) => {
  var requireFNA = agent.channel.type === 'AGENCY' && !quotation.quickQuote;
  if (!requireFNA) {
    return Promise.resolve(true);
  } else {
    return prodDao.getProductSuitability().then((suitability) => {
      return needDao.getItem(quotation.pCid, needDao.ITEM_ID.FE).then((fe) => {
        return getQuotDriver().validateQuotSuitability(suitability, { fe: fe }, quotation);
      });
    });
  }
};

var prepareProposal = function (data, session, cb, createProposal, readonly) {
  const {agent} = session;
  getQuotation(data, session).then((quotation) => {
    return getPlanDetails(session, quotation.baseProductId, false, true).then((planDetails) => {
      return bundleDao.getAppIdByQuotId(quotation.pCid, quotation.id).then((appId) => {
        logger.log('Quotation :: prepareProposal :: begin preparing pdfs');
        return session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
      }).then((standalone) => {
        return ProposalUtils.prepareProposalData(quotation, planDetails, false).then((proposalData) => {
          const {illustrations, errorMsg, warningMsg} = proposalData;
          if (errorMsg) {
            cb({ success: true, errorMsg });
            return;
          }
          let promise;
          if (createProposal) {
            promise = ProposalUtils.genAttachments(agent, quotation, planDetails, standalone, proposalData);
          } else {
            promise = ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, null, true);
          }
          return promise.then((attachments) => {
            const saveAttachments = _.filter(attachments, (att) => !!att.saveAttId);
            return Promise.resolve(
              createProposal && handleSave(session, quotation, saveAttachments)
            ).then(() => {
              return getProposalFuncPerms(session, quotation, readonly).then((funcPerms) => {
                const pdfs = [];
                _.each(attachments, (att) => {
                  if (!att.hidden) {
                    const pdf = {
                      id: att.id,
                      label: att.label,
                      allowSave: !!att.allowSave && !readonly,
                      fileName: att.fileName
                    };
                    if (att.attId) {
                      pdf.docId = quotation.id;
                      pdf.attachmentId = att.attId;
                    } else if (att.data) {
                      pdf.data = att.data;
                    }
                    pdfs.push(pdf);
                  }
                });
                cb({
                  success: true,
                  proposal: pdfs,
                  quotation: quotation,
                  illustrateData: illustrations,
                  planDetails: getPlanDetails4Client(planDetails),
                  warningMsg: warningMsg,
                  funcPerms: funcPerms
                });
              });
            });
          });
        });
      });
    });
  }).catch((error) => {
    logger.error('Quotation :: prepareProposal :: failed to get quotation\n', error);
    cb({ success: false });
  });
};

module.exports.genProposal = function (data, session, cb) {
  getQuotation(data, session).then((quotation) => {
    return validateQuotation(session.agent, quotation);
  }).then((validResult) => {
    if (validResult === true) {
      prepareProposal(data, session, cb, true);
    } else {
      cb({
        success: true,
        errorMsg: validResult || 'Please change the selected product/payment mode or input the relevant budget in FNA.'
      });
    }
  }).catch((err) => {
    logger.error('Quotation :: genProposal :: failed to gen proposal\n', err);
    cb({ success: false });
  });
};

module.exports.getProposal = function (data, session, cb) {
  prepareProposal(data, session, cb, false, data.readonly);
};

module.exports.getEmailTemplate = function (data, session, callback) {
  const {standalone} = data;
  quotDao.getEmailTemplate((result) => {
    if (result && !result.error) {
      callback({
        success: true,
        clientSubject: result.clientSubject,
        clientContent: standalone ? result.clientContentStandalone : result.clientContent,
        agentSubject: result.agentSubject,
        agentContent: standalone ? result.agentContentStandalone : result.agentContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({ success: false });
    }
  });
};

module.exports.emailProposal = function (data, session, callback) {
  const {agent} = session;
  const {emails} = data;
  if (emails) {
    getQuotation(data, session).then((quotation) => {
      return bundleDao.getAppIdByQuotId(quotation.pCid, quotation.id).then((appId) => {
        getApplication(appId, (application) => {
          let standalone = session.agent.channel.type === 'AGENCY' ? quotation.quickQuote : !appId;
          let isShield = _.get(quotation, 'quotType') === 'SHIELD';
          let isProposalSigned = _.get(application, 'isProposalSigned');
          let requiredPdfs = {};
          _.each(emails, (email) => {
            _.each(email.attachmentIds, (id) => {
              requiredPdfs[id] = true;
            });
          });
          return getPlanDetails(session, quotation.baseProductId).then((planDetails) => {
            return ProposalUtils.getAttachments(agent, quotation, planDetails, standalone, _.keys(requiredPdfs)).then((attachments) => {
              return getCompanyInfo().then((companyInfo) => {
                _.each(emails, (email) => {
                  const allAttachments = _.map(email.attachmentIds, (attId) => {
                    const attachment = _.find(attachments, att => att.id === attId);
                    let attData = attachment.data;
                    if (isShield) {
                      const agentCode = quotation.agent.agentCode;
                      const dob = parseDate(quotation.pDob);
                      attachment.agentPassword = agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
                      attachment.clientPassword = dob.format('ddmmmyyyy').toUpperCase();
                    }

                    if ((!standalone && !isShield) || (!standalone && isShield && isProposalSigned)) {
                      if (email.id === 'agent' && attachment.agentPassword) {
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.agentPassword);
                      } else if (email.id === 'client' && attachment.clientPassword) {
                        attData = CommonFunctions.protectBase64Pdf(attachment.data, attachment.clientPassword);
                      }
                    }
                    return {
                      fileName: attachment.fileName || attachment.id + '.pdf',
                      data: attData
                    };
                  });
                  _.each(email.embedImgs, (value, key) => {
                    allAttachments.push({
                      fileName: key,
                      cid: key,
                      data: value
                    });
                  });
                  EmailUtils.sendEmail({
                    from: companyInfo.fromEmail,
                    to: _.join(email.to, ','),
                    title: email.title,
                    content: email.content,
                    attachments: allAttachments
                  }, (result) => {
                    logger.log('Quotation :: emailProposal :: email sent');
                  });
                });
                callback({ success: true });
              });
            });
          });
        });
      });
    }).catch((error) => {
      logger.error('Quotation :: emailProposal :: failed to prepare proposal pdfs\n', error);
      callback({ success: false });
    });
  } else {
    callback({ success: true });
  }
};

module.exports.getPdfToken = (data, session, callback) => {
  const {docId, attachmentId} = data;
  TokenUtils.getPdfToken(docId, attachmentId, session).then((token) => {
    callback({
      success: true,
      token
    });
  }).catch((error) => {
    logger.error('Quotation :: getPdfToken :: failed to get pdf token\n', error);
    callback({ success: false });
  });
};

// module.exports.alterate = function(data, session, callback) {

//   // var allocations = data.quotation.allocations;
//   var newAlters = data.newAlters;

//   getPlanDetails(session, data.quotation.baseProductId, (basicPlanCode, planDetails) => {
//     if (!session.planDetails[basicPlanCode]) {
//       session.planDetails = planDetails;
//     }

//     var validCodes = []
//     var plan = planDetails[data.planCode];
//     for (var f in plan.alterationList) {
//       var alter = plan.alterationList[f];
//       validCodes.push(alter.alterationCode);
//     }

//     var errors = {};
//     var alteration = [];

//     logger.log('update alteration:', validCodes);

//     for (var n = 0; n < newAlters.length; n++) {
//       alter = newAlters[n];
//       var error = false;
//       if (validCodes.indexOf(alter.alterationCode) >= 0) {
//         for (var m=n+1; m < newAlters.length; m++) {
//           var calter = newAlters[m];
//           if (alter.alterationCode == calter.alterationCode &&
//             ((alter.fromYear <= calter.fromYear && calter.fromYear <= alter.toYear)
//             || (alter.fromYear <= calter.toYear && calter.toYear <= alter.toYear )
//             || (calter.fromYear <= alter.fromYear && alter.fromYear <= calter.toYear)
//             || (calter.fromYear <= alter.toYear && alter.toYear <= calter.toYear)
//             || alter.fromYear > alter.toYear)) {
//             error = "invalid_alteration_period"
//             break;
//           }
//         }
//       } else {
//         error = ["invalid_alteration_code", alter.alterationCode];
//       }
//       if (isNaN(alter.amount) || alter.amount == 0) {
//         error = ["invalid_alteration_amount", alter.amount];
//       }
//       if (!error) {
//         alteration.push(alter);
//       } else {
//         errors[n] = error;
//       }
//     }
//     data.error = JSON.stringify(errors);
//     logger.log('update alteration:', errors);
//     if (Object.keys(errors).length == 0) {
//       data.quotation.alteration = alteration;
//     }
//     quote(data, session, callback);
//   });
// };
