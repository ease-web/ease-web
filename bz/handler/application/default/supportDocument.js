const _       = require('lodash');
const logger  = global.logger || console;
const appDao  = require('../../../cbDao/application');
const bDao    = require('../../../cbDao/bundle');
const dao     = require('../../../cbDaoFactory').create();

const _validateOtherDocumentName = function(documentName, app, tabId){
  const reservedNamesDocId = 'suppDocsDefaultDocNames';
  const otherDocValuesArr = _.get(app, 'supportDocuments.values.' + tabId + '.otherDoc.template', []);

  return new Promise((resolve) => {
    dao.getDocFromCacheFirst(reservedNamesDocId, function (reservedNamesDoc) {
      // 1.Check whether is duplicated with reserved names
      if (_.includes(reservedNamesDoc.defaultDocumentNames, documentName)) {
        resolve(true);
      }

      // 2.Check whether is duplicated with current otherDoc names
      for (let index in otherDocValuesArr) {
        if (otherDocValuesArr[index].title === documentName) {
          resolve(true);
        }
      }

      // 3.Check whether is duplicated with 'e-App (iName/pName)' for Shield case
      if (app.type === 'masterApplication') {
        if ('e-App (' + _.get(app, 'quotation.pFullName') + ')' === documentName) {
          resolve(true);
        }
        _.forEach(_.get(app, 'quotation.insureds'), (insuredObj) => {
          if ('e-App (' + insuredObj.iFullName + ')' === documentName) {
            resolve(true);
          }
        });
      }

      resolve(false);
    });
  });
};

const _updateOtherDocumentName = function(data, session, cb){
  const methodName = 'updateOtherDocumentName';
  const appId = data.appId;
  const tabId = data.tabId;
  const documentId = data.documentId;
  const newName = _.trim(data.newName);
  let cbValues;

  logger.log('INFO: ' + methodName + ' - start', appId, documentId);

  appDao.getApplication(appId, function(app){
    if (app) {
      _validateOtherDocumentName(newName, app, tabId).then((isDuplicated) => {
        if (!isDuplicated) {
          return bDao.getApplicationByBundleId(app.bundleId, app.id).then((bApp) => {
            if (bApp.appStatus === 'SUBMITTED') {
              cbValues = data.rootValues;
            } else {
              cbValues = _.get(app, 'supportDocuments.values');
            }

            // const otherDocumentArr = _.get(app, 'supportDocuments.values.' + tabId + '.otherDoc.template');
            const otherDocumentArr = _.get(cbValues, tabId + '.otherDoc.template');
            const index = _.findIndex(otherDocumentArr, (obj) => {
              return obj.id === documentId;
            });

            if (index > -1) {
              if (bApp.appStatus === 'SUBMITTED') {
                _.set(cbValues, tabId + '.otherDoc.template.' + index + '.title', newName);
                cb({
                  success: true,
                  values: cbValues
                });
              } else {
                _.set(app, 'supportDocuments.values.' + tabId + '.otherDoc.template.' + index + '.title', newName);
                appDao.upsertApplication(appId, app, function (resp) {
                  if (resp) {
                    logger.log('INFO: ' + methodName + ' - end [RETURN=1]', appId, documentId);
                    cb({
                      success: true,
                      values: _.get(app, 'supportDocuments.values')
                    });
                  } else {
                    logger.error('INFO: ' + methodName + ' - end [RETURN=2] - update app fail', appId, documentId);
                    cb({ success: false });
                  }
                });
              }
            } else {
              logger.error('INFO: ' + methodName + ' - end [RETURN=4] - get otherDoc existing template fails', appId, documentId);
              cb({ success: false });
            }
          });
        } else {
          logger.log('INFO: ' + methodName + ' - end [RETURN=3] - newName is duplicated', appId, documentId);
          cb({
            success: true,
            duplicated: true
          });
        }
      }).catch((error)=>{
        logger.error('Error in ' + methodName + ' - _validateOtherDocumentName: ', error);
      });
    } else {
      logger.error('Error in ' + methodName + ' : cannot get application document');
    }
  });
};

module.exports.validateOtherDocumentName = _validateOtherDocumentName;
module.exports.updateOtherDocumentName = _updateOtherDocumentName;
