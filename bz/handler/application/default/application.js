const _             = require('lodash');
const moment        = require('moment');
const async         = require('async');
const logger        = global.logger || console;

const utils         = require('../../../Quote/utils');
const CommonUtils   = require('../../../utils/CommonUtils');
const DateUtils     = require('../../../../common/DateUtils');
const commonApp     = require('../common');
const dao           = require('../../../cbDaoFactory').create();
const bDao          = require('../../../cbDao/bundle');
const cDao          = require('../../../cbDao/client');
const nDao          = require('../../../cbDao/needs');
const pDao          = require('../../../cbDao/product');
const appDao        = require('../../../cbDao/application');
const PDFHandler    = require('../../../PDFHandler');
const needsHandler  = require('../../../NeedsHandler');

const _genMenuForm = function(forms, title, detailSeq){
  let result = {
    'title':      title,
    'type':       'menuItem',
    'skipCheck':  forms.skipCheck,
    'key':        forms.formId || forms.id || forms._id,
    'detailSeq':  detailSeq,
    'items':      []
  };

  if (forms.id && (forms.id === 'menu_plan' || forms.id === 'plan')){
    result.items = forms.items;
  } else {
    let tabs = {
      type: 'tabs',
      items: forms.items
    };
    result.items.push(tabs);
  }
  return result;
};

const _getAppFormTemplate = function(quotation, callback) {
  let isPhSameAsLa = false;
  if (quotation.iCid === quotation.pCid){
    isPhSameAsLa = true;
  }
  let plans = quotation.plans;
  let dynTemplate = {};

  let allDone = function(sections) {
    let eFormMenus = [];
    if (sections && sections.length) {
      for (let s in sections) {
        let sectTmp = {
          type: 'menuSection',
          detailSeq: parseInt(s),
          items: []
        };
        let forms = sections[s];
        for (let f in forms.items) {
          let form = forms.items[f];
          if (form.items && form.items.length) {
            sectTmp.items.push(_genMenuForm(form, form.title, parseInt(f)));
          }
        }
        eFormMenus.push(sectTmp);
      }
    }

    callback(
      {
        items: [
          {
            // "id": "stepApp",  comment by sam
            'type': 'section',
            'title': 'Application',
            'detailSeq': 1,
            'items': [
              {
                'type': 'menu',
                'items': eFormMenus
              }
            ]
          },
          {
            'id': 'stepSign',
            'type': 'section',
            'title': 'Signature',
            'detailSeq': 2,
            'items': [
              {
                'id': 'sign',
                'type': 'signature'
              }
            ]
          },
          {
            'id': 'stepPay',
            'type': 'section',
            'title': 'Payment',
            'detailSeq': 3,
            'items': [
              {
                'id': 'pay',
                'type': 'payment'
              }
            ]
          },
          {
            'id': 'stepSubmit',
            'type': 'section',
            'title': 'Submit',
            'detailSeq': 4,
            'items': [
              {
                'id': 'submit',
                'type': 'submission'
              }
            ]
          }
        ]
      },
      sections,
      dynTemplate
    );
  };


  let getInsForms = function(insForms, forms, index, cb) {
    logger.log('INFO: getAppFormTemplate - getInsForms', index, isPhSameAsLa);
    if (forms.length > index) {
      let form = forms[index];
      _.set(dynTemplate, `insurability.${form.id}.${form.covCode}`, form.form);

      dao.getDocFromCacheFirst(form.form, function(tpl) {
        if (tpl && !tpl.error) {
          let template =  _.cloneDeep(tpl);
          if (isPhSameAsLa) {
            template.item.subType = 'proposer';
          } else if (form.id === 'ph') {
            template.item.subType = 'proposer';
          } else if (form.id === 'la') {
            template.item.subType = 'insured';
          }
          // assume only one insure form for proposer/insured
          // TODO::: handle multiple insur form for proposer/insured

          insForms.formId = template.formId;
          insForms.items.push(template.item);
        }
        getInsForms(insForms, forms, index + 1, cb);
      });
    } else {
      cb();
    }
  };

  let getTemplate = function(forms, index, cb) {
    if (forms.length > index) {
      let form = forms[index];

      if (typeof form.form === 'string') {
        dynTemplate[form.id] = form.form;

        dao.getDocFromCacheFirst(form.form, function(tpl) {
          if (tpl && !tpl.error) {
            let template = _.cloneDeep(tpl);
            if (isPhSameAsLa) {
              let newItems = [];
              newItems.push(template.items[0]);
              // newItems[0].priority = "";
              newItems[0].subType = 'proposer';
              template.items = newItems;
            }

            delete form.form;
            form.formId = template.formId;
            form.skipCheck = template.skipCheck;
            form.items = template.items;
          }
          logger.log('INFO: getAppFormTemplate - getTemplate', index,
          '(got forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
          getTemplate(forms, index + 1, cb);
        });
      } else {
        let insForms = [];
        if (!isPhSameAsLa && form.form.ph) {
          for (let i in plans) {
            let plan = plans[i];
            let tplCode = form.form.ph[plan.covCode];
            if (tplCode) {
              if (_.findIndex(insForms, (it)=>{return it.form === tplCode;}) === -1) {
                insForms.push({
                  id: 'ph',
                  form: tplCode,
                  covCode: plan.covCode
                });
              }
            }
          }
        }

        if (form.form.la) {
          for (let i in plans) {
            let plan = plans[i];
            if (form.form.la[plan.covCode]) {
              let tplCode = form.form.la[plan.covCode];
              if (_.findIndex(insForms, (it)=>{return it.form === tplCode;}) === -1) {
                insForms.push({
                  id: 'la',
                  form: tplCode,
                  covCode: plan.covCode
                });
              }
            }
          }
        }

        // Filter out basic plan template when there is different template used in Rider
        let baseProductCode = plans[0].covCode;
        let isBPDuplicateinLa = _.filter(insForms, insForm => {return insForm.covCode !== baseProductCode && insForm.id === 'la';}).length;
        let isBPDuplicateinPh = _.filter(insForms, insForm => {return insForm.covCode !== baseProductCode && insForm.id === 'ph';}).length;
        let removeArray = [];

        if (isBPDuplicateinLa) {
          removeArray.push('la');
        }
        if (isBPDuplicateinPh) {
          removeArray.push('ph');
        }
        _.each(removeArray, removeArrayId => {
          insForms = _.filter(insForms, insForm => {
            if (insForm.covCode === baseProductCode && insForm.id === removeArrayId) {
              return false;
            } else {
              return true;
            }
          });
        });

        if (insForms.length > 0) {
          delete form.form;
          form.items = [];
          getInsForms(form, insForms, 0, function() {
            logger.log('INFO: getAppFormTemplate - getTemplate', index,
            '(got Ins forms) [id=', _.get(form, 'id'), '; formId=', _.get(form, 'formId'), '; title=', _.get(form, 'title'), ']');
            getTemplate(forms, index + 1, cb);
          });
        } else {
          getTemplate(forms, index + 1, cb);
        }
      }
    } else {
      cb();
    }
  };

  let getSectionItems = function(sections, index) {
    logger.log('INFO: getAppFormTemplate - getSectionItems [index=', index, '; numOfSection=', sections.length, ']');
    if (sections.length > index) {
      getTemplate(sections[index].items, 0, function() {
        getSectionItems(sections, index + 1);
      });
    } else {
      allDone(sections);
    }
  };

  dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function(mapping) {
    if (mapping && mapping[quotation.baseProductCode]) {
      var sections = _.cloneDeep(mapping[quotation.baseProductCode].eFormSections);
      if (sections && sections.length) {
        getSectionItems(sections, 0);
      } else {
        allDone(sections);
      }
    } else {
      logger.error('ERROR: getAppFormTemplate - [RETURN=-1]');
    }
  });
};

const _checkCrossAge = function(app, callback){
  const appId = app.id;
  const productId = _.get(app, 'quotation.baseProductId');
  const days = global.config.crossAgeDay;
  logger.log('INFO: checkCrossAge - start', appId);

  let willCrossAgeInDays = function(nowAge, inDays, ageCrossLine){
    let futureDate = new Date();
    futureDate.setDate((futureDate.getDate() + inDays));
    return ageCrossLine === DateUtils.getNearestAge(futureDate, nowAge);
  };

  let getCrossAgeStatus = function(dobDate, ageCrossLine, bundleApp) {
    let currentAge = DateUtils.getNearestAge(new Date(), dobDate);
    if (currentAge - ageCrossLine >= 0) {
      // crossed age
      if (!app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
        return ({
          status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED,
          crossedAge: true
        });
      // appStatus need to change to get from bundle
      } else if (app.isStartSignature && app.quotation.isBackDate === 'N' && !app.isBackDate) {
          if (_.get(bundleApp, 'appStatus') === 'APPLYING') {
            return ({
              status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_SIGNED,
              crossedAge: true
            });
          }
      } else {
        return ({
          status: commonApp.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION,
          crossedAge: true
        });
      }
    } else {
      if (willCrossAgeInDays(dobDate, days, ageCrossLine)) {
        if (app.quotation.isBackDate === 'N' && !app.isBackDate && !app.isStartSignature) {
          return ({
            status: commonApp.CROSSAGE_STATUS.WILL_CROSSAGE
          });
        } else {
          return ({
            status: commonApp.CROSSAGE_STATUS.WILL_CROSSAGE_NO_ACTION
          });
        }
      } else {
        return ({
          status: commonApp.CROSSAGE_STATUS.NO_CROSSAGE
        });
      }
    }
  };

  let needCheckProposerCrossAge = function(){
    return new Promise((resolve) => {
      // It is a third-party application where Proposer is not Life Assured AND
      if (_.get(app, 'quotation.iCid') !== _.get(app, 'quotation.pCid')) {
        // Proposer has selected a rider product AND
        // Rider’s age cross has an impact on premium (all Riders which are for Proposer (titled PH in medical questions product matrix)
        let riders = _.drop(_.get(app, 'quotation.plans', []));
        dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, function(appFormMapping) {
          let proposerRiders = _.keys(
            _.get(
              _.find(
                _.get(appFormMapping, app.quotation.baseProductCode + '.eFormSections[0].items'),
                obj => obj.id === 'insurability',
                {}
              ),
              'form.ph',
              {}
            )
          );
          for (let i in riders) {
            if (_.includes(proposerRiders, riders[i].covCode)) {
              resolve(true);
            }
          }
        });
      }
      resolve(false);
    });
  };

  pDao.getProduct(productId, function(product){
    if (product && app) {
      console.log('INFO: checkCrossAge - product is', product.covName['en']);
      bDao.getApplicationByBundleId(_.get(app, 'bundleId'), app.id).then((bundleApp)=>{
        return needCheckProposerCrossAge().then((checkProposer) => {
          console.log('INFO: checkCrossAge - needCheckProposerCrossAge', checkProposer);
          let proposerResult = {}, insuredResult = {};
          let crossedAge = false;
          let allowBackdate = product.allowBackdate === 'Y';

          insuredResult = getCrossAgeStatus(new Date(_.get(app, 'quotation.iDob')), _.get(app, 'quotation.iAge') + 1, bundleApp);
          if (checkProposer) {
            proposerResult = getCrossAgeStatus(new Date(_.get(app, 'quotation.pDob')), _.get(app, 'quotation.pAge') + 1, bundleApp);
          }

          crossedAge = insuredResult.crossedAge || proposerResult.crossedAge || false;

          console.log('INFO: checkCrossAge - end [RETURN=-0] - insuredResult, proposerResult, allowBackdate, crossedAge', insuredResult, proposerResult, allowBackdate, crossedAge);
          callback({
            success:true,
            insuredStatus : insuredResult.status,
            proposerStatus : proposerResult.status,
            allowBackdate,
            crossedAge
          });

        });
      }).catch((err)=>{
        logger.error('ERROR: checkCrossAge - getApplicationByBundleId [RETURN=-2]', appId, err);
        callback({success:false});
      });

    } else {
      logger.error('ERROR: checkCrossAge - end [RETURN=-1 (app:', _.get(app, 'error'), ')(product:', _.get(product, 'error'), ')]', appId);
      callback({success: false});
    }
  });
};

const _genAppFormPdfSectionVisible = function(values, callback) {
  let showQuestions = {
    residency: {
      question: []
    },
    foreigner: {
      question: []
    },
    policies: {
      question: []
    },
    insurability: {
      question: []
    },
    declaration: {
      question: []
    }
  };

  for (let menuItemKey in showQuestions) {
    commonApp.checkAppFormByPerson(values.proposer, menuItemKey, showQuestions);
    values.insured.forEach(function(la, index) {
      commonApp.checkAppFormByPerson(la, menuItemKey, showQuestions);
    });
  }

  callback(showQuestions);
};

const _genAppFormPdfData = function(app, lang, callback) {
  logger.log('INFO: _genAppFormPdfData - start', app._id);
  const keyStr = 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy';
  const isShowDollarSign = _.get(app, keyStr, true);
  const dollarSign = isShowDollarSign ? '$' : utils.getCurrencySign(_.get(app, 'quotation.ccy'));
  const ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');

  let trigger = {};

  _getAppFormTemplate(app.quotation, function(template) {

    commonApp.getOptionsList(app, template, function() {
      let appTemplate   = template.items[0].items[0]; //contain items of menuSection
      let appOrgValues  = app.applicationForm.values;
      let appNewValues  = _.cloneDeep(app.applicationForm.values);

      commonApp.replaceAppFormValuesByTemplate(appTemplate, appOrgValues,
            appNewValues, trigger, [], lang, ccy);

      appNewValues.lang               = lang;
      appNewValues.baseProductCode    = app.quotation.baseProductCode;
      appNewValues.policyNumber       = app.policyNumber || app._id;
      appNewValues.showLaSignature    = commonApp.checkLaIsAdult(app);
      appNewValues.agent              = app.quotation.agent;
      appNewValues.policyOptions      = app.quotation.policyOptions;
      appNewValues.policyOptionsDesc  = app.quotation.policyOptionsDesc;
      appNewValues.payment            = app.payment;

      if (app.quotation.fund) {
        appNewValues.fund = app.quotation.fund;
      }

      let policyOptions = appNewValues.policyOptions;

      //format currency in policyOptions
      if (policyOptions) {
        if (policyOptions.topUpAmt && policyOptions.topUpAmt !== 'null') {
          policyOptions.topUpAmt = utils.getCurrency(
                  policyOptions.topUpAmt, dollarSign, 2);

        }

        if (policyOptions.rspAmount && policyOptions.rspAmount !== 'null') {
          policyOptions.rspAmount = utils.getCurrency(
                  policyOptions.rspAmount, dollarSign, 2);
        }

        if (policyOptions.wdAmount && policyOptions.wdAmount !== 'null') {
          policyOptions.wdAmount = utils.getCurrency(
                  policyOptions.wdAmount, dollarSign, 2);
        }
      }

      let planDetails = appNewValues.planDetails;

      //format currency in planDetails
      if (planDetails){
        if (planDetails.rspAmount && planDetails.rspAmount !== 'null') {
          planDetails.rspAmount = utils.getCurrency(
                  planDetails.rspAmount, dollarSign, 2);
        }

        if (planDetails.wdAmount && planDetails.wdAmount !== 'null') {
          planDetails.wdAmount = utils.getCurrency(
                  planDetails.wdAmount, dollarSign, 2);
        }
      }

      if (planDetails && planDetails.planList) {
        if (planDetails.totYearPrem) {
          planDetails.totYearPrem = utils.getCurrency(
                  planDetails.totYearPrem, dollarSign, 2);
        }

        for (let i = 0; i < planDetails.planList.length; i ++) {
          let plan = planDetails.planList[i];
          for (let key in plan) {
            if (key === 'halfYearPrem' || key === 'monthPrem' || key === 'premium' ||
                key === 'quarterPrem' || key === 'yearPrem' || key === 'sumInsured') {
              plan[key] = utils.getCurrency(plan[key], dollarSign, 2);
            }
          }
        }
      }


      //handle residency question (PR / Type of Pass)
      _.set(appNewValues, 'proposer.residency.groupOnePass', '');
      if (_.get(appOrgValues, 'proposer.personalInfo.prStatus') === 'Y' || _.filter(appOrgValues.insured, (la) => { return _.get(la, 'personalInfo.prStatus') === 'Y'; }).length > 0) {
        _.set(appNewValues, 'proposer.residency.groupOnePass', 'Singapore Permanent Resident');
      }

      const _handlePass = function(groupPass, checkValue, useValue) {
        let phPass = _.get(appOrgValues, 'proposer.personalInfo.pass');
        let laPass = appOrgValues.insured.map((la) => {
          return _.get(la, 'personalInfo.pass');
        });
        let phPassText = _.get(appNewValues, 'proposer.personalInfo.pass');
        let laPassText = appNewValues.insured.map((la) => {
          return _.get(la, `personalInfo.${useValue}`);
        });
        if (phPass === checkValue || laPass.indexOf(checkValue) >= 0) {
          let passText = phPass === checkValue ? phPassText : laPassText[laPass.indexOf(checkValue)];
          let orgPassText = _.get(appNewValues, `proposer.residency.${groupPass}`, '');
          let addPassText = (orgPassText.length > 0 ? ' / ' : '') + passText;
          _.set(appNewValues, `proposer.residency.${groupPass}`, orgPassText + addPassText);
        }
      };
      _handlePass('groupOnePass', 'ep', 'pass');
      _handlePass('groupOnePass', 'sp', 'pass');
      _handlePass('groupOnePass', 'wp', 'pass');

      //Uppercase personalInfo
      for (let key in appNewValues.proposer.personalInfo) {
        let info = appNewValues.proposer.personalInfo[key];
        if (typeof info === 'string' && key !== 'email' && key !== 'isSameAddr') {
          appNewValues.proposer.personalInfo[key] = info.toUpperCase();
        }
      }
      for (let i = 0; i < appNewValues.insured.length; i ++) {
        for (let key in appNewValues.insured[i].personalInfo) {
          let info = appNewValues.insured[i].personalInfo[key];
          if (typeof info === 'string' && key !== 'email' && key !== 'isSameAddr') {
            appNewValues.insured[i].personalInfo[key] = info.toUpperCase();
          }
        }
      }
      //Uppercase payor & trusted individual information
      for (let key in appNewValues.proposer.declaration) {
        let info = appNewValues.proposer.declaration[key];
        if (typeof info === 'string' && commonApp.CAPITAL_LETTER_IDS.indexOf(key) >= 0) {
          appNewValues.proposer.declaration[key] = info.toUpperCase();
        } else if (key === 'trustedIndividuals') {
          appNewValues.proposer.declaration.trustedIndividuals.fullName =
            _.toUpper(appNewValues.proposer.declaration.trustedIndividuals.fullName);
        }
      }


      for (let key in appOrgValues) {
        if (key === 'proposer') {
          commonApp.removeAppFormValuesByTrigger(key, appOrgValues[key],
            appNewValues[key], appOrgValues[key], trigger[key], true);
        } else if (key === 'insured') {
          for (let i = 0; i < appOrgValues[key].length; i ++) {
            let iOrgValues = appOrgValues[key][i];
            let iNewValues = appNewValues[key][i];
            let iTrigger = trigger[key][i];
            commonApp.removeAppFormValuesByTrigger(key, iOrgValues,
              iNewValues, iOrgValues, iTrigger, true);
          }
        }
      }

      //Grouping HEALTH data into one array
      let groupingHealthTableData = function(person, groupHealthKey, groupedKey) {
        let groupHealthData = [];

        for (let i = 0; i < groupHealthKey.length; i ++) {
          let groupHealthDataKey = groupHealthKey[i] + '_DATA';

          let healthDataArray = _.get(person, 'insurability.' + groupHealthDataKey);
          if (healthDataArray) {
            for (let j = 0; j < healthDataArray.length; j ++) {
              let healthDataRow = healthDataArray[j];
              let groupedRow = {};
              for (let key in healthDataRow) {
                let postfix = key.replace(groupHealthKey[i], '');

                groupedRow['HEALTH_GROUP' + postfix] = healthDataRow[key];
                groupedRow.DATATABLE = groupHealthDataKey;
                groupedRow.SHOW_QN = j === 0 ? 'Y' : 'N';
                groupedRow.ROW_COUNT = healthDataArray.length;
              }
              groupHealthData.push(groupedRow);
            }
          }
        }

        if (groupHealthData.length > 0) {
          person.insurability[groupedKey] = groupHealthData;
        }
      };

      let groupHealthKey = ['HEALTH02',
                              'HEALTH03',
                              'HEALTH04',
                              'HEALTH05',
                              'HEALTH06',
                              'HEALTH07',
                              'HEALTH01',
                              'HEALTH08',
                              'HEALTH09',
                              'HEALTH10',
                              'HEALTH11'];
      let groupedKey = 'HEALTH_GROUP_DATA';
      if (appNewValues.baseProductCode === 'HIM' || appNewValues.baseProductCode === 'HER'){
        groupHealthKey = ['HEALTH_HER02',
                          'HEALTH_HER03',
                          'HEALTH_HER04',
                          'HEALTH_HER05',
                          'HEALTH_HER06',
                          'HEALTH_HER07',
                          'HEALTH_HER08',
                          'HEALTH_HIM02',
                          'HEALTH_HIM03',
                          'HEALTH_HIM04'];
        groupedKey = 'HEALTH_GROUP_DATA';
      }
      groupingHealthTableData(appNewValues.proposer,groupHealthKey,groupedKey);
      let { insured } = appNewValues;
      if (insured) {
        for (let i = 0; i < insured.length; i ++) {
          groupingHealthTableData(appNewValues.insured[i],groupHealthKey,groupedKey);
        }
      }

      _genAppFormPdfSectionVisible(appNewValues, function(showQuestions) {
        appNewValues.showQuestions = showQuestions;
        appNewValues.originalData = app.applicationForm.values;
        logger.log('INFO: _genAppFormPdfData - end', app._id);
        callback(appNewValues);
      });
    });
  });
};

const _genApplicationPDF = function(application, session, callback){
  //get client id
  let appId = application.id;
  let clientId = application.pCid;
  let isFaChannel = session.agent.channel.type === 'FA';

  logger.log('INFO: _genApplicationPDF - start', appId);

  //get bundle
  bDao.getCurrentBundle(clientId).then((bundle)=>{
    // let bundleId = _.get(bundle, 'id');
    let lang = 'en';

    //4. call the function to get FNA report string and update bundle
    let generateFNAReportPdf = function(profile) {
      logger.log('INFO: _genApplicationPDF - (4) generateFNAReport', appId);
      if (profile && !profile.error) {
        needsHandler.generateFNAReport({cid: profile.cid}, session, function(pdfResult) {
          if (!pdfResult.fnaReport){
            logger.error('ERROR: _genApplicationPDF - (4) generateFNAReport [RETURN=-4]', appId);
            callback({success:false});
          }
          else {
            bDao.getCurrentBundle(profile.cid).then((bundle)=>{
              dao.uploadAttachmentByBase64(bundle.id, commonApp.PDF_NAME.FNA,
                bundle._rev, pdfResult.fnaReport, 'application/pdf', function(res) {
                // bDao.updateStatus(profile.cid, bDao.BUNDLE_STATUS.START_GEN_PDF, (newBundle) => {
                bDao.updateStatus(profile.cid, bDao.BUNDLE_STATUS.START_GEN_PDF).then((newBundle)=>{
                  logger.log('INFO: _genApplicationPDF - end [RETURN=2]', appId);
                  callback({success:true});
                }).catch((error)=>{
                  logger.error('ERROR: _genApplicationPDF - end [RETURN=2] fails:', error);
                  callback({success:false});
                });
              });
            });
          }
        });
      } else {
        logger.error('ERROR: _genApplicationPDF - (4) generateFNAReport [RETURN=-3]', appId);
        callback({success:false});
      }
    };

    //3. check FNA is signed, if no, generate FNA report
    let checkFnaReportSigned = function(cid) {
      logger.log('INFO: _genApplicationPDF - (3) checkFnaReportSigned', bundle.isFnaReportSigned, appId);
      if (bundle.isFnaReportSigned || isFaChannel) {
        logger.log('INFO: _genApplicationPDF - end [RETURN=1]', appId);
        callback({success:true});
      } else {
        cDao.getProfileById(cid, function(profile) {
          generateFNAReportPdf(profile);
        });
      }
    };

    //2. generate App Form Pdf
    let generateAppFormPdf = function(appFormTemplates) {
      logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf', appId);

      let reportData = {};
      _genAppFormPdfData(application, lang, function(data) {
        dao.getDoc(bundle.fna[nDao.ITEM_ID.PDA], (pda)=>{
          if (pda && !pda.error) {
            logger.log('INFO: _genApplicationPDF - (2) generateAppFormPdf - populate PDA data', appId);
            //For not FA channel
            if (pda.ownerConsentMethod) {
              let ownerConsentMethodList = pda.ownerConsentMethod.split(',');

              for (let i = 0; i < ownerConsentMethodList.length; i ++) {
                let method = ownerConsentMethodList[i];
                if (method === 'phone') {
                  data.proposer.declaration.PDPA01a = 'Yes';
                } else if (method === 'text') {
                  data.proposer.declaration.PDPA01b = 'Yes';
                } else if (method === 'fax') {
                  data.proposer.declaration.PDPA01c = 'Yes';
                }
              }
            }
          }

          reportData.root = data;

          // handle mutiple template
          let reportTemplates = [];
          for (let i = 0; i < appFormTemplates.length; i ++) {
            if (appFormTemplates[i] instanceof Array) {
              reportTemplates.push.apply(reportTemplates, appFormTemplates[i]);
            } else {
              reportTemplates.push(appFormTemplates[i]);
            }
          }

          PDFHandler.getAppFormPdf(reportData, reportTemplates, lang, function(pdfStr) {
            dao.uploadAttachmentByBase64(appId, commonApp.PDF_NAME.eAPP,
              application._rev, pdfStr, 'application/pdf', function(res) {
              checkFnaReportSigned(clientId);
            });
          });

        });
      });
    };

    //1. get App Form Pdf template from CB
    let getAppFormPdfTemplates = function() {
      logger.log('INFO: _genApplicationPDF - (1) getAppFormPdfTemplates', appId);
      let appFormTemplate = application.applicationForm.values.appFormTemplate;

      let startGen = function() {
        dao.getDocFromCacheFirst(appFormTemplate.main, function(main) {
          if (main && !main.error) {
            var tplFiles = [];
            commonApp.getReportTemplates(tplFiles, appFormTemplate.template, 0, function() {
              if (tplFiles.length) {
                generateAppFormPdf([main, tplFiles]);
              } else {
                logger.error('ERROR: _genApplicationPDF - (1) getAppFormPdfTemplates [RETURN=-2]');
                callback({success:false});
              }
            });
          } else {
            logger.error('ERROR: _genApplicationPDF - (1) getAppFormPdfTemplates [RETURN=-1');
            callback({success:false});
          }
        });
      };

      if (!appFormTemplate) {
        dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping)=> {
          if (mapping && !mapping.error && mapping[application.quotation.baseProductCode]) {
            appFormTemplate = mapping[application.quotation.baseProductCode].appFormTemplate;
            startGen();
          }
        });
      } else {
        startGen();
      }
    };

    getAppFormPdfTemplates();
  });

};

const _saveApplication = function(app, callback){
  let now = new Date().getTime();
  app.lastUpdateDate = now;
  dao.updDoc(app._id || app.id, app, callback);
};

const _saveAppForm = function(data, session, callback) {
  let appId = data.applicationId;
  let formValues = data.values;
  let clientId = data.clientId;
  let application = null;

  logger.log('INFO: saveForm - start', appId);

  bDao.getCurrentBundle(clientId).then((bundle)=>{
    let bId = _.get(bundle, 'id');
    logger.log('INFO: saveForm - (1) getBundle', bId, 'for', appId);

    let policyNumber = null;
    let updIds = [];

    async.waterfall([
      (cb) => {
        appDao.getApplication(appId, function(app) {
          application = app;

          if (app && !app.error) {
            var orgFormValues = app.applicationForm.values;
            let diffs = CommonUtils.difference(orgFormValues, data.values);

            if (Object.keys(diffs).length > 0) {
              logger.log('INFO: saveForm - (2) getApplication - update diffs to bundle', bId, 'for', appId);
              let proposer = formValues.proposer;
              let proposers = [];
              if (proposer instanceof Object) {
                proposers.push(proposer);
              } else if (proposer instanceof Array) {
                proposers = proposer;
              }

              let cids = [];
              for (let j = 0; j < proposers.length; j++){
                cids.push(proposers[j]['personalInfo']['cid']);
                commonApp.updateClientFormValues(bundle, proposers[j]);
              }

              let insured = formValues.insured;
              for (let j = 0; j < insured.length; j++){
                let _insured = insured[j];
                if (cids.length === 0 || cids.indexOf(_insured.personalInfo.cid) === -1) {
                  commonApp.updateClientFormValues(bundle, _insured);
                }
              }

              logger.log(`INFO: BundleUpdate - [cid:${clientId}; bundleId:${_.get(bundle, 'id')}; fn:_saveAppForm]`);
              bDao.updateBundle(bundle, function(upRes) {
                if (upRes && !upRes.error) {
                  cb(null);
                } else {
                  cb('Fail to update bundle:' + _.get(upRes, 'error'));
                }
              });
            } else {
              cb(null);
            }
          } else {
            cb('Fail to get application:' + _.get(app, 'error'));
          }
        });
      }, (cb) => {
        logger.log('INFO: saveForm - (3) saveApp', appId);

        // may need to get policy number
        let toGetPolicyNumber = false;
        let formCompleted = false;
        let resetAppStep = false;
        if (data.extra) {
          toGetPolicyNumber = data.extra.toGetPolicyNumber;
          formCompleted = data.extra.formCompleted;
          resetAppStep = data.extra.resetAppStep;
        }

        application.applicationForm.values = formValues;
        if (formCompleted && application.appStep <= 1) {
          application.appStep = 1;
        }
        if (resetAppStep) {
          application.appStep = 0;
        }
        logger.log('INFO: saveForm - (3) saveApp', appId, 'appStep=', application.appStep);
        //add policy number
        if (!toGetPolicyNumber) {
          cb(null, application);
        } else {
          commonApp.getPolicyNumberFromApi(1, false, function (pNumbers) {
            if (pNumbers && pNumbers.length) {
              application.policyNumber = pNumbers[0];
              policyNumber = pNumbers[0];
            }
            cb(null, application);
          });
        }
      }, (application, cb) => {
        if (session.agent.channel.type !== 'FA') {
          bDao.updateApplicationFnaAnsToApplications(application).then((results) => {
            cb(null, application);
          }).catch((error) => {
            cb(error);
          });
        } else {
          cb(null, application);
        }
      }, (application, cb) => {
        _saveApplication(application, (saveRes) => {
          if (saveRes && !saveRes.error) {
            application._rev = saveRes.rev;
            cb(null, application);
          } else {
            cb('Fail to save application:', _.get(saveRes, 'error'));
          }
        });
      }, (application, cb) => {
        let _populateClientInfo = function (personalInfo, curPersonalInfo, cbk) {
          // logger.log('INFO: saveForm - (4) populateClientInfo', appId);

          let toUpdate = false;
          for (let cfIndex = 0; cfIndex < commonApp.CLIENT_FIELDS_TO_UPDATE.length; cfIndex++) {
            if (personalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]] !==
                curPersonalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]]) {

                  toUpdate = true;
              curPersonalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]] =
                personalInfo[commonApp.CLIENT_FIELDS_TO_UPDATE[cfIndex]];
            }
          }

          logger.log('INFO: saveForm - (4) populateClientInfo', appId, _.get(curPersonalInfo, '_id'), 'update=', toUpdate);
          if (toUpdate) {
            updIds.push(curPersonalInfo._id);
            dao.updDoc(curPersonalInfo._id, curPersonalInfo, cbk);
          } else {
            cbk();
          }
        };

        let _populateLaProfile = function (index) {
          logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'index=', index);
          if (!Number(index)) {
            index = 0;
          }
          let laPersonalInfo = formValues.insured;
          let phPersonalInfo = formValues.proposer.personalInfo;
          if (laPersonalInfo && laPersonalInfo.length > 0) {
            logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'index=', index, 'numOfLa=', laPersonalInfo.length);
            let info = laPersonalInfo[index].personalInfo;
            if (info.cid !== phPersonalInfo.cid) {
              dao.getDoc(info.cid, function (curPersonalInfo) {
                if (index + 1 === laPersonalInfo.length) {
                  logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'ready to genPdf (with LA)');
                  // bDao.updateStatus(clientId, bDao.BUNDLE_STATUS.START_GEN_PDF, ()=>{
                  bDao.updateStatus(clientId, bDao.BUNDLE_STATUS.START_GEN_PDF).then(() => {
                    _populateClientInfo(info, curPersonalInfo, () => {
                      cb(null, application);
                    });
                  }).catch((error) => {
                    cb('Fail to populateLaProfile' + error);
                  });
                } else {
                  _populateClientInfo(info, curPersonalInfo, () => { _populateLaProfile(index + 1); });
                }
              });
            }
          } else {
            logger.log('INFO: saveForm - (4) populateLaProfile', appId, 'ready to genPdf (no LA)');
            cb(null, application);
          }
        };

        let personalInfo = formValues.proposer.personalInfo;
        dao.getDoc(personalInfo.cid, function(curPersonalInfo){
          _populateClientInfo(personalInfo, curPersonalInfo, _populateLaProfile);
        });
      }, (application, cb) => {
        logger.log('INFO: saveForm - (5) genPDF', appId, 'genPDF', data.genPDF);
        if (data.genPDF) {
          _genApplicationPDF(application, session, (resp) => {
            cb(null, resp);
          });
        } else {
          cb(null, {success: true});
        }
      }, (resp, cb) => {
        let success = true;
        if (resp && resp.error){
          success = false;
        }
        let result = {
          success: success,
          updIds: updIds
        };
        if (policyNumber){
          result.policyNumber = policyNumber;
          //update view
          dao.updateViewIndex('main', 'summaryApps');
        }
        cb(null, result);
      }
    ], (err, result) => {
      if (err) {
        logger.error('ERROR: saveForm - end', appId, err);
        callback({success: false});
      } else {
        logger.log('INFO: saveForm - end', appId, result.success);
        callback(result);
      }
    });
  }).catch((error) => {
    logger.error('ERROR: saveForm - unexpected error', error);
  });
};

module.exports.getAppFormTemplate = _getAppFormTemplate;
module.exports.checkCrossAge = _checkCrossAge;
module.exports.saveAppForm = _saveAppForm;
module.exports.genAppFormPdfData = _genAppFormPdfData;
module.exports.genApplicationPDF = _genApplicationPDF;
