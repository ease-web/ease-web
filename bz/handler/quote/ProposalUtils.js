const _               = require('lodash');

const dao             = require('../../cbDaoFactory').create();
const quotDao         = require('../../cbDao/quotation');
const prodDao         = require('../../cbDao/product');

const QuotUtils       = require('./QuotUtils');
const {getQuotDriver, getCompanyInfo} = require('./common');
const PDFHandler      = require('../../PDFHandler');
const CommonFunctions = require('../../CommonFunctions');
const {formatDatetime, parseDate} = require('../../../common/DateUtils');

const getPdfTemplates = (bpDetail, quotation) => {
  let pdfTemplates = {};
  let promises = _.map(bpDetail.reportTemplate, (reportTemplate) => {
    return new Promise((resolve) => {
      var code = reportTemplate.pdfCode;
      if (reportTemplate.covCode && reportTemplate.covCode !== '0' &&
            reportTemplate.covCode !== 'na') {

        var plan = _.find(quotation.plans, (p) => reportTemplate.covCode === p.covCode);
        if (plan) {
          code += '/' + reportTemplate.covCode;
        }
      }

      quotDao.getPdfTemplate('01', reportTemplate.pdfCode, (pdfTemplate) => {
        if (pdfTemplate) {
          pdfTemplates[code] = pdfTemplate;
        }
        resolve();
      });
    });
  });
  return Promise.all(promises).then(() => pdfTemplates);
};


const getIllustrationProps = (quotation, bpDetail, planDetails) => {
  let illustProps = {};
  let promises = [];
  if (bpDetail.nfInd && bpDetail.nfInd.toUpperCase() === 'Y') {
    _.each(quotation.plans, (plan) => {
      promises.push(new Promise((resolve) => {
        let covCode = plan.covCode;
        let planDetail = planDetails[covCode];
        dao.getDoc(planDetail._id + '_' + quotation.iGender + quotation.iSmoke + quotation.iAge, (doc) => {
          if (!doc || doc.error === 'not_found') {
            dao.getDoc(planDetail._id + '_A' + quotation.iSmoke + quotation.iAge, (doc) => {
              if (!doc || doc.error === 'not_found') {
                dao.getDoc(planDetail._id + '_' + quotation.iGender + 'A' + quotation.iAge, (doc) => {
                  if (doc && doc.error !== 'not_found') {
                    illustProps[covCode] = doc;
                  }
                  resolve();
                });
              } else {
                illustProps[covCode] = doc;
                resolve();
              }
            });
          } else {
            illustProps[covCode] = doc;
            resolve();
          }
        });
      }));
    });
  }
  return Promise.all(promises).then(() => illustProps);
};


const prepareProposalData = function (quotation, planDetails, standalone) {
  let bpDetail = planDetails[quotation.baseProductCode];
  let promises = [];
  promises.push(getPdfTemplates(bpDetail, quotation));
  promises.push(getIllustrationProps(quotation, bpDetail, planDetails));
  promises.push(getCompanyInfo());

  return Promise.all(promises).then((args) => {
    let [pdfTemplates, illustProps, companyInfo] = args;
    let extraPara = {
      prepareProposalDataFunc: bpDetail.prepareProposalDataFunc,
      systemDate:         formatDatetime(new Date()),
      dateFormat:         companyInfo.dateFormat,
      datetimeFormat:     companyInfo.datetimeFormat,
      langMap:            !!global.langMaps && (global.langMaps.en ||
                          global.langMaps[Object.keys(global.langMaps)[0]]),
      optionsMap:         !!global.optionsMap && global.optionsMap,
      requireReportData:  true,
      templateFuncs:      {},
      illustProps:        illustProps,
      company:            companyInfo,
      quickQuote:         standalone,
      releaseVersion:     global.config.biReleaseVersion
    };

    _.each(pdfTemplates, (pdfTemplate) => {
      if (pdfTemplate.formulas && pdfTemplate.formulas.prepareReportData) {
        extraPara.templateFuncs[pdfTemplate.pdfCode] = pdfTemplate.formulas.prepareReportData;
      }
    });

    let result = JSON.parse(getQuotDriver(quotation)
                  .generateIllustrationData(quotation, planDetails, extraPara));
    const {illustrations} = result;
    return {
      pdfTemplates: pdfTemplates,
      reportData: result.reportData,
      illustrations: illustrations,
      errorMsg: (_.find(illustrations, ill => ill.errorMsg) || {}).errorMsg,
      warningMsg: (_.find(illustrations && illustrations[quotation.baseProductCode], ill => ill.warningMsg) || {}).warningMsg
    };
  });
};


const hidePdfTempalte = function (reportData, pdfTemplates) {
  _.each(reportData, (obj, pdfCodekey) => {
    // Do when the report Data has hidePagesArray
    if (reportData[pdfCodekey] && reportData[pdfCodekey].hidePagesIndexArray) {
      // Start to remove the page in different langauges
      _.each(pdfTemplates[pdfCodekey].template, (langArr, lang) => {
        pdfTemplates[pdfCodekey].template[lang] = _.remove(langArr, (value, index) => {
          return reportData[pdfCodekey].hidePagesIndexArray.indexOf(index) === -1;
        });
      });
    }
  });
  return pdfTemplates;
};


const genBenefitIllustration = function (quotation, planDetails, proposalData) {
  let bpDetail = planDetails[quotation.baseProductCode];
  let {pdfTemplates, reportData} = proposalData;
  let pdfCodes = _.map(bpDetail.reportTemplate, (reportTemplate) => {
    var retVal = reportTemplate.pdfCode;
    if (reportTemplate.covCode && reportTemplate.covCode !== '0' && reportTemplate.covCode !== 'na') {
      retVal = retVal + '/' + reportTemplate.covCode;
    }
    return retVal;
  });

  return new Promise((resolve) => {
    pdfTemplates = hidePdfTempalte(reportData, pdfTemplates);
    PDFHandler.getReportPdf(pdfCodes, reportData, pdfTemplates, bpDetail.dynPdfInd === 'Y', (pdf) => {
      resolve(pdf && CommonFunctions.addPdfTitle(pdf, 'benefit_illustration.pdf'));
    });
  });
};


const getBenefitIllustration = function (quotation, planDetails, standalone) {
  return new Promise((resolve) => {
    quotDao.getQuotationPDF(quotation.id, standalone, (pdfData) => {
      resolve(pdfData.data);
    });
  });
};


const hasBenefitIllustration = (quotation, planDetails) => {
  const bpDetail = planDetails[quotation.baseProductCode];
  return !_.isEmpty(bpDetail.reportTemplate);
};


const getProductSummary = function (productId, lang) {
  return new Promise((resolve) => {
    prodDao.getAttachmentWithLang(productId, 'prod_summary', lang, (attachData) => {
      resolve(attachData && attachData.data);
    });
  });
};


const getProductSummaries = function (compCode, quotation, lang) {
  return Promise.all(_.map(quotation.plans, (plan) => {
    return new Promise((resolve) => {
      if (plan.covCode === quotation.baseProductCode) {
        getProductSummary(quotation.baseProductId, lang).then((pdf) => {
          resolve(pdf);
        });
      } else {
        prodDao.getRiderByCovCode(compCode, plan.covCode, (prod) => {
          getProductSummary(prod._id, lang).then((pdf) => {
            resolve(pdf);
          });
        });
      }
    });
  })).then((pdfs) => {
    return new Promise((resolve) => {
      CommonFunctions.mergePdfs(_.filter(pdfs, pdf => !!pdf), (data) => {
        resolve(data);
      });
    });
  });
};


const hasProductSummary = (quotation, planDetails) => {
  const bpDetail = planDetails[quotation.baseProductCode];
  return bpDetail && bpDetail.prod_summary;
};

const getProductHighlightSheets = function (compCode, quotation) {
  let {invOpt} = quotation.fund || {};
  let {paymentMethod} = quotation.policyOptions || {};
  let fundCodes = _.map(quotation.fund.funds, f => f.fundCode);
  return QuotUtils.getFunds(compCode, fundCodes, paymentMethod, invOpt).then((funds) => {
    return Promise.all(_.map(funds, (fund) => {
      return new Promise((resolve) => {
        quotDao.getProductHighlightSheet(fund._id, (pdf) => {
          resolve(pdf && pdf.data);
        });
      });
    }));
  }).then((pdfs) => {
    return new Promise((resolve) => {
      CommonFunctions.mergePdfs(_.filter(pdfs, pdf => !!pdf), (data) => {
        resolve(data);
      });
    });
  });
};


const getFundInfoBooklet = (productId, lang) => {
  return new Promise((resolve) => {
    prodDao.getAttachmentWithLang(productId, 'fund_info_booklet', lang, (attachData) => {
      resolve(attachData && attachData.data);
    });
  });
};


const getProductAttachment = function (productId, attachmentId, lang) {
  return new Promise((resolve) => {
    prodDao.getAttachmentWithLang(productId, attachmentId, lang, (attachData) => {
      resolve(attachData && attachData.data);
    });
  });
};


const getSystemAttachmentConfigs = function (quotation, planDetails, standalone, proposalData) {
  const bpDetail = planDetails[quotation.baseProductCode];
  const attachments = [];
  if (hasBenefitIllustration(quotation, planDetails)) {
    if (!quotation.quickQuote) {
      attachments.push({
        id:         'bi',
        label:      'Benefit Illustration',
        fileName:   'benefit_illustration.pdf',
        saveAttId:  'proposal',
        hidden:     standalone,
        getAgentPassword: function () {
          const agentCode = quotation.agent.agentCode;
          return agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
        },
        getClientPassword: function () {
          const dob = parseDate(quotation.pDob);
          return dob.format('ddmmmyyyy').toUpperCase();
        },
        getData: () => getBenefitIllustration(quotation, planDetails, false),
        genData: () => genBenefitIllustration(quotation, planDetails, proposalData)
      });
    }

    if (standalone) {
      attachments.push({
        id:         'standaloneBi',
        label:      'Benefit Illustration',
        fileName:   'benefit_illustration.pdf',
        saveAttId:  'standaloneProposal',
        allowAsync: true,
        allowSave:  true,
        getAgentPassword: function () {
          const agentCode = quotation.agent.agentCode;
          return agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
        },
        getClientPassword: function () {
          const dob = parseDate(quotation.pDob);
          return dob.format('ddmmmyyyy').toUpperCase();
        },
        getData: () => getBenefitIllustration(quotation, planDetails, true),
        genData: () => {
          return prepareProposalData(quotation, planDetails, true).then(
            (standaloneProposalData) => {
            return genBenefitIllustration(quotation, planDetails, standaloneProposalData);
          });
        }
      });
    }
  }

  if (hasProductSummary(quotation, planDetails)) {
    attachments.push({
      id:         'prodSummary',
      label:      'Product Summary',
      fileName:   'product_summary.pdf',
      getData:    () => getProductSummaries(quotation.compCode, quotation)
    });
  }

  if (bpDetail.fundInd === 'Y') {
    attachments.push({
      id:         'phs',
      label:      'Product Highlight Sheet',
      fileName:   'product_highlight_sheet.pdf',
      getData:    () => getProductHighlightSheets(quotation.compCode, quotation)
    });
    attachments.push({
      id:         'fib',
      label:      'Fund Info Booklet',
      fileName:   'fund_info_booklet.pdf',
      getData:    () => getFundInfoBooklet(quotation.baseProductId)
    });
  }
  return attachments;
};


const getExtraAttachmentPdf = function (attId, agent, quotation, planDetails) {
  const extraAttachments = getExtraAttachmentConfigs(agent, quotation, planDetails);
  const attachment = _.find(extraAttachments, att => att.id === attId);
  if (attachment.saveAttId) {
    return getExtraAttachmentData(quotation, attachment);
  } else {
    return genExtraAttachmentData(quotation, planDetails, attachment);
  }
};


const getExtraAttachmentConfigs = (agent, quotation, planDetails) => {
  const bpDetail = planDetails[quotation.baseProductCode];
  if (bpDetail.formulas && bpDetail.formulas.getExtraAttachments) {
    return getQuotDriver().runFunc(bpDetail.formulas.getExtraAttachments,
                          agent, quotation, planDetails);
  }
  return null;
};

const genExtraAttachmentPdf = (quotation, planDetails, attachment) => {
  const promises = [];
  _.each(attachment.files, (file) => {
    const planDetail = planDetails[file.covCode];
    if (planDetail) {
      promises.push(getProductAttachment(planDetail._id, file.fileId).then((attData) => {
        if (file.covCode === 'ASIM'){
          if (file.fileId === 'prod_summary_2' || file.fileId === 'prod_summary_1'){
            let agentName = '';
            let proposerName = '';
            if (quotation.agent.name){
              agentName = quotation.agent.name;
            }
            if (quotation.pFullName){
              proposerName = quotation.pFullName;
            }
            var x1 = 68.0;
            var y1 = 630.0;
            var x2 = 310.0;
            var y2 = 630.0;
            if (file.fileId === 'prod_summary_2'){
              y1 += 10;
              y2 += 10;
            }
            return CommonFunctions.addWordsPdfs(attData, agentName, proposerName, x1, y1, x2, y2);
          } else {
            return attData;
          }
        } else {
          return attData;
        }
      }));
    }
  });
  return Promise.all(promises).then((pdfs) => {
    return new Promise((resolve) => {
      CommonFunctions.mergePdfs(_.filter(pdfs, pdf => !!pdf), resolve);
    });
  });

};


const genExtraAttachmentData = (quotation, planDetails, attachmentConfig) => {
  return genExtraAttachmentPdf(quotation, planDetails, attachmentConfig).then((data) => {
      let attData = data;
      if (attachmentConfig.title) {
        attData = CommonFunctions.addPdfTitle(data, attachmentConfig.title);
      }
      return attData;
    });
};


const getExtraAttachmentData = (quotation, attachmentConfig) => {
  return quotDao.getAttachment(quotation.id, attachmentConfig.saveAttId).then((result) => {
    return result && result.data;
  });
};

/**
 * Returns possible attachment lists for the quotation.
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 */
const getAttachmentList = function (agent, quotation, planDetails, standalone) {
  const sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone);
  const extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  const attachments = [];
  _.each([].concat(sysAttConfigs, extraAttConfigs), (config) => {
    attachments.push({
      id: config.id,
      label: config.label,
      hidden: config.hidden
    });
  });
  return attachments;
};

/**
 * Returns list of proposal attachment with following fields:
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - fileName: file name for download
 * - data: base64 data of the attachment
 * - saveAttId: attachment id for storage
 * - attId: attachment id for async retrieval
 * - allowSave: allow the user to save the attachment
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 * @param {*} attachmentIds
 */
const getAttachments = function (agent, quotation, planDetails, standalone, attachmentIds, async) {
  const sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone);
  const extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  let attIds = attachmentIds || [];

  if (!attachmentIds) {
    attIds = attIds.concat(_.map(sysAttConfigs, c => c.id));
    attIds = attIds.concat(_.map(extraAttConfigs, c => c.id));
  }

  const promises = _.map(attIds, (attId) => {
    let config = _.find(sysAttConfigs, c => c.id === attId);
    if (config) {
      const att = {
        id:             config.id,
        label:          config.label,
        fileName:       config.fileName,
        allowSave:      config.allowSave,
        hidden:         config.hidden,
        agentPassword:  config.getAgentPassword && config.getAgentPassword(),
        clientPassword: config.getClientPassword && config.getClientPassword()
      };

      if (async && config.allowAsync && config.saveAttId) {
        att.attId = config.saveAttId;
        return att;
      } else {
        return config.getData().then((data) => {
          att.data = data;
          return att;
        });
      }
    } else {
      config = _.find(extraAttConfigs, c => c.id === attId);
      if (config) {
        const promise = config.saveAttId ? getExtraAttachmentData(quotation, config) :
                        genExtraAttachmentData(quotation, planDetails, config);
        return promise.then((data) => {
          return {
            id:         config.id,
            label:      config.label,
            fileName:   config.fileName,
            data:       data,
            attId:      config.attId,
            allowSave:  config.allowSave,
            hidden:     config.hidden
          };
        });
      } else {
        return Promise.resolve();
      }
    }
  });
  return Promise.all(promises).then(atts => sortAttachments(atts));
};


/**
 * Returns list of proposal attachment with following fields:
 * - id: mandatory id for referencing the attachment
 * - label: attachment name for display
 * - data: base64 data of the attachment
 * - saveAttId: attachment id for storage
 * - attId: attachment id for async retrieval
 * - allowSave: allow the user to save the attachment
 * - hidden: true if not for display
 *
 * @param {*} agent
 * @param {*} quotation
 * @param {*} planDetails
 * @param {*} standalone
 * @param {*} proposalData
 */
const genAttachments = function (agent, quotation, planDetails, standalone, proposalData) {
  let promises = [];

  const sysAttConfigs = getSystemAttachmentConfigs(quotation, planDetails, standalone, proposalData);
  _.each(sysAttConfigs, (config) => {
    const promise = config.genData ? config.genData() : config.getData();
    promises.push(promise.then((data) => {
      return {
        id:           config.id,
        label:        config.label,
        fileName:     config.fileName,
        data:         data,
        attId:        config.allowAsync && config.saveAttId,
        allowSave:    config.allowSave,
        hidden:       config.hidden,
        saveAttId:    config.saveAttId
      };
    }));
  });

  const extraAttConfigs = getExtraAttachmentConfigs(agent, quotation, planDetails);
  _.each(extraAttConfigs, (config) => {
    promises.push(genExtraAttachmentData(quotation, planDetails, config).then((data) => {
      return {
        id:           config.id,
        label:        config.label,
        fileName:     config.fileName,
        data:         data,
        attId:        config.attId,
        allowSave:    config.allowSave,
        hidden:       config.hidden,
        saveAttId:    config.saveAttId
      };
    }));
  });
  return Promise.all(promises).then(atts => sortAttachments(atts));
};

const sortAttachments = function (attachments) {
  const attSeqs = ['Benefit Illustration', 'Product Summary', 'Product Highlight Sheet', 'Fund Info Booklet'];
  return _.sortBy(attachments, (att, index) => {
    const seq = attSeqs.indexOf(att.label);
    return seq === -1 ? attSeqs.length + index : seq;
  });
};

module.exports = {
  prepareProposalData,
  getBenefitIllustration,
  getProductSummaries,
  getProductHighlightSheets,
  getFundInfoBooklet,
  getExtraAttachmentPdf,

  getAttachmentList,
  genAttachments,
  getAttachments
};
