const aDao = require('../cbDao/agent');
const dao = require('../cbDaoFactory').create();
const _ = require('lodash');


module.exports.getAllCustomer = function () {
    return new Promise(resolve => {
        dao.getViewRange('main', 'contacts', '', '', null, (pList = {}) => {
            resolve(_.compact(_.map(pList.rows, 'id')));
        });
    });
}

module.exports.getFilterAgent = function (impactAgents = []) {
    return new Promise(resolve => {
        dao.getViewRange('main', 'agents', '', '', null, (pList = {}) => {
            const result = _.filter(pList.rows, row => impactAgents.indexOf(row.key[1]) > -1);
            resolve(_.map(result, 'value.profileId'));
        });
    });
}

module.exports.insertNoticeToAgent = function (agentId, id, message) {
    return new Promise(resolve => {
        let agentDocId = aDao.getAgentSuppDocId(agentId);
        dao.getDoc(agentDocId, agentDoc => {
            if (!agentDoc.notifications) {
                agentDoc.notifications = [];
            }

            // remove the notice which has the same key and not read before
            let noticeIndex = _.findIndex(agentDoc.notifications, notice => notice.id === id && !notice.read);
            if (noticeIndex > - 1) {
                agentDoc.notifications.splice(noticeIndex, 1);
            }

            agentDoc.notifications.push({id, message});
            dao.updDoc(agentDocId, agentDoc, resolve);
        });
    });
}