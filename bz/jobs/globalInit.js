const dao = require('../cbDaoFactory').create();

const logger = global.logger || console;

module.exports.execute = function () {
  global.optionsMap = {};
  const optionsMapKeys = [
    'marital',
    'country',
    'occupation',
    'industry',
    'ccy',
    {key: 'productLine', docId: 'productLine_template'},
    'relationship',
    'nationality',
    'countryTelCode',
    'residency',
    'city',
    'cityCategory',
    'assetClass',
    'riskRating',
    'portfolioModels',
    'idDocType',
    'pass'
  ];
  return Promise.all(optionsMapKeys.map((val) => {
    return new Promise((resolve) => {
      let docId = val.docId || val;
      let key = val.key || val;
      dao.getDoc(docId, (doc) => {
        global.optionsMap[key] = doc;
        logger.log('Jobs :: globalInit :: initialized optionsMap:', key);
        resolve();
      });
    });
  })).then(() => {
    logger.log('Jobs :: globalInit :: job completed');
  });
};
