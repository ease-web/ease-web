var { callApi } = require('./utils/RemoteUtils');

var actions = require('./uri/actions');
var requestIp = require('request-ip');
var auditDao = require("./cbDao/audit");
var _ = require('lodash');
var ip = require('ip');
var logger = global.logger || console;

var genAccessLogId = function(agentCode, time){
	var date = time?new Date(time):new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDay();
	if (month < 10) month = "0"+month;
    if (day < 10) day = "0"+day;

	return "AUDA_" + agentCode +"_"+ year+mouth+day;
}

var getLoginLogId = function(agentCode, time){
	var date = time?new Date(time):new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDay();
    if (month < 10) month = "0"+month;
    if (day < 10) day = "0"+day;

	return "AUDL_"+ agentCode + "_"+ year+month+day;
}

module.exports.updateLoginAud = function(data, meta, session, cb) {

	// let id = getLoginLogId(data.profileId, data.loginTime);
	let localTime = meta.localTime || 0;

	let entry = {
		loginSuccess: !data.error,
		loginDate: session.loginTime,
		loginToken: session.loginToken,
		profileId: data.profileId,
		version: global.config.version,
		build: global.config.build,
		timeDiff: localTime - session.loginTime,
		userIPAddr: session.userIp,
		servIPAddr: ip.address(),
		device: meta.device,
		browser: meta.browser
	}
	logger.debug('updateLoginAud:', entry);
	// Store on DB
	callApi("/audit/login", entry);

	// Store on CB
	// auditDao.getAuditLog(id, function(auditLog){
	// 	if(!auditLog || auditLog.error) {
	// 		//has no record
	// 		auditLog = {
	// 			type: "loginAudit",
	// 			profileId: data.profileId,
	// 			logs: []
	// 		}
	// 	}

	// 	auditLog.logs.push(entry)
	// 	auditDao.updateAuditLog(id, auditLog, cb);
	// })
}

module.exports.updateLogoutAud = function(data, session, cb) {

	// let id = getLoginLogId(data.profileId, data.loginTime);
	if (session.loginToken && session.agent && session.agent.profileId) {
		let entry = {
			logoutDate: new Date().getTime(),
			loginToken: session.loginToken,
			profileId: session.agent && session.agent.profileId
		}
		// logger.log('updateLogoutAud:', entry);
		callApi("/audit/logout", entry);
	}

	// Store on CB
	// auditDao.getAuditLog(id, function(auditLog){
	// 	if(!auditLog || auditLog.error) {
	// 		//has no record
	// 		auditLog = {
	// 			type: "loginAudit",
	// 			profileId: data.profileId,
	// 			compCode: data.compCode,
	// 			logs: []
	// 		}
	// 	}

	// 	auditLog.logs.push(entry)
	// 	auditDao.updateAuditLog(id, auditLog, cb);
	// })
}

module.exports.accessLog = function(session, action, extraData, cb) {
	// logger.log("func -> accessLog session ", session);
	if(actions[action] && actions[action].level == 0){
	  // do nth
	  if (cb && typeof cb == 'function') cb(false);
  	} else {
		let timestamp = new Date().getTime();
		//	log details
		var log = {
			timestamp: timestamp,
			action: action,
			token: session.loginToken,
			profileId: (session.agent && session.agent.profileId) || session.samlUser && session.samlUser.id,
			extraData: extraData?JSON.stringify(extraData):""
		};
		// Store on DB
		logger.debug('access aud:', log);
		callApi("/audit/access", log, cb);

		// Store on CB
		// let profileId = (session.agent && session.agent.profileId) || "anonymous";

		// let logId = genAccessLogId(session.agent.profileId, timestamp);

		// if(!session.clientLog){
		// 	auditDao.getAuditLog(logId, function(cLogRet){
		// 		if(cLogRet && !cLogRet.error){
		// 			session.clientLog = cLogRet;
		// 			upsertLog(session, log, logId, cb)
		// 		} else {
		// 			insertNewClientLog(session, log, logId, cb);
		// 		}
		// 	})
		// } else{
		// 	upsertLog(session, log, logId, cb);
		// }
  	}
}

var insertNewClientLog = function(session, newlog, cLogId, cb){
	var nLog = {
		id: cLogId,
		type: 'auditLog',
		clientId: session.agent.profileId,
		logs: []
	};
	nLog.logs.push(newlog);

	auditDao.updateAudById(cLogId, nLog, function(result){
		if (result && !result.error) {
			nLog._rev = result.rev;
			session.clientLog = nLog;
			if (cb && typeof cb == 'function') cb(true);
		} else {
			if (cb && typeof cb == 'function') cb(false);
		}
	});
}

var upsertLog = function(session, newLog, clogId, cb){
	var cLog = session.clientLog;

	cLog.logs.push(newLog);
	//update client log current details log
	auditDao.updateAudById(clogId, cLog, function(result){
		if (result && !result.error) {
			cLog._rev = result.rev;
			session.clientLog = cLog;
			if (cb && typeof cb == 'function') cb(true);
		} else {
			auditDao.getAuditLog(clogId, function(cLogRet){
				if(cLogRet && !cLogRet.error){
					cLogRet.logs.push(newLog);
					auditDao.updateAudById(clogId, cLogRet, function(upResult) {
						if (upResult && !upResult.error) {
							cLogRet._rev = upResult.rev;
							session.clientLog = cLogRet;
							if (cb && typeof cb == 'function') cb(true);
						} else {
							if (cb && typeof cb == 'function') cb(false);
						}
					})
				} else {
					insertNewClientLog(session, newLog, clogId, cb);
				}
			})
		}
	})
}
