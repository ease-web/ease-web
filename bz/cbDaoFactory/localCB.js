// const jr = require('../JavaRunner');
var inited = false;

module.exports.init = function(ddname, view, callback) {
  var jr = global.JavaRunner;
  if (!inited) {
    inited = true;
    jr.initCBL(callback)
  }
}

module.exports.createView = function(ddname, view, callback) {
  var jr = global.JavaRunner;
  jr.createView(ddname, view, callback);
}

module.exports.getViewRange = function(ddname, name, start, end, params, callback) {
  var jr = global.JavaRunner;
  jr.getView(ddname, name, start, end, JSON.stringify(params), callback);
}

module.exports.getDoc = function(docId, callback) {
  var jr = global.JavaRunner;
  jr.getDoc(docId, callback);
}

module.exports.getAttachment = function(docId, attName, callback) {
  var jr = global.JavaRunner;
  jr.getAttachment(docId, attName, callback);
}

module.exports.addDoc = function(docId, data, callback) {
  var jr = global.JavaRunner;
  jr.addDoc(docId, data, callback);
}

module.exports.updDoc = function(docId, data, callback) {
  var jr = global.JavaRunner;
  jr.updDoc(docId, data, callback);
}

module.exports.delDoc = function(docId, callback) {
  var jr = global.JavaRunner;
  jr.delDoc(docId, callback);
}

module.exports.setAttachment = function(docId, name, mime, data, callback) {
  var jr = global.JavaRunner;
  jr.setAttachment(docId, name, mime, data, callback);
}

module.exports.delAttachment = function(docId, name, callback) {
  var jr = global.JavaRunner;
  jr.delAttachment(docId, name, callback);
}
