const _ = require('lodash');
const dao = require('../cbDaoFactory').create();
var agentDao = require("./agent");
const bDao = require('./bundle');
const _getOr = require('lodash/fp/getOr');
var async = require('async');
const approvalHandler = require('../ApprovalHandler');
var logger = global.logger || console;

module.exports.getSupervisorTemplate = function(approverLevel) {
    return new Promise((resolve) => {
        if (approverLevel === 'FAADMIN') {
            dao.getDoc('SupervisorFAAdminTemplate', function(template){
                resolve(template);
            });
        } else if (approverLevel === 'FAAGENT'){
            dao.getDoc('SupervisorFATemplate', function(template){
                resolve(template);
            });
        } else {
            dao.getDoc('supervisorTemplate', function(template){
                resolve(template);
            });
        }
    }).catch((e) => {
        logger.error('ERROR:: GET Supervisor Template :: Doc Ids:: SupervisorFAAdminTemplate, SupervisorFATemplate, supervisorTemplate', e);
    });
};

module.exports.getApprovalPageTemplate = function(role, session, callback) {
    let promiseArr = [];
    let rpTemplate;
    let apTemplate;
    let rejTemplate;
    let reviewPage_selectClientTab_Template = 'reviewPage_selectedClient_tmpl';
    let reviewPage_jwf_Template = 'reviewPage_jwf_tmpl';
    let workbenchDocId;
    let channelType = _getOr('', 'channel.type', session.agent );
    if (channelType === 'FA') {
        workbenchDocId = 'workbenchTemplate_FA';
    } else {
        workbenchDocId = 'workbenchTemplate';
    }
    if (role === 'FAADMIN'){
        rpTemplate = 'reviewPageFAAdminTemplate';
        apTemplate = 'approvePageFATemplate';
        rejTemplate = 'rejectPageFATemplate';
    } else if (role === 'FAAGENT') {
        rpTemplate = 'reviewPageFATemplate';
        apTemplate = 'approvePageFATemplate';
        rejTemplate = 'rejectPageFATemplate';
    }else {
        rpTemplate = 'reviewPageTemplate';
        apTemplate = 'approvePageTemplate';
        rejTemplate = 'rejectPageTemplate';
    }

    // Get template for review page
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(rpTemplate, function(template){
                resolve(template);
            });
        })
    );

    // Get template for approve page
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(apTemplate, function(template){
                resolve(template);
            });
        })
    );

    // Get template for reject page
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(rejTemplate, function(template){
                resolve(template);
            });
        })
    );

    // Get template for review page selected client tab when submitted the case to rls
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(reviewPage_selectClientTab_Template, function(template){
                resolve(template);
            });
        })
    );

    // Get template for review page jwf tab when submitted the case to rls
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(reviewPage_jwf_Template, function(template){
                resolve(template);
            });
        })
    );

    // Get template for pending for approval filter fields
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc('approvalTemplate', function(template){
                resolve(template);
            });
        })
    );

    // Get template for workbench filter fields
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc(workbenchDocId, function(template){
                resolve(template);
            });
        })
    );

    // Get caseTemplate for card view in searching page
    promiseArr.push(
        new Promise((resolve, reject) => {
            dao.getDoc('caseTemplate', function(template){
                resolve(template);
            });
        })
    );

    Promise.all(promiseArr).then(result=>{
        callback({success: true, result});
    }).catch((error)=>{
        logger.error("ERROR:: getApprovalPageTemplate->Promise.all: ", error);
    });
};

module.exports.searchApprovalCases = function(compCode, id, filter, callback){
    let result = [];
    return new Promise((resolve, reject) => {
        dao.getViewRange(
            'main',
            'approvalCases',
            '["'+ compCode + '","' + id + '","' + '0' + '"' +']',
            '["'+ compCode + '","' + id + '","' + '9' + '"' +']',
            null,
            function(pList){
                if (pList && pList.rows){
                    for (var i=0; i<pList.rows.length; i++){
                        result.push(pList.rows[i].value);
                    }
                }
            resolve(callback({success: true, result, filter}));
            }
        );

    }).catch((e) => {
        logger.error('ERROR:: searchApprovalCases', e);
        callback({success: false});
    });
};

module.exports.searchApprovalCaseById = function(id, callback){
    new Promise((resolve, reject) => {
        dao.getDoc(id, function(foundCase){
            resolve(foundCase);
        });
    }).then(approvalCase => {
      return new Promise (resolve => {
        agentDao.getMangerProfileByAgentCode(approvalCase.compCode, approvalCase.agentId, (managerProfile) => {
          resolve({approvalCase, managerProfile})
        });
      }).catch(getManagerExp => {
        logger.error(getManagerExp);
      })
    }).then(result => {
      let approvalCase = result.approvalCase;
      let mProfile = result.managerProfile;
      const showStampedInformationStatus = ['A', 'R', 'E', 'PFAFA'];
      _getBundleidByApplication(approvalCase.compCode, approvalCase.applicationId, 'pCid').then(pcId => {
        if (showStampedInformationStatus.indexOf(approvalCase.approvalStatus) === -1 || approvalCase.managerName === undefined || _.isEmpty(approvalCase.managerName)) {
            approvalCase.managerName = _getOr('', 'name', mProfile);
        }

        new Promise((resolve, reject) => {
            let trustedIndividualPathInApplication = 'applicationForm.values.proposer.extra.hasTrustedIndividual';
            dao.getDoc(approvalCase.applicationId, function(applicationJson){
                let trustedIndividual = _getOr('N', trustedIndividualPathInApplication, applicationJson );
                let hasTrustedIndividual;
                (trustedIndividual === 'Y') ? hasTrustedIndividual = true : hasTrustedIndividual = false;
                resolve(callback({success: true, foundCase: approvalCase, hasTrustedIndividual}));
            });
        }).catch((error)=>{
            logger.error("ERROR:: searchApprovalCaseById->_getBundleidByApplication: ", error);
        });
    }).catch((error)=>{
        logger.error("ERROR:: searchApprovalCaseById->new Promise: ", error);
    });
  });
};

module.exports.getApprovalCaseByAppId = (appId) => {
    return new Promise((resolve) => {
        dao.getDoc(appId, (appDoc = {}) => {
            let approvalId = (appId && appId.indexOf('SA') > -1) ? approvalHandler.getMasterApprovalIdFromMasterApplicationId(appId) : appDoc.policyNumber;
            dao.getDoc(approvalId, (foundCase = {}) => {
                resolve(foundCase);
            });
        });
    }).catch( error => {
        logger.error('ERROR:: getApprovalCaseByAppId' + error);
    });
}

module.exports.geteApprovalByAppid = function(id, callback){
    return new Promise((resolve, reject) => {
        dao.getDoc(id, function(appDoc = {}){
            resolve(appDoc.policyNumber);
        });
    }).then(eApprovalId => {
         return new Promise( resolve => {
            dao.getDoc(eApprovalId, function(foundCase = {}){
                resolve(callback({success: true, foundCase}));
            });
         })
    }).catch((error)=>{
        logger.error("ERROR:: geteApprovalByAppid->new Promise: ", error);
    });
};

module.exports.updateApprovalCaseById = function(id, approvalCase, callback){
    return new Promise((resolve, reject) => {
        dao.getDoc(id, function(foundCase){
            resolve(foundCase);
        });
    }).then(appCase => {
        approvalCase._rev = appCase._rev;
        if (appCase._attachments) {
            approvalCase._attachments = appCase._attachments;
        }
        appCase = approvalCase;
        return new Promise(resolve => {
            dao.updDoc(id, approvalCase, function(result) {
                if (result && !result.error) {
                    resolve(id);
                } else {
                    resolve(false);
                }
            });
        });
    }).then(id =>{
        if (id) {
            dao.getDoc(id, function(updatedCase){
                dao.updateViewIndex("main", "approvalCases");
                callback({success:true, updatedCase});
            });
        }else {
            callback({success:false});
        }
    }).catch((error)=>{
        logger.error("Error in updateApprovalCaseById->new Promise: ", error);
    });
};

module.exports.uploadAttachment = function(id, attchId, rev, data, mime, callback){
    dao.uploadAttachmentByBase64(id, attchId, rev, data, mime, function(result) {
        if (result && !result.error) {
            callback(result.rev);
        } else {
          logger.error('ERROR:: uploadAttachment', _getOr('', 'error', result));
          callback({success:false});
        }
    });
};

module.exports.getAllDoc = function(docArr, cb){
    let promiseArr =[];
    docArr.forEach((obj) => {
        promiseArr.push(new Promise((resolve, reject)=>{
            dao.getDoc(obj, (doc)=>{
                resolve(doc);
            });
        }));
    });
    Promise.all(promiseArr).then(result =>{
        cb({success: true, result});
    }).catch((error)=>{
        logger.error("Error in getAllDoc->Promise.all: ", error);
    });
};

module.exports.createApprovalCase = function(approvalCase, cb){
    return new Promise((resolve, reject) => {
        dao.getDoc(approvalCase.approvalCaseId, function(foundCase){
            resolve(foundCase);
        });
    }).then(doc=>{
        return new Promise(resolve => {
            if (doc._rev)
            approvalCase._rev = doc._rev;

            dao.updDoc(approvalCase.approvalCaseId, approvalCase, function(result) {
                if (result && !result.error) {
                    resolve(approvalCase.approvalCaseId);
                } else {
                    resolve(undefined);
                }
            });
        });
    }).then(id => {
        if (id) {
            dao.getDoc(id, function(createdCase){
                dao.updateViewIndex("main", "approvalCases");
                cb({success:true, createdCase});
            });
        }else {
            cb({success:false});
        }
    }).catch((error)=>{
        logger.error("Error in createApprovalCase->new Promise: ", error);
    });
};

module.exports.getPendingForApprovalCaseListLength = function(compCode, id, callback) {
    return new Promise((resolve, reject) => {
        dao.getViewRange(
            'main',
            'approvalCases',
            '["' + compCode + '","' + id + '","' + '0' + '"' + ']',
            '["' + compCode + '","' + id + '","' + '9' + '"' + ']',
            null,
            function(pList){
                if (pList && pList.rows){
                    let noOfPending = 0;
                    let objStatus;
                    pList.rows.forEach(obj =>{
                        if(obj.value && ['A','R','E'].indexOf(obj.value.approvalStatus) === -1){
                            noOfPending++;
                        }
                    });

                    resolve(callback({success: true, length: noOfPending}));
                } else {
                    reject(callback({ success: false }));
                }
            }
        );
    }).catch((e) => {
        logger.error('ERROR:: getPendingForApprovalCaseListLength', e);
        callback({ success: false });
    });
};

module.exports.getApprovalCases = (compCode, statusList) => {
    return new Promise((resolve) => {
        let promises = [];
        promises.push(new Promise((resolve2) => {
            dao.getViewRange(
                'main',
                'approvalDetails',
                null,
                null,
                null,
                (result) => {
                let approvals = [];
                _.each(result && result.rows, (row) => {
                    if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
                        approvals.push(row.value);
                    }
                });
                resolve2(approvals);
                }
            );
        }));
        promises.push(new Promise((resolve2) => {
            dao.getViewRange(
                'main',
                'masterApprovalDetails',
                null,
                null,
                null,
                (result) => {
                let approvals = [];
                _.each(result && result.rows, (row) => {
                    if (row.value && (!statusList || statusList.indexOf(row.value.approvalStatus) > -1)) {
                        approvals.push(row.value);
                    }
                });
                resolve2(approvals);
                }
            );
        }));
        Promise.all(promises).then((args)=>{
            let approvalList = args[0];
            let masterApprovalList = args[1];

            _.remove(approvalList, (approval) => {
                return approval.isShield;
            });
            resolve(_.concat(approvalList, masterApprovalList));
        }).catch((error) => {
            logger.error('Error in getValidBundleCaseByAgent->getViewByKeys: ', error);
        });
    });
};

module.exports.getValidBundleCaseByAgent = (compCode, keys) => {
  return new Promise(resolve => {
    dao.getViewByKeys('main', 'validbundleApplicationsByAgent', keys, null).then(result => {
        let validCaseNo = [];
        _.each(result && result.rows, (row) => {
          if (row.value && row.value.applicationDocId) {
            validCaseNo.push(row.value.applicationDocId);
          } else if (row.value && row.value.quotationDocId) {
            validCaseNo.push(row.value.quotationDocId);
          }
        });
        resolve(validCaseNo);
      }
    ).catch((error)=>{
        logger.error("Error in getValidBundleCaseByAgent->getViewByKeys: ", error);
    });
  })
}

let _getAgentsProfile = (compCode, keys) => {
  return new Promise(resolve => {
    dao.getViewByKeys('main', 'agents', keys, null).then(result => {
        let agentProfiles = {};
        _.each(result && result.rows, (row) => {
          if (row.value && row.value.agentCode) {
            agentProfiles[row.value.agentCode] = row.value;
          }
        });
        resolve(agentProfiles);
      }
    ).catch((error)=>{
        logger.error("Error in getAgentsProfile->getViewByKeys: ", error);
    });
  })
}
module.exports.getAgentsProfile = _getAgentsProfile;

module.exports.getquotationByAgent = (compCode, keys) => {
  return new Promise(resolve => {
    dao.getViewByKeys('main', 'quotationByAgent', keys, null).then(result => {
        let quotationList = [];
        _.each(result && result.rows, (row) => {
          if (row.value) {
            quotationList.push(row.value);
          }
        });
        resolve(quotationList);
      }
    ).catch((error)=>{
        logger.error("Error in getquotationByAgent->getViewByKeys: ", error);
    });
  })
}

module.exports.getapplicationByAgent = (compCode, keys) => {
  return new Promise(resolve => {
    dao.getViewByKeys('main', 'applicationsByAgent', keys, null).then(result => {
        let applicationList = [];
        _.each(result && result.rows, (row) => {
          if (row.value) {
            applicationList.push(row.value);
          }
        });
        resolve(applicationList);
      }
    )
  }).catch((error)=>{
    logger.error("Error in getapplicationByAgent->getViewByKeys: ", error);
  });
}

let _getBundleidByApplication = (compCode, appId, targetId) => {
    //To-Do
    if (compCode === 'AG'){
        compCode = '01';
    }
    return new Promise((resolve) => {
        dao.getViewRange(
            'main',
            'bundleApplications',
            '["' + compCode + '","application","' + 'SUBMITTED' + '","' + appId + '"]',
            '["' + compCode + '","application","' + 'SUBMITTED' + '","' + appId + '"]',
            null,
            (result) => {
                let resultId = '';
                _.each(result && result.rows, (row) => {
                    if (row.value) {
                        resultId = _.get(row, 'value.'+targetId);
                    }
                });
                resolve(resultId);
            }
        );
    }).catch(error => {
      logger.error("ERROR:: in _getBundleidByApplication: ", error);
    });
}
module.exports.getBundleidByApplication = _getBundleidByApplication;

