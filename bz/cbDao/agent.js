const dao = require('../cbDaoFactory').create();
const _ = require('lodash');
var async = require('async');
var logger = global.logger || console;

module.exports.getDocById = function(docId, callback) {
  logger.log('getDocById');
  dao.getDoc(docId, function(result) {
    if (result) {
      callback(result);
    }
  });
};

var getAgentDocId = function (id) {
  // var agentId = "A-" + (new Buffer(email).toString('base64'));
  var agentId = "U_" + id;
  return agentId;
};

var getAgentSuppDocId = (id) => {
  return 'UX_' + id;
};

module.exports.getAgentSuppDocId = getAgentSuppDocId;

const getAgentById = (profileId, callback) => {
  dao.getDoc(getAgentDocId(profileId), callback);
};
module.exports.getAgentById = getAgentById;

const getLoginAgentById = (profileId, callback) => {
  dao.getDoc(getAgentDocId(profileId), (agent) => {
    if (agent && !agent.error) {
      dao.getDoc(getAgentSuppDocId(profileId), (supp) => {
        let loginAgent = Object.assign({}, agent);
        if (supp && !supp.error) {
          loginAgent = Object.assign(loginAgent, supp);
          getEligProducts(loginAgent, (eligProducts) => {
            loginAgent.eligProducts = eligProducts;
            callback(loginAgent);
          });
        } else if (supp.error === 'not_found') {
          let docId = getAgentSuppDocId(profileId);
          dao.updDoc(docId,{},(resp)=>{
            if (resp && !resp.error){
              loginAgent._id = resp.id;
              loginAgent._rev = resp.rev;
              getEligProducts(loginAgent, (eligProducts) => {
                loginAgent.eligProducts = eligProducts;
                callback(loginAgent);
              });
            }
            else {
              callback(resp);
            }
          });
        }
        else {
          getEligProducts(loginAgent, (eligProducts) => {
            loginAgent.eligProducts = eligProducts;
            callback(loginAgent);
          });
        }
      });
    } else {
      callback(agent);
    }
  });
};
module.exports.getLoginAgentById = getLoginAgentById;

const getEligProducts = function (agent, callback) {
  dao.getDoc('eligProducts', (eligProducts) => {
    if (eligProducts && eligProducts.mapping) {
      getChannels(function(channels){
        const channelType = channels.channels[agent.channel].type === 'AGENCY' ? 'AG' : 'FA';
        const mapping = eligProducts.mapping[channelType];
        const covCodes = new Set();
        _.each(mapping['999999999'], (covCode) => {
          covCodes.add(covCode);
        });
        _.each(mapping[agent.profileId], (covCode) => {
          covCodes.add(covCode);
        });
        callback(Array.from(covCodes));
      });
    } else {
      callback(agent.eligProducts);
    }
  });
};

module.exports.updateDoc = function(docId, data, callback) {
  logger.log('updateDoc');
  dao.updDoc(docId, data, function(result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};


module.exports.getAgentProfile = function(docId, callback) {
  logger.log('getAgentProfile');
  dao.getDoc(docId, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.addAgentProfile = function(docId, user, callback) {
  logger.log('addAgentProfile');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.updateAgentProfile = function(docId, user, callback) {
  logger.log('updateAgentProfile');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

module.exports.agentLoginAud = function(docId, user, callback) {
  logger.log('agentLoginAud');
  dao.updDoc(docId, user, function(result) {
    if (result) {
      callback(result);
    }
  });
};

const getChannels = function(callback) {
  dao.getDocFromCacheFirst("channels", function(result) {
    if (result) {
      callback(result);
    } else {
      callback(false);
    }
  });
};
module.exports.getChannels = getChannels;

module.exports.updateProfilePic = function (docId, type, data, callback) {
  dao.getDoc(docId, (agent) => {
    dao.uploadAttachmentByBase64(docId, 'agentProfilePic', agent._rev, data, type, () => {
      callback(true);
    });
  });
};

module.exports.searchAgents = function(compCode, idS, idE, callback){
    let result = [];
    return new Promise((resolve) => {
        dao.getViewRange(
            'main',
            'agents',
            '["'+ compCode + '","' + idS + '"' +']',
            '["'+ compCode + '","' + idE + '"' +']',
            null,
            function(pList){
              if(pList && pList.rows){
                  for(var i=0; i<pList.rows.length; i++){
                  pList.rows[i].value.docId = pList.rows[i].id;
                  result.push(pList.rows[i].value);
                  }
              }
              resolve(callback({success: true, result}));
            }
        );
    }).catch((e) => {
        logger.error(e);
        callback({success: false});
    });
};

let _getAgents = (compCode, agentCodes) => {
  let currentDate = new Date();
  let keys = _.map(agentCodes, agentCode => '["' + compCode + '","agentCode","' + agentCode + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let agentMap = {};
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        agent.isProxying = agent.rawData && (
          currentDate >= new Date(agent.rawData.proxyStartDate) &&
          currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
        );
        agentMap[row.value.agentCode] = agent;
      });
    }
    return agentMap;
  }).catch((error)=>{
    logger.error("Error in getAgents->getViewByKeys: ", error);
  });
};

module.exports.getAgents = _getAgents;

let _geAllAgents = (compCode) => {
  return new Promise((resolve) => {
    let currentDate = new Date();
    dao.getViewRange(
      'main',
      'agentDetails',
      '["' + compCode + '","agentCode","0"]',
      '["' + compCode + '","agentCode","ZZZ"]',
      null,
      (result) => {
        let agentMap = {};
        if (result) {
          _.each(result.rows, (row) => {
            let agent = row.value || {};
            agent.isProxying = agent.rawData && (
              currentDate >= new Date(agent.rawData.proxyStartDate) &&
              currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
            );
            agentMap[row.value.agentCode] = agent;
            if (row.value.rawData) {
              agentMap[row.value.rawData.userId] = agent;
            }
          });
        }
        resolve(agentMap);
      }
    );
  }).catch(e =>{
    logger.error('Get All Agents Details: ' + e);
  });
};

module.exports.geAllAgents = _geAllAgents;

module.exports.getAgentsByUserId = (compCode, userIds) => {
  let currentDate = new Date();
  let keys = _.map(userIds, userId => '["' + compCode + '","userId","' + userId + '"]');
  return dao.getViewByKeys('main', 'agentDetails', keys, null).then((result) => {
    let agentMap = {};
    if (result) {
      _.each(result.rows, (row) => {
        let agent = row.value || {};
        agent.isProxying = agent.rawData && (
          currentDate >= new Date(agent.rawData.proxyStartDate) &&
          currentDate.getTime() < new Date(agent.rawData.proxyEndDate).getTime() + (1000 * 60 * 60 * 24)
        );
        agentMap[agent.rawData.userId] = agent;
      });
    }
    return agentMap;
  }).catch((error)=>{
    logger.error("Error in getAgentsByUserId->getViewByKeys: ", error);
  });
};

module.exports.getMangerProfileByAgentCode = (compCode, agentCode, cb) => {

  async.waterfall([
    (callback) => {
      _geAllAgents(compCode).then(result => {
          let agentProfile = _.get(result, agentCode);
          callback(null, _.get(result, _.get(agentProfile, 'managerCode')));
        });
      }
  ], (err, managerProfile) => {
      if (err) {
          cb({});
      } else {
          cb(managerProfile);
      }
  });
};

module.exports.getDirectorProfileByAgentCode = (compCode, agentCode) => {
  return new Promise(resolve => {
    async.waterfall([
      (callback) => {
        _geAllAgents(compCode).then(result => {
            let agentProfile = _.get(result, agentCode);
            let managerProfile = _.get(result,  _.get(agentProfile, 'managerCode'));
            callback(null, _.get(result, _.get(managerProfile, 'managerCode')));
          });
        }
      ], (err, directorProfile) => {
          if (err) {
            resolve({});
          } else {
            resolve(directorProfile);
          }
      });
  });
};

module.exports.getManagerDirectorProfileByAgentCode = (compCode, agentCode) => {
    return new Promise(resolve => {
        async.waterfall([
        (callback) => {
            _geAllAgents(compCode).then(result => {
                let agentProfile = _.get(result, agentCode);
                let managerProfile = _.get(result,  _.get(agentProfile, 'managerCode'));
                let directorProfile = _.get(result,  _.get(managerProfile, 'managerCode'));

                let profiles = {};
                profiles.managerProfile = managerProfile;
                profiles.directorProfile = directorProfile;

                callback(null, profiles);
            });
            }
        ], (err, profiles) => {
            if (err) {
                resolve({});
            } else {
                resolve(profiles);
            }
        });
    });
};


module.exports.getNotifications = (agent) => {
  return new Promise(resolve => {
    const { profileId } = agent;
    dao.getDoc(getAgentSuppDocId(profileId), suppDoc => {
      if (suppDoc && !suppDoc.error) {
        const notifications = _.filter(_.get(suppDoc, 'notifications', []), notice => !notice.read);
        resolve(notifications);
      } else {
        resolve([]);
      }
    });
  });
}

module.exports.readAllNotifications = (agent) => {
  return new Promise(resolve => {
    const { profileId } = agent;
    const suppDocId = getAgentSuppDocId(profileId);
    dao.getDoc(suppDocId, suppDoc => {
      if (suppDoc && !suppDoc.error) {
        _.forEach(suppDoc.notifications, notice => {
          if (!notice.read) {
            notice.readTime = new Date();
          }
          notice.read = true;
        });
        dao.updDoc(suppDocId, suppDoc, () => {
          resolve(true);
        });
      } else {
        resolve(false);
      }
    });
  });
}