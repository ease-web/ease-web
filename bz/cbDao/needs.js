const dao = require('../cbDaoFactory').create();
const bDao = require('./bundle');
const cDao = require('./client');
const _ = require('lodash');
const cFunctions = require('../utils/CommonUtils');
var logger = global.logger || console;

const _deactiveData = function(data){
  if(data.init === false)
    data.init = true;
    
  if(data.isValid === true) 
    data.isValid = false;
  
  if (data.isCompleted === true)
    data.isCompleted = false;

  if(data.lastStepIndex > -1){
    data.lastStepIndex = -1;
  }
  
  for(var i in data){
    if(_.isPlainObject(data[i]) || _.isArray(data[i])){
      _deactiveData(data[i]);
    }
  }
}

module.exports.deactiveData = _deactiveData

module.exports.getCNAForm = function() {
  return new Promise((resolve)=>{
    dao.getDocFromCacheFirst("CNAForm", resolve);
  })
}

module.exports.getPdaForm = function() {
  return new Promise((resolve)=>{
    dao.getDocFromCacheFirst("pdaForm", resolve);
  })
}

module.exports.getFeForm = function() {
  return new Promise((resolve)=>{
    dao.getDocFromCacheFirst("feForm", resolve);
  })
}

module.exports.getFnaForm = function(callback) {
  return new Promise((resolve)=>{
    dao.getDocFromCacheFirst("fnaForm", resolve);
  })
}

module.exports.getFNAEmailTemplate = function (callback) {
  dao.getDocFromCacheFirst('fna_email_template', callback);
};

module.exports.getFNAReportTemplate = function () {
  let docIds = ['fna_report_cover_template',
    'fna_report_information_template', 
    'fna_report_needs_template', 
    'fna_report_budget_template',
    'fna_report_cka_template',
    'fna_report_cmd_template'];
  let promises = [];
  _.forEach(docIds, (docId)=>{
    promises.push(new Promise((resolve)=>{
      dao.getDoc(docId, (exDoc)=>{resolve(exDoc)});
    }));
  })
  return Promise.all(promises);
};

module.exports.getNeedsSummary = function(callback) {
  dao.getDocFromCacheFirst('needs_summary', callback);
}

const ITEM_ID = {
  NA: "naid",
  FE: "feid",
  PDA: "pdaid"
}

module.exports.ITEM_ID = ITEM_ID;

module.exports.isFNACompleted = function(cid){
  return new Promise((resolve)=>{
    _getItem(cid, ITEM_ID.PDA).then((pda)=>{
      _getItem(cid, ITEM_ID.FE).then((fe)=>{
        _getItem(cid, ITEM_ID.NA).then((na)=>{
          resolve(pda.isCompleted && fe.isCompleted && na.isCompleted ? true : false);
        }).catch((error)=>{
          logger.error("Error in isFNACompleted-_getItem[NA]: ", error);
        });
      }).catch((error)=>{
        logger.error("Error in isFNACompleted-_getItem[FE]: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in isFNACompleted-_getItem[PDA]: ", error);
    });
  })
}

var _invalidSection = function(cid, itemId, params = {}){
  return new Promise((resolve)=>{
    _getItemId(cid, itemId).then((docId)=>{
      dao.getDoc(docId, (doc)=>{
        if(!doc._rev){
          resolve();
        }
        else {
          _deactiveData(doc);
          if(itemId === ITEM_ID.PDA){
            let {removeApplicant, removeDependant, fid} = params;
            if(removeApplicant && _.get(doc, "applicant") === "joint"){
              delete doc.applicant;
            }
            if(fid && removeDependant){
              let dependants = _.split(_.get(doc, "dependants"), ",");
              let dIdx = dependants.indexOf(fid);
              logger.log("remove dependants's application: ", fid);
              if(dIdx > -1){
                dependants.splice(dIdx, 1);
                doc.dependants = dependants.join(",");
              }
            }
          } else if (itemId === ITEM_ID.FE) {
            doc.revisit = true;
          }
          dao.updDoc(docId, doc, ()=>{
            resolve(doc);
          })
        }
      })
    }).catch((error)=>{
      logger.error("Error in _invalidSection->_getItem: ", error);
    });
  })
}
module.exports.invalidSection = _invalidSection;

var _getItemId = function(cid, itemId){
  return bDao.getCurrentBundle(cid).then((bundle={})=>{
      if (!bundle.id) {
        return;
      }

      let _itemId = _.get(bundle, `fna.${itemId}`);
      let _itemId2 = _itemId === ITEM_ID.PDA? "PDA": itemId === ITEM_ID.FE? "FE": "NA";
      return _itemId || `${bundle.id}-${_itemId2}`;
    }).catch((error)=>{
      logger.error("Error in _getItemId->:getCurrentBundle ", error);
      return;
    });
}
module.exports.getItemId = _getItemId;

var saveItem = function(agent, cid, itemId, data){
  return new Promise((resolve)=>{
    _getItemId(cid, itemId).then((docId)=>{
      data.id = docId;
      data.type = itemId && itemId.substring(0, itemId.length -2);
      delete data.error;
      var now = new Date();
      data.lastUpd = now.getTime();

      data.agentCode = agent.agentCode;
      data.compCode = agent.compCode;
      data.dealerGroup = agent.channel.code;

      //set a flag to record fna have completed
      if (itemId === ITEM_ID.NA && data.isCompleted) {
        data.haveFnaCompleted = true;
      }

      dao.getDoc(docId, function(exDoc) {
        data._rev = exDoc._rev;
        dao.updDoc(docId, data, ()=>{resolve(data)});
      });
    }).catch((error)=>{
      logger.error("Error in saveItem->: _getItemId", error);
    });
      
  })
}

var _getItem = function(cid, itemId){
  return new Promise((resolve)=>{
    _getItemId(cid, itemId).then((docId)=>{
      if (!docId) {
        resolve();
      }
      else {
        dao.getDoc(docId, (exDoc)=>{
          if (exDoc._id && exDoc._rev) {
            delete exDoc.error;
          }
          resolve(exDoc.error?{id: docId}: exDoc);
        });
      }
    }).catch((error)=>{
      logger.error("Error in _getItem->_getItemId: ", error);
      resolve();
    });
      
  });
}
module.exports.getItem = _getItem;

module.exports.savePDA = function(agent, cid, content, tiInfo){
  return new Promise((resolve)=>{
    let promises = [];
    promises.push(saveItem(agent, cid, ITEM_ID.PDA, content));
    promises.push(_invalidSection(cid, ITEM_ID.FE));
    promises.push(_invalidSection(cid, ITEM_ID.NA));
    if(tiInfo){
      promises.push(new Promise((resolve2)=>{
        dao.getDoc(cid, function(profile){
          profile.trustedIndividuals = tiInfo;
          dao.updDoc(cid, profile, ()=>{resolve2(profile)});
        });
      }))
    }
    Promise.all(promises).then((args) => {
      resolve({
        pda: args[0],
        fe: args[1],
        fna: args[2],
        profile: args[3]
      })
    }).catch((error)=>{
      logger.error("Error in savePDA->Promise.all: ", error);
    });
  })
}

module.exports.saveFE = function(agent, cid, content){
  return new Promise((resolve)=>{
    let promises = [];
    promises.push(saveItem(agent, cid, ITEM_ID.FE, content));
    promises.push(_invalidSection(cid, ITEM_ID.NA));
    Promise.all(promises).then((args) => {
      resolve({
        fe: args[0],
        fna: args[1]
      })
    }).catch((error)=>{
      logger.error("Error in saveFE->Promise.all: ", error);
    });
  })
}

module.exports.saveFNA = function(agent, cid, content){
  return new Promise((resolve)=>{
    if(content.lastStepIndex === 4 && _.get(content, "productType.prodType")){
      content.productType.lastProdType = _.get(content, "productType.prodType");
    }

    saveItem(agent, cid, ITEM_ID.NA, content).then((fna)=>{resolve({fna})}).catch((error)=>{
      logger.error("Error in saveFNA->saveItem: ", error);
    });
  })
}

// module.exports.saveFNAReportAtt = function(data, pdfStr, callback){
//   getNAId(data.cid, function(docId){
//     var isFNAReportSigned = data.isSigned;
//     let exec = function(){
//       dao.getDoc(docId, function(exDoc){
//         if (!exDoc.error){
//           var content = exDoc;
//           content._rev = exDoc._rev;
//           var now = new Date();
//           content.lastUpd = now.getTime();
//           content.type = 'fna';

//           dao.uploadAttachmentByBase64(docId, 'fnaReport', content._rev, pdfStr, 'application/pdf', (re) => {
//             dao.getDoc(docId, function(Doc){
//               if (!Doc.error){
//                 now = new Date();
//                 Doc.lastUpd = now.getTime();
//                 Doc.type = 'fna';
//                 Doc.fnaReportSigned = isFNAReportSigned;
//                 dao.updDoc(docId, Doc, function(result) {
//                   if (result && !result.error) {
//                     callback({success:true});
//                   } else {
//                     callback({success:false});
//                   }
//                 })
//               }
//             })
//           });
//         }
//       })  
//     }
    
//     if (isFNAReportSigned) {
      // bDao.updateStatus(data.cid, bDao.BUNDLE_STATUS.SIGN_FNA)
      // .then(exec)
      // .catch((error)=>{
      //   logger.error('ERROR: saveFNAReportAtt :', error);
      //   callback({success: false});
      // });
//       // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.SIGN_FNA, exec);
//     } else {
//       exec();
//     }
//   });
// };

var _getDependants = function(cid){
  return new Promise((resolve)=>{
    _getItem(cid, ITEM_ID.PDA).then((pda)=>{
      cDao.getProfile(cid, true).then((profile)=>{
        let {dependants = "", applicant = ""} = pda;
        let result = dependants.split(",");
        if(applicant == "joint"){
          let spouse = _.find(profile.dependants, d=>d.relationship === "SPO");
          if(spouse){
            result.push(spouse.cid);
          }
        }
        resolve(result);
      }).catch((error)=>{
          logger.error("Error in _getDependants->:getProfile ", error);
      });
    }).catch((error)=>{
      logger.error("Error in _getDependants->_getItem[PDA] ", error);
    });
  });
}
module.exports.getDependants = _getDependants;

var _getExistingPoliciesFromFe = function(pCid, iCid, fe) {
  let policyTableFeValues = {
    existLife: 0,
    existTpd: 0,
    existCi: 0,
    existPaAdb: 0,
    existTotalPrem: 0
  };

  return new Promise((resolve, reject) => {
    if (pCid == iCid) {
      let owner = fe.owner;
      if (owner) {
        policyTableFeValues = Object.assign({}, {
          existLife: (parseInt(owner.lifeInsProt) || 0) + (parseInt(owner.invLinkPol) || 0),
          existTpd: (parseInt(owner.disIncomeProt) || 0),
          existCi: (parseInt(owner.ciPort) || 0),
          existPaAdb: (parseInt(owner.personAcc) || 0),
          existTotalPrem: (parseInt(owner.aInsPrem) || 0)
        })
      }
      resolve(policyTableFeValues);
    } else {
      cDao.getClientById(pCid, function(client) {
        logger.log('INFO: getExistingPoliciesFromFe - getClientById', pCid);
        if (client && !client.error) {
          _getItem(pCid, ITEM_ID.PDA).then((pda)=>{
            if (pda) {
              let dependant = client.dependants.find(function(d) {
                return d.cid == iCid;
              })
  
              if (dependant && dependant.relationship == 'SPO' && pda.applicant == 'joint') {
                logger.log('INFO: getExistingPoliciesFromFe - fe.spouse iCid', iCid);
  
                let spouse = fe.spouse;
                if (spouse) {
                  policyTableFeValues = Object.assign({}, {
                    existLife: (parseInt(spouse.lifeInsProt) || 0) + (parseInt(spouse.invLinkPol) || 0),
                    existTpd: (parseInt(spouse.disIncomeProt) || 0),
                    existCi: (parseInt(spouse.ciPort) || 0),
                    existPaAdb: (parseInt(spouse.personAcc) || 0),
                    existTotalPrem: (parseInt(spouse.aInsPrem) || 0)
                  });
                }
  
              } else {
                if (fe.dependants) {
                  logger.log('INFO: getExistingPoliciesFromFe - fe.dependants iCid', iCid);
                  let dep = fe.dependants.find(function(d) {
                    return d.cid == iCid;
                  });
  
                  if (dep) {
                    policyTableFeValues = Object.assign({}, {
                      existLife: (parseInt(dep.lifeInsProt) || 0) + (parseInt(dep.invLinkPol) || 0),
                      existTpd: (parseInt(dep.disIncomeProt) || 0),
                      existCi: (parseInt(dep.ciPort) || 0),
                      existPaAdb: (parseInt(dep.personAcc) || 0),
                      existTotalPrem: (parseInt(dep.aInsPrem) || 0)
                    });
                  }
                }
              }
            }
            resolve(policyTableFeValues);
          }).catch((error) => {
            logger.error('ERROR: getExistingPoliciesFromFe - getPda', error);
          })
        } else {
          reject(client);
        }
      })
    }
  })
}
module.exports.getExistingPoliciesFromFe = _getExistingPoliciesFromFe

const _showFnaInvalidFlag = function (cid) {
  return new Promise((resolve, reject) => {
    //return true if fna is completed before but not complete in current status
    _getItem(cid, ITEM_ID.NA).then((na = {})=>{
      resolve(!na.isCompleted && na.haveFnaCompleted);
    });
  });
}
module.exports.showFnaInvalidFlag = _showFnaInvalidFlag