const _ = require('lodash');
const dao = require('../cbDaoFactory').create();
const {getAges} = require('../../common/DateUtils');
const {checkEntryAge} = require('../../common/ProductUtils');
var logger = global.logger || console;

module.exports.queryBasicPlansByClient = function(compCode, insured, proposer, params) {
  return new Promise((resolve) => {
    if (!proposer) {
      proposer = insured;
    }
    const currentDate = new Date();
    const iAges = getAges(currentDate, new Date(insured.dob));
    const oAges = getAges(currentDate, new Date(proposer.dob));
    const now = currentDate.getTime();
    dao.getViewRange('main','products',
      '["' + compCode + '","B","0"]',
      '["' + compCode + '","B","ZZZ"]',
      params ? params : null,
      function(list){
        var filteredList = [];

        if (list && list.rows && list.rows.length) {
          logger.log('before filter :', list.rows.length);

          var pubDateMap = {};
          for (var i in list.rows) {
            var p = list.rows[i].value;

            if (!canViewProduct(p, insured, iAges, proposer, oAges, params.ccy, now)) {
              continue;
            }

            // set the doc id
            p._id = list.rows[i].id;

            if (pubDateMap[p.covCode]) {
              if (pubDateMap[p.covCode].version > p.version) {
                continue;
              } else {
                filteredList[pubDateMap[p.covCode].index] = p;
                pubDateMap[p.covCode] = {
                  version: p.version,
                  index: pubDateMap[p.covCode].index
                };
              }
            } else {
              pubDateMap[p.covCode] = {
                pubDate: p.pubDate,
                index: filteredList.length
              };
              filteredList.push(p);
            }
          }
        }
        resolve(filteredList);
      }
    );
  });
};

module.exports.getProductByVersion = function(compCode, covCode, version, callback) {
  dao.getDocFromCacheFirst(compCode + '_product_' + covCode + '_' + version, function(product) {
    callback(product);
  });
};

module.exports.getProduct = function(docId, callback) {
  dao.getDoc(docId, function(product) {
    callback(product);
  });
};

module.exports.getAttachment = function(planId, attName, callback) {
  dao.getAttachment(planId, attName, callback);
};

module.exports.getAttachmentWithLang = function (productId, attName, lang, callback) {
  dao.getAttachment(productId, attName + (lang ? ('_' + lang) : ''), callback);
};

module.exports.getRiderByCovCode = function(compCode, covCode, callback) {
  var now = (new Date()).getTime();
  dao.getViewRange('main', 'products',
    '["' + compCode + '","R","' + covCode + '"]',
    '["' + compCode + '","R","' + covCode + '"]',
    null,
    function(list){
      if (!!list && list.rows && list.rows.length) {
        logger.log('list size:', list.rows.length);

        var pubDateMap = {};
        var filteredList = [];

        for (var i in list.rows) {
          var productId = list.rows[i].id;
          var p = list.rows[i].value;

          if (!(new Date(p.effDate) <= now && new Date(p.expDate) >= now)) {
            continue; // filter it if not match condition
          }

          if (pubDateMap[p.covCode]) {
            if (pubDateMap[p.covCode].pubDate > p.pubDate) {
              continue;
            } else {
              pubDateMap[p.covCode] = {
                pubDate: p.pubDate,
                index: pubDateMap[p.covCode].index
              };
              filteredList[pubDateMap[p.covCode].index] = p;
            }
          } else {
            pubDateMap[p.covCode] = {
              pubDate: p.pubDate,
              index: filteredList.length
            };
            filteredList.push(p);
          }
          logger.log('Products :: getRiderByCovCode :: getting: product with covCode: after filter', filteredList.length);

          if (filteredList.length) {
            dao.getDoc(productId, function(product) {
              if (product) {
                callback(product);
              } else {
                callback(false);
              }
            });
          } else {
            callback(false);
          }
        }
      } else {
        callback(false);
      }
    }
  );
};

module.exports.canViewProduct = function (productId, proposer, insured, callback) {
  if (!insured) {
    insured = proposer;
  }
  dao.getDoc(productId, (product) => {
    const currentDate = new Date();
    const pAges = getAges(currentDate, new Date(proposer.dob));
    const iAges = getAges(currentDate, new Date(insured.dob));
    const now = currentDate.getTime();
    if (canViewProduct(product, insured, iAges, proposer, pAges, null, now)) {
      callback(product);
    } else {
      callback(false);
    }
  });
};

var canViewProduct = function (product, insured, iAges, proposer, pAges, ccy, now) {
  var p = product;

  if (!checkEntryAge(p, insured.residenceCountry, pAges, iAges)) {
    logger.log('Products :: canViewProduct :: product filtered as owner age:', p.covCode, p.entryAge);
    return false;
  }

  if (!(p.genderInd === '*' || p.genderInd === insured.gender)){
    logger.log('Products :: canViewProduct :: product filtered as gender:', p.genderInd);
    return false;
  }

  if (p.ctyGroup.indexOf('*') === -1 && p.ctyGroup.indexOf(insured.residenceCountry) === -1) {
    logger.log('Products :: canViewProduct :: product filtered as city group:', p.covCode, p.ctyGroup);
    return false;
  }

  if (p.smokeInd !== '*' && p.smokeInd !== insured.isSmoker) {
    logger.log('Products :: canViewProduct :: product filtered as smoke:', p.covCode, p.smokeInd);
    return false;
  }

  if (ccy && ccy !== 'ALL') {
    var currency = _.find(p.currencies, (c) => (c.country === '*' || c.country === insured.residenceCountry));
    if (!currency || !_.find(currency.ccy, (c) => c === ccy)) {
      logger.log('Products :: canViewProduct :: product filtered as ccy:', p.covCode, currency, ccy);
      return false;
    }
  }

  if (now < new Date(p.effDate) || now > new Date(p.expDate)) {
    logger.log('Products :: canViewProduct :: product filtered as not within date:', p.covCode, p.effDate, p.expDate, now);
    return false;
  }

  return true;
};

module.exports.getProductSuitability = () => {
  return new Promise((resolve) => {
    dao.getDocFromCacheFirst('productSuitability', resolve);
  });
};
