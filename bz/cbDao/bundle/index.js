const _c = require("./common");
const _a = require("./applications");
const _cl = require("./client");
const _n = require("./needs");
const _q = require("./quotation");

module.exports.BUNDLE_STATUS = _c.BUNDLE_STATUS;
module.exports.CHECK_CODE = _c.CHECK_CODE;
module.exports.rollbackStatus = _c.rollbackStatus;
module.exports.updateStatus = _c.updateStatus;
module.exports.createNewBundle = _c.createNewBundle;
module.exports.getCurrentBundle = _c.getCurrentBundle;
module.exports.getBundle = _c.getBundle;
module.exports.updateBundle = _c.updateBundle;

module.exports.onCreateQuotation = _q.onCreateQuotation;
module.exports.onSaveQuotation = _q.onSaveQuotation;
module.exports.getProposalFuncPerms = _q.getProposalFuncPerms;
module.exports.getAppIdByQuotId = _q.getAppIdByQuotId;
module.exports.invalidateApplicationsByFund = _q.invalidateApplicationsByFund;

module.exports.invalidFamilyMember = _cl.invalidFamilyMember;
module.exports.invalidClient = _cl.invalidClient;
module.exports.onSaveClient = _cl.onSaveClient;
module.exports.onSaveTrustedIndividual = _cl.onSaveTrustedIndividual;
module.exports.invalidTrustedIndividual = _cl.invalidTrustedIndividual;
module.exports.updateApplicationTrustedIndividual = _cl.updateApplicationTrustedIndividual;

module.exports.onDeleteApplications = _a.onDeleteApplications;
module.exports.onCreateApplication = _a.onCreateApplication;
module.exports.onSubmitApplication = _a.onSubmitApplication;
module.exports.onApplyApplication = _a.onApplyApplication;
module.exports.onInvalidateApplicationById = _a.onInvalidateApplicationById;
module.exports.getApplicationIdsByBundle = _a.getApplicationIdsByBundle;
module.exports.getApplication = _a.getApplication;
module.exports.getDocByQuotId = _a.getDocByQuotId;
module.exports.getApplicationByBundleId = _a.getApplicationByBundleId;
module.exports.updateApplicationClientProfile = _a.updateApplicationClientProfile;
module.exports.updateApplicationClientProfiles = _a.updateApplicationClientProfiles;
module.exports.getFormValuesByCid = _a.getFormValuesByCid;
module.exports.updateApplicationReplacementOfPolicies = _a.updateApplicationReplacementOfPolicies;
module.exports.getApplyingApplicationsCount = _a.getApplyingApplicationsCount;
module.exports.updateQuotCheckedList = _a.updateQuotCheckedList;
module.exports.updateApplicationFnaAnsToApplications = _a.updateApplicationFnaAnsToApplications;
module.exports.getApplicationStatus = _a.getApplicationStatus;
module.exports.invalidateApplicationByClientId = _a.invalidateApplicationByClientId;

module.exports.onSaveNeedItem = _n.onSaveNeedItem;
module.exports.invalidNeedItem = _n.invalidNeedItem;
module.exports.onSavePDA = _n.onSavePDA;
module.exports.onSaveFE = _n.onSaveFE;
module.exports.onSaveFNA = _n.onSaveFNA;

