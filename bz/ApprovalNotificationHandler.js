const dao = require('./cbDaoFactory').create();
const nUtil = require('./utils/NotificationsUtils');
const _isEqual = require('lodash/fp/isEqual');
const _isEmpty = require('lodash/fp/isEmpty');
const  _getOr = require('lodash/fp/getOr');
const approvalStatusModel = require('./model/approvalStatus');
const emailHandler = require('./EmailHandler');
const approvalhandler = require('./ApprovalHandler');
const smsHandler = require('./SMSHandler');

const _get = require('lodash/fp/get');
const logger = global.logger || console;
const moment = require('moment');
const ConfigConstant = require('../app/constants/ConfigConstants');

let DefaultEmailSender;


module.exports.pendingDocNotification = function(data, session, cb){
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });  
        });
    }).then(tempAppCase => {
        let agentId = _get('agentId', tempAppCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, tempAppCase).then(rolesObj =>{
            if (_isEqual(approvalStatusModel.APPRVOAL_STATUS_PDOC, _get('approvalStatus', tempAppCase))) {
                const currentUserCode = _get('agentCode', session.agent);
                const recipients = _get('agent.email', rolesObj);
                const assignedMangerCode = _get('assignedManager.agentCode', rolesObj);
                const mangerCode = _get('manager.agentCode', rolesObj);
                let cc = '';
                if (!_isEqual(currentUserCode, mangerCode)){
                    cc = _get('manager.email', rolesObj);
                }

                // const caseNumber = _get('policyId', tempAppCase);
                const caseNumber = _get('proposalNumber', tempAppCase) || _get('policyId', tempAppCase);
                const productName = _get('productName', tempAppCase);
                const agentName = _get('agentName', tempAppCase);
                const proposerName = _get('customerName', tempAppCase);

                const submissionDate = moment(_get('submittedDate', tempAppCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                const expiryDate = moment(_get('submittedDate', tempAppCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);

                try {
                    const email = nUtil.getEmailObject(nUtil.TYPE.PENDING_FOR_DOCUMENT_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients,
                        caseNumber,
                        productName,
                        agentName,
                        proposerName,
                        submissionDate,
                        expiryDate,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});
                } catch (e) {
                    // ignore
                    logger.error('Pending Doc Email Exception: ' + e);
                    cb({success: false});
                }
            }
            cb({success: true});
        });
    }).catch(e => {
        logger.error('Pending Doc Email Exception: ' + e);
        cb({success: false});
    });
};

module.exports.ApproveNotification = function(data, session){
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });
        });
    }).then(updatedCase => {
        let agentId = _get('agentId', updatedCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, updatedCase).then(rolesObj =>{
            let managerCode = _get('manager.agentCode', rolesObj);
            let cc = '';
            const managerEmail = _get('manager.email', rolesObj);
            const assignedManagerEmail = _get('assignedManager.email', rolesObj);

            if (!_isEqual(_getOr('', 'approveRejectManagerId', updatedCase ), managerCode) ){
                cc = managerEmail;
            }

            const approvedManagerEmail = _get('approveRejectManagerEmail', updatedCase);
            const agentEmail = _get('agent.email', rolesObj);
            let recipients = agentEmail;

            const mobileNo = _get('agent.mobile', rolesObj);

            const productName = _get('productName', updatedCase);
            // const proposalNumber = _get('policyId', updatedCase);
            const proposalNumber = _get('proposalNumber', updatedCase) || _get('policyId', updatedCase);
            const proposerName = _get('customerName', updatedCase);
            const supervisorName = _get('approveRejectManagerName', updatedCase);

            let approvalDate = moment(_get('approveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
            const adviserName = _get('agentName', updatedCase);
            const approvalRemarksText = _isEmpty(_getOr('', 'accept.approveComment', updatedCase)) ? '-' : _get('accept.approveComment', updatedCase);

            if (data.approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION) {
                try {
                    const email = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT, {
                        sender: DefaultEmailSender,
                        recipients,
                        productName,
                        proposalNumber,
                        proposerName,
                        supervisorName,
                        approvalDate,
                        adviserName,
                        approvalRemarksText,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});

                    const sms = nUtil.getSMSObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_NOT_SELECTED_AGENT, {
                        mobileNo,
                        productName,
                        proposalNumber,
                        proposerName,
                        supervisorName,
                        approvalDate
                    });
                    smsHandler.sendSMS({sms}, session, () => {});

                } catch (e) {
                    // ignore
                    logger.error('Prepare Approved Notification Error' + e);
                }
            }else if (data.approverLevel === approvalStatusModel.FAAGENT) {
                try {
                    const agentName = _get('agentName', updatedCase);
                    const submissionDate = moment(_get('submittedDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    const expiryDate = moment(_get('submittedDate', updatedCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    approvalDate = moment(_get('supervisorApproveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                    const email = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_FASUPERVISOR, {
                        sender: DefaultEmailSender,
                        recipients,
                        productName,
                        proposalNumber,
                        proposerName,
                        supervisorName,
                        approvalDate,
                        adviserName,
                        approvalRemarksText,
                        agentName,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});

                    const agentId = _get('agentId', updatedCase);
                    const actionLink = _getOr('', 'config.host.path', global) + '/goApproval';
                    approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: agentId}, session, (resp) => {
                        if (resp && resp.success) {
                            let resultArr = [];
                            ['faAdminCode'].forEach(obj => {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            })
                            approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: resultArr[0]}, session, (resp) => {
                                if (resp && resp.success) {
                                    let emailResult = [];
                                    ['email'].forEach(obj => {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    })
                                    recipients = emailResult[0];
                                    try {
                                      const faEmail = nUtil.getEmailObject(nUtil.TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM, {
                                          sender: DefaultEmailSender,
                                          recipients,
                                          productName,
                                          proposalNumber,
                                          proposerName,
                                          supervisorName,
                                          adviserName,
                                          submissionDate,
                                          expiryDate,
                                          agentName,
                                          actionLink
                                      });
                                      emailHandler.sendEmail({email: faEmail}, session, () => {});
                                    } catch (faExp) {
                                      logger.error('Prepare FA Channel Approve Email: ' + faExp);
                                    }
                                }
                            })
                        }
                    })

                } catch (e) {
                    //ignore
                    logger.error('Prepare FA Channel Approve Email: ' + e);
                }
            }else if (data.approverLevel === approvalStatusModel.FAADMIN) {
                try {
                  recipients = (_isEqual(managerCode, _getOr('', 'approveRejectManagerId', updatedCase ))) ? `${agentEmail},${managerEmail}` : `${agentEmail},${managerEmail},${assignedManagerEmail}`
                  const faAdminApprovalRemarksText = _isEmpty(_getOr('', 'accept_FAAdmin.approveComment', updatedCase)) ? '-' : _get('accept_FAAdmin.approveComment', updatedCase);
                  const agentName = _get('agentName', updatedCase);
                  const email = nUtil.getEmailObject(nUtil.TYPE.APPROVED_NOTIFICATION_FOR_FAFIRM, {
                      sender: DefaultEmailSender,
                      recipients,
                      productName,
                      adviserName,
                      proposalNumber,
                      agentName,
                      proposerName,
                      supervisorName,
                      approvalDate,
                      faAdminApprovalRemarksText
                  });

                  emailHandler.sendEmail({email}, session, () => {});
                } catch (e) {
                    //ignore
                    logger.error('Prepare FA Admin Approve Email: ' + e);
                }

            }
        });
    }).catch(e => {
        logger.error('Prepare Approve notification Error: ' + e);
    });
};

module.exports.RejectNotification = function(data, session){
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });
        });
    }).then(updatedCase => {
        let agentId = _get('agentId', updatedCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, updatedCase).then(rolesObj =>{
            let managerCode = _get('manager.agentCode', rolesObj);
            let cc = '';
            const managerEmail = _get('manager.email', rolesObj);
            let assignedManagerEmail = _get('assignedManager.email', rolesObj);
            if (!_isEqual(_getOr('', 'approveRejectManagerId', updatedCase ), managerCode) ){
                cc = managerEmail;
            }

            const agentEmail = _get('agent.email', rolesObj);
            let recipients = agentEmail;

            const mobileNo = _get('agent.mobile', rolesObj);

            const productName = _get('productName', updatedCase);
            const adviserName = _get('agentName', updatedCase);
            // const proposalNumber = _get('policyId', updatedCase);
            const proposalNumber = _get('proposalNumber', updatedCase) || _get('policyId', updatedCase);
            const proposerName = _get('customerName', updatedCase);
            const supervisorName = _get('approveRejectManagerName', updatedCase);
            const reasonForRejection = _get('reject.rejectReason', updatedCase);
            const rejectionCommentText = _get('reject.rejectComment', updatedCase);
            let rejectionDate = moment(_get('approveRejectDate', updatedCase)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);;
            if (data.approverLevel === approvalStatusModel.ONELEVEL_SUBMISSION){
                try {
                    const email = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients,
                        productName,
                        adviserName,
                        proposalNumber,
                        proposerName,
                        supervisorName,
                        reasonForRejection,
                        rejectionCommentText,
                        rejectionDate,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});

                    if (_getOr('', 'channel.type', session.agent) !== 'FA'){
                        const sms = nUtil.getSMSObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                            mobileNo,
                            productName,
                            proposalNumber,
                            proposerName,
                            supervisorName,
                            rejectionDate
                        });
                        smsHandler.sendSMS({sms}, session, () => {});
                    }

                } catch (e) {
                    // ignore
                    logger.error('Prepare Reject Notification Agency Channel: ' + e);
                }
            } else if (data.approverLevel === approvalStatusModel.FAAGENT) {
                try {
                    const email = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION, {
                        sender: DefaultEmailSender,
                        recipients,
                        productName,
                        adviserName,
                        proposalNumber,
                        proposerName,
                        supervisorName,
                        reasonForRejection,
                        rejectionCommentText,
                        rejectionDate,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});

                    const agentId = _get('agentId', updatedCase);
                    approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: agentId}, session, (resp) => {
                        if (resp && resp.success) {
                            let resultArr = [];
                            ['faAdminCode'].forEach(obj => {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            })
                            approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: resultArr[0]}, session, (resp) => {
                                if (resp && resp.success) {
                                    let emailResult = [];
                                    ['email'].forEach(obj => {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    })
                                    recipients = emailResult[0];
                                    try {
                                      const faEmail = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION_SUPERVISOR_TO_FAFIRM, {
                                          sender: DefaultEmailSender,
                                          recipients,
                                          productName,
                                          adviserName,
                                          proposalNumber,
                                          proposerName,
                                          supervisorName,
                                          reasonForRejection,
                                          rejectionCommentText,
                                          rejectionDate
                                      });
                                      emailHandler.sendEmail({email: faEmail}, session, () => {});
                                    } catch (faExp) {
                                      logger.error('Prepare Reject Notification FA Channel: ' + faExp);
                                    }

                                }
                            })
                        }
                    })

                } catch (e) {
                    //ignore
                    logger.error('Prepare Reject Notification FA Channel: ' + e);
                }
            }else if (data.approverLevel === approvalStatusModel.FAADMIN) {
                try {
                    recipients = (_isEqual(managerCode, _getOr('', 'approveRejectManagerId', updatedCase ))) ? `${agentEmail},${managerEmail}` : `${agentEmail},${managerEmail},${assignedManagerEmail}`
                    const faAdminRejectionCommentText = _isEmpty(_getOr('', 'reject_FAAdmin.rejectComment', updatedCase)) ? '-' : _get('reject_FAAdmin.rejectComment', updatedCase);
                    const faAdminreasonForRejection = _get('reject_FAAdmin.rejectReason', updatedCase);
                    const email = nUtil.getEmailObject(nUtil.TYPE.REJECTED_NOTIFICATION_FAFIRM, {
                        sender: DefaultEmailSender,
                        recipients,
                        productName,
                        adviserName,
                        proposalNumber,
                        proposerName,
                        faAdminreasonForRejection,
                        faAdminRejectionCommentText,
                        rejectionDate
                    });
                    emailHandler.sendEmail({email}, session, () => {});
                } catch (e) {
                    //ignore
                    logger.error('Prepare Reject Notification FA Admin: ' + e);
                }

            }
        })

    }).catch( e=>{
        logger.error('Prepare Reject Notification Error: ' + e);
    });
}

module.exports.AdditionalDocumentNotification = function(data, session, cb){
    let { isHealthDeclaration, id } = data;
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });
        });
    }).then(approvalCase => {
        let agentId = _get('agentId', approvalCase);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, approvalCase).then(rolesObj =>{
            let agentProfile = session.agent;

            let managerEmail = _get('manager.email', rolesObj);
            let proxyManagerEmail = _get('assignedManager.email', rolesObj);
            let managerMobile = _get('manager.mobile', rolesObj);
            let proxyManagerMobile  = _get('assignedManager.mobile', rolesObj);

            let recipients = _isEqual(managerEmail, proxyManagerEmail) ? managerEmail : proxyManagerEmail;
            let cc = _isEqual(managerEmail, proxyManagerEmail) ? '' : managerEmail;
            let mobileNo = _isEqual(managerMobile, proxyManagerMobile) ? managerMobile : proxyManagerMobile;

            // const proposalNumber = _get('policyId', approvalCase);
            const proposalNumber = _get('proposalNumber', approvalCase) || _get('policyId', approvalCase);
            const productName = _get('productName', approvalCase);
            
            // const caseNumber = _get('policyId', approvalCase);
            const caseNumber = _get('proposalNumber', approvalCase) || _get('policyId', approvalCase);
            const proposerName = _get('proposerName', approvalCase);
            const expiryDate = moment(_get('submittedDate', approvalCase)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);

            let agentName = _get('agentName', approvalCase);

            try {
                if ((_isEqual(session.agent.agentCode, _get('manager.agentCode', rolesObj)) ||
                        _isEqual(session.agent.agentCode, _get('assignedManager.agentCode', rolesObj))) && isHealthDeclaration ) {
                          // Health Declaration is uploaded by supervisor
                          _healthDeclarationIsUploadedBySupervisorNotification(id, session);
                        }  

                let pendingFAFirmStatus =[approvalStatusModel.APPRVOAL_STATUS_PDOCFAF, approvalStatusModel.APPRVOAL_STATUS_PFAFA, approvalStatusModel.APPRVOAL_STATUS_PDISFAF];
                // Pending in FA Firm state
                if (approvalCase && pendingFAFirmStatus.indexOf(_get('approvalStatus', approvalCase)) > -1) {
                    const agentId = _get('agentId', approvalCase);
                    if (agentId === session.agent.agentCode) {
                        cc = _isEqual(managerEmail, proxyManagerEmail) ? `${managerEmail}` : `${managerEmail},${proxyManagerEmail}`;
                    }else {
                        cc = '';
                    }
                    agentName = session.agent.name;
                    approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: agentId}, session, (resp) => {
                        if (resp && resp.success) {
                            let resultArr = [];
                            ['faAdminCode'].forEach(obj => {
                                resultArr.push(_getOr('', 'result.' + obj, resp));
                            })
                            approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: resultArr[0]}, session, (resp) => {
                                if (resp && resp.success) {
                                    let emailResult = [];
                                    ['email'].forEach(obj => {
                                        emailResult.push(_getOr('', 'result.' + obj, resp));
                                    })
                                    recipients = emailResult[0];
                                    try {
                                      let email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_FA_NOTIFICATION, {
                                        sender: DefaultEmailSender,
                                        recipients,
                                        proposalNumber,
                                        productName,
                                        agentName,
                                        caseNumber,
                                        proposerName,
                                        expiryDate,
                                        cc
                                        });
                                        emailHandler.sendEmail({email}, session, () => {});
                                    } catch (additionalExp) {
                                      logger.error('Prepare Additional Doc Email Exception: ' + additionalExp);
                                    }
                                }
                            })
                        }
                    })
                    cb({success: true});
                } else {
                    // Agency Channel -- Supervisor will not receive addtional doc email
                    if (_isEqual(session.agent.agentCode, _get('manager.agentCode', rolesObj)) ||
                        _isEqual(session.agent.agentCode, _get('assignedManager.agentCode', rolesObj)) ) {
                          cb({success: true});
                        } else {
                          try {
                            let email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION, {
                              sender: DefaultEmailSender,
                              recipients,
                              proposalNumber,
                              productName,
                              agentName,
                              caseNumber,
                              proposerName,
                              expiryDate,
                              cc
                              });
                              emailHandler.sendEmail({email}, session, () => {});

                              if (_getOr('', 'channel.type', agentProfile) !== 'FA'){
                                  const sms = nUtil.getSMSObject(nUtil.TYPE.ADDITIONAL_DOCUMENT_UPLOADED_NOTIFICATION, {
                                      mobileNo,
                                      agentName,
                                      caseNumber,
                                      proposerName,
                                      expiryDate
                                  });
                                  smsHandler.sendSMS({sms}, session, () => {});
                              }
                              cb({success: true});
                          } catch (addExp) {
                            logger.error('Prepare Additional Doc Email Exception: ' + addExp);
                            cb({success: false});
                          }

                        }

                }
            } catch (e) {
                // ignore
                logger.error('Prepare Additional Doc Email Exception: ' + e);
                cb({success: false});
            }
        });

    }).catch(e => {
        logger.error('Additional Doc Email Exception: ' + e);
        cb({success: false});
    });
};

const _healthDeclarationIsUploadedBySupervisorNotification = function (approvalId, session) {
    new Promise(resolve => {
        dao.getDoc(approvalId, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });
        });
    }).then((doc) => {
        let agentId = _get('agentId', doc);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, doc).then(rolesObj =>{
            if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_APPROVED) {
                let agentProfile = session.agent;
                const agentEmail = _get('agent.email', rolesObj);
                const managerEmail = _get('manager.email', rolesObj);
                const assignedManagerEmail = _get('assignedManager.email', rolesObj);
                let cc = '';
                if (!_isEqual(managerEmail, assignedManagerEmail)){
                    cc = managerEmail;
                }
                let recipients = agentEmail;
                const mobileNo = _get('agent.mobile', rolesObj);
                
                const adviserName = _get('agentName', doc);
                // const proposalNumber = _get('policyId', doc);
                let proposalNumber = _get('proposalNumber', doc) || _get('policyId', doc);
                const proposerName = _get('customerName', doc);

                try {
                    const email = nUtil.getEmailObject(nUtil.TYPE.ADDITIONALDOC_HEALTHDECLARATION, {
                        sender: DefaultEmailSender,
                        recipients,
                        adviserName,
                        proposalNumber,
                        proposerName,
                        cc
                    });
                    emailHandler.sendEmail({email}, session, () => {});
                } catch (e) {
                // ignore
                logger.error('Prepare Additional Document in Health Declaration email: ' + e);
                }

                try {
                if (_getOr('', 'channel.type', agentProfile) !== 'FA'){
                    const sms = nUtil.getSMSObject(nUtil.TYPE.ADDITIONALDOC_HEALTHDECLARATION, {
                        mobileNo,
                        adviserName,
                        proposalNumber,
                        proposerName
                    });
                    smsHandler.sendSMS({sms}, session, () => {});
                }

                } catch (ex) {
                //ignore
                    logger.error('Prepare Additional Document in Health Declaration sms: ' + ex);
                }
            }
        });
    }).catch(e =>{
        logger.error('Additional Document in Health Declaration: ' + e);
    });
};

module.exports.SubmittedCaseNotification = function(data, session){
    new Promise(resolve => {
        dao.getDoc(data.id, function(doc){
          let approvalCase = doc;
          dao.getDoc('companyInfo', function(companyInfo) {
            DefaultEmailSender = _get('fromEmail', companyInfo);
            resolve(approvalCase);
          });
        });
    }).then((doc) => {
        let agentId = _get('agentId', doc);
        return approvalhandler.getRelatedAgentsWithRoleAssigned(session.agent.compCode, agentId, doc).then(rolesObj =>{
            if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_APPROVED) {
                let agentProfile = session.agent;
                const managerEmail = _get('manager.email', rolesObj);
                const assignedManagerEmail = _get('assignedManager.email', rolesObj);
                let recipients = _isEqual(managerEmail, assignedManagerEmail) ? managerEmail : assignedManagerEmail;

                const managerMobile = _get('manager.mobile', rolesObj);
                const assignedManagerMobile = _get('assignedManager.mobile', rolesObj);
                const mobileNo = _isEqual(managerMobile, assignedManagerMobile) ? managerMobile : assignedManagerMobile;

                const agentName = _get('agentName', doc);
                const productName = _get('productName', doc);
                const adviserName = _get('agentName', doc);
                const submissionDate = moment(_get('submittedDate', doc)).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                // const proposalNumber = _get('policyId', doc);
                let proposalNumber = _get('proposalNumber', doc) || _get('policyId', doc);
                const proposerName = _get('customerName', doc);
                const expiryDate = moment(_get('submittedDate', doc)).add(global.config.EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION - 1, 'd').utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3);
                const actionLink = _getOr('', 'config.host.path', global) + '/goApproval';

                try {
                    if (doc && _get('approvalStatus', doc) !== approvalStatusModel.APPRVOAL_STATUS_PFAFA) {
                        const email = nUtil.getEmailObject(nUtil.TYPE.SUBMISSION_NOTIFICATION, {
                            sender: DefaultEmailSender,
                            recipients,
                            agentName,
                            productName,
                            adviserName,
                            submissionDate,
                            proposalNumber,
                            proposerName,
                            expiryDate,
                            actionLink
                        });
                        emailHandler.sendEmail({email}, session, () => {});
                    } else {
                        approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: agentId}, session, (resp) => {
                            let supervisorName = adviserName;
                            if (resp && resp.success) {
                                let resultArr = [];
                                ['faAdminCode'].forEach(obj => {
                                    resultArr.push(_getOr('', 'result.' + obj, resp));
                                })
                                approvalhandler.getAgentProfileId({compCode: session.agent.compCode, id: resultArr[0]}, session, (resp) => {
                                    if (resp && resp.success) {
                                        let emailResult = [];
                                        ['email'].forEach(obj => {
                                            emailResult.push(_getOr('', 'result.' + obj, resp));
                                        })
                                        recipients = emailResult[0];
                                        try {
                                          const faEmail = nUtil.getEmailObject(nUtil.TYPE.APPROVED_SUPERVISOR_NOTIFICATION_FOR_FAFIRM, {
                                              sender: DefaultEmailSender,
                                              recipients,
                                              productName,
                                              proposalNumber,
                                              proposerName,
                                              supervisorName,
                                              adviserName,
                                              submissionDate,
                                              expiryDate,
                                              agentName,
                                              actionLink
                                          });
                                          emailHandler.sendEmail({email: faEmail}, session, () => {});
                                        } catch (faExp) {
                                          logger.error('Prepare Application submitted approval email: ' + faExp);
                                        }

                                    }
                                })
                            }
                        })
                    }
                } catch (e) {
                // ignore
                logger.error('Prepare Application submitted approval email: ' + e);
                }

                try {
                if (_getOr('', 'channel.type', agentProfile) !== 'FA'){
                    const sms = nUtil.getSMSObject(nUtil.TYPE.SUBMISSION_NOTIFICATION, {
                    mobileNo,
                    productName,
                    adviserName,
                    submissionDate,
                    proposalNumber,
                    proposerName,
                    expiryDate
                    });
                    smsHandler.sendSMS({sms}, session, () => {});
                }

                } catch (ex) {
                //ignore
                    logger.error('Prepare Application submitted approval sms: ' + ex);
                }
            }
        });
    }).catch(e =>{
        logger.error('Application Submitted approval Notifiation: ' + e);
    })
}
