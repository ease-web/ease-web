"use strict";
var _               = require('lodash');
const getByLang     = require('./utils/CommonUtils').getByLang;
var commonFunction  = require('./CommonFunctions');
var XSLT            = require('./xslt/XSLT.js');
var logger          = global.logger || console;
var moment          = require('moment');
const ConfigConstant = require('../app/constants/ConfigConstants');


var _getStaticPdf = function(pdfCodes, reportData, reportTemplates, callback){
  var lang = 'en'; // TODO

  var pageNum = 1;
  var startPages = {};
  _.each(pdfCodes, (pdfCode) => {
    startPages[pdfCode] = pageNum;
    var reportTemplate = reportTemplates[pdfCode];
    if (reportTemplate) {
      var templates = getByLang(reportTemplate.template);
      pageNum += templates.length;
    }
  });

  var promises = [];
  _.each(pdfCodes, (pdfCode) => {
    if (reportTemplates[pdfCode]) {
      promises.push(new Promise((resolve) => {
        var data = Object.assign({}, reportData.root, reportData[pdfCode]);
        XSLT.data2Xml({ root: data }, (xml) => {
          var xsl = XSLT.prepareReportXSL(reportTemplates[pdfCode], lang, 
                startPages[pdfCode], pageNum - 1);
          
          XSLT.xsltTransform(xml, xsl, (xslt) => {
            resolve(xslt);
          });
        });
      }));
    }
  });

  Promise.all(promises).then((result) => {
    let styles = [];
    _.each(reportTemplates, (reportTemplate) => {
      if (reportTemplate.style) {
        styles.push(reportTemplate.style);
      }
    });
    let contentHtml = _.join(result, '');
    let html = XSLT.prepareHtml(contentHtml, _.join(styles, '\n'));
    return XSLT.html2Pdf(html).then(callback);
  }).catch((err) => {
    logger.error(err);
    callback(false);
  });
}


var _getDynamicPdf = function(pdfCodes, reportData = {root: {}}, reportTemplates, callback){
  let reportHtml = "", reportStyle = "", pdfOptions = {
    header: {
      first: "",
      default: "",
    },
    footer: {
      first: "",
      default: "",
    }
  };

  var _handleHeaderFooter = function(options, template, type, lang = "en"){
    pdfOptions.header[type] = _.get(template, `header.${lang}`);
    pdfOptions.footer[type] = _.get(template, `footer.${lang}`);
    pdfOptions.footer[type] = pdfOptions.footer[type]
                .replace("[[@PAGE_NO]]", "<span class=\"page_no\"/>")
                .replace("[[@TOTAL_PAGE]]", "<span class=\"total_page_no\"/>");
  }

  let data = Object.assign({}, reportData.root);
  
  _.forEach(pdfCodes, (pdfCode, pdfIdx)=>{
    let reportTemplate = reportTemplates[pdfCode];
    data = Object.assign({}, data, reportData[pdfCode]);
    reportHtml += XSLT.prepareDynamicReportHtml(reportTemplate.template, "en");
    reportStyle += reportTemplate.style;
    if(pdfIdx === 0 ){
      _handleHeaderFooter(pdfOptions, reportTemplate, "first", "en");;
      _handleHeaderFooter(pdfOptions, reportTemplate, "default", "en");;
    }
    if(pdfIdx === 1){
      _handleHeaderFooter(pdfOptions, reportTemplate, "default", "en");;
    }
  })
  
  XSLT.data2Xml({root: data}, (xml) => {
    var xsl = XSLT.prepareDynamicReportXsl(reportHtml, pdfOptions);

    var startTime = new Date();
    XSLT.xsltTransform(xml, xsl, (xslt) => {
      var elapsedTimeMs = new Date() - startTime;
      logger.log("EXEC: xsltTransform: Elapsed " + elapsedTimeMs + "ms");
      let html = XSLT.prepareDynamicHtml(xslt, pdfOptions, reportStyle);
      commonFunction.convertHtml2Pdf(html, pdfOptions, callback);
    });
  });
}


module.exports.getReportPdf = function (pdfCodes, reportData, reportTemplates, isDynamic, callback) {
  if(!isDynamic){
    _getStaticPdf(pdfCodes, reportData, reportTemplates, callback);
  }
  else {
    _getDynamicPdf(pdfCodes, reportData, reportTemplates, callback);
  }
};


module.exports.getFnaReportPdf = function (reportData, reportTemplates, callback) {
  var data = Object.assign({}, reportData.root);
  new Promise((resolve) => {
    XSLT.data2Xml({ root: data }, (xml) => {
        var xsl = XSLT.prepareFnaReportXsl(reportTemplates, 'en', 1, 1);
        XSLT.xsltTransform(xml, xsl, (xslt) => {
          resolve(xslt);
      });
    })
  }).then((result) => {
    let contentHtml = _.join(result, '').split('[[@newLine]]').join('<br/>');
    let style = "";
    let params = {
      header: {
        height: "40px",
        contents: {}
      },
      footer: {
        height: "40px",
        contents: {}
      }
    };

    _.forEach(reportTemplates, (t,index)=>{
      if(index===0){
        var hf = _.at(t, ['header.en','footer.en']);
        if(hf[0]){
          params.header.contents.first = `<div class=\"pageMargin\">${hf[0]}</div>`;
        }
        if(hf[1]){
          params.footer.contents.first = `<div class=\"pageMargin\">${hf[1]}</div>`;
        }
      }
      else if(index===1){
        var hf = _.at(t, ['header.en','footer.en']);
        if(hf[0]){
          params.header.contents.default = `<div class=\"pageMargin\">${hf[0]}</div>`;
        }
        if(hf[1]){
          params.footer.contents.default = `<div class=\"pageMargin\">${hf[1]}</div>`;
        }
      }
      style += _.at(t, 'style')[0] || "";
    })

    let html = XSLT.prepareHtml(contentHtml, style);

    XSLT.html2Pdf(html, params)
      .then(callback)
      .catch(err=>{
        logger.error(err);
      });
  }).catch((err) => {
    callback(false);
  })

}


module.exports.getSupervisorTemplatePdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    if (reportData) {
      reportData.policyId = (!_.isEmpty(reportData.proposalNumber)) ? reportData.proposalNumber : reportData.policyId;
      reportData.supervisorApproveRejectDate = (reportData.supervisorApproveRejectDate) ?
        moment(reportData.supervisorApproveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
      reportData.approveRejectDate = (reportData.approveRejectDate) ?
        moment(reportData.approveRejectDate).utcOffset(ConfigConstant.MOMENT_TIME_ZONE).format(ConfigConstant.DateTimeFormat3) : '';
    }

    XSLT.data2Xml({ root: reportData }, (xml) => {
      var xsl = XSLT.prepareSupervisorXSL(reportTemplates.template[lang], lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    });
  }).then(function(result) {
    let style = '';
    if (reportTemplates.style) {
      style += reportTemplates.style;
    }
    let html = XSLT.prepareHtml(result, style);
    let params = {
      header: {
        height: '40px',
        contents: {}
      },
      footer: {
        height: '40px',
        contents: {}
      }
    };

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    logger.error(err);
    callback(false);
  });
};


module.exports.getAppFormPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    });
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      if (index == 0) {
        params.header.contents.first = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>'
        params.footer.contents.first = '<div class=\"appFormFooter\">' + template.footer[lang]  + '</div>'
      } else {
        params.header.contents.default = '<div class=\"appFormFooter\">' + template.header[lang] + '</div>'
        params.footer.contents.default = '<div class=\"appFormFooter\">' + template.footer[lang]  + '</div>'
      }
    });

    let html = XSLT.prepareHtml(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}


module.exports.getPremiumPaymentPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    })
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      // params.header.contents.default = '<div class=\"pageMargin\">' + template.header[lang] + '</div>';
      // params.footer.contents.default = '<div class=\"pageMargin\">' + template.footer[lang]  + '</div>';
    });

    let html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}


module.exports.getECpdPdf = function (reportData, reportTemplates, lang, callback) {
  new Promise((resolve) => {
    XSLT.data2Xml(reportData, (xml) => {
      var xsl = XSLT.prepareAppFormXSL(reportTemplates, lang, 1, 1);
      XSLT.xsltTransform(xml, xsl, (xslt) => {
        resolve(xslt);
      });
    })
  }).then(function(result) {
    let params = {
      header: {
        height: '40px',
        contents: {
        }
      },
      footer: {
        height: '90px',
        contents: {
        }
      }
    };

    let style = '';
    reportTemplates.forEach(function(template, index) {
      if (template.style) {
        style += template.style;
      }
      params.header.contents.default = '<div class=\"pageFooter\">' + template.header[lang] + '</div>';
      params.footer.contents.default = '<div class=\"pageFooter\">' + template.footer[lang]  + '</div>';
    });

    let html = XSLT.prepareHtmlOnePage(result, style);

    return XSLT.html2Pdf(html, params).then(callback);
  }).catch((err) => {
    callback(false);
  })
}