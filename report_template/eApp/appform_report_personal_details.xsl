<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="personalDetailsRowTmpl">
  <xsl:param name="titleLeft" />
  <xsl:param name="valueLeft" />
  <xsl:param name="titleRight" />
  <xsl:param name="valueRight" />
  <xsl:param name="isEmptyTitleLeft" />
  <xsl:param name="isEmptyTitleRight" />
  <xsl:param name="isEmptyValueLeft" />
  <xsl:param name="isEmptyValueRight" />

  <td width="23%" class="tdFirst padding">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$isEmptyTitleLeft = 'true'"></xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$titleLeft"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="27%" class="tdLast padding">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($valueLeft) > 0">
            <xsl:value-of select="$valueLeft"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="not($isEmptyValueLeft = 'true')">-</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="23%" class="tdFirst padding">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$isEmptyTitleRight = 'true'"></xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$titleRight"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="27%" class="tdLast padding">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($valueRight) > 0">
            <xsl:value-of select="$valueRight"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="not($isEmptyValueRight = 'true')">-</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>

</xsl:template>

<xsl:template name="appform_pdf_personalInfo">
  <div>
    <p class="sectionGroup">
      <span class="sectionGroup">PERSONAL DETAILS</span>
    </p>

    <xsl:for-each select="/root/insured">
      <table class="dataGroup">
        <thead>
          <tr>
            <td class="personHeaderLeft" colspan="2">
              <p class="personHeader">
                <span lang="EN-HK" class="personHeader">Life Assured</span>
              </p>
            </td>
            <td class="personHeaderRight" colspan="2">
              <p class="personHeader">
                <span lang="EN-HK" class="personHeader">Proposer</span>
              </p>
            </td>
          </tr>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Personal Particulars'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Name'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/fullName"/>
              <xsl:with-param name="titleRight" select="'Name'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/fullName"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Surname (as shown in ID)'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/lastName"/>
              <xsl:with-param name="titleRight" select="'Surname (as shown in ID)'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/lastName"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Given Name (as shown in ID)'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/firstName"/>
              <xsl:with-param name="titleRight" select="'Given Name (as shown in ID)'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/firstName"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'English or other name'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/othName"/>
              <xsl:with-param name="titleRight" select="'English or other name'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/othName"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Han Yu Pin Yin Name'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/hanyuPinyinName"/>
              <xsl:with-param name="titleRight" select="'Han Yu Pin Yin Name'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/hanyuPinyinName"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'ID Document Type'"/>
              <xsl:with-param name="valueLeft">
                <xsl:choose>
                  <xsl:when test="personalInfo/idDocType = 'OTHERS'">
                    <xsl:value-of select="personalInfo/idDocTypeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="personalInfo/idDocType"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
              <xsl:with-param name="titleRight" select="'ID Document Type'"/>
              <xsl:with-param name="valueRight">
                <xsl:choose>
                  <xsl:when test="/root/proposer/personalInfo/idDocType = 'OTHERS'">
                    <xsl:value-of select="/root/proposer/personalInfo/idDocTypeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/personalInfo/idDocType"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'ID Number'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/idCardNo"/>
              <xsl:with-param name="titleRight" select="'ID Number'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/idCardNo"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Date of Birth (dd/mm/yyyy)'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/dob"/>
              <xsl:with-param name="titleRight" select="'Date of Birth (dd/mm/yyyy)'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/dob"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Gender'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/gender"/>
              <xsl:with-param name="titleRight" select="'Gender'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/gender"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Marital Status'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/marital"/>
              <xsl:with-param name="titleRight" select="'Marital Status'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/marital"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Nationality'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/nationality"/>
              <xsl:with-param name="titleRight" select="'Nationality'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/nationality"/>
            </xsl:call-template>
          </tr>

          <xsl:variable name="valueRelationship">
            <xsl:choose>
              <xsl:when test="personalInfo/relationship = 'OTHER'">
                <xsl:value-of select="personalInfo/relationshipOther"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="personalInfo/relationship"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:variable name="relationshipPosition">
            <xsl:choose>
              <xsl:when test="string-length(personalInfo/prStatus) = 0 and string-length(/root/proposer/personalInfo/prStatus) > 0">
                <xsl:value-of select="'1'"/>
              </xsl:when>
              <xsl:when test="string-length(personalInfo/prStatus) > 0 and string-length(personalInfo/pass) = 0 and string-length(/root/proposer/personalInfo/pass) > 0">
                <xsl:value-of select="'2'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'3'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:if test="string-length(personalInfo/prStatus) > 0 or string-length(/root/proposer/personalInfo/prStatus) > 0">
            <tr class="dataGroup">
              <xsl:call-template name="personalDetailsRowTmpl">
                <xsl:with-param name="titleLeft">
                  <xsl:choose>
                    <xsl:when test="$relationshipPosition = '1'">
                      <xsl:value-of select="'Relationship to Proposer'"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'Singapore PR Status'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:with-param>

                <xsl:with-param name="valueLeft">
                  <xsl:choose>
                    <xsl:when test="$relationshipPosition = '1'">
                      <xsl:value-of select="$valueRelationship"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="personalInfo/prStatus"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:with-param>

                <xsl:with-param name="titleRight" select="'Singapore PR Status'"/>
                <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/prStatus"/>
                <xsl:with-param name="isEmptyTitleRight" select="count(/root/proposer/personalInfo/prStatus) = 0"/>
                <xsl:with-param name="isEmptyValueRight" select="count(/root/proposer/personalInfo/prStatus) = 0"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/proposer/personalInfo/pass or personalInfo/pass">
            <tr class="dataGroup">
              <xsl:call-template name="personalDetailsRowTmpl">
                <xsl:with-param name="titleLeft">
                  <xsl:choose>
                    <xsl:when test="$relationshipPosition = '2'">
                      <xsl:value-of select="'Relationship to Proposer'"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'Type of Pass'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="valueLeft">
                  <xsl:choose>
                    <xsl:when test="$relationshipPosition = '2'">
                      <xsl:value-of select="$valueRelationship"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="personalInfo/pass = 'OTHER'">
                          <xsl:value-of select="personalInfo/passOther"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="personalInfo/pass"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="isEmptyTitleLeft" select="not($relationshipPosition = '2') and count(personalInfo/pass) = 0"/>
                <xsl:with-param name="isEmptyValueLeft" select="not($relationshipPosition = '2') and count(personalInfo/pass) = 0"/>
                <xsl:with-param name="titleRight" select="'Type of Pass'"/>
                <xsl:with-param name="valueRight">
                  <xsl:choose>
                    <xsl:when test="/root/proposer/personalInfo/pass = 'OTHER'">
                      <xsl:value-of select="/root/proposer/personalInfo/passOther"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="/root/proposer/personalInfo/pass"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="isEmptyTitleRight" select="count(/root/proposer/personalInfo/pass) = 0"/>
                <xsl:with-param name="isEmptyValueRight" select="count(/root/proposer/personalInfo/pass) = 0"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="personalDetailsRowTmpl">
                <xsl:with-param name="titleLeft" select="'Pass Expiry Date (dd/mm/yyyy)'"/>
                <xsl:with-param name="valueLeft" select="personalInfo/passExpDate"/>
                <xsl:with-param name="isEmptyTitleLeft" select="count(personalInfo/passExpDate) = 0"/>
                <xsl:with-param name="isEmptyValueLeft" select="count(personalInfo/passExpDate) = 0"/>
                <xsl:with-param name="titleRight" select="'Pass Expiry Date (dd/mm/yyyy)'"/>
                <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/passExpDate"/>
                <xsl:with-param name="isEmptyTitleRight" select="count(/root/proposer/personalInfo/passExpDate) = 0"/>
                <xsl:with-param name="isEmptyValueRight" select="count(/root/proposer/personalInfo/passExpDate) = 0"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="$relationshipPosition = '3'">
            <tr class="dataGroup">
              <xsl:call-template name="personalDetailsRowTmpl">
                <xsl:with-param name="titleLeft" select="'Relationship to Proposer'"/>
                <xsl:with-param name="valueLeft" select="$valueRelationship"/>
                <xsl:with-param name="titleRight" select="''"/>
                <xsl:with-param name="valueRight" select=""/>
                <xsl:with-param name="isEmptyValueRight" select="'true'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
        </tbody>
      </table>
      <table class="dataGroup">
        <thead>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Professional Details'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Name of Employer /Business/ School'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/organization"/>
              <xsl:with-param name="titleRight" select="'Name of Employer /Business/ School'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/organization"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Industry'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/industry"/>
              <xsl:with-param name="titleRight" select="'Industry'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/industry"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Occupation'"/>
              <xsl:with-param name="valueLeft">
                <xsl:choose>
                  <xsl:when test="/root/originalData/insured/personalInfo/occupation = 'O921'">
                    <xsl:value-of select="personalInfo/occupationOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="personalInfo/occupation"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
              <xsl:with-param name="titleRight" select="'Occupation'"/>
              <xsl:with-param name="valueRight">
                <xsl:choose>
                  <xsl:when test="/root/originalData/proposer/personalInfo/occupation = 'O921'">
                    <xsl:value-of select="/root/proposer/personalInfo/occupationOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/personalInfo/occupation"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Country of Employer/ Business/ School'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/organizationCountry"/>
              <xsl:with-param name="titleRight" select="'Country of Employer/ Business/ School'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/organizationCountry"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Monthly Income/Allowance SGD'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/allowance"/>
              <xsl:with-param name="titleRight" select="'Monthly Income/Allowance SGD'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/allowance"/>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>
      <table class="dataGroupLast">
        <thead>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Contact Details'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Residential Address</span>
              </p>
            </td>
            <td width="27%" class="tdLast padding">
              <xsl:call-template name="personalInfoAddressTmpl">
                <xsl:with-param name="addrBlock" select="personalInfo/addrBlock"/>
                <xsl:with-param name="addrStreet" select="personalInfo/addrStreet"/>
                <xsl:with-param name="addrUnitNum" select="personalInfo/unitNum"/>
                <xsl:with-param name="addrEstate" select="personalInfo/addrEstate"/>
                <xsl:with-param name="addrCity" select="personalInfo/addrCity"/>
                <xsl:with-param name="addrCountry" select="personalInfo/addrCountry"/>
                <xsl:with-param name="addrPostalCode" select="personalInfo/postalCode"/>
              </xsl:call-template>
            </td>
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Residential Address</span>
              </p>
            </td>
            <td width="27%" class="tdLast padding">
              <xsl:call-template name="personalInfoAddressTmpl">
                <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/addrBlock"/>
                <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/addrStreet"/>
                <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/unitNum"/>
                <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/addrEstate"/>
                <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/addrCity"/>
                <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/addrCountry"/>
                <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/postalCode"/>
              </xsl:call-template>
            </td>
          </tr>
          <tr class="dataGroup">
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic"></span>
              </p>
            </td>

            <td width="27%" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer"></span>
              </p>
            </td>
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Mailing Address</span>
              </p>
            </td>
            <td width="27%" class="tdLast padding">
              <xsl:choose>
                <xsl:when test="/root/proposer/personalInfo/isSameAddr = 'No'">
                  <xsl:call-template name="personalInfoAddressTmpl">
                    <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/mAddrBlock"/>
                    <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/mAddrStreet"/>
                    <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/mAddrnitNum"/>
                    <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/mAddrEstate"/>
                    <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/mAddrCity"/>
                    <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/mAddrCountry"/>
                    <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/mPostalCode"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="personalInfoAddressTmpl">
                    <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/addrBlock"/>
                    <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/addrStreet"/>
                    <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/unitNum"/>
                    <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/addrEstate"/>
                    <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/addrCity"/>
                    <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/addrCountry"/>
                    <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/postalCode"/>
                  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Mobile'"/>
              <xsl:with-param name="valueLeft">
                <xsl:choose>
                  <xsl:when test="string-length(personalInfo/mobileNo) > 0">
                    <xsl:value-of select="personalInfo/mobileCountryCode"/> - <xsl:value-of select="personalInfo/mobileNo"/>
                  </xsl:when>
                  <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
              <xsl:with-param name="titleRight" select="'Mobile'"/>
              <xsl:with-param name="valueRight">
                <xsl:choose>
                  <xsl:when test="string-length(/root/proposer/personalInfo/mobileNo) > 0">
                    <xsl:value-of select="/root/proposer/personalInfo/mobileCountryCode"/> - <xsl:value-of select="/root/proposer/personalInfo/mobileNo"/>
                  </xsl:when>
                  <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Email'"/>
              <xsl:with-param name="valueLeft" select="personalInfo/email"/>
              <xsl:with-param name="titleRight" select="'Email'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/email"/>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>
    </xsl:for-each>

    <xsl:if test="$lifeAssuredIsProposer = 'true'">
      <table class="dataGroup">
        <thead>
          <tr>
            <td colspan="4" class="personHeader">
              <p class="personHeader">
                <span lang="EN-HK" class="personHeader bold">Life Assured</span>
                <span lang="EN-HK" class="personHeader"> (Life Assured is also the Proposer)</span>
              </p>
            </td>
          </tr>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Personal Particulars'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Name'"/>
              <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/fullName"/>
              <xsl:with-param name="titleRight" select="'Date of Birth (dd/mm/yyyy)'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/dob"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Surname (as shown in ID)'"/>
              <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/lastName"/>
              <xsl:with-param name="titleRight" select="'Gender'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/gender"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Given Name (as shown in ID)'"/>
              <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/firstName"/>
              <xsl:with-param name="titleRight" select="'Marital Status'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/marital"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'English or other name'"/>
              <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/othName"/>
              <xsl:with-param name="titleRight" select="'Nationality'"/>
              <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/nationality"/>
            </xsl:call-template>
          </tr>

          <xsl:variable name="hasTypeOfPass" select="count(/root/proposer/personalInfo/pass) = 0"/>
          <xsl:variable name="hasPassExpiryDate" select="count(/root/proposer/personalInfo/passExpDate) = 0"/>

          <xsl:choose>
            <xsl:when test="count(/root/proposer/personalInfo/prStatus) = 0">
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'Han Yu Pin Yin Name'"/>
                  <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/hanyuPinyinName"/>
                  <xsl:with-param name="titleRight" select="'Type of Pass'"/>
                  <xsl:with-param name="valueRight">
                    <xsl:choose>
                      <xsl:when test="/root/proposer/personalInfo/pass = 'OTHER'">
                        <xsl:value-of select="/root/proposer/personalInfo/passOther"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="/root/proposer/personalInfo/pass"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="isEmptyTitleRight" select="$hasTypeOfPass"/>
                  <xsl:with-param name="isEmptyValueRight" select="$hasTypeOfPass"/>
                </xsl:call-template>
              </tr>
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'ID Document Type'"/>
                  <xsl:with-param name="valueLeft">
                    <xsl:choose>
                      <xsl:when test="/root/proposer/personalInfo/idDocType = 'OTHERS'">
                        <xsl:value-of select="/root/proposer/personalInfo/idDocTypeOther"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="/root/proposer/personalInfo/idDocType"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="titleRight" select="'Pass Expiry Date (dd/mm/yyyy)'"/>
                  <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/passExpDate"/>
                  <xsl:with-param name="isEmptyTitleRight" select="$hasPassExpiryDate"/>
                  <xsl:with-param name="isEmptyValueRight" select="$hasPassExpiryDate"/>
                </xsl:call-template>
              </tr>
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'ID Number'"/>
                  <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/idCardNo"/>
                  <xsl:with-param name="isEmptyTitleRight" select="'true'"/>
                  <xsl:with-param name="isEmptyValueRight" select="'true'"/>
                </xsl:call-template>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'Han Yu Pin Yin Name'"/>
                  <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/hanyuPinyinName"/>
                  <xsl:with-param name="titleRight" select="'Singapore PR Status'"/>
                  <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/prStatus"/>
                </xsl:call-template>
              </tr>
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'ID Document Type'"/>
                  <xsl:with-param name="valueLeft">
                    <xsl:choose>
                      <xsl:when test="/root/proposer/personalInfo/idDocType = 'OTHERS'">
                        <xsl:value-of select="/root/proposer/personalInfo/idDocTypeOther"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="/root/proposer/personalInfo/idDocType"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="titleRight" select="'Type of Pass'"/>
                  <xsl:with-param name="valueRight">
                    <xsl:choose>
                      <xsl:when test="/root/proposer/personalInfo/pass = 'OTHER'">
                        <xsl:value-of select="/root/proposer/personalInfo/passOther"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="/root/proposer/personalInfo/pass"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                  <xsl:with-param name="isEmptyTitleRight" select="$hasTypeOfPass"/>
                  <xsl:with-param name="isEmptyValueRight" select="$hasTypeOfPass"/>
                </xsl:call-template>
              </tr>
              <tr class="dataGroup">
                <xsl:call-template name="personalDetailsRowTmpl">
                  <xsl:with-param name="titleLeft" select="'ID Number'"/>
                  <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/idCardNo"/>
                  <xsl:with-param name="titleRight" select="'Pass Expiry Date (dd/mm/yyyy)'"/>
                  <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/passExpDate"/>
                  <xsl:with-param name="isEmptyTitleRight" select="$hasPassExpiryDate"/>
                  <xsl:with-param name="isEmptyValueRight" select="$hasPassExpiryDate"/>
                </xsl:call-template>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
        </tbody>
      </table>
      <table class="dataGroup">
        <thead>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Professional Details'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
        <tr class="dataGroup">
          <xsl:call-template name="personalDetailsRowTmpl">
            <xsl:with-param name="titleLeft" select="'Name of Employer /Business/ School'"/>
            <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/organization"/>
            <xsl:with-param name="titleRight" select="'Country of Employer/ Business/ School'"/>
            <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/organizationCountry"/>
          </xsl:call-template>
        </tr>
        <tr class="dataGroup">
          <xsl:call-template name="personalDetailsRowTmpl">
            <xsl:with-param name="titleLeft" select="'Industry'"/>
            <xsl:with-param name="valueLeft" select="/root/proposer/personalInfo/industry"/>
            <xsl:with-param name="titleRight" select="'Monthly Income/Allowance SGD'"/>
            <xsl:with-param name="valueRight" select="/root/proposer/personalInfo/allowance"/>
          </xsl:call-template>
        </tr>
        <tr class="dataGroup">
          <xsl:call-template name="personalDetailsRowTmpl">
            <xsl:with-param name="titleLeft" select="'Occupation'"/>
            <xsl:with-param name="valueLeft">
              <xsl:choose>
                <xsl:when test="/root/originalData/proposer/personalInfo/occupation = 'O921'">
                  <xsl:value-of select="/root/proposer/personalInfo/occupationOther"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="/root/proposer/personalInfo/occupation"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="titleRight" select="''"/>
            <xsl:with-param name="valueRight" select="''"/>
            <xsl:with-param name="isEmptyValueRight" select="'true'"/>
          </xsl:call-template>
        </tr>
        </tbody>
      </table>
      <table class="dataGroupLast">
        <thead>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Contact Details'"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Residential Address</span>
              </p>
            </td>
            <td width="27%" class="tdLast padding">
              <xsl:call-template name="personalInfoAddressTmpl">
                <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/addrBlock"/>
                <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/addrStreet"/>
                <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/unitNum"/>
                <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/addrEstate"/>
                <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/addrCity"/>
                <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/addrCountry"/>
                <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/postalCode"/>
              </xsl:call-template>
            </td>
            <td width="23%" class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Mailing Address</span>
              </p>
            </td>
            <td width="27%" class="tdLast padding">
              <xsl:choose>
                <xsl:when test="/root/proposer/personalInfo/isSameAddr = 'No'">
                  <xsl:call-template name="personalInfoAddressTmpl">
                    <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/mAddrBlock"/>
                    <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/mAddrStreet"/>
                    <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/mAddrnitNum"/>
                    <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/mAddrEstate"/>
                    <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/mAddrCity"/>
                    <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/mAddrCountry"/>
                    <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/mPostalCode"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="personalInfoAddressTmpl">
                    <xsl:with-param name="addrBlock" select="/root/proposer/personalInfo/addrBlock"/>
                    <xsl:with-param name="addrStreet" select="/root/proposer/personalInfo/addrStreet"/>
                    <xsl:with-param name="addrUnitNum" select="/root/proposer/personalInfo/unitNum"/>
                    <xsl:with-param name="addrEstate" select="/root/proposer/personalInfo/addrEstate"/>
                    <xsl:with-param name="addrCity" select="/root/proposer/personalInfo/addrCity"/>
                    <xsl:with-param name="addrCountry" select="/root/proposer/personalInfo/addrCountry"/>
                    <xsl:with-param name="addrPostalCode" select="/root/proposer/personalInfo/postalCode"/>
                  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="personalDetailsRowTmpl">
              <xsl:with-param name="titleLeft" select="'Mobile'"/>
              <xsl:with-param name="valueLeft">
                <xsl:choose>
                  <xsl:when test="string-length(/root/proposer/personalInfo/mobileNo) > 0">
                    <xsl:value-of select="/root/proposer/personalInfo/mobileCountryCode"/> - <xsl:value-of select="/root/proposer/personalInfo/mobileNo"/>
                  </xsl:when>
                  <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
              <xsl:with-param name="titleRight" select="'Email'"/>
              <xsl:with-param name="valueRight">
                <xsl:value-of select="/root/proposer/personalInfo/email"/>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>
    </xsl:if>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
