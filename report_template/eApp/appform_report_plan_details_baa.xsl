<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroup">
      <col width="12%"/>
      <col width="38%"/>
      <col width="25%"/>
      <col width="25%"/>
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <tr class="odd">
          <td class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic"><br/></span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Basic Plan / Rider</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="title"></p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td  class="tdFirst padding">
            <p class="title noPadding">
              <span lang="EN-HK" class="questionItalic">Basic Plan</span>
            </p>
          </td>
          <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                      <xsl:if test="./saPostfix and string-length(./saPostfix/en) > 0">
                        (<xsl:value-of select="./saPostfix/en"/>)
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                      <xsl:if test="./saPostfix and string-length(./saPostfix/zh-Hant) > 0">
                        (<xsl:value-of select="./saPostfix/zh-Hant"/>)
                      </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./sumInsured"/>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer"></p>
            </td>
          </xsl:for-each>
        </tr>
        <xsl:for-each select="/root/planDetails/planList[not(covCode = /root/baseProductCode)]">
          <tr class="dataGroup">
            <td class="tdFirst padding">
              <p class="title noPadding">
                <span lang="EN-HK" class="questionItalic">
                  <xsl:choose>
                    <xsl:when test="position() = 1">Rider</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                      <xsl:if test="./saPostfix and string-length(./saPostfix/en) > 0">
                        (<xsl:value-of select="./saPostfix/en"/>)
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                      <xsl:if test="./saPostfix and string-length(./saPostfix/zh-Hant) > 0">
                        (<xsl:value-of select="./saPostfix/zh-Hant"/>)
                      </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./saViewInd = 'Yes'">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./sumInsured"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer"></p>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>

    <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
      <table class="dataGroupLast">
        <thead>
          <tr>
            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
              <xsl:with-param name="colspan" select="8"/>
            </xsl:call-template>
          </tr>
        </thead>
        <tbody>
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Policy Currency'"/>
              <xsl:with-param name="value1" select="/root/planDetails/ccy"/>
              <xsl:with-param name="header2" select="'Occupation Class'"/>
              <xsl:with-param name="value2" select="./covClass"/>
            </xsl:call-template>
          </tr>
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Payment Mode'"/>
              <xsl:with-param name="value1" select="/root/planDetails/paymentMode"/>
              <xsl:with-param name="header2" select="'Total Premium Amount (Inclusive of GST)'"/>
              <xsl:with-param name="value2" select="/root/planDetails/totYearPrem"/>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>
    </xsl:for-each>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
