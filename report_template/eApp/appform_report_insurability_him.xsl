<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_insurability">
  <xsl:if test="count(/root/showQuestions/insurability/question) > 0">
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']">
        <p class="sectionGroup">
          <span class="sectionGroup">INSURABILITY INFORMATION</span>
        </p>
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Height and Weight'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW01'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Height (m)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW01'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Weight (kg)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW02'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Any weight change in the last 12 months? (kg)'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03a'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Weight change'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03a'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03b'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'How many kg?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03b'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HW03c'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Reason of weight change'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HW03c'"/>
                <xsl:with-param name="repalceOtherAnswerId" select="'HW03c1'"/>
                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 3) = 'INS']">
      <div class="section">
        <xsl:if test="count(/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']) = 0">
          <p class="sectionGroup">
            <span class="sectionGroup">INSURABILITY INFORMATION</span>
          </p>
        </xsl:if>
        <table class="dataGroup first">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Insurance History'"/>
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'1.
                Have you ever made an application or application for
                reinstatement of a life, disability, accident, medical or
                critical illness insurance which has been accepted with an
                extra premium or on special terms, postponed, declined,
                withdrawn or is still being considered?'"/>
              <xsl:with-param name="sectionGroupName" select="'insurability'"/>
              <xsl:with-param name="answerId" select="'INS01'"/>
            </xsl:call-template>
          </tr>

          <xsl:if test="/root/showQuestions/insurability/question = 'INS01_DATA'">
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <col width="72"/>
                  </xsl:if>
                  <col width="148"/>
                  <col width="110"/>
                  <col width="110"/>
                  <col width="95"/>
                  <col width="156"/>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Insurance Company</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Type of Coverage</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Condition of Special Terms</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Decision &amp; Detailed Reason(s) of special terms</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'INS01_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS01_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <xsl:if test="INS01b1 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Life
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b2 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Total Permanent Disability
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b3 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Critical Illness
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b4 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Accident
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS01b5 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                </span>
                              </p>
                            </xsl:if>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01d"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS01e"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS01_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <xsl:if test="INS01b1 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Life
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b2 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Total Permanent Disability
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b3 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Critical Illness
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b4 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Accident
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS01b5 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                            </span>
                          </p>
                        </xsl:if>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01d"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS01e"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup last">
          <tr class="even">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'2.
                  Are you presently receiving a disability benefit or incapable
                  for work or have you ever made or intend to make any claim
                  against any insurer for disability, accident, medical care,
                  hospitalisation, critical illness and/or other benefits?'"/>
              <xsl:with-param name="sectionGroupName" select="'insurability'"/>
              <xsl:with-param name="answerId" select="'INS02'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'INS02_DATA'">
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="118"/>
                      <col width="80"/>
                      <col width="112"/>
                      <col width="240"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="118"/>
                      <col width="80"/>
                      <col width="112"/>
                      <col width="156"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Insurance Company</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Type of the Claim</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Medical Condition</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Claim Details</span>
                      </p>
                    </td>
                  </tr>

                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'INS02_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS02_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <xsl:if test="INS02b1 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Total Permanent Disability
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b2 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Critical Illness
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b3 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Accident
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="INS02b4 = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="other = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                                </span>
                              </p>
                            </xsl:if>
                          </td>
                          <td class="tdAns" width="100">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns" width="100">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02d"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns" width="149">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="INS02e"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>

                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS02_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <xsl:if test="INS02b1 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Total Permanent Disability
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b2 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Critical Illness
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b3 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Accident
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="INS02b4 = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="other = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                            </span>
                          </p>
                        </xsl:if>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02d"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="INS02e"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </div>
    </xsl:if>

    <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 9) = 'LIFESTYLE']">

      <div class="section">
        <table class="dataGroup first">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Lifestyle and Habits'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                  Have you smoked or used any tobacco, nicotine or smokeless
                  tobacco products (e.g. cigarettes, cigar, e-cigarettes or
                  pipes) within the past 12 months?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE01'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01a'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'i) Type of product:'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01a'"/>
                  <xsl:with-param name="repalceOtherAnswerId" select="'LIFESTYLE01a_OTH'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01b'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'ii) Number of products smoked per day:'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01b'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01c'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'iii) Number of years smoked:'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE01c'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <!-- <tr>
              <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr> -->
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'2.
                    Do you consume alcohol? If yes, how much alcohol do you drink
                    per week on average?'"/>
                <xsl:with-param name="extraQuestion" select="'Note:
                    ONE standard unit of alcoholic drink equates to Beer 330ml/can,
                    Wine 125ml/glass, or Spirits 30ml/cup.'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE02'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02a_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Beer (330 ml/can)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02a_2'"/>
                  <xsl:with-param name="extraAnswer" select="' can(s)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02a_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02b_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Wine (125 ml/glass)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02b_2'"/>
                  <xsl:with-param name="extraAnswer" select="' glass(es)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02b_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE02c_2'">
              <tr>
                <xsl:call-template name="questionTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                  <xsl:with-param name="question" select="'Spirits (30 ml/cup)'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                  <xsl:with-param name="answerId" select="'LIFESTYLE02c_2'"/>
                  <xsl:with-param name="extraAnswer" select="' cup(s)'"/>
                  <xsl:with-param name="isAddExtraAnswerId" select="'LIFESTYLE02c_1'"/>
                  <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                  <xsl:with-param name="isSubQuestionWithTick" select="boolean(1)"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
            <!-- <xsl:if test="/root/showQuestions/insurability/question[. = 'LIFESTYLE02a_2' or . = 'LIFESTYLE02b_2' or . = 'LIFESTYLE02c_2']">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if> -->
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="/root/showQuestions/insurability/question = 'LIFESTYLE04'">
                <xsl:value-of select="'dataGroup mid'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'dataGroup last'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'3.
                    Have you ever used any habit forming drugs or narcotics or been treated for drug habits?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE03'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE03_DATA'">
              <tr>
                <td style="border: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>

                  <table class="data">
                    <xsl:choose>
                      <xsl:when test="$lifeAssuredIsProposer = 'true'">
                        <col width="140"/>
                        <col width="142"/>
                        <col width="142"/>
                        <col width="266"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <col width="84"/>
                        <col width="140"/>
                        <col width="142"/>
                        <col width="142"/>
                        <col width="182"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                        <td class="tdAns">
                          <p class="thAnsCenter">
                            <span lang="EN-HK" class="th">For</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Substances used</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Date Commenced (MM/YYYY)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Date Ceased (MM/YYYY)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Details</span>
                        </p>
                      </td>
                    </tr>

                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <xsl:for-each select="/root/insured">
                        <xsl:for-each select="insurability/LIFESTYLE03_DATA">
                          <tr class="odd">
                            <xsl:if test="position() = 1">
                              <td>
                                <xsl:attribute name="class">
                                  <xsl:choose>
                                    <xsl:when test="count(/root/proposer/insurability/LIFESTYLE03_DATA) > 0">
                                      <xsl:value-of select="'tdFor'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="'tdFor bottom'"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:attribute>
                                <xsl:attribute name="rowspan">
                                  <xsl:value-of select="last()"/>
                                </xsl:attribute>
                                <p>
                                  <span lang="EN-HK" class="person">Life Assured</span>
                                </p>
                              </td>
                            </xsl:if>
                            <td class="tdAns">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03a"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03b"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03c"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE03d"/>
                                </span>
                              </p>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:if>

                    <xsl:for-each select="/root/proposer/insurability/LIFESTYLE03_DATA">
                      <tr class="even">
                        <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                          <td class="tdFor bottom">
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p class="tdAns">
                              <span lang="EN-HK" class="person">Proposer</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td class="tdAns">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03a"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03b"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03c"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE03d"/>
                            </span>
                          </p>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup mid">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE04'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'4.
                    Do you have any intention of residing outside Singapore for more than 6 months?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE04'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE04_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="lifeStyleTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'LIFESTYLE04_DATA'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
      <div class="section">
        <table class="dataGroup last">
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE05'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'5.
                    Do you participate or intend to participate in any hazardous
                    activities related to your occupation or recreation such as
                    diving, mountaineering, skydiving, parachuting, hang gliding,
                    motor sports or aviation (excluding flying as a passenger on a
                    regular scheduled airline)?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE05'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE05_DATA'">
              <tr>
                <td style="border: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <table class="data">
                    <xsl:choose>
                      <xsl:when test="$lifeAssuredIsProposer = 'true'">
                        <col width="160"/>
                        <col width="140"/>
                        <col width="380"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <col width="84"/>
                        <col width="160"/>
                        <col width="140"/>
                        <col width="296"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <tr>
                      <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                        <td class="tdAns" width="84">
                          <p class="thAnsCenter">
                            <span lang="EN-HK" class="th">For</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Type of hazardous activities</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Frequency (per year)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">Details</span>
                        </p>
                      </td>
                    </tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <xsl:for-each select="/root/insured">
                        <xsl:for-each select="insurability/LIFESTYLE05_DATA">
                          <tr class="odd">
                            <xsl:if test="position() = 1">
                              <td>
                                <xsl:attribute name="class">
                                  <xsl:choose>
                                    <xsl:when test="count(/root/proposer/insurability/LIFESTYLE05_DATA) > 0">
                                      <xsl:value-of select="'tdFor'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="'tdFor bottom'"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:attribute>
                                <xsl:attribute name="rowspan">
                                  <xsl:value-of select="last()"/>
                                </xsl:attribute>
                                <p>
                                  <span lang="EN-HK" class="person">Life Assured</span>
                                </p>
                              </td>
                            </xsl:if>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:choose>
                                    <xsl:when test="LIFESTYLE05a = 'Other'">
                                      <xsl:value-of select="resultOther"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="LIFESTYLE05a"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:value-of select="LIFESTYLE05b"/>
                                </span>
                              </p>
                            </td>
                            <td class="tdAns">
                              <p class="tdAns">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:choose>
                                    <xsl:when test="string-length(LIFESTYLE05c) > 0">
                                      <xsl:value-of select="LIFESTYLE05c"/>
                                    </xsl:when>
                                    <xsl:otherwise>-</xsl:otherwise>
                                  </xsl:choose>
                                </span>
                              </p>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </xsl:for-each>
                    </xsl:if>
                    <xsl:for-each select="/root/proposer/insurability/LIFESTYLE05_DATA">
                      <tr class="even">
                        <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                          <td class="tdFor bottom">
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p class="tdAns">
                              <span lang="EN-HK" class="person">Proposer</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:choose>
                                <xsl:when test="LIFESTYLE05a = 'Other'">
                                  <xsl:value-of select="resultOther"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="LIFESTYLE05a"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="LIFESTYLE05b"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:choose>
                                <xsl:when test="string-length(LIFESTYLE05c) > 0">
                                  <xsl:value-of select="LIFESTYLE05c"/>
                                </xsl:when>
                                <xsl:otherwise>-</xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </p>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE06'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'6.
                    Do you travel outside Singapore for more than 3 months in a year (other than
                    for holidays or studies)?'"/>
                <xsl:with-param name="extraQuestion" select="'Please provide details of country/city, frequency and
                    duration of the trips?'"/>
                <xsl:with-param name="isHideableExtraQuestion" select="'true'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'LIFESTYLE06'"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE06_DATA'">
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <xsl:call-template name="lifeStyleTableTmpl">
                    <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                    <xsl:with-param name="answerId" select="'LIFESTYLE06_DATA'"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td class="tdOneCol">
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="$colspan"/>
                  </xsl:attribute>
                  <p class="tdAns">
                    <span lang="EN-HK" class="tdAns"><br/></span>
                  </p>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
        </table>
      </div>
    </xsl:if>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'FAMILY']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Family History'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'FAMILY01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                    Has your biological mother, father, or any sister or brother been
                    diagnosed prior to age 60 with any of the following?'"/>
                <xsl:with-param name="extraQuestion" select="concat(
                    'Cancer, heart disease, stroke, diabetes, Huntington', $apos, 's disease,
                    polycystic kidney disease, Multiple Sclerosis, Alzheimer', $apos, 's
                    or any other inherited conditions.')"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'FAMILY01'"/>
                <xsl:with-param name="isQuestionWithoutNumber" select="boolean(1)" />
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'FAMILY01_DATA'">
            <tr>
              <td width="749" valign="top"
                style="border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="410"/>
                      <col width="140"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="326"/>
                      <col width="140"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Relationship</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Medical Condition</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Age of Onset</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/FAMILY01_DATA">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/FAMILY01_DATA) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01b"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="FAMILY01c"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>

                  <xsl:for-each select="/root/proposer/insurability/FAMILY01_DATA">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01b"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="FAMILY01c"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'REG_DR']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Details of Regular Doctor'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/showQuestions/insurability/question = 'REG_DR01'">
            <tr>
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'Do you have a regular doctor?'"/>
                  <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'REG_DR01'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'REG_DR01_DATA'">
            <tr>
              <td width="714" valign="top"
                style="width: 535.25pt; border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="160"/>
                      <col width="125"/>
                      <col width="265"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="160"/>
                      <col width="125"/>
                      <col width="181"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name of Doctor</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Name &amp; Address of Clinic/ Hospital</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Date of last consultation</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Details of Consultations</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/*[local-name() = 'REG_DR01_DATA']">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'REG_DR01_DATA']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p class="tdAns">
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01b"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="REG_DR01c"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:choose>
                                  <xsl:when test="REG_DR01d = 'Other'">
                                    <xsl:value-of select="resultOther"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="REG_DR01d"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'REG_DR01_DATA']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01b"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="REG_DR01c"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:choose>
                              <xsl:when test="REG_DR01d = 'Other'">
                                <xsl:value-of select="resultOther"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="REG_DR01d"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td width="701" valign="top"
                style="width: 526.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </table>
      </xsl:if>
    </div>
    <div class="section">
      <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'HEALTH']">
        <table class="dataGroupLastNoAutoColor">
          <tr>
            <xsl:call-template name="infoSectionHeaderLaTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="sectionHeader" select="'Medical and Health Information'"/>
            </xsl:call-template>
          </tr>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_HIM01'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1. Have either of your natural parents or any siblings died or suffered from cancer, heart conditions like coronary artery disease, heart valve disease, stroke, cardiomyopathy, arrhythmia or sudden cardiac death before the age of 55 years?'"/>

                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_HIM01'"/>
                <xsl:with-param name="isQuestionWithoutNumber" select="boolean(1)" />
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_HIM01_DATA'">
            <tr>
              <td width="749" valign="top"
                style="border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="140"/>
                      <col width="410"/>
                      <col width="140"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="84"/>
                      <col width="140"/>
                      <col width="326"/>
                      <col width="140"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td class="tdAns">
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Relationship</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Medical Condition</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Age of Onset</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="insurability/HEALTH_HIM01_DATA">
                        <tr class="odd">
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/insurability/HEALTH_HIM01_DATA) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="HEALTH_HIM01a"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="HEALTH_HIM01b"/>
                              </span>
                            </p>
                          </td>
                          <td class="tdAns">
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="HEALTH_HIM01c"/>
                              </span>
                            </p>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>

                  <xsl:for-each select="/root/proposer/insurability/HEALTH_HIM01_DATA">
                    <tr class="odd">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="HEALTH_HIM01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="HEALTH_HIM01b"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="HEALTH_HIM01c"/>
                          </span>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 10) = 'HEALTH_HIM' and not(substring(., 1, 11) = 'HEALTH_HIM_')]">
            <tr class="odd">
              <td class="question">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="question">2. Have you ever had or been told to have or been treated or under investigations for:</span>
                </p>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_HIM02'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(a) Cancer, tumour, lump or growth?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_HIM02'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_HIM03'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(b) Diabetes, high blood pressure, heaviness or pain or discomfort in chest, palpitations, heart attack, stroke or any other disorders of the heart, chronic kidney disease, chronic lung disease, muscle weakness, joint pain, arthritis or gout, disorders of the bladder or genital organs, blood in urine, Hepatitis B, Hepatitis C, sexually transmitted diseases, HIV or AIDS?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_HIM03'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH_HIM04'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'(c) Raised blood sugar, cholesterol or triglycerides, abnormal findings in electrocardiogram, treadmill test, chest x-ray, echocardiogram or angiography or any other cardiac investigation?'"/>
                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                <xsl:with-param name="answerId" select="'HEALTH_HIM04'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question[. = 'HEALTH_GROUP_DATA']">
            <tr>
              <td width="713" valign="top"
                style="width: 535.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <xsl:call-template name="healthGroupTableTmpl">
                  <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>

                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns"><br/></span>
                </p>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="/root/showQuestions/insurability/question = 'insAllNoInd' and (count(/root/proposer/insurability/insAllNoInd[. = 'Yes']) > 0 or count(/root/insured/insurability/insAllNoInd[. = 'Yes']) > 0)">
            <tr>
              <td style="border: solid windowtext 1.0pt; border-top: none">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table>
                  <tr>
                    <td
                      style="padding: 0in 5.4pt 0in 5.4pt; border: none">
                      <xsl:for-each select="/root/insured">
                        <xsl:if test="position() = 1 and insurability/insAllNoInd[. = 'Yes']">
                          <p class="question">
                            <span class="tdAns">
                              <xsl:copy-of select="$tick"/>
                              I confirm the answer to all of the above medical and health questions for
                              <b><u>life assured</u></b> is &quot;No&quot;
                            </span>
                          </p>
                        </xsl:if>
                      </xsl:for-each>
                      <xsl:if test="/root/proposer/insurability/insAllNoInd[. = 'Yes']">
                        <p class="question">
                          <span class="tdAns">
                            <xsl:copy-of select="$tick"/>
                            <xsl:choose>
                              <xsl:when test="$lifeAssuredIsProposer = 'true'">
                                I confirm the answer to all of the above medical and health questions for
                                <b><u>life assured</u></b> is &quot;No&quot;
                              </xsl:when>
                              <xsl:otherwise>
                                I confirm the answer to all of the above medical and health questions for
                                <b><u>proposer</u></b> is &quot;No&quot;
                              </xsl:otherwise>
                            </xsl:choose>
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="/root/showQuestions/insurability/question = 'LIFESTYLE01'">
                        <p class="question">
                          <span class="warningHealth">
                            Note: The above declaration doesn&#39;t apply to smoking question,
                            which is autopopulated from Client Profile
                          </span>
                        </p>
                      </xsl:if>
                    </td>
                  </tr>
                </table>
                <p class="tdAns"></p>
              </td>
            </tr>
          </xsl:if>

        </table>
      </xsl:if>
    </div>
    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
