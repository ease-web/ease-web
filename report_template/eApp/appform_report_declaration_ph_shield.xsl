<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



<xsl:template name="appform_shield_ph_declaration">
  <xsl:param  name="node"/>

  <!--Declaration; Bankruptcy -->  
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">DECLARATION</span>
    </p>
    
    <table class="dataGroupLast">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader" 
                            select="'Bankruptcy'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="'Proposer'"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'Are you an undischarged bankrupt?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/BANKRUPTCY01"/>
        </xsl:call-template>
      </tr>
    </table>

    <xsl:if test="/root/reportData/showQuestions/declaration/question[substring(., 1, 11) = 'TRUSTED_IND']"> 
      <div class="section">
        <xsl:call-template  name  ="comSecHeaderCols01">
          <xsl:with-param   name  ="sectionHeader" 
                            select="'Trusted Individual'"/>
        </xsl:call-template>

        <table class="dataGroupLastNoAutoColor">
          <col width="38"/>
          <col width="680"/>
          <tr>
            <td class="tdFirst padding">
              <xsl:call-template  name  ="comCheckbox">
                <xsl:with-param   name  ="isChecked" 
                                  select="'1'"/>
              </xsl:call-template>
            </td>

            <td class="tdLast padding">
              <span class="statement">
              I, <span class="statementBold"><xsl:value-of select="$node/declaration/trustedIndividuals/fullName"/></span> 
              of NRIC/Passport: <span class="statementBold"><xsl:value-of select="$node/declaration/trustedIndividuals/idCardNo"/></span> 
              acknowledge that I have fulfilled the definition of a Trusted 
              Individual and I am a Trusted Individual to 
              <span class="statementBold"><xsl:value-of select="$node/personalInfo/fullName"/></span>. 
              
              I confirm that the Financial Consultant has conversed in 
              <span class="statementBold">
                <xsl:choose>
                  <xsl:when test="$node/declaration/TRUSTED_IND01 = 'Other'">
                    <xsl:value-of select="$node/declaration/TRUSTED_IND02"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$node/declaration/TRUSTED_IND01"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span> 
              to conduct the Financial Needs Analysis and explained the 
              recommendations and application form(s). I confirm that I 
              understand the explanations and recommendations made by the
              Financial Consultant and I have translated and explained them to 
              <span class="statementBold"><xsl:value-of select="$node/personalInfo/fullName"/></span>.
              </span>
            </td>
          </tr>
        </table>
      </div>
    </xsl:if>

    <!-- Please uncomment it after confirmed in release 2 -->
    <!-- <div class="section">
      <xsl:call-template  name  ="comSecHeaderCols01">
        <xsl:with-param   name  ="sectionHeader" 
                          select="'e-Policy'"/>
      </xsl:call-template>

      <table class="dataGroupLastNoAutoColor">
        <col width="38"/>
        <col width="680"/>
        <tr>
          <xsl:variable   name="chkEPolicy">
            <xsl:choose>
              <xsl:when test="$node/declaration/EPOLICY01 = 'No'">
                <xsl:value-of select="'0'"/>
              </xsl:when>
                
              <xsl:otherwise>
                <xsl:value-of select="'1'"/>
              </xsl:otherwise>
            </xsl:choose>           
          </xsl:variable>

          <td class="tdFirst padding">
            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked" 
                                select="$chkEPolicy"/>
            </xsl:call-template>
          </td>

          <td class="tdLast padding">
            <span class = "normal">
            Go green! I would be happy to only receive my ePolicy document 
            on myAXA instead of receiving hardcopy of the Policy documents by mail.
            </span>
          </td>
        </tr>
      </table>
    </div> -->

  </div>

  <!--Declaration; Declaration to Central Provident Fund Board (CPFB) -->
  <div class="section">
    <xsl:call-template  name  ="comSecHeaderCols01">
      <xsl:with-param   name  ="sectionHeader"    
                        select="'Declaration to Central Provident Fund Board (CPFB)'"/>
    </xsl:call-template>

    <div class="sectionContent">
      <div class="oneCol">
        <p class="statement">
          <span lang="EN-HK" class="statement">           
            I authorize the Central Provident Fund Board (the “CPFB”) to
            <br/>
            <br/>

            (i) Deduct premium(s) due for the Life/Lives to be Assured as named 
            under this application (the “Life/Lives to be Assured”) from my 
            MediSave account (including any new MediSave account(s) which I may 
            have arising from obtaining Singapore Permanent Resident status or 
            otherwise) in accordance with the provisions of the Central Provident 
            Fund Act (Chapter 36), the MediShield Life Scheme Act 
            (Act No. 4 of 2015) and the respective subsidiary legislation made 
            thereunder and as amended from time to time and subject to all 
            terms and conditions as may be imposed by the CPFB from time to 
            time for the purposes of the Private Medical Insurance Scheme (or 
            by such other name as it may be referred to from time to time) (PMIS).
            <br/>
            <br/>

            (ii) Disclose/seek information on a confidential basis to/from any 
            insurer(s) for the PMIS in respect of the insurance cover issued 
            following this application. Such information includes but is not limited to:
            
            <br/><span style="margin-right:20px;"></span>(a) Payment and amount of 
            premiums due, including the deduction of premiums from my MediSave 
            account and my MediSave account balance; and
    
            <br/><span style="margin-right:20px;"></span>(b) the making of refunds under 
            the PMIS, as the CPFB shall reasonably consider appropriate; and
    
            <br/><span style="margin-right:20px;"></span>(c) The amount of premium 
            subsidies for the Life/Lives to be Assured and the amount of additional 
            premium applicable to the Life/Lives to be Assured.
            <br/>
            <br/>

            I and the Life/Lives to be Assured named under this application, 
            hereby consent to the transfer and disclosure, at any time and 
            without notice to me/us, of any medical information on me/us, in 
            the Insurer’s or the CPFB’s possession, between the Insurer and the 
            CPFB, for the purpose of assessing the insurability of me/us and/or 
            the making of a claim under the PMIS.
            <br/>
            <br/>

            Subject to the relevant laws and terms and conditions, I understand that:
            <br/>(i) Upon the commencement of this AXA Shield cover, any other 
            existing Integrated Shield Plan (if any) under the PMIS in favour 
            of the Life/Lives to be Assured shall automatically terminate; and
            <br/>(ii) Upon the commencement of another Integrated Shield Plan in 
            favour of the Life/Lives to be Assured, this AXA Shield cover of 
            the Life/Lives to be Assured shall automatically terminate.

          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol first">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          I declare that:
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          1. To the best of my knowledge and belief that the information given 
          by me to AXA Insurance Pte Ltd or its Medical Examiner is true and 
          complete and that no material facts such as facts likely to influence 
          the assessment and acceptance of this proposal have been withheld.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          2. I, the Life to be Assured, authorize any medical source, insurance 
          office or organization, to release to AXA Insurance Pte Ltd and AXA 
          Insurance Pte Ltd to release to any medical source, insurance office 
          or organization, any relevant information concerning me or ourselves, 
          at any time, irrespective of whether the proposal is accepted by AXA 
          Insurance Pte Ltd. A photocopy of this authorization shall be as valid 
          as the original.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          3a. I agree that payment of premium before acceptance of this proposal 
          by AXA Insurance Pte Ltd does not commit AXA Insurance Pte Ltd to issue 
          the policy I have applied for and the said policy shall not take effect 
          unless and until this proposal has been fully accepted and the full 
          initial premium has been paid during my life or our lives and good health.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          3b. I agree to inform AXA Insurance Pte Ltd if there is any change in 
          the state of health, occupation or activity of the Life Assured, 
          between the date of this proposal or medical examination and the issue
           of my policy. On receiving this information AXA Insurance Pte Ltd is 
           entitled to accept or reject my proposal. The last signature date on 
           the proposal form(s) shall be construed as the date of proposal.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          4. I confirm that I have been explained to me or our satisfaction (a) 
          My Financial Profile, (b) the Product Summary and (c) Your Guide to 
          Health Insurance. A copy of (a) to (c) have been received.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          5. I aware that I can seek advice from a qualified financial consultant 
          before I sign this proposal form. Should I choose not to, I take sole 
          responsibility to ensure that this product is appropriate to my 
          financial needs and insurance objectives.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          6. Should I decide not to take up the proposal under the standard terms 
          offered by AXA Insurance Pte Ltd or if the proposal is officially 
          accepted by AXA Insurance Pte Ltd and I decide to terminate the policy 
          within 21 days from the date of receipt of the Policy Contract, then 
          the amount refundable to me shall be determined by AXA Insurance Pte Ltd 
          after taking into account the premium(s) paid, less medical fees 
          incurred in underwriting the policy. However, should AXA Insurance 
          Pte Ltd decline the proposal, then I shall be entitled to a full 
          refund of the premium(s) paid.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          7. My financial consultant has advised me that all Singapore Citizens 
          and Permanent Residents will be covered by MediShield Life, regardless 
          of my decision on an Integrated Shield Plan. An Integrated Shield Plan 
          comprises two parts – a MediShield Life portion provided by the Central 
          Provident Fund Board (CPFB) and an additional private insurance coverage 
          portion provided by the Insurance Company. As Integrated Shield Plan 
          premiums are higher than MediShield Life premiums, there should be 
          sufficient monies in my Medisave account(s) or I/we should have enough 
          cash to pay for MediShield Life premiums on an ongoing basis before 
          I/we consider purchasing an Integrated Shield Plan.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          8a. The information I have provided is my personal data and, where it 
          is not my personal data, that I have the consent of the owner of such 
          personal data to provide such information.
          <br/>
        </span>
      </p>
    
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          8b. I am happy to receive customer service communication by e-mail 
          and/or SMS instead of hard copies by post.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          8c. By providing this information, I understand and give my consent 
          for AXA Insurance Pte Ltd and its representatives or agents to:
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          i. Collect, use, store, transfer and/or disclose the information, 
          to or with all such persons (including any member of the AXA Group or 
          any third party service provider, and whether within or outside of 
          Singapore) for the purpose of enabling AXA Insurance Pte Ltd to provide 
          me with services required of an insurance provider, including the 
          evaluating, processing, administering and/or managing of my relationship 
          and policy(ies) with AXA Insurance Pte Ltd, and for the purposes set 
          out in AXA Insurance Pte Ltd’s Data Use Statement which can be found at 
          http://www.axa.com.sg (“Purposes”).
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          ii. Collect, use, store, transfer and/or disclose personal data about 
          me and those whose personal data I have provided from sources other 
          than myself for the Purposes.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          iii. Contact me to share information about products and services offered 
          by AXA Insurance Pte Ltd that may be of interest to me by post and e-mail and
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <table style="width:100%; table-collapse:collapse;">
        <xsl:variable   name="chkContact1">
          <xsl:choose>
            <xsl:when test="$node/declaration/PDPA01a = 'Yes'">
              <xsl:value-of select="'1'"/>
            </xsl:when>
              
            <xsl:otherwise>
              <xsl:value-of select="'0'"/>
            </xsl:otherwise>
          </xsl:choose>           
        </xsl:variable>

        <td style="width:33%; text-align:center;">
            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked" 
                                select="$chkContact1"/>
            </xsl:call-template>
            <span class="normal">By Telephone</span>
        </td>

        <xsl:variable   name="chkContact2">
          <xsl:choose>
            <xsl:when test="$node/declaration/PDPA01b = 'Yes'">
              <xsl:value-of select="'1'"/>
            </xsl:when>
              
            <xsl:otherwise>
              <xsl:value-of select="'0'"/>
            </xsl:otherwise>
          </xsl:choose>           
        </xsl:variable>

        <td style="width:33%; text-align:center;">
            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked" 
                                select="$chkContact2"/>
            </xsl:call-template>
            <span class="normal">By Text Message</span>
        </td>

        <xsl:variable   name="chkContact3">
          <xsl:choose>
            <xsl:when test="$node/declaration/PDPA01c = 'Yes'">
              <xsl:value-of select="'1'"/>
            </xsl:when>
              
            <xsl:otherwise>
              <xsl:value-of select="'0'"/>
            </xsl:otherwise>
          </xsl:choose>           
        </xsl:variable>

        <td style="width:33%; text-align:center;">
            <xsl:call-template  name  ="comCheckbox">
              <xsl:with-param   name  ="isChecked" 
                                select="$chkContact3"/>
            </xsl:call-template>
            <span class="normal">By Fax</span>
        </td>
      </table>
    </div>
  </div>


  <div class="sectionContent">
    <div class="oneCol last">
      <p class="statement">
        <span lang="EN-HK" class="warningDeclaration">
          <br/>
          If a material fact is not disclosed in this proposal, any policy 
          issued may not be valid. If you are in doubt as to whether a fact is 
          material, you are advised to disclose it. <u>This includes any 
          information that you may have provided to the Financial Consultant 
          but was not included in the proposal.</u> Please check to ensure that you 
          are fully satisfied with the information declared in this proposal 
          before signing.
          <br/>
        </span>
      </p>
    </div>
  </div>


  <!--Financial Consultant’s Declaration -->
  <div class="section">
    <table class="dataGroup first last">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader" 
                            select="'Financial Consultant’s Declaration'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="'Proposer'"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'1. How long have you known your customer?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/FCD_01"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'2. Are you related to your customer?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/FCD_02"/>
        </xsl:call-template>
      </tr>

      <xsl:if test="$node/declaration/FCD_02 = 'Yes'">
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text" 
                              select="'3. Relationship to customer'"/>
            <xsl:with-param   name  ="col02Text" 
                              select="$node/declaration/FCD_03"/>
          </xsl:call-template>
        </tr>
      </xsl:if>



    </table>
  </div>

  <div class="sectionContent">
    <div class="oneCol last" style = "padding-left:25px;">
      <p class="normal">
        <br/>
        <ul class="statement">
          <li>I, Financial Consultant, certify that the proof of Identity/(ies) 
          uploaded and submitted is(are) true copy(ies) of the original(s).
          </li>
          <li class="red">I declare that all answers provided to me by the Proposer or Life 
          Assured are declared in the proposal. I have not withheld any other 
          information which may influence the acceptance of this proposal by the 
          Company.
          </li>
          <li>I have not given any statement to the Proposer or Life Assured 
          which is contrary to the provision given in the Company’s standard policy.
          </li>
          <li class="red">I confirm that the Proposer or Life Assured have fully understood 
          all questions and answers in the Proposal form. I have checked and 
          verified the personal particulars given against their original NRIC or 
          Passport or Birth certificate and confirm them to be true and correct.
          </li>
          <li>I have personally seen the Proposer or Life Assured and have 
          explained the terms of the insurance to him / her.
          </li>
        </ul>
      </p>
    </div>
  </div>

</xsl:template>


<xsl:template name="appform_shield_ph_signature">
  <xsl:param  name="node"/>

  <div class="section">
    <p><br/></p>
    <xsl:choose>
      <xsl:when test = "$node/extra/hasTrustedIndividual = 'Y'">
        <table  style= "width:740px; border-collapse: collapse;">
          <colgroup>
            <col style = "width:33.3%"></col>
            <col style = "width:33.3%"></col>
            <col style = "width:33.3%"></col>
          </colgroup>

          <tr>
            <td colspan = "3">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signed and dated in Singapore</span>
              </p>
            </td>
          </tr>

          <tr>
            <td style = "padding-right:10px;">
                <div class="signatureCell">
                    <div>
                        <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
                    </div>
                </div>
            </td>
            
            <td style = "padding:0 10px;">
                <div class="signatureCell">
                    <div>
                        <span lang="EN-HK" class="signatureCell">TI_SIGNATURE</span>
                    </div>
                </div>
            </td>
            
            <td style = "padding-left:10px;">
                <div class="signatureCell">
                    <div>
                        <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
                    </div>
                </div>
            </td>
          </tr>

          <tr>
            <td style = "text-align:center;">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant<br/>(Witness)</span>
              </p>
            </td>

            <td style = "text-align:center;">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signature of Trusted Individual</span>
              </p>
            </td>

            <td style = "text-align:center;">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signature of Proposer<br/> 
                  <xsl:value-of select="$node/personalInfo/fullName"/>
                </span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:when>

      <xsl:otherwise>
        <table style= "width:740px; border-collapse: collapse;">
          <colgroup>
            <col style = "width:50%"></col>
            <col style = "width:50%"></col>
          </colgroup>

          <tr>
            <td colspan = "2">
              <p class="normal">
                <span lang="EN-HK" class="signature"><br/><br/><br/>Signed and dated in Singapore</span>
              </p>
            </td>
          </tr>

          <tr style = "width:100%">
            <td style = "padding-right:10px;">
                <div class="signatureCell">
                    <div>
                        <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
                    </div>
                </div>
            </td>
            
            <td style = "padding-left:10px;">
                <div class="signatureCell">
                    <div>
                        <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
                    </div>
                </div>
            </td>
          </tr>

          <tr style = "width:100%">
            <td style = "text-align:center;">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>


            <td style = "text-align:center;">
              <p class="normal">
                <span lang="EN-HK" class="signature">Signature of Proposer<br/> 
                  <xsl:value-of select="$node/personalInfo/fullName"/>
                </span>
              </p>
            </td>
          </tr>

        </table>
      </xsl:otherwise>
    </xsl:choose>
  </div>

</xsl:template>



<xsl:template name="appform_report_declaration_ph">
  <xsl:call-template    name  ="appform_shield_ph_declaration">
    <xsl:with-param     name  ="node" select="/root/reportData/proposer"/>
  </xsl:call-template>

  <xsl:call-template    name  ="appform_shield_ph_signature">
    <xsl:with-param     name  ="node" select="/root/reportData/proposer"/>
  </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
