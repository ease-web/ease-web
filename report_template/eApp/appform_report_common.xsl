<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="apos">'</xsl:variable>
<xsl:variable name="tick">
  <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABOklEQVRYR+2W7W3CMBCGn0xAN2iZgHaDMkFHgA0QE0AnKCN0k9IJ2m4AG7AB6JUSyVhHc8KX5E/8K7Lse5/78OUqBl7VwPqMALcisAPegKegFB2AT+A9t2cBSHwVJJybEcA23bQATsAEeAF+g0CegR9AkZi2AZzrA9EFatq1REaAiAg8AB911SvvWr2lQOJfgArvG3jtEyAV/6vF9ap6icB/4kUATT7XQONN3h7axIsA1EIXdVOaGxAe8SIACeyBmQHhFS8C0GULQvtNtecFZ3Xw4meYQ0hET80jXhyBxqMUQnte8TCANB36VpO59TLyNBSnIDWoSGh5xUMjYBWYZy80Ah7BTlPQKUCXI9kxH3StiUhD4+YeFx13XEOp7AhiCTw6jHqOyHP9U64mYl2MHjw9MFdnRoDBI3ABXmlgIXxZe+kAAAAASUVORK5CYII="/>
</xsl:variable>
<!-- &#254; -->
<xsl:variable name="untick">
  <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqUlEQVRYR+2WwQ2CUBAFhwosQexAKrEFO7AEtQNLoCMsQTuwA81LOMjPEoNZ8i+PI4Hd+QOHaah8NZX3Y4A5AzfgALRJn+gB9MC1nBcBaPkpaXE5RgCX75sRwAvYAB1wTwLZAwMgE7tfAO/xgewfNJwbLTGADdiADdiADdiADVQ3sGaSPcvQjYpI0XhOasG/olQvCeIIbJNAdHJl+aSINTs7PBfzGqC6gQ8cPDYhnSgMLQAAAABJRU5ErkJggg=="/>
</xsl:variable>
<!-- &#168;  -->

<xsl:variable name="lifeAssuredIsProposer" select="count(/root/insured) = 0"/>

<xsl:variable name="colspan">
  <xsl:choose>
    <xsl:when test="not($lifeAssuredIsProposer = 'true')">
      <xsl:value-of select="count(/root/insured) + 2" />
    </xsl:when>
    <xsl:otherwise>2</xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="hasTrustedIndividuals">
  <xsl:value-of select="count(/root/proposer/extra[hasTrustedIndividual = 'Y']) > 0"/>
</xsl:variable>

<xsl:template name="infoSectionHeaderLaTmpl">
  <xsl:param name="laIsProp" />
  <xsl:param name="sectionHeader" />

  <td class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
    </p>
  </td>
  <xsl:if test="not($laIsProp = 'true')">
    <xsl:for-each select="/root/insured">
      <td width="99" class="headerRow"
        style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
        <p class="header">
          <span lang="EN-HK" class="sectionHeader">Life Assured</span>
        </p>
      </td>
    </xsl:for-each>
  </xsl:if>
  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none; padding: 0in 5.4pt 0in 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:choose>
          <xsl:when test="not($laIsProp = 'true')">Proposer</xsl:when>
          <xsl:otherwise>Life Assured</xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="infoSectionHeaderLaTmplDiv">
  <xsl:param name="laIsProp" />
  <xsl:param name="sectionHeader" />

  <div class="sectionHeader">
    <div class="title">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:value-of select="$sectionHeader"/>
        </span>
      </p>
    </div>
    <xsl:if test="not($laIsProp = 'true')">
      <xsl:for-each select="/root/insured">
        <div class="person">
          <p class="header">
            <span lang="EN-HK" class="sectionHeader">Life Assured</span>
          </p>
        </div>
      </xsl:for-each>
    </xsl:if>
    <div class="person">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:choose>
            <xsl:when test="not($laIsProp = 'true')">Proposer</xsl:when>
            <xsl:otherwise>Life Assured</xsl:otherwise>
          </xsl:choose>
        </span>
      </p>
    </div>
  </div>
</xsl:template>

<xsl:template name="infoSectionHeaderPropTmpl">
  <xsl:param name="colspan" />
  <xsl:param name="sectionHeader" />

  <td class="tdFirst section">
    <xsl:attribute name="colspan">
      <xsl:value-of select="$colspan - 1"/>
    </xsl:attribute>
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
    </p>
  </td>
  <td width="99" class="tdLast section">
  </td>
</xsl:template>

<xsl:template name="infoSectionHeaderNoneTmpl">
  <xsl:param name="sectionHeader" />
  <xsl:param name="colspan" />

  <td width="714" class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$colspan">
      <xsl:attribute name="colspan">
        <xsl:value-of select="$colspan"/>
      </xsl:attribute>
    </xsl:if>
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="infoSectionHeaderNoneTmplDiv">
  <xsl:param name="sectionHeader" />

  <div class="sectionHeader">
    <div class="title">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:value-of select="$sectionHeader"/>
        </span>
      </p>
    </div>
  </div>
</xsl:template>

<xsl:template name="questionTmpl">
  <xsl:param name="laIsProp" />
  <xsl:param name="question" />
  <xsl:param name="isQuestionWithIndent" />
  <xsl:param name="isQuestionWithoutNumber" />
  <xsl:param name="extraQuestion" />
  <xsl:param name="isHideableExtraQuestion" />
  <xsl:param name="isItalicExtraQuestion" />
  <xsl:param name="sectionGroupName" />
  <xsl:param name="answerId" />
  <xsl:param name="extraAnswer" />
  <xsl:param name="isAddExtraAnswerId" />
  <xsl:param name="repalceOtherAnswerId" />
  <xsl:param name="isSubQuestion" />
  <xsl:param name="isSubQuestionWithTick" />

  <xsl:variable name="laAnswerYesCount">
    <xsl:choose>
      <xsl:when test="$sectionGroupName = 'residency'">
        <xsl:value-of select="count(/root/insured/residency/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'foreigner'">
        <xsl:value-of select="count(/root/insured/foreigner/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'policies'">
        <xsl:value-of select="count(/root/insured/policies/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'insurability'">
        <xsl:value-of select="count(/root/insured/insurability/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'declaration'">
        <xsl:value-of select="count(/root/insured/declaration/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="ppAnswerYesCount">
    <xsl:choose>
      <xsl:when test="$sectionGroupName = 'residency'">
        <xsl:value-of select="count(/root/proposer/residency/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'foreigner'">
        <xsl:value-of select="count(/root/proposer/foreigner/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'policies'">
        <xsl:value-of select="count(/root/proposer/policies/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'insurability'">
        <xsl:value-of select="count(/root/proposer/insurability/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:when test="$sectionGroupName = 'declaration'">
        <xsl:value-of select="count(/root/proposer/declaration/*[local-name()=$answerId and text() = 'Yes'])"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="showExtraQuestion">
    <xsl:choose>
      <xsl:when test="$isHideableExtraQuestion = 'true'">
        <xsl:value-of select="$laAnswerYesCount + $ppAnswerYesCount > 0"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'true'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <td class="tdFirst padding">
    <p>
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="$isQuestionWithoutNumber">
            <xsl:value-of select="'statement'"/>
          </xsl:when>
          <xsl:when test="$isQuestionWithIndent">
            <xsl:value-of select="'question indent'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'question'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="style">
        <xsl:choose>
          <xsl:when test="$isSubQuestion and not($isSubQuestionWithTick)">
            <xsl:value-of select="'text-align: right;'"/>
          </xsl:when>
          <xsl:when test="$isSubQuestion and $isSubQuestionWithTick">
            <xsl:choose>
              <xsl:when test="$laIsProp = 'true'">
                <xsl:value-of select="'text-align: left; padding-left: 480px'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'text-align: left; padding-left: 360px'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'text-align: justify;'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:if test="$isSubQuestionWithTick">
        <xsl:copy-of select="$tick"/>
      </xsl:if>
      <span lang="EN-HK" class="question">
        <xsl:value-of select="$question"/>
      </span>
    </p>
    <xsl:if test="$extraQuestion and $showExtraQuestion = 'true'">
      <p class="statement">
        <span lang="EN-HK" class="question">
          <xsl:attribute name="style">
            <xsl:if test="$isItalicExtraQuestion">
              <xsl:value-of select="'font-style: italic;'"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:value-of select="$extraQuestion"/>
        </span>
      </p>
    </xsl:if>
  </td>
  <xsl:if test="not($laIsProp = 'true')">
    <xsl:for-each select="/root/insured">
      <xsl:variable name="laAnswer">
        <xsl:choose>
          <xsl:when test="$sectionGroupName = 'residency'">
            <xsl:value-of select="residency/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'foreigner'">
            <xsl:value-of select="foreigner/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'policies'">
            <xsl:value-of select="policies/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'insurability'">
            <xsl:value-of select="insurability/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'declaration'">
            <xsl:value-of select="declaration/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="laOtherAnswer">
        <xsl:choose>
          <xsl:when test="$sectionGroupName = 'residency'">
            <xsl:value-of select="residency/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'foreigner'">
            <xsl:value-of select="foreigner/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'policies'">
            <xsl:value-of select="policies/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'insurability'">
            <xsl:value-of select="insurability/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'declaration'">
            <xsl:value-of select="declaration/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="laExtraAnswer">
        <xsl:value-of select="insurability/*[local-name()=$isAddExtraAnswerId]"/>
      </xsl:variable>

      <td width="99" class="tdMid">
        <p class="answer">
          <span lang="EN-HK" class="answer">
            <xsl:choose>
              <xsl:when test="string-length($laAnswer) > 0 and string-length($laOtherAnswer) = 0">
                <xsl:value-of select="$laAnswer"/>
              </xsl:when>
              <xsl:when test="string-length($laOtherAnswer) > 0">
                <xsl:value-of select="$laOtherAnswer"/>
              </xsl:when>
              <xsl:otherwise>
                -
              </xsl:otherwise>
            </xsl:choose>
          </span>
          <xsl:if test="$laExtraAnswer = 'Yes'">
            <span lang="EN-HK" class="answer"><xsl:value-of select="$extraAnswer"/></span>
          </xsl:if>
        </p>
      </td>
    </xsl:for-each>
  </xsl:if>
  <td width="99" class="tdLast padding">
    <p class="answer">
      <xsl:variable name="ppAnswer">
        <xsl:choose>
          <xsl:when test="$sectionGroupName = 'residency'">
            <xsl:value-of select="/root/proposer/residency/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'foreigner'">
            <xsl:value-of select="/root/proposer/foreigner/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'policies'">
            <xsl:value-of select="/root/proposer/policies/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'insurability'">
            <xsl:value-of select="/root/proposer/insurability/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'declaration'">
            <xsl:value-of select="/root/proposer/declaration/*[local-name()=$answerId]"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="ppOtherAnswer">
        <xsl:choose>
          <xsl:when test="$sectionGroupName = 'residency'">
            <xsl:value-of select="/root/proposer/residency/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'foreigner'">
            <xsl:value-of select="/root/proposer/foreigner/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'policies'">
            <xsl:value-of select="/root/proposer/policies/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'insurability'">
            <xsl:value-of select="/root/proposer/insurability/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:when test="$sectionGroupName = 'declaration'">
            <xsl:value-of select="/root/proposer/declaration/*[local-name()=$repalceOtherAnswerId]"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="ppExtraAnswer">
        <xsl:value-of select="/root/proposer/insurability/*[local-name()=$isAddExtraAnswerId]"/>
      </xsl:variable>

      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($ppAnswer) > 0 and string-length($ppOtherAnswer) = 0">
            <xsl:value-of select="$ppAnswer"/><xsl:value-of select="$ppOtherAnswer"/>
          </xsl:when>
          <xsl:when test="string-length($ppOtherAnswer) > 0">
            <xsl:value-of select="$ppOtherAnswer"/>
          </xsl:when>
          <xsl:otherwise>
            -
          </xsl:otherwise>
        </xsl:choose>
      </span>
      <xsl:if test="$ppExtraAnswer = 'Yes'">
        <span lang="EN-HK" class="answer"><xsl:value-of select="$extraAnswer"/></span>
      </xsl:if>
    </p>
  </td>
</xsl:template>

<xsl:template name="personalInfoAddressTmpl">
  <xsl:param name="addrBlock" />
  <xsl:param name="addrStreet" />
  <xsl:param name="addrUnitNum" />
  <xsl:param name="addrEstate" />
  <xsl:param name="addrCity" />
  <xsl:param name="addrCountry" />
  <xsl:param name="addrPostalCode" />

  <p class="answer">
    <xsl:if test="$addrBlock">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrBlock"/>
      </span>
    </xsl:if>
    <xsl:if test="$addrStreet">
      <span lang="EN-HK" class="answer">, </span>
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrStreet"/>
      </span>
    </xsl:if>
  </p>
  <xsl:if test="$addrUnitNum or $addrEstate or string-length($addrUnitNum) > 0 or string-length($addrEstate) > 0">
    <p class="answer">
      <xsl:if test="string-length($addrUnitNum) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrUnitNum"/>
        </span>
        <span lang="EN-HK" class="answer">, </span>
      </xsl:if>
      <xsl:if test="string-length($addrEstate) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrEstate"/>
        </span>
      </xsl:if>
    </p>
  </xsl:if>
  <p class="answer">
    <xsl:choose>
      <xsl:when test="$addrCity and string-length($addrCity) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrCity"/>
        </span>
        <span lang="EN-HK" class="answer">, </span>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$addrCountry">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrCountry"/>
      </span>
    </xsl:if>
    <xsl:if test="$addrPostalCode">
      <span lang="EN-HK" class="answer">, </span>
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrPostalCode"/>
      </span>
    </xsl:if>
  </p>
</xsl:template>

<xsl:template name="dataGroupTableColumnsDataWithCheckboxTmpl">
  <xsl:param name="header1" />
  <xsl:param name="value1" />
  <xsl:param name="checkboxText" />
  <xsl:param name="checkboxValue" />

  <td width="25%" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$value1 and not($value1 = '') and not($value1 = 'null')">
            <xsl:value-of select="$header1"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="25%" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$value1 and not($value1 = '') and not($value1 = 'null')">
            <xsl:value-of select="$value1"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="50%" colspan="4" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$checkboxValue = 'Yes'">
            <xsl:copy-of select="$tick"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$untick"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$checkboxText"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="dataGroupTableOneColumnCheckboxTmpl">
  <xsl:param name="title" />
  <xsl:param name="value" />

  <td width="100%" colspan="8" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$value = 'Yes'">
            <xsl:copy-of select="$tick"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$untick"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$title"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="dataGroupTableOneColumnDataTmpl">
  <xsl:param name="header" />
  <xsl:param name="headerIsSubHeader"/>
  <xsl:param name="value" />

  <td width="50%" colspan="4" class="tdMid">
    <p>
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="$headerIsSubHeader = 'true'">
            <xsl:value-of select="'title subHeader bold'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'title'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$value and not($value = '') and not($value = 'null')">
            <xsl:value-of select="$header"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="50%" colspan="4" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$value and not($value = '') and not($value = 'null')">
            <xsl:value-of select="$value"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="dataGroupTableTwoColumnsDataTmpl">
  <xsl:param name="header1" />
  <xsl:param name="value1" />
  <xsl:param name="value1Currency" />
  <xsl:param name="header2" />
  <xsl:param name="value2" />
  <xsl:param name="value2Currency" />

  <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$value1 and not($value1 = '') and not($value1 = 'null')">
            <xsl:value-of select="$header1"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$value1 and not($value1 = '') and not($value1 = 'null')">
            <xsl:if test="string-length($value1Currency) > 0">
              <xsl:value-of select="$value1Currency"/>
            </xsl:if>
            <xsl:value-of select="$value1"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
   <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$value2 and not($value2 = '') and not($value2 = 'null')">
            <xsl:value-of select="$header2"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="$value2 and not($value2 = '') and not($value2 = 'null')">
            <xsl:if test="string-length($value2Currency) > 0">
              <xsl:value-of select="$value2Currency"/>
            </xsl:if>
            <xsl:value-of select="$value2"/>
          </xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="planDetails_planInfoFourHeadersTmpl">
  <xsl:param name="header1" />
  <xsl:param name="header2" />
  <xsl:param name="header3" />
  <xsl:param name="header4" />

  <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header1"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header2"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header3"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header4"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="planDetails_planInfoTwoHeadersTmpl">
  <xsl:param name="header1" />
  <xsl:param name="header2" />

  <td width="50%" colspan="4" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header1"/>
      </span>
    </p>
  </td>
  <td width="50%" colspan="4" class="tdMid">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:value-of select="$header2"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="planDetails_planInfoFourValuesTmpl">
  <xsl:param name="value1" />
  <xsl:param name="value2" />
  <xsl:param name="value3" />
  <xsl:param name="value4" />

  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value1"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value2"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value3"/>
      </span>
    </p>
  </td>
  <td width="180" colspan="2" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value4"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="planDetails_planInfoTwoValuesTmpl">
  <xsl:param name="value1" />
  <xsl:param name="value2" />

  <td width="50%" colspan="4" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value1"/>
      </span>
    </p>
  </td>
  <td width="50%" colspan="4" class="tdMid">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$value2"/>
      </span>
    </p>
  </td>
</xsl:template>

<xsl:template name="appform_pdf_header">
  <table class="office">
    <tr>
      <td class="officeHeading">
        <p><span lang="EN-HK" class="officeHeading">FOR OFFICE USE ONLY</span></p>
      </td>
    </tr>
    <tr>
      <td class="officeContentFirstRow">
        <p><span lang="EN-HK" class="officeTitle">Proposer Number:</span></p>
        <p>
          <span lang="EN-HK" class="officeContent">
            <xsl:choose>
              <xsl:when test="/root/policyNumber">
                <xsl:value-of select="/root/policyNumber"/>
              </xsl:when>
              <xsl:otherwise>
                <br/>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
    </tr>
    <tr>
      <td class="officeContent">
        <p><span lang="EN-HK" class="officeTitle">Financial Consultant Code:</span> </p>
        <p>
          <span lang="EN-HK" class="officeContent">
            <xsl:choose>
              <xsl:when test="/root/agent/agentCode">
                <xsl:value-of select="/root/agent/agentCode"/>
              </xsl:when>
              <xsl:otherwise>
                <br/>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
    </tr>
    <tr>
      <td class="officeContent">
        <p><span lang="EN-HK" class="officeTitle">Financial Consultant Name:</span></p>
        <p>
          <span lang="EN-HK" class="officeContent">
            <xsl:choose>
              <xsl:when test="/root/agent/name">
                <xsl:value-of select="/root/agent/name"/>
              </xsl:when>
              <xsl:otherwise>
                <br/>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
    </tr>
    <tr>
      <td class="officeContent">
        <p><span lang="EN-HK" class="officeTitle">Name of Organisation:</span></p>
        <p>
          <span lang="EN-HK" class="officeContent">
            <xsl:choose>
              <xsl:when test="/root/agent/company">
                <xsl:value-of select="/root/agent/company"/>
              </xsl:when>
              <xsl:otherwise>
                <br/>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
    </tr>
    <xsl:if test="/root/proposer/personalInfo/branchInfo/branch">
      <tr>
        <td class="officeContent">
          <p><span lang="EN-HK" class="officeTitle">SingPost Branch:</span></p>
          <p>
            <span lang="EN-HK" class="officeContent">
              <xsl:choose>
                <xsl:when test="/root/proposer/personalInfo/branchInfo/branch">
                  <xsl:value-of select="/root/proposer/personalInfo/branchInfo/branch"/>
                </xsl:when>
                <xsl:otherwise>
                  <br/>
                </xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </tr>
    </xsl:if>
    <xsl:if test="/root/proposer/personalInfo/branchInfo/bankRefId">
      <tr>
        <td class="officeContent">
          <p><span lang="EN-HK" class="officeTitle">Referrer ID:</span></p>
          <p>
            <span lang="EN-HK" class="officeContent">
              <xsl:choose>
                <xsl:when test="/root/proposer/personalInfo/branchInfo/bankRefId">
                  <xsl:value-of select="/root/proposer/personalInfo/branchInfo/bankRefId"/>
                </xsl:when>
                <xsl:otherwise>
                  <br/>
                </xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </tr>
    </xsl:if>
  </table>
  <p class="img">
    <img class="companyIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPcAAAD1CAYAAACMRGNYAAAgAElEQVR4nO2dZ1RUV/v2/2sBtkT2zNDLoFETK9iNPYnRJxqN0RiNpmk0McXyGEuKqWqMUQF7w4I1NrDGbqyJvWLsIooNEUEklGFmzvV+4MEXlSn7sIczZ+a+1/p9ZPbZm32ds8td/s+DRYIgCOeiLJuA3pqPcTukAUxh4XaTq4/Ayird8e6rMfg/pTtBEMTjeLKJaMGG4Vhgcy5hm8LCsT3gZdQMmYBWrywncROEsxHIxmCD/6sw6vmEfTqoKeqwkfAJmU7iJghnoyybgBjfTsjTR3AJ+2ZIA7Rng+DBIkncBOGMDNe+h1xOYWfrI/Bf7QeowMbDg5G4CcKp8GIT8RobhDvBfAdoefpwLPB9HYFszKPfInEThJPgySJRl32DfQGtuPbZRn049ge2QgP29WO/R+ImCCchiI3GfN+OyOZcjt8KaYBOmgHwZBMf+z0SN0E4AeXYBHytexcZofW4hG3Qh+MTTV+UYxOe+k0SN0E4AW3YEGSE1OW+z47y6VKssD0YiZsgFKca+wG3g+tziTpfH45t/i/Dr8gB2pOQuAlCQcK8f8a2gJe5hG3Uh+N0UDO0YkOf2mcXhcRNEArhy8YiStcV/4byH6D11fR7dJ9tCRI3QShABTYe/TUf4Y6MgJAJuregYeNstkHiJggFaOo9HJeCGnMfoO0NaAUN+9WuNkjcBFHK+LFfsCugNbewrwQ3QigbZXc7JG6CKEU0bBxm+XbiFvb14IaPAkLshcRNEKVEeTYe3+h6IY8zhDMtpD6Ga9/DszYO0J6ExE0QpYAni0RPTX/cCGnIfYA2z7cjAtlo7jZJ3ARRCjRiX+FQYAvuxAuHA1ughvd38JTRJombIBxMMBuNRb4duIV9N6Q+XtPw7bOLQuImCAdSno3HSF1P7kivzNC6+K/2A3hY8UCzBYmbIBxID/YpUkL4/Mbz9OGI0nVFec4DtCchcROEg2jKRiApuBH3tVe8fzuEef9c4vZJ3AThAELYaBwObMEt7FNBzdCcDZd1gPYkJG6CEIyGjcMc3ze4ExzeCWmAfpq+8CrBPrsoJG6CEMizbDwGaj9EioyAkIm6t1CR/SbsWUjcBCEILzYR7dhgnAtqwr0cX+/3KkJkOKpYg8RNEIIIZqNxILAl9312QlBTVGU/CH8eEjdBCMCLTcAf/m2Qzyns1JB6aMmGOuSZSNwEUULKs/EYpevBvRQvdFSxlVFFLiRugigB5dgE9NT0x11OR5VsfQRifDshWPA+uygkboKQSWGJ3aOBzbkrhOwNaIW67BuHPh+JmyBkEub9M+L82nHfZ6eG1EMbNkTYfbYlSNwEIYOybALG6Lpzl9g16sPxgeZjhwvbg5G4CYIbLzYRXTWfc3+xc/URmOTzZqk9J4mbIDhpyYbhDucBmkEfjg3+ryLIgQdoT0LiJggOarOR2BfYivva62hgc7Rkw0plOV4IiZsg7CSAjcE8347I4VyO3wxpgN6aj0scn80LiZsg7KACG48vte/jQShfJc48fQSifEqeeEEOJG6CsIEni0Rb9l9c58xcatSHY1fAS/C2o/SPIyBxE4QNqrEfcDCwJfc++2RgM1RhPyr23CRugrCCr/dYxPu15RZ2YnAj7gohoiFxE4QFyrPxiNZ1gYEz0ut+aD2HBoTYC4mbICzQX/MR7sm4z47x6QQf77GKPz+JmyCewItNRGs2FKeDmnEfoP0V0BI1vL9TvA8ejMRNEE9Rhf2INX7tuDOqJMuoxOlISNwEUYTybDyifbpwVwh5GFoXHwvMXCoCEjdBFOFzTW9kcDqqGPXhGKPrrvgB2pOQuAnif7RmQ/FvKN8X2xRWUCEkiI1R/PmfhMRNuD2eLBIvsO+RENSU+4t9MqgZmrLhivehOEjchNsTwkZhsW8H7vjswoCQsmyC4n0oDhI34dYwNg4jdT2Rxl2JMwK/6LqDKeQ3bg8kbsJt8WQT8YZmAJI5A0JMYeFY598WfuwXxftgDRI34bYEsjG4LKPE7oWgJk7hgWYLEjfhlmjZr9gT0Jpb2LeCG6CVgyqEiIbETbgdWjYO0T5duYWdGlIPg7UfOt19tiVI3IRbUYGNx+eaPtwBIf+GRmCWTyenvM+2BImbcBs8/1di92zQi9wVQvYHtER19r3ifeCBxE24DZXZT9ge8DJ3QEhmSF008x4OTyfoAw8kbsIteJaNxySfLtz77KzQuuir6af488uBxE24PGXZBHyh6c2dkvjf0AhM9HkLnk4U6cUDiZtwebpqPkdqSD0uYefrw7HSrx1C2CjFn18uJG7CpWnMvsIJzowqprBwHA5sgWZsuFPFZ/NC4iZcliA2GnF+7ZDPeYB2O6QBPtB8omphezASN+GiVGS/4UdtD+747Fx9BCbo3nLaSC8eSNyEy+HJJqKH5lPcDGnAvRxf699WFX7j9kDiJlyOeuwbnOFMvGAKC8eBwJaoqmCFENGQuAmXIoCNkVX652pwI7zChij+/CIhcRMuQzk2Hkt923N7oD0MrYvPNH1QzgX22UUhcRMuQWGJ3fRQvvvsHH1BQIivkydekAOJm1A9ZdkEvK4ZiPNBTbiEbdSH48+Al1CbjVS8D46AxE2onhre3+HPgJe4l+M3ghuijYvts4tC4iZUTVk2AYv9+DOX5ugj0J19ijIqd1SxBombUDXf6npxl9g1hYXjJ10P1Xug2YLETaiSMmwiOmsGcLuWGvThiPNrhwAVZVSRC4mbUB2eLBJN2XCcDXqR+wDtSGALNGXqS7wgBxI3oTqqsh8Q798OeZz77OTghnhX0x/lXew+2xIkbkJVaNmvGKt7Gw85K3Ea9OEYpeuBiuw3xftQWpC4CdXgySLRTfMZMjmFbQoLx7aAl1WTklgUJG5CNdRi3yE5mC/Sy6gPx+mgZi7pgWYLEjehCp5jP+KQjICQS8GNXdpRxRokbsLp8WO/INb3dW5hp4TUx0Dth3jWzZbjhZC4CaemAhuPkbqeyOAMCMnWR2CGzxtuuRwvhMRNOC2eLBJvaL7ANRmVOA8GtkQl9rPifVASEjfhtNRhI7E/sBW3sO+EqKcSpyMhcRNOiZb9ikW+HbiFnRZaH300Hyv+/M4AiZtwOsqyCfhF97asSK/Ruh4o4yYeaLYgcRNOx8eavtyOKkZ9OJb4tld1hRDRkLgJp6IlG4YrwY25l+NHApujMftK8ed3JkjchFPgySJRhf2ITf5tZGVUeU/TX7UF+xwFiZtwCnTeYxHp05W7Qkje//bZFbzd01HFGiRuQnE8WST6az7C3ZD63MvxZX6vubWjijVI3ITivMhGIDm4oSxHlWA6QLMIiZtQlAA2BskyanrdDGlAB2g2IHETiuHjPRbx/u1kOKrUw2eaPijvpgEhdo8viZtQAg0bhx917yCD8z47K7Qupvl0dosEhyWFxE2UOuXYBPTQfIpEzoAQoz4cOwJeRnX2veJ9UAMkbqLUCWff4nhgM+777NSQ+mjiPcItMpeKgMRNlCpebAI2+7/CLex8fTheZwNJ2ByQuIlSowIbj3G6btwHaNn6CIzR9VD8+dUGiZsoFcqxCein6ctd+idXH4Hlfv9BIButeB/UBombcDieLBLt2SBcDOYvsXsgsCWasBEuX9fLEZC4CYdTm43EtoCXub/aN0Iaogf7FOUoPlsWJG7CoejYr5ji05m7YJ9RH45fdN3pi10CSNyEw/BiE/GJ5iPuSK98fTg2+LdxuwohoiFxEw7jFTYEN2X4je8PoIAQEZC4CYdQl32LfzhL7JrCwnEuqAlecdMKIaIhcRPCCWJjsMH/VW5hp4bUw6eaPrQcFwSJmxBKBTYe43XdkM2ZuTTvfxVCGBuneB9cBRI3IYxybALe0/SXtc/eHdAaVdiPivfBlSBxE8JoykbgWGBzbmEnBTdCa6oQIhwSNyEEnfdYbJIREHI/tB56sE8pIMQBkLiJElOGTcBC39eRxylsgz4cX2t7oSx5oDkEEjdRYoZq3+f+YpvCwrHErwP8KXOpwyBxE7IpwyaiAxvEXWI3X1+QuZQSHDoWEjchC08WiQj2LXYFtOb+al8Pboh3NP1RhvzGHQqJm5CFPxuDeb4due+z8/Xh+FbbC8+So4rDIXETshihe4+7EqcprKASp4YcVUoFEjfBTXM2DDmcX2xTWDhOBjZDBe/fFH9+d4HETXDxPPsBScGNkK2P4OJicBM0ZcMVf353gsRNcNGeDcIYXQ9uOmkGUkBIKUPiJggXhcRNEC4KiZsgXBQSN0G4KCRugnBRSNwE4aKQuAnCRXF7cWsCpyCo0swSE1hpBir6T37q98v7ToK/foaQNorir5/u0HEpo42CX+h0AeMyEyxgSrFtPOs/GQFhJR8b31DHjoVacWtxV6s5F9u2J0GEZWcb8d0P+59qI7TKLKxafR5msySknUK7dCkdZXXRDhub+o0X4fLl9BI9Y36+GXHxF1ClRsxTv1/OJxrjJx5GXp6pxGNx+MhtPOM3SfH55Gy4rbjL6qLxYZ9NyMjIK/HkAgCDwYTIqCPFtvXu+xuRkpItpJ1CS0zMQDkfx4m7a7e1uH07S/bzZWcbMSfmFAIrzSz29+s1WohDh28LGYuTp+4i5LlZis8pZ8Ntxe2vn4GNf1wRMrkAwGyWsCA2AZ6ap9vyC52O7TuSIAn8eN+/n+swcXtqIvH9j/uRnW2U9WzZ2Ub8NuEwtEFTi/39Z/0n4+tv9yI7O1/IWJw/n4Y69WIVn1POhtuKu1PneOTkyJu8lmx13IVi990eLBL9PtkKo9EsrC2zWYK/foZDxiao0kzEr7ko62WUl2dCVPRRBIRZfraaEfPxzz/3hI1FUtIDvNJ2heJzytlwW3H/9fdNYZOr0LZsvYrgysUvD5/1n4w7d/4V2t5/OqxyyNh06hyPW7f4l+Qmk4Sly85afel4aiIxMfKI0HFISclGr/c2KD6nnA23FPfbPdYJnVyF9tffN1EzYoHFdod/tUdoe5OnHBM+NmFVZyMu/qKs59m2PanYw7OiNGiyCP/+K2Y5XmhZWfkY8fUexeeVs+F24g6sNAMnT90VOrkKLSEhFc1bLbPYdkDYDKFf72vXHuAZv+K3AXIoo43C6F8OwGDgP8E+dvwOIhostPr72qCp2LM3WVj/C81slhAz95TFLZG74lbi9tREYtiI3bImrz2WnJyJzl3WWH2GOTGnhLbZorXllwkvffpukXV7cP58Gpo0X2Jj7KMwdPhu4VeChbZ3X7LVVZM74lbirlMvFkeP3RF6al3UcnKM6P/Z9mJPzAtp//pqoRN8zNgDQsbm5VdXIDU1h7v9lJRsdO4Sb/P327RbgUuXSnZvbs1u387C653iFJ9jzoTbiLusLhpjxspbcvLY1GnHLV4BebBIVH4hBmfPijspPnT4Fsrqoko0NpVfiMGJkyncbefmGvHlsF0o72vdgSTkuVnYsPGKw16qhTZ0+G6H3v2rDbcQt5c2Ci+/uhxnz6Y5dnYBOHjoFl6oPc/is2iDpmLGzJPC2rt27QEavrhY9tgEhM3AgtgE5OfzXdMZDCbMX5Bg0UmlkIr+k/HdD/uFXzsWZ6tWn7d6BeduuIW4/fUzsHTZWUiO/nQAePjQgHbtV1lcmntpo/Bhn03CTowzMvLw2Rc7ZI2Ld8AUfPXNXqSn8++z9+5LRnh9244jb7wZj+TkTCF9tWVJSQ9QvY7lF6u74RbiFukNZY/9NOpvlNFaXio3ab5E2Im9ySRhTswpVLCxNC6Ort3WyhLenTv/4qU2y23+fli1OTh2/I6QftpjJpOErt3WKj7fnAWXF3e79itlfZlKYgcO3rIqNp+QaVi67Kyw9nbvScbztfi+WFVqxMgKDMnNNaJT53h4WXl5ebBIVPCdhM1brpbKaqmozZx1UvE55yy4tLjDqs4uUfCDXDObJVR6fo7VZ/ty2G48fGgQ0t61aw+4Toq9A6bg3Dn+8wdJkux2Fhn9ywEhfeO1a9ceKD7vnAWXFbdf6HSsWCk+1NJe++qbvVafr3mrZSUOqSy0/Hwz/vvlLpSxIwRUGzQNC2ITuE+u8/PNWLL0H5sHaGW0UejyVskiykpqDZosUnz+OQMuKW65B0Vms4TcXDFXZcdPpFjdd7OAKdi2XVyk2MJFZ2wGkrCAKRj+1R5Z47Jv/w2bovHUFIRy7tt/g3s5bjSauU/sLdm48YcUn4POgMuJu4w2Ch3fiJP1VUxPz8OmzYlCJpjRaEatutY9pr4ZuQ8mkxh1n05IRY3w+RbbKquLRtdua5GYmMH929evP8Abb8ZbfVl5sIJbibnzTnMnYJAkYM/eZGFf+/Pn0xyayEItuJy4w6rOxoGDt2Qd5MTFX8SQobuETDAAGP6V9f1peP1YYU41eXkmtGtvOUqsZsQCHD+RImtcPh+ww6ajigeLxMDBO2Vd8WVk5OHNrmtw9Sr/i6c4y8szoX5jWpq7lLg9NZFYtPiMrAmRm2tEtZpz8dkXO4RMMADYu++GVVdUDxYpyzPMkv08+u9iT7E9NZGy00nNnnPKrrGvUy9WtqPK5wN2oE69WGHiNpkkDBuxW/H5qDQuI+5yPtEYOHinrC9Tbq4Jnw/YAS9tFPr03SJkggHA9euZNiOlBg/5U1h7+/+6+dTS2TtgCiZNPsr9WyaThM1brtr1xa5SIwZXrvBvgySpoA0vbRRqRizAhQv3uX+j+N8FNmy84vauqC4hbk9NFDp3WYP793O5J4LZLGHFyvOPwgV79FwvbKn84IEBnw+w7j0WVnU2srLEONhkZ+c/lkvsGb9JGDzkT+4MMJIEHDp8G/UaWX8xebCCrC3LV5yT9bwXL95Hs5ZL4cEi8XyteTh6TJzDy9mz91AzwvIZhDvgEuJu0GQR/j5wU9ZX++Spu2jaYumj5XOHjnFITRWTzNBkkhC78AyetRFnvHXbVSHtAcBH/bbAgxW4ub7dYx2uXXvA/RuJiRno/s46m1++iv5TMPL7/cjM5L+vz8jIw5Chux6NTVjV2di8Rdw43L2bjfc++EPxuUniLgFBlWZi/oIEWV/btLRcfPbFdpQvMolfbL4ECQmpwibZgYO3UNtG8r5PPt0urL1NmxPhwSLRqOliHDx0i/ue/+FDA779bp/NF5KXNgpvdl0j6+VhMklYvuIcgiv//ztzXfA0zJotLqDGaDRjYuSREkfMqRlVi7ucTzQ+H7BDVu5rk0nC4iX/PJUwP6zqbKxbf1nYJEtJycbbPdZZ7UeN8PlIS+PfUhRn+fkmPFc9BitWnpf193HxF+zyU69ac67s1MSXLqU/FcnmpY3CwME7YTKJSyL5x6ZEm56Croyqxf1Sm+W4deuhrH/8yVMpxWbuKKuLxrjxh4Q5VEiShFFj/raaND8gbAbWrJWXt6w427L1qizPvBMnUx77mlqiov9kzJt/WtazZWfno+/HW4v93favr8bVq/wrAUt26VK6XQEuropqxV2t5lwcOSrvAObevRx06mw5e0jPdzcIzXW2aXMiwqpZ/oKU952EocN3C019zGvnzqXZdTdczicaX32zV9azmkxmRE86anEvX6XGXOz885qwPuXlmfBx/202nW9cFVWK2zd0OuLXXJD9Dx84eKfVqKbna83FKYFJFFNSsm3mGGvXfiWSksR9tXjs1q2HeKeXfamBe723UdYBGgCsWXsRPiHTLP52eZ9oTJ12XNiqCQBmzT4JbZDlNl0Z1Ym7rC4aY8cdlF1jasbMExYL0xVtY/mKc8KCTiRJenSPbqnNSs/PEeb6ymPZ2fkY+f0+VPS3PiYerOCw8bTMw8ZTp+7aFdDxQe9NSEvjz+VmyU6euuu2+27VifudXhtk+yDv3XfDagqkovT7ZKvQfGvr1l+yerXkqYkUute3xwru+M/ZVTE0qNJMxMVfkHXdmJaWg17vbbAZA+7BCg7qRCZSNJmkR3fp7oZqxO2piULDFxfLqhQiScCVKxl44814m+6ghTxXPUZYkUAASE/PhZ+NUrNd3lqLmzflHRDKsSNHbeca92AFXm5ya4cV1A07ZHO1VJQ1ay8J7edPo/5SfP4qgWrE7a+fgUWLz8haKmdmGjB0+G7uVETbdyQJnWTvvr/RanuhVWbJPiTktTt3/kWHjqvtGodu3dfJvpVYu+4S97L4o37iXICBggyxSs9fJVCNuH8a9ZfsqpOzZp+0epBjid59NwudZH9sSrTanqcmEjNmnnB4gomsrHx82GeTXcvkGuHyi/ZdvHjfrpXBk4RWmSU0W2purtEt992qEPfbPdbJPqHdt/+GXXvK4tAFT0NWlphUSACQk5NvM/Xu653iHLrvNpsl/PjzX4955Vnu/1QcOnxLVkKJjIw8NGu51O5tUFE8NZHCDxcHDNqp+DwubZxe3LXrLpB9enr9+gPoq5SsKLtIbzUA+KD3JqvtVfCdhHv3xHirPWmSBKxcdR5BNlIlebCCpBdz58lzVDEazejTd0uJxr3nuxuE9n3P3mTF53Jp49TiDqs2R7a31e3bWejy1hq7lp7W6N13s9AMnitXnbf5TNNnHBfWXqGZzRIOH7mNF5vbPjmu4DsJ/T/bLuulmptrxOQpx2Rtg4qiCZwqK8rPkuXk5CO0hC96teG04vYJmYbxEw/LCodMT8/Ft9/tgybQclkfe6leZ57sw6Ti7NKl+zb3fy82XyIs/VKhpabm4N0PNtp8sZTRRqFNuxWy7rONRjM2/nEF1euUPNTSSxsl2z/e0rPZWjW5Gk4p7nI+0ej90WZZLqAGgwkLYhMQXFnMW9ovdDpWrRY3ydLScm0Wii+ri5Z9iGXJrl7NQN2Gtg+3Kj0/B5u3XJX1cvnnn3to+dLvsvbZT+KpicSHfTYJWzWZzQXhtyKeTS04pbjD68fKTvt7+MhtPFfdegF4Hsr5RGPAoJ3CTrCNRjOmTjtu8wv662+HhLRXaGlpOfiwj+0v14yZJ2Q57+Tnm9G5yxqhftx1Gy5EUpKY1EsAcPTYHbdamjuduMv7RMsuQZOZmWe3BxoPr7RdISy/FwDs/PM6Kr9g/QXUrOVSoafmJpOEadMtv1Q8NfKv/sxmCQMHiz+NLkmWl+IsOTkTHd+IU3yOlxZOJW5N4FTZJ7R372aj4xu260TLodLzc7Bh4xVhkywpyXaFkNp1F8jKS2bNtm1PKval4qWNwqv/WSnLjyAvz4QZM084ZNzL6qIxZOguGAxiXnLZ2UZ898P+Eh+yqgWnEfczfpMxZOgu5ObyTzCzWcLquAvo0DEObV9bJZwOHVcjLv6CsKW5wWDCiK/3WPQ1L+cTjfETDwu/775yJb3Y9Mf1Gi3C4SPyEi9cvHgfvftudsi4t31tFb7/cT/u3RMXSLJ8hX2+9K6AU4i7MGXPlSvylr6SJOHmzYe4fDndYaSl5Qq9Elux8rzFxAgDB+8UVkesqBWGuxZN2B9WdTYWL/lHdpRdRkYeEhMzHDbuN248FBrAc/xECho3sx5+6yo4hbjr1IvF3n03FKvrpYRZOr3u+EacsJRLxdn8BQmPAlgq+k/G19/uFZZ9VQ328KEB3d9Zr7jw3ELcmqCpiJkrb5+tduv13obHrmZqhM+XHS9tr51OSH106Nj2tVV48KB0yxs7g40dd/BRKmtXRlFxl9FGYfCQnYqmF1LSFi/559ESOeS5WViy9KzDx8JgMKHtayvxQu15uHhRTBEAtdmu3cmP5Xd3VRQVd+cua5Ce7rglqLPbzZsPoQue9iheWm5wDK/NiTmF3XuSS6UtZ7TMTAOat1qmuPhcVtyNmi7G2bNivbDUZpIEdHlrDXr0XF+q9awzMw3CSger1caMPai4+FxS3MGVZ2LtuktudYBmyfb/dRPXryuTGNGd7dKldJd3RS11cVfwnYSfR/8t6z6bjEykufrSvFTFXUYbhW7d1+HGjdLLE0ZGZsli5p5WXIAuI+6IBrGyPaHIyETbrVtZ8AlxXW+1UhO3JnAqNmy8IsvLa8XKc/j1t0NOw+Qpx2RHrZXERHrI2WPJyZmKj/WTxMw9LbCumhldu61VXISqFreXNgozZp6QdYe7fcc1hFWdDe+AKU5DUKWZmDZdfLYUa2YymREZdbTUDiENBhP6fbJV8bF+khdqzxNW8thsljBvfoLiIlStuD01Uej3yVZZEUeJiRlo2sL5Esp7aiLx/od/CPV5tmZms4Rvv9uHKjViSuX6UJKABbEJNvOsK8EzfgUHsqLsxMm70FedrXi/HIFDxe2ljcJLbZbj3Lk07kG/dy8Hn32xw64snUrQovUyWf3itfx8M5YuO4uyumhog6ZhxswTDm1PkiQcO57ilC/VQt7usQ5372YL6e+tW1no1t16iWW14lBxP19rHjZsvMKdsicvz4Rp04875ZejkNAqs4RXxnjSJEnC7j3Jj0oNe2mj8EHvTQ6JGCu0tLQcfNx/22ORY85G7Xqx+PsAf+WZ4sxgMOG3CYec9iNSEhwmbu+AKYiedExWcvnde5JRpYa4VEmOoKwuGr/8etChS/OLF++jU+f4x4TWoMliHD3muKoks+eccvqqmM/6T8b8BQnCkkhu3nJVaGouZ8Fh4v6o3xZZoYSpqTmoU2+B4gNjD93fcZzbaGamAcNGPF0CiQVMwfwFCQ45Ob948b7FGHNn49PPtwur5Xbt2gO0fW2l4n0SjUPEXb/xIllf7Lw8k931q5yB52vNE1rHu9AkqSCZg6VMLZ98uk1oTm+goJRvRINYxcfUXsLrx+LqVTFuu5IEDB7yp9UqrGpEuLir15knKy1vdrYRo385wF2sT0m8tFFYHSevrK01O3rsDgIrWS47VKvuAuGpj38e/beqfK3LaKOwfcc1Yf2Pi78Af731Uk9qQ6i49VVnY9nv/DHJ+flmrFp9QZXF2gYM2ik015nZLKFFa+s+z2W0UVi85B+hkV2Dh/ypKnF7sEgMGbpLWP9v385C7brq2A7aizBxawKn4oef9nNn9pCkgl7HThQAAAl3SURBVLxWzVouU2VWylp1FwitSClJkl1bky5vrRFalWTHzmvw1Khr/J+vNU/YGEgS8FG/rYr3SSRCxF1GG4Wu3dbKqhCSkZGHXu9tcOqrF1t9P3kqRcgEK7Tfl5+12W5F/8m4dk1cqGhOjtGuAoHOhJc2CocOi4tV2PjHFcX7JBIh4n6ueoyslD1ms4SJkUcUH4SS8uPP+4VNMADIyzOCBUyx2e6Ir/cIbXfwkD8VH0te/vvln8L6n5dngiao5PXlnIUSi9tfPwNbtvL7+prNEtZvuKyqAzRLhNePhckkNvdZr/c22mz3Px1WCW3z6LE7qltBhVWbLXRb9Mmn2xTvkyhKJG5N0FTZARR/H7iJajXnKj4AIvDSRuH8ebGuqMtXnLN6BvFCbfHXcGazhDr11HMd5sEKnIlE5oPbuy9ZlWc/xSFb3OV8ojFsxG5ZjiqXL6ejU+d4lxnECr7ROH5C7L774sX7qGrBS88nZBoWLjrjEEeWb0buVXw8efDSRgndnmRl5aNGeMlLEDsDssXd5a01sorjZWYaMPyrPXjGz3XyRv/62yHhbqj37+cWW5WzjDYKQ4buEuad9aTt3Zesuq1Si9bLhGWONRrNGDJ0l+J9EoEscdepF4v9f/E77ptMEn5ffg5aFzm08NREoUfP9Q7xLzeZJMyJOfVYSVxPTSRefnW58C1AUUtKeuDUEWHF8Vz1GGFLc0kCtmy9qrqzh+LgFre/fgYWxCbIcqA4dvyOyyx5vLRReLH5UuGeYkVt3/4bj41XWNXZiF9z0WHtAQXldoYO360qhxYWMAXjJx4Wtk25cOE+GjdbrHi/SgqXuMvqovHTqL9kFY27deuhSznnV6kRg7j4iw6tEHL7dhbeersgDZCXJhJjxh4QVs7WkkmShGW/n4Uu2Lkjw4riqYnEux9sFOZv/+BBHgYM2qmqF1xxcIn7k0+3yaotlZVlQO++m1U/WIVoAqfg198OObyAnskkYdSYA3jWfzJ69FyP9PTSqet14mQKmjRXVyXMBk0WCQuFNZslxC48A58Q9bzgisNucbdrvwo3b/KnJDabJYwddwgV/W07ZaiFd3qtx4MHpVP6Z9v2JLzeKa5UK5JkZeXjwz6bVPUy9g6Ygt+XnxPmb3/8RAoaNVX30twucVevMw8HD93iHiBJkrDxjyuo/ILrBMI/X2uew06qi7PMTANu384q9fI/06YfV93B59Dhu/HwoZjVVG6uEe++v1FVL7gnsSnu4MozMX9BAnfkkyQBZ86kok27FYp3UhSVX5iDEyfF3mc7qx0/kaI6J6NmLZciOTlT2BjMmn1SdS+4olgV9zN+kzDy+32y7hDT03PR75OtLnGl4MEiEVRpJmIXnhEaieXMlptrVFXiDA9WEEzz199icqsBBafmVWqo6wVXFKvifqfXeqSkyMsyOXbcQXjbEfygBrwDpuDrb/eW6nLcGWzS5GOKjz0vo8YcENZ/o9GM7u+sV7xPcrEo7vqNF8kK4QSAtesuQROo3uVMUby0UejcZY2s5Z7aq5gmJmaobs/ZuNlioWOwOu6C4n2SS7Hi1ledLcsLSpKAhIRUBKosLtgaVWvEyArQMBhMGDJ0F+7dy+H+W7lmNJpx4CD/wac1a/mSuiphemoicf26uH13bq5RVXf+RXlK3MGVZ+L35ee4nTMkCbhyJQOvvb7aZQJCfEOnIy7+AveEyMrKx9DhuxEQNsPhHmWFlp1txLTpx+Gvny40r/n8BeortzNu/CFh/QeAL4ftVrxPcnhM3LrgaRg15m9Zjip372bji4E78Ky/awSEsIApiIo+yn0FlZdnwtRpx1HBNxrlfSdh6PDdwmO9i2tz8ZJ/EFplFjxYJP7YlCjst9PScuDrxMUhiqPhi4uEjvnp03dRUYXz+pG4y/lEo3ffzbKcJQwGE2bPOam6SWCJsrpoDB2+G//+y3dnKknA+g2XH7tCeq3DKty4IW6Z+KSZzRK277iGOvViH+2PP/tiu9A2ur+jrnI7z/hNElrqKTfXhHbtVyneL14eibtJ8yWy7wjPnElFWDX1ZS61RLfua5GUxJ+f7OzZe2jTbsVj25JqNecJTcH7pCUmZjyVXLJeo4VC85qvWHlOVQdrZbRRmDlLXE01s1nC3HmnFe8XL4/EnZoq7+DHYDCp7tDFEp6aSDRquhiHj/An3cvMNBQbB1zOJxpTph5zyMl5fr4ZPXo+fVUTWmUWtu9IEtbO1asZaNxMPb7mnpoo9P5os7D+AwUHxTUj1BXR+Ejccjqcl2fCdz/sV7wToqheZz42bLwi65+/fMU5i/f6fT/eKqxgfKEZDCaMGXug2Pa8AwoCW0RZbq4JEyOPqOo8pUXrZUJzq2Vk5GHg4J2qOiyWLW5JAtatvwyfENfYZ1etORfxay7KiglOTMxA9TqW3+rNWy3D5cvp3L9rzdauu2TxZeKpiUTvjzYLndwJCalo3ko9K7Tw+guRmMifKciSSVJBVZLCQ0s1IFvcFy7cR5t2K1X1JrNEjfD52LL1qqyls9ksofdHm63+fmiVWUJ90pOTM/FK2xVW98HtX18t1M+6sNStWpyTqtWcK/zO//btLHR8I0415w+yxJ2dbcT3P+53iTxoL9Seh23bk2TviXfvSbY54ctoo7DzTzGHapJU4BZqK79242ZLhCdtvHnzIRo0WaT4/8wewqrOxsY/xF0JAgVRjmoocVwIt7glCdizNxmVX1D36binpuAke8PGK7LT8xgMJnTrvs6uMjyz55wUcqh26VI6Xv2P7Yw2z1WPkZVP3pYtiE1QRdmhwEozsXTZWeH9f/AgTzWHi4/EnZlpgD3cuvUQH/R+Oiun2girNgexCxOQkZFnV7+LgydWfcCgnbh/P1d2W5mZBty/n4vpM07YFZDDAqYgZu6pErVXHKmpOajf2Pm/3j4h0zB12nHh/c/MNGDlqvN2VYRRmkfiHjh4J+yh78dbXaLkSq26CzBgkH19tkTrV35HeTtrOlevMx9fDNxRovb6f7YddRsutKu9wkypJWnPcr+XK/7/s0UF30l4pe0Kh/R/wKCdCHnO+Q/WhNfnJgjCOSBxE4SLQuImCBeFxE0QLgqJmyBcFBI3QbgoJG6CcFFI3AThopC4CcJFIXEThItC4iYIF4XETRAuCombIFwUEjdBuCgkboJwUUjcBOGikLgJwkUhcROEi0LiJggXhcRNEC4KiZsgXBQSN0G4KCRugnBRCsX9/wD/QaoB28MevAAAAABJRU5ErkJggg==" />
    <span class="pageTitle"><br/></span>
  </p>

  <p>
    <span class="pageTitle">Application Form (EASE)</span>
  </p>

  <p class="sectionGroup">
    <span class="sectionGroup"><br/></span>
  </p>

  <table>
    <tr>
      <td class="warning">
        <p class="statement">
          <span class="warningLarge">WARNING NOTE:</span>
        </p>
        <p class="statement">
          <span class="warningLarge"><br/>Pursuant
            to Section 25(5) of the Insurance Act of Singapore (CAP 142),
            you are to dislose in this proposal form, fully and faithfully,
            all the facts which you know or ought to know, or the policy
            issued below may be void</span>
        </p>
      </td>
    </tr>
  </table>

  <p class="sectionGroup">
    <span class="sectionGroup"><br/></span>
  </p>
</xsl:template>

<xsl:template name="appform_pdf_signature">
  <div class="section">
    <p class="statement">
      <span class="signature">Signed and dated in Singapore</span>
    </p>
    <xsl:choose>
      <xsl:when test="not($lifeAssuredIsProposer = 'true') and /root/showLaSignature = 'true' and $hasTrustedIndividuals = 'true'">
        <table class="signature">
          <col width="145"/>
          <col width="52"/>
          <col width="145"/>
          <col width="52"/>
          <col width="145"/>
          <col width="52"/>
          <col width="145"/>
          <tr>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">TI_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">LA_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Trusted Individual</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Life Assured</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Proposer</span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:when test="not($lifeAssuredIsProposer = 'true') and not(/root/showLaSignature = 'true') and $hasTrustedIndividuals = 'true'">
        <table class="signature">
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <tr>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">TI_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Trusted Individual</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Proposer</span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:when test="not($lifeAssuredIsProposer = 'true') and not($hasTrustedIndividuals = 'true') and /root/showLaSignature = 'true'">
        <table class="signature">
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <tr>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">LA_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Life Assured</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Proposer</span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:when test="not(not($lifeAssuredIsProposer = 'true')) and $hasTrustedIndividuals = 'true'">
        <table class="signature">
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <col width="49"/>
          <col width="210"/>
          <tr>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="statement">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">TI_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Trusted Individual</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Proposer</span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:otherwise>
        <table class="signature">
          <col width="330"/>
          <col width="60"/>
          <col width="330"/>
          <tr>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
              </p>
            </td>
            <td class="tdMid">
            </td>
            <td class="signatureCell">
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell"><br/></span>
              </p>
              <p class="signatureCell">
                <span lang="EN-HK" class="signatureCell">PROP_SIGNATURE</span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature"><br/></span>
              </p>
            </td>
            <td class="tdMid">
              <p class="signature">
                <span lang="EN-HK" class="signature">Signature of Proposer</span>
              </p>
            </td>
          </tr>
        </table>
      </xsl:otherwise>
    </xsl:choose>
  </div>
</xsl:template>


<xsl:template name="healthTableTmpl">
  <xsl:param name="laIsProp" />
  <xsl:param name="answerId" />
  <xsl:param name="isAnswerIdDataSuffixNumber" />
  <xsl:param name="headerColumn1" />
  <xsl:param name="headerColumn2" />
  <xsl:param name="headerColumn3" />
  <xsl:param name="headerColumn4" />
  <xsl:param name="headerColumn5" />
  <xsl:param name="headerColumn6" />
  <xsl:param name="headerColumn7" />
  <xsl:param name="headerColumn8" />

  <xsl:variable name="answerIdData" select="concat($answerId, '_DATA')"/>

  <table class="data">
    <xsl:if test="not($laIsProp = 'true')">
      <col width="69"/>
    </xsl:if>
    <col width="99"/>
    <col width="59"/>
    <col width="64"/>
    <col width="69"/>
    <col width="74"/>
    <col width="120"/>
    <col width="62"/>
    <col width="83"/>
    <tr>
      <xsl:if test="not($laIsProp = 'true')">
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="th">For</span>
          </p>
        </td>
      </xsl:if>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn1) > 0">
                <xsl:value-of select="$headerColumn1"/>
              </xsl:when>
              <xsl:otherwise>
                Medical Condition/ Diagnosis
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn2) > 0">
                <xsl:value-of select="$headerColumn2"/>
              </xsl:when>
              <xsl:otherwise>
                Year of diagnosis
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn3) > 0">
                <xsl:value-of select="$headerColumn3"/>
              </xsl:when>
              <xsl:otherwise>
                Type of Test done (if any)
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn4) > 0">
                <xsl:value-of select="$headerColumn4"/>
              </xsl:when>
              <xsl:otherwise>
                Date of Test done (if any)
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn5) > 0">
                <xsl:value-of select="$headerColumn5"/>
              </xsl:when>
              <xsl:otherwise>
                Test Result (if any)
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn6) > 0">
                <xsl:value-of select="$headerColumn6"/>
              </xsl:when>
              <xsl:otherwise>
                Details (Medication/ Treatment/ Follow-up/ Complication)
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn7) > 0">
                <xsl:value-of select="$headerColumn7"/>
              </xsl:when>
              <xsl:otherwise>
                Name of Doctor
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">
            <xsl:choose>
              <xsl:when test="string-length($headerColumn8) > 0">
                <xsl:value-of select="$headerColumn8"/>
              </xsl:when>
              <xsl:otherwise>
                Address of Hospital/Clinic
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </p>
      </td>
    </tr>
    <xsl:if test="not($laIsProp = 'true')">
      <xsl:for-each select="/root/insured">
        <xsl:for-each select="insurability/*[local-name()=$answerIdData]">
          <tr>
            <xsl:if test="position() = 1">
              <td>
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="count(/root/proposer/insurability/*[local-name()=$answerIdData]) > 0">
                      <xsl:value-of select="'tdFor'"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'tdFor bottom'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="rowspan">
                  <xsl:value-of select="last()"/>
                </xsl:attribute>
                <p>
                  <span lang="EN-HK" class="person">Life Assured</span>
                </p>
              </td>
            </xsl:if>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer1">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '1')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'a')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer1) > 0">
                      <xsl:value-of select="$laAnswer1"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer2">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '2')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'b')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer2) > 0">
                      <xsl:value-of select="$laAnswer2"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer3">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '3')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'c')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer3) > 0">
                      <xsl:value-of select="$laAnswer3"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer4">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '4')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'd')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer4) > 0">
                      <xsl:value-of select="$laAnswer4"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer5">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '5')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'e')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer5) > 0">
                      <xsl:value-of select="$laAnswer5"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer6">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '6')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'f')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer6) > 0">
                      <xsl:value-of select="$laAnswer6"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer7">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '7')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'g')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer7) > 0">
                      <xsl:value-of select="$laAnswer7"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:variable name="laAnswer8">
                    <xsl:choose>
                      <xsl:when test="$isAnswerIdDataSuffixNumber">
                        <xsl:value-of select="./*[local-name()=concat($answerId, '8')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()=concat($answerId, 'h')]"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="string-length($laAnswer8) > 0">
                      <xsl:value-of select="$laAnswer8"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
          </tr>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:if>
    <xsl:for-each select="/root/proposer/insurability/*[local-name()=$answerIdData]">
      <tr class="even">
        <xsl:if test="not($laIsProp = 'true') and position() = 1">
          <td class="tdFor bottom">
            <xsl:attribute name="rowspan">
              <xsl:value-of select="last()"/>
            </xsl:attribute>
            <p class="tdAns">
              <span lang="EN-HK" class="person">Proposer</span>
            </p>
          </td>
        </xsl:if>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer1">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '1')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'a')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer1) > 0">
                  <xsl:value-of select="$phAnswer1"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer2">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '2')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'b')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer2) > 0">
                  <xsl:value-of select="$phAnswer2"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer3">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '3')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'c')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer3) > 0">
                  <xsl:value-of select="$phAnswer3"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer4">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '4')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'd')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer4) > 0">
                  <xsl:value-of select="$phAnswer4"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer5">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '5')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'e')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer5) > 0">
                  <xsl:value-of select="$phAnswer5"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer6">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '6')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'f')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer6) > 0">
                  <xsl:value-of select="$phAnswer6"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer7">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '7')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'g')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer7) > 0">
                  <xsl:value-of select="$phAnswer7"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:variable name="phAnswer8">
                <xsl:choose>
                  <xsl:when test="$isAnswerIdDataSuffixNumber">
                    <xsl:value-of select="./*[local-name()=concat($answerId, '8')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()=concat($answerId, 'h')]"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="string-length($phAnswer8) > 0">
                  <xsl:value-of select="$phAnswer8"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>

<xsl:template name="healthGroupTableTmpl">
  <xsl:param name="laIsProp" />

  <table class="data">
    <xsl:if test="not($laIsProp = 'true')">
      <col width="59"/>
    </xsl:if>
    <col width="30"/>
    <col width="95"/>
    <col width="50"/>
    <col width="82"/>
    <col width="72"/>
    <col width="66"/>
    <col width="101"/>
    <col width="71"/>
    <col width="71"/>
    <tr>
      <xsl:if test="not($laIsProp = 'true')">
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="th">For</span>
          </p>
        </td>
      </xsl:if>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Qn</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Medical Condition/ Diagnosis</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Year of diagnosis</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Type of Test done (if any)</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Date of Test done (if any)</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Test Result (if any) </span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Details (Medication/ Treatment/ Follow-up/ Complication)</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Name of Doctor</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Address of Hospital/ Clinic</span>
        </p>
      </td>
    </tr>

    <xsl:if test="not($laIsProp = 'true')">
      <xsl:for-each select="/root/insured">
        <xsl:for-each select="insurability/HEALTH_GROUP_DATA">
          <xsl:variable name="qn">
            <xsl:choose>
              <xsl:when test="DATATABLE = 'HEALTH02_DATA'">1a</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH03_DATA'">1b</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH04_DATA'">1c</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH05_DATA'">1d</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH06_DATA'">1e</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH07_DATA'">1f</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH01_DATA'">1g</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH08_DATA'">2</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH09_DATA'">3</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH10_DATA'">4</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH11_DATA'">5</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HIM02_DATA'">2a</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HIM03_DATA'">2b</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HIM04_DATA'">2c</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER02_DATA'">2</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER03_DATA'">3</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER04_DATA'">4a</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER05_DATA'">4b</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER06_DATA'">4c</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER07_DATA'">4d</xsl:when>
              <xsl:when test="DATATABLE = 'HEALTH_HER08_DATA'">4e</xsl:when>
              <xsl:otherwise>-</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <tr class="odd">
            <xsl:if test="position() = 1">
              <td>
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="count(/root/proposer/insurability/HEALTH_GROUP_DATA) > 0">
                      <xsl:value-of select="'tdFor'"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'tdFor bottom'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="rowspan">
                  <xsl:value-of select="last()"/>
                </xsl:attribute>
                <p>
                  <span lang="EN-HK" class="person">Life Assured</span>
                </p>
              </td>
            </xsl:if>
            <xsl:if test="SHOW_QN = 'Y'">
              <td class="tdAns">
                <xsl:attribute name="rowspan">
                  <xsl:value-of select="ROW_COUNT"/>
                </xsl:attribute>
                <p class="tdAns">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:value-of select="$qn"/>
                  </span>
                </p>
              </td>
            </xsl:if>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="HEALTH_GROUPa"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="HEALTH_GROUPb"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:choose>
                    <xsl:when test="string-length(HEALTH_GROUPc) > 0">
                      <xsl:value-of select="HEALTH_GROUPc"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:choose>
                    <xsl:when test="string-length(HEALTH_GROUPd) > 0">
                      <xsl:value-of select="HEALTH_GROUPd"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:choose>
                    <xsl:when test="string-length(HEALTH_GROUPe) > 0">
                      <xsl:value-of select="HEALTH_GROUPe"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="HEALTH_GROUPf"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:choose>
                    <xsl:when test="string-length(HEALTH_GROUPg) > 0">
                      <xsl:value-of select="HEALTH_GROUPg"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:choose>
                    <xsl:when test="string-length(HEALTH_GROUPh) > 0">
                      <xsl:value-of select="HEALTH_GROUPh"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
          </tr>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:if>

    <xsl:for-each select="/root/proposer/insurability/HEALTH_GROUP_DATA">
      <xsl:variable name="qn">
        <xsl:choose>
          <xsl:when test="DATATABLE = 'HEALTH02_DATA'">1a</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH03_DATA'">1b</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH04_DATA'">1c</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH05_DATA'">1d</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH06_DATA'">1e</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH07_DATA'">1f</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH01_DATA'">1g</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH08_DATA'">2</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH09_DATA'">3</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH10_DATA'">4</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH11_DATA'">5</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HIM02_DATA'">2a</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HIM03_DATA'">2b</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HIM04_DATA'">2c</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER02_DATA'">2</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER03_DATA'">3</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER04_DATA'">4a</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER05_DATA'">4b</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER06_DATA'">4c</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER07_DATA'">4d</xsl:when>
          <xsl:when test="DATATABLE = 'HEALTH_HER08_DATA'">4e</xsl:when>
          <xsl:otherwise>-</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <tr class="even">
        <xsl:if test="not($laIsProp = 'true') and position() = 1">
          <td class="tdFor bottom">
            <xsl:attribute name="rowspan">
              <xsl:value-of select="last()"/>
            </xsl:attribute>
            <p class="tdAns">
              <span lang="EN-HK" class="person">Proposer</span>
            </p>
          </td>
        </xsl:if>
        <xsl:if test="SHOW_QN = 'Y'">
          <td class="tdAns">
            <xsl:attribute name="rowspan">
              <xsl:value-of select="ROW_COUNT"/>
            </xsl:attribute>
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:value-of select="$qn"/>
              </span>
            </p>
          </td>
        </xsl:if>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="HEALTH_GROUPa"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="HEALTH_GROUPb"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:choose>
                <xsl:when test="string-length(HEALTH_GROUPc) > 0">
                  <xsl:value-of select="HEALTH_GROUPc"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:choose>
                <xsl:when test="string-length(HEALTH_GROUPd) > 0">
                  <xsl:value-of select="HEALTH_GROUPd"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:choose>
                <xsl:when test="string-length(HEALTH_GROUPe) > 0">
                  <xsl:value-of select="HEALTH_GROUPe"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="HEALTH_GROUPf"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:choose>
                <xsl:when test="string-length(HEALTH_GROUPg) > 0">
                  <xsl:value-of select="HEALTH_GROUPg"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:choose>
                <xsl:when test="string-length(HEALTH_GROUPh) > 0">
                  <xsl:value-of select="HEALTH_GROUPh"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>

<xsl:template name="lifeStyleTableTmpl">
  <xsl:param name="laIsProp" />
  <xsl:param name="answerId" />

  <xsl:variable name="dataIdPrefix" select="substring-before($answerId, '_DATA')"/>

  <table class="data">
    <xsl:if test="not($laIsProp = 'true')">
      <col width="84"/>
    </xsl:if>
    <col width="136"/>
    <col width="110"/>
    <col width="149"/>
    <col width="211"/>
    <tr>
      <xsl:if test="not($laIsProp = 'true')">
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="th">For</span>
          </p>
        </td>
      </xsl:if>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Country</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">City</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Duration (days per year)</span>
        </p>
      </td>
      <td class="tdAns">
        <p class="thAnsCenter">
          <span lang="EN-HK" class="th">Reason</span>
        </p>
      </td>
    </tr>

    <xsl:if test="not($laIsProp = 'true')">
      <xsl:for-each select="/root/insured">
        <xsl:for-each select="insurability/*[local-name() = $answerId]">
          <tr class="odd">
            <xsl:if test="position() = 1">
              <td>
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="count(/root/proposer/insurability/*[local-name() = $answerId]) > 0">
                      <xsl:value-of select="'tdFor'"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'tdFor bottom'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="rowspan">
                  <xsl:value-of select="last()"/>
                </xsl:attribute>
                <p>
                  <span lang="EN-HK" class="person">Life Assured</span>
                </p>
              </td>
            </xsl:if>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'a')]"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'b')]"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <p class="tdAns">
                <span lang="EN-HK" class="tdAns">
                  <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'c')]"/>
                </span>
              </p>
            </td>
            <td class="tdAns">
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd1') or local-name()='permanent') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Permanent Residence
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd2') or local-name()='work') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Business / Work
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd3') or local-name()='study') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Study
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd4') or local-name()='cob') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Country of birth
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd5') or local-name()='family') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Place where family members or relatives residing
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd6') or local-name()='immigrate') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Immigration
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd7') or local-name()='visit') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>Visit family/relatives/friends
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd8') or local-name()='other') and  . = 'Y']">
                <p class="tdAnsLeft">
                  <span lang="EN-HK" class="tdAns">
                    <xsl:copy-of select="$tick"/>
                    <xsl:choose>
                      <xsl:when test="./*[local-name()=concat($dataIdPrefix, 'd8_OTH')]">
                        <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'd8_OTH')]"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./*[local-name()='resultOther']"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </p>
              </xsl:if>
            </td>
          </tr>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:if>

    <xsl:for-each select="/root/proposer/insurability/*[local-name() = $answerId]">
      <tr class="even">
        <xsl:if test="not($laIsProp = 'true') and position() = 1">
          <td class="tdFor bottom">
            <xsl:attribute name="rowspan">
              <xsl:value-of select="last()"/>
            </xsl:attribute>
            <p class="tdAns">
              <span lang="EN-HK" class="person">Proposer</span>
            </p>
          </td>
        </xsl:if>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'a')]"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'b')]"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <p class="tdAns">
            <span lang="EN-HK" class="tdAns">
              <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'c')]"/>
            </span>
          </p>
        </td>
        <td class="tdAns">
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd1') or local-name()='permanent') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Permanent Residence
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd2') or local-name()='work') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Business / Work
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd3') or local-name()='study') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Study
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd4') or local-name()='cob') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Country of birth
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd5') or local-name()='family') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Place where family members or relatives residing
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd6') or local-name()='immigrate') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Immigration
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd7') or local-name()='visit') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>Visit family/relatives/friends
              </span>
            </p>
          </xsl:if>
          <xsl:if test="./*[(local-name()=concat($dataIdPrefix, 'd8') or local-name()='other') and  . = 'Y']">
            <p class="tdAnsLeft">
              <span lang="EN-HK" class="tdAns">
                <xsl:copy-of select="$tick"/>
                <xsl:choose>
                  <xsl:when test="./*[local-name()=concat($dataIdPrefix, 'd8_OTH')]">
                    <xsl:value-of select="./*[local-name()=concat($dataIdPrefix, 'd8_OTH')]"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="./*[local-name()='resultOther']"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </xsl:if>
        </td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>



<!-- Below here are codes copied from appform_report_common_shield.-->

<xsl:variable name="space">
  <xsl:value-of select="'&#160;'"/>
</xsl:variable>


<xsl:variable name="checkboxTick">
  <span style="margin-right: 6px;">
    <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABOklEQVRYR+2W7W3CMBCGn0xAN2iZgHaDMkFHgA0QE0AnKCN0k9IJ2m4AG7AB6JUSyVhHc8KX5E/8K7Lse5/78OUqBl7VwPqMALcisAPegKegFB2AT+A9t2cBSHwVJJybEcA23bQATsAEeAF+g0CegR9AkZi2AZzrA9EFatq1REaAiAg8AB911SvvWr2lQOJfgArvG3jtEyAV/6vF9ap6icB/4kUATT7XQONN3h7axIsA1EIXdVOaGxAe8SIACeyBmQHhFS8C0GULQvtNtecFZ3Xw4meYQ0hET80jXhyBxqMUQnte8TCANB36VpO59TLyNBSnIDWoSGh5xUMjYBWYZy80Ah7BTlPQKUCXI9kxH3StiUhD4+YeFx13XEOp7AhiCTw6jHqOyHP9U64mYl2MHjw9MFdnRoDBI3ABXmlgIXxZe+kAAAAASUVORK5CYII="/>
  </span>
</xsl:variable>


<xsl:variable name="checkboxUntick">
  <span style="margin-right: 6px;">
    <img class="checkboxIcon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqUlEQVRYR+2WwQ2CUBAFhwosQexAKrEFO7AEtQNLoCMsQTuwA81LOMjPEoNZ8i+PI4Hd+QOHaah8NZX3Y4A5AzfgALRJn+gB9MC1nBcBaPkpaXE5RgCX75sRwAvYAB1wTwLZAwMgE7tfAO/xgewfNJwbLTGADdiADdiADdiADVQ3sGaSPcvQjYpI0XhOasG/olQvCeIIbJNAdHJl+aSINTs7PBfzGqC6gQ8cPDYhnSgMLQAAAABJRU5ErkJggg=="/>
  </span>
</xsl:variable>


<xsl:template name="comSecHeaderCols01">
  <xsl:param  name="sectionHeader" />

  <div class="sectionHeader">
    <div class="title">
      <p class="header">
        <span lang="EN-HK" class="sectionHeader">
          <xsl:value-of select="$sectionHeader"/>
        </span>
      </p>
    </div>
  </div>
</xsl:template>


<xsl:template name="comSecHeaderCols02WideCol01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="sectionSubHeader"/>
  <xsl:param  name="col02Text" />

  <td class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
      <xsl:if test="string-length($sectionSubHeader) > 0">
        <span lang="EN-HK" class="sectionHeader normal">
          <xsl:value-of select="$sectionSubHeader"/>
        </span>
      </xsl:if>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col02Text"/>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="comSecTableRowCols02WideCol01">
  <xsl:param  name="col01Text"/>
  <xsl:param  name="col02Text"/>
  <xsl:param  name="col01Align"/>
  <xsl:param  name="col01PaddingLeft"/>
  <xsl:param  name="col01IsBold"/>
  <xsl:param  name="col01IsRed"/>
  <xsl:param  name="col01WithCheckbox"/>

  <xsl:param  name="col02WithCheckbox"/>
  <xsl:param  name="col02CheckboxVal"/>
  <xsl:param  name="isHideCol02Text"/>



  <!--
  col01Align - optional
  0   = align left; default; if null; will always align left
  1   = align center;
  2   = align right

  col01PaddingLeft - optional

  col01IsRed      - optional
  col01IsBold     - optional
  isHideCol02Text - optional
  -->

  <td style="padding-left: 7.2pt;">

    <xsl:variable name="styleTextAlign">
      <xsl:choose>
        <xsl:when test="not($col01Align)">
          <xsl:value-of select="'text-align: left;'"/>
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$col01Align = '1'">
              <xsl:value-of select="'text-align: center;'"/>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="'text-align: right;'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="stylePaddingLeft">
      <xsl:choose>
        <xsl:when test="string-length($col01PaddingLeft) > 0">
          <xsl:value-of select="concat('padding-left: ', $col01PaddingLeft, ';')"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <p class="normal">
      <xsl:attribute name="style">
        <xsl:value-of select="concat($styleTextAlign, $stylePaddingLeft)"/>
      </xsl:attribute>

      <xsl:if test="$col01WithCheckbox = 1">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>
      </xsl:if>

      <span lang="EN-HK">
        <xsl:attribute name="class">
          <xsl:value-of select="'question '"/>
          <xsl:if test="$col01IsBold"><xsl:value-of select="'bold '"/></xsl:if>
          <xsl:if test="$col01IsRed"><xsl:value-of select="'red '"/></xsl:if>
        </xsl:attribute>

        <xsl:value-of select="$col01Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="tdLast padding">
    <xsl:if test="$col02WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col02CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="not($isHideCol02Text = 'true') and string-length($col02Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col02Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="comSecHeaderCols03WideCol01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="sectionSubHeader"/>
  <xsl:param  name="col02Text" />
  <xsl:param  name="col03Text" />

  <td class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
      <xsl:if test="string-length($sectionSubHeader) > 0">
        <span lang="EN-HK" class="sectionHeader normal">
          <xsl:value-of select="$sectionSubHeader"/>
        </span>
      </xsl:if>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col02Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="headerRow"
    style="border: solid windowtext 1.0pt; border-left: none;  padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$col03Text"/>
      </span>
    </p>
  </td>

</xsl:template>


<xsl:template name="comSecTableRowCols03WideCol01">
  <xsl:param  name="col01Text"/>
  <xsl:param  name="col02Text"/>
  <xsl:param  name="col03Text"/>
  <xsl:param  name="col01Align"/>
  <xsl:param  name="col01PaddingLeft"/>
  <xsl:param  name="col01IsBold"/>
  <xsl:param  name="col01IsRed"/>
  <xsl:param  name="col01WithCheckbox"/>

  <xsl:param  name="col02WithCheckbox"/>
  <xsl:param  name="col02CheckboxVal"/>
  <xsl:param  name="isHideCol02Text"/>

  <xsl:param  name="col03WithCheckbox"/>
  <xsl:param  name="col03CheckboxVal"/>


  <!--
  col01Align - optional
  0   = align left; default; if null; will always align left
  1   = align center;
  2   = align right

  col01PaddingLeft - optional

  col01IsRed - optional
  col01IsBold - optional
  isHideCol02Text - optional
  -->

  <td style="padding-left: 7.2pt;">

    <xsl:variable name="styleTextAlign">
      <xsl:choose>
        <xsl:when test="not($col01Align)">
          <xsl:value-of select="'text-align: left;'"/>
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$col01Align = '1'">
              <xsl:value-of select="'text-align: center;'"/>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="'text-align: right;'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="stylePaddingLeft">
      <xsl:choose>
        <xsl:when test="string-length($col01PaddingLeft) > 0">
          <xsl:value-of select="concat('padding-left: ', $col01PaddingLeft, ';')"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <p class="normal">
      <xsl:attribute name="style">
        <xsl:value-of select="concat($styleTextAlign, $stylePaddingLeft)"/>
      </xsl:attribute>

      <xsl:if test="$col01WithCheckbox = 1">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>
      </xsl:if>

      <span lang="EN-HK">
        <xsl:attribute name="class">
          <xsl:value-of select="'question '"/>
          <xsl:if test="$col01IsBold"><xsl:value-of select="'bold '"/></xsl:if>
          <xsl:if test="$col01IsRed"><xsl:value-of select="'red '"/></xsl:if>
        </xsl:attribute>

        <xsl:value-of select="$col01Text"/>
      </span>
    </p>
  </td>

  <td width="99" class="" style = "padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$col02WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col02CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="not($isHideCol02Text = 'true') and string-length($col02Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col02Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>

  <td width="99" class="tdLast padding">
    <xsl:if test="$col03WithCheckbox = 1">
      <xsl:call-template  name  ="comCheckbox">
        <xsl:with-param   name  ="isChecked"
                          select="$col03CheckboxVal"/>
      </xsl:call-template>
    </xsl:if>

    <p class = "userData">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($col03Text) = 0">-</xsl:when>
          <xsl:otherwise><xsl:value-of select="$col03Text"/></xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
</xsl:template>


<xsl:template name="comCheckbox">
  <xsl:param  name="isChecked" />
  <!--
  col01Align - optional
  0   = not checked
  1   = checked;
  -->
  <xsl:choose>
    <xsl:when test="$isChecked = '1'">
      <xsl:copy-of select="$checkboxTick"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy-of select="$checkboxUntick"/>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>


<xsl:template name="comCheckboxWithText">
  <xsl:param  name="text" />
  <tr>
    <td style = "text-align:left; border:none">
      <p class="userData">
        <xsl:call-template  name  ="comCheckbox">
          <xsl:with-param   name  ="isChecked"
                            select="1"/>
        </xsl:call-template>

        <span lang="EN-HK" class="tdAns">
          <xsl:value-of select="$text"/>
        </span>
      </p>
    </td>
  </tr>

</xsl:template>


<xsl:template name="comSecHeaderNumCols01">
  <xsl:param  name="sectionHeader" />
  <xsl:param  name="colspan" />

  <td width="714" class="headerRow"
    style="border: solid windowtext 1.0pt; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
    <xsl:if test="$colspan">
      <xsl:attribute name="colspan">
        <xsl:value-of select="$colspan"/>
      </xsl:attribute>
    </xsl:if>
    <p class="header">
      <span lang="EN-HK" class="sectionHeader">
        <xsl:value-of select="$sectionHeader"/>
      </span>
    </p>
  </td>
</xsl:template>





</xsl:stylesheet>
