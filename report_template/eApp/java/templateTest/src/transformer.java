import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.json.JSONException;
import org.json.JSONObject;

public class transformer {
	private static String strXML = "";
	private static String strXSLT = "";
    public static void main(String[] args) throws Exception{
    	String basePath = "";
    	String htmlFile = "";
    	String xmlFile = "";
    	boolean replaceIncludePath = false;
    	String outputFile = "";
    	
    	if (args.length > 2) {
    		basePath = args[0];
    		htmlFile = args[1];
        	xmlFile = args[2];
        	
        	if (args.length > 3) {
        		replaceIncludePath = args[3].equals("R");
        	}
    	} else {
    		basePath = System.getProperty("user.dir") + "/../../";
    		htmlFile = "appform_report_shield_main.html";
        	xmlFile = "appForm.xml";
//        	htmlFile = "appform_report_payment_main.html";
//        	xmlFile = "premiumPayment.xml";
//        	htmlFile = "appform_report_ecpd_main.html";
//        	xmlFile = "ecpd.xml";
    	}
    	outputFile = "output_" + htmlFile;
    	
    	File currfolder = new File(basePath);
    	System.out.println("basePath: " + currfolder.getAbsolutePath());
    	System.out.println("replaceIncludePath: " + replaceIncludePath);
    	
		if (strXML.equals("") || strXSLT.equals("")) {
			System.out.println("get xml and xsl from file....");
			long time1 = System.currentTimeMillis();
			long time2;
			
			int length = -1; 
			char[] cs = new char[1024]; 
			FileInputStream fis = new FileInputStream(basePath+"/"+xmlFile);
			InputStreamReader reader = new InputStreamReader(fis,"UTF-8");
			while ((length = reader.read(cs)) != -1) {
				strXML += new String(cs,0,length);
			}

			reader.close();
			fis = new FileInputStream(basePath+"/"+htmlFile);
			reader = new InputStreamReader(fis,"UTF-8");
			while ((length = reader.read(cs)) != -1) {
				strXSLT += new String(cs,0,length)	;
			}
			
			if (replaceIncludePath) {
				int findIndex = 0;
				String searchStr = "<xsl:include href=\"";
				while (findIndex >= 0 && findIndex < strXSLT.length()) {
					findIndex = strXSLT.indexOf(searchStr, findIndex + 1);
					
					if (findIndex > 0) {
						
						int splitIndex = findIndex + searchStr.length();
						String splitBefore = strXSLT.substring(0, splitIndex);
						String splitAfter = strXSLT.substring(splitIndex, strXSLT.length());
						
						strXSLT = splitBefore + currfolder.getAbsolutePath() + "/" + splitAfter;
					}
				}
//				System.out.println(strXSLT);
			}
			

			fis.close();
			reader.close();
			time2 = System.currentTimeMillis();
			System.out.println("time="+(time2-time1)+"ms");
			
		}
     
		String xml = strXML;
        String html = trans(xml, strXSLT);      
        
        File file = new File(basePath+"/" + outputFile);
	 
		try (FileOutputStream fop = new FileOutputStream(file)) {
			if (!file.exists()) {
				file.createNewFile();
			}
			byte[] contentInBytes = html.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}	 
        
    }
    
    public static String trans(String xml, String xslt) {
        String retVal = "";
        try {
            StringReader reader = new StringReader(xml);
            StringWriter writer = new StringWriter();
            TransformerFactory factory = TransformerFactory	.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(new ByteArrayInputStream(xslt.getBytes("UTF-8"))));
            transformer.transform(new StreamSource(reader), new StreamResult(writer));
            return writer.toString();
        } catch (Exception ex) {
            System.out.println("Exception in transformXml:" + ex.getMessage());
        }
        return retVal;
    }
 
    public static String jsonToXml(String jsonStr){
        JSONObject jsonObj;
        String xml = "";
        try {
            jsonObj = new JSONObject(jsonStr);
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + org.json.XML.toString(jsonObj);
            System.out.println("xml:" + xml);
        } catch (JSONException e) {
            System.out.println("jsonToXml"); 
            e.printStackTrace();	
        } 
        
        return xml;
    }
}

