<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroup">
      <col width="50%"/>
      <col width="25%"/>
      <col width="25%"/>
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="3"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Basic Plan</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Total Premium Amount (including riders, if any)</span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <xsl:for-each select="/root/planDetails/displayPlanList">
            <td class="tdFirst padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./saViewInd = 'Yes'">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./sumInsured"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./totalPremium"/>
                </span>
              </p>
            </td>
          </xsl:for-each>
        </tr>
        <xsl:if test="/root/planDetails/hasRiders = 'Y'">
          <tr class="dataGroup">
            <td class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Rider</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Term</span>
              </p>
            </td>
          </tr>
          <xsl:for-each select="/root/planDetails/riderList">
            <tr class="dataGroup">
              <xsl:attribute name="class">
                <xsl:choose>
                  <xsl:when test="position() mod 2 = 0">
                    <xsl:value-of select="'odd'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'even'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <td class="tdFirst padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:choose>
                      <xsl:when test="/root/lang = 'en'">
                        <xsl:value-of select="./covName/en"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./covName/zh-Hant"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:choose>
                      <xsl:when test="./saViewInd = 'Yes'">-</xsl:when>
                      <xsl:when test="not(./sumInsured)">-</xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./sumInsured"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </p>
              </td>
              <td class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./polTermDesc"/>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:for-each>
        </xsl:if>
      </tbody>
    </table>

    <table class="dataGroup">
      <col width="25%"/>
      <col width="25%"/>
      <col width="25%"/>
      <col width="25%"/>
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
            <xsl:with-param name="colspan" select="4"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Currency</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/ccy"/>
              </span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payment Mode</span>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/paymentMode"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Basic Benefit</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/basicBenefit"/>
              </span>
            </p>
          </td>
          <td class="tdMid"><p class="title"></p></td>
          <td class="tdLast padding"><p class="answer"></p></td>
        </tr>
        <tr class="dataGroup">
          <xsl:choose>
            <xsl:when test="(/root/planDetails/topUpAmt = 'null')">
              <td class="tdFirst padding">
                <p class="title">
                    <span lang="EN-HK" class="questionItalic">Insurance Charge</span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/planDetails/insuranceCharge"/>
                  </span>
                </p>
              </td>
              <td class="tdMid">
              </td>
              <td class="tdLast padding">
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td class="tdFirst padding">
                <p class="title">
                    <span lang="EN-HK" class="questionItalic">Top-Up Amount</span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/planDetails/topUpAmt"/>
                  </span>
                </p>
              </td>
              <td class="tdMid">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Insurance Charge</span>
                </p>
              </td>
              <td class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/planDetails/insuranceCharge"/>
                  </span>
                </p>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </tr>
        <xsl:if test="not(/root/planDetails/rspAmount = 'null') and not(/root/planDetails/rspPayFreq = 'null')">
          <tr class="dataGroup">
            <td class="tdOneCol" colspan="4">
              <p class="title subHeader">
                <span lang="EN-HK" class="questionItalic">Recurring Single Premium Details (RSP):</span>
              </p>
            </td>
          </tr>
          <tr class="dataGroup">
            <td class="tdFirst padding">
              <p class="title">
                  <span lang="EN-HK" class="questionItalic">RSP Amount</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/planDetails/rspAmount"/>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">RSP Payment Mode</span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/planDetails/rspPayFreq"/>
                </span>
              </p>
            </td>
          </tr>
          <xsl:if test="not(/root/policyOptions/rspSalesCharges = 'null') and /root/policyOptions/rspSalesCharges != ''">
            <tr class="dataGroup">
              <td class="tdFirst padding">
                <p class="title">
                    <span lang="EN-HK" class="questionItalic">RSP Sales Charge</span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/policyOptions/rspSalesCharges"/>
                  </span>
                </p>
              </td>
              <td class="tdLast padding" colspan="2">
                <p class="title">
                    <span lang="EN-HK" class="questionItalic"></span>
                </p>
              </td>
            </tr>
          </xsl:if>
        </xsl:if>
      </tbody>
    </table>

    <table class="dataGroup">
      <col width="50%"/>
      <col width="25%"/>
      <col width="25%"/>
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Fund Details'"/>
            <xsl:with-param name="colspan" select="3"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <xsl:choose>
            <xsl:when test="(/root/planDetails/topUpAmt = 'null')">
              <td colspan="2" class="tdFirst padding">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Fund Names</span>
                </p>
              </td>

              <td colspan="1" class="tdLast padding">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Fund Allocation (Regular Premium / RSP, as applicable)</span>
                </p>
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td colspan="1" class="tdFirst padding">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Fund Names</span>
                </p>
              </td>

              <td colspan="1" class="tdMid">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Fund Allocation (Regular Premium / RSP, as applicable)</span>
                </p>
              </td>

              <td colspan="1" class="tdLast padding">
                <p class="title">
                  <span lang="EN-HK" class="questionItalic">Fund Allocation (Top-Up Premium)</span>
                </p>
              </td>
            </xsl:otherwise>
          </xsl:choose>

        </tr>
        <xsl:for-each select="/root/planDetails/fundList">
          <tr class="dataGroup">
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                  <xsl:value-of select="'odd'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'even'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>

            <xsl:choose>
              <xsl:when test="(/root/planDetails/topUpAmt = 'null')">
                <td colspan="2" class="tdFirst padding">
                  <p class="answer">
                    <span lang="EN-HK" class="answer">
                      <xsl:value-of select="./fundName"/>
                    </span>
                  </p>
                </td>

                <td colspan="1" class="tdLast padding">
                  <p class="answer">
                    <span lang="EN-HK" class="answer">
                      <xsl:choose>
                        <xsl:when test="contains(./alloc, '%')">
                          <xsl:value-of select="./alloc"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="concat(./alloc, '%')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </p>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td colspan="1" class="tdFirst padding">
                  <p class="answer">
                    <span lang="EN-HK" class="answer">
                      <xsl:value-of select="./fundName"/>
                    </span>
                  </p>
                </td>

                <td colspan="1" class="tdMid">
                  <p class="answer">
                    <span lang="EN-HK" class="answer">
                      <xsl:choose>
                        <xsl:when test="contains(./alloc, '%')">
                          <xsl:value-of select="./alloc"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="concat(./alloc, '%')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </p>
                </td>

                <td colspan="1" class="tdLast padding">
                  <p class="answer">
                    <span lang="EN-HK" class="answer">
                      <xsl:choose>
                        <xsl:when test="contains(./topUpAlloc, '%')">
                          <xsl:value-of select="./topUpAlloc"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="concat(./topUpAlloc, '%')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </p>
                </td>

              </xsl:otherwise>
            </xsl:choose>
          </tr>
        </xsl:for-each>
        <tr class="dataGroup">
          <td class="tdFirst padding" style="border-bottom: solid windowtext 1.0pt;"></td>
          <td class="tdMid" style="border-bottom: solid windowtext 1.0pt;"></td>
          <td class="tdLast padding" style="border-bottom: solid windowtext 1.0pt;"></td>
        </tr>
      </tbody>
    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
