<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



<xsl:template name="appform_awt_plan_details">
  <xsl:param  name="nodePlan"/>

  
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroupLast">
      <colgroup>
        <col style="width:28%"/>
        <col style="width:18%"/>
        <col style="width:22%"/>
        <col style="width:32%"/>
      </colgroup>

      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'AXA Wealth Treasure'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>
      
      <!-- Plan Name-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Plan Name</span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Premium Term</span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Total Premium Amount</span>
          </p>
        </td>
      </tr>

      <!-- AXA Wealth Treasure-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/covName/en"/>
            </span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/premTermDesc"/>
            </span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/premium"/>
            </span>
          </p>
        </td>
      </tr>

      <!-- Other Plan Details-->
      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'Other Plan Details'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>

      <!-- Policy Currency-->
      <tr>
        <td style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Policy Currency</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/ccy"/>
            </span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Payment Mode</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/paymentMode"/>
            </span>
          </p>
        </td>
      </tr>

      <!-- Death Benefit-->
      <tr class = "even">
        <td style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Death Benefit</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/deathBenefit"/>
            </span>
          </p>
        </td>

        <xsl:choose>
          <xsl:when test="$nodePlan/deathBenefit= 'Enhanced'">
            <td>
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Insurance Charge</span>
              </p>
            </td>

            <td>
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="$nodePlan/insuranceCharge"/>
                </span>
              </p>
            </td>
          </xsl:when>

          <xsl:otherwise>
            <td></td>
            <td></td>
          </xsl:otherwise>
        </xsl:choose>
      </tr>

	    <xsl:variable name="tempStr1">
        <xsl:choose>
          <xsl:when test="not($nodePlan/topUpAmt = 'null')">
            <xsl:value-of select="'.'"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
	  
      <!-- Top-Up Amount-->
      <xsl:if test="not($nodePlan/topUpAmt = 'null')">
        <tr> <!-- This is always an odd row -->
          <td style="padding-left: 7.2pt;">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Top-Up Amount</span>
            </p>
          </td>

          <td colspan = "3">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$nodePlan/topUpAmt"/>
              </span>
            </p>
          </td>  
        </tr>
      </xsl:if>

      <xsl:variable name="tempStr2">
        <xsl:choose>
          <xsl:when test="not($nodePlan/wdAmount = 'null')">
            <xsl:value-of select="concat($tempStr1, '.')"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$tempStr1"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="tempStr3">
        <xsl:choose>
          <xsl:when test="not($nodePlan/wdAmount = 'null')">
            <xsl:value-of select="concat($tempStr2, '.')"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$tempStr2"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="tempStr4">
        <xsl:choose>
          <xsl:when test="not($nodePlan/wdAmount = 'null')">
            <xsl:value-of select="concat($tempStr3, '.')"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$tempStr3"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>


      <!-- Regular Withdrawal Details-->
      <xsl:if test="not($nodePlan/wdAmount = 'null')">
        <tr>
          <xsl:if test ="string-length($tempStr2) mod 2 = 0">
            <xsl:attribute name="class">
              <xsl:value-of select="'even'"/>
            </xsl:attribute> 
          </xsl:if>

          <td colspan = "4" style="padding-left: 7.2pt;">
            <p class="title subHeader">
              <span lang="EN-HK" class="questionItalic">Regular Withdrawal Details:</span>
            </p>
          </td>
        </tr>

        <!-- Amount-->
        <tr>
          <xsl:if test ="string-length($tempStr3) mod 2 = 0">
            <xsl:attribute name="class">
              <xsl:value-of select="'even'"/>
            </xsl:attribute> 
          </xsl:if>

          <td style="padding-left: 7.2pt;">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Amount</span>
            </p>
          </td>

          <td>
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$nodePlan/wdAmount"/>
              </span>
            </p>
          </td>

          <td>
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payment Frequency</span>
            </p>
          </td>

          <td>
            <xsl:variable       name="wdFrequency">
              <xsl:if test="$nodePlan/wdFreq = 'M'">
                <xsl:value-of select="'Monthly'"/>
              </xsl:if>
              
              <xsl:if test="$nodePlan/wdFreq = 'Q'">
                <xsl:value-of select="'Monthly'"/>
              </xsl:if>

              <xsl:if test="$nodePlan/wdFreq = 'S'">
                <xsl:value-of select="'Semi-Annual'"/>
              </xsl:if>

              <xsl:if test="$nodePlan/wdFreq = 'A'">
                <xsl:value-of select="'Annual'"/>
              </xsl:if>           
            </xsl:variable>

            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$wdFrequency"/>
              </span>
            </p>
          </td>
        </tr>

        <!-- From Age-->
        <tr>
          <xsl:if test ="string-length($tempStr4) mod 2 = 0">
            <xsl:attribute name="class">
              <xsl:value-of select="'even'"/>
            </xsl:attribute> 
          </xsl:if>

          <td style="padding-left: 7.2pt;">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">From Age</span>
            </p>
          </td>

          <td>
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$nodePlan/wdFromAge"/>
              </span>
            </p>
          </td>

          <td>
            <p class="title">
              <span lang="EN-HK" class="questionItalic">To Age</span>
            </p>
          </td>

          <td>
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$nodePlan/wdToAge"/>
              </span>
            </p>
          </td>
        </tr>
      </xsl:if>
      
      <xsl:variable name="tempStr5">
        <xsl:choose>
          <xsl:when test="not($nodePlan/rspAmount = 'null')">
            <xsl:value-of select="concat($tempStr4, '.')"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$tempStr4"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="tempStr6">
        <xsl:choose>
          <xsl:when test="not($nodePlan/rspAmount = 'null')">
            <xsl:value-of select="concat($tempStr5, '.')"/>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$tempStr5"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>


      <!-- Recurring Single Premium (RSP) Details:-->
      <xsl:if test="not($nodePlan/rspAmount = 'null')">
        <tr>
          <xsl:if test ="string-length($tempStr5) mod 2 = 0">
            <xsl:attribute name="class">
              <xsl:value-of select="'even'"/>
            </xsl:attribute> 
          </xsl:if>

          <td colspan = "4" style="padding-left: 7.2pt;">
            <p class="title subHeader">
              <span lang="EN-HK" class="questionItalic">Recurring Single Premium (RSP) Details:</span>
            </p>
          </td>
        </tr>

        <!-- RSP Amount-->
        <tr>
          <xsl:if test ="string-length($tempStr6) mod 2 = 0">
            <xsl:attribute name="class">
              <xsl:value-of select="'even'"/>
            </xsl:attribute> 
          </xsl:if>

          <td style="padding-left: 7.2pt;">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">RSP Amount</span>
            </p>
          </td>

          <td>
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$nodePlan/rspAmount"/>
              </span>
            </p>
          </td>

          <td>
            <p class="title">
              <span lang="EN-HK" class="questionItalic">RSP Payment Mode</span>
            </p>
          </td>

          <td>
            <xsl:variable       name="rspFrequency">
              <xsl:if test="$nodePlan/rspPayFreq = 'M'">
                <xsl:value-of select="'Monthly'"/>
              </xsl:if>
              
              <xsl:if test="$nodePlan/rspPayFreq = 'Q'">
                <xsl:value-of select="'Monthly'"/>
              </xsl:if>

              <xsl:if test="$nodePlan/rspPayFreq = 'S'">
                <xsl:value-of select="'Semi-Annual'"/>
              </xsl:if>

              <xsl:if test="$nodePlan/rspPayFreq = 'A'">
                <xsl:value-of select="'Annual'"/>
              </xsl:if>
            </xsl:variable>

            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="$rspFrequency"/>
              </span>
            </p>
          </td>
        </tr>
      </xsl:if>

      <!-- Fund Details-->
      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'Fund Details'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>

      <!-- Fund Name-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Fund Name</span>
          </p>
        </td>

        <xsl:choose>
          <xsl:when test="not($nodePlan/topUpAmt = 'null')">
            <td>
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Fund Allocation 
                (Regular Premium or RSP, as applicable)</span>
              </p>
            </td>

            <td>
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Fund Allocation<br/> 
                (Top-Up Amount)
                </span>
              </p>
            </td>
          </xsl:when>

          <xsl:otherwise>
            <td colspan="2">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Fund Allocation 
                (Regular Premium or RSP, as applicable)</span>
              </p>
            </td>
          </xsl:otherwise>

        </xsl:choose>
      </tr>

      <xsl:for-each select="$nodePlan/fundList">
        <!-- Fund Row-->
        
        <xsl:choose>
          <xsl:when test="position() mod 2 = 0">
            
            <tr class="">
              <!-- doesnt work
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">
                  <xsl:value-of select="even"/>
                </xsl:attribute>
              </xsl:if>
              -->

              <td colspan = "2" style="padding-left: 7.2pt;">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./fundName/en"/>
                  </span>
                </p>
              </td>

              <xsl:choose>
                <xsl:when test="not($nodePlan/topUpAmt = 'null')">
                  <td>
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="./alloc"/>%
                      </span>
                    </p>
                  </td>

                  <td>
                    <p class="answer">
                      <span lang="EN-HK" class="answer">   
                          <xsl:value-of select="./topUpAlloc"/>%
                      </span>
                    </p>
                  </td>
                </xsl:when>

                <xsl:otherwise>
                  <td colspan="2">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="./alloc"/>%
                      </span>
                    </p>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
          </xsl:when>

          <xsl:otherwise>
            
            <tr class="even">
              <!-- doesnt work
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">
                  <xsl:value-of select="even"/>
                </xsl:attribute>
              </xsl:if>
              -->

              <td colspan = "2" style="padding-left: 7.2pt;">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./fundName/en"/>
                  </span>
                </p>
              </td>

              <xsl:choose>
                <xsl:when test="not($nodePlan/topUpAmt = 'null')">
                  <td>
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="./alloc"/>%
                      </span>
                    </p>
                  </td>

                  <td>
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                          <xsl:value-of select="./topUpAlloc"/>%
                      </span>
                    </p>
                  </td>
                </xsl:when>

                <xsl:otherwise>
                  <td colspan="2">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="./alloc"/>%
                      </span>
                    </p>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:for-each>
      


    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
   
  </div>

  

</xsl:template>

</xsl:stylesheet>
