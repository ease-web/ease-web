<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



<xsl:template name="appform_shield_la_declaration">
  <xsl:param  name="node"/>
  <xsl:param  name="orgNode"/>

  <xsl:variable name="laFullName" select="$node/personalInfo/fullName"/>

  <!--Declaration; Bankruptcy -->  
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">DECLARATION</span>
    </p>
    
    <table class="dataGroupLast">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader" 
                            select="'Bankruptcy'"/>
          <xsl:with-param   name  ="sectionSubHeader" 
                            select="concat(' (', $laFullName, ')')"/>
          <xsl:with-param   name  ="col02Text" 
                            select="'Life Assured'"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'Are you an undischarged bankrupt?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/BANKRUPTCY01"/>
        </xsl:call-template>
      </tr>
    </table>
  </div>

  <!--Declaration; Declaration to Central Provident Fund Board (CPFB) -->
  <div class="section">
    <xsl:call-template  name  ="comSecHeaderCols01">
      <xsl:with-param   name  ="sectionHeader"    
                        select="'Declaration to Central Provident Fund Board (CPFB)'"/>
    </xsl:call-template>

    <div class="sectionContent">
      <div class="oneCol">
        <p class="statement">
          <span lang="EN-HK" class="statement">
            I, the Life to be Assured named under this application, hereby 
            consent to the transfer and disclosure, at any time and without 
            notice to me, of any medical information on me, in the Insurer’s or 
            the CPFB’s possession, between the Insurer and the CPFB, for the 
            purpose of assessing the insurability of me and/or the making of a 
            claim under the PMIS.
            <br/>
            <br/>
            
            Subject to the relevant laws and terms and conditions, I understand that:
            <br/>
            (i) Upon the commencement of this AXA Shield cover, any other
             existing Integrated Shield Plan (if any) under the PMIS in favour 
             of the Life to be Assured shall automatically terminate; and
            <br/>
            (ii) Upon the commencement of another Integrated Shield Plan in favour 
            of the Life to be Assured, this AXA Shield cover of the Life to be 
            Assured shall automatically terminate. 
            <br/>
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol first">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          I declare that:
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          1. To the best of my knowledge and belief that the information given 
          by me to AXA Insurance Pte Ltd or its Medical Examiner is true and 
          complete and that no material facts such as facts likely to influence 
          the assessment and acceptance of this proposal have been withheld.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          2. I, the Life to be Assured, authorize any medical source, insurance 
          office or organization, to release to AXA Insurance Pte Ltd and AXA 
          Insurance Pte Ltd to release to any medical source, insurance office 
          or organization, any relevant information concerning me or ourselves, 
          at any time, irrespective of whether the proposal is accepted by AXA 
          Insurance Pte Ltd. A photocopy of this authorization shall be as valid 
          as the original.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          3a. I agree that payment of premium before acceptance of this proposal 
          by AXA Insurance Pte Ltd does not commit AXA Insurance Pte Ltd to issue 
          the policy I have applied for and the said policy shall not take effect 
          unless and until this proposal has been fully accepted and the full 
          initial premium has been paid during my life or our lives and good health.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          3b. I agree to inform AXA Insurance Pte Ltd if there is any change in 
          the state of health, occupation or activity of the Life Assured, 
          between the date of this proposal or medical examination and the issue
           of my policy. On receiving this information AXA Insurance Pte Ltd is 
           entitled to accept or reject my proposal. The last signature date on 
           the proposal form(s) shall be construed as the date of proposal.
           <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          4. My financial consultant has advised me that all Singapore Citizens 
          and Permanent Residents will be covered by MediShield Life, regardless 
          of my decision on an Integrated Shield Plan. An Integrated Shield Plan 
          comprises two parts – a MediShield Life portion provided by the Central 
          Provident Fund Board (CPFB) and an additional private insurance 
          coverage portion provided by the Insurance Company. As Integrated 
          Shield Plan premiums are higher than MediShield Life premiums, there 
          should be sufficient monies in my Medisave account(s) or I/we should 
          have enough cash to pay for MediShield Life premiums on an ongoing 
          basis before I/we consider purchasing an Integrated Shield Plan.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          5a. The information I have provided is my personal data and, where it 
          is not my personal data, that I have the consent of the owner of such 
          personal data to provide such information.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <p class="statement">
        <span lang="EN-HK" class="statement">
          5b. By providing this information, I understand and give my consent 
          for AXA Insurance Pte Ltd and its representatives or agents to:
          <br/>
        </span>
      </p>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <div class="indentLeft">
        <p class="statement">
          <span lang="EN-HK" class="statement">
            i. Collect, use, store, transfer and/or disclose the information, 
            to or with all such persons (including any member of the AXA Group 
            or any third party service provider, and whether within or outside 
            of Singapore) for the purpose of enabling AXA Insurance Pte Ltd to 
            provide me with services required of an insurance provider, 
            including the evaluating, processing, administering and/or managing 
            of my relationship and policy(ies) with AXA Insurance Pte Ltd, and 
            for the purposes set out in AXA Insurance Pte Ltd’s Data Use 
            Statement which can be found at http://www.axa.com.sg (“Purposes”).
            <br/>
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol">
      <div class="indentLeft">
        <p class="statement">
          <span lang="EN-HK" class="statement">
            ii. Collect, use, store, transfer and/or disclose personal data 
            about me and those whose personal data I have provided from sources 
            other than myself for the Purposes.
            <br/>
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol last">
      <p class="statement">
        <span lang="EN-HK" class="warningDeclaration">
          <br/>
          If a material fact is not disclosed in this proposal, any policy 
          issued may not be valid. If you are in doubt as to whether a fact is 
          material, you are advised to disclose it. <u>This includes any 
          information that you may have provided to the Financial Consultant 
          but was not included in the proposal.</u> Please check to ensure that you 
          are fully satisfied with the information declared in this proposal 
          before signing.
          <br/>
        </span>
      </p>
    </div>
  </div>

  <!--Financial Consultant’s Declaration -->
  <div class="section">
    <table class="dataGroup first last">
      <tr>
        <xsl:call-template  name  ="comSecHeaderCols02WideCol01">
          <xsl:with-param   name  ="sectionHeader" 
                            select="'Financial Consultant’s Declaration'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="'Life Assured'"/>
          <xsl:with-param   name  ="col02SubText" 
                            select="concat(' (', $laFullName, ')')"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'1. How long have you known your customer?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/FCD_01"/>
        </xsl:call-template>
      </tr>

      <tr>
        <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
          <xsl:with-param   name  ="col01Text" 
                            select="'2. Are you related to your customer?'"/>
          <xsl:with-param   name  ="col02Text" 
                            select="$node/declaration/FCD_02"/>
        </xsl:call-template>
      </tr>

      <xsl:if test="$node/declaration/FCD_02 = 'Yes'">
        <tr>
          <xsl:call-template  name  ="comSecTableRowCols02WideCol01">
            <xsl:with-param   name  ="col01Text" 
                              select="'3. Relationship to customer'"/>
            <xsl:with-param   name  ="col02Text" 
                              select="$node/declaration/FCD_03"/>
          </xsl:call-template>
        </tr>
      </xsl:if>



    </table>
  </div>

  <div class="sectionContent">
    <div class="oneCol last" style = "padding-left:25px;">
      <p class="normal">
        <br/>
        <ul class="statement">
          <li>I, Financial Consultant, certify that the proof of Identity/(ies) 
          uploaded and submitted is(are) true copy(ies) of the original(s).
          </li>
          <li class="red">I declare that all answers provided to me by the Proposer or Life 
          Assured are declared in the proposal. I have not withheld any other 
          information which may influence the acceptance of this proposal by the 
          Company.
          </li>
          <li>I have not given any statement to the Proposer or Life Assured 
          which is contrary to the provision given in the Company’s standard policy.
          </li>
          <li class="red">I confirm that the Proposer or Life Assured have fully understood 
          all questions and answers in the Proposal form. I have checked and 
          verified the personal particulars given against their original NRIC or 
          Passport or Birth certificate and confirm them to be true and correct.
          </li>
          <li>I have personally seen the Proposer or Life Assured and have 
          explained the terms of the insurance to him / her.
          </li>
        </ul>
      </p>
    </div>
  </div>

</xsl:template>


<xsl:template name="appform_shield_la_signature">
  <xsl:param  name="node"/>
  <xsl:param  name="orgNode"/>
  <xsl:param  name="pNode"/>
  <xsl:param  name="pOrgNode"/>

  <div class="section">
    <p><br/></p>
    <table  style= "width:740px; border-collapse: collapse;">
      <colgroup>
        <col style="width:45%"/>
        <col style="width:10%"/>
        <col style="width:45%"/>
      </colgroup>
      
      <tr>
        <td colspan = "3">
          <p class="normal">
            <span lang="EN-HK" class="signature">Signed and dated in Singapore</span>
          </p>
        </td>
      </tr>

      <tr>
        <td>
          <div class="signatureCell">
            <div>
                <span lang="EN-HK" class="signatureCell">ADVISOR_SIGNATURE</span>
            </div>
          </div>
        </td>
        
        <td >
        </td>
        
        <td>
          <div class="signatureCell">
            <div>
                <span lang="EN-HK" class="signatureCell">LA_SIGNATURE</span>
            </div>
          </div>
        </td>
      </tr>

      <tr>
        <td style = "text-align:center;">
          <p class="normal">
            <span lang="EN-HK" class="signature">Signature of Financial Consultant (Witness)</span>
          </p>
        </td>

        <td>
        </td>

        <td style = "text-align:center;">
          <p class="normal">
            <span lang="EN-HK" class="signature">
              Signature of Life Assured (Age 16 or above, otherwise Legal Guardian)<br/>
              <xsl:value-of select="$node/personalInfo/fullName"/>
            </span>
          </p>
        </td>
      </tr>

    </table>
  </div>

</xsl:template>



<xsl:template name="appform_report_declaration_la">
  <xsl:call-template    name  ="appform_shield_la_declaration">
    <xsl:with-param     name  ="node" select="/root/reportData/insured"/>
    <xsl:with-param     name  ="orgNode" select="/root/originalData/insured"/>
  </xsl:call-template>

  <xsl:call-template    name  ="appform_shield_la_signature">
    <xsl:with-param     name  ="node" select="/root/reportData/insured"/>
    <xsl:with-param     name  ="orgNode" select="/root/originalData/insured"/>
    <xsl:with-param     name  ="pNode" select="/root/reportData/proposer"/>
    <xsl:with-param     name  ="pOrgNode" select="/root/originalData/proposer"/>
  </xsl:call-template>
</xsl:template>


</xsl:stylesheet>
