<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="personalDetailsRowTmpl">
  <xsl:param name="titleLeft" />
  <xsl:param name="valueLeft" />
  <xsl:param name="titleRight" />
  <xsl:param name="valueRight" />
  <xsl:param name="isEmptyTitleLeft" />
  <xsl:param name="isEmptyTitleRight" />
  <xsl:param name="isEmptyValueLeft" />
  <xsl:param name="isEmptyValueRight" />

  <td width="23%" class="tdFirst padding">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$isEmptyTitleLeft = 'true'"></xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$titleLeft"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="27%" class="tdLast padding">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($valueLeft) > 0">
            <xsl:value-of select="$valueLeft"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="not($isEmptyValueLeft = 'true')">-</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="23%" class="tdFirst padding">
    <p class="title">
      <span lang="EN-HK" class="questionItalic">
        <xsl:choose>
          <xsl:when test="$isEmptyTitleRight = 'true'"></xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$titleRight"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>
  <td width="27%" class="tdLast padding">
    <p class="answer">
      <span lang="EN-HK" class="answer">
        <xsl:choose>
          <xsl:when test="string-length($valueRight) > 0">
            <xsl:value-of select="$valueRight"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="not($isEmptyValueRight = 'true')">-</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </p>
  </td>

</xsl:template>


<xsl:template name="personalInfoAddressTmpl">
  <xsl:param name="addrBlock" />
  <xsl:param name="addrStreet" />
  <xsl:param name="addrUnitNum" />
  <xsl:param name="addrEstate" />
  <xsl:param name="addrCity" />
  <xsl:param name="addrCountry" />
  <xsl:param name="addrPostalCode" />

  <p class="answer">
    <xsl:if test="$addrBlock">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrBlock"/>
      </span>
    </xsl:if>

    <xsl:if test="$addrStreet">
      <span lang="EN-HK" class="answer">, </span>
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrStreet"/>
      </span>
    </xsl:if>
  </p>

  <xsl:if test="$addrUnitNum or $addrEstate or string-length($addrUnitNum) > 0 or string-length($addrEstate) > 0">
    <p class="answer">
      <xsl:if test="string-length($addrUnitNum) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrUnitNum"/>
        </span>
        <span lang="EN-HK" class="answer">, </span>
      </xsl:if>
      <xsl:if test="string-length($addrEstate) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrEstate"/>
        </span>
      </xsl:if>
    </p>
  </xsl:if>

  <p class="answer">
    <xsl:choose>
      <xsl:when test="$addrCity and string-length($addrCity) > 0">
        <span lang="EN-HK" class="answer">
          <xsl:value-of select="$addrCity"/>
        </span>
        <span lang="EN-HK" class="answer">, </span>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$addrCountry">
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrCountry"/>
      </span>
    </xsl:if>
    <xsl:if test="$addrPostalCode">
      <span lang="EN-HK" class="answer">, </span>
      <span lang="EN-HK" class="answer">
        <xsl:value-of select="$addrPostalCode"/>
      </span>
    </xsl:if>
  </p>
</xsl:template>




<xsl:template name="appform_shield_ph_personal_details">

  <div>
    <p class="sectionGroup">
      <span class="sectionGroup">PERSONAL DETAILS</span>
    </p>


    <xsl:variable   name="proposerName">
      <xsl:value-of select ="/root/reportData/proposer/personalInfo/fullName"/>
    </xsl:variable>

    <table class="dataGroup">
      <thead>
        <tr>
          <td class="personHeader" colspan="4">
            <p class="personHeader">
              <span lang="EN-HK" class="personHeader bold">Proposer</span>
              <span lang="EN-HK" class="personHeader">
                <xsl:value-of select="concat(' (', $proposerName, ')')"/>
                <xsl:value-of select="$space"/>-<xsl:value-of select="$space"/>Proposer is<xsl:value-of select="$space"/>
              </span>
              <xsl:if test="/root/reportData/proposer/extra/isPhSameAsLa = 'N'">
                <span lang="EN-HK" class="personHeader">not<xsl:value-of select="$space"/></span>
              </xsl:if>
              <span lang="EN-HK" class="personHeader">Life Assured</span>
            </p>
          </td>
        </tr>

        <tr>
          <xsl:call-template  name="comSecHeaderNumCols01">
            <xsl:with-param   name="sectionHeader" select="'Personal Particulars'"/>
            <xsl:with-param   name="colspan" select="4"/>
          </xsl:call-template>
        </tr>
      </thead>

      <tbody>
        <tr class="dataGroup">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Name'"/>
            <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/fullName"/>
            <xsl:with-param   name="titleRight" select="'Gender'"/>
            <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/gender"/>
          </xsl:call-template>
        </tr>

        <tr class="dataGroup even">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Surname (as shown in ID)'"/>
            <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/lastName"/>
            <xsl:with-param   name="titleRight" select="'Marital Status'"/>
            <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/marital"/>
          </xsl:call-template>
        </tr>

        <tr class="dataGroup">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Given Name (as shown in ID)'"/>
            <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/firstName"/>
            <xsl:with-param   name="titleRight" select="'Nationality'"/>
            <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/nationality"/>
          </xsl:call-template>
        </tr>

        <xsl:choose>
          <xsl:when test="count(/root/reportData/proposer/personalInfo/prStatus) = 0">
            <tr class="dataGroup even">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'English or other name'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/othName"/>
                <xsl:with-param   name="titleRight" select="'ID Document Type'"/>
                <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/idDocType"/>
              </xsl:call-template>
            </tr>

            <tr class="dataGroup">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'Han Yu Pin Yin Name'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/hanyuPinyinName"/>
                <xsl:with-param   name="titleRight" select="'ID Number'"/>
                <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/idCardNo"/>
              </xsl:call-template>
            </tr>

            <tr class="dataGroup even">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'Date of Birth (dd/mm/yyyy)'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/dob"/>
                <xsl:with-param   name="titleRight" select="''"/>
                <xsl:with-param   name="valueRight" select="''"/>
                <xsl:with-param   name="isEmptyTitleRight" select="'true'"/>
                <xsl:with-param   name="isEmptyValueRight" select="'true'"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr class="dataGroup even">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'English or other name'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/othName"/>
                <xsl:with-param   name="titleRight" select="'Singapore PR Status'"/>
                <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/prStatus"/>
              </xsl:call-template>
            </tr>

            <tr class="dataGroup">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'Han Yu Pin Yin Name'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/hanyuPinyinName"/>
                <xsl:with-param   name="titleRight" select="'ID Document Type'"/>
                <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/idDocType"/>
              </xsl:call-template>
            </tr>

            <tr class="dataGroup even">
              <xsl:call-template  name="personalDetailsRowTmpl">
                <xsl:with-param   name="titleLeft"  select="'Date of Birth (dd/mm/yyyy)'"/>
                <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/dob"/>
                <xsl:with-param   name="titleRight" select="'ID Number'"/>
                <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/idCardNo"/>
              </xsl:call-template>
            </tr>
          </xsl:otherwise>
        </xsl:choose>



      </tbody>

    </table>

    <table class="dataGroup">
      <thead>

        <tr>
          <xsl:call-template  name="comSecHeaderNumCols01">
            <xsl:with-param   name="sectionHeader" select="'Professional Details'"/>
            <xsl:with-param   name="colspan" select="4"/>
          </xsl:call-template>
        </tr>
      </thead>

      <tbody>
        <tr class="dataGroup">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Name of Employer /Business/ School'"/>
            <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/organization"/>
            <xsl:with-param   name="titleRight" select="'Monthly Income/ Allowance SGD'"/>
            <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/allowance"/>
          </xsl:call-template>
        </tr>

        <tr class="dataGroup even">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Industry'"/>
            <xsl:with-param   name="valueLeft"  select="/root/reportData/proposer/personalInfo/industry"/>
            <xsl:with-param   name="titleRight" select="'Occupation'"/>
            <xsl:with-param   name="valueRight">
              <xsl:choose>
                <xsl:when test="/root/originalData/proposer/personalInfo/occupation = 'O921'">
                  <xsl:value-of select="/root/reportData/proposer/personalInfo/occupationOther"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="/root/reportData/proposer/personalInfo/occupation"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>

          </xsl:call-template>
        </tr>
      </tbody>
    </table>

    <table class="dataGroupLast">
      <thead>

        <tr>
          <xsl:call-template  name="comSecHeaderNumCols01">
            <xsl:with-param   name="sectionHeader" select="'Contact Details'"/>
            <xsl:with-param   name="colspan" select="4"/>
          </xsl:call-template>
        </tr>
      </thead>

      <tbody>
        <tr class="dataGroup">
          <td width="23%" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Residential Address</span>
            </p>
          </td>

          <td width="27%" class="tdLast padding">
            <xsl:call-template name="personalInfoAddressTmpl">
              <xsl:with-param name="addrBlock"      select="/root/reportData/proposer/personalInfo/addrBlock"/>
              <xsl:with-param name="addrStreet"     select="/root/reportData/proposer/personalInfo/addrStreet"/>
              <xsl:with-param name="addrUnitNum"    select="/root/reportData/proposer/personalInfo/unitNum"/>
              <xsl:with-param name="addrEstate"     select="/root/reportData/proposer/personalInfo/addrEstate"/>
              <xsl:with-param name="addrCity"       select="/root/reportData/proposer/personalInfo/addrCity"/>
              <xsl:with-param name="addrCountry"    select="/root/reportData/proposer/personalInfo/addrCountry"/>
              <xsl:with-param name="addrPostalCode" select="/root/reportData/proposer/personalInfo/postalCode"/>
            </xsl:call-template>
          </td>

          <td width="23%" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Mailing Address</span>
            </p>
          </td>

          <td width="27%" class="tdLast padding">
            <xsl:choose>
              <xsl:when test="/root/reportData/proposer/personalInfo/isSameAddr = 'Yes'">
                <xsl:call-template name="personalInfoAddressTmpl">
                  <xsl:with-param name="addrBlock"      select="/root/reportData/proposer/personalInfo/addrBlock"/>
                  <xsl:with-param name="addrStreet"     select="/root/reportData/proposer/personalInfo/addrStreet"/>
                  <xsl:with-param name="addrUnitNum"    select="/root/reportData/proposer/personalInfo/unitNum"/>
                  <xsl:with-param name="addrEstate"     select="/root/reportData/proposer/personalInfo/addrEstate"/>
                  <xsl:with-param name="addrCity"       select="/root/reportData/proposer/personalInfo/addrCity"/>
                  <xsl:with-param name="addrCountry"    select="/root/reportData/proposer/personalInfo/addrCountry"/>
                  <xsl:with-param name="addrPostalCode" select="/root/reportData/proposer/personalInfo/postalCode"/>
                </xsl:call-template>
              </xsl:when>

              <xsl:otherwise>
                <xsl:call-template name="personalInfoAddressTmpl">
                  <xsl:with-param name="addrBlock"      select="/root/reportData/proposer/personalInfo/mAddrBlock"/>
                  <xsl:with-param name="addrStreet"     select="/root/reportData/proposer/personalInfo/mAddrStreet"/>
                  <xsl:with-param name="addrUnitNum"    select="/root/reportData/proposer/personalInfo/mAddrnitNum"/>
                  <xsl:with-param name="addrEstate"     select="/root/reportData/proposer/personalInfo/mAddrEstate"/>
                  <xsl:with-param name="addrCity"       select="/root/reportData/proposer/personalInfo/mAddrCity"/>
                  <xsl:with-param name="addrCountry"    select="/root/reportData/proposer/personalInfo/mAddrCountry"/>
                  <xsl:with-param name="addrPostalCode" select="/root/reportData/proposer/personalInfo/mPostalCode"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>

        <tr class="dataGroup even">
          <xsl:call-template  name="personalDetailsRowTmpl">
            <xsl:with-param   name="titleLeft"  select="'Mobile'"/>
            <xsl:with-param   name="valueLeft">
              <xsl:value-of select="/root/reportData/proposer/personalInfo/mobileCountryCode"/> - <xsl:value-of select="/root/reportData/proposer/personalInfo/mobileNo"/>
            </xsl:with-param>
            <xsl:with-param   name="titleRight" select="'Email'"/>
            <xsl:with-param   name="valueRight" select="/root/reportData/proposer/personalInfo/email"/>
          </xsl:call-template>
        </tr>

      </tbody>
    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>




<xsl:template name="appform_shield_ph_personal_details_la">

  <xsl:for-each select="/root/reportData/insured">
    <xsl:variable name="iCid" select="personalInfo/cid"/>


    <xsl:variable name="odOccupation">
      <xsl:value-of select="/root/originalData/insured/personalInfo[cid = $iCid]/occupation"/>
    </xsl:variable>

    <div class="section">

      <table class="dataGroupLast">
        <thead>
          <tr>
            <td class="personHeader" colspan="4">
              <p class="personHeader">
                <span lang="EN-HK" class="personHeader bold">Life Assured</span>
                <span lang="EN-HK" class="personHeader">
                  <xsl:value-of select="concat(' (', personalInfo/fullName, ')')"/>
                </span>
              </p>
            </td>
          </tr>

          <tr>
            <xsl:call-template  name="comSecHeaderNumCols01">
              <xsl:with-param   name="sectionHeader" select="'Personal Particulars'"/>
              <xsl:with-param   name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>

        <tbody>
          <tr class="dataGroup">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Name'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/fullName"/>
              <xsl:with-param   name="titleRight" select="'Gender'"/>
              <xsl:with-param   name="valueRight" select="personalInfo/gender"/>
            </xsl:call-template>
          </tr>

          <tr class="dataGroup even">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Surname (as shown in ID)'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/lastName"/>
              <xsl:with-param   name="titleRight" select="'Marital Status'"/>
              <xsl:with-param   name="valueRight" select="personalInfo/marital"/>
            </xsl:call-template>
          </tr>

          <tr class="dataGroup">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Given Name (as shown in ID)'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/firstName"/>
              <xsl:with-param   name="titleRight" select="'Nationality'"/>
              <xsl:with-param   name="valueRight" select="personalInfo/nationality"/>
            </xsl:call-template>
          </tr>

          <xsl:choose>
            <xsl:when test="count(personalInfo/prStatus) = 0">
              <tr class="dataGroup even">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'English or other name'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/othName"/>
                  <xsl:with-param   name="titleRight" select="'ID Document Type'"/>
                  <xsl:with-param   name="valueRight" select="personalInfo/idDocType"/>
                </xsl:call-template>
              </tr>

              <tr class="dataGroup">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'Han Yu Pin Yin Name'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/hanyuPinyinName"/>
                  <xsl:with-param   name="titleRight" select="'ID Number'"/>
                  <xsl:with-param   name="valueRight" select="personalInfo/idCardNo"/>
                </xsl:call-template>
              </tr>

              <tr class="dataGroup even">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'Date of Birth (dd/mm/yyyy)'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/dob"/>
                  <xsl:with-param   name="isEmptyTitleRight" select="'true'"/>
                  <xsl:with-param   name="isEmptyValueRight" select="'true'"/>
                </xsl:call-template>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <tr class="dataGroup even">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'English or other name'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/othName"/>
                  <xsl:with-param   name="titleRight" select="'Singapore PR Status'"/>
                  <xsl:with-param   name="valueRight" select="personalInfo/prStatus"/>
                </xsl:call-template>
              </tr>

              <tr class="dataGroup">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'Han Yu Pin Yin Name'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/hanyuPinyinName"/>
                  <xsl:with-param   name="titleRight" select="'ID Document Type'"/>
                  <xsl:with-param   name="valueRight" select="personalInfo/idDocType"/>
                </xsl:call-template>
              </tr>

              <tr class="dataGroup even">
                <xsl:call-template  name="personalDetailsRowTmpl">
                  <xsl:with-param   name="titleLeft"  select="'Date of Birth (dd/mm/yyyy)'"/>
                  <xsl:with-param   name="valueLeft"  select="personalInfo/dob"/>
                  <xsl:with-param   name="titleRight" select="'ID Number'"/>
                  <xsl:with-param   name="valueRight" select="personalInfo/idCardNo"/>
                </xsl:call-template>
              </tr>
            </xsl:otherwise>
          </xsl:choose>

          <tr class="dataGroup">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Relationship to Proposer'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/relationship"/>
              <xsl:with-param   name="isEmptyTitleRight" select="'true'"/>
              <xsl:with-param   name="isEmptyValueRight" select="'true'"/>
            </xsl:call-template>
          </tr>
        </tbody>

      </table>

      <table class="dataGroupLast">
        <thead>

          <tr>
            <xsl:call-template  name="comSecHeaderNumCols01">
              <xsl:with-param   name="sectionHeader" select="'Professional Details'"/>
              <xsl:with-param   name="colspan" select="4"/>
            </xsl:call-template>
          </tr>
        </thead>

        <tbody>
          <tr class="dataGroup">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Name of Employer /Business/ School'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/organization"/>
              <xsl:with-param   name="titleRight" select="'Monthly Income/ Allowance SGD'"/>
              <xsl:with-param   name="valueRight" select="personalInfo/allowance"/>
            </xsl:call-template>
          </tr>

          <tr class="dataGroup even">
            <xsl:call-template  name="personalDetailsRowTmpl">
              <xsl:with-param   name="titleLeft"  select="'Industry'"/>
              <xsl:with-param   name="valueLeft"  select="personalInfo/industry"/>
              <xsl:with-param   name="titleRight"  select="'Occupation'"/>
              <xsl:with-param   name="valueRight">
                <xsl:choose>
                  <xsl:when test="$odOccupation = 'O921'">
                    <xsl:value-of select="personalInfo/occupationOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="personalInfo/occupation"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:with-param>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>

      <p class="sectionGroup">
        <span class="sectionGroup"><br/></span>
      </p>
    </div>
  </xsl:for-each>
</xsl:template>



<xsl:template name="appform_report_personal_details">
  <xsl:call-template    name  ="appform_shield_ph_personal_details"/>
  <xsl:call-template    name  ="appform_shield_ph_personal_details_la"/>
</xsl:template>

</xsl:stylesheet>
