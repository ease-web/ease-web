<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="premiumPaymentTmpl">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PAYMENT</span>
    </p>
    <table class="dataGroup">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'PREMIUM DETAILS'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
            <xsl:with-param name="header1" select="'Policy Number'"/>
            <xsl:with-param name="value1" select="/root/payment/proposalNo"/>
            <xsl:with-param name="header2" select="'Policy Currency'"/>
            <xsl:with-param name="value2" select="/root/payment/policyCcy"/>
          </xsl:call-template>
        </tr>
        <xsl:choose>
          <xsl:when test="/root/originalData/backdatingPrem &gt; 0 and /root/originalData/topupPrem &gt; 0 and /root/originalData/initRspPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Backdating Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/backdatingPrem"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Top up Premium'"/>
                <xsl:with-param name="value1" select="/root/payment/topupPrem"/>
                <xsl:with-param name="header2" select="'Initial RSP Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/initRspPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/backdatingPrem &gt; 0 and /root/originalData/topupPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Backdating Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/backdatingPrem"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Top up Premium'"/>
                <xsl:with-param name="value1" select="/root/payment/topupPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/backdatingPrem &gt; 0 and /root/originalData/initRspPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Backdating Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/backdatingPrem"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial RSP Premium'"/>
                <xsl:with-param name="value1" select="/root/payment/initRspPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/topupPrem &gt; 0 and /root/originalData/initRspPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Top up Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/topupPrem"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial RSP Premium'"/>
                <xsl:with-param name="value1" select="/root/payment/initRspPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/backdatingPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Backdating Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/backdatingPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/topupPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Top up Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/topupPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:when test="/root/originalData/initRspPrem &gt; 0">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
                <xsl:with-param name="header2" select="'Initial RSP Premium'"/>
                <xsl:with-param name="value2" select="/root/payment/initRspPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                <xsl:with-param name="header1" select="'Initial Basic Premium (including Riders, if any)'"/>
                <xsl:with-param name="value1" select="/root/payment/initBasicPrem"/>
              </xsl:call-template>
            </tr>
          </xsl:otherwise>
        </xsl:choose>
        <tr class="dataGroup">
          <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
            <xsl:with-param name="header2" select="'Total Initial Premium'"/>
            <xsl:with-param name="value2" select="/root/payment/initTotalPrem"/>
          </xsl:call-template>
        </tr>
      </tbody>
    </table>
    <table class="dataGroupLast">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'PAYMENT METHOD'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <xsl:if test="/root/payment/subseqPayMethod and string-length(/root/payment/subseqPayMethod) > 0">
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Subsequent Payment Method as GIRO'"/>
              <xsl:with-param name="value1" select="/root/payment/subseqPayMethod"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <tr class="dataGroup">
          <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
            <xsl:with-param name="header1" select="'Initial Payment Method'"/>
            <xsl:with-param name="value1" select="/root/payment/initPayMethod"/>
          </xsl:call-template>
        </tr>
        <xsl:if test="/root/payment/ttRemittingBank and string-length(/root/payment/ttRemittingBank) > 0">
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Remitting Bank'"/>
              <xsl:with-param name="value1" select="/root/payment/ttRemittingBank"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/payment/ttRemarks and string-length(/root/payment/ttRemarks) > 0">
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Remarks'"/>
              <xsl:with-param name="value1" select="/root/payment/ttRemarks"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/payment/ttDOR and string-length(/root/payment/ttDOR) > 0">
          <tr class="dataGroup">
            <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
              <xsl:with-param name="header1" select="'Date of Remittance (DD/MM/YYYY)'"/>
              <xsl:with-param name="value1" select="/root/payment/ttDOR"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/payment/initPayMethod[. = 'cpfissa' or . ='cpfisoa' or . = 'srs' or . = 'CPFIS-SA' or . = 'CPFIS-OA' or . = 'SRS']">
          <xsl:if test="/root/payment/initPayMethod[. = 'cpfissa' or . = 'CPFIS-SA']">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableOneColumnDataTmpl">
                <xsl:with-param name="header" select="'Special Account (SA)'"/>
                <xsl:with-param name="headerIsSubHeader" select="'true'"/>
                <xsl:with-param name="value" select="' '"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableColumnsDataWithCheckboxTmpl">
                <xsl:with-param name="header1" select="'CPF Account Number (NRIC)'"/>
                <xsl:with-param name="value1" select="/root/payment/cpfissaBlock/idCardNo"/>
                <xsl:with-param name="checkboxText" select="'Process CPF Fund Request on Specific Date'"/>
                <xsl:with-param name="checkboxValue" select="/root/payment/cpfissaBlock/cpfissaFundReqDate"/>
              </xsl:call-template>
            </tr>
            <xsl:if test="/root/payment/cpfissaBlock/cpfissaFundReqDate = 'Yes'">
              <tr class="dataGroup">
                <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                  <xsl:with-param name="header2" select="'CPF Fund Processing Date (DD/MM/YYYY)'"/>
                  <xsl:with-param name="value2" select="/root/payment/cpfissaBlock/cpfissaFundProcessDate"/>
                </xsl:call-template>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="/root/payment/initPayMethod[. = 'cpfisoa' or . = 'CPFIS-OA']">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableOneColumnDataTmpl">
                <xsl:with-param name="header" select="'Do you have an existing CPF Investment Account?'"/>
                <xsl:with-param name="value" select="/root/payment/cpfisoaBlock/cpfisoaIsHaveCpfAcct"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableOneColumnDataTmpl">
                <xsl:with-param name="header" select="'Ordinary Account (OA)'"/>
                <xsl:with-param name="headerIsSubHeader" select="'true'"/>
                <xsl:with-param name="value" select="' '"/>
              </xsl:call-template>
            </tr>
            <xsl:choose>
              <xsl:when test="/root/payment/cpfisoaBlock/cpfisoaIsHaveCpfAcct = 'Yes'">
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableColumnsDataWithCheckboxTmpl">
                    <xsl:with-param name="header1" select="'Bank Name'"/>
                    <xsl:with-param name="value1" select="/root/payment/cpfisoaBlock/cpfisoaBankNamePicker"/>
                    <xsl:with-param name="checkboxText" select="'Process CPF Fund Request on Specific Date'"/>
                    <xsl:with-param name="checkboxValue" select="/root/payment/cpfisoaBlock/cpfisoaFundReqDate"/>
                  </xsl:call-template>
                </tr>
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                    <xsl:with-param name="header1" select="'CPF Investment Account Number'"/>
                    <xsl:with-param name="value1" select="/root/payment/cpfisoaBlock/cpfisoaInvmAcctNo"/>
                    <xsl:with-param name="header2">
                      <xsl:choose>
                        <xsl:when test="/root/payment/cpfisoaBlock/cpfisoaFundReqDate = 'Yes'">
                          <xsl:value-of select="'CPF Fund Processing Date (DD/MM/YYYY)'"/>
                        </xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="value2">
                      <xsl:choose>
                        <xsl:when test="/root/payment/cpfisoaBlock/cpfisoaFundReqDate = 'Yes'">
                          <xsl:value-of select="/root/payment/cpfisoaBlock/cpfisoaFundProcessDate"/>
                        </xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </tr>
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                    <xsl:with-param name="header1" select="'CPF Account Number (NRIC)'"/>
                    <xsl:with-param name="value1" select="/root/payment/cpfisoaBlock/idCardNo"/>
                  </xsl:call-template>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <xsl:call-template name="dataGroupTableOneColumnCheckboxTmpl">
                    <xsl:with-param name="title" select="/root/payment/cpfisoaBlock/cpfisoaAcctDeclare"/>
                    <xsl:with-param name="value" select="'Yes'"/>
                  </xsl:call-template>
                </tr>
                <xsl:if test="contains(/root/payment/cpfisoaBlock/cpfisoaAcctDeclare, 'opening')">
                  <tr>
                    <td class="tdMid" colspan="8">
                      <p class="statement">
                        <span class="warningDeclaration">Please provide your CPF account details to your Financial Consultant by submitting the Supplementary Proposal Form.</span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <xsl:if test="/root/payment/initPayMethod[. = 'srs' or . = 'SRS']">
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableOneColumnDataTmpl">
                <xsl:with-param name="header" select="'Do you have an existing CPF Investment Account?'"/>
                <xsl:with-param name="value" select="/root/payment/srsBlock/srsIsHaveCpfAcct"/>
              </xsl:call-template>
            </tr>
            <tr class="dataGroup">
              <xsl:call-template name="dataGroupTableOneColumnDataTmpl">
                <xsl:with-param name="header" select="'Supplementary Retirement Scheme (SRS)'"/>
                <xsl:with-param name="headerIsSubHeader" select="'true'"/>
                <xsl:with-param name="value" select="' '"/>
              </xsl:call-template>
            </tr>
            <xsl:choose>
              <xsl:when test="/root/payment/srsBlock/srsIsHaveCpfAcct = 'Yes'">
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableColumnsDataWithCheckboxTmpl">
                    <xsl:with-param name="header1" select="'Bank Name'"/>
                    <xsl:with-param name="value1" select="/root/payment/srsBlock/srsBankNamePicker"/>
                    <xsl:with-param name="checkboxText" select="'Process CPF Fund Request on Specific Date'"/>
                    <xsl:with-param name="checkboxValue" select="/root/payment/srsBlock/srsFundReqDate"/>
                  </xsl:call-template>
                </tr>
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                    <xsl:with-param name="header1" select="'CPF Investment Account Number'"/>
                    <xsl:with-param name="value1" select="/root/payment/srsBlock/srsInvmAcctNo"/>
                    <xsl:with-param name="header2">
                      <xsl:choose>
                        <xsl:when test="/root/payment/srsBlock/srsFundReqDate = 'Yes'">
                          <xsl:value-of select="'CPF Fund Processing Date (DD/MM/YYYY)'"/>
                        </xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="value2">
                      <xsl:choose>
                        <xsl:when test="/root/payment/srsBlock/srsFundReqDate = 'Yes'">
                          <xsl:value-of select="/root/payment/srsBlock/srsFundProcessDate"/>
                        </xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </tr>
                <tr class="dataGroup">
                  <xsl:call-template name="dataGroupTableTwoColumnsDataTmpl">
                    <xsl:with-param name="header1" select="'CPF Account Number (NRIC)'"/>
                    <xsl:with-param name="value1" select="/root/payment/srsBlock/idCardNo"/>
                  </xsl:call-template>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <xsl:call-template name="dataGroupTableOneColumnCheckboxTmpl">
                    <xsl:with-param name="title" select="/root/payment/srsBlock/srsAcctDeclare"/>
                    <xsl:with-param name="value" select="'Yes'"/>
                  </xsl:call-template>
                </tr>
                <xsl:if test="contains(/root/payment/srsBlock/srsAcctDeclare, 'opening')">
                  <tr>
                    <td class="tdMid" colspan="8">
                      <p class="statement">
                        <span class="warningDeclaration">Please provide your CPF account details to your Financial Consultant by submitting the Supplementary Proposal Form.</span>
                      </p>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </xsl:if>
      </tbody>
    </table>
  </div>
</xsl:template>

</xsl:stylesheet>
