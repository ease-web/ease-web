<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="premiumPaymentTmpl">
  <div class="section">
    <p class="sectionGroup">  
      <span class="sectionGroup">PAYMENT</span>
    </p>
    
    <xsl:call-template name="infoSectionHeaderNoneTmplDiv">
      <xsl:with-param name="sectionHeader" select="'PREMIUM DETAILS'"/>
    </xsl:call-template>

    <div class="sectionContent">
      <div class="premiumDetails title">
        <div>
          <p>
            <span lang="EN-HK">Life Assured Name</span>
          </p>
        </div>
        <div>
          <p>
            <span lang="EN-HK">Plan / Rider Name</span>
          </p>
        </div>
        <div>
          <p>
            <span lang="EN-HK">Proposal Number</span>
          </p>
        </div>
        <div>
          <p>
            <span lang="EN-HK">CPF Portion<br/></span>
            <span lang="EN-HK" class="small">(incl. Medishield Life)</span>
          </p>
        </div>
        <div>
          <p>
            <span lang="EN-HK">Cash Portion</span>
          </p>
        </div>
        <div>
          <p>
            <span lang="EN-HK">Subsequent Premium<br/>Payment Method*</span>
          </p>
        </div>
      </div>
      <xsl:for-each select="/root/reportData/premiumDetails">
        <div>
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="(position() mod 2) = 1">
                <xsl:value-of select="'premiumDetails value odd'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'premiumDetails value'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <div>
            <p>
              <span lang="EN-HK" class="">
                <xsl:value-of select="laName"/>
              </span>
            </p>
          </div>
          <div>
            <p>
              <span lang="EN-HK">
                <xsl:value-of select="covName/en"/>
              </span>
            </p>
          </div>
          <div>
            <p>
              <span lang="EN-HK">
                <xsl:value-of select="policyNumber"/>
              </span>
            </p>
          </div>
          <div>
            <p>
              <span lang="EN-HK">
                <xsl:choose>
                  <xsl:when test="contains(medisave, '$') and not(medisave = 0)"><xsl:value-of select="medisave"/></xsl:when>
                  <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </div>
          <div>
            <p>
              <span lang="EN-HK">
                <xsl:choose>
                  <xsl:when test="contains(cashPortion, '$') and not(cashPortion = 0)"><xsl:value-of select="cashPortion"/></xsl:when>
                  <xsl:otherwise>-</xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </div>
          <div>
            <p>
              <span lang="EN-HK">
                <xsl:choose>
                  <xsl:when test="subseqPayMethod = 'No'">-</xsl:when>
                  <xsl:otherwise><xsl:copy-of select="$checkboxTick"/>GIRO</xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </div>
        </div>
      </xsl:for-each>
      <div class="premiumDetails total">
        <div><br/></div>
        <div><br/></div>
        <div class="title">
          <p>
            <span lang="EN-HK" class="">Total</span>
          </p>
        </div>
        <div class="value">
          <p>
            <span lang="EN-HK">
              <xsl:choose>
                <xsl:when test="/root/originalData/totMedisave > 0"><xsl:value-of select="/root/reportData/totMedisave"/></xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </div>
        <div class="value">
          <p>
            <span lang="EN-HK">
              <xsl:choose>
                <xsl:when test="/root/originalData/totCashPortion > 0"><xsl:value-of select="/root/reportData/totCashPortion"/></xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </div>
        <div><br/></div>
      </div>
      <div class="warning">
        <div>
          <p>
            <span lang="EN-HK">
              *The CPF account no. used for initial payment will be used for all subsequent payments for AXA shield.
            </span>
          </p>
          <p>
            <span lang="EN-HK">
              *Any premium in excess of the withdrawal limits on Medisave will be payable via the selected subsequent payment method.
            </span>
          </p>
        </div>
      </div>
    </div>


    
    <xsl:call-template name="infoSectionHeaderNoneTmplDiv">
      <xsl:with-param name="sectionHeader" select="'INITIAL PAYMENT METHOD'"/>
    </xsl:call-template>


    <div class="sectionContent">
      <div class="initPayment subtitle">
        <div>
          <p>
            <span lang="EN-HK">CPF Portion (Medisave Account Details)</span>
          </p>
        </div>
      </div>
      <div class="initPayment">
        <div class="title">
          <p>
            <span lang="EN-HK">CPF account Holder Name</span>
          </p>
        </div>
        <div class="title">
          <p>
            <span lang="EN-HK">CPF account no.</span>
          </p>
        </div>
      </div>
      <div class="initPayment">
        <div class="value">
          <p>
            <xsl:copy-of select="$checkboxTick"/>
            <span lang="EN-HK">
              <xsl:value-of select="/root/reportData/cpfMedisaveAccDetails/cpfAccHolderName"/>
            </span>
          </p>
        </div>
        <div class="value">
          <p>
            <span lang="EN-HK">
              <xsl:value-of select="/root/reportData/cpfMedisaveAccDetails/cpfAccNo"/>
            </span>
          </p>
        </div>
      </div>
      <div>
        <xsl:attribute name="class">
          <xsl:choose>
            <xsl:when test="/root/originalData/totCashPortion > 0">
              <xsl:value-of select="'initPayment'"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'initPayment lastRow'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <br/>
      </div>
      <xsl:if test="/root/originalData/totCashPortion > 0">
        <div class="initPayment subtitle">
          <div>
            <p>
              <span lang="EN-HK">Cash Portion</span>
            </p>
          </div>
        </div>
        <div>
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="/root/reportData/ttRemittingBank and string-length(/root/reportData/ttRemittingBank) > 0">
                <xsl:value-of select="'initPayment'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'initPayment lastRow'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <div class="title">
            <p>
              <span lang="EN-HK">Initial Payment Method</span>
            </p>
          </div>
          <div class="value">
            <p>
              <span lang="EN-HK">
                <xsl:value-of select="/root/reportData/initPayMethod"/>
              </span>
            </p>
          </div>
        </div>
        <xsl:if test="/root/reportData/ttRemittingBank and string-length(/root/reportData/ttRemittingBank) > 0">
          <div class="initPayment">
            <div class="title">
              <p>
                <span lang="EN-HK">Remitting Bank</span>
              </p>
            </div>
            <div class="value">
              <p>
                <span lang="EN-HK">
                  <xsl:value-of select="/root/reportData/ttRemittingBank"/>
                </span>
              </p>
            </div>
          </div>
        </xsl:if>
        <xsl:if test="/root/reportData/ttRemarks and string-length(/root/reportData/ttRemarks) > 0">
          <div class="initPayment">
            <div class="title">
              <p>
                <span lang="EN-HK">Remarks</span>
              </p>
            </div>
            <div class="value">
              <p>
                <span lang="EN-HK">
                  <xsl:value-of select="/root/reportData/ttRemarks"/>
                </span>
              </p>
            </div>
          </div>
        </xsl:if>
        <xsl:if test="/root/reportData/ttRemittingBank and string-length(/root/reportData/ttRemittingBank) > 0">
          <div class="initPayment lastRow">
            <div class="title">
              <p>
                <span lang="EN-HK">Date of Remittance (DD/MM/YYYY)</span>
              </p>
            </div>
            <div class="value">
              <p>
                <span lang="EN-HK">
                  <xsl:value-of select="/root/reportData/ttDOR"/>
                </span>
              </p>
            </div>
          </div>
        </xsl:if>
      </xsl:if>
    </div>
  </div>
</xsl:template>

</xsl:stylesheet>
